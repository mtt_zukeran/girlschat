﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AccessMailCountDailyList.aspx.cs" Inherits="Status_AccessMailCountDailyList" Title="アクセス日別メールからのアクセス集計"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="アクセス日別メールからのアクセス集計"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" class="tableStyle" style="width: 200px">
					<tr>
                        <td class="tdHeaderStyle">
                            送信日
                        </td>
                        <td class="tdDataStyle" align="right">
                            <asp:Label ID="lblTxMailDay" runat="server" Visible="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
						<td class="tdHeaderStyle2">
							送信数
						</td>
						<td class="tdDataStyle" align="right">
							<asp:Label ID="lblTxMailCount" runat="server" Visible="true"></asp:Label>
						</td>
					</tr>
				</table>
			</asp:Panel>
		</fieldset>
		<fieldset>
			<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="750px">
				<asp:GridView ID="grdAccessMailCountDaily" runat="server" AutoGenerateColumns="False" DataSourceID="dsAccessMailCountDaily" CellPadding="0" AllowSorting="True" ShowFooter="true"
					OnRowDataBound="grdAccessMailCountDaily_RowDataBound" SkinID="GridViewColorNoRowStyle">
					<Columns>
						<asp:TemplateField HeaderText="経過日数">
							<ItemStyle HorizontalAlign="Center" Width="70px" />
							<ItemTemplate>
								<asp:Label ID="Label1" runat="server" Text='<%# Eval("PASSAGE_DAY_COUNT") %>' BackColor=''></asp:Label>日目
							</ItemTemplate>
							<FooterTemplate>
								<asp:Label ID="lblPassageDayCountFooter" Text="合計" runat="server"></asp:Label>
							</FooterTemplate>
							<FooterStyle HorizontalAlign="center" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="ｱｸｾｽ総数">
							<HeaderStyle Wrap="false" />
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# Eval("ACCESS_COUNT") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
							<FooterTemplate>
								<asp:Label ID="lblAccessCountFooter" Text="" runat="server"></asp:Label>
							</FooterTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="ﾕﾆｰｸ数">
							<HeaderStyle Wrap="false" />
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# Eval("UNIQUE_ACCESS_COUNT") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
							<FooterTemplate>
								<asp:Label ID="lblUniqueAccessCountFooter" Text="" runat="server"></asp:Label>
							</FooterTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="ｱｸｾｽ率">
							<HeaderStyle Wrap="false" />
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text=''></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
							<FooterTemplate>
								<asp:Label ID="lblUniqueAccessRateFooter" Text="" runat="server"></asp:Label>
							</FooterTemplate>
						</asp:TemplateField>
					</Columns>
					<FooterStyle ForeColor="Black" BackColor="LightYellow" />
				</asp:GridView>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsAccessMailCountDaily" runat="server" SelectMethod="GetAccessMailCountByAccessDay" TypeName="AccessMailCountDaily" OnSelecting="dsAccessMailCountDaily_Selecting">
		<SelectParameters>
            <asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pMailTemplateNo" Type="String" />
			<asp:Parameter Name="pSexCd" Type="String" />
			<asp:Parameter Name="pTxMailDay" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
</asp:Content>
