﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TwilioSessionList.aspx.cs" Inherits="Status_TwilioSessionList" Title="Untitled Page" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" Runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="音声通話セッション"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" Runat="Server">
	 <div class="admincontent">
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset>
				<legend>[音声通話セッション]</legend>
				<asp:Button runat="server" ID="btnRefresh" Text="リフレッシュ" CssClass="seekbutton" OnClick="btnRefresh_Click" CausesValidation="False" />
				<br /><br />
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="500px">
					<asp:GridView ID="grdTwilioSession" runat="server" AllowPaging="False" AutoGenerateColumns="False" DataSourceID="dsTwilioSession" AllowSorting="False" SkinID="GridViewColor">
						<Columns>
							<asp:TemplateField HeaderText="通話開始時間">
								<ItemTemplate>
									<asp:Label ID="lblStartTime" runat="server" Text='<%# Eval("START_TIME") %>' />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="通話終了時間">
								<ItemTemplate>
									<asp:Label ID="lblEndTime" runat="server" Text='<%# Eval("END_TIME") %>' />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="発信者">
								<ItemTemplate>
									<asp:HyperLink ID="lnkFromTelNo" runat="server" NavigateUrl='<%# GetFromTelUrl(Eval("FROM_TEL_NO"),Eval("REQUESTER_SEX_CD"),Eval("USE_MAN_VOICEAPP_FLAG"),Eval("USE_CAST_VOICEAPP_FLAG")) %>' Text='<%# Eval("FROM_TEL_NO") %>'></asp:HyperLink>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="着信者">
								<ItemTemplate>
									<asp:HyperLink ID="lnkToTelNo" runat="server" NavigateUrl='<%# GetToTelUrl(Eval("TO_TEL_NO"),Eval("REQUESTER_SEX_CD"),Eval("USE_MAN_VOICEAPP_FLAG"),Eval("USE_CAST_VOICEAPP_FLAG")) %>' Text='<%# Eval("TO_TEL_NO") %>'></asp:HyperLink>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="種別">
								<ItemTemplate>
									<asp:Label ID="lblType" runat="server" Text='<%# GetTypeText(Eval("REQUESTER_SEX_CD"),Eval("USE_FREE_DIAL_FLAG"),Eval("USE_WHITE_PLAN_FLAG")) %>' />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="通話可能秒数">
								<ItemTemplate>
									<asp:Label ID="lblAvailableSec" runat="server" Text='<%# Eval("AVAILABLE_SEC") %>' />
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="通話秒数">
								<ItemTemplate>
									<asp:Label ID="lblDuration" runat="server" Text='<%# Eval("DURATION") %>' />
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="状態">
								<ItemTemplate>
									<asp:Label ID="lblStatus" runat="server" Text='<%# GetStatusText(Eval("STATUS")) %>' ForeColor='<%# GetStatusColor(Eval("Status")) %>' />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="操作">
								<ItemTemplate>
									<asp:LinkButton ID="lnkDisconnect" runat="server" Text="切断" CommandArgument='<%# Eval("PARENT_CALL_SID") %>' OnCommand="lnkDisconnect_Command" Enabled='<%# GetDisconnectEnabled(Eval("STATUS")) %>'></asp:LinkButton>
									<ajaxToolkit:ConfirmButtonExtender ID="cfmLnkDisconnect" runat="server" TargetControlID="lnkDisconnect" ConfirmText="切断します。よろしいですか？" ConfirmOnFormSubmit="true" />
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
					</asp:GridView>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
	 </div>
	<asp:ObjectDataSource ID="dsTwilioSession" runat="server" SelectMethod="GetSessionList" TypeName="Twilio" EnablePaging="False" OnSelecting="dsTwilioSession_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pAccountSid" Type="String" />
			<asp:Parameter Name="pAuthToken" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>

