﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" 
    CodeFile="OmikujiLog.aspx.cs" Inherits="Status_OmikujiLog" Title="おみくじ履歴"
    ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="おみくじ履歴"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <fieldset class="fieldset">
            <legend>[検索条件]</legend>
            <fieldset class="fieldset-inner">
                <table border="0" style="width: 800px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle">
                            サイトコード
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                DataValueField="SITE_CD" Width="180px">
                            </asp:DropDownList>
                        </td>
                        <td class="tdHeaderStyle2">
                            ログインID

                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtLoginId" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle2">
                            期間
                        </td>
                        <td class="tdDataStyle" colspan="3">
                            <asp:DropDownList ID="lstFromYYYY" runat="server" Width="60px" DataSource='<%# YearArray %>'>
                            </asp:DropDownList>年
                            <asp:DropDownList ID="lstFromMM" runat="server" Width="40px" DataSource='<%# MonthArray %>'>
                            </asp:DropDownList>月
                            <asp:DropDownList ID="lstFromDD" runat="server" Width="40px" DataSource='<%# DayArray %>'>
                            </asp:DropDownList>日～
                            <asp:DropDownList ID="lstToYYYY" runat="server" Width="60px" DataSource='<%# YearArray %>'>
                            </asp:DropDownList>年
                            <asp:DropDownList ID="lstToMM" runat="server" Width="40px" DataSource='<%# MonthArray %>'>
                            </asp:DropDownList>月
                            <asp:DropDownList ID="lstToDD" runat="server" Width="40px" DataSource='<%# DayArray %>'>
                            </asp:DropDownList>日
                        </td>
                    </tr>
                </table>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    CausesValidation="False" ValidationGroup="Search" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
                    CausesValidation="False" ValidationGroup="Search" />
            </fieldset>
            <asp:Panel runat="server" ID="pnlGrid"　Visible="false">
                <fieldset class="fieldset-inner">
                    <legend>[履歴一覧]</legend>
                    <asp:Panel runat="server" ID="pnlCount">
                        <a class="reccount">Record Count
                            <%#GetRecCount() %>
                        </a>
                        <br />
                        <a class="reccount">Current viewing page
                            <%=grdOmikujiLog.PageIndex + 1%>
                            of
                            <%=grdOmikujiLog.PageCount%>
                        </a>
                    </asp:Panel>
                    &nbsp;
                    <asp:GridView ID="grdOmikujiLog" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        DataSourceID="" AllowSorting="True" SkinID="GridViewColor">
                        <Columns>
                            <asp:TemplateField HeaderText="ログインID">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkLoginId" runat="server" Text='<%# Eval("LOGIN_ID") %>' CausesValidation="False" 
                                        CommandArgument='<%# string.Format("{0},{1}",Eval("SITE_CD"),Eval("LOGIN_ID")) %>' OnCommand="lnkLoginId_Command"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="付与P" DataField="OMIKUJI_POINT" >
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="付与日時" DataField="CREATE_DATE" >
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </fieldset>
            </asp:Panel>
        </fieldset>
        <br />
    </div>
    <asp:ObjectDataSource ID="dsOmikujiLog" runat="server" EnablePaging="True" OnSelected="dsOmikujiLog_Selected" OnSelecting="dsOmikujiLog_Selecting"
        SelectCountMethod="GetPageCount" SelectMethod="GetPageCollection" TypeName="OmikujiLog">
        <SelectParameters>
            <asp:Parameter Name="pSiteCd"           Type="String" />
            <asp:Parameter Name="pSexCd"            Type="String" />
            <asp:Parameter Name="pLoginId"          Type="String" />
            <asp:Parameter Name="pReportDayFrom"    Type="String" />
            <asp:Parameter Name="pReportDayTo"      Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
</asp:Content>
