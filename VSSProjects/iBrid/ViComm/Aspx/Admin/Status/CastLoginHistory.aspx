﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CastLoginHistory.aspx.cs" Inherits="Status_CastLoginHistory"
	Title="出演者待機履歴" %>

<%@ Import Namespace="ViComm" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="出演者待機履歴"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" class="tableStyle">
					<tr>
						<td class="tdHeaderSmallStyle">
							サイト
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderStyle">
							待機開始日時
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtReportDayFrom" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:DropDownList ID="lstReportTimeFrom" runat="server" Width="44px">
							</asp:DropDownList>
							～
							<asp:TextBox ID="txtReportDayTo" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:DropDownList ID="lstReportTimeTo" runat="server" Width="44px">
							</asp:DropDownList>
							<asp:RequiredFieldValidator ID="vdrReportDayFrom" runat="server" ErrorMessage="待機開始日Fromを入力して下さい。" ControlToValidate="txtReportDayFrom" ValidationGroup="Key"
								Display="Dynamic">*</asp:RequiredFieldValidator>
							<asp:RangeValidator ID="vdeReportDayFrom" runat="server" ErrorMessage="待機開始日Fromを正しく入力して下さい。" ControlToValidate="txtReportDayFrom" MaximumValue="2099/12/31"
								MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							<asp:RangeValidator ID="vdeReportDayTo" runat="server" ErrorMessage="待機開始日Toを正しく入力して下さい。" ControlToValidate="txtReportDayTo" MaximumValue="2099/12/31"
								MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							<asp:CompareValidator ID="vdcReportDay" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtReportDayFrom" ControlToValidate="txtReportDayTo"
								Operator="GreaterThanEqual" ValidationGroup="Key" Display="Dynamic">*</asp:CompareValidator>
						</td>
						<td class="tdHeaderSmallStyle">
							出力方式						</td>
						<td class="tdDataStyle">
							<asp:RadioButton ID="rdoDetail" runat="server" Text="明細" GroupName="ListType" AutoPostBack="True" Checked="True" OnCheckedChanged="rdoList_CheckedChanged">
							</asp:RadioButton>
							<asp:RadioButton ID="rdoSummay" runat="server" Text="集計" GroupName="ListType" AutoPostBack="True" OnCheckedChanged="rdoList_CheckedChanged">
							</asp:RadioButton>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderSmallStyle2">
							ログインＩＤ
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtLoginId" runat="server"></asp:TextBox>
						</td>
						<td class="tdHeaderStyle2">
							ハンドル名
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtHandelNm" runat="server"></asp:TextBox>
						</td>
					    <td class="tdHeaderStyle2">
						    Paging Off
					    </td>
					    <td class="tdDataStyle">
						    <asp:CheckBox ID="chkPagingOff" runat="server" />
					    </td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
			</asp:Panel>
		</fieldset>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset>
				<legend>[ログイン履歴]</legend>
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="560px">
					<asp:GridView ID="grdLogin" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsLoginCharacter" AllowSorting="True" SkinID="GridViewColor"
						PageSize="100" Font-Size="X-Small" OnSorting="grdLogin_Sorting">
						<Columns>
							<asp:BoundField DataField="START_DATE" HeaderText="開始" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
								<ItemStyle HorizontalAlign="Center" />
							</asp:BoundField>
							<asp:BoundField DataField="LOGOFF_DATE" HeaderText="終了" DataFormatString="{0:HH:mm}" HtmlEncode="False">
								<ItemStyle HorizontalAlign="Center" />
							</asp:BoundField>
							<asp:TemplateField HeaderText="ﾛｸﾞｲﾝID">
								<ItemTemplate>
									<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# string.Format("~/Cast/CastView.aspx?loginid={0}&return={1}",Eval("LOGIN_ID"),"CastLoginHistory.aspx") %>'
										Text='<%# Eval("LOGIN_ID") %>'>
									</asp:HyperLink>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾊﾝﾄﾞﾙ名">
								<ItemStyle HorizontalAlign="Left" />
								<ItemTemplate>
									<asp:Label runat="server" Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>' ID="lblHandleNm" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField DataField="WORK_MIN" HeaderText="待機分" SortExpression="WORK_MIN">
								<ItemStyle HorizontalAlign="Right" />
							</asp:BoundField>
							<asp:BoundField DataField="DUMMY_MIN" HeaderText="ﾀﾞﾐｰ分" SortExpression="DUMMY_MIN">
								<ItemStyle HorizontalAlign="Right" />
							</asp:BoundField>
							<asp:TemplateField HeaderText="合計&lt;BR&gt;分/回" SortExpression="TOTAL_TALK_MIN">
								<ItemTemplate>
									<asp:Label ID="Label7" runat="server" Text='<%#string.Format("{0}({1,2})",Eval("TOTAL_TALK_MIN"),Eval("TOTAL_TALK_COUNT")) %>' Width="50px"></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="TV&lt;BR&gt;分/回" SortExpression="PRV_TV_TALK_MIN">
								<ItemTemplate>
									<asp:Label ID="Label7" runat="server" Text='<%#string.Format("{0}({1,2})",Eval("PRV_TV_TALK_MIN"),Eval("PRV_TV_TALK_COUNT")) %>' Width="50px"></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="TVﾁｬｯﾄ&lt;BR&gt;分/回" SortExpression="PUB_TV_TALK_MIN">
								<ItemTemplate>
									<asp:Label ID="Label8" runat="server" Text='<%#string.Format("{0}({1,2})",Eval("PUB_TV_TALK_MIN"),Eval("PUB_TV_TALK_COUNT")) %>' Width="50px"></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="音声&lt;BR&gt;分/回" SortExpression="PRV_VOICE_TALK_MIN">
								<ItemTemplate>
									<asp:Label ID="Label8" runat="server" Text='<%#string.Format("{0}({1,2})",Eval("PRV_VOICE_TALK_MIN"),Eval("PRV_VOICE_TALK_COUNT")) %>' Width="50px"></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="音声ﾁｬｯﾄ&lt;BR&gt;分/回" SortExpression="PUB_VOICE_TALK_MIN">
								<ItemTemplate>
									<asp:Label ID="Label8" runat="server" Text='<%#string.Format("{0}({1,2})",Eval("PUB_VOICE_TALK_MIN"),Eval("PUB_VOICE_TALK_COUNT")) %>' Width="50px"></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="会話ﾓﾆﾀ&lt;BR&gt;分/回" SortExpression="VIEW_TALK_MIN">
								<ItemTemplate>
									<asp:Label ID="Label8" runat="server" Text='<%#string.Format("{0}({1,2})",Eval("VIEW_TALK_MIN"),Eval("VIEW_TALK_COUNT")) %>' Width="50px"></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="音声ﾓﾆﾀ&lt;BR&gt;分/回" SortExpression="WIRETAP_MIN">
								<ItemTemplate>
									<asp:Label ID="Label8" runat="server" Text='<%#string.Format("{0}({1,2})",Eval("WIRETAP_MIN"),Eval("WIRETAP_COUNT")) %>' Width="50px"></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="部屋ﾓﾆﾀ&lt;BR&gt;分/回" SortExpression="VIEW_BROADCAST_MIN">
								<ItemTemplate>
									<asp:Label ID="Label8" runat="server" Text='<%#string.Format("{0}({1,2})",Eval("VIEW_BROADCAST_MIN"),Eval("VIEW_BROADCAST_COUNT")) %>' Width="50px"></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="待機TV&lt;BR&gt;分/回" SortExpression="CAST_PRV_TV_TALK_MIN">
								<ItemTemplate>
									<asp:Label ID="Label8" runat="server" Text='<%#string.Format("{0}({1,2})",Eval("CAST_PRV_TV_TALK_MIN"),Eval("CAST_PRV_TV_TALK_COUNT")) %>' Width="50px"></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="待機TV&lt;BR&gt;ﾁｬｯﾄ分/回" SortExpression="CAST_PUB_TV_TALK_MIN">
								<ItemTemplate>
									<asp:Label ID="Label8" runat="server" Text='<%#string.Format("{0}({1,2})",Eval("CAST_PUB_TV_TALK_MIN"),Eval("CAST_PUB_TV_TALK_COUNT")) %>' Width="50px"></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="待機音声&lt;BR&gt;分/回" SortExpression="CAST_PRV_VOICE_TALK_MIN">
								<ItemTemplate>
									<asp:Label ID="Label8" runat="server" Text='<%#string.Format("{0}({1,2})",Eval("CAST_PRV_VOICE_TALK_MIN"),Eval("CAST_PRV_VOICE_TALK_COUNT")) %>' Width="50px"></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="待機音声&lt;BR&gt;ﾁｬｯﾄ分/回" SortExpression="CAST_PUB_VOICE_TALK_MIN">
								<ItemTemplate>
									<asp:Label ID="Label8" runat="server" Text='<%#string.Format("{0}({1,2})",Eval("CAST_PUB_VOICE_TALK_MIN"),Eval("CAST_PUB_VOICE_TALK_COUNT")) %>' Width="50px"></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
							</asp:TemplateField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsLoginCharacter" runat="server" SelectMethod="GetSitePageCollection" TypeName="LoginCharacter" SelectCountMethod="GetSitePageCount"
		EnablePaging="True" OnSelecting="dsLoginCharacter_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pReportDayFrom" Type="String" />
			<asp:Parameter Name="pReportDayTo" Type="String" />
			<asp:Parameter Name="pSummaryFlag" Type="Int16" />
			<asp:Parameter Name="pLoginId" Type="String" />
			<asp:Parameter Name="pHandleNm" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrReportDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdeReportDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdeReportDayTo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="vdcReportDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:MaskedEditExtender ID="mskReportDayFrom" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="False"
		TargetControlID="txtReportDayFrom">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskReportDayTo" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="False"
		TargetControlID="txtReportDayTo">
	</ajaxToolkit:MaskedEditExtender>
</asp:Content>
