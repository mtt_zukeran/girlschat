﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Progaram ID		: UserDefPoinHistoryInquiry
--
--  Creation Date	: 2011.06.11
--  Creater			: iBrid
--
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class Status_UserDefPointHistoryInquiry : System.Web.UI.Page {
	private string SexCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SexCd"]);
		}
		set {
			this.ViewState["SexCd"] = value;
		}
	}

	string recCount;

	protected void Page_Load(object sender, EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);

		if (!IsPostBack) {
			this.SexCd = this.Request.QueryString["sexcd"];
			this.InitPage();
			this.GetList();
		}
	}

	protected void btnListSeek_Click(object sender, EventArgs e) {
		this.GetList();
	}

	protected void btnClear_Click(object sender, EventArgs e) {
		this.InitPage();
	}

	protected void btnCSV_Click(object sender, EventArgs e) {
		if (this.grdUserDefPointHistory.Rows.Count == 0) {
			return;
		}

		DataTable oCsvData;
		using (UserDefPointHistory oUserDefPointHistory = new UserDefPointHistory()) {
			using (DataSet oDataSet = oUserDefPointHistory.GetListPageCollection(
				this.lstSiteCd.SelectedValue,
				this.txtLoginId.Text.TrimEnd(),
				this.txtDayFrom.Text,
				string.IsNullOrEmpty(this.txtDayTo.Text) ? this.txtDayFrom.Text : this.txtDayTo.Text,
				this.SexCd,
				this.chkCastSite.Checked ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR,
				0,
				SysConst.DB_MAX_ROWS)) {

				if (oDataSet.Tables[0].Rows.Count > 0) {
					oCsvData = oDataSet.Tables[0];
				} else {
					return;
				}
			}
		}

		//ヘッダ作成
		string sHeader = "サイトコード,追加日時,ユーザーID,ハンドル名,氏名,ポイントID,増減ポイント,備考,性別コード,性別";

		//CSV出力 
		Response.ContentType = "application/download";
		Response.AppendHeader("Content-Disposition", "attachment;filename=UserDefPointHistoryList.csv");
		Response.ContentEncoding = System.Text.Encoding.GetEncoding("Shift_JIS");

		Response.Write(sHeader + "\r\n");
		Response.Write("$NO_TRANS_START;");
		foreach (DataRow oCsvRow in oCsvData.Rows) {
			DateTime oCreateDate;
			string sData =
				SetCsvString(oCsvRow["SITE_CD"].ToString()) + "," +
				SetCsvString(DateTime.TryParse(oCsvRow["ADD_POINT_DATE"].ToString(), out oCreateDate) ? oCreateDate.ToString("yyyy/MM/dd HH:mm:ss") : string.Empty) + "," +
				SetCsvString(oCsvRow["LOGIN_ID"].ToString()) + "," +
				SetCsvString(oCsvRow["HANDLE_NM"].ToString()) + "," +
				SetCsvString(oCsvRow["USER_NM"].ToString()) + "," +
				SetCsvString(oCsvRow["ADD_POINT_ID"].ToString()) + "," +
				SetCsvString(oCsvRow["ADD_POINT"].ToString()) + "," +
				SetCsvString(oCsvRow["REMARKS"].ToString()) + "," +
				SetCsvString(oCsvRow["SEX_CD"].ToString()) + "," +
				SetCsvString(oCsvRow["SEX_NM"].ToString());

			Response.Write(sData + "\r\n");
		}
		Response.Write("$NO_TRANS_END;");
		Response.End();
	}

	protected void dsUserDefPointHistory_Selecting(object sender, ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pSiteCd"] = this.lstSiteCd.SelectedValue;
		e.InputParameters["pLoginId"] = this.txtLoginId.Text.TrimEnd();
		e.InputParameters["pDayFrom"] = this.txtDayFrom.Text;
		if (string.IsNullOrEmpty(this.txtDayTo.Text)) {
			this.txtDayTo.Text = this.txtDayFrom.Text;
		}
		e.InputParameters["pDayTo"] = this.txtDayTo.Text;
		e.InputParameters["pContainsCastSite"] = this.chkCastSite.Checked ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
	}

	protected void dsUserDefPointHistory_Selected(object sender, ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void grdUserDefPointHistory_DataBound(object sender, EventArgs e) {
		if (ViCommConst.MAN.Equals(this.SexCd)) {
			this.grdUserDefPointHistory.Columns[3].Visible = false;
		}
	}

	private void InitPage() {
		this.pnlInfo.Visible = false;
		this.txtLoginId.Text = string.Empty;
		//this.lstSexCd.SelectedIndex = 0;
		this.txtDayFrom.Text = DateTime.Now.ToString("yyyy/MM/dd");
		this.txtDayTo.Text = DateTime.Now.ToString("yyyy/MM/dd");
		this.lstSiteCd.DataBind();
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals(string.Empty)) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}

		if (ViCommConst.MAN.Equals(this.SexCd)) {
			this.tdDataCastSite.Visible = false;
			this.tdHeaderCastSite.Visible = false;
			this.tdHeaderAddPointDate.Attributes["class"] = "tdHeaderStyle2";
			this.tdDataLoginId.ColSpan = 3;
		} else {
			this.chkCastSite.Checked = true;
			this.tdDataCastSite.Visible = true;
			this.tdHeaderCastSite.Visible = true;
			this.tdHeaderAddPointDate.Attributes["class"] = "tdHeaderStyle";
			this.tdDataLoginId.ColSpan = 1;
		}
	}

	private string SetCsvString(string pData) {
		if (pData.IndexOf('"') > -1 ||
			pData.IndexOf(',') > -1 ||
			pData.IndexOf('\r') > -1 ||
			pData.IndexOf('\n') > -1 ||
			pData.StartsWith(" ") || pData.StartsWith("\t") ||
			pData.EndsWith(" ") || pData.EndsWith("\t")) {
			if (pData.IndexOf('"') > -1) {
				//"を""とする
				pData = pData.Replace("\"", "\"\"");
			}
			if (pData.IndexOf('\r') > -1 || pData.IndexOf('\n') > -1) {
				//改行しない 
				pData = pData.Replace("\r", string.Empty);
				pData = pData.Replace("\n", string.Empty);
			}
			pData = "\"" + pData + "\"";
		}
		return pData;
	}

	private void GetList() {
		if (string.IsNullOrEmpty(this.txtDayTo.Text)) {
			this.txtDayTo.Text = this.txtDayFrom.Text;
		}

		this.grdUserDefPointHistory.PageSize = 50;
		this.grdUserDefPointHistory.DataSourceID = "dsUserDefPointHistory";
		this.grdUserDefPointHistory.DataBind();
		this.pnlCount.DataBind();
		this.pnlInfo.Visible = true;
	}

	protected string GetRecCount() {
		return this.recCount;
	}
}
