﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 期間別出演者獲得報酬集計
--	Progaram ID		: DailyReceivePtCnt
--
--  Creation Date	: 2017.03.03
--  Creater			: M&TT Zukeran
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Status_DailyReceivePtCnt:System.Web.UI.Page {
	private int iDays;

	// 各日にちのデータ設定用
	private int[] aiCount;
	private int[] aiPoint;

	// 総合計のデータ設定用
	private int iTotalCount;
	private int iTotalPoint;

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			this.InitPage();
		}
	}

	private void InitPage() {
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}

		// 表示されてる検索結果の非表示
		grdData.DataSourceID = "";
		pnlInfo.Visible = false;

		// サイトCDの選択肢を設定
		lstSiteCd.DataBind();

		if (!IsPostBack) {
			// 日付リストの選択肢を設定
			SysPrograms.SetupYear(lstYYYY);
			SysPrograms.SetMonthList(lstMM);

			// 検索の度に取得されないようにしておく
			lstSiteCd.DataSourceID = "";
		}

		// 日付リストの初期値設定
		lstYYYY.SelectedIndex = 0;
		lstMM.SelectedValue = DateTime.Now.ToString("MM");

		// 検索結果の合計数を初期化
		this.InitData();
	}

	private void InitData() {
		int i = 0;

		// 表示月の日数を設定(データ表示用)
		this.iDays = DateTime.DaysInMonth(int.Parse(lstYYYY.SelectedItem.Value),int.Parse(lstMM.SelectedItem.Value));

		// 合計欄表示用
		this.aiCount = new int[iDays];
		this.aiPoint = new int[iDays];
		for (i = 0;i < iDays;i++) {
			this.aiCount[i] = 0;
			this.aiPoint[i] = 0;
		}
		this.iTotalCount = 0;
		this.iTotalPoint = 0;
	}

	private void GetList() {
		// 初期化
		this.InitData();

		pnlInfo.Visible = true;
		grdData.DataSourceID = "dsDailyReceivePtCnt";
		grdData.PageIndex = 0;
		grdData.DataBind();
	}

	/// <summary>
	/// 検索ボタン押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (IsValid) {
			this.GetList();
		}
	}

	/// <summary>
	/// クリアボタン押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	/// <summary>
	/// 検索前処理
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void dsDailyReceivePtCnt_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		DailyReceivePtCnt.SearchCondition oSearchConditions = new DailyReceivePtCnt.SearchCondition();
		oSearchConditions.SiteCd = lstSiteCd.SelectedValue;
		oSearchConditions.Year = lstYYYY.SelectedValue;
		oSearchConditions.Month = lstMM.SelectedValue;
		e.InputParameters["pSearchCondition"] = oSearchConditions;
	}

	/// <summary>
	/// データ設定処理
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void grdData_RowDataBound(object sender,GridViewRowEventArgs e) {
		// データが存在しない場合
		if (grdData.Controls.Count <= 0) {
			return;
		}

		// ヘッダー
		if (e.Row.RowType == DataControlRowType.Header) {
			this.CustomHeader(e.Row);
		}
		// データ行
		else if (e.Row.RowType == DataControlRowType.DataRow) {
			this.CustomDataRow(e.Row);
		}
		// フッター
		else if (e.Row.RowType == DataControlRowType.Footer) {
			this.CustomFooter(e.Row);
		}
	}

	/// <summary>
	/// ヘッダー行のカスタマイズ
	/// </summary>
	private void CustomHeader(GridViewRow oHeaderRow) {
		if (oHeaderRow == null) {
			return;
		}
		GridViewRow oHeaderRow1 = new GridViewRow(-1,-1,DataControlRowType.Header,DataControlRowState.Normal);
		GridViewRow oHeaderRow2 = new GridViewRow(-1,-1,DataControlRowType.Header,DataControlRowState.Normal);
		TableCell oCell;

		// 経過期間
		oCell = new TableCell();
		oCell.ColumnSpan = 1;
		oCell.RowSpan = 2;
		oCell.Text = "経過";
		oHeaderRow1.Cells.Add(oCell);

		int i = 0;
		for (i = 1;i <= this.iDays;i++) {
			// 日付
			oCell = new TableCell();
			oCell.ColumnSpan = 2;
			oCell.RowSpan = 1;
			oCell.HorizontalAlign = HorizontalAlign.Center;
			oCell.Text = i.ToString();
			oHeaderRow1.Cells.Add(oCell);
			// 人数、獲得pt
			oCell = new TableCell();
			oCell.ColumnSpan = 1;
			oCell.RowSpan = 1;
			oCell.HorizontalAlign = HorizontalAlign.Center;
			oCell.Text = "人数";
			oHeaderRow2.Cells.Add(oCell);
			oCell = new TableCell();
			oCell.ColumnSpan = 1;
			oCell.RowSpan = 1;
			oCell.HorizontalAlign = HorizontalAlign.Center;
			oCell.Text = "獲得pt";
			oHeaderRow2.Cells.Add(oCell);
		}

		// 合計(経過期間ごと)
		oCell = new TableCell();
		oCell.ColumnSpan = 2;
		oCell.RowSpan = 1;
		oCell.HorizontalAlign = HorizontalAlign.Center;
		oCell.Text = "合計";
		oHeaderRow1.Cells.Add(oCell);
		// 人数、獲得pt
		oCell = new TableCell();
		oCell.ColumnSpan = 1;
		oCell.RowSpan = 1;
		oCell.HorizontalAlign = HorizontalAlign.Center;
		oCell.Text = "人数";
		oHeaderRow2.Cells.Add(oCell);
		oCell = new TableCell();
		oCell.ColumnSpan = 1;
		oCell.RowSpan = 1;
		oCell.HorizontalAlign = HorizontalAlign.Center;
		oCell.Text = "獲得pt";
		oHeaderRow2.Cells.Add(oCell);

		// 元のヘッダー行を削除
		grdData.Controls[0].Controls.Remove(oHeaderRow);
		// カスタマイズしたヘッダー行を挿入
		grdData.Controls[0].Controls.AddAt(0,oHeaderRow1);
		grdData.Controls[0].Controls.AddAt(1,oHeaderRow2);
	}

	/// <summary>
	/// データ行のカスタマイズ
	/// </summary>
	/// <param name="oDataRow"></param>
	private void CustomDataRow(GridViewRow oDataRow) {
		TableCell oCell;
		HyperLink oLink;
		int i = 0;
		string sCnt = string.Empty;
		string sElapsedDays = DataBinder.Eval(oDataRow.DataItem,"ELAPSED_DAYS_DISP_TERM").ToString();

		// 右端のセル(月末のデータ)から追加していく
		for (i = this.iDays;i > 0;i--) {
			// 獲得pt
			oCell = new TableCell();
			oCell.ColumnSpan = 1;
			oCell.RowSpan = 1;
			oCell.HorizontalAlign = HorizontalAlign.Right;
			oCell.Text = DataBinder.Eval(oDataRow.DataItem,"POINT_" + i).ToString();
			this.aiPoint[i - 1] += int.Parse(oCell.Text);
			oDataRow.Cells.AddAt(1,oCell);
			// 人数
			oCell = new TableCell();
			oCell.ColumnSpan = 1;
			oCell.RowSpan = 1;
			oCell.HorizontalAlign = HorizontalAlign.Right;
			sCnt = DataBinder.Eval(oDataRow.DataItem,"COUNT_" + i).ToString();
			if (sCnt.Equals("0")) {
				// 継続人数が0人の場合は詳細ページへのリンクなし
				oCell.Text = sCnt;
			} else {
				// 継続人数が1人以上の場合は詳細ページへのリンクを設定
				oLink = new HyperLink();
				oLink.NavigateUrl = string.Format(
					"~/Status/DailyReceivePtCntDtl.aspx?sitecd={0}&report={1}/{2}/{3:D2}&days={4}"
					,lstSiteCd.SelectedValue
					,lstYYYY.SelectedItem.Value
					,lstMM.SelectedItem.Value
					,i
					,sElapsedDays
				);
				// リンクの幅をセルと同じにする
				oLink.Width = Unit.Percentage(100);
				oLink.Text = sCnt;
				oCell.Controls.Add(oLink);
			}
			this.aiCount[i - 1] += int.Parse(sCnt);
			oDataRow.Cells.AddAt(1,oCell);
		}
	}

	/// <summary>
	/// フッター行のカスタマイズ
	/// </summary>
	/// <param name="oFooterRow"></param>
	private void CustomFooter(GridViewRow oFooterRow) {
		TableCell oCell;
		int i = 0;

		// 右端のセル(月末のデータ)から追加していく
		for (i = this.iDays;i > 0;i--) {
			// 獲得pt
			oCell = new TableCell();
			oCell.ColumnSpan = 1;
			oCell.RowSpan = 1;
			oCell.HorizontalAlign = HorizontalAlign.Right;
			oCell.Text = this.aiPoint[i - 1].ToString();
			oFooterRow.Cells.AddAt(1,oCell);
			// 人数
			oCell = new TableCell();
			oCell.ColumnSpan = 1;
			oCell.RowSpan = 1;
			oCell.HorizontalAlign = HorizontalAlign.Right;
			oCell.Text = this.aiCount[i - 1].ToString();
			oFooterRow.Cells.AddAt(1,oCell);

			// 総合計人数、獲得ptを設定
			this.iTotalPoint += this.aiPoint[i - 1];
			this.iTotalCount += this.aiCount[i - 1];
		}

		i = this.iDays * 2;
		// 総合計人数
		oFooterRow.Cells[i + 1].Text = iTotalCount.ToString();
		// 総合計獲得pt
		oFooterRow.Cells[i + 2].Text = iTotalPoint.ToString();
	}

	/// <summary>
	/// 経過期間の表示文言を取得
	/// </summary>
	/// <param name="sDispTerm"></param>
	/// <returns></returns>
	protected string GetTerm(string sDispTerm) {
		int iDispTerm = int.Parse(sDispTerm);
		int iYear = 0;
		int iMonth = 0;
		int iMonthOfYear = 0;
		string sResult = string.Empty;

		if (iDispTerm >= 30) {
			iMonth = (iDispTerm / 30);
			iYear = (int)decimal.Floor(iMonth / 12);
			iMonthOfYear = iMonth - iYear * 12;

			if (iYear == 0) {
				sResult = string.Format("{0}ヶ月",iMonth);
			} else if (iMonthOfYear == 0) {
				sResult = string.Format("{0}年",iYear);
			} else {
				sResult = string.Format("{0}年{1}ヶ月",iYear,iMonthOfYear);
			}
		} else if (iDispTerm > 6) {
			sResult = string.Format("{0}週間",(iDispTerm/7));
		} else {
			sResult = string.Format("{0}日",iDispTerm);
		}
		return sResult;
	}
}
