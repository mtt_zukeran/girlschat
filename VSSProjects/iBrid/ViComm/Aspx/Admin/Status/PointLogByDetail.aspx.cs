﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ポイントログ(明細)
--	Progaram ID		: PointLogByDetail
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.IO;
using iBridCommLib;
using ViComm;

public partial class Status_PointLogByDetail:System.Web.UI.Page {

	private string recCount = "";
	private int totalMin = 0;
	private int totalPoint = 0;
	private int totalPointFreeDial = 0;
	private int totalInvitePoint = 0;
	private int totalPaymentPoint = 0;
	private Stream filter;

	protected void Page_Load(object sender,EventArgs e) {
		filter = Response.Filter;
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			FisrtLoad();
			InitPage();
		}
	}

	private void FisrtLoad() {
		grdPointDetail.PageSize = int.Parse(Session["PageSize"].ToString());
		grdPointDetail.DataSourceID = "";
		DataBind();
		if (Session["SiteCd"].ToString().Equals("")) {
			lstSiteCd.Items.Insert(0,new ListItem("",""));
		}
		lstSiteCd.DataSourceID = "";
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		lstChargeType.Items.Insert(0,new ListItem("",""));
		lstChargeType.DataSourceID = "";
		lstCallResult.Items.Insert(0,new ListItem("",""));
		lstCallResult.DataSourceID = "";
	}

	private void InitPage() {
		if (!IsPostBack) {
			SysPrograms.SetupTime(lstReportTimeFrom,lstReportTimeTo);
		}
		ClearField();
		if (!IsPostBack) {
			string sSiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
			string sDayFrom = iBridUtil.GetStringValue(Request.QueryString["dayfrom"]);
			string sDayTo = iBridUtil.GetStringValue(Request.QueryString["dayTo"]);
			string sTel = iBridUtil.GetStringValue(Request.QueryString["tel"]);

			if ((!sDayFrom.Equals("")) && (!sDayTo.Equals(""))) {
				lstSiteCd.SelectedValue = sSiteCd;
				txtReportDayFrom.Text = sDayFrom;
				txtReportDayTo.Text = sDayTo;
				txtTel.Text = sTel;
				GetList();
			}
		}

		this.vdcLoginId.ErrorMessage = DisplayWordUtil.Replace(this.vdcLoginId.ErrorMessage);
		DisplayWordUtil.ReplaceGridColumnHeader(this.grdPointDetail);

		using (Site oSite = new Site()) {
			oSite.GetOne(lstSiteCd.SelectedValue);
			lblCharNo.Visible = (oSite.multiCharFlag == 1);
			txtUserCharNo.Visible = (oSite.multiCharFlag == 1);
		}
	}

	private void ClearField() {
		totalMin = 0;
		totalPoint = 0;
		totalPointFreeDial = 0;
		totalInvitePoint = 0;
		totalPaymentPoint = 0;
		txtLoginId.Text = "";
		txtTel.Text = "";
		txtReportDayFrom.Text = DateTime.Now.ToString("yyyy/MM/dd");
		txtReportDayTo.Text = DateTime.Now.ToString("yyyy/MM/dd");
		lstReportTimeFrom.SelectedIndex = 0;
		lstReportTimeTo.SelectedIndex = 0;
		chkPagingOff.Checked = false;
		lnkCastNm.Text = "";
		lnkManNm.Text = string.Empty;
		lstChargeType.SelectedIndex = 0;
		lstCallResult.SelectedIndex = 0;
		rdoTalkSubTypeAll.Checked = true;
		GetList();
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void dsUsedLog_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (txtLoginId.Text.Equals("")) {
			lnkCastNm.Text = "";
			lnkCastNm.NavigateUrl = "";
		}
		if (IsValid) {
			GetList();
		}
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void btnCSV_Click(object sender,EventArgs e) {
		DataSet ds;

		if (txtReportDayTo.Text.Equals("")) {
			txtReportDayTo.Text = txtReportDayFrom.Text;
		}

		using (UsedLog oUsedLog = new UsedLog()) {
			ds = oUsedLog.GetPageCollection(
				lstSiteCd.SelectedValue,
				ViCommConst.OPERATOR,
				txtLoginId.Text,
				txtUserCharNo.Text,
				txtManLoginId.Text,
				ViCommConst.MAIN_CHAR_NO,
				txtTel.Text,
				txtReportDayFrom.Text,
				txtReportDayTo.Text,
				lstChargeType.SelectedValue,
				lstReportTimeFrom.SelectedValue,
				lstReportTimeTo.SelectedValue,
				lstCallResult.SelectedValue,
				GetTalkSubType(),
				0,
				Int32.MaxValue
			);
		}

		Response.Filter = filter;
		Response.ContentType = "application/download";
		Response.AddHeader("Content-Disposition","attachment;filename=pointLogByDetail.csv");
		Response.ContentEncoding = Encoding.GetEncoding("Shift_JIS");

		Response.Write("\"通話開始時間\",\"通話終了時間\",\"会員ID\",\"出演者ID\",\"出演者ﾊﾝﾄﾞﾙ名\",\"発信者TEL\",\"種別\",\"秒数\",\"通話終了理由\",\"分数\",\"Pt\",\"Free\",\"招待Pt\",\"報酬Pt\"\r\n");

		foreach (DataRow dr in ds.Tables[0].Rows) {
			string sData = string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\"\r\n",
				iBridUtil.GetStringValue(dr["CHARGE_START_DATE"]),
				iBridUtil.GetStringValue(dr["CHARGE_END_DATE"]),
				iBridUtil.GetStringValue(dr["MAN_LOGIN_ID"]),
				iBridUtil.GetStringValue(dr["CAST_LOGIN_ID"]),
				iBridUtil.GetStringValue(dr["HANDLE_NM"]),
				string.Empty,
				iBridUtil.GetStringValue(dr["CHARGE_TYPE_NM"]),
				iBridUtil.GetStringValue(dr["CHARGE_SEC"]),
				iBridUtil.GetStringValue(dr["CALL_RESULT_NM"]),
				iBridUtil.GetStringValue(dr["CHARGE_MIN"]),
				iBridUtil.GetStringValue(dr["CHARGE_POINT"]),
				iBridUtil.GetStringValue(dr["CHARGE_POINT_FREE_DIAL"]),
				iBridUtil.GetStringValue(dr["INVITE_TALK_SERVICE_POINT"]),
				iBridUtil.GetStringValue(dr["PAYMENT_POINT"])
			);

			Response.Write(sData);
		}

		Response.End();
	}

	protected void vdcLoginId_ServerValidate(object source,ServerValidateEventArgs args) {
		using (Cast oCast = new Cast()) {
			args.IsValid = oCast.IsExistLoginId(txtLoginId.Text);
			if (args.IsValid) {
				lnkCastNm.Text = oCast.castNm;
				lnkCastNm.NavigateUrl = string.Format("~/Cast/CastView.aspx?loginid={0}&return=PointLogByDetail.aspx",txtLoginId.Text);
			} else {
				lnkCastNm.Text = "";
				lnkCastNm.NavigateUrl = "";
			}
		}
	}

	protected void vdcManLoginId_ServerValidate(object source,ServerValidateEventArgs args) {
		using (UserManCharacter oMan = new UserManCharacter()) {
			args.IsValid = oMan.IsExistLoginId(this.lstSiteCd.SelectedValue,this.txtManLoginId.Text.Trim(),ViCommConst.MAIN_CHAR_NO);
			if (args.IsValid) {
				lnkManNm.Text = oMan.handleNm;
				lnkManNm.NavigateUrl = string.Format("~/Man/ManView.aspx?&site={0}&manloginid={1}",this.lstSiteCd.SelectedValue,this.txtManLoginId.Text);
			} else {
				lnkManNm.Text = string.Empty;
				lnkManNm.NavigateUrl = string.Empty;
			}
		}
	}

	private void GetList() {
		if (txtReportDayTo.Text.Equals("")) {
			txtReportDayTo.Text = txtReportDayFrom.Text;
		}

		using (UsedLog oUsedLog = new UsedLog()) {
			oUsedLog.GetTotal(
					lstSiteCd.SelectedValue,
					ViCommConst.OPERATOR,
					txtLoginId.Text,
					txtUserCharNo.Text,
					txtManLoginId.Text,
					ViCommConst.MAIN_CHAR_NO,
					txtTel.Text,
					txtReportDayFrom.Text,
					txtReportDayTo.Text,
					lstChargeType.SelectedValue,
					lstReportTimeFrom.SelectedValue,
					lstReportTimeTo.SelectedValue,
					lstCallResult.SelectedValue,
					GetTalkSubType(),
					out totalMin,
					out totalPoint,
					out totalPointFreeDial,
					out totalInvitePoint,
					out totalPaymentPoint);
		}
		if (chkPagingOff.Checked) {
			grdPointDetail.PageSize = 999999;
			grdPointDetail.AllowSorting = true;
		} else {
			grdPointDetail.PageSize = 200;
			grdPointDetail.AllowSorting = false;
		}

		if (chkViewCallResult.Checked) {
			grdPointDetail.Columns[8].Visible = true;
			//			grdPointDetail.Width = 1320;
		} else {
			grdPointDetail.Columns[8].Visible = false;
			//			grdPointDetail.Width = 990;
		}

		grdPointDetail.DataSourceID = "dsUsedLog";
		grdPointDetail.DataBind();
		pnlCount.DataBind();
		pnlCount2.DataBind();
	}

	protected string GetTotalMin() {
		return totalMin.ToString();
	}

	protected string GetTotalPoint() {
		return totalPoint.ToString();
	}

	protected string GetTotalPointFreeDial() {
		return totalPointFreeDial.ToString();
	}

	protected string GetTotalInvitePoint() {
		return totalInvitePoint.ToString();
	}

	protected string GetTotalPaymentPoint() {
		return totalPaymentPoint.ToString();
	}

	protected void dsUsedLog_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pSiteCd"] = lstSiteCd.SelectedValue;
		e.InputParameters["pSexCd"] = ViCommConst.OPERATOR;
		e.InputParameters["pLoginId"] = txtLoginId.Text;
		e.InputParameters["pUserCharNo"] = txtUserCharNo.Text;
		e.InputParameters["pPartnerLoginId"] = txtManLoginId.Text;
		e.InputParameters["pPartnerUserCharNo"] = ViCommConst.MAIN_CHAR_NO;
		e.InputParameters["pTel"] = txtTel.Text;
		e.InputParameters["pReportDayFrom"] = txtReportDayFrom.Text;

		if (txtReportDayTo.Text.Equals("")) {
			txtReportDayTo.Text = txtReportDayFrom.Text;
		}

		e.InputParameters["pReportDayTo"] = txtReportDayTo.Text;
		e.InputParameters["pChargeType"] = lstChargeType.SelectedValue;
		e.InputParameters["pReportTimeFrom"] = lstReportTimeFrom.SelectedValue;
		e.InputParameters["pReportTimeTo"] = lstReportTimeTo.SelectedValue;
		e.InputParameters["pCallResult"] = lstCallResult.SelectedValue;
		e.InputParameters["pTalkSubType"] = GetTalkSubType();
	}

	protected bool UseOtherSysInfoFlag(object pUseOtherSysInfoFlag) {
		if (pUseOtherSysInfoFlag.ToString().Equals("1")) {
			return true;
		} else {
			return false;
		}
	}

	protected string GetSipUserNm(object pUri) {
		return Regex.Replace(pUri.ToString(),"@.*","",RegexOptions.Compiled);
	}

	private int GetTalkSubType() {
		int iTalkSubType = ViCommConst.TALK_SUB_TYPE_ALL;
		if (rdoTalkSubTypeNormal.Checked) {
			iTalkSubType = ViCommConst.TALK_SUB_TYPE_NORMAL;
		} else if (rdoTalkSubTypeInvite.Checked) {
			iTalkSubType = ViCommConst.TALK_SUB_TYPE_INVITE;
		} else if (rdoTalkSubTypeMeeting.Checked) {
			iTalkSubType = ViCommConst.TALK_SUB_TYPE_MEETING;
		}
		return iTalkSubType;
	}
}
