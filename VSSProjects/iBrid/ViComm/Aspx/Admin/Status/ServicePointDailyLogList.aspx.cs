﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 出演者つぶやき

--	Progaram ID		: ServicePointDailyLogList
--
--  Creation Date	: 2015.05.12
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using System.Collections.Generic;

public partial class Status_ServicePointDailyLogList:System.Web.UI.Page {
	private string recCount = string.Empty;

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.InitPage();

			this.pnlInfo.Visible = true;
			this.GetList();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		this.pnlInfo.Visible = true;
		this.GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void dsServicePointDailyLog_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void grdServicePointDailyLog_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
		}
	}

	protected void dsServicePointDailyLog_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		ServicePointDailyLog.SearchCondition oSearchCondition = new ServicePointDailyLog.SearchCondition();
		oSearchCondition.ReportDayFrom = string.Format("{0}/{1}/{2}",this.lstFromYYYY.SelectedValue,this.lstFromMM.SelectedValue,this.lstFromDD.SelectedValue);
		string sToDD;
		if (this.lstToDD.SelectedIndex == 0) {
			sToDD = DateTime.DaysInMonth(int.Parse(this.lstToYYYY.SelectedValue),int.Parse(this.lstToMM.SelectedValue)).ToString();
		} else {
			sToDD = this.lstToDD.SelectedValue;
		}
		oSearchCondition.ReportDayTo = string.Format("{0}/{1}/{2}",this.lstToYYYY.SelectedValue,this.lstToMM.SelectedValue,sToDD);
		oSearchCondition.SiteCd = this.lstSiteCd.SelectedValue;
		e.InputParameters[0] = oSearchCondition;
	}

	protected void vdcFromTo_ServerValidate(object source,ServerValidateEventArgs e) {
		if (this.IsValid) {
			if (e.IsValid) {
				DateTime dtFrom;
				DateTime dtTo;
				if (!DateTime.TryParse(string.Format("{0}/{1}/{2}",this.lstFromYYYY.SelectedValue,this.lstFromMM.SelectedValue,this.lstFromDD.SelectedValue),out dtFrom)) {
					this.vdcFromTo.Text = "開始日に正しい日付を設定してください。";
					e.IsValid = false;
					return;
				}
				string sToDD;
				if (this.lstToDD.SelectedIndex == 0) {
					sToDD = DateTime.DaysInMonth(int.Parse(this.lstToYYYY.SelectedValue),int.Parse(this.lstToMM.SelectedValue)).ToString();
				} else {
					sToDD = this.lstToDD.SelectedValue;
				}
				if (!DateTime.TryParse(string.Format("{0}/{1}/{2}",this.lstToYYYY.SelectedValue,this.lstToMM.SelectedValue,sToDD),out dtTo)) {
					this.vdcFromTo.Text = "終了日に正しい日付を設定してください。";
					e.IsValid = false;
					return;
				}
				if (dtFrom > dtTo) {
					this.vdcFromTo.Text = "期間の大小関係に誤りがあります。";
					e.IsValid = false;
				}
			}
		}
	}

	private void InitPage() {
		if (!this.IsPostBack) {
			SysPrograms.SetupFromToDay(this.lstFromYYYY,this.lstFromMM,this.lstFromDD,this.lstToYYYY,this.lstToMM,this.lstToDD,false);
			this.lstFromMM.Items.Insert(0,new ListItem("--","01"));
			this.lstToMM.Items.Insert(0,new ListItem("--","12"));
			this.lstFromDD.Items.Insert(0,new ListItem("--","01"));
			this.lstToDD.Items.Insert(0,new ListItem("--","31"));
		}

		this.recCount = "0";
		this.grdServicePointDailyLog.DataSourceID = string.Empty;

		this.lstFromYYYY.SelectedIndex = 0;
		this.lstToYYYY.SelectedIndex = 0;
		this.lstFromMM.SelectedValue = DateTime.Now.ToString("MM");
		this.lstToMM.SelectedValue = DateTime.Now.ToString("MM");
		this.lstFromDD.SelectedValue = DateTime.Now.ToString("dd");
		this.lstToDD.SelectedValue = DateTime.Now.ToString("dd");

		this.pnlInfo.Visible = false;
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	private void GetList() {
		this.grdServicePointDailyLog.PageIndex = 0;
		this.grdServicePointDailyLog.PageSize = 100;
		this.grdServicePointDailyLog.DataSourceID = "dsServicePointDailyLog";
		this.grdServicePointDailyLog.DataBind();
		this.pnlCount.DataBind();
	}

	protected void btnCSV_Click(object sender,EventArgs e) {
		string fileName = string.Format("SERVICE_POINT_DAILY_LOG_{0}.csv",lstSiteCd.SelectedValue);


		Response.AddHeader("Content-Disposition","attachment;filename=" + fileName);
		Response.ContentType = "application/octet-stream-dummy";
		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		using (ServicePointDailyLog oServicePointDailyLog = new ServicePointDailyLog()) {
			ServicePointDailyLog.SearchCondition oSearchCondition = new ServicePointDailyLog.SearchCondition();
			oSearchCondition.ReportDayFrom = string.Format("{0}/{1}/{2}",this.lstFromYYYY.SelectedValue,this.lstFromMM.SelectedValue,this.lstFromDD.SelectedValue);
			string sToDD;
			if (this.lstToDD.SelectedIndex == 0) {
				sToDD = DateTime.DaysInMonth(int.Parse(this.lstToYYYY.SelectedValue),int.Parse(this.lstToMM.SelectedValue)).ToString();
			} else {
				sToDD = this.lstToDD.SelectedValue;
			}
			oSearchCondition.ReportDayTo = string.Format("{0}/{1}/{2}",this.lstToYYYY.SelectedValue,this.lstToMM.SelectedValue,sToDD);
			oSearchCondition.SiteCd = this.lstSiteCd.SelectedValue;
			
			DataSet ds = oServicePointDailyLog.GetPageCollection(
								oSearchCondition,
								0,
								SysConst.DB_MAX_ROWS
							);

			string sCsvRec = GetCsvString(ds);

			Response.BinaryWrite(encoding.GetBytes(sCsvRec));
			Response.End();
		}
	}

	private string GetCsvString(DataSet pDataSet) {
		string sCsv = "";

		sCsv += "報告日,";
		sCsv += "付与ﾎﾟｲﾝﾄ,";
		sCsv += "失効ﾎﾟｲﾝﾄ";
		sCsv += "\r\n";

		DataRow dr;

		for (int i = 0;i < pDataSet.Tables[0].Rows.Count;i++) {
			dr = pDataSet.Tables[0].Rows[i];

			sCsv += dr["REPORT_DAY"] + ",";
			sCsv += dr["ADD_SERVICE_POINT"] + ",";
			sCsv += dr["LOST_SERVICE_POINT"];
			sCsv += "\r\n";
		}
		return DisplayWordUtil.Replace(sCsv);
	}
}
