<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="FriendIntroLogInquiry.aspx.cs" Inherits="Status_FriendIntroLogInquiry" Title="友達紹介ポイント追加記録"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="友達紹介ポイント追加記録"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 740px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイト
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="180px">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderStyle2">
							追加日
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtCreateDayFrom" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:RequiredFieldValidator ID="vdrCreateDayFrom" runat="server" ErrorMessage="追加日Fromを入力して下さい。" ControlToValidate="txtCreateDayFrom" ValidationGroup="Key"
								Display="Dynamic">*</asp:RequiredFieldValidator>
							<asp:RangeValidator ID="vdeCreateDayFrom" runat="server" ErrorMessage="追加日Fromを正しく入力して下さい。" ControlToValidate="txtCreateDayFrom" MaximumValue="2099/12/31"
								MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							〜&nbsp;
							<asp:TextBox ID="txtCreateDayTo" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:RangeValidator ID="vdeCreateDayTo" runat="server" ErrorMessage="追加日Toを正しく入力して下さい。" ControlToValidate="txtCreateDayTo" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
								Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							ユーザーＩＤ
						</td>
						<td class="tdDataStyle2">
							<asp:TextBox ID="txtLoginId" runat="server" Width="70px"></asp:TextBox>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" ValidationGroup="Key" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
			</asp:Panel>
		</fieldset>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset>
				<legend>[ﾎﾟｲﾝﾄ追加記録]</legend>
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="430px">
					<asp:GridView ID="grdFriendIntroLog" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsFriendIntroLog" AllowSorting="True" SkinID="GridViewColor">
						<Columns>
							<asp:BoundField DataField="CREATE_DATE" HeaderText="追加日時" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False" >
								<ItemStyle HorizontalAlign="Center" />
								<ItemStyle Font-Size="Small" />
							</asp:BoundField>
							<asp:TemplateField HeaderText="紹介したﾕｰｻﾞｰID">
								<ItemTemplate>
									<asp:HyperLink ID="lnkLoginId" runat="server" NavigateUrl='<%# GetUserLinkByFriendIntroType(Eval("SITE_CD"),Eval("LOGIN_ID"),Eval("SEX_CD"),Eval("PARTNER_SITE_CD"),Eval("PARTNER_LOGIN_ID"),Eval("PARTNER_SEX_CD"),Eval("FRIEND_INTRO_TYPE")) %>'
										Text='<%# GetUserLoginIdByFriendIntroType(Eval("LOGIN_ID"),Eval("PARTNER_LOGIN_ID"),Eval("FRIEND_INTRO_TYPE")) %>'></asp:HyperLink>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="紹介されたﾕｰｻﾞｰID">
								<ItemTemplate>
									<asp:HyperLink ID="lnkPartnerLoginId" runat="server" NavigateUrl='<%# GetUserLinkByFriendIntroType(Eval("PARTNER_SITE_CD"),Eval("PARTNER_LOGIN_ID"),Eval("PARTNER_SEX_CD"),Eval("SITE_CD"),Eval("LOGIN_ID"),Eval("SEX_CD"),Eval("FRIEND_INTRO_TYPE")) %>'
										Text='<%# GetUserLoginIdByFriendIntroType(Eval("PARTNER_LOGIN_ID"),Eval("LOGIN_ID"),Eval("FRIEND_INTRO_TYPE")) %>'></asp:HyperLink>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField DataField="FRIEND_INTRO_TYPE_NM" HeaderText="追加種別">
								<ItemStyle HorizontalAlign="Center" />
							</asp:BoundField>
							<asp:BoundField DataField="FRIEND_INTRO_POINT" HeaderText="追加P">
								<ItemStyle HorizontalAlign="Right" />
							</asp:BoundField>
							<asp:BoundField DataField="BEFORE_FRIEND_INTRO_POINT" HeaderText="追加前P">
								<ItemStyle HorizontalAlign="Right" />
							</asp:BoundField>
							<asp:BoundField DataField="AFTER_FRIEND_INTRO_POINT" HeaderText="追加後">
								<ItemStyle HorizontalAlign="Right" />
							</asp:BoundField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
				&nbsp;
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">Record Count of
						<%#GetRecCount() %>
					</a>
					<br>
					<a class="reccount">Current viewing page
						<%=grdFriendIntroLog.PageIndex + 1%>
						of
						<%=grdFriendIntroLog.PageCount%>
					</a>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsFriendIntroLog" runat="server" SelectMethod="GetPageCollection" TypeName="FriendIntroLog" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelecting="dsFriendIntroLog_Selecting" OnSelected="dsFriendIntroLog_Selected">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
			<asp:Parameter Name="pCreateDayFrom" Type="String" />
			<asp:Parameter Name="pCreateDayTo" Type="String" />
			<asp:Parameter Name="pFriendIntroType" Type="String" />
			<asp:Parameter Name="pLoginId" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrCreateDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdeCreateDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdeCreateDayTo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:MaskedEditExtender ID="mskCreateDayFrom" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="True"
		TargetControlID="txtCreateDayFrom">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskCreateDayTo" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="True"
		TargetControlID="txtCreateDayTo">
	</ajaxToolkit:MaskedEditExtender>
</asp:Content>
