<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CastTalkMovieDownload.aspx.cs" Inherits="Status_CastTalkMovieDownload" Title="出演者別ムービーアクセス集計" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="出演者別ムービーアクセス集計"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 640px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイト
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							<asp:Label ID="lblAccessUnit" runat="server" Text="集計開始日"></asp:Label>
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstFromYYYY" runat="server" Width="60px">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstFromMM" runat="server" Width="40px">
							</asp:DropDownList>月
							<asp:DropDownList ID="lstFromDD" runat="server" Width="40px">
							</asp:DropDownList>日
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							<asp:Label ID="Label3" runat="server" Text="集計終了日"></asp:Label>
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstToYYYY" runat="server" Width="60px">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstToMM" runat="server" Width="40px">
							</asp:DropDownList>月
							<asp:DropDownList ID="lstToDD" runat="server" Width="40px">
							</asp:DropDownList>日
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							<%= DisplayWordUtil.Replace("出演者ＩＤ") %>
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtLoginId" runat="server"></asp:TextBox>
							<asp:HyperLink ID="lnkCastNm" runat="server"></asp:HyperLink>
							<asp:RequiredFieldValidator ID="vdrLoginId" runat="server" ErrorMessage="出演者ＩＤを入力して下さい。" ControlToValidate="txtLoginId" ValidationGroup="Key">*</asp:RequiredFieldValidator>
							<asp:CustomValidator ID="vdcLoginId" runat="server" ControlToValidate="txtLoginId" ErrorMessage="この出演者ＩＤは存在しません。" OnServerValidate="vdcLoginId_ServerValidate" ValidationGroup="Key">*</asp:CustomValidator>
							<asp:Label ID="lblUserSeq" runat="server" Text="" Visible="false"></asp:Label>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" ValidationGroup="Key" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
				<asp:Button runat="server" ID="btnCsv" Text="ＣＳＶ出力" CssClass="seekbutton" OnClick="btnCSV_Click" />
			</asp:Panel>
		</fieldset>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset>
				<legend>[アクセス集計]</legend>
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="540px">
					<asp:GridView ID="grdDailyDownload" runat="server" AutoGenerateColumns="False" DataSourceID="dsAccessTalkMovie" AllowSorting="True" SkinID="GridViewColor" Width="600px">
						<Columns>
							<asp:BoundField DataField="LOGIN_ID" HeaderText="出演者ID" SortExpression="LOGIN_ID">
								<ItemStyle HorizontalAlign="Center" />
							</asp:BoundField>
							<asp:BoundField DataField="MOVIE_TITLE" HeaderText="動画タイトル" SortExpression="MOVIE_TITLE"></asp:BoundField>
							<asp:BoundField DataField="ACCESS_COUNT" HeaderText="アクセス数" SortExpression="ACCESS_COUNT">
								<ItemStyle HorizontalAlign="Right" />
							</asp:BoundField>
							<asp:BoundField DataField="TOTAL_USED_POINT" HeaderText="利用Pt" SortExpression="TOTAL_USED_POINT">
								<ItemStyle HorizontalAlign="Right" />
							</asp:BoundField>
							<asp:BoundField DataField="TOTAL_USED_SEC" HeaderText="利用秒数" SortExpression="TOTAL_USED_SEC">
								<ItemStyle HorizontalAlign="Right" />
							</asp:BoundField>
						</Columns>
					</asp:GridView>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsAccessTalkMovie" runat="server" SelectMethod="GetListByCast" TypeName="AccessTalkMovie">
		<SelectParameters>
			<asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstFromYYYY" Name="pFromYYYY" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstFromMM" Name="pFromMM" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstFromDD" Name="pFromDD" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstToYYYY" Name="pToYYYY" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstToMM" Name="pToMM" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstToDD" Name="pToDD" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lblUserSeq" DefaultValue="" Name="pUserSeq" PropertyName="Text" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="vdrLoginId" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdcLoginId" HighlightCssClass="validatorCallout" />
</asp:Content>
