﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: アクセス日別メールからのアクセス集計
--	Progaram ID		: AccessMailCountDailyList
--
--  Creation Date	: 2015.06.03
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.IO;
using iBridCommLib;
using ViComm;

public partial class Status_AccessMailCountDailyList:System.Web.UI.Page {
	private string[] Week = { "日","月","火","水","木","金","土" };

	private double dAccessCount = 0;
	private double dUniqueAccessCount = 0;
	private double dTxMailCount = 0;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SITE_CD"]);
		}
		set {
			this.ViewState["SITE_CD"] = value;
		}
	}

	private string MailTemplateNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["MAIL_TEMPLATE_NO"]);
		}
		set {
			this.ViewState["MAIL_TEMPLATE_NO"] = value;
		}
	}

	private string SexCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SEX_CD"]);
		}
		set {
			this.ViewState["SEX_CD"] = value;
		}
	}

	private string TxMailDay {
		get {
			return iBridUtil.GetStringValue(this.ViewState["TX_MAIL_DAY"]);
		}
		set {
			this.ViewState["TX_MAIL_DAY"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.SiteCd = iBridUtil.GetStringValue(Request.QueryString["site"]);
			this.MailTemplateNo = iBridUtil.GetStringValue(Request.QueryString["mailtemplateno"]);
			this.SexCd = iBridUtil.GetStringValue(Request.QueryString["sexcd"]);
			this.TxMailDay = iBridUtil.GetStringValue(Request.QueryString["txmailday"]);
			
			lblTxMailDay.Text = this.TxMailDay;
			
			using (TxMailCountDaily oTxMailCountDaily = new TxMailCountDaily()) {
				DataSet oDataSet = oTxMailCountDaily.GetOne(this.SiteCd,this.MailTemplateNo,this.SexCd,this.TxMailDay);
				
				if (oDataSet.Tables[0].Rows.Count > 0) {
					lblTxMailCount.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["TX_MAIL_COUNT"]);
					this.dTxMailCount = double.Parse(lblTxMailCount.Text);
				}
			}
		}
		
		GetList();
	}

	private void InitPage() {
	}

	protected void grdAccessMailCountDaily_RowDataBound(object sender,GridViewRowEventArgs e) {

		if (e.Row.RowType == DataControlRowType.DataRow) {
			DataRowView drv = (DataRowView)e.Row.DataItem;

			if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(drv["ACCESS_COUNT"]))) {
				dAccessCount = dAccessCount + int.Parse(iBridUtil.GetStringValue(drv["ACCESS_COUNT"]));
				dUniqueAccessCount = dUniqueAccessCount + int.Parse(iBridUtil.GetStringValue(drv["UNIQUE_ACCESS_COUNT"]));
			}

			e.Row.Cells[3].Text = GetUniqueAccessRate();
		} else if (e.Row.RowType == DataControlRowType.Footer) {
			Label lblAccessCountFooter = (Label)e.Row.FindControl("lblAccessCountFooter");
			Label lblUniqueAccessCountFooter = (Label)e.Row.FindControl("lblUniqueAccessCountFooter");
			Label lblUniqueAccessRateFooter = (Label)e.Row.FindControl("lblUniqueAccessRateFooter");
			
			lblAccessCountFooter.Text = this.dAccessCount.ToString();
			lblUniqueAccessCountFooter.Text = this.dUniqueAccessCount.ToString();

			if (this.dTxMailCount > 0) {
				double dUniqueAccessRate;
				dUniqueAccessRate = this.dUniqueAccessCount / this.dTxMailCount * 100;
				dUniqueAccessRate = Math.Round(dUniqueAccessRate,2);
				lblUniqueAccessRateFooter.Text = string.Format("{0}%",dUniqueAccessRate);
			} else {
				lblUniqueAccessRateFooter.Text = "-";
			}
		}
	}

	protected void dsAccessMailCountDaily_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = this.SiteCd;
		e.InputParameters[1] = this.MailTemplateNo;
		e.InputParameters[2] = this.SexCd;
		e.InputParameters[3] = this.TxMailDay;
	}

	private void GetList() {

		grdAccessMailCountDaily.PageIndex = 0;
		grdAccessMailCountDaily.DataSourceID = "dsAccessMailCountDaily";
		DataBind();
	}

	protected Color GetBackColor(object pDayOfWeek) {
		if (pDayOfWeek.ToString().Equals("Sat")) {
			return Color.FromArgb(0xCCCCFF);
		} else {
			if (pDayOfWeek.ToString().Equals("Sun")) {
				return Color.FromArgb(0xFFCCCC);
			} else {
				return Color.Empty;
			}
		}
	}

	private bool IsCorrectDate() {
		return true;
	}

	protected string GetDisplayReportDay(string pReportDay) {
		DateTime dtReportDay = DateTime.Parse(pReportDay);
		return dtReportDay.Day.ToString();
	}

	protected string GetDisplayDayOfWeek(string pReportDay) {
		DateTime dtReportDay = DateTime.Parse(pReportDay);
		int iDayOfWeekNo = (int)dtReportDay.DayOfWeek;

		return Week[iDayOfWeekNo];
	}

	protected string GetUniqueAccessRate() {
		string sValue = "-";

		if (this.dTxMailCount > 0) {
			double dValue = dUniqueAccessCount / dTxMailCount * 100;

			dValue = Math.Round(dValue,2);
			sValue = string.Format("{0}%",dValue.ToString());
		}

		return sValue;
	}
}
