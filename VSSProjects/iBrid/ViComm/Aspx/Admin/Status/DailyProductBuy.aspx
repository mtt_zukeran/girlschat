﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="DailyProductBuy.aspx.cs" Inherits="Status_DailyProductBuy" Title="期間別商品購入集計"
    ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="期間別商品購入集計"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <fieldset class="fieldset">
            <legend>[検索条件]</legend>
            <asp:Panel runat="server" ID="pnlKey">
                <table border="0" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle">
                            サイトコード
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstSiteCd" runat="server" Width="240px" DataSourceID="dsSite"
                                DataTextField="SITE_NM" DataValueField="SITE_CD">
                            </asp:DropDownList>
                        </td>
                        <td class="tdHeaderStyle">
                            ｺﾝﾃﾝﾂ ﾌﾟﾛﾊﾞｲﾀﾞ
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstProductAgent" runat="server" Width="240px" DataSourceID="dsProductAgent"
                                DataTextField="PRODUCT_AGENT_NM" DataValueField="PRODUCT_AGENT_CD" OnDataBound="lst_DataBound">
                            </asp:DropDownList>
                            <% // 必須チェック %>
                            <%--<asp:RequiredFieldValidator ID="vdrProductAgent" runat="server" ErrorMessage="ｺﾝﾃﾝﾂ ﾌﾟﾛﾊﾞｲﾀﾞを選択してください。"
                                ControlToValidate="lstProductAgent" ValidationGroup="Create" Display="Dynamic">*</asp:RequiredFieldValidator>
                            <ajaxToolkit:ValidatorCalloutExtender ID="vceProductAgent1" runat="Server" TargetControlID="vdrProductAgent"
                                HighlightCssClass="validatorCallout" />--%>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            商品種別
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstProductType" runat="server" Width="180px" DataSourceID="dsProductType"
                                DataTextField="CODE_NM" DataValueField="CODE">
                                <asp:ListItem Text="" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <% // 必須チェック %>
                            <asp:RequiredFieldValidator ID="vdrProductType" runat="server" ErrorMessage="商品種別を選択してください。"
                                ControlToValidate="lstProductType" ValidationGroup="Seek" Display="Dynamic">*</asp:RequiredFieldValidator>
                            <ajaxToolkit:ValidatorCalloutExtender ID="vceProductType1" runat="Server" TargetControlID="vdrProductType"
                                HighlightCssClass="validatorCallout" />
                        </td>
                        <td class="tdHeaderStyle2">
                            製造元
                        </td>
                        <td class="tdDataStyle">
							<asp:TextBox ID="txtProductMakerSummary" runat="server" MaxLength="64" Width="200px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            <asp:Label ID="lblAccessUnit" runat="server" Text="集計開始日"></asp:Label>
                        </td>
                        <td class="tdDataStyle" colspan="3">
                            <asp:DropDownList ID="lstFromYYYY" runat="server" Width="60px">
                            </asp:DropDownList>年
                            <asp:DropDownList ID="lstFromMM" runat="server" Width="40px">
                            </asp:DropDownList>月
                            <asp:DropDownList ID="lstFromDD" runat="server" Width="40px">
                            </asp:DropDownList>日
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle2">
                            <asp:Label ID="Label3" runat="server" Text="集計終了日"></asp:Label>
                        </td>
                        <td class="tdDataStyle" colspan="3">
                            <asp:DropDownList ID="lstToYYYY" runat="server" Width="60px">
                            </asp:DropDownList>年
                            <asp:DropDownList ID="lstToMM" runat="server" Width="40px">
                            </asp:DropDownList>月
                            <asp:DropDownList ID="lstToDD" runat="server" Width="40px">
                            </asp:DropDownList>日
                        </td>
                    </tr>
                </table>
                <asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Seek" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
                <asp:Button runat="server" ID="btnCsv" Text="ＣＳＶ出力" CssClass="seekbutton" OnClick="btnCSV_Click" />
            </asp:Panel>
        </fieldset>
        <asp:Panel ID="pnlInfo" runat="server">
            <fieldset>
                <legend>[購入集計]</legend>
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto">
                    <asp:GridView ID="grdDailyProductBuy" runat="server" AutoGenerateColumns="False"
                        DataSourceID="dsProductHistory" AllowPaging="true" AllowSorting="True" SkinID="GridViewColor"
                        Width="850px">
                        <Columns>
                            <asp:TemplateField HeaderText="商品ID" SortExpression="PRODUCT_ID">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkProductMainte" runat="server" Text='<%# Eval("PRODUCT_ID") %>'
                                        NavigateUrl='<%# GenerateProductMainteUrl(Container.DataItem) %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="PRODUCT_NM" HeaderText="商品名" SortExpression="PRODUCT_NM">
                                <FooterStyle HorizontalAlign="Center" />
                                <ItemStyle Width="300px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="PRODUCT_MAKER_SUMMARY" HeaderText="製造元" SortExpression="PRODUCT_MAKER_SUMMARY">
                                <FooterStyle HorizontalAlign="Center" />
                                <ItemStyle Width="100px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="BUY_COUNT" HeaderText="購入数" SortExpression="BUY_COUNT">
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="TOTAL_POINT" HeaderText="ポイント計" SortExpression="TOTAL_POINT">
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ROYALTY_POINT" HeaderText="ロイヤリティ計" SortExpression="ROYALTY_POINT">
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="購入詳細">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkProductBuy" runat="server" Text="詳細表示" Width="90px" NavigateUrl='<%# GenerateProductBuyUrl(Container.DataItem) %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count of
                        <%#GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%=grdDailyProductBuy.PageIndex + 1%>
                        of
                        <%=grdDailyProductBuy.PageCount%>
                    </a>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <br />
    </div>
    <% // *****************************************		%>
    <% //  DataSource									%>
    <% // *****************************************		%>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsProductAgent" runat="server" SelectMethod="GetList" TypeName="ProductAgent">
        <SelectParameters>
            <asp:Parameter Name="pOuterAgentFlag" Type="object" DefaultValue="null" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsProductType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
        <SelectParameters>
            <asp:Parameter DefaultValue="25" Name="pCodeType" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsProductHistory" runat="server" SelectMethod="GetPageCollection"
        SelectCountMethod="GetPageCount" EnablePaging="true" TypeName="ProductHistory"
        OnSelected="dsProductHistory_Selected">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue"
                Type="String" />
            <asp:ControlParameter ControlID="lstProductAgent" Name="pProductAgentCd" PropertyName="SelectedValue"
                Type="String" />
            <asp:ControlParameter ControlID="lstProductType" Name="pProductType" PropertyName="SelectedValue"
                Type="String" />
            <asp:ControlParameter ControlID="lstFromYYYY" Name="pFromYYYY" PropertyName="SelectedValue"
                Type="String" />
            <asp:ControlParameter ControlID="lstFromMM" Name="pFromMM" PropertyName="SelectedValue"
                Type="String" />
            <asp:ControlParameter ControlID="lstFromDD" Name="pFromDD" PropertyName="SelectedValue"
                Type="String" />
            <asp:ControlParameter ControlID="lstToYYYY" Name="pToYYYY" PropertyName="SelectedValue"
                Type="String" />
            <asp:ControlParameter ControlID="lstToMM" Name="pToMM" PropertyName="SelectedValue"
                Type="String" />
            <asp:ControlParameter ControlID="lstToDD" Name="pToDD" PropertyName="SelectedValue"
                Type="String" />
            <asp:ControlParameter ControlID="txtProductMakerSummary" Name="pProductMakerSumary" PropertyName="Text"
				Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
