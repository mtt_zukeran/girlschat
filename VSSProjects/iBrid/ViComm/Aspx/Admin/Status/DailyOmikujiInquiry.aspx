﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" 
    CodeFile="DailyOmikujiInquiry.aspx.cs" Inherits="Status_DailyOmikujiInquiry" Title="日別おみくじ利用集計"
    ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="日別おみくじ利用集計"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <fieldset class="fieldset">
            <legend>[検索条件]</legend>
            <fieldset class="fieldset-inner">
                <table border="0" style="width: 800px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle">
                            サイトコード
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                DataValueField="SITE_CD" Width="180px">
                            </asp:DropDownList>
                        </td>
                    </tr>
					<tr>
						<td class="tdHeaderStyle2">
							<asp:Label ID="lblReportUnit" runat="server" Text="報告年月"></asp:Label>
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstYYYY" runat="server" Width="54px">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstMM" runat="server" Width="43px">
								<asp:ListItem Value="">--</asp:ListItem>
								<asp:ListItem Value="01">01</asp:ListItem>
								<asp:ListItem Value="02">02</asp:ListItem>
								<asp:ListItem Value="03">03</asp:ListItem>
								<asp:ListItem Value="04">04</asp:ListItem>
								<asp:ListItem Value="05">05</asp:ListItem>
								<asp:ListItem Value="06">06</asp:ListItem>
								<asp:ListItem Value="07">07</asp:ListItem>
								<asp:ListItem Value="08">08</asp:ListItem>
								<asp:ListItem Value="09">09</asp:ListItem>
								<asp:ListItem Value="10">10</asp:ListItem>
								<asp:ListItem Value="11">11</asp:ListItem>
								<asp:ListItem Value="12">12</asp:ListItem>
							</asp:DropDownList>
							<asp:Label ID="lblMonth" runat="server" Text="月"></asp:Label>
						</td>
					</tr>
                </table>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    CausesValidation="False" />
            </fieldset>
            <fieldset class="fieldset-inner">
                <legend>[おみくじ利用状況]</legend>
                <asp:Panel runat="server" ID="pnlGrid">
                    <asp:GridView ID="grdDailyOmikujiInquiry" runat="server" AutoGenerateColumns="False" OnDataBound="grdDailyOmikujiInquiry_DataBound" 
                        DataSourceID="" SkinID="GridViewFreeRowStyle" ShowFooter="True" OnRowDataBound="grdDailyOmikujiInquiry_RowDataBound" OnInit="grdDailyOmikujiInquiry_Init">
                        <Columns>
						<asp:TemplateField HeaderText="日付">
							<ItemStyle HorizontalAlign="Center" CssClass="RowStyleNoPad" />
							<ItemTemplate>
								<asp:Label ID="Label1" runat="server" Text='<%# Eval("PRINT_DAY") %>' Width="38px"></asp:Label>
							</ItemTemplate>
							<HeaderStyle Height="30px" CssClass="HeaderStyle"  />
							<FooterStyle Height="20px" HorizontalAlign="Center" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="曜日">
							<ItemStyle HorizontalAlign="Center" CssClass="RowStyleNoPad" VerticalAlign="Middle" />
							<ItemTemplate>
								<asp:Label ID="Label2" runat="server" Text='<%# Eval("REPORT_DAY_OF_WEEK") %>' Width="38px"></asp:Label>
							</ItemTemplate>
							<FooterStyle HorizontalAlign="Center" />
							<HeaderStyle CssClass="HeaderStyle"  />
						</asp:TemplateField>
						<asp:BoundField DataField="OMIKUJI_TYPE" HeaderText=""  >
						<ItemStyle HorizontalAlign="Center" />
						</asp:BoundField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
        </fieldset>
        <br />
    </div>
    <asp:ObjectDataSource ID="dsDailyOmikujiInquiry" runat="server" OnSelecting="dsDailyOmikujiInquiry_Selecting" OnSelected="dsDailyOmikujiInquiry_Selected"
         SelectMethod="GetPageCollection" TypeName="DailyOmikuji">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSeekSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
            <asp:Parameter Name="pYYYY" Type="String" />
            <asp:Parameter Name="pMM" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
</asp:Content>
