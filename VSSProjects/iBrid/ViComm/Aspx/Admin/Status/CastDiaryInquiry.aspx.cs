﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 日記記録
--	Progaram ID		: CastDiaryInquiry
--
--  Creation Date	: 2011.08.29
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Configuration;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using iBridCommLib;
using ViComm;

public partial class Status_CastDiaryInquiry : Page {
	private string recCount = string.Empty;

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			this.FirstLoad();
			this.InitPage();
			this.grdCastDiary.DataSourceID = string.Empty;

            if (!string.IsNullOrEmpty(Request.QueryString["redirect"])) {
                this.FirstSearchByQuery();
            }
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		this.GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void lnkDelCastDiary_Command(object sender,CommandEventArgs e) {
		string[] arguments = e.CommandArgument.ToString().Split(',');

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("DELETE_DIARY");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,arguments[0]);
			oDbSession.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,arguments[1]);
			oDbSession.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,arguments[2]);
			oDbSession.ProcedureInParm("pREPORT_DAY",DbSession.DbType.VARCHAR2,arguments[3]);
			oDbSession.ProcedureInParm("pCAST_DIARY_SUB_SEQ",DbSession.DbType.VARCHAR2,arguments[4]);
			oDbSession.ProcedureInParm("pDEL_FLAG",DbSession.DbType.VARCHAR2,arguments[6]);
			oDbSession.ProcedureInParm("pADMIN_DEL_FLAG",DbSession.DbType.VARCHAR2,ViCommConst.FLAG_ON_STR.Equals(arguments[7]) ? ViCommConst.FLAG_OFF_STR: ViCommConst.FLAG_ON_STR);
			oDbSession.ProcedureOutParm("pPIC_SEQ",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			string sPicSeq = oDbSession.GetStringValue("pPIC_SEQ");

			if (!string.IsNullOrEmpty(sPicSeq)) {
				string sFileNm = string.Empty;
				string sWebPhisicalDir = string.Empty;
				using (Site oSite = new Site()) {
					oSite.GetValue(arguments[0],"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
				}

				sFileNm = iBridUtil.addZero(sPicSeq,ViCommConst.OBJECT_NM_LENGTH);
				ViCommPrograms.DeleteFiles(ViCommPrograms.GetCastPicDir(sWebPhisicalDir,arguments[0],arguments[5]),string.Format("{0}*",sFileNm));
			}
		}

		this.GetList();
	}

	protected void dsCastDiary_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pSiteCd"] = this.lstSiteCd.SelectedValue;
		e.InputParameters["pCreateDayFrom"] = this.txtCreateDayFrom.Text;
		e.InputParameters["pCreateTimeFrom"] = this.txtCreateTimeFrom.Text;
		e.InputParameters["pCreateDayTo"] = this.txtCreateDayTo.Text;
		e.InputParameters["pCreateTimeTo"] = this.txtCreateTimeTo.Text;
		e.InputParameters["pLoginId"] = this.txtLoginId.Text.TrimEnd();
		e.InputParameters["pUserCharNo"] = this.txtUserCharNo.Text;
		e.InputParameters["pKeyword"] = this.txtKeyword.Text.Trim();
	}

	protected void dsCastDiary_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void grdCastDiary_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			if (DataBinder.Eval(e.Row.DataItem,"DEL_FLAG").ToString().Equals(ViCommConst.FLAG_ON_STR)) {
				e.Row.ForeColor = Color.Gray;
			}
			if (DataBinder.Eval(e.Row.DataItem,"ADMIN_DEL_FLAG").ToString().Equals(ViCommConst.FLAG_ON_STR)) {
				e.Row.ForeColor = Color.Gray;
			}
		}
	}

	private void FirstLoad() {
		this.grdCastDiary.PageSize = int.Parse(Session["PageSize"].ToString());
		this.grdCastDiary.DataSourceID = string.Empty;
		this.DataBind();
		if (string.IsNullOrEmpty(Session["SiteCd"].ToString())) {
			this.lstSiteCd.Items.Insert(0,new ListItem(string.Empty,string.Empty));
		}
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		this.lstSiteCd.DataSourceID = string.Empty;
	}

	private void InitPage() {
		this.pnlInfo.Visible = false;
		this.ClearField();
	}

	private void ClearField() {
		this.txtLoginId.Text = string.Empty;
		this.txtCreateDayFrom.Text = DateTime.Now.ToString("yyyy/MM/dd");
		this.txtCreateTimeFrom.Text = string.Empty;
		this.txtCreateDayTo.Text = string.Empty;
		this.txtCreateTimeTo.Text = string.Empty;
	}

    protected void FirstSearchByQuery() {
        this.txtLoginId.Text = Request.QueryString["loginid"].ToString();
        this.txtUserCharNo.Text = Request.QueryString["usercharno"].ToString();
        this.lstSiteCd.SelectedValue = Request.QueryString["sitecd"].ToString();
        this.txtCreateDayFrom.Text = DateTime.Now.AddMonths(-1).ToString("yyyy/MM/dd");
        this.txtCreateDayTo.Text = DateTime.Now.ToString("yyyy/MM/dd");
        this.GetList();
    }

	protected string GetRecCount() {
		return recCount;
	}

	private void GetList() {
		if (string.IsNullOrEmpty(this.txtCreateDayTo.Text)) {
			this.txtCreateDayTo.Text = this.txtCreateDayFrom.Text;
		}
		this.grdCastDiary.DataSourceID = "dsCastDiary";
		this.grdCastDiary.DataBind();
		this.pnlCount.DataBind();
		this.pnlInfo.Visible = true;
	}

	protected bool GetPicVisible(object pPicSeq) {
		return !string.IsNullOrEmpty(iBridUtil.GetStringValue(pPicSeq));
	}
	
	protected string GetDelFlagMark(object pDelFlag,object pAdminDelFlag) {
		if (pAdminDelFlag.ToString().Equals(ViCommConst.FLAG_ON_STR)) {
			return "管理者削除";
		} else if (pDelFlag.ToString().Equals(ViCommConst.FLAG_ON_STR)) {
			return "ﾕｰｻﾞｰ削除";
		} else {
			return string.Empty;
		}
	}

	protected string GetClientScript(object pPicUrl) {
		string sRoot = Request.Url.Authority + ConfigurationManager.AppSettings["Root"];
		return string.Format("javascript:win=window.open('http://{1}/ViewPic.html?url={0}','PictureZoom','width=300,height=320,resizable=yes,directories=no,scrollbars=no' , false);win.focus();return false;",pPicUrl,sRoot);
	}
}
