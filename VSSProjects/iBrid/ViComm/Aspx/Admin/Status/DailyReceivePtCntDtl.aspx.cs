﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 期間別出演者獲得報酬集計詳細
--	Progaram ID		: DailyReceivePtCntDtl
--
--  Creation Date	: 2017.03.08
--  Creater			: M&TT Zukeran
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Status_DailyReceivePtCntDtl:System.Web.UI.Page {
	/// <summary>データ検索用</summary>
	private string sSiteCd = string.Empty;
	private string sReportDate = string.Empty;
	private string sElapsedDays = string.Empty;

	/// <summary>データ行の結合用</summary>
	private string sBufCastLoginId = string.Empty;
	private int iRowIdx = 0;
	private int iRowSpan = 1;

	protected void Page_Load(object sender,EventArgs e) {
		// 一覧からのデータ取得
		this.sSiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		this.sReportDate = iBridUtil.GetStringValue(Request.QueryString["report"]);
		this.sElapsedDays = iBridUtil.GetStringValue(Request.QueryString["days"]);

		// ページの説明文を設定
		lblDescription.Text = string.Format(
			"集計日（{0}）でやりとり開始から{1}経過した組み合わせ"
			,this.sReportDate
			,this.GetTerm(this.sElapsedDays)
		);
	}

	/// <summary>
	/// 検索前処理
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void dsDailyReceivePtCntDtl_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		DailyReceivePtCnt.SearchCondition oSearchConditions = new DailyReceivePtCnt.SearchCondition();
		oSearchConditions.SiteCd = this.sSiteCd;
		oSearchConditions.ReportDate = this.sReportDate;
		oSearchConditions.DispElapsedDays = sElapsedDays;
		e.InputParameters["pSearchCondition"] = oSearchConditions;
	}

	/// <summary>
	/// データ設定処理
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void grdData_RowDataBound(object sender,GridViewRowEventArgs e) {
		// データが存在しない場合
		if (grdData.Controls.Count <= 0) {
			return;
		}

		// ヘッダー
		if (e.Row.RowType == DataControlRowType.Header) {
		}
		// データ行
		else if (e.Row.RowType == DataControlRowType.DataRow) {
			string sCastLoginId = DataBinder.Eval(e.Row.DataItem,"CAST_LOGIN_ID").ToString();

			// 上の行と結合しない場合
			if (!sBufCastLoginId.Equals(sCastLoginId)) {
				sBufCastLoginId = sCastLoginId;
				iRowIdx = e.Row.DataItemIndex;
				iRowSpan = 1;
			}
			// 上の行と結合する場合
			else {
				iRowSpan++;

				// 出演者IDを削除&結合
				e.Row.Cells.RemoveAt(0);
				grdData.Rows[iRowIdx].Cells[0].RowSpan = iRowSpan;
			}
		}
		// フッター
		else if (e.Row.RowType == DataControlRowType.Footer) {
		}
	}

	/// <summary>
	/// 経過期間の表示文言を取得
	/// </summary>
	/// <param name="sDispTerm"></param>
	/// <returns></returns>
	private string GetTerm(string sDispTerm) {
		int iDispTerm = int.Parse(sDispTerm);
		int iYear = 0;
		int iMonth = 0;
		int iMonthOfYear = 0;
		string sResult = string.Empty;

		if (iDispTerm >= 30) {
			iMonth = (iDispTerm / 30);
			iYear = (int)decimal.Floor(iMonth / 12);
			iMonthOfYear = iMonth - iYear * 12;

			if (iYear == 0) {
				sResult = string.Format("{0}ヶ月",iMonth);
			} else if (iMonthOfYear == 0) {
				sResult = string.Format("{0}年",iYear);
			} else {
				sResult = string.Format("{0}年{1}ヶ月",iYear,iMonthOfYear);
			}
		} else if (iDispTerm > 6) {
			sResult = string.Format("{0}週間",(iDispTerm / 7));
		} else {
			sResult = string.Format("{0}日",iDispTerm);
		}
		return sResult;
	}

	/// <summary>
	/// 出演者IDのリンクURLを取得
	/// </summary>
	/// <param name="pUserSeq"></param>
	/// <returns></returns>
	protected string GetCastLink(object pUserSeq) {
		return string.Format("~/Cast/CastView.aspx?userseq={0}",pUserSeq);
	}

	/// <summary>
	/// 会員IDのリンクURLを取得
	/// </summary>
	/// <param name="pSiteCd"></param>
	/// <param name="pUserSeq"></param>
	/// <returns></returns>
	protected string GetManLink(object pSiteCd,object pUserSeq) {
		return string.Format("~/Man/ManView.aspx?site={0}&userseq={1}",pSiteCd,pUserSeq);
	}
}
