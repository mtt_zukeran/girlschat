﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: メール送信日別アクセス集計
--	Progaram ID		: AccessMailCountByTxMailDayList
--
--  Creation Date	: 2015.06.03
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.IO;
using iBridCommLib;
using ViComm;

public partial class Status_AccessMailCountByTxMailDayList:System.Web.UI.Page {
	private string[] Week = { "日","月","火","水","木","金","土" };
	private Stream filter;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SITE_CD"]);
		}
		set {
			this.ViewState["SITE_CD"] = value;
		}
	}

	private string MailTemplateNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["MAIL_TEMPLATE_NO"]);
		}
		set {
			this.ViewState["MAIL_TEMPLATE_NO"] = value;
		}
	}

	private string TxMailDayFrom {
		get {
			return iBridUtil.GetStringValue(this.ViewState["TX_MAIL_DAY_FROM"]);
		}
		set {
			this.ViewState["TX_MAIL_DAY_FROM"] = value;
		}
	}

	private string TxMailDayTo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["TX_MAIL_DAY_TO"]);
		}
		set {
			this.ViewState["TX_MAIL_DAY_TO"] = value;
		}
	}

	private string Year {
		get {
			return iBridUtil.GetStringValue(this.ViewState["YEAR"]);
		}
		set {
			this.ViewState["YEAR"] = value;
		}
	}

	private string Month {
		get {
			return iBridUtil.GetStringValue(this.ViewState["MONTH"]);
		}
		set {
			this.ViewState["MONTH"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			grdAccessMailCountDaily.DataSourceID = "";
			DataBind();

			this.InitPage();
			
			this.SiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
			this.MailTemplateNo = iBridUtil.GetStringValue(Request.QueryString["mailtemplateno"]);
			this.TxMailDayFrom = iBridUtil.GetStringValue(Request.QueryString["txmaildayfrom"]);
			this.TxMailDayTo = iBridUtil.GetStringValue(Request.QueryString["txmaildayto"]);
			
			if (!string.IsNullOrEmpty(this.SiteCd)) {
				lstSiteCd.SelectedValue = this.SiteCd;
			}
			
			if (!string.IsNullOrEmpty(this.TxMailDayFrom)) {
				DateTime dtFrom;
				if (DateTime.TryParse(this.TxMailDayFrom,out dtFrom)) {
					lstFromYYYY.SelectedValue = dtFrom.ToString("yyyy");
					lstFromMM.SelectedValue = dtFrom.ToString("MM");
					lstFromDD.SelectedValue = dtFrom.ToString("dd");
				}
			}

			if (!string.IsNullOrEmpty(this.TxMailDayTo)) {
				DateTime dtTo;
				if (DateTime.TryParse(this.TxMailDayTo,out dtTo)) {
					lstToYYYY.SelectedValue = dtTo.ToString("yyyy");
					lstToMM.SelectedValue = dtTo.ToString("MM");
					lstToDD.SelectedValue = dtTo.ToString("dd");
				}
			}
			
			if (!string.IsNullOrEmpty(this.MailTemplateNo)) {
				txtMailTemplateNo.Text = this.MailTemplateNo;
				GetList();
			}
		}
	}

	private void InitPage() {
		lstSiteCd.DataBind();
		txtMailTemplateNo.Text = string.Empty;
		
		if (!IsPostBack) {
			SysPrograms.SetupFromToDay(lstFromYYYY,lstFromMM,lstFromDD,lstToYYYY,lstToMM,lstToDD,false);
		}

		lstFromYYYY.SelectedValue = DateTime.Now.AddDays(-8).ToString("yyyy");
		lstFromMM.SelectedValue = DateTime.Now.AddDays(-8).ToString("MM");
		lstFromDD.SelectedValue = DateTime.Now.AddDays(-8).ToString("dd");

		lstToYYYY.SelectedValue = DateTime.Now.AddDays(-1).ToString("yyyy");
		lstToMM.SelectedValue = DateTime.Now.AddDays(-1).ToString("MM");
		lstToDD.SelectedValue = DateTime.Now.AddDays(-1).ToString("dd");
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (this.IsValid) {
			GetList();
		}
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void btnCSV_Click(object sender,EventArgs e) {

		Response.Filter = filter;
		Response.AddHeader("Content-Disposition","attachment;filename=ACCESS_MAIL_COUNT_BY_TX_MAIL_DAY.CSV");
		Response.ContentType = "application/octet-stream-dummy";
		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		DataSet ds;
		using (AccessMailCountDaily oAccessMailCountDaily = new AccessMailCountDaily()) {
			AccessMailCountDaily.SearchCondition oSearchCondition = new AccessMailCountDaily.SearchCondition();
			oSearchCondition.SiteCd = this.lstSiteCd.SelectedValue;
			oSearchCondition.FromYYYY = this.lstFromYYYY.SelectedValue;
			oSearchCondition.FromMM = this.lstFromMM.SelectedValue;
			oSearchCondition.FromDD = this.lstFromDD.SelectedValue;
			oSearchCondition.ToYYYY = this.lstToYYYY.SelectedValue;
			oSearchCondition.ToMM = this.lstToMM.SelectedValue;
			oSearchCondition.ToDD = this.lstToDD.SelectedValue;
			oSearchCondition.MailTemplateNo = this.txtMailTemplateNo.Text;

			ds = oAccessMailCountDaily.GetAccessCountByTxMailDay(oSearchCondition);
			SetCsvData(ds);
			Response.End();
		}
	}

	protected void dsAccessMailCountDaily_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		
		AccessMailCountDaily.SearchCondition oSearchCondition = new AccessMailCountDaily.SearchCondition();
		oSearchCondition.SiteCd = this.lstSiteCd.SelectedValue;
		oSearchCondition.FromYYYY = this.lstFromYYYY.SelectedValue;
		oSearchCondition.FromMM = this.lstFromMM.SelectedValue;
		oSearchCondition.FromDD = this.lstFromDD.SelectedValue;
		oSearchCondition.ToYYYY = this.lstToYYYY.SelectedValue;
		oSearchCondition.ToMM = this.lstToMM.SelectedValue;
		oSearchCondition.ToDD = this.lstToDD.SelectedValue;
		oSearchCondition.MailTemplateNo = this.txtMailTemplateNo.Text;
		e.InputParameters[0] = oSearchCondition;
	}

	public void SetCsvData(DataSet pDataSet) {

		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		string sHeaderString = "送信日,送信数,ｱｸｾｽ総数,ﾕﾆｰｸ数,ｱｸｾｽ率\r\n";
		Response.BinaryWrite(encoding.GetBytes(sHeaderString));

		string sDtl = "";

		DataRow dr = null;
		for (int i = 0;i < pDataSet.Tables[0].Rows.Count;i++) {
			dr = pDataSet.Tables[0].Rows[i];

			sDtl = string.Format("{0},{1},{2},{3},{4}",
							dr["TX_MAIL_DAY"].ToString(),
							dr["TX_MAIL_COUNT"].ToString(),
							dr["ACCESS_COUNT"].ToString(),
							dr["UNIQUE_ACCESS_COUNT"].ToString(),
							GetUniqueAccessRate(dr["TX_MAIL_COUNT"].ToString(),dr["UNIQUE_ACCESS_COUNT"].ToString())
							);
			sDtl += "\r\n";
			Response.BinaryWrite(encoding.GetBytes(sDtl));
		}
	}

	protected void grdAccessMailCountDaily_RowDataBound(object sender,GridViewRowEventArgs e) {

		if (e.Row.RowType == DataControlRowType.DataRow) {
		}
	}

	private void GetList() {

		grdAccessMailCountDaily.PageIndex = 0;
		grdAccessMailCountDaily.DataSourceID = "dsAccessMailCountDaily";
		DataBind();
	}

	protected Color GetBackColor(object pDayOfWeek) {
		if (pDayOfWeek.ToString().Equals("Sat")) {
			return Color.FromArgb(0xCCCCFF);
		} else {
			if (pDayOfWeek.ToString().Equals("Sun")) {
				return Color.FromArgb(0xFFCCCC);
			} else {
				return Color.Empty;
			}
		}
	}

	private bool IsCorrectDate() {
		return true;
	}

	protected string GetUniqueAccessRate(string pTxMailCount,string pUniqueAccessCount) {
		double dTxMailCount;
		double dUniqueAccessCount;
		string sValue = "-";

		if (double.TryParse(pTxMailCount,out dTxMailCount) && double.TryParse(pUniqueAccessCount,out dUniqueAccessCount) && dTxMailCount > 0) {
			double dValue = dUniqueAccessCount / dTxMailCount * 100;

			dValue = Math.Round(dValue,2);
			sValue = string.Format("{0}%",dValue.ToString());
		}

		return sValue;
	}

	protected void vdcFrom_ServerValidate(object source,ServerValidateEventArgs args) {
		if (this.IsValid) {
			DateTime dtFrom;
			if (!DateTime.TryParse(string.Format("{0}/{1}/{2}",this.lstFromYYYY.SelectedValue,this.lstFromMM.SelectedValue,this.lstFromDD.SelectedValue),out dtFrom)) {
				args.IsValid = false;
			}
		}
	}

	protected void vdcTo_ServerValidate(object source,ServerValidateEventArgs args) {
		if (this.IsValid) {
			DateTime dtTo;
			if (!DateTime.TryParse(string.Format("{0}/{1}/{2}",this.lstToYYYY.SelectedValue,this.lstToMM.SelectedValue,this.lstToDD.SelectedValue),out dtTo)) {
				args.IsValid = false;
			}
		}
	}
}
