﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="BbsInquiry.aspx.cs" Inherits="Status_BbsInquiry" Title="Untitled Page" %>

<%@ Import Namespace="ViComm" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="掲示板記録"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 900px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイト
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="180px">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderStyle">
							性別
						</td>
						<td class="tdDataStyle">
							<asp:RadioButton ID="rdoSexMan" runat="server" Text="男性" GroupName="SexCd">
							</asp:RadioButton>
							<asp:RadioButton ID="rdoSexCast" runat="server" Text="出演者" GroupName="SexCd">
							</asp:RadioButton>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							書込日
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtReportDayFrom" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:TextBox ID="txtReportTimeFrom" runat="server" Width="15px" MaxLength="2"></asp:TextBox>
							～&nbsp;
							<asp:TextBox ID="txtReportDayTo" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:TextBox ID="txtReportTimeTo" runat="server" Width="15px" MaxLength="2"></asp:TextBox>
							<asp:RangeValidator ID="vdrReportTimeFrom" runat="server" ErrorMessage="書込時Fromを正しく入力して下さい。" ControlToValidate="txtReportTimeFrom" MaximumValue="23" MinimumValue="00"
								Type="Integer" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							<asp:RangeValidator ID="vdrReportTimeTo" runat="server" ErrorMessage="書込時Toを正しく入力して下さい。" ControlToValidate="txtReportTimeTo" MaximumValue="23" MinimumValue="00"
								Type="Integer" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							<asp:RequiredFieldValidator ID="vdrReportDayFrom" runat="server" ErrorMessage="送受信日Fromを入力して下さい。" ControlToValidate="txtReportDayFrom" ValidationGroup="Key"
								Display="Dynamic">*</asp:RequiredFieldValidator>
							<asp:RangeValidator ID="vdeReportDayFrom" runat="server" ErrorMessage="書込日Fromを正しく入力して下さい。" ControlToValidate="txtReportDayFrom" MaximumValue="2099/12/31"
								MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							<asp:RangeValidator ID="vdeReportDayTo" runat="server" ErrorMessage="書込日Toを正しく入力して下さい。" ControlToValidate="txtReportDayTo" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
								Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							<asp:CompareValidator ID="vdcReportDayFrom" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtReportDayFrom" ControlToValidate="txtReportDayTo"
								Operator="GreaterThanEqual" ValidationGroup="Key" Display="Dynamic">*</asp:CompareValidator>
						</td>
						<td class="tdHeaderStyle">
							書込ｷｬﾗｸﾀｰNo
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtTxUserCharNo" runat="server" Width="35px" MaxLength="2"></asp:TextBox>
						</td>
					</tr>
                    <tr>
                        <td class="tdHeaderStyle2">
                            書込ＩＤ
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtTxLoginId" runat="server" Width="70px"></asp:TextBox>
                        </td>
                        <td class="tdHeaderStyle2">
                            キーワード(空白区切り)
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtKeyword" runat="server" Width="300px"></asp:TextBox>
                        </td>
                    </tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" ValidationGroup="Key" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
			</asp:Panel>
		</fieldset>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset>
				<legend>[掲示板記録]</legend>
				<asp:Button runat="server" ID="btnAdminCheck" Text="選択したものを確認済にする" CssClass="seektopbutton" OnClick="btnAdminCheck_Click" />&nbsp;
				<asp:Button runat="server" ID="btnAdminCheckAll" Text="表示されているものを確認済にする" CssClass="seektopbutton" OnClick="btnAdminCheckAll_Click" />
				<br /><br />
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="500px">
					<asp:GridView ID="grdBbs" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsBbsLog" AllowSorting="True" SkinID="GridViewColor"
						OnRowDataBound="grdBbs_RowDataBound">
						<Columns>
							<asp:TemplateField HeaderText="確認">
								<ItemTemplate>
									<asp:Label ID="lblAdminChek" runat="server" Text="済" Visible='<%# Eval("ADMIN_CHECK_FLAG").ToString().Equals(ViCommConst.FLAG_ON_STR) ? true : false %>'></asp:Label>
									<asp:CheckBox ID="chkAdminCheck" runat="server" Visible='<%# Eval("ADMIN_CHECK_FLAG").ToString().Equals(ViCommConst.FLAG_OFF_STR) ? true : false %>' />
	                                <asp:HiddenField ID="hdnBbsSeq" runat="server" Value='<%# Eval("BBS_SEQ") %>' />
								</ItemTemplate>
								<ItemStyle HorizontalAlign="center" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="書込日時">
								<ItemTemplate>
									$NO_TRANS_START;
                                    <asp:Label ID="lblCreateDate" runat="server" Text='<%# Eval("CREATE_DATE")%>'></asp:Label><br />
									<asp:Label ID="lblSexCd" runat="server" Text='<%# GetSexCd(Eval("SEX_CD"))%>'></asp:Label><br />
									<asp:LinkButton ID="lnkDel" runat="server" CommandArgument='<%#string.Format("{0},{1},{2}",Eval("BBS_SEQ"),Eval("SEX_CD"),1) %>' Text="削除" Visible='<%#!iBridCommLib.iBridUtil.GetStringValue(Eval("DEL_FLAG")).Equals(ViComm.ViCommConst.FLAG_ON_STR)%>'
										OnCommand="lnkDelBbs_Command" OnClientClick="return confirm('削除を実行しますか？');"></asp:LinkButton>
									<asp:LinkButton ID="lnkRetrieve" runat="server" CommandArgument='<%#string.Format("{0},{1},{2}",Eval("BBS_SEQ"),Eval("SEX_CD"),0) %>' Text="復活" Visible='<%#!iBridCommLib.iBridUtil.GetStringValue(Eval("DEL_FLAG")).Equals(ViComm.ViCommConst.FLAG_OFF_STR)%>'
										OnCommand="lnkDelBbs_Command"></asp:LinkButton><br />
								    $NO_TRANS_END;
								</ItemTemplate>
								<ItemStyle VerticalAlign="Top" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="書込者">
								<ItemTemplate>
									<asp:HyperLink ID="lblTxLoginId" runat="server" NavigateUrl='<%# GetViewUrl(Eval("SEX_CD"),Eval("SITE_CD"),Eval("LOGIN_ID")) %>' Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
								</ItemTemplate>
								<ItemStyle VerticalAlign="Top" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾊﾝﾄﾞﾙ名">
								<ItemTemplate>
									<asp:Label runat="server" Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>' ID="lblHandleNm" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="内容">
								<ItemTemplate>
									<asp:Label ID="lblMailTitle" runat="server" Text='<%# Eval("BBS_TITLE") %>'></asp:Label><br />
									<asp:Label ID="lblDoc" runat="server" Text='<%# Eval("BBS_DOC") %>' Width="500px"></asp:Label>
								</ItemTemplate>
								<ItemStyle VerticalAlign="Top" />
							</asp:TemplateField>
							<asp:BoundField DataField="READING_COUNT" HeaderText="被閲覧回数" ItemStyle-HorizontalAlign="Right" />
							<asp:TemplateField HeaderText="削除">
								<ItemTemplate>
									<%# GetDelFlag(Eval("DEL_FLAG"),Eval("ADMIN_DEL_FLAG")) %>
								</ItemTemplate>
								<ItemStyle VerticalAlign="Top" HorizontalAlign="Center" />
							</asp:TemplateField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
				&nbsp;
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">Record Count
						<%#GetRecCount() %>
					</a>
					<br />
					<a class="reccount">Current viewing page
						<%=grdBbs.PageIndex + 1%>
						of
						<%=grdBbs.PageCount%>
					</a>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsBbsLog" runat="server" SelectMethod="GetPageCollection" TypeName="BbsLog" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsBbs_Selected" OnSelecting="dsBbs_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pSexCd" Type="String" />
			<asp:Parameter Name="pReportDayFrom" Type="String" />
			<asp:Parameter Name="pReportTimeFrom" Type="String" />
			<asp:Parameter Name="pReportDayTo" Type="String" />
			<asp:Parameter Name="pReportTimeTo" Type="String" />
			<asp:Parameter Name="pTxLoginId" Type="String" />
			<asp:Parameter Name="pTxUserCharNo" Type="String" />
            <asp:Parameter Name="pKeyword" Type="String" DefaultValue="" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsMailType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="82" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrReportDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdeReportDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdeReportDayTo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdrReportTimeFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5" TargetControlID="vdrReportTimeTo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="vdcReportDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:MaskedEditExtender ID="mskReportDayFrom" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="True"
		TargetControlID="txtReportDayFrom">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskReportDayTo" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="True"
		TargetControlID="txtReportDayTo">
	</ajaxToolkit:MaskedEditExtender>
</asp:Content>
