﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 会員別使用ポイント集計
--	Progaram ID		: ManUsedPoint
--
--  Creation Date	: 2010.05.13
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Text;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class Status_ManUsedPoint:System.Web.UI.Page {
	private string recCount = "";
	private int totalCount = 0;

	private int prvTvTalkPoint;			// TV 
	private int pubTvTalkPoint;			// ﾁｬｯﾄ 
	private int prvVoiceTalkPoint;		// 音声 
	private int pubVoiceTalkPoint;		// 音声ﾁｬｯﾄ 
	private int viewTalkPoint;			// 会話覗き 
	private int viewOnlinePoint;		// 部屋覗き 
	private int viewLivePoint;			// ﾗｲﾌﾞ視聴 
	private int playMoviePoint;			// ﾛﾝｸﾞﾑｰﾋﾞｰ 
	private int playPfVoicePoint;		// PF再生 
	private int recPfVoicePoint;		// PF録音 
	private int wiretappingPoint;		// 盗聴 
	private int gpfVoiceTalkPoint;		// GP会話 
	private int playPvMsgPoint;			// ﾌﾟﾗｲﾍﾞｰﾄ伝言再生 
	private int castPrvTvTalkPoint;		// 男性待機・TV 
	private int castPubTvTalkPoint;		// 男性待機・ﾁｬｯﾄ 
	private int castPrvVoiceTalkPoint;	// 男性待機・音声 
	private int castPubVoiceTalkPoint;	// 男性待機・音声ﾁｬｯﾄ 
	private int talkPoint;				// 電話計 

	private int txMailPoint;			// ﾒｰﾙ送信 
	private int txMailWithPicPoint;		// 写真添付ﾒｰﾙ送信 
	private int txMailWithMoviePoint;	// 動画添付ﾒｰﾙ送信 
	private int mailWithPicPoint;		// ﾒｰﾙ添付写真閲覧 
	private int mailWithMoviePoint;		// ﾒｰﾙ添付動画閲覧 
	private int bbsPicPoint;			// 掲示板写真閲覧 
	private int bbsMoviePoint;			// 掲示板動画閲覧 
	private int writeBbsPoint;			// 掲示板書込 
	private int downloadMoviePoint;		// ﾀﾞｳﾝﾛｰﾄﾞ動画
	private int socialGamePoint;		// ｿｰｼｬﾙｹﾞｰﾑ	private int yakyukenPoint;			// 野球拳 or FC会費	private int presentMailPoint;		// ﾌﾟﾚｾﾞﾝﾄﾒｰﾙ
	private int timeCall;				// 定時ｺｰﾙ

	private int downloadVoicePoint;		// ﾀﾞｳﾝﾛｰﾄﾞ音声
	private int webTotalPoint;			// WEB計 

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
			if (Session["SiteCd"].ToString().Equals("")) {
				lstSiteCd.Items.Insert(0,new ListItem("",""));
			}
			lstSiteCd.DataSourceID = "";
			if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
				lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			}
		} else {
			GetList();
		}
	}

	private void InitPage() {
		grdUserManUsedPoint.PageSize = int.Parse(Session["PageSize"].ToString());
		grdUserManUsedPoint.PageIndex = 0;
		grdUserManUsedPoint.DataSourceID = "";
		pnlInfo.Visible = false;
		ClearField();
		DataBind();
		if (!IsPostBack) {
			SysPrograms.SetupFromToDayTime(lstFromYYYY,lstFromMM,lstFromDD,lstFromHH,lstToYYYY,lstToMM,lstToDD,lstToHH,true);
			lstFromYYYY.SelectedIndex = 0;
			lstToYYYY.SelectedIndex = 0;
			lstFromMM.SelectedValue = DateTime.Now.ToString("MM");
			lstToMM.SelectedValue = DateTime.Now.ToString("MM");
			lstFromDD.SelectedValue = DateTime.Now.ToString("dd");
			lstToDD.SelectedValue = DateTime.Now.ToString("dd");
			lstFromHH.SelectedIndex = 0;
			lstToHH.SelectedIndex = 0;
		}
	}

	private void ClearField() {
		totalCount = 0;
		btnCsv.Visible = false;
		btnCSVDailyTotal.Visible = false;

		prvTvTalkPoint = 0;
		pubTvTalkPoint = 0;
		prvVoiceTalkPoint = 0;
		pubVoiceTalkPoint = 0;
		viewTalkPoint = 0;
		viewOnlinePoint = 0;
		viewLivePoint = 0;
		playMoviePoint = 0;
		playPfVoicePoint = 0;
		recPfVoicePoint = 0;
		wiretappingPoint = 0;
		gpfVoiceTalkPoint = 0;
		playPvMsgPoint = 0;
		castPrvTvTalkPoint = 0;
		castPubTvTalkPoint = 0;
		castPrvVoiceTalkPoint = 0;
		castPubVoiceTalkPoint = 0;
		talkPoint = 0;

		txMailPoint = 0;
		txMailWithPicPoint = 0;
		txMailWithMoviePoint = 0;
		mailWithPicPoint = 0;
		mailWithMoviePoint = 0;
		bbsPicPoint = 0;
		bbsMoviePoint = 0;
		writeBbsPoint = 0;
		downloadMoviePoint = 0;
		timeCall = 0;
		downloadVoicePoint = 0;
		socialGamePoint = 0;
		yakyukenPoint = 0;
		presentMailPoint = 0;

		webTotalPoint = 0;
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void btnCSV_Click(object sender,EventArgs e) {
		string fileName = ViCommConst.CSV_FILE_NM_MAN_USED_POINT;

		Response.AddHeader("Content-Disposition","attachment;filename=" + fileName);
		Response.ContentType = "application/octet-stream-dummy";

		using (UserManUsedPoint oManUsedPoint = new UserManUsedPoint()) {
			DataSet ds = oManUsedPoint.GetPageCollection(
								lstSiteCd.SelectedValue,
								lstFromYYYY.SelectedValue,
								lstFromMM.SelectedValue,
								lstFromDD.SelectedValue,
								lstFromHH.SelectedValue,
								lstToYYYY.SelectedValue,
								lstToMM.SelectedValue,
								lstToDD.SelectedValue,
								lstToHH.SelectedValue
							);

			WriteCsvString(ds);
			Response.End();

			Response.AddHeader("Content-Disposition","attachment;filename=" + fileName);
			Response.ContentType = "application/octet-stream-dummy";
			
			
		}
	}

	protected void btnCSVDailyTotal_Click(object sender,EventArgs e) {
		string fileName = "DailyManUsedPoint.csv";

		Response.AddHeader("Content-Disposition","attachment;filename=" + fileName);
		Response.ContentType = "application/octet-stream-dummy";

		using (UserManUsedPoint oManUsedPoint = new UserManUsedPoint()) {
			DataSet ds = oManUsedPoint.GetDailyTotalPoint(
								lstSiteCd.SelectedValue,
								lstFromYYYY.SelectedValue,
								lstFromMM.SelectedValue,
								lstFromDD.SelectedValue,
								lstFromHH.SelectedValue,
								lstToYYYY.SelectedValue,
								lstToMM.SelectedValue,
								lstToDD.SelectedValue,
								lstToHH.SelectedValue
							);

			WriteDailyTotalCsvString(ds);
			Response.End();
		}
	}

	protected void grdUserManUsedPoint_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			prvTvTalkPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PRV_TV_TALK_POINT"));
			pubTvTalkPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PUB_TV_TALK_POINT"));
			prvVoiceTalkPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PRV_VOICE_TALK_POINT"));
			pubVoiceTalkPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PUB_VOICE_TALK_POINT"));
			viewTalkPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"VIEW_TALK_POINT"));
			viewOnlinePoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"VIEW_ONLINE_POINT"));
			viewLivePoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"VIEW_LIVE_POINT"));
			playMoviePoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PLAY_MOVIE_POINT"));
			playPfVoicePoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PLAY_PF_VOICE_POINT"));
			recPfVoicePoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"REC_PF_VOICE_POINT"));
			wiretappingPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"WIRETAPPING_POINT"));
			gpfVoiceTalkPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"GPF_TALK_VOICE_POINT"));
			playPvMsgPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PLAY_PV_MSG_POINT"));
			castPrvTvTalkPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CAST_PRV_TV_TALK_POINT"));
			castPubTvTalkPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CAST_PUB_TV_TALK_POINT"));
			castPrvVoiceTalkPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CAST_PRV_VOICE_TALK_POINT"));
			castPubVoiceTalkPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CAST_PUB_VOICE_TALK_POINT"));
			talkPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"TALK_POINT"));

			txMailPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"TX_MAIL_POINT"));
			txMailWithPicPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"TX_MAIL_WITH_PIC_POINT"));
			txMailWithMoviePoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"TX_MAIL_WITH_MOVIE_POINT"));
			mailWithPicPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"MAIL_WITH_PIC_POINT"));
			mailWithMoviePoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"MAIL_WITH_MOVIE_POINT"));
			bbsPicPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"BBS_PIC_POINT"));
			bbsMoviePoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"BBS_MOVIE_POINT"));
			writeBbsPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"WRITE_BBS_POINT"));
			downloadMoviePoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"DOWNLOAD_MOVIE_POINT"));
			timeCall += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"TIME_CALL_POINT"));
			downloadVoicePoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"DOWNLOAD_VOICE_POINT"));
			socialGamePoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"SOCIAL_GAME_POINT"));
			yakyukenPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"YAKYUKEN_POINT"));
			presentMailPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PRESENT_MAIL_POINT"));
			webTotalPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"WEB_TOTAL_POINT"));

		} else if (e.Row.RowType == DataControlRowType.Footer) {
			Label lblPrvTvTalkPoint = (Label)e.Row.FindControl("lblPrvTvTalkPoint");
			Label lblPubTvTalkPoint = (Label)e.Row.FindControl("lblPubTvTalkPoint");
			Label lblPrvVoiceTalkPoint = (Label)e.Row.FindControl("lblPrvVoiceTalkPoint");
			Label lblPubVoiceTalkPoint = (Label)e.Row.FindControl("lblPubVoiceTalkPoint");
			Label lblViewTalkPoint = (Label)e.Row.FindControl("lblViewTalkPoint");
			Label lblViewOnlinePoint = (Label)e.Row.FindControl("lblViewOnlinePoint");
			Label lblViewLivePoint = (Label)e.Row.FindControl("lblViewLivePoint");
			Label lblPlayMoviePoint = (Label)e.Row.FindControl("lblPlayMoviePoint");
			Label lblPlayPfVoicePoint = (Label)e.Row.FindControl("lblPlayPfVoicePoint");
			Label lblRecPfVoicePoint = (Label)e.Row.FindControl("lblRecPfVoicePoint");
			Label lblWiretappingPoint = (Label)e.Row.FindControl("lblWiretappingPoint");
			Label lblGpfVoiceTalkPoint = (Label)e.Row.FindControl("lblGpfVoiceTalkPoint");
			Label lblPlayPvMsgPoint = (Label)e.Row.FindControl("lblPlayPvMsgPoint");
			Label lblCastPrvTvTalkPoint = (Label)e.Row.FindControl("lblCastPrvTvTalkPoint");
			Label lblCastPubTvTalkPoint = (Label)e.Row.FindControl("lblCastPubTvTalkPoint");
			Label lblCastPrvVoiceTalkPoint = (Label)e.Row.FindControl("lblCastPrvVoiceTalkPoint");
			Label lblCastPubVoiceTalkPoint = (Label)e.Row.FindControl("lblCastPubVoiceTalkPoint");
			Label lblTalkPoint = (Label)e.Row.FindControl("lblTalkPoint");

			Label lblTxMailPoint = (Label)e.Row.FindControl("lblTxMailPoint");
			Label lblTxMailWithPicPoint = (Label)e.Row.FindControl("lblTxMailWithPicPoint");
			Label lblTxMailWithMoviePoint = (Label)e.Row.FindControl("lblTxMailWithMoviePoint");
			Label lblMailWithPicPoint = (Label)e.Row.FindControl("lblMailWithPicPoint");
			Label lblMailWithMoviePoint = (Label)e.Row.FindControl("lblMailWithMoviePoint");
			Label lblBbsPicPoint = (Label)e.Row.FindControl("lblBbsPicPoint");
			Label lblBbsMoviePoint = (Label)e.Row.FindControl("lblBbsMoviePoint");
			Label lblWriteBbsPoint = (Label)e.Row.FindControl("lblWriteBbsPoint");
			Label lblWebTotalPoint = (Label)e.Row.FindControl("lblWebTotalPoint");
			Label lblDownloadMoviePoint = (Label)e.Row.FindControl("lblDownloadMoviePoint");
			Label lblTimeCallPoint = (Label)e.Row.FindControl("lblTimeCallPoint");
			Label lblDownloadVoicePoint = (Label)e.Row.FindControl("lblDownloadVoicePoint");
			Label lblSocialGamePoint = (Label)e.Row.FindControl("lblSocialGamePoint");
			Label lblYakyukenPoint = (Label)e.Row.FindControl("lblYakyukenPoint");
			Label lblPresentMailPoint = (Label)e.Row.FindControl("lblPresentMailPoint");
			Label lblTotalPoint = (Label)e.Row.FindControl("lblTotalPoint");

			int iTotalPoint = talkPoint + webTotalPoint;

			lblPrvTvTalkPoint.Text = prvTvTalkPoint.ToString();
			lblPubTvTalkPoint.Text = pubTvTalkPoint.ToString();
			lblPrvVoiceTalkPoint.Text = prvVoiceTalkPoint.ToString();
			lblPubVoiceTalkPoint.Text = pubVoiceTalkPoint.ToString();
			lblViewTalkPoint.Text = viewTalkPoint.ToString();
			lblViewOnlinePoint.Text = viewOnlinePoint.ToString();
			lblViewLivePoint.Text = viewLivePoint.ToString();
			lblPlayMoviePoint.Text = playMoviePoint.ToString();
			lblPlayPfVoicePoint.Text = playPfVoicePoint.ToString();
			lblRecPfVoicePoint.Text = recPfVoicePoint.ToString();
			lblWiretappingPoint.Text = wiretappingPoint.ToString();
			lblGpfVoiceTalkPoint.Text = gpfVoiceTalkPoint.ToString();
			lblPlayPvMsgPoint.Text = playPvMsgPoint.ToString();
			lblCastPrvTvTalkPoint.Text = castPrvTvTalkPoint.ToString();
			lblCastPubTvTalkPoint.Text = castPubTvTalkPoint.ToString();
			lblCastPrvVoiceTalkPoint.Text = castPrvVoiceTalkPoint.ToString();
			lblCastPubVoiceTalkPoint.Text = castPubVoiceTalkPoint.ToString();
			lblTalkPoint.Text = talkPoint.ToString();

			lblTxMailPoint.Text = txMailPoint.ToString();
			lblTxMailWithPicPoint.Text = txMailWithPicPoint.ToString();
			lblTxMailWithMoviePoint.Text = txMailWithMoviePoint.ToString();
			lblMailWithPicPoint.Text = mailWithPicPoint.ToString();
			lblMailWithMoviePoint.Text = mailWithMoviePoint.ToString();
			lblBbsPicPoint.Text = bbsPicPoint.ToString();
			lblBbsMoviePoint.Text = bbsMoviePoint.ToString();
			lblWriteBbsPoint.Text = writeBbsPoint.ToString();
			lblDownloadMoviePoint.Text = downloadMoviePoint.ToString();
			lblTimeCallPoint.Text = timeCall.ToString();
			lblDownloadVoicePoint.Text = downloadVoicePoint.ToString();
			lblSocialGamePoint.Text = socialGamePoint.ToString();
			lblYakyukenPoint.Text = yakyukenPoint.ToString();
			lblPresentMailPoint.Text = presentMailPoint.ToString();
			lblWebTotalPoint.Text = webTotalPoint.ToString();

			lblTotalPoint.Text = iTotalPoint.ToString();

		}
	}

	public static bool IsAvailableService(ulong pService) {
		using (ManageCompany oInstance = new ManageCompany()) {
			return oInstance.IsAvailableService(pService);
		}
	}
	private void GetList() {
		ClearField();

		for (int i = 0;i < grdUserManUsedPoint.Columns.Count;i++) {
			grdUserManUsedPoint.Columns[i].Visible = true;
		}

		if (rdoTel.Checked) {
			for (int i = 19;i < 32;i++) {
				grdUserManUsedPoint.Columns[i].Visible = false;
			}
		} else {
			for (int i = 1;i < 18;i++) {
				grdUserManUsedPoint.Columns[i].Visible = false;
			}
			grdUserManUsedPoint.Columns[27].Visible = IsAvailableService(ViCommConst.RELEASE_SALE_MOVIE);
			grdUserManUsedPoint.Columns[28].Visible = IsAvailableService(ViCommConst.RELEASE_TIME_CALL);
			grdUserManUsedPoint.Columns[29].Visible = IsAvailableService(ViCommConst.RELEASE_SALE_VOICE);
		}

		for (int i = 10;i < 14;i++) {
			grdUserManUsedPoint.Columns[i].Visible = false;
		}

		using (UserManUsedPoint oUserManUsedPoint = new UserManUsedPoint()) {
			totalCount = oUserManUsedPoint.GetTotal(
					lstSiteCd.SelectedValue,
					lstFromYYYY.SelectedValue,
					lstFromMM.SelectedValue,
					lstFromDD.SelectedValue,
					lstFromHH.SelectedValue,
					lstToYYYY.SelectedValue,
					lstToMM.SelectedValue,
					lstToDD.SelectedValue,
					lstToHH.SelectedValue);
		}

		grdUserManUsedPoint.DataSourceID = "dsUserManUsedPoint";
		DataBind();

		int iCompare = string.Compare(Session["AdminType"].ToString(),ViCommConst.RIGHT_SITE_OWNER);
		btnCsv.Visible = (iCompare >= 0);
		btnCSVDailyTotal.Visible = (iCompare >= 0);
		pnlInfo.Visible = true;
	}


	private void WriteCsvString(DataSet pDataSet) {
		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");
		bool bReleaseSaleMovie = IsAvailableService(ViCommConst.RELEASE_SALE_MOVIE);
		bool bReleaseTimeCall = IsAvailableService(ViCommConst.RELEASE_TIME_CALL);
		bool bReleaseSaleVoice = IsAvailableService(ViCommConst.RELEASE_SALE_VOICE);
		int iCompanyMask = int.Parse(Session["CompanyMask"].ToString());
		bool bReleaseSocialGame = true;

		StringBuilder oCsvHeaderBuilder = new StringBuilder();

		oCsvHeaderBuilder.Append("ﾛｸﾞｲﾝID,");
		oCsvHeaderBuilder.Append("TV,");
		oCsvHeaderBuilder.Append("TVﾁｬｯﾄ,");
		oCsvHeaderBuilder.Append("音声,");
		oCsvHeaderBuilder.Append("音声ﾁｬｯﾄ,");
		oCsvHeaderBuilder.Append("会話ﾓﾆﾀ,");
		oCsvHeaderBuilder.Append("音声ﾓﾆﾀ,");
		oCsvHeaderBuilder.Append("部屋ﾓﾆﾀ,");
		oCsvHeaderBuilder.Append("ﾗｲﾌﾞ視聴,");
		oCsvHeaderBuilder.Append("ﾛﾝｸﾞﾑｰﾋﾞｰ,");
		oCsvHeaderBuilder.Append("男性待機・TV,");
		oCsvHeaderBuilder.Append("男性待機・TVﾁｬｯﾄ,");
		oCsvHeaderBuilder.Append("男性待機・音声,");
		oCsvHeaderBuilder.Append("男性待機・音声ﾁｬｯﾄ,");
		oCsvHeaderBuilder.Append("電話計,");
		oCsvHeaderBuilder.Append("ﾒｰﾙ送信,");
		oCsvHeaderBuilder.Append("写真添付ﾒｰﾙ送信,");
		oCsvHeaderBuilder.Append("動画添付ﾒｰﾙ送信,");
		oCsvHeaderBuilder.Append("ﾒｰﾙ添付写真閲覧,");
		oCsvHeaderBuilder.Append("ﾒｰﾙ添付動画閲覧,");
		oCsvHeaderBuilder.Append("BBS写真閲覧,");
		oCsvHeaderBuilder.Append("BBS動画閲覧,");
		oCsvHeaderBuilder.Append("BBS書込,");
		if (bReleaseSaleMovie) {
			oCsvHeaderBuilder.Append("ﾀﾞｳﾝﾛｰﾄﾞﾑｰﾋﾞｰ,");
		}
		if (bReleaseTimeCall) {
			oCsvHeaderBuilder.Append("定時ｺｰﾙ,");
		}
		if (bReleaseSaleVoice) {
			oCsvHeaderBuilder.Append("ﾀﾞｳﾝﾛｰﾄﾞ着ﾎﾞｲｽ,");
		}
		if (bReleaseSocialGame) {
			oCsvHeaderBuilder.Append("ｿｰｼｬﾙｹﾞｰﾑ,");
		}
		oCsvHeaderBuilder.Append("野球拳/FC会費,");
		oCsvHeaderBuilder.Append("ﾌﾟﾚｾﾞﾝﾄﾒｰﾙ,");
		oCsvHeaderBuilder.Append("WEB計,");
		oCsvHeaderBuilder.Append("合計,");
		oCsvHeaderBuilder.Append("ﾕｰｻﾞｰﾗﾝｸ");
		oCsvHeaderBuilder.AppendLine();

		Response.BinaryWrite(encoding.GetBytes(oCsvHeaderBuilder.ToString()));

		DataRow dr = null;
		int iTotalPoint;

		for (int i = 0;i < pDataSet.Tables[0].Rows.Count;i++) {
			dr = pDataSet.Tables[0].Rows[i];

			iTotalPoint = int.Parse(dr["TALK_POINT"].ToString()) + int.Parse(dr["WEB_TOTAL_POINT"].ToString());

			StringBuilder oCsvDataBuilder = new StringBuilder();

			oCsvDataBuilder.Append(dr["LOGIN_ID"]).Append(",");
			oCsvDataBuilder.Append(dr["PRV_TV_TALK_POINT"]).Append(",");
			oCsvDataBuilder.Append(dr["PUB_TV_TALK_POINT"]).Append(",");
			oCsvDataBuilder.Append(dr["PRV_VOICE_TALK_POINT"]).Append(",");
			oCsvDataBuilder.Append(dr["PUB_VOICE_TALK_POINT"]).Append(",");
			oCsvDataBuilder.Append(dr["VIEW_TALK_POINT"]).Append(",");
			oCsvDataBuilder.Append(dr["WIRETAPPING_POINT"]).Append(",");
			oCsvDataBuilder.Append(dr["VIEW_ONLINE_POINT"]).Append(",");
			oCsvDataBuilder.Append(dr["VIEW_LIVE_POINT"]).Append(",");
			oCsvDataBuilder.Append(dr["PLAY_MOVIE_POINT"]).Append(",");
			oCsvDataBuilder.Append(dr["CAST_PRV_TV_TALK_POINT"]).Append(",");
			oCsvDataBuilder.Append(dr["CAST_PUB_TV_TALK_POINT"]).Append(",");
			oCsvDataBuilder.Append(dr["CAST_PRV_VOICE_TALK_POINT"]).Append(",");
			oCsvDataBuilder.Append(dr["CAST_PUB_VOICE_TALK_POINT"]).Append(",");
			oCsvDataBuilder.Append(dr["TALK_POINT"]).Append(",");
			oCsvDataBuilder.Append(dr["TX_MAIL_POINT"]).Append(",");
			oCsvDataBuilder.Append(dr["TX_MAIL_WITH_PIC_POINT"]).Append(",");
			oCsvDataBuilder.Append(dr["TX_MAIL_WITH_MOVIE_POINT"]).Append(",");
			oCsvDataBuilder.Append(dr["MAIL_WITH_PIC_POINT"]).Append(",");
			oCsvDataBuilder.Append(dr["MAIL_WITH_MOVIE_POINT"]).Append(",");
			oCsvDataBuilder.Append(dr["BBS_PIC_POINT"]).Append(",");
			oCsvDataBuilder.Append(dr["BBS_MOVIE_POINT"]).Append(",");
			oCsvDataBuilder.Append(dr["WRITE_BBS_POINT"]).Append(",");
			if (bReleaseSaleMovie) {
				oCsvDataBuilder.Append(dr["DOWNLOAD_MOVIE_POINT"]).Append(",");
			}
			if (bReleaseTimeCall) {
				oCsvDataBuilder.Append(dr["TIME_CALL_POINT"]).Append(",");
			}
			if (bReleaseSaleVoice) {
				oCsvDataBuilder.Append(dr["DOWNLOAD_VOICE_POINT"]).Append(",");
			}
			if (bReleaseSocialGame) {
				oCsvDataBuilder.Append(dr["SOCIAL_GAME_POINT"]).Append(",");
			}
			oCsvDataBuilder.Append(dr["YAKYUKEN_POINT"]).Append(",");
			oCsvDataBuilder.Append(dr["PRESENT_MAIL_POINT"]).Append(",");
			oCsvDataBuilder.Append(dr["WEB_TOTAL_POINT"]).Append(",");
			oCsvDataBuilder.Append(iTotalPoint.ToString()).Append(",");
			oCsvDataBuilder.Append(dr["USER_RANK_NM"]);
			oCsvDataBuilder.AppendLine();
			Response.BinaryWrite(encoding.GetBytes(oCsvDataBuilder.ToString()));
		}
	}


	private void WriteDailyTotalCsvString(DataSet pDataSet) {
		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		StringBuilder oCsvHeaderBuilder = new StringBuilder();

		oCsvHeaderBuilder.Append("ﾛｸﾞｲﾝID,");
		oCsvHeaderBuilder.Append("ﾕｰｻﾞｰSEQ,");
		oCsvHeaderBuilder.Append("報告日,");
		oCsvHeaderBuilder.Append("使用ﾎﾟｲﾝﾄ");
		oCsvHeaderBuilder.AppendLine();

		Response.BinaryWrite(encoding.GetBytes(oCsvHeaderBuilder.ToString()));

		DataRow dr = null;

		for (int i = 0;i < pDataSet.Tables[0].Rows.Count;i++) {
			dr = pDataSet.Tables[0].Rows[i];

			StringBuilder oCsvDataBuilder = new StringBuilder();

			oCsvDataBuilder.Append(dr["LOGIN_ID"]).Append(",");
			oCsvDataBuilder.Append(dr["MAN_USER_SEQ"]).Append(",");
			oCsvDataBuilder.Append(dr["CHARGE_DAY"]).Append(",");
			oCsvDataBuilder.Append(dr["CHARGE_POINT"]);
			oCsvDataBuilder.AppendLine();
			Response.BinaryWrite(encoding.GetBytes(oCsvDataBuilder.ToString()));
		}
	}

	protected string CalcSum(object pSum1,object pSum2) {
		int iSum = int.Parse(pSum1.ToString()) + int.Parse(pSum2.ToString());
		return iSum.ToString();
	}

	protected void grdUserManUsedPoint_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = lstFromYYYY.SelectedValue;
		e.InputParameters[2] = lstFromMM.SelectedValue;
		e.InputParameters[3] = lstFromDD.SelectedValue;
		e.InputParameters[4] = lstFromHH.SelectedValue;
		e.InputParameters[5] = lstToYYYY.SelectedValue;
		e.InputParameters[6] = lstToMM.SelectedValue;
		e.InputParameters[7] = lstToDD.SelectedValue;
		e.InputParameters[8] = lstToHH.SelectedValue;
	}

	protected void grdUserManUsedPoint_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	protected string GetTotalCount() {
		return totalCount.ToString();
	}
}
