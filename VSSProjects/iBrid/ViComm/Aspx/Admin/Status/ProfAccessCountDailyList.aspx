﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ProfAccessCountDailyList.aspx.cs" Inherits="Status_ProfAccessCountDailyList"
    Title="プロフアクセス集計(日別)" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="プロフアクセス集計(日別)"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <fieldset class="fieldset">
            <legend>[検索条件]</legend>
            <asp:Panel runat="server" ID="pnlKey">
                <table border="0" style="width: 740px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle">
                            サイト
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                DataValueField="SITE_CD" Width="180px">
                            </asp:DropDownList>
                        </td>
					</tr>
					<tr>
                        <td class="tdHeaderStyle2">
							報告日
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstYear" runat="server" Width="56px">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstMonth" runat="server" Width="40px">
							</asp:DropDownList>月
							<asp:DropDownList ID="lstDay" runat="server" Width="40px">
							</asp:DropDownList>日
							<asp:CustomValidator runat="server" ID="vdcReportDay" ErrorMessage="報告日が不正です。" OnServerValidate="vdcReportDay_ServerValidate" ValidationGroup="Seek"></asp:CustomValidator>
						</td>
                    </tr>
                </table>
                <asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Seek" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
                <asp:Button runat="server" ID="btnCSV" Text="CSV出力" CssClass="seekbutton" CausesValidation="False"
                    OnClick="btnCSV_Click" />
            </asp:Panel>
        </fieldset>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[集計データ]</legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count of
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%=grdProfAccessCountDaily.PageIndex + 1%>
                        of
                        <%=grdProfAccessCountDaily.PageCount%>
                    </a>
                </asp:Panel>
                &nbsp;
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="430px">
                    <asp:GridView ID="grdProfAccessCountDaily" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        DataSourceID="dsProfAccessCountDaily" OnSorting="grdProfAccessCountDaily_Sorting" OnSorted="grdProfAccessCountDaily_Sorted" AllowSorting="True" SkinID="GridViewColor" OnDataBound="grdProfAccessCountDaily_DataBound">
                        <Columns>
                            <asp:BoundField DataField="REFERER_ASPX_NM" HeaderText="リファラ">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="DISPLAY_NM" HeaderText="ページ名称">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="ｱｸｾｽ数" SortExpression="ACCESS_COUNT">
								<ItemTemplate>
									<asp:Label ID="lblAccessCount" runat="server" Text='<%# Eval("ACCESS_COUNT") %>'>
									</asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ｱｸｾｽ数(ﾕﾆｰｸ)" SortExpression="UNIQUE_ACCESS_COUNT">
								<ItemTemplate>
									<asp:Label ID="lblUniqueAccessCount" runat="server" Text='<%# Eval("UNIQUE_ACCESS_COUNT") %>'>
									</asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <br />
    </div>
    <asp:ObjectDataSource ID="dsProfAccessCountDaily" runat="server" SelectMethod="GetPageCollection"
        TypeName="ProfAccessCountDaily" SelectCountMethod="GetPageCount" EnablePaging="True"
        OnSelecting="dsProfAccessCountDaily_Selecting" OnSelected="dsProfAccessCountDaily_Selected">
        <SelectParameters>
            <asp:Parameter Name="pSearchCondition" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
</asp:Content>
