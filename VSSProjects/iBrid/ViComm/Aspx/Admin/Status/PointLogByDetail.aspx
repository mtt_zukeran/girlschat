<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PointLogByDetail.aspx.cs" Inherits="Status_PointLogByDetail"
	Title="通話明細" ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="通話明細"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 760px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイト
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="180px">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderStyle">
							課金種別
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstChargeType" runat="server" Width="140px" DataSourceID="dsChargeType" DataTextField="CHARGE_TYPE_NM" DataValueField="CHARGE_TYPE">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							利用日
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtReportDayFrom" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:DropDownList ID="lstReportTimeFrom" runat="server" Width="44px">
							</asp:DropDownList>
							〜
							<asp:TextBox ID="txtReportDayTo" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:DropDownList ID="lstReportTimeTo" runat="server" Width="44px">
							</asp:DropDownList>
							<asp:RequiredFieldValidator ID="vdrReportDayFrom" runat="server" ErrorMessage="利用日Fromを入力して下さい。" ControlToValidate="txtReportDayFrom" ValidationGroup="Key"
								Display="Dynamic">*</asp:RequiredFieldValidator>
							<asp:RangeValidator ID="vdeReportDayFrom" runat="server" ErrorMessage="利用日Fromを正しく入力して下さい。" ControlToValidate="txtReportDayFrom" MaximumValue="2099/12/31"
								MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							<asp:RangeValidator ID="vdeReportDayTo" runat="server" ErrorMessage="利用日Toを正しく入力して下さい。" ControlToValidate="txtReportDayTo" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
								Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							<asp:CompareValidator ID="vdcReportDay" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtReportDayFrom" ControlToValidate="txtReportDayTo"
								Operator="GreaterThanEqual" ValidationGroup="Key" Display="Dynamic">*</asp:CompareValidator>
						</td>
						<td class="tdHeaderStyle">
							Paging Off
						</td>
						<td class="tdDataStyle">
							<asp:CheckBox ID="chkPagingOff" runat="server" />
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							<%= DisplayWordUtil.Replace("出演者ＩＤ")%>
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtLoginId" runat="server" Width="70px" MaxLength="8"></asp:TextBox>
							<asp:Label ID="lblCharNo" runat="server" Text="ｷｬﾀｸﾀｰNo:"></asp:Label>
							<asp:TextBox ID="txtUserCharNo" runat="server" MaxLength="2" Width="24px"></asp:TextBox>
							<asp:RegularExpressionValidator ID="vdeUserCharNo" runat="server" ErrorMessage="ｷｬﾀｸﾀｰ番号は00〜99の間で入力してください" ControlToValidate="txtUserCharNo" ValidationGroup="Detail"
								ValidationExpression="(\d\d)">*</asp:RegularExpressionValidator>
							<asp:HyperLink ID="lnkCastNm" runat="server"></asp:HyperLink>
							<asp:CustomValidator ID="vdcLoginId" runat="server" ControlToValidate="txtLoginId" ErrorMessage="この出演者ＩＤは存在しません。" OnServerValidate="vdcLoginId_ServerValidate"
								ValidationGroup="Key">*</asp:CustomValidator>
						</td>
						<td class="tdHeaderStyle">
							<%= DisplayWordUtil.Replace("出演者電話番号") %>
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtTel" runat="server" Width="100px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							<%= DisplayWordUtil.Replace("会員ＩＤ")%>
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtManLoginId" runat="server" Width="70px" MaxLength="8"></asp:TextBox>
							<asp:HyperLink ID="lnkManNm" runat="server"></asp:HyperLink>
							<asp:CustomValidator ID="vdcManLoginId" runat="server" ControlToValidate="txtManLoginId" ErrorMessage="この会員ＩＤは存在しません。" OnServerValidate="vdcManLoginId_ServerValidate"
								ValidationGroup="Key">*</asp:CustomValidator>
						</td>
						<td class="tdHeaderStyle">
							通話状態表示
						</td>
						<td class="tdDataStyle">
							<asp:CheckBox ID="chkViewCallResult" runat="server" Text="通話の詳細ﾛｸﾞを表示する" />
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							通話種別
						</td>
						<td class="tdDataStyle">
							<asp:RadioButton ID="rdoTalkSubTypeAll" runat="server" GroupName="TalkType" Text="全て">
							</asp:RadioButton>
							<asp:RadioButton ID="rdoTalkSubTypeNormal" runat="server" GroupName="TalkType" Text="一般">
							</asp:RadioButton>
							<asp:RadioButton ID="rdoTalkSubTypeInvite" runat="server" GroupName="TalkType" Text="招待">
							</asp:RadioButton>
							<asp:RadioButton ID="rdoTalkSubTypeMeeting" runat="server" GroupName="TalkType" Text="待ち合せ">
							</asp:RadioButton>
						</td>
						<td class="tdHeaderStyle2">
							通話状態
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstCallResult" runat="server" DataSourceID="dsCodeDtl" DataTextField="CODE_NM" DataValueField="CODE" Width="140px">
							</asp:DropDownList>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" ValidationGroup="Key" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
				<asp:Button runat="server" ID="btnCSV" Text="CSV出力" CssClass="seekbutton" OnClick="btnCSV_Click" ValidationGroup="Key" />
			</asp:Panel>
		</fieldset>
		<fieldset>
			<legend>[ポイントログ]</legend>
			<asp:Panel runat="server" ID="pnlCount2">
				合計分数
				<%#GetTotalMin() %>
				分&nbsp;利用ポイント
				<%#GetTotalPoint() %>
				Pt.&nbsp;&nbsp;Free Dial利用ポイント<%#GetTotalPointFreeDial()%>Pt. &nbsp;&nbsp;（内招待分<%#GetTotalInvitePoint() %>
				Pt.）&nbsp;&nbsp;&nbsp;&nbsp;支払ポイント<%#GetTotalPaymentPoint()%>Pt.
			</asp:Panel>
			<asp:Panel ID="pnlGrid" runat="server">
				<asp:GridView ID="grdPointDetail" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsUsedLog" AllowSorting="True" Font-Size="X-Small"
					SkinID="GridViewColor">
					<HeaderStyle CssClass="Freezing" />
					<Columns>
						<asp:BoundField DataField="CHARGE_START_DATE" HeaderText="通話開始時間" DataFormatString="{0:MM/dd HH:mm:ss}" HtmlEncode="False" SortExpression="CHARGE_START_DATE">
							<ItemStyle HorizontalAlign="Center" />
						</asp:BoundField>
						<asp:BoundField DataField="CHARGE_END_DATE" HeaderText="通話終了時間" DataFormatString="{0:MM/dd HH:mm:ss}" HtmlEncode="False" SortExpression="CHARGE_END_DATE">
							<ItemStyle HorizontalAlign="Center" />
						</asp:BoundField>
						<asp:TemplateField HeaderText="会員ID" SortExpression="MAN_LOGIN_ID">
							<ItemTemplate>
								<asp:HyperLink ID="lnkManLoginId" runat="server" NavigateUrl='<%# string.Format("../Man/ManView.aspx?&site={0}&manloginid={1}",Eval("MAN_USER_SITE_CD"),Eval("MAN_LOGIN_ID")) %>'
									Text='<%# Eval("MAN_LOGIN_ID") %>' Enabled='<%# !UseOtherSysInfoFlag(Eval("USE_OTHER_SYS_INFO_FLAG")) %>'></asp:HyperLink>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Center" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="出演者ID" SortExpression="CAST_LOGIN_ID">
							<ItemTemplate>
								<asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl='<%# string.Format("~/Cast/CastView.aspx?loginid={0}&return={1}",Eval("CAST_LOGIN_ID"),"PointLogByDetail.aspx") %>'
									Text='<%# Eval("CAST_LOGIN_ID") %>'></asp:HyperLink>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Center" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="出演者ﾊﾝﾄﾞﾙ名" SortExpression="HANDLE_NM">
							<ItemStyle HorizontalAlign="Left" />
							<FooterStyle HorizontalAlign="Center" />
							<ItemTemplate>
								<asp:Label runat="server" Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>' ID="lblHandleNm" />
							</ItemTemplate>
						</asp:TemplateField>
						<asp:BoundField DataField="CHARGE_TYPE_NM" HeaderText="種別" SortExpression="CHARGE_TYPE">
							<ItemStyle HorizontalAlign="Left" />
							<FooterStyle HorizontalAlign="Center" />
						</asp:BoundField>
						<asp:BoundField DataField="CHARGE_SEC" HeaderText="秒数" DataFormatString="{0}秒 " SortExpression="CHARGE_SEC">
							<ItemStyle HorizontalAlign="Right" />
							<FooterStyle HorizontalAlign="Center" />
						</asp:BoundField>
						<asp:BoundField DataField="CALL_RESULT_NM" HeaderText="通話終了理由" SortExpression="CALL_RESULT_NM">
							<ItemStyle HorizontalAlign="Left" />
							<FooterStyle HorizontalAlign="Center" />
						</asp:BoundField>
						<asp:BoundField DataField="CHARGE_MIN" HeaderText="分数" DataFormatString="{0}分 " SortExpression="CHARGE_MIN">
							<ItemStyle HorizontalAlign="Right" />
							<FooterStyle HorizontalAlign="Center" />
						</asp:BoundField>
						<asp:BoundField DataField="CHARGE_POINT" HeaderText="Pt" DataFormatString="{0}Pt " SortExpression="CHARGE_POINT">
							<ItemStyle HorizontalAlign="Right" />
							<FooterStyle HorizontalAlign="Center" />
						</asp:BoundField>
						<asp:BoundField DataField="CHARGE_POINT_FREE_DIAL" HeaderText="Free" DataFormatString="{0}Pt " SortExpression="CHARGE_POINT_FREE_DIAL">
							<ItemStyle HorizontalAlign="Right" />
							<FooterStyle HorizontalAlign="Center" />
						</asp:BoundField>
						<asp:BoundField DataField="INVITE_TALK_SERVICE_POINT" HeaderText="招待Pt" DataFormatString="{0}Pt " SortExpression="INVITE_TALK_SERVICE_POINT">
							<ItemStyle HorizontalAlign="Right" />
							<FooterStyle HorizontalAlign="Center" />
						</asp:BoundField>
						<asp:BoundField DataField="PAYMENT_POINT" HeaderText="報酬Pt" DataFormatString="{0}Pt " SortExpression="PAYMENT_POINT">
							<ItemStyle HorizontalAlign="Right" />
							<FooterStyle HorizontalAlign="Center" />
						</asp:BoundField>
					</Columns>
					<PagerSettings Mode="NumericFirstLast" Position="Top" />
					<PagerStyle BorderStyle="None" />
				</asp:GridView>
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">Record Count
						<%# GetRecCount() %>
						&nbsp; </a><a class="reccount">Current viewing page
							<%=grdPointDetail.PageIndex + 1%>
							of
							<%=grdPointDetail.PageCount%>
						</a>
				</asp:Panel>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsUsedLog" runat="server" SelectMethod="GetPageCollection" TypeName="UsedLog" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsUsedLog_Selected" OnSelecting="dsUsedLog_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pSexCd" Type="String" />
			<asp:Parameter Name="pLoginId" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
            <asp:Parameter Name="pPartnerLoginId" Type="String" />
            <asp:Parameter Name="pPartnerUserCharNo" Type="String" />
            <asp:Parameter Name="pTel" Type="String" />
			<asp:Parameter Name="pReportDayFrom" Type="String" />
			<asp:Parameter Name="pReportDayTo" Type="String" />
			<asp:Parameter Name="pChargeType" Type="String" />
			<asp:Parameter Name="pReportTimeFrom" Type="String" />
			<asp:Parameter Name="pReportTimeTo" Type="String" />
			<asp:Parameter Name="pCallResult" Type="String" />
			<asp:Parameter Name="pTalkSubType" Type="int32" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsChargeType" runat="server" SelectMethod="GetList" TypeName="ChargeType"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCodeDtl" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="40" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtUserCharNo" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrReportDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdeReportDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdeReportDayTo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdcLoginId" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5" TargetControlID="vdeUserCharNo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="vdcReportDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:MaskedEditExtender ID="mskReportDayFrom" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="False"
		TargetControlID="txtReportDayFrom">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskReportDayTo" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="False"
		TargetControlID="txtReportDayTo">
	</ajaxToolkit:MaskedEditExtender>
</asp:Content>
