﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: メール別アクセス集計
--	Progaram ID		: AccessMailCountByMailTemplateList
--
--  Creation Date	: 2015.06.03
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.IO;
using iBridCommLib;
using ViComm;

public partial class Status_AccessMailCountByMailTemplateList:System.Web.UI.Page {
	private string[] Week = { "日","月","火","水","木","金","土" };
	private Stream filter;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SITE_CD"]);
		}
		set {
			this.ViewState["SITE_CD"] = value;
		}
	}

	private string Year {
		get {
			return iBridUtil.GetStringValue(this.ViewState["YEAR"]);
		}
		set {
			this.ViewState["YEAR"] = value;
		}
	}

	private string Month {
		get {
			return iBridUtil.GetStringValue(this.ViewState["MONTH"]);
		}
		set {
			this.ViewState["MONTH"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			grdAccessMailCountDaily.DataSourceID = "";
			DataBind();

			this.SiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);

			this.InitPage();
		}
	}

	private void InitPage() {
		lstSiteCd.DataBind();
		txtMailTemplateNo.Text = string.Empty;

		if (!IsPostBack) {
			SysPrograms.SetupFromToDay(lstFromYYYY,lstFromMM,lstFromDD,lstToYYYY,lstToMM,lstToDD,false);
		}

		lstFromYYYY.SelectedValue = DateTime.Now.AddDays(-8).ToString("yyyy");
		lstFromMM.SelectedValue = DateTime.Now.AddDays(-8).ToString("MM");
		lstFromDD.SelectedValue = DateTime.Now.AddDays(-8).ToString("dd");

		lstToYYYY.SelectedValue = DateTime.Now.AddDays(-1).ToString("yyyy");
		lstToMM.SelectedValue = DateTime.Now.AddDays(-1).ToString("MM");
		lstToDD.SelectedValue = DateTime.Now.AddDays(-1).ToString("dd");
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (this.IsValid) {
			GetList();
		}
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void btnCSV_Click(object sender,EventArgs e) {

		Response.Filter = filter;
		Response.AddHeader("Content-Disposition","attachment;filename=ACCESS_MAIL_COUNT_BY_MAIL_TEMPLATE.CSV");
		Response.ContentType = "application/octet-stream-dummy";
		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		DataSet ds;
		using (AccessMailCountDaily oAccessMailCountDaily = new AccessMailCountDaily()) {
			AccessMailCountDaily.SearchCondition oSearchCondition = new AccessMailCountDaily.SearchCondition();
			oSearchCondition.SiteCd = this.lstSiteCd.SelectedValue;
			oSearchCondition.FromYYYY = this.lstFromYYYY.SelectedValue;
			oSearchCondition.FromMM = this.lstFromMM.SelectedValue;
			oSearchCondition.FromDD = this.lstFromDD.SelectedValue;
			oSearchCondition.ToYYYY = this.lstToYYYY.SelectedValue;
			oSearchCondition.ToMM = this.lstToMM.SelectedValue;
			oSearchCondition.ToDD = this.lstToDD.SelectedValue;
			oSearchCondition.MailTemplateNo = this.txtMailTemplateNo.Text;

			ds = oAccessMailCountDaily.GetAccessCountByMailTemplate(oSearchCondition);
			SetCsvData(ds);
			Response.End();
		}
	}

	protected void dsAccessMailCountDaily_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {

		AccessMailCountDaily.SearchCondition oSearchCondition = new AccessMailCountDaily.SearchCondition();
		oSearchCondition.SiteCd = this.lstSiteCd.SelectedValue;
		oSearchCondition.FromYYYY = this.lstFromYYYY.SelectedValue;
		oSearchCondition.FromMM = this.lstFromMM.SelectedValue;
		oSearchCondition.FromDD = this.lstFromDD.SelectedValue;
		oSearchCondition.ToYYYY = this.lstToYYYY.SelectedValue;
		oSearchCondition.ToMM = this.lstToMM.SelectedValue;
		oSearchCondition.ToDD = this.lstToDD.SelectedValue;
		oSearchCondition.MailTemplateNo = this.txtMailTemplateNo.Text.Trim();
		e.InputParameters[0] = oSearchCondition;
	}

	public void SetCsvData(DataSet pDataSet) {

		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		string sHeaderString = "ﾃﾝﾌﾟﾚｰﾄNO,ﾃﾝﾌﾟﾚｰﾄ名称,送信数,ｱｸｾｽ総数,ﾕﾆｰｸ数,ｱｸｾｽ率\r\n";
		Response.BinaryWrite(encoding.GetBytes(sHeaderString));

		string sDtl = "";

		DataRow dr = null;
		for (int i = 0;i < pDataSet.Tables[0].Rows.Count;i++) {
			dr = pDataSet.Tables[0].Rows[i];

			sDtl = string.Format("{0},{1},{2},{3},{4},{5}",
							dr["MAIL_TEMPLATE_NO"].ToString(),
							dr["TEMPLATE_NM"].ToString(),
							dr["TX_MAIL_COUNT"].ToString(),
							dr["ACCESS_COUNT"].ToString(),
							dr["UNIQUE_ACCESS_COUNT"].ToString(),
							GetUniqueAccessRate(dr["TX_MAIL_COUNT"].ToString(),dr["UNIQUE_ACCESS_COUNT"].ToString())
							);
			sDtl += "\r\n";
			Response.BinaryWrite(encoding.GetBytes(sDtl));
		}
	}

	protected void grdAccessMailCountDaily_RowDataBound(object sender,GridViewRowEventArgs e) {

		if (e.Row.RowType == DataControlRowType.DataRow) {
		}
	}

	private void GetList() {

		grdAccessMailCountDaily.PageIndex = 0;
		grdAccessMailCountDaily.DataSourceID = "dsAccessMailCountDaily";
		DataBind();
	}

	private bool IsCorrectDate() {
		return true;
	}

	protected string GetUniqueAccessRate(string pTxMailCount,string pUniqueAccessCount) {
		double dTxMailCount;
		double dUniqueAccessCount;
		string sValue = "-";

		if (double.TryParse(pTxMailCount,out dTxMailCount) && double.TryParse(pUniqueAccessCount,out dUniqueAccessCount) && dTxMailCount > 0) {
			double dValue = dUniqueAccessCount / dTxMailCount * 100;

			dValue = Math.Round(dValue,2);
			sValue = string.Format("{0}%",dValue.ToString());
		}

		return sValue;
	}

	protected void vdcFrom_ServerValidate(object source,ServerValidateEventArgs args) {
		if (this.IsValid) {
			DateTime dtFrom;
			if (!DateTime.TryParse(string.Format("{0}/{1}/{2}",this.lstFromYYYY.SelectedValue,this.lstFromMM.SelectedValue,this.lstFromDD.SelectedValue),out dtFrom)) {
				args.IsValid = false;
			}
		}
	}

	protected void vdcTo_ServerValidate(object source,ServerValidateEventArgs args) {
		if (this.IsValid) {
			DateTime dtTo;
			if (!DateTime.TryParse(string.Format("{0}/{1}/{2}",this.lstToYYYY.SelectedValue,this.lstToMM.SelectedValue,this.lstToDD.SelectedValue),out dtTo)) {
				args.IsValid = false;
			}
		}
	}
}
