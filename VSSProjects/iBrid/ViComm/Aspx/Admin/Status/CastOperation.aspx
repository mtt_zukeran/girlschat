﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CastOperation.aspx.cs" Inherits="Status_CastOperation" Title="出演者稼動状況" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" Runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="出演者稼動状況"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" Runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 640px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイト
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							登録日時
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtRegistDayFrom" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:DropDownList ID="lstRegistTimeFrom" runat="server" Width="44px">
							</asp:DropDownList>
							～
							<asp:TextBox ID="txtRegistDayTo" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:DropDownList ID="lstRegistTimeTo" runat="server" Width="44px">
							</asp:DropDownList>
							<asp:RequiredFieldValidator ID="vdrRegistDayFrom" runat="server" ErrorMessage="登録日Fromを入力して下さい。" ControlToValidate="txtRegistDayFrom" ValidationGroup="Key"
								Display="Dynamic">*</asp:RequiredFieldValidator>
							<asp:RangeValidator ID="vdeRegistDayFrom" runat="server" ErrorMessage="登録日Fromを正しく入力して下さい。" ControlToValidate="txtRegistDayFrom" MaximumValue="2099/12/31"
								MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							<asp:RangeValidator ID="vdeRegistDayTo" runat="server" ErrorMessage="登録日Toを正しく入力して下さい。" ControlToValidate="txtRegistDayTo" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
								Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							<asp:CompareValidator ID="vdcRegistDay" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtRegistDayFrom" ControlToValidate="txtRegistDayTo"
								Operator="GreaterThanEqual" ValidationGroup="Key" Display="Dynamic">*</asp:CompareValidator>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							稼動日時
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtLoginDayFrom" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:DropDownList ID="lstLoginTimeFrom" runat="server" Width="44px">
							</asp:DropDownList>
							～
							<asp:TextBox ID="txtLoginDayTo" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:DropDownList ID="lstLoginTimeTo" runat="server" Width="44px">
							</asp:DropDownList>
							<asp:RequiredFieldValidator ID="vdrLoginDayFrom" runat="server" ErrorMessage="稼動日Fromを入力して下さい。" ControlToValidate="txtLoginDayFrom" ValidationGroup="Key"
								Display="Dynamic">*</asp:RequiredFieldValidator>
							<asp:RangeValidator ID="vdeLoginDayFrom" runat="server" ErrorMessage="稼動日Fromを正しく入力して下さい。" ControlToValidate="txtLoginDayFrom" MaximumValue="2099/12/31"
								MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							<asp:RangeValidator ID="vdeLoginDayTo" runat="server" ErrorMessage="稼動日Toを正しく入力して下さい。" ControlToValidate="txtLoginDayTo" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
								Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							<asp:CompareValidator ID="vdcLoginDay" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtLoginDayFrom" ControlToValidate="txtLoginDayTo"
								Operator="GreaterThanEqual" ValidationGroup="Key" Display="Dynamic">*</asp:CompareValidator>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
			</asp:Panel>
		</fieldset>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset>
				<legend>[稼動状況]</legend>
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="560px">
					<asp:GridView ID="grdCastOperation" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsCastOperation" AllowSorting="True" SkinID="GridViewColor" Width="450px" PageSize="12">
						<Columns>
							<asp:TemplateField HeaderText="ﾛｸﾞｲﾝID">
								<ItemTemplate>
									<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# string.Format("~/Cast/CastView.aspx?loginid={0}&return={1}",Eval("LOGIN_ID"),"CastOperation.aspx") %>'
										Text='<%# Eval("LOGIN_ID") %>'>
									</asp:HyperLink>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="稼動日数">
								<ItemTemplate>
									<asp:Label ID="Label7" runat="server" Text='<%#Eval("LOGIN_DAYS") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" Width="60px"/>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="獲得PT">
								<ItemTemplate>
									<asp:Label ID="Label8" runat="server" Text='<%#Eval("TOTAL_POINT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" Width="60px"/>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="認証状態">
								<ItemTemplate>
									<asp:Label ID="Label8" runat="server" Text='<%#Eval("USER_STATUS_NM") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" Width="80px"/>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="最終ﾛｸﾞｲﾝ">
								<ItemTemplate>
									<asp:Label ID="Label8" runat="server" Text='<%#CreateDateCountString(Eval("LAST_ACTION").ToString()) %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" Width="80px"/>
							</asp:TemplateField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsCastOperation" runat="server" SelectMethod="GetPageCollection" TypeName="CastOperation" SelectCountMethod="GetPageCount"
		EnablePaging="True" OnSelecting="dsCastOperation_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pRegistDayFrom" Type="String" />
			<asp:Parameter Name="pRegistDayTo" Type="String" />
			<asp:Parameter Name="pLoginDayFrom" Type="String" />
			<asp:Parameter Name="pLoginDayTo" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrRegistDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdeRegistDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdeRegistDayTo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdcRegistDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:MaskedEditExtender ID="mskRegistDayFrom" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="False"
		TargetControlID="txtRegistDayFrom">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskRegistDayTo" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="False"
		TargetControlID="txtRegistDayTo">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5" TargetControlID="vdrLoginDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="vdeLoginDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7" TargetControlID="vdeLoginDayTo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8" TargetControlID="vdcLoginDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:MaskedEditExtender ID="mskLoginDayFrom" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="False"
		TargetControlID="txtLoginDayFrom">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskLoginDayTo" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="False"
		TargetControlID="txtLoginDayTo">
	</ajaxToolkit:MaskedEditExtender>
</asp:Content>

