﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;

public partial class MasterPage:System.Web.UI.MasterPage {

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
		}
		form1.Attributes["onkeydown"] = "if(event.keyCode==13){if(window.event.srcElement.type!='submit' && window.event.srcElement.type!='textarea'){return false;}}";

		string[] sSplitUrl = Request.CurrentExecutionFilePath.Split('/');

		if (sSplitUrl[sSplitUrl.Length - 1].Equals("SipCommStatus.aspx")) {
			pnlTitle.Visible = false;
		}

		if (sSplitUrl[sSplitUrl.Length - 1].Equals("LoginTemplateMainte.aspx")) {
			lnkTopMenu.Visible = false;
		}

		if (sSplitUrl[sSplitUrl.Length - 1].Equals("MainView.aspx")) {
			lnkTopMenu.Text = "[全サイト表示]";
		}
	}

	protected void tmRefresh_Tick(object sender,EventArgs e) {
	}

	protected void lnkTopMenu_Click(object sender,EventArgs e) {
		Server.Transfer("~/Cast/MainView.aspx?refresh=1");
	}
}
