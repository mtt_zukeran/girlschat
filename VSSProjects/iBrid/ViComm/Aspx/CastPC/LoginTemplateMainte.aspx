<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="LoginTemplateMainte.aspx.cs" Inherits="LoginTemplateMainte"
	Title="ログインメール編集" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="ログインメール編集"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<asp:Panel runat="server" ID="pnlMainte">
		<asp:Label ID="lblHtmlDocSeq" runat="server" Text="" Visible="false"></asp:Label>
		<asp:Label ID="lblMailTemplateNo" runat="server" Text="" Visible="false"></asp:Label>
		<asp:TextBox ID="txtHtmlDocTitle" runat="server" Visible="false"></asp:TextBox>
		<fieldset class="fieldset">
			<legend>[設定]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<asp:Button runat="server" ID="btnTxMail2" Text="変更完了・ログイン通知送信" CssClass="seekbutton" OnClick="btnTxMail_Click" ValidationGroup="Detail" OnClientClick='return confirm("ログイン通知を送信しますか？");' />
			</asp:Panel>
			<br clear="all" />
			<asp:Panel runat="server" ID="pnlDtl">
				<fieldset class="fieldset">
					<legend>[文章編集]</legend>
					<table border="0" style="width: 640px" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle">
								タイトル
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtMailTitle" runat="server" MaxLength="60" Width="180px"></asp:TextBox>
								<asp:RequiredFieldValidator ID="vdrMailTitle" runat="server" ErrorMessage="メールタイトルを入力して下さい。" ControlToValidate="txtMailTitle" ValidationGroup="Detail"
									Display="Dynamic">*</asp:RequiredFieldValidator>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								文章
							</td>
							<td class="tdDataStyle">
								$NO_TRANS_START;
								<pin:pinEdit ID="txtHtmlDoc" runat="server" UserMode="HTML" DisplayStyle="Style5" DocumentView="True" HtmlMode="Body" Ruler="False" ReturnMode="BR" SpellMode="Inline"
									ToolbarConfiguration="ToolbarDesigner" Toolbar="T222324272829303536" FormatMode="Classic" BorderWidth="1px" Height="250px" Width="500px" DocumentWidth="230"
									Font-Bold="False" FontSizeType="px" RelativeBaseImageUrl="/" RelativeBaseLinkUrl="/" RelativeImageRoot="/">
								</pin:pinEdit>
								$NO_TRANS_END;
							</td>
						</tr>
					</table>
					<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" OnClientClick='return confirm("更新を行いますか？");' />
					<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" OnClientClick='return confirm("削除を行いますか？");' />
					<asp:Button runat="server" ID="btnCancel" Text="戻る" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
				</fieldset>
			</asp:Panel>
		</fieldset>
	</asp:Panel>
	<fieldset>
		<legend>[メールテンプレート一覧]</legend>
		<asp:GridView ID="grdTemplate" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsMailTemplate" AllowSorting="True" SkinID="GridView">
			<Columns>
				<asp:TemplateField>
					<ItemTemplate>
						<asp:Label ID="lblSiteCd" runat="server" Text='<%# Eval("SITE_CD") %>'></asp:Label>
						<asp:Label ID="lblSiteNM" runat="server" Text='<%# Eval("SITE_NM") %>'></asp:Label>
						<asp:Label ID="lblUserCharNo" runat="server" Text='<%# Eval("EDIT_USER_CHAR_NO") %>'></asp:Label>
					</ItemTemplate>
					<HeaderTemplate>
						サイト
					</HeaderTemplate>
					<ItemStyle HorizontalAlign="Left" />
				</asp:TemplateField>
				<asp:TemplateField>
					<ItemTemplate>
						<asp:LinkButton ID="lnkTemplate" runat="server" Text='<%# string.Format("{0}",Eval("MAIL_TITLE")) %>' CommandArgument='<%# string.Format("{0}:{1}:{2}:{3}",Eval("SITE_CD"),Eval("HTML_DOC_SEQ"),Eval("MAIL_TEMPLATE_NO"),Eval("USER_CHAR_NO"))  %>'
							OnCommand="lnkTemplate_Command" CausesValidation="False">
						</asp:LinkButton>
					</ItemTemplate>
					<HeaderTemplate>
						メールタイトル
					</HeaderTemplate>
				</asp:TemplateField>
			</Columns>
			<PagerSettings Mode="NumericFirstLast" />
		</asp:GridView>
	</fieldset>
	<br />
	<asp:Panel runat="server" ID="pnlHideValue" Visible="false">
		<asp:TextBox ID="txtDecomeTemplateNo" runat="server"></asp:TextBox>
	</asp:Panel>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSiteHtmlDoc" runat="server" SelectMethod="GetListByDocSeq" TypeName="SiteHtmlDoc" OnSelecting="dsSiteHtmlDoc_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pHtmlDocSeq" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsMailTemplate" runat="server" SelectMethod="GetPageCollection" TypeName="CastMailTemplate" SelectCountMethod="GetPageCount"
		EnablePaging="True" OnSelecting="dsMailTemplate_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
			<asp:Parameter Name="pMailTemplateType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>
