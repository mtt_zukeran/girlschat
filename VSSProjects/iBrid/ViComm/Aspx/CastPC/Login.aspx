﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" Title="管理画面" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Card Sericve Admin Login</title>
</head>
<body>
	<form id="form1" runat="server">
		<div id="container">
			<div id="header">
				<asp:Label ID="Label1" runat="server" Text="ViCOMM キャストログイン" CssClass="sysname"></asp:Label>
			</div>
			<br />
			<table align="center" cellpadding="0" cellspacing="0" style="height: 214px">
				<tr>
					<td style="width: 169px; height: 102px;">
						<asp:Login ID="ctlLogin" runat="server" BackColor="#F7F6F3" BorderColor="#E6E2D8" BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" Font-Size="0.8em" OnAuthenticate="ctlLogin_Authenticate" BorderPadding="4" DisplayRememberMe="False"
							ForeColor="#333333" Height="106px" InstructionText=" " TitleText="ViCOMMキャストログイン" Width="250px" PasswordLabelText="ﾊﾟｽﾜｰﾄﾞ:" UserNameLabelText="ﾛｸﾞｲﾝID:" UserNameRequiredErrorMessage="ﾛｸﾞｲﾝIDが必要です。" >
							<TitleTextStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" Font-Size="0.9em" />
							<InstructionTextStyle BorderStyle="None" Font-Italic="True" ForeColor="Black" />
							<TextBoxStyle Font-Size="0.8em" Width="100px" />
							<LoginButtonStyle BackColor="#FFFBFF" BorderColor="#CCCCCC" BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" Font-Size="0.8em" ForeColor="#284775" />
							<LabelStyle BorderWidth="0px" />
						</asp:Login>
						<asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label></td>
				</tr>
			</table>
		</div>
	</form>
</body>
</html>
