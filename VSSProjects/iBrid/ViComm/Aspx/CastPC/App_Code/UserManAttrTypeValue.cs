﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 男性会員属性値
--	Progaram ID		: UserManAttrTypeValue
--
--  Creation Date	: 2009.07.20
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;

public class UserManAttrTypeValue:DbSession {

	public UserManAttrTypeValue() {
	}



	public DataSet GetAttrNm(string pSiteCd, string pManAttrTypeSeq) {
		string sSql = "SELECT " +
						"MAN_ATTR_SEQ				," +
						"MAN_ATTR_NM				," +
						"PRIORITY					 " +
					  "FROM	" +
						"VW_USER_MAN_ATTR_TYPE_VALUE01		 " +
					  "WHERE " +
						"SITE_CD			=: SITE_CD AND			" +
						"MAN_ATTR_TYPE_SEQ	=: MAN_ATTR_TYPE_SEQ	" +
					  "ORDER BY " +
						"PRIORITY";

		DataSet ds;
		conn = DbConnect();
		ds = new DataSet();

		using (cmd = CreateSelectCommand(sSql,conn)) {
			cmd.Parameters.Add("SITE_CD",pSiteCd);
			cmd.Parameters.Add("MAN_ATTR_TYPE_SEQ",pManAttrTypeSeq);
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds);
			}
		}
		conn.Close();
		return ds;
	}
}

