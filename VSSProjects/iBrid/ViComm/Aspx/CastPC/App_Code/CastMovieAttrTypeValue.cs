﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 出演者動画属性種別値
--	Progaram ID		: CastMovieAttrTypeValue
--
--  Creation Date	: 2010.05.21
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using ViComm;
using iBridCommLib;

public class CastMovieAttrTypeValue:DbSession {

	public CastMovieAttrTypeValue() {
	}

	
	public DataSet GetList(string pSiteCd,string pCastMovieAttrTypeSeq) {
		DataSet ds;

		conn = DbConnect();
		ds = new DataSet();

		string sSql = "SELECT CAST_MOVIE_ATTR_SEQ,CAST_MOVIE_ATTR_NM FROM T_CAST_MOVIE_ATTR_TYPE_VALUE WHERE SITE_CD = :SITE_CD AND CAST_MOVIE_ATTR_TYPE_SEQ = :CAST_MOVIE_ATTR_TYPE_SEQ ";
		sSql = sSql + " ORDER BY SITE_CD,PRIORITY";

		using (cmd = CreateSelectCommand(sSql,conn)) {
			cmd.Parameters.Add("SITE_CD",pSiteCd);
			cmd.Parameters.Add("CAST_MOVIE_ATTR_TYPE_SEQ",pCastMovieAttrTypeSeq);
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds);
			}
		}
		conn.Close();
		return ds;
	}
}
