﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: 出演者・日記
--	Progaram ID		: CastDiary
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Data;
using System.Data.Common;


public class CastDiary:DbSession {
	public string year;
	public string month;
	public string day;
	public string diaryTitle;
	public string htmlDoc1;
	public string htmlDoc2;
	public string htmlDoc3;
	public string htmlDoc4;
	public string picSeq;
	public string reportDay;
	public string objPhotoImgPath;

	public CastDiary() {
	}

	public bool GetDiaryInfo(string pSiteCd,string pCastSeq,string pCastCharNo,string pReportDay) {
		DataSet ds;
		DataRow dr;

		bool bExist = false;

		conn = DbConnect();

		string sSql = "SELECT " +
							"REPORT_DAY		," +
							"DIARY_TITLE	," +
							"HTML_DOC1		," +
							"HTML_DOC2		," +
							"HTML_DOC3		," +
							"HTML_DOC4		," +
							"USER_CHAR_NO	," +    // add UCN
							"PIC_SEQ		," +
							"OBJ_PHOTO_IMG_PATH " +
						"FROM " +
							"VW_CAST_DIARY02 " +
						"WHERE " +
							"SITE_CD		= :SITE_CD		AND " +
							"USER_SEQ		= :USER_SEQ		AND " +
							"USER_CHAR_NO	= :USER_CHAR_NO	AND " +
							"REPORT_DAY		= :REPORT_DAY ";

		using (cmd = CreateSelectCommand(sSql,conn))
		using (ds = new DataSet()) {
			cmd.Parameters.Add("SITE_CD",pSiteCd);
			cmd.Parameters.Add("USER_SEQ",pCastSeq);
			cmd.Parameters.Add("USER_CHAR_NO",pCastCharNo);
			cmd.Parameters.Add("REPORT_DAY",pReportDay);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"VW_CAST_DIARY02");

				if (ds.Tables["VW_CAST_DIARY02"].Rows.Count != 0) {
					dr = ds.Tables["VW_CAST_DIARY02"].Rows[0];
					year = dr["REPORT_DAY"].ToString().Substring(0,4);
					month = dr["REPORT_DAY"].ToString().Substring(5,2);
					day = dr["REPORT_DAY"].ToString().Substring(8,2);
					diaryTitle = dr["DIARY_TITLE"].ToString();
					htmlDoc1 = dr["HTML_DOC1"].ToString();
					htmlDoc2 = dr["HTML_DOC2"].ToString();
					htmlDoc3 = dr["HTML_DOC3"].ToString();
					htmlDoc4 = dr["HTML_DOC4"].ToString();
					picSeq = dr["PIC_SEQ"].ToString();
					objPhotoImgPath = dr["OBJ_PHOTO_IMG_PATH"].ToString();
					bExist = true;
				}
			}
		}
		conn.Close();
		return bExist;
	}

	public DataSet GetDiaryList(string pSiteCd,string pCastSeq,string pCastCharNo,string pReportDayFrom,string pReportDayTo) {
		DataSet ds = new DataSet();

		conn = DbConnect();

		string sSql = "SELECT " +
							"REPORT_DAY		," +
							"DIARY_TITLE	," +
							"HTML_DOC1		," +
							"HTML_DOC2		," +
							"HTML_DOC3		," +
							"HTML_DOC4		," +
							"USER_CHAR_NO	," +
							"PIC_SEQ " +
						"FROM " +
							"T_CAST_DIARY " +
						"WHERE " +
							"SITE_CD		= :SITE_CD		AND " +
							"USER_SEQ		= :USER_SEQ		AND " +
							"USER_CHAR_NO	= :USER_CHAR_NO	AND " +
							"REPORT_DAY	   >= :REPORT_DAY_FROM AND " +
							"REPORT_DAY	   <= :REPORT_DAY_TO ";

		using (cmd = CreateSelectCommand(sSql,conn)) {
			cmd.Parameters.Add("SITE_CD",pSiteCd);
			cmd.Parameters.Add("USER_SEQ",pCastSeq);
			cmd.Parameters.Add("USER_CHAR_NO",pCastCharNo);
			cmd.Parameters.Add("REPORT_DAY_FROM",pReportDayFrom);
			cmd.Parameters.Add("REPORT_DAY_TO",pReportDayTo);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds);
			}
		}
		conn.Close();
		return ds;
	}
}
