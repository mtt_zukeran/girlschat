﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: アクセス集計
--	Progaram ID		: Access
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class Access : DbSession {

	public Access() {
	}

	public void AccessPage(string pSiteCd, string pProgramRoot, string pProgramId, string pAction, string pUserSeq) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("ACCESS_PAGE");
			// [in]
			db.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, pSiteCd);
			db.ProcedureInParm("pPROGRAM_ROOT", DbSession.DbType.VARCHAR2, pProgramRoot);
			db.ProcedureInParm("pPROGRAM_ID", DbSession.DbType.VARCHAR2, pProgramId);
			db.ProcedureInParm("pACTION", DbSession.DbType.VARCHAR2, pAction);
			db.ProcedureInParm("pUSER_SEQ", DbSession.DbType.VARCHAR2, pUserSeq);
			// [out]
			db.ProcedureOutParm("PSTATUS", DbSession.DbType.VARCHAR2);

			db.ExecuteProcedure();

			string sStatus = db.GetStringValue("PSTATUS");

		}
	}
}
