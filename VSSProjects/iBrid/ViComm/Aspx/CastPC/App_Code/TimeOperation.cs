﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: 時間別稼動集計
--	Progaram ID		: TimeOperation
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using iBridCommLib;

public class TimeOperation:DbSession {

	public TimeOperation() {
	}

	public DataSet GetList(
		string pFromYYYY,
		string pFromMM,
		string pFromDD,
		string pFromHH,
		string pToYYYY,
		string pToMM,
		string pToDD,
		string pToHH,
		string pUserSeq
	) {
		DataSet ds;

		conn = DbConnect();
		ds = new DataSet();

		string sFromDay = pFromYYYY + pFromMM + pFromDD + pFromHH;
		string sToDay = pToYYYY + pToMM + pToDD + pToHH;
        // 当処理において、GROUP BYの対象にUSER_CHAR_NOを追加する必要はない。
		string sSql = "SELECT " +
						"T_SITE.SITE_NM				," +
						"T_SUM.SITE_CD				," +
						"T_SUM.USER_SEQ				," +
						"T_SUM.PRV_TV_TALK_MIN		," +
						"T_SUM.PUB_TV_TALK_MIN		," +
						"T_SUM.PRV_VOICE_TALK_MIN	," +
						"T_SUM.PUB_VOICE_TALK_MIN	," +
						"T_SUM.VIEW_TALK_MIN		," +
						"T_SUM.VIEW_BROADCAST_MIN	," +
						"T_SUM.WIRETAP_MIN			," +
						"T_SUM.MOVIE_MIN			," +
						"T_SUM.USER_MAIL_COUNT		," +
						"T_SUM.TALK_MIN " +
					"FROM " +
						"T_SITE,(" +
							"SELECT " +
								"SITE_CD	," +
								"USER_SEQ	," +
								"SUM(PRV_TV_TALK_MIN	- INV_PRV_TV_TALK_MIN		) AS PRV_TV_TALK_MIN	," +
								"SUM(PUB_TV_TALK_MIN	- INV_PUB_TV_TALK_MIN		) AS PUB_TV_TALK_MIN	," +
								"SUM(PRV_VOICE_TALK_MIN	- INV_PRV_VOICE_TALK_MIN	) AS PRV_VOICE_TALK_MIN	," +
								"SUM(PUB_VOICE_TALK_MIN	- INV_PUB_VOICE_TALK_MIN	) AS PUB_VOICE_TALK_MIN	," +
								"SUM(VIEW_TALK_MIN		- INV_VIEW_TALK_MIN			) AS VIEW_TALK_MIN		," +
								"SUM(VIEW_BROADCAST_MIN	- INV_VIEW_BROADCAST_MIN	) AS VIEW_BROADCAST_MIN	," +
								"SUM(WIRETAP_MIN		- INV_WIRETAP_MIN			) AS WIRETAP_MIN		," +
								"SUM(MOVIE_MIN			- INV_MOVIE_MIN				) AS MOVIE_MIN			," +
								"SUM(USER_MAIL_COUNT) AS USER_MAIL_COUNT," +
								"SUM(PRV_TV_TALK_MIN + PUB_TV_TALK_MIN + PRV_VOICE_TALK_MIN + PUB_VOICE_TALK_MIN + VIEW_TALK_MIN + VIEW_BROADCAST_MIN + WIRETAP_MIN - INV_PRV_TV_TALK_MIN - INV_PUB_TV_TALK_MIN - INV_PRV_VOICE_TALK_MIN - INV_PUB_VOICE_TALK_MIN - INV_VIEW_TALK_MIN - INV_VIEW_BROADCAST_MIN - INV_WIRETAP_MIN) AS TALK_MIN " +
							"FROM " +
								"VW_TIME_OPERATION01 " +
							"WHERE " +
								"REPORT_DAY_TIME  >= :REPORT_DAY_TIME_FROM  AND REPORT_DAY_TIME  <= :REPORT_DAY_TIME_TO AND USER_SEQ = :USER_SEQ " +
							"GROUP BY " +
								"SITE_CD, " +
								"USER_SEQ " +
							"ORDER BY " +
								"SITE_CD, " +
								"USER_SEQ" +
							") T_SUM " +
					"WHERE " +
						"T_SITE.SITE_CD = T_SUM.SITE_CD  " +
					"ORDER BY SITE_CD ";

		using (cmd = CreateSelectCommand(sSql,conn)) {
			cmd.Parameters.Add("REPORT_DATE_FROM",sFromDay);
			cmd.Parameters.Add("REPORT_DAY_TIME_TO",sToDay);
			cmd.Parameters.Add("USER_SEQ",pUserSeq);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds);
			}
		}

		conn.Close();

		return ds;
	}
}
