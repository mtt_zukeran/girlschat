﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public class SessionObjs {
	public Site site;
	public Cast cast;

	public SessionObjs() {
		site = new Site();
		cast = new Cast();
	}
}
