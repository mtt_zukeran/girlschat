﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 男性会員属性値
--	Progaram ID		: UserManAttrValue
--
--  Creation Date	: 2009.07.17
--  Creater			: i-Brid(Y.Inoue)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;

public class UserManAttrType:DbSession {

	public UserManAttrType() {
	}



	public DataSet GetInqManAttrType(string pSiteCd) {
		string sSql = "SELECT " +
						"MAN_ATTR_TYPE_SEQ1			,"	+
						"MAN_ATTR_TYPE_SEQ2			,"	+
						"MAN_ATTR_TYPE_SEQ3			,"	+
						"MAN_ATTR_TYPE_SEQ4			,"	+
						"MAN_ATTR_TYPE_NM1			,"	+
						"MAN_ATTR_TYPE_NM2	  		,"	+
						"MAN_ATTR_TYPE_NM3			,"	+
						"MAN_ATTR_TYPE_NM4			,"	+
						"GROUPING_CATEGORY_CD1		,"	+
						"GROUPING_CATEGORY_CD2		,"	+
						"GROUPING_CATEGORY_CD3		,"	+
						"GROUPING_CATEGORY_CD4		"	+
					  "FROM	"							+
					    "VW_USER_MAN_ATTR_TYPE02	 "	+
					  "WHERE "							+
						"SITE_CD =: SITE_CD";
					  
		DataSet ds;
		conn = DbConnect();
		ds = new DataSet();

		using (cmd = CreateSelectCommand(sSql,conn)) {
			cmd.Parameters.Add("SITE_CD",pSiteCd);
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds);
			}
		}
		conn.Close();
		return ds;
	}
}

