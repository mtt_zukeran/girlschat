﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: サイト運営会社

--	Progaram ID		: ManageCompany
--
--  Creation Date	: 2009.09.03
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class ManageCompany:DbSession {


	public ManageCompany() {
	}

	public string manageCompanyNm;
	public string tel;
	public string managePersonNm;
	public string emailAddress;
	public int settleCompanyMask;
	public string adManageHost;
	public int companyMask;

	public bool GetOne() {
		DataSet ds;
		DataRow dr;

		bool bExist = false;

		conn = DbConnect();

		string sSql = " SELECT " +
						" MANAGE_COMPANY_NM		," +
						" TEL					," +
						" MANAGE_PERSON_NM		," +
						" EMAIL_ADDR			," +
						" SETTLE_COMPANY_MASK	," +
						" AD_MANAGE_HOST		," +
						" COMPANY_MASK			" +
					" FROM " +
						" T_MANAGE_COMPANY ";

		using (cmd = CreateSelectCommand(sSql,conn))
		using (ds = new DataSet()) {
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"T_MANAGE_COMPANY");

				if (ds.Tables["T_MANAGE_COMPANY"].Rows.Count != 0) {
					dr = ds.Tables["T_MANAGE_COMPANY"].Rows[0];
					manageCompanyNm = iBridUtil.GetStringValue(dr["MANAGE_COMPANY_NM"]);
					tel = iBridUtil.GetStringValue(dr["TEL"]);
					managePersonNm = iBridUtil.GetStringValue(dr["MANAGE_PERSON_NM"]);
					emailAddress = iBridUtil.GetStringValue(dr["EMAIL_ADDR"]);
					settleCompanyMask = int.Parse(iBridUtil.GetStringValue(dr["SETTLE_COMPANY_MASK"]));
					adManageHost = iBridUtil.GetStringValue(dr["AD_MANAGE_HOST"]);
					companyMask = int.Parse(iBridUtil.GetStringValue(dr["COMPANY_MASK"]));
					bExist = true;
				}
			}
		}
		conn.Close();
		return bExist;
	}

	public bool IsAvailableService(ulong pService) {
		DataSet ds;
		DataRow dr;
		bool bOk = false;

		conn = DbConnect();
		ulong uMask = 0;
		string sSql = " SELECT " +
						" RELEASE_PROGRAM_MASK	" +
					" FROM " +
						" T_MANAGE_COMPANY ";

		using (cmd = CreateSelectCommand(sSql,conn))
		using (ds = new DataSet()) {
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds, "T_MANAGE_COMPANY");

				if (ds.Tables["T_MANAGE_COMPANY"].Rows.Count != 0) {
					dr = ds.Tables["T_MANAGE_COMPANY"].Rows[0];
					uMask = ulong.Parse(iBridUtil.GetStringValue(dr["RELEASE_PROGRAM_MASK"]));
					bOk = ((pService & uMask) > 0);
				}
			}
		}
		conn.Close();
		return bOk;
	}
}
