﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Admin
--	Title			: 出演者写真
--	Progaram ID		: CastPic
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  2009/08/10  iBrid(Y.Inoue)

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class CastPic:DbSession {

	public string siteCd;
	public string loginId;
	public string userSeq;
	public string userCharNo;
	public string picSeq;
	public string picTitle;
	public int objNotApproveFlag;
	public int picType;

	public CastPic() {
	}

	public int GetPageCount(string pSiteCd,string pUserSeq,string pUserCharNo,string pPicType) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;

		conn = DbConnect();

		string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_CAST_PIC01 ";

		string sWhere = "";
		OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,pPicType,ref sWhere);
		sSql = sSql + sWhere;

		using (cmd = CreateSelectCommand(sSql,conn))
		using (da = new OracleDataAdapter(cmd))
		using (ds = new DataSet()) {
			for (int i = 0;i < objParms.Length;i++) {
				cmd.Parameters.Add((OracleParameter)objParms[i]);
			}

			da.Fill(ds);
			if (ds.Tables[0].Rows.Count != 0) {
				dr = ds.Tables[0].Rows[0];
				iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
			}
		}

		conn.Close();
		return iPageCount;
	}


	public DataSet GetPageCollection(string pSiteCd,string pUserSeq,string pUserCharNo,string pPicType,int startRowIndex,int maximumRows) {
		DataSet ds;
		conn = DbConnect();
		ds = new DataSet();

		string sOrder = "ORDER BY SITE_CD,USER_SEQ,USER_CHAR_NO,PIC_SEQ DESC ";
		string sSql = "SELECT " +
							"SITE_CD					," +
							"USER_SEQ					," +
							"USER_CHAR_NO	        	," +
							"LOGIN_ID					," +
							"PIC_SEQ					," +
							"PIC_TITLE					," +
							"PIC_DOC					," +
							"OBJ_SMALL_PHOTO_IMG_PATH	," +
							"OBJ_NOT_APPROVE_FLAG		," +
							"NOT_APPROVE_MARK				," +
							"UPLOAD_DATE " +
						"FROM(" +
							"SELECT " +
								"VW_CAST_PIC01.*," +
								"ROW_NUMBER() OVER (" + sOrder + ") AS RNUM " +
							"FROM " +
								"VW_CAST_PIC01  ";

		string sWhere = "";
		OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,pPicType,ref sWhere);
		sSql = sSql + sWhere;

		sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
		sSql = sSql + sOrder;

		using (cmd = CreateSelectCommand(sSql,conn)) {
			for (int i = 0;i < objParms.Length;i++) {
				cmd.Parameters.Add((OracleParameter)objParms[i]);
			}

			cmd.Parameters.Add("FIRST_ROW",startRowIndex);
			cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"VW_CAST_PIC01");
			}
		}
		conn.Close();
		return ds;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pUserSeq,string pUserCharNo,string pPicType,ref string pWhere) {
		ArrayList list = new ArrayList();

		if (!pSiteCd.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if (!pUserSeq.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref pWhere);
			list.Add(new OracleParameter("USER_SEQ",pUserSeq));

			if (!string.IsNullOrEmpty(pUserCharNo)) {
				iBridCommLib.SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref pWhere);
				list.Add(new OracleParameter("USER_CHAR_NO",pUserCharNo));
			}
		}

		if (!pPicType.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("PIC_TYPE = :PIC_TYPE",ref pWhere);
			list.Add(new OracleParameter("PIC_TYPE",pPicType));
		}

		iBridCommLib.SysPrograms.SqlAppendWhere("(OBJ_NOT_APPROVE_FLAG IN (:OBJ_NOT_APPROVE_FLAG0,:OBJ_NOT_APPROVE_FLAG1))",ref pWhere);
		list.Add(new OracleParameter("OBJ_NOT_APPROVE_FLAG0",ViCommConst.FLAG_OFF));
		list.Add(new OracleParameter("OBJ_NOT_APPROVE_FLAG1",ViCommConst.FLAG_ON));

		iBridCommLib.SysPrograms.SqlAppendWhere("OBJ_NOT_PUBLISH_FLAG = :OBJ_NOT_PUBLISH_FLAG",ref pWhere);
		list.Add(new OracleParameter("OBJ_NOT_PUBLISH_FLAG",ViCommConst.FLAG_OFF));

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public DataSet GetList(string pSiteCd,string pUserSeq,string pUserCharNo,string pPicType) {
		DataSet ds;
		DataColumn col;

		conn = DbConnect();
		ds = new DataSet();

		string sSql = "SELECT " +
							"PIC_SEQ			," +
							"OBJ_PHOTO_IMG_PATH	," +
							"PROFILE_PIC_FLAG " +
						"FROM " +
							"VW_CAST_PIC01 " +
						"WHERE " +
							"SITE_CD				=:SITE_CD		AND " +
							"USER_SEQ				=:USER_SEQ		AND " +
							"USER_CHAR_NO			=:USER_CHAR_NO	AND " +
							"PIC_TYPE				=:PIC_TYPE		AND " +
							"OBJ_NOT_PUBLISH_FLAG	= 0		" +
						"ORDER BY SITE_CD,USER_SEQ,USER_CHAR_NO,DISPLAY_POSITION,PIC_SEQ DESC ";

		using (cmd = CreateSelectCommand(sSql,conn)) {
			cmd.Parameters.Add("SITE_CD",pSiteCd);
			cmd.Parameters.Add("USER_SEQ",pUserSeq);
			cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
			cmd.Parameters.Add("PIC_TYPE",pPicType);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"VW_CAST_PIC01");
			}
		}

		conn.Close();

		if (ds.Tables[0].Rows.Count > 0) {
			col = new DataColumn("UP_BTN_FLAG",System.Type.GetType("System.Boolean"));
			ds.Tables[0].Columns.Add(col);

			col = new DataColumn("DOWN_BTN_FLAG",System.Type.GetType("System.Boolean"));
			ds.Tables[0].Columns.Add(col);

			foreach (DataRow dr in ds.Tables[0].Rows) {
				dr["UP_BTN_FLAG"] = true;
				dr["DOWN_BTN_FLAG"] = true;
			}
			DataRow drFisrt = ds.Tables[0].Rows[0];
			drFisrt["UP_BTN_FLAG"] = false;

			DataRow drLast = ds.Tables[0].Rows[ds.Tables[0].Rows.Count - 1];
			drLast["DOWN_BTN_FLAG"] = false;
		}
		return ds;
	}

	public decimal GetPicNo() {

		decimal dPicNo = 0;
		conn = DbConnect();

		string sSql = "SELECT SEQ_OBJ.NEXTVAL FROM DUAL";

		using (cmd = CreateSelectCommand(sSql,conn)) {
			dPicNo = (decimal)cmd.ExecuteScalar();
		}
		conn.Close();

		return dPicNo;
	}

	public bool GetOne(string pSiteCd,string pUserSeq,string pUserCharNo,string pPicSeq) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;

		conn = DbConnect();

		string sSql = "SELECT " +
						"SITE_CD,USER_SEQ,USER_CHAR_NO,LOGIN_ID,PIC_SEQ,PIC_TITLE,OBJ_NOT_APPROVE_FLAG,PIC_TYPE " +
										"FROM VW_CAST_PIC01 WHERE " +
										"SITE_CD		=: SITE_CD		AND " +
										"USER_SEQ		=: USER_SEQ		AND " +
										"USER_CHAR_NO	=: USER_CHAR_NO	AND " +
										"PIC_SEQ		=: PIC_SEQ ";
		using (cmd = CreateSelectCommand(sSql,conn))
		using (ds = new DataSet()) {
			cmd.Parameters.Add("SITE_CD",pSiteCd);
			cmd.Parameters.Add("USER_SEQ",pUserSeq);
			cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
			cmd.Parameters.Add("PIC_SEQ",pPicSeq);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"VW_CAST_PIC01");
				if (ds.Tables["VW_CAST_PIC01"].Rows.Count != 0) {
					dr = ds.Tables["VW_CAST_PIC01"].Rows[0];
					siteCd = dr["SITE_CD"].ToString();
					userSeq = dr["USER_SEQ"].ToString();
					userCharNo = dr["USER_CHAR_NO"].ToString();
					loginId = dr["LOGIN_ID"].ToString();
					picSeq = dr["PIC_SEQ"].ToString();
					picTitle = dr["PIC_TITLE"].ToString();
					objNotApproveFlag = int.Parse(dr["OBJ_NOT_APPROVE_FLAG"].ToString());
					picType = int.Parse(dr["PIC_TYPE"].ToString());
					bExist = true;
				}
			}
		}
		conn.Close();
		return bExist;
	}
}
