﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 男性会員属性値
--	Progaram ID		: UserManAttrValue
--
--  Creation Date	: 2009.07.17
--  Creater			: i-Brid(Y.Inoue)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;

public class UserManAttrValue:DbSession {

	public UserManAttrValue() {
	}


	public DataSet GetList(string pSiteCd,string pUserSeq,string pUserCharNo) {

		DataSet ds = new DataSet();
		string sSql;

		conn = DbConnect("UserManAttrTypeValue.GetList");

		sSql = "SELECT " +
					"MAN_ATTR_TYPE_SEQ		," +
					"MAN_ATTR_TYPE_NM		," +
					"MAN_ATTR_SEQ			," +
					"MAN_ATTR_NM			," +
					"DISPLAY_VALUE			," +
					"INPUT_TYPE				," +
					"PRIORITY				," +
					"ROW_COUNT				," +
					"ITEM_NO				" +
				"FROM " +
					"VW_USER_MAN_ATTR_VALUE01 " +
				"WHERE " +
					"SITE_CD		= :SITE_CD		AND	" +
					"USER_SEQ		= :USER_SEQ		AND	" +
					"USER_CHAR_NO	= :USER_CHAR_NO	AND	" +
					"NA_FLAG		= 0 " +
				"ORDER BY " +
					"SITE_CD		," +
					"USER_SEQ		," +
					"USER_CHAR_NO   ," +
					"PRIORITY ";

		using (cmd = CreateSelectCommand(sSql,conn)) {
			cmd.Parameters.Add("SITE_CD",pSiteCd);
			cmd.Parameters.Add("USER_SEQ",pUserSeq);
			cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds);
			}
		}
		conn.Close();
		return ds;
	}



	public string GetAttrValue(string pSiteCd,string pUserSeq,string pUserCharNo,string pManAttrTypeSeq) {
		DataSet ds;
		DataRow dr;
		string sResult = "";

		if (string.IsNullOrEmpty(pManAttrTypeSeq)) {
			return "";
		}

		conn = DbConnect("UserManAttrValue.GetAttrValue");

		using (cmd = CreateSelectCommand("SELECT DISPLAY_VALUE FROM VW_USER_MAN_ATTR_VALUE01 " +
										"WHERE " +
											"SITE_CD			=:SITE_CD		AND " +
											"USER_SEQ			=:USER_SEQ		AND " +
											"USER_CHAR_NO		=:USER_CHAR_NO	AND " +
											"MAN_ATTR_TYPE_SEQ	=:MAN_ATTR_TYPE_SEQ",conn))
		using (ds = new DataSet()) {
			cmd.Parameters.Add("SITE_CD",pSiteCd);
			cmd.Parameters.Add("USER_SEQ",pUserSeq);
			cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
			cmd.Parameters.Add("MAN_ATTR_TYPE_SEQ",pManAttrTypeSeq);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"VW_USER_MAN_ATTR_VALUE01");
				if (ds.Tables["VW_USER_MAN_ATTR_VALUE01"].Rows.Count != 0) {
					dr = ds.Tables["VW_USER_MAN_ATTR_VALUE01"].Rows[0];
					sResult = dr["DISPLAY_VALUE"].ToString();
				}
			}
		}
		conn.Close();
		return sResult;
	}
}