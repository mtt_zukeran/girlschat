/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: メールテンプレート
--	Progaram ID		: MailTemplate
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class MailTemplate:DbSession {

	public string htmlDoc;
	public string mailTitle;
	public string mailTemplateType;
	public string mailAttachedObjType;

	public MailTemplate() {
		htmlDoc = "";
		mailTitle = "";
		mailTemplateType = "";
		mailAttachedObjType = "";
	}

	public DataSet GetCastCreateOriginal(string pSiteCd) {
		DataSet ds;

		conn = DbConnect();
		ds = new DataSet();

		string sSql = "SELECT " +
							"MAIL_TEMPLATE_NO	," +
							"MAIL_TITLE			" +
						"FROM " +
							"VW_MAIL_TEMPLATE01 " +
						"WHERE " +
							"SITE_CD =:SITE_CD AND CAST_CREATE_ORG_SUB_SEQ != 0 AND CAST_CREATE_ORG_SUB_SEQ = HTML_DOC_SUB_SEQ " +
						"ORDER BY SITE_CD,MAIL_TEMPLATE_NO DESC ";

		using (cmd = CreateSelectCommand(sSql,conn)) {
			cmd.Parameters.Add("SITE_CD",pSiteCd);
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"VW_MAIL_TEMPLATE01");
			}
		}
		conn.Close();
		return ds;
	}

	public DataSet GetCastMailList(string pSiteCd) {
		DataSet ds;

		conn = DbConnect();
		ds = new DataSet();

		string sSql = "SELECT " +
							"MAIL_TEMPLATE_NO	," +
							"TEMPLATE_NM		" +
						"FROM " +
							"T_MAIL_TEMPLATE " +
						"WHERE " +
							"SITE_CD =:SITE_CD AND MAIL_TEMPLATE_TYPE IN (:TYPE1,:TYPE2,:TYPE3,:TYPE4) " +
						"ORDER BY SITE_CD,MAIL_TEMPLATE_TYPE,MAIL_TEMPLATE_NO ";

		using (cmd = CreateSelectCommand(sSql,conn)) {
			cmd.Parameters.Add("SITE_CD",pSiteCd);
			cmd.Parameters.Add("TYPE1",ViCommConst.MAIL_TP_APPROACH_TO_USER);
			cmd.Parameters.Add("TYPE2",ViCommConst.MAIL_TP_INVITE_TALK);
			cmd.Parameters.Add("TYPE3",ViCommConst.MAIL_TP_NOTIFY_LOGOUT);
			cmd.Parameters.Add("TYPE4",ViCommConst.MAIL_TP_RETURN_TO_USER);
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"T_MAIL_TEMPLATE");
			}
		}
		conn.Close();
		return ds;
	}

	public string GetModifiableSubSeq(string pSiteCd,string pMailTemplateNo) {
		string sSubSeq = "";
		DataSet ds;
		DataRow dr;

		conn = DbConnect();

		string sSql = "SELECT " +
						"CAST_CREATE_ORG_SUB_SEQ " +
					"FROM " +
						"T_MAIL_TEMPLATE " +
					"WHERE " +
						"SITE_CD = :SITE_CD AND MAIL_TEMPLATE_NO = :MAIL_TEMPLATE_NO";

		using (cmd = CreateSelectCommand(sSql,conn))
		using (ds = new DataSet()) {
			cmd.Parameters.Add("SITE_CD",pSiteCd);
			cmd.Parameters.Add("MAIL_TEMPLATE_NO",pMailTemplateNo);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"T_MAIL_TEMPLATE");

				if (ds.Tables["T_MAIL_TEMPLATE"].Rows.Count != 0) {
					dr = ds.Tables["T_MAIL_TEMPLATE"].Rows[0];
					sSubSeq = dr["CAST_CREATE_ORG_SUB_SEQ"].ToString();
				}
			}
		}
		conn.Close();
		return sSubSeq;
	}

	public bool GetOne(string pSiteCd,string pMailTemplateNo) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;

		conn = DbConnect();

		string sSql = "SELECT " +
							"MAIL_TITLE," +
							"MAIL_TEMPLATE_TYPE," +
							"MAIL_ATTACHED_OBJ_TYPE," +
							"HTML_DOC1," +
							"HTML_DOC2," +
							"HTML_DOC3," +
							"HTML_DOC4," +
							"HTML_DOC5," +
							"HTML_DOC6," +
							"HTML_DOC7," +
							"HTML_DOC8," +
							"HTML_DOC9," +
							"HTML_DOC10	 " +
						 "FROM VW_MAIL_TEMPLATE01 " +
							"WHERE " +
								"SITE_CD				=:SITE_CD			AND " +
								"MAIL_TEMPLATE_NO		=:MAIL_TEMPLATE_NO	AND " +
								"CAST_CREATE_ORG_SUB_SEQ=HTML_DOC_SUB_SEQ   ";

		using (cmd = CreateSelectCommand(sSql,conn))
		using (ds = new DataSet()) {
			cmd.Parameters.Add("SITE_CD",pSiteCd);
			cmd.Parameters.Add("MAIL_TEMPLATE_NO",pMailTemplateNo);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"VW_MAIL_TEMPLATE01");
				if (ds.Tables["VW_MAIL_TEMPLATE01"].Rows.Count != 0) {
					dr = ds.Tables["VW_MAIL_TEMPLATE01"].Rows[0];
					mailTitle = dr["MAIL_TITLE"].ToString();
					htmlDoc = dr["HTML_DOC1"].ToString() +
								dr["HTML_DOC2"].ToString() +
								dr["HTML_DOC3"].ToString() +
								dr["HTML_DOC4"].ToString() +
								dr["HTML_DOC5"].ToString() +
								dr["HTML_DOC6"].ToString() +
								dr["HTML_DOC7"].ToString() +
								dr["HTML_DOC8"].ToString() +
								dr["HTML_DOC9"].ToString() +
								dr["HTML_DOC10"].ToString();
					mailTemplateType = dr["MAIL_TEMPLATE_TYPE"].ToString();
					mailAttachedObjType = dr["MAIL_ATTACHED_OBJ_TYPE"].ToString();
					
					bExist = true;
				}
			}
		}
		conn.Close();
		return bExist;
	}

	public DataSet GetListByTemplateType(string pSiteCd,string pMailTemplateType) {
		DataSet ds;

		conn = DbConnect();
		ds = new DataSet();

		string sSql = "SELECT MAIL_TEMPLATE_NO,TEMPLATE_NM FROM T_MAIL_TEMPLATE WHERE SITE_CD = :SITE_CD  AND MAIL_TEMPLATE_TYPE = :MAIL_TEMPLATE_TYPE";
		sSql = sSql + " ORDER BY SITE_CD,MAIL_TEMPLATE_NO";

		using (cmd = CreateSelectCommand(sSql,conn)) {
			cmd.Parameters.Add("SITE_CD",pSiteCd);
			cmd.Parameters.Add("MAIL_TEMPLATE_TYPE",pMailTemplateType);
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds);
			}
		}
		conn.Close();
		return ds;
	}
}
