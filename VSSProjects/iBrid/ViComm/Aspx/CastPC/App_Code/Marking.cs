﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: 足あと
--	Progaram ID		: Marking
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class Marking:DbSession {
	public string refuseType;

	public Marking() {
		refuseType = "";
	}

	public int GetPageCount(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerUserSeq,string pPartnerUserCharNo) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;

		conn = DbConnect();

		string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_MARKING02 ";

		string sWhere = "";
		OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,pPartnerUserSeq,pPartnerUserCharNo,ref sWhere);
		sSql = sSql + sWhere;

		using (cmd = CreateSelectCommand(sSql,conn))
		using (da = new OracleDataAdapter(cmd))
		using (ds = new DataSet()) {
			for (int i = 0;i < objParms.Length;i++) {
				cmd.Parameters.Add((OracleParameter)objParms[i]);
			}

			da.Fill(ds);
			if (ds.Tables[0].Rows.Count != 0) {
				dr = ds.Tables[0].Rows[0];
				iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
			}
		}

		conn.Close();
		return iPageCount;
	}


	public DataSet GetPageCollection(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerUserSeq,string pPartnerUserCharNo,int startRowIndex,int maximumRows) {
		DataSet ds;
		conn = DbConnect();
		ds = new DataSet();

		string sOrder = "ORDER BY SITE_CD,PARTNER_USER_SEQ,USER_CHAR_NO,MARKING_DATE DESC ";
		string sSql = "SELECT " +
							"SITE_CD				," +
							"USER_SEQ				," +
							"USER_CHAR_NO			," +
							"PARTNER_USER_SEQ		," +
							"PARTNER_USER_CHAR_NO	," +
							"BAL_POINT				," +
							"CHARACTER_ONLINE_STATUS," +
							"MARKING_DATE			," +
							"LAST_LOGIN_DATE		," +
							"HANDLE_NM	" +
						"FROM(" +
						" SELECT VW_MARKING02.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_MARKING02  ";

		string sWhere = "";
		OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,pPartnerUserSeq,pPartnerUserCharNo,ref sWhere);
		sSql = sSql + sWhere;

		sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
		sSql = sSql + sOrder;

		using (cmd = CreateSelectCommand(sSql,conn)) {
			for (int i = 0;i < objParms.Length;i++) {
				cmd.Parameters.Add((OracleParameter)objParms[i]);
			}

			cmd.Parameters.Add("FIRST_ROW",startRowIndex);
			cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"VW_MARKING02");
			}
		}
		conn.Close();
		return ds;
	}


	private OracleParameter[] CreateWhere(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerUserSeq,string pPartnerUserCharNo,ref string pWhere) {
		ArrayList list = new ArrayList();

		if (!pSiteCd.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if (!pUserSeq.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref pWhere);
			list.Add(new OracleParameter("USER_SEQ",pUserSeq));
			if (!string.IsNullOrEmpty(pUserCharNo)) {
				iBridCommLib.SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref pWhere);
				list.Add(new OracleParameter("USER_CHAR_NO",pUserCharNo));
			}
		}

		if (!pPartnerUserSeq.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("PARTNER_USER_SEQ = :PARTNER_USER_SEQ",ref pWhere);
			list.Add(new OracleParameter("PARTNER_USER_SEQ",pPartnerUserSeq));
			if (!string.IsNullOrEmpty(pPartnerUserCharNo)) {
				iBridCommLib.SysPrograms.SqlAppendWhere("PARTNER_USER_CHAR_NO = :PARTNER_USER_CHAR_NO",ref pWhere);
				list.Add(new OracleParameter("PARTNER_USER_CHAR_NO",pPartnerUserCharNo));
			}
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public void MarkingMainte(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerUserSeq,string pPartnerCharNo,int pDelFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MARKING_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("PPARTNER_USER_SEQ",DbSession.DbType.VARCHAR2,pPartnerUserSeq);
			db.ProcedureInParm("PPARTNER_USER_CHAR_NO",DbSession.DbType.VARCHAR2,pPartnerCharNo);
			db.ProcedureInParm("PPARTNER_SEX_CD",DbSession.DbType.VARCHAR2,ViCommConst.MAN);
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}


	public bool GetOne(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerUserSeq,string pPartnerUserCharNo) {
		DataSet ds;
		bool bExist = false;

		conn = DbConnect();

		string sSql = "SELECT " +
							"MARKING_DATE " +
						 "FROM T_MARKING " +
							"WHERE " +
								"SITE_CD			=:SITE_CD			AND " +
								"USER_SEQ			=:USER_SEQ			AND " +
								"USER_CHAR_NO		=:USER_CHAR_NO		AND " +
								"PARTNER_USER_SEQ	=:PARTNER_USER_SEQ	AND " +
								"PARTNER_USER_CHAR_NO	=:PARTNER_USER_CHAR_NO ";

		using (cmd = CreateSelectCommand(sSql,conn))
		using (ds = new DataSet()) {
			cmd.Parameters.Add("SITE_CD",pSiteCd);
			cmd.Parameters.Add("USER_SEQ",pUserSeq);
			cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
			cmd.Parameters.Add("PARTNER_USER_SEQ",pPartnerUserSeq);
			cmd.Parameters.Add("PARTNER_USER_CHAR_NO",pPartnerUserCharNo);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"T_MARKING");
				if (ds.Tables["T_MARKING"].Rows.Count != 0) {
					bExist = true;
				}
			}
		}
		conn.Close();
		return bExist;
	}
}
