﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: サイト運営
--	Progaram ID		: SiteManagement
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class SiteManagement:DbSession {

	public int findManElapsedDays;
	public int txMailRefMarking;
	public string[] inqManTypeSeq;
	public int bbsPicAttrFlag;
	public int bbsMovieAttrFlag;
	
	public SiteManagement() {
		findManElapsedDays = 0;
		inqManTypeSeq = new string[ViCommConst.MAX_MAN_INQUIRY_ITEMS];
		bbsPicAttrFlag = 0;
		bbsMovieAttrFlag = 0;
	}

	public bool GetOne(string pSiteCd) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;

		conn = DbConnect();

		string sSql = "SELECT " +
							"INQ_MAN_TYPE_SEQ1		," +
							"INQ_MAN_TYPE_SEQ2		," +
							"INQ_MAN_TYPE_SEQ3		," +
							"INQ_MAN_TYPE_SEQ4		," +
							"TX_MAIL_REF_MARKING	," +
							"FIND_MAN_ELAPASED_DAYS ," +
							"BBS_PIC_ATTR_FLAG		," +
							"BBS_MOVIE_ATTR_FLAG    " +
						 "FROM T_SITE_MANAGEMENT " +
							"WHERE " +
								"SITE_CD =:SITE_CD ";

		using (cmd = CreateSelectCommand(sSql,conn))
		using (ds = new DataSet()) {
			cmd.Parameters.Add("SITE_CD",pSiteCd);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"T_SITE_MANAGEMENT");
				if (ds.Tables["T_SITE_MANAGEMENT"].Rows.Count != 0) {
					dr = ds.Tables["T_SITE_MANAGEMENT"].Rows[0];
					findManElapsedDays = int.Parse(dr["FIND_MAN_ELAPASED_DAYS"].ToString());
					txMailRefMarking = int.Parse(dr["TX_MAIL_REF_MARKING"].ToString());
					bbsPicAttrFlag = int.Parse(dr["BBS_PIC_ATTR_FLAG"].ToString());
					bbsMovieAttrFlag = int.Parse(dr["BBS_MOVIE_ATTR_FLAG"].ToString());
					for (int i = 0;i < ViCommConst.MAX_MAN_INQUIRY_ITEMS;i++) {
						inqManTypeSeq[i] = dr[string.Format("INQ_MAN_TYPE_SEQ{0}",i + 1)].ToString();
					}
					bExist = true;
				}
			}
		}
		conn.Close();
		return bExist;
	}

	public DataSet GetInqManTypeSeq(string pSiteCd) {
		DataSet ds = new DataSet();

		conn = DbConnect();
		string sSql = "SELECT " +
							"INQ_MAN_TYPE_SEQ1		," +
							"INQ_MAN_TYPE_SEQ2		," +
							"INQ_MAN_TYPE_SEQ3		," +
							"INQ_MAN_TYPE_SEQ4		" +
						"FROM T_SITE_MANAGEMENT " +
							"WHERE " +
								"SITE_CD =:SITE_CD ";

		using (cmd = CreateSelectCommand(sSql,conn)) {
			cmd.Parameters.Add("SITE_CD",pSiteCd);
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds);
			}
		}
		conn.Close();
		return ds;
	}
}
