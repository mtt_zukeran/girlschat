/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: キャストメールテンプレート
--	Progaram ID		: CastMailTemplate
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class CastMailTemplate:DbSession {

	public string htmlDoc;
	public string mailTitle;
	public string mailTemplateType;
	public string mailAttachedObjType;

	public CastMailTemplate() {
		htmlDoc = "";
		mailTitle = "";
		mailTemplateType = "";
		mailAttachedObjType = "";
	}

	public int GetPageCount(string pSiteCd,string pUserSeq,string pUserCharNo,string pMailTemplateType) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;

		conn = DbConnect();

		string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_CAST_MAIL_TEMPLATE01 ";

		string sWhere = "";
		OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,pMailTemplateType,ref sWhere);
		sSql = sSql + sWhere;

		using (cmd = CreateSelectCommand(sSql,conn))
		using (da = new OracleDataAdapter(cmd))
		using (ds = new DataSet()) {
			for (int i = 0;i < objParms.Length;i++) {
				cmd.Parameters.Add((OracleParameter)objParms[i]);
			}

			da.Fill(ds);
			if (ds.Tables[0].Rows.Count != 0) {
				dr = ds.Tables[0].Rows[0];
				iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
			}
		}

		conn.Close();
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pUserSeq,string pUserCharNo,string pMailTemplateType,int startRowIndex,int maximumRows) {
		DataSet ds;
		conn = DbConnect();
		ds = new DataSet();

		string sOrder = "ORDER BY SITE_CD,USER_SEQ,USER_CHAR_NO,MAIL_TEMPLATE_NO ";
		string sSql = "SELECT " +
						"SITE_CD			," +
						"USER_SEQ			," +
						"USER_CHAR_NO		," +
						"EDIT_USER_CHAR_NO	," +
						"MAIL_TEMPLATE_NO	," +
						"MAIL_TEMPLATE_TYPE	," +
						"HTML_DOC_SEQ		," +
						"UPDATE_DATE		," +
						"REVISION_NO		," +
						"MAIL_TITLE			," +
						"HTML_DOC_TITLE		," +
						"SUBSTR(HTML_DOC1,1,40) DOC," +
						"HTML_DOC1			," +
						"HTML_DOC2			," +
						"HTML_DOC3			," +
						"HTML_DOC4			," +
						"MAIL_TEMPLATE_TYPE_NM," +
						"SITE_NM	" +
						"FROM(" +
						" SELECT VW_CAST_MAIL_TEMPLATE01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_CAST_MAIL_TEMPLATE01  ";

		string sWhere = "";
		OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,pMailTemplateType,ref sWhere);
		sSql = sSql + sWhere;

		sSql = sSql + ") innerTable WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";    // modify MCF
		sSql = sSql + sOrder;

		using (cmd = CreateSelectCommand(sSql,conn)) {
			for (int i = 0;i < objParms.Length;i++) {
				cmd.Parameters.Add((OracleParameter)objParms[i]);
			}

			cmd.Parameters.Add("FIRST_ROW",startRowIndex);
			cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"VW_CAST_MAIL_TEMPLATE01");
			}
		}
		conn.Close();
		return ds;
	}


	private OracleParameter[] CreateWhere(string pSiteCd,string pUserSeq,string pUserCharNo,string pMailTemplateType,ref string pWhere) {
		pWhere = "";
		ArrayList list = new ArrayList();

		if (pSiteCd.Equals("") == false) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD ",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ ",ref pWhere);
		list.Add(new OracleParameter("USER_SEQ",pUserSeq));

		if (pUserCharNo.Equals("") == false) {
			SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO ",ref pWhere);
			list.Add(new OracleParameter("USER_CHAR_NO",pUserCharNo));
		}

		if (!pMailTemplateType.Equals("")) {
			SysPrograms.SqlAppendWhere("MAIL_TEMPLATE_TYPE = :MAIL_TEMPLATE_TYPE ",ref pWhere);
			list.Add(new OracleParameter("MAIL_TEMPLATE_TYPE",pMailTemplateType));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}


	public bool GetOne(string pSiteCd,string pUserSeq,string pUserCharNo,string pMailTemplateNo) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;

		conn = DbConnect();

		string sSql = "SELECT " +
							"MAIL_TITLE				," +
							"MAIL_TEMPLATE_TYPE		," +
							"MAIL_ATTACHED_OBJ_TYPE	," +
							"USER_CHAR_NO			," +
							"HTML_DOC1				," +
							"HTML_DOC2				," +
							"HTML_DOC3				," +
							"HTML_DOC4				," +
							"HTML_DOC5				," +
							"HTML_DOC6				," +
							"HTML_DOC7				," +
							"HTML_DOC8				," +
							"HTML_DOC9				," +
							"HTML_DOC10	 " +
						 "FROM VW_CAST_MAIL_TEMPLATE01 " +
							"WHERE " +
								"SITE_CD			=:SITE_CD		AND " +
								"USER_SEQ			=:USER_SEQ		AND " +
								"USER_CHAR_NO		=:USER_CHAR_NO	AND " +
								"MAIL_TEMPLATE_NO	=:MAIL_TEMPLATE_NO ";

		using (cmd = CreateSelectCommand(sSql,conn))
		using (ds = new DataSet()) {
			cmd.Parameters.Add("SITE_CD",pSiteCd);
			cmd.Parameters.Add("USER_SEQ",pUserSeq);
			cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
			cmd.Parameters.Add("MAIL_TEMPLATE_NO",pMailTemplateNo);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"VW_CAST_MAIL_TEMPLATE01");
				if (ds.Tables["VW_CAST_MAIL_TEMPLATE01"].Rows.Count != 0) {
					dr = ds.Tables["VW_CAST_MAIL_TEMPLATE01"].Rows[0];
					mailTitle = dr["MAIL_TITLE"].ToString();
					htmlDoc = dr["HTML_DOC1"].ToString() +
								dr["HTML_DOC2"].ToString() +
								dr["HTML_DOC3"].ToString() +
								dr["HTML_DOC4"].ToString() +
								dr["HTML_DOC5"].ToString() +
								dr["HTML_DOC6"].ToString() +
								dr["HTML_DOC7"].ToString() +
								dr["HTML_DOC8"].ToString() +
								dr["HTML_DOC9"].ToString() +
								dr["HTML_DOC10"].ToString();
					mailTemplateType = dr["MAIL_TEMPLATE_TYPE"].ToString();
					mailAttachedObjType = dr["MAIL_ATTACHED_OBJ_TYPE"].ToString();
					bExist = true;
				}
			}
		}
		conn.Close();
		return bExist;
	}
}
