﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 出演者動画属性種別
--	Progaram ID		: CastMovieAttrType
--
--  Creation Date	: 2010.05.20
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using ViComm;
using iBridCommLib;

public class CastMovieAttrType:DbSession {

	public CastMovieAttrType() {
	}

	private OracleParameter[] CreateWhere(string pSiteCd,ref string pWhere) {
		pWhere = "";

		ArrayList list = new ArrayList();
		pWhere = pWhere + " WHERE  SITE_CD = :SITE_CD AND CAST_MOVIE_ATTR_TYPE_SEQ <> :CAST_MOVIE_ATTR_TYPE_SEQ ";
		list.Add(new OracleParameter("SITE_CD",pSiteCd));
		list.Add(new OracleParameter("CAST_MOVIE_ATTR_TYPE_SEQ",ViCommConst.DEFAULT_CAST_MOVIE_ATTR_TYPE_SEQ));
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public DataSet GetList(string pSiteCd) {
		DataSet ds;

		conn = DbConnect();
		ds = new DataSet();

		string sOrder = " ORDER BY SITE_CD,PRIORITY ";
		string sSql = "SELECT CAST_MOVIE_ATTR_TYPE_SEQ,CAST_MOVIE_ATTR_TYPE_NM FROM T_CAST_MOVIE_ATTR_TYPE ";
		string sWhere = "";
		OracleParameter[] objParms = CreateWhere(pSiteCd,ref sWhere);

		sSql += sWhere;
		sSql += sOrder;

		using (cmd = CreateSelectCommand(sSql,conn)) {
			for (int i = 0;i < objParms.Length;i++) {
				cmd.Parameters.Add((OracleParameter)objParms[i]);
			}

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"T_CAST_MOVIE_ATTR_TYPE");
			}
		}
		conn.Close();
		return ds;
	}

	public DataSet GetListIncDefault(string pSiteCd) {
		DataSet ds;

		conn = DbConnect();
		ds = new DataSet();

		string sSql = "SELECT CAST_MOVIE_ATTR_TYPE_SEQ,CAST_MOVIE_ATTR_TYPE_NM FROM T_CAST_MOVIE_ATTR_TYPE WHERE SITE_CD = :SITE_CD ";
		sSql = sSql + " ORDER BY SITE_CD,PRIORITY";

		using (cmd = CreateSelectCommand(sSql,conn)) {
			cmd.Parameters.Add("SITE_CD",pSiteCd);
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds);
			}
		}
		conn.Close();
		return ds;
	}
}

