﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: IVP 要求
--	Progaram ID		: IvpRequest
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;
using System.Collections;
using System;
using System.Web;
using System.Web.UI;
using System.Configuration;
using iBridCommLib;
using ViComm;

public class IvpRequest:DbSession {

	public string acceptSeq;
	public string dialNo;
	public string videoPrefix;
	public string audioPrefix;
	public string interfaceResult;

	public IvpRequest() {
	}

	public string Request(
		string pSiteCd,
		string pIvpLocCd,
		string pIvpSiteCd,
		string pRequestId,
		string pSrcId,
		string pUserSeq,
		string pUserCharNo,
		string pMenuId,
		string pObjectSendMailAddr,
		int pBroadcastingSec,
		string pAddOnInfo,
		string pUseXsm
	) {
		string sResult = "";
		string sRequestSeq = "";
		string sUrl = "";
		string sServiceCode = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["xsmDialOutServiceCode"]);

		string sRequestUrl;
		using (Sys oSys = new Sys()) {
			oSys.GetValue("IVP_REQUEST_URL",out sRequestUrl);
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SET_IVP_REQUEST");
			db.ProcedureInParm("PREQUEST_SRC_ID",DbSession.DbType.VARCHAR2,pSrcId);
			db.ProcedureInParm("PCARRIER_TYPE",DbSession.DbType.VARCHAR2,ViCommConst.CARRIER_OTHERS);
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureBothParm("PREQUEST_ID",DbSession.DbType.VARCHAR2,pRequestId);
			db.ProcedureInParm("PMENU_ID",DbSession.DbType.VARCHAR2,pMenuId);
			db.ProcedureInParm("PREQUESTER_SEX_CD",DbSession.DbType.VARCHAR2,ViCommConst.OPERATOR);
			db.ProcedureInParm("PMAN_USER_SEQ",DbSession.DbType.VARCHAR2,"");
			db.ProcedureInParm("PCAST_USER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PCAST_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("PLIVE_SEQ",DbSession.DbType.VARCHAR2,"");
			db.ProcedureInParm("PCAMERA_ID",DbSession.DbType.VARCHAR2,"");
			db.ProcedureInParm("PMOVIE_SEQ",DbSession.DbType.VARCHAR2,"");
			db.ProcedureInParm("PMOVIE_PART_NO",DbSession.DbType.VARCHAR2,"");
			db.ProcedureInParm("PINVITE_TALK_KEY",DbSession.DbType.VARCHAR2,"");
			db.ProcedureInParm("PBROADCASTING_SEC",DbSession.DbType.NUMBER,pBroadcastingSec);
			db.ProcedureInParm("PPARTNER_SHARE_LIVE_ACCEPT_NO",DbSession.DbType.VARCHAR2,"");
			db.ProcedureInParm("PPARTNER_SHARE_LIVE_KEY",DbSession.DbType.VARCHAR2,"");
			db.ProcedureInParm("PMEETING_KEY",DbSession.DbType.VARCHAR2,"");
			db.ProcedureOutParm("PIVP_REQUEST_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PDEST_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PDEST_TYPE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSHARED_MEDIA_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCONNECT_TYPE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PTALK_TYPE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCHARGE_UNIT_SEC",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCHARGE_POINT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PIVP_MENU_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PIVP_ACTION_DTMF",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PIVP_ACCEPT_SEQ",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PPLAY_FILE_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPLAY_OFFSET_SEC",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPLAY_SEC",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PDIAL_OUT_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTART_GUIDANCE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSRC_USER_AGENT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PDEST_USER_AGENT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREMARKS",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PVCS_REQUEST_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSHARE_LIVE_TALK_KEY",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSHARE_LIVE_TALK_END_DATE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCAST_LOGIN_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREC_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PUAS_SIP_URI",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PUSE_MAN_CROSMILE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PUSE_CAST_CROSMILE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pUSE_MAN_VOICEAPP_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pUSE_CAST_VOICEAPP_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			sResult = db.GetStringValue("PRESULT");
			sRequestSeq = db.GetStringValue("PIVP_REQUEST_SEQ");

			sUrl = string.Format(
							"{0}?tel={1}&sid={2}&carrier={3}&req={4}&menu={5}&desttype={6}&destno={7}&loc={8}&site={9}" +
							"&camera={10}&accept={11}&filenm={12}&offset={13}&playsec={14}&dialout={15}&action={16}&mailaddr={17}&addon={18}&remarks={19}&shkey={20}&shend={21}&xsm={22}&serviceCode={23}",
							sRequestUrl,
							pSrcId,
							sRequestSeq,
							ViCommConst.CARRIER_OTHERS,
							pRequestId,
							pMenuId,
							db.GetStringValue("PDEST_TYPE"),
							db.GetStringValue("PDEST_NO"),
							pIvpLocCd,
							pIvpSiteCd,
							db.GetStringValue("PSHARED_MEDIA_NM"),
							db.GetStringValue("PIVP_ACCEPT_SEQ"),
							db.GetStringValue("PPLAY_FILE_NM"),
							db.GetStringValue("PPLAY_OFFSET_SEC"),
							db.GetStringValue("PPLAY_SEC"),
							db.GetStringValue("PDIAL_OUT_NO"),
							db.GetStringValue("PIVP_ACTION_DTMF"),
							pObjectSendMailAddr,
							pAddOnInfo,
							db.GetStringValue("PREMARKS"),
							db.GetStringValue("PSHARE_LIVE_TALK_KEY"),
							db.GetStringValue("PSHARE_LIVE_TALK_END_DATE"),
							pUseXsm,
							sServiceCode
						);

			if (!db.GetStringValue("PSTART_GUIDANCE").Equals("")) {
				sUrl = sUrl + string.Format("&gsec=20&gdest={0}",db.GetStringValue("PSTART_GUIDANCE"));
			}

		}

		if (sResult.Equals(ViCommConst.SP_RESULT_OK)) {
			sResult = ViCommConst.SP_RESULT_NG;
			if (ViCommInterface.TransIVPUserRequest(sUrl,ref acceptSeq,ref dialNo,ref videoPrefix,ref audioPrefix,ref interfaceResult)) {

				using (DbSession db = new DbSession()) {
					db.PrepareProcedure("UPDATE_ACCEPT_SEQ");
					db.ProcedureInParm("PREQUESTER_SEX_CD",DbSession.DbType.VARCHAR2,ViCommConst.OPERATOR);
					db.ProcedureInParm("PIVP_REQUEST_SEQ",DbSession.DbType.NUMBER,sRequestSeq);
					db.ProcedureInParm("PIVP_ACCEPT_SEQ",DbSession.DbType.NUMBER,acceptSeq);
					db.ProcedureInParm("PDIALIN_NO",DbSession.DbType.NUMBER,dialNo);
					db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
					db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
					db.ExecuteProcedure();
					sResult = db.GetStringValue("PRESULT");
				}
			}
		}
		return sResult;
	}



	public bool RecordingRequest(string pSiteCd,string pAcceptSeq,int pStopFlag) {
		string ivpControlUrl = "";
		string sRequest = "";
		string sAcceptSeq = "";
		string sDialNo = "";
		string sVideoPrefix = "";
		string sAudioPrefix = "";
		string sInterfaceResult = "";

		if (pStopFlag == 0) {
			sRequest = "AdminStartRecording";
		} else {
			sRequest = "AdminStopRecording";
		}

		using (Sys oSys = new Sys()) {
			oSys.GetValue("IVP_ADMIN_URL",out ivpControlUrl);
		}

		string sUrl = "";

		using (Site oSite = new Site()) {
			oSite.GetOne(pSiteCd);
			sUrl = string.Format("{0}?loc={1}&site={2}&accept={3}&request={4}&reccorder={5}",
								ivpControlUrl,
								oSite.ivpLocCd,
								oSite.ivpSiteCd,
								pAcceptSeq,
								sRequest,
								ViCommConst.REC_H264
						);
		}
		return ViCommInterface.TransIVPUserRequest(sUrl,ref sAcceptSeq,ref sDialNo,ref sVideoPrefix,ref sAudioPrefix,ref sInterfaceResult);
	}


	public DataSet GetOutgoingSession(string pTerminalId,int pSessionFlag,string pIvpRequestSeq) {
		string sSiteCd = "";
		DataSet ds;

		conn = DbConnect();
		ds = new DataSet();

		string sOrder = "ORDER BY IVP_REQUEST_SEQ DESC ";
		string sSql = "SELECT " +
							"IVP_REQUEST_SEQ				," +
							"IVP_ACCEPT_SEQ					," +
							"REQUEST_SITE_CD				," +
							"SITE_NM						," +
							"MAN_USER_SITE_CD				," +
							"MAN_USER_SEQ					," +
							"CAST_USER_SEQ					," +
							"CAST_CHAR_NO					," +    // add UCN
							"CAST_HANDLE_NM					," +
							"TO_CHAR(REGIST_DATE,'YY/MM/DD HH24:MI') REGIST_DATE," +
							"BAL_POINT						," +
							"SERVICE_POINT					," +
							"SERVICE_POINT_EFFECTIVE_DATE	," +
							"INVITE_TALK_SERVICE_POINT		," +
							"CHARGE_TYPE					," +
							"CHARGE_TYPE_NM					," +
							"USER_RANK						," +
							"HANDLE_NM						," +
							"FAVORIT_COMMENT				," +
							"BIRTHDAY	" +
					"FROM(" +
						" SELECT VW_IVP_REQUEST02.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_IVP_REQUEST02  ";

		if (string.IsNullOrEmpty(pIvpRequestSeq)) {
			sSql += "WHERE " +
				"SESSION_CONNECT_FLAG = :SESSION_CONNECT_FLAG AND " +
				"OUTGOING_TERMINAL_ID	LIKE :OUTGOING_TERMINAL_ID ";
		} else {
			sSql += "WHERE " +
				"IVP_REQUEST_SEQ = :IVP_REQUEST_SEQ ";
		}

		sSql = sSql + ")WHERE RNUM  = 1 ";
		sSql = sSql + sOrder;


		using (cmd = CreateSelectCommand(sSql,conn)) {
			if (string.IsNullOrEmpty(pIvpRequestSeq)) {
				cmd.Parameters.Add("SESSION_CONNECT_FLAG",pSessionFlag);
				cmd.Parameters.Add("OUTGOING_TERMINAL_ID",pTerminalId + "%");
			} else {
				cmd.Parameters.Add("IVP_REQUEST_SEQ",pIvpRequestSeq);
			}

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"VW_IVP_REQUEST02");
			}
		}

		conn.Close();

		string sTalkHistory = "SELECT NVL(COUNT(*),0) AS CNT, TO_CHAR(MAX(TALK_START_DATE),'YY/MM/DD HH24:MI') AS LAST_DATE  FROM T_TALK_HISTORY " +
				   "WHERE " +
					   "SITE_CD				= :SITE_CD	AND " +
					   "USER_SEQ			= :USER_SEQ	AND " +
					   "USER_CHAR_NO		= :USER_CHAR_NO	AND " + // add UCN
					   "PARTNER_USER_SEQ	= :PARTNER_USER_SEQ ";

		ds.Tables[0].Columns.Add(new DataColumn("LAST_TALK_DATE",System.Type.GetType("System.String")));
		ds.Tables[0].Columns.Add(new DataColumn("TALK_COUNT",System.Type.GetType("System.String")));


		foreach (DataRow dr in ds.Tables[0].Rows) {
			sSiteCd = dr["REQUEST_SITE_CD"].ToString();

			using (DataSet dsSub = new DataSet())
			using (cmd = CreateSelectCommand(sTalkHistory,conn)) {
				cmd.Parameters.Add("SITE_CD",dr["REQUEST_SITE_CD"].ToString());
				cmd.Parameters.Add("USER_SEQ",dr["MAN_USER_SEQ"].ToString());
				cmd.Parameters.Add("USER_CHAR_NO",dr["CAST_CHAR_NO"].ToString());   // add UCN
				cmd.Parameters.Add("PARTNER_USER_SEQ",dr["CAST_USER_SEQ"].ToString());
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(dsSub,"T_TALK_HISTORY");
					if (dsSub.Tables[0].Rows.Count != 0) {
						DataRow drSub = dsSub.Tables[0].Rows[0];
						dr["LAST_TALK_DATE"] = drSub["LAST_DATE"].ToString();
						dr["TALK_COUNT"] = drSub["CNT"].ToString();
					}
				}
			}
		}

		SessionObjs userObjs = (SessionObjs)(HttpContext.Current.Session["objs"]);
		ds.Tables[0].Columns.Add(new DataColumn("MAN_ATTR_TYPE_NM1",System.Type.GetType("System.String")));
		ds.Tables[0].Columns.Add(new DataColumn("MAN_ATTR_TYPE_NM2",System.Type.GetType("System.String")));
		ds.Tables[0].Columns.Add(new DataColumn("MAN_ATTR_VALUE1",System.Type.GetType("System.String")));
		ds.Tables[0].Columns.Add(new DataColumn("MAN_ATTR_VALUE2",System.Type.GetType("System.String")));

		using (UserManAttrType oUserManAttrType = new UserManAttrType()) {
			DataSet dsUserManAttrType = oUserManAttrType.GetInqManAttrType(sSiteCd);
			using (UserManAttrValue oUserManAttrValue = new UserManAttrValue()) {
				foreach (DataRow dr in ds.Tables[0].Rows) {
					dr["MAN_ATTR_TYPE_NM1"] = dsUserManAttrType.Tables[0].Rows[0]["MAN_ATTR_TYPE_NM1"].ToString();
					dr["MAN_ATTR_TYPE_NM2"] = dsUserManAttrType.Tables[0].Rows[0]["MAN_ATTR_TYPE_NM2"].ToString();
					dr["MAN_ATTR_VALUE1"] = oUserManAttrValue.GetAttrValue(sSiteCd,dr["MAN_USER_SEQ"].ToString(),ViCommConst.MAIN_CHAR_NO,dsUserManAttrType.Tables[0].Rows[0]["MAN_ATTR_TYPE_SEQ1"].ToString());
					dr["MAN_ATTR_VALUE2"] = oUserManAttrValue.GetAttrValue(sSiteCd,dr["MAN_USER_SEQ"].ToString(),ViCommConst.MAIN_CHAR_NO,dsUserManAttrType.Tables[0].Rows[0]["MAN_ATTR_TYPE_SEQ2"].ToString());
				}
			}
		}

		ds.Tables[0].Columns.Add(new DataColumn("USER_RANK_NM",System.Type.GetType("System.String")));

		foreach (DataRow dr in ds.Tables[0].Rows) {
			using (UserRank oRank = new UserRank()) {
				if (oRank.GetOne(dr["MAN_USER_SITE_CD"].ToString(),dr["USER_RANK"].ToString())) {
					dr["USER_RANK_NM"] = oRank.userRanNm;
				}

			}
		}
		return ds;
	}
}