﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: メール記録
--	Progaram ID		: MailLog
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System.Web;
using iBridCommLib;
using ViComm;

public class MailLog:DbSession {
	public void TxCastToManMail(
		string pSiteCd,
		string pMailTemplateNo,
		string pCastUserSeq,
		string pCastCharNo,
		string[] pManUserSeq,
		int pServicePoint,
		string pTitle,
		string pDoc,
		string pAttachedObjType,
		string pPicSeq,
		string pMovieSeq
	) {
		string[] sDoc;
		int iDocCount;

		SysPrograms.SeparateHtml(HttpUtility.HtmlDecode(pDoc),ViCommConst.MAX_ORG_MAIL_BLOCKS,out sDoc,out iDocCount);

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("TX_CAST_TO_MAN_MAIL");
			db.ProcedureInParm("PSITE_CD",DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PMAIL_TEMPLATE_NO",DbType.VARCHAR2,pMailTemplateNo);
			db.ProcedureInParm("PCAST_USER_SEQ",DbType.VARCHAR2,pCastUserSeq);
			db.ProcedureInParm("PCAST_CHAR_NO",DbType.VARCHAR2,pCastCharNo);
			db.ProcedureInArrayParm("PMAN_USER_SEQ",DbType.VARCHAR2,pManUserSeq.Length,pManUserSeq);
			db.ProcedureInParm("PMAN_USER_COUNT",DbType.NUMBER,pManUserSeq.Length);
			db.ProcedureInParm("PSERVICE_POINT",DbType.NUMBER,pServicePoint);
			db.ProcedureInParm("PORIGINAL_TITLE",DbType.VARCHAR2,pTitle);
			db.ProcedureInArrayParm("PORIGINAL_DOC",DbType.VARCHAR2,iDocCount,sDoc);
			db.ProcedureInParm("PORIGINAL_DOC_COUNT",DbType.NUMBER,iDocCount);
			db.ProcedureInParm("PATTACHED_OBJ_TYPE",DbType.VARCHAR2,pAttachedObjType);
			db.ProcedureInParm("PPIC_SEQ",DbType.VARCHAR2,pPicSeq);
			db.ProcedureInParm("PMOVIE_SEQ",DbType.VARCHAR2,pMovieSeq);
			db.ProcedureInParm("PBATCH_MAIL_FLAG",DbType.NUMBER,ViCommConst.FLAG_OFF);
			db.ProcedureInArrayParm("PSEARCH_CND_NM",DbType.VARCHAR2,0,null);
			db.ProcedureInArrayParm("PSEARCH_CND_VALUE",DbType.VARCHAR2,0,null);
			db.ProcedureInParm("PSERACH_CND_COUNT",DbType.NUMBER,0);
			db.ProcedureInParm("PPARTNER_MAIL_SEQ",DbType.VARCHAR2,"");
			db.ProcedureOutParm("PMAIL_SEQ",DbType.VARCHAR2);
			db.ProcedureOutParm("POBJ_TEMP_ID",DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public bool CheckTxMail(string pSiteCd,string pCastUserSeq,string pCastCharNo,string pManUserSeq,string pMailTemplateNo,out string pExisMsg) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CHECK_TX_MAIL");
			db.ProcedureInParm("PSITE_CD",DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PCAST_USER_SEQ",DbType.VARCHAR2,pCastUserSeq);
			db.ProcedureInParm("PCAST_CHAR_NO",DbType.VARCHAR2,pCastCharNo);
			db.ProcedureInParm("PMAN_USER_SEQ",DbType.VARCHAR2,pManUserSeq);
			db.ProcedureInParm("pMAIL_TEMPLATE_NO",DbType.VARCHAR2,pMailTemplateNo);
			db.ProcedureOutParm("PRESULT",DbType.VARCHAR2);
			db.ProcedureOutParm("PEXIST_MSG",DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbType.VARCHAR2);
			db.ExecuteProcedure();
			pExisMsg = db.GetStringValue("PEXIST_MSG");
			return db.GetStringValue("PRESULT").Equals("0");
		}
	}

	public void TxLoginMail(string pSiteCd,string pCastUserSeq,string pCastCharNo) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("TX_LOGIN_MAIL");
			db.ProcedureInParm("PSITE_CD",DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbType.VARCHAR2,pCastUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbType.VARCHAR2,pCastCharNo);
			db.ProcedureOutParm("PSTATUS",DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

}
