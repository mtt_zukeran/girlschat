﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: 出演者動画
--	Progaram ID		: CastMovie
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  2010/06/11  M.Horie    属性区分名、属性値名を取得

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class CastMovie:DbSession {

	public string siteCd;
	public string loginId;
	public string userSeq;
	public string userCharNo;
	public string movieSeq;
	public string movieTitle;
	public int chargePoint;
	public int objNotApproveFlag;
	public int movieType;

	public CastMovie() {
	}

	public int GetPageCount(string pSiteCd,string pUserSeq,string pUserCharNo,string pMovieType) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;

		conn = DbConnect();

		string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_CAST_MOVIE01 ";

		string sWhere = "";
		OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,pMovieType,ref sWhere);
		sSql = sSql + sWhere;

		using (cmd = CreateSelectCommand(sSql,conn))
		using (da = new OracleDataAdapter(cmd))
		using (ds = new DataSet()) {
			for (int i = 0;i < objParms.Length;i++) {
				cmd.Parameters.Add((OracleParameter)objParms[i]);
			}

			da.Fill(ds);
			if (ds.Tables[0].Rows.Count != 0) {
				dr = ds.Tables[0].Rows[0];
				iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
			}
		}

		conn.Close();
		return iPageCount;
	}


	public DataSet GetPageCollection(string pSiteCd,string pUserSeq,string pUserCharNo,string pMovieType,int startRowIndex,int maximumRows) {
		DataSet ds;
		conn = DbConnect();
		ds = new DataSet();

		string sOrder = "ORDER BY SITE_CD,USER_SEQ,USER_CHAR_NO,MOVIE_SEQ DESC ";
		string sSql = "SELECT " +
							"SITE_CD					," +
							"USER_SEQ					," +
							"USER_CHAR_NO				," +
							"LOGIN_ID					," +
							"MOVIE_SEQ					," +
							"MOVIE_TITLE				," +
							"CHARGE_POINT				," +
							"OBJ_NOT_APPROVE_FLAG		," +
							"OBJ_NOT_PUBLISH_FLAG		," +
							"NOT_APPROVE_MARK				," +
							"UPLOAD_DATE				," +
							"CAST_MOVIE_ATTR_TYPE_NM	," +
							"CAST_MOVIE_ATTR_NM " +
						"FROM(" +
							"SELECT " +
								"VW_CAST_MOVIE01.*," +
								"ROW_NUMBER() OVER (" + sOrder + ") AS RNUM " +
							"FROM " +
								"VW_CAST_MOVIE01  ";

		string sWhere = "";
		OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,pMovieType,ref sWhere);
		sSql = sSql + sWhere;

		sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
		sSql = sSql + sOrder;

		using (cmd = CreateSelectCommand(sSql,conn)) {
			for (int i = 0;i < objParms.Length;i++) {
				cmd.Parameters.Add((OracleParameter)objParms[i]);
			}

			cmd.Parameters.Add("FIRST_ROW",startRowIndex);
			cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"VW_CAST_MOVIE01");
			}
		}
		conn.Close();
		return ds;
	}


	private OracleParameter[] CreateWhere(string pSiteCd,string pUserSeq,string pUserCharNo,string pMovieType,ref string pWhere) {
		ArrayList list = new ArrayList();

		if (!pSiteCd.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if (!pUserSeq.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref pWhere);
			list.Add(new OracleParameter("USER_SEQ",pUserSeq));

			if (!string.IsNullOrEmpty(pUserCharNo)) {
				iBridCommLib.SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref pWhere);
				list.Add(new OracleParameter("USER_CHAR_NO",pUserCharNo));
			}
		}

		if (!pMovieType.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("MOVIE_TYPE = :MOVIE_TYPE",ref pWhere);
			list.Add(new OracleParameter("MOVIE_TYPE",pMovieType));
		}

		iBridCommLib.SysPrograms.SqlAppendWhere("(OBJ_NOT_APPROVE_FLAG IN (:OBJ_NOT_APPROVE_FLAG0,:OBJ_NOT_APPROVE_FLAG1))",ref pWhere);
		list.Add(new OracleParameter("OBJ_NOT_APPROVE_FLAG0",ViCommConst.FLAG_OFF));
		list.Add(new OracleParameter("OBJ_NOT_APPROVE_FLAG1",ViCommConst.FLAG_ON));

		iBridCommLib.SysPrograms.SqlAppendWhere("OBJ_NOT_PUBLISH_FLAG = :OBJ_NOT_PUBLISH_FLAG",ref pWhere);
		list.Add(new OracleParameter("OBJ_NOT_PUBLISH_FLAG",ViCommConst.FLAG_OFF));

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public bool GetOne(string pMovieSeq) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;

		conn = DbConnect();

		string sSql = "SELECT SITE_CD,USER_SEQ,USER_CHAR_NO,LOGIN_ID,MOVIE_SEQ,MOVIE_TITLE,"+
						" CHARGE_POINT,OBJ_NOT_APPROVE_FLAG,MOVIE_TYPE FROM VW_CAST_MOVIE01 WHERE MOVIE_SEQ =:MOVIE_SEQ";

		using (cmd = CreateSelectCommand(sSql,conn))
		using (ds = new DataSet()) {
			cmd.Parameters.Add("MOVIE_SEQ",pMovieSeq);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"VW_CAST_MOVIE01");
				if (ds.Tables["VW_CAST_MOVIE01"].Rows.Count != 0) {
					dr = ds.Tables["VW_CAST_MOVIE01"].Rows[0];
					siteCd = dr["SITE_CD"].ToString();
					userSeq = dr["USER_SEQ"].ToString();
					userCharNo = dr["USER_CHAR_NO"].ToString();
					loginId = dr["LOGIN_ID"].ToString();
					movieSeq = dr["MOVIE_SEQ"].ToString();
					movieTitle = dr["MOVIE_TITLE"].ToString();
					chargePoint = int.Parse(dr["CHARGE_POINT"].ToString());
					objNotApproveFlag = int.Parse(dr["OBJ_NOT_APPROVE_FLAG"].ToString());
					movieType = int.Parse(dr["MOVIE_TYPE"].ToString());
					bExist = true;
				}
			}
		}
		conn.Close();
		return bExist;
	}

	public decimal GetMovieNo() {
		decimal dMovieNo = 0;
		conn = DbConnect();

		string sSql = "SELECT SEQ_OBJ.NEXTVAL FROM DUAL";

		using (cmd = CreateSelectCommand(sSql,conn)) {
			dMovieNo = (decimal)cmd.ExecuteScalar();
		}
		conn.Close();
		return dMovieNo;
	}
}
