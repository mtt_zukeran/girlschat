﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: サイト構成HTML文章
--	Progaram ID		: SiteHtmlDoc
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class SiteHtmlDoc:DbSession {

	public string htmlDoc1;
	public string htmlDoc2;
	public string htmlDoc3;
	public string htmlDoc4;

	public SiteHtmlDoc() {
	}


	public bool GetOne(string pSiteCd,string pHtmlDocType,string pStartPubDay) {
		DataSet ds;
		DataRow dr;

		bool bExist = false;

		conn = DbConnect();

		string sSql = "SELECT * FROM VW_SITE_HTML_DOC03 WHERE SITE_CD = :SITE_CD AND HTML_DOC_TYPE = :HTML_DOC_TYPE AND START_PUB_DAY = :START_PUB_DAY";

		using (cmd = CreateSelectCommand(sSql,conn))
		using (ds = new DataSet()) {
			cmd.Parameters.Add("SITE_CD",pSiteCd);
			cmd.Parameters.Add("HTML_DOC_TYPE",pHtmlDocType);
			cmd.Parameters.Add("START_PUB_DAY",pStartPubDay);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"VW_SITE_HTML_DOC03");

				if (ds.Tables["VW_SITE_HTML_DOC03"].Rows.Count != 0) {
					dr = ds.Tables["VW_SITE_HTML_DOC03"].Rows[0];
					htmlDoc1 = dr["HTML_DOC1"].ToString();
					htmlDoc2 = dr["HTML_DOC2"].ToString();
					htmlDoc3 = dr["HTML_DOC3"].ToString();
					htmlDoc4 = dr["HTML_DOC4"].ToString();
					bExist = true;
				}
			}
		}

		conn.Close();

		return bExist;
	}
	
	public DataSet GetListByDocSeq(string pHtmlDocSeq) {
		DataSet ds;

		conn = DbConnect();
		ds = new DataSet();

		string sSql = "SELECT * FROM T_SITE_HTML_DOC WHERE HTML_DOC_SEQ = :HTML_DOC_SEQ";
		sSql = sSql + " ORDER BY HTML_DOC_SEQ, HTML_DOC_SUB_SEQ ";

		using (cmd = CreateSelectCommand(sSql,conn)) {
			cmd.Parameters.Add("HTML_DOC_SEQ",pHtmlDocSeq);
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds);
			}
		}
		conn.Close();
		return ds;
	}

}
