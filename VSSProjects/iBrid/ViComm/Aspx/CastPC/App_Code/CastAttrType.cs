﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 出演者属性種別
--	Progaram ID		: CastAttrType
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  2009/07/09  iBrid(Y.Inoue)    データ表示用にアイテムNOを追加

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class CastAttrType:DbSession {
	public bool naFlag;

	public CastAttrType() {
	}

	public bool GetOne(string pSiteCd,string pAttrTypeSeq) {
		DataSet ds;
		DataRow dr;

		bool bExist = false;

		conn = DbConnect();

		string sSql = "SELECT " +
						"CAST_ATTR_TYPE_SEQ		," +
						"CAST_ATTR_TYPE_NM		," +
						"PRIORITY				," +
						"INPUT_TYPE				," +
						"NA_FLAG				," +
						"GROUPING_CATEGORY_CD	," +
						"ITEM_NO				," +
						"ROW_COUNT				 " +
					"FROM " +
						"T_CAST_ATTR_TYPE " +
					"WHERE " +
						"SITE_CD			= :SITE_CD	AND " +
						"CAST_ATTR_TYPE_SEQ	= :CAST_ATTR_TYPE_SEQ	";

		using (cmd = CreateSelectCommand(sSql,conn))
		using (ds = new DataSet()) {
			cmd.Parameters.Add("SITE_CD",pSiteCd);
			cmd.Parameters.Add("CAST_ATTR_TYPE_SEQ",pAttrTypeSeq);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"T_CAST_ATTR_TYPE");

				if (ds.Tables["T_CAST_ATTR_TYPE"].Rows.Count != 0) {
					dr = ds.Tables["T_CAST_ATTR_TYPE"].Rows[0];
					if (dr["NA_FLAG"].ToString().Equals("1")) {
						naFlag = true;
					} else {
						naFlag = false;
					}
					bExist = true;
				}
			}
		}
		conn.Close();
		return bExist;
	}
}
