﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: メールBOX
--	Progaram ID		: MailBox
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater		Update Explain
  2010/06/15  KazuakiItoh	一括送信メールのデフォルト非表示対応

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using iBridCommLib;
using ViComm;

public class MailBox:DbSession {

	public MailBox() {
	}
	public int GetPageCount(string pSiteCd,string pUserSeq,string pUserCharNo,string pMailBoxType,string pTxRxType) {
		return GetPageCount(pSiteCd,pUserSeq,pUserCharNo,pMailBoxType,pTxRxType,true);
	}

	public int GetPageCount(string pSiteCd,string pUserSeq,string pUserCharNo,string pMailBoxType,string pTxRxType,bool pWithBatchMail) {
		DataSet ds;
		DataRow dr;
		int iPages = 0;

		conn = DbConnect();
		string sViewNm;
		if (pTxRxType.Equals(ViCommConst.RX)) {
			sViewNm = "VW_MAIL_BOX02";
		} else {
			sViewNm = "VW_MAIL_BOX03";
		}

		string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM " + sViewNm;
		string sWhere = "";

		OracleParameter[] oParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,pMailBoxType,pTxRxType,pWithBatchMail,ref sWhere);
		sSql = sSql + sWhere;

		using (cmd = CreateSelectCommand(sSql,conn))
		using (da = new OracleDataAdapter(cmd))
		using (ds = new DataSet()) {
			for (int i = 0;i < oParms.Length;i++) {
				cmd.Parameters.Add((OracleParameter)oParms[i]);
			}

			da.Fill(ds);
			if (ds.Tables[0].Rows.Count != 0) {
				dr = ds.Tables[0].Rows[0];
				iPages = int.Parse(dr["ROW_COUNT"].ToString());
			}
		}
		conn.Close();
		return iPages;
	}
	public DataSet GetPageCollection(string pSiteCd,string pUserSeq,string pUserCharNo,string pMailBoxType,string pTxRxType,int startRowIndex,int maximumRows) {
		return GetPageCollection(pSiteCd,pUserSeq,pUserCharNo,pMailBoxType,pTxRxType,startRowIndex,maximumRows,true);
	}

	public DataSet GetPageCollection(string pSiteCd,string pUserSeq,string pUserCharNo,string pMailBoxType,string pTxRxType,int startRowIndex,int maximumRows,bool pWithBatchMail) {
		DataSet ds;

		conn = DbConnect();
		ds = new DataSet();

		string sOrder = "ORDER BY SITE_CD,USER_SEQ,USER_CHAR_NO,MAIL_BOX_TYPE,MAIL_SEQ DESC";

		string sViewNm;
		if (pTxRxType.Equals(ViCommConst.RX)) {
			sViewNm = "VW_MAIL_BOX02";
		} else {
			sViewNm = "VW_MAIL_BOX03";
		}

		string sSql = string.Format("SELECT " +
							"SITE_CD			," +
							"USER_SEQ			," +
							"USER_CHAR_NO		," +
							"MAIL_BOX_TYPE		," +
							"MAIL_SEQ			," +
							"MAIL_TYPE			," +
							"CREATE_DATE		," +
							"MAIL_TITLE			," +
							"MAIL_DOC1			," +
							"MAIL_DOC2			," +
							"MAIL_DOC3			," +
							"MAIL_DOC4			," +
							"MAIL_DOC5			," +
							"READ_FLAG			," +
							"TX_DEL_FLAG		," +
							"RX_DEL_FLAG		," +
							"TX_SITE_CD			," +
							"TX_USER_SEQ		," +
							"TX_USER_CHAR_NO	," +
							"TX_MAIL_BOX_TYPE	," +
							"RX_SITE_CD			," +
							"RX_USER_SEQ		," +
							"RX_USER_CHAR_NO	," +
							"RX_MAIL_BOX_TYPE	," +
							"SERVICE_POINT		," +
							"SERVICE_POINT_EFFECTIVE_DATE	," +
							"SERVICE_POINT_TRANSFER_FLAG	," +
							"BATCH_MAIL_LOG_SEQ	," +
							"HANDLE_NM			," +
							"PARTNER_LOGIN_ID	," +
							"ATTACHED_OBJ_TYPE	," +
							"PIC_SEQ			," +
							"MOVIE_SEQ			," +
							"RNUM				" +
						"FROM(SELECT {0}.*, ROW_NUMBER() OVER ({1}) AS RNUM FROM {0}  ",sViewNm,sOrder);
		string sWhere = "";

		OracleParameter[] oParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,pMailBoxType,pTxRxType,pWithBatchMail,ref sWhere);

		sSql = sSql + sWhere;
		sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
		sSql = sSql + sOrder;
		using (cmd = CreateSelectCommand(sSql,conn)) {
			for (int i = 0;i < oParms.Length;i++) {
				cmd.Parameters.Add((OracleParameter)oParms[i]);
			}
			cmd.Parameters.Add("FIRST_ROW",startRowIndex);
			cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"MAIL_BOX");
			}
		}
		conn.Close();
		return ds;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pUserSeq,string pUserCharNo,string pMailBoxType,string pTxRxType,bool pWithBatchMail,ref string pWhere) {
		ArrayList list = new ArrayList();
		if (pTxRxType.Equals(ViCommConst.RX)) {
			pWhere = " WHERE SITE_CD = :SITE_CD AND  USER_SEQ = :USER_SEQ AND  USER_CHAR_NO = :USER_CHAR_NO AND MAIL_BOX_TYPE = :MAIL_BOX_TYPE AND RX_DEL_FLAG = 0 ";
		} else {
			pWhere = " WHERE SITE_CD = :SITE_CD AND  USER_SEQ = :USER_SEQ AND  USER_CHAR_NO = :USER_CHAR_NO AND MAIL_BOX_TYPE = :MAIL_BOX_TYPE AND TX_DEL_FLAG = 0 ";
		}
		if(!pWithBatchMail) {
			pWhere += " AND NVL(BATCH_MAIL_FLAG,0) = 0 ";
		}
		list.Add(new OracleParameter("SITE_CD",pSiteCd));
		list.Add(new OracleParameter("USER_SEQ",pUserSeq));
		list.Add(new OracleParameter("USER_CHAR_NO",pUserCharNo));
		list.Add(new OracleParameter("MAIL_BOX_TYPE",pMailBoxType));
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public void UpdateMailDel(string pMailSeq,string pTxRxType) {
		string[] mailSeqList = new string[] { pMailSeq };
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_MAIL_DEL");
			db.ProcedureInArrayParm("PMAIL_SEQ",DbSession.DbType.VARCHAR2,mailSeqList.Length,mailSeqList);
			db.ProcedureInParm("PMAIL_SEQ_COUNT",DbSession.DbType.NUMBER,mailSeqList.Length);
			db.ProcedureInParm("PTX_RX_TYPE",DbSession.DbType.VARCHAR2,pTxRxType);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}
}
