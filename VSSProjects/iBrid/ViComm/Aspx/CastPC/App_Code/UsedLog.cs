﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Admin
--	Title			: 利用明細
--	Progaram ID		: UsedLog
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class UsedLog:DbSession {

	public UsedLog() {
	}

	public int GetPageCount(string pSiteCd,string pSexCd,string pUserSeq,string pTel,string pReportDayFrom,string pReportDayTo,string pChargeType) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;

		conn = DbConnect();

		string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_USED_LOG01 ";

		string sWhere = "";
		OracleParameter[] objParms = CreateWhere(pSiteCd,pSexCd,pUserSeq,pTel,pReportDayFrom,pReportDayTo,pChargeType,ref sWhere);
		sSql = sSql + sWhere;

		using (cmd = CreateSelectCommand(sSql,conn))
		using (da = new OracleDataAdapter(cmd))
		using (ds = new DataSet()) {
			for (int i = 0;i < objParms.Length;i++) {
				cmd.Parameters.Add((OracleParameter)objParms[i]);
			}

			da.Fill(ds);
			if (ds.Tables[0].Rows.Count != 0) {
				dr = ds.Tables[0].Rows[0];
				iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
			}
		}

		conn.Close();
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pSexCd,string pUserSeq,string pTel,string pReportDayFrom,string pReportDayTo,string pChargeType,int startRowIndex,int maximumRows) {
		DataSet ds;
		conn = DbConnect();
		ds = new DataSet();

		string sOrder = "ORDER BY CHARGE_START_DATE DESC,CHARGE_END_DATE DESC ";
		string sSql = "SELECT " +
						"CHARGE_START_DATE	," +
						"CHARGE_END_DATE	," +
						"MAN_LOGIN_ID		," +
						"CAST_LOGIN_ID		," +
						"CHARGE_SEC			," +
						"CHARGE_TYPE		," +
						"CHARGE_TYPE_NM		," +
						"CHARGE_POINT		," +
						"CONNECT_TYPE		," +
						"CONNECT_TYPE_NM	," +
						"TALK_TYPE			," +
						"TALK_TYPE_NM		," +
						"CHARGE_MIN			," +
						"OUTGOING_TERMINAL_ID " +
					"FROM(" +
						" SELECT VW_USED_LOG01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_USED_LOG01  ";

		string sWhere = "";
		OracleParameter[] objParms = CreateWhere(pSiteCd,pSexCd,pUserSeq,pTel,pReportDayFrom,pReportDayTo,pChargeType,ref sWhere);
		sSql = sSql + sWhere;

		sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
		sSql = sSql + sOrder;

		using (cmd = CreateSelectCommand(sSql,conn)) {
			for (int i = 0;i < objParms.Length;i++) {
				cmd.Parameters.Add((OracleParameter)objParms[i]);
			}

			cmd.Parameters.Add("FIRST_ROW",startRowIndex);
			cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"VW_USED_LOG01");
			}
		}
		conn.Close();
		return ds;
	}

	public void GetTotal(string pSiteCd,string pSexCd,string pUserSeq,string pTel,string pReportDayFrom,string pReportDayTo,string pChargeType,ref int pMin,ref int pPoint) {
		DataSet ds;
		DataRow dr;
		pMin = 0;
		pPoint = 0;

		conn = DbConnect();

		string sSql = "SELECT NVL(SUM(CHARGE_MIN),0) AS MIN, NVL(SUM(CHARGE_POINT),0) AS POINT FROM VW_USED_LOG01";
		string sWhere = "";

		OracleParameter[] objParms = CreateWhere(pSiteCd,pSexCd,pUserSeq,pTel,pReportDayFrom,pReportDayTo,pChargeType,ref sWhere);

		sSql = sSql + sWhere;

		using (cmd = CreateSelectCommand(sSql,conn))
		using (da = new OracleDataAdapter(cmd))
		using (ds = new DataSet()) {
			for (int i = 0;i < objParms.Length;i++) {
				cmd.Parameters.Add((OracleParameter)objParms[i]);
			}

			da.Fill(ds);
			if (ds.Tables[0].Rows.Count != 0) {
				dr = ds.Tables[0].Rows[0];
				pMin = int.Parse(dr["MIN"].ToString());
				pPoint = int.Parse(dr["POINT"].ToString());
			}
		}
		conn.Close();
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pSexCd,string pUserSeq,string pTel,string pReportDayFrom,string pReportDayTo,string pChargeType,ref string pWhere) {
		ArrayList list = new ArrayList();

		if (!pSiteCd.Equals("")) {
			if (pSexCd.Equals(ViCommConst.OPERATOR)) {
				iBridCommLib.SysPrograms.SqlAppendWhere("CAST_SITE_CD = :SITE_CD",ref pWhere);
			} else {
				iBridCommLib.SysPrograms.SqlAppendWhere("MAN_USER_SITE_CD = :SITE_CD",ref pWhere);
			}
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if (!pUserSeq.Equals("")) {
			if (pSexCd.Equals(ViCommConst.OPERATOR)) {
				iBridCommLib.SysPrograms.SqlAppendWhere("CAST_USER_SEQ = :USER_SEQ",ref pWhere);
			} else {
				iBridCommLib.SysPrograms.SqlAppendWhere("MAN_USER_SEQ = :USER_SEQ",ref pWhere);
			}
			list.Add(new OracleParameter("USER_SEQ",pUserSeq));
		}

		if (!pChargeType.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("CHARGE_TYPE = :CHARGE_TYPE",ref pWhere);
			list.Add(new OracleParameter("CHARGE_TYPE",pChargeType));
		}

		if (!pTel.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("OUTGOING_TERMINAL_ID LIKE :OUTGOING_TERMINAL_ID",ref pWhere);
			list.Add(new OracleParameter("OUTGOING_TERMINAL_ID",pTel + "%"));
		}


		if ((!pReportDayFrom.Equals("")) && (!pReportDayTo.Equals(""))) {
			DateTime dtForm = System.DateTime.ParseExact(pReportDayFrom + " 00:00:00","yyyy/MM/dd HH:mm:ss",System.Globalization.DateTimeFormatInfo.InvariantInfo,System.Globalization.DateTimeStyles.None);
			DateTime dtTo = System.DateTime.ParseExact(pReportDayTo + " 00:00:00","yyyy/MM/dd HH:mm:ss",System.Globalization.DateTimeFormatInfo.InvariantInfo,System.Globalization.DateTimeStyles.None);
			iBridCommLib.SysPrograms.SqlAppendWhere("CHARGE_START_DATE >= :CHARGE_START_DATE_FROM AND CHARGE_START_DATE < :CHARGE_START_DATE_TO",ref pWhere);
			list.Add(new OracleParameter("CHARGE_START_DATE_FROM",dtForm));
			list.Add(new OracleParameter("CHARGE_START_DATE_TO",dtTo));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

}
