﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: システム設定
--	Progaram ID		: Sys
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class Sys:DbSession {

	public Sys() {
	}

	public bool GetValue(string pItem,out string pValue) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		pValue = "";
		conn = DbConnect();
		string sSql = "SELECT " +
							"SIP_IVP_LOC_CD			," +
							"SIP_IVP_SITE_CD		," +
							"SIP_REGIST_URL			," +
							"IVP_ADMIN_URL			," +
							"IVP_REQUEST_URL		," +
							"IVP_STARTUP_CAMERA_URL	," +
							"LIVE_KEY				," +
							"ONLINE_MONITOR_SITE_CD	," +
							"SIP_DOMAIN				," +
							"DECOMAIL_SERVER_TYPE " +
						"FROM " +
							"T_SYS ";

		using (cmd = CreateSelectCommand(sSql,conn))
		using (ds = new DataSet()) {

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"T_SYS");
				if (ds.Tables["T_SYS"].Rows.Count != 0) {
					dr = ds.Tables["T_SYS"].Rows[0];
					pValue = dr[pItem].ToString();
					bExist = true;
				}
			}
		}
		conn.Close();
		return bExist;
	}

}
