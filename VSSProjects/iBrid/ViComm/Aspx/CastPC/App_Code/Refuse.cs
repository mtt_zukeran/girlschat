﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: 拒否
--	Progaram ID		: Refuse
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class Refuse:DbSession {
	public string refuseType;

	public Refuse() {
		refuseType = "";
	}

	public int GetPageCount(string pSiteCd,string pUserSeq,string pUserCharNo) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;

		conn = DbConnect();

		string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_REFUSE02 ";

		string sWhere = "";
		OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,ref sWhere);
		sSql = sSql + sWhere;

		using (cmd = CreateSelectCommand(sSql,conn))
		using (da = new OracleDataAdapter(cmd))
		using (ds = new DataSet()) {
			for (int i = 0;i < objParms.Length;i++) {
				cmd.Parameters.Add((OracleParameter)objParms[i]);
			}

			da.Fill(ds);
			if (ds.Tables[0].Rows.Count != 0) {
				dr = ds.Tables[0].Rows[0];
				iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
			}
		}

		conn.Close();
		return iPageCount;
	}


	public DataSet GetPageCollection(string pSiteCd,string pUserSeq,string pUserCharNo,int startRowIndex,int maximumRows) {
		DataSet ds;
		conn = DbConnect();
		ds = new DataSet();

		string sOrder = "ORDER BY SITE_CD,USER_SEQ,USER_CHAR_NO,REFUSE_DATE DESC ";
		string sSql = "SELECT " +
							"SITE_CD				," +
							"USER_SEQ				," +
							"USER_CHAR_NO			," +
							"PARTNER_USER_SEQ		," +
							"PARTNER_USER_CHAR_NO	," +
							"CHARACTER_ONLINE_STATUS," +
							"REFUSE_DATE			," +
							"HANDLE_NM				," +
							"LAST_LOGIN_DATE	" +
						"FROM(" +
						" SELECT VW_REFUSE02.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_REFUSE02  ";

		string sWhere = "";
		OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,ref sWhere);
		sSql = sSql + sWhere;

		sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
		sSql = sSql + sOrder;

		using (cmd = CreateSelectCommand(sSql,conn)) {
			for (int i = 0;i < objParms.Length;i++) {
				cmd.Parameters.Add((OracleParameter)objParms[i]);
			}

			cmd.Parameters.Add("FIRST_ROW",startRowIndex);
			cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"VW_REFUSE02");
			}
		}
		conn.Close();

		ds.Tables[0].Columns.Add(new DataColumn("LAST_TALK_DATE",System.Type.GetType("System.String")));
		ds.Tables[0].Columns.Add(new DataColumn("TALK_COUNT",System.Type.GetType("System.String")));

		int iTalkCount;
		string sLastTalkDay;

		using (TalkHistory oTalkHis = new TalkHistory()) {
			foreach (DataRow dr in ds.Tables[0].Rows) {
				oTalkHis.GetTalkCount(
					pSiteCd,
					dr["PARTNER_USER_SEQ"].ToString(),
					dr["PARTNER_USER_CHAR_NO"].ToString(),
					dr["USER_SEQ"].ToString(),
					dr["USER_CHAR_NO"].ToString(),
					out iTalkCount,
					out sLastTalkDay
				);
				dr["LAST_TALK_DATE"] = sLastTalkDay;
				dr["TALK_COUNT"] = iTalkCount;
			}
		}

		return ds;
	}


	private OracleParameter[] CreateWhere(string pSiteCd,string pUserSeq,string pUserCharNo,ref string pWhere) {
		ArrayList list = new ArrayList();

		if (!pSiteCd.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if (!pUserSeq.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref pWhere);
			list.Add(new OracleParameter("USER_SEQ",pUserSeq));

			if (!string.IsNullOrEmpty(pUserCharNo)) {
				iBridCommLib.SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref pWhere);
				list.Add(new OracleParameter("USER_CHAR_NO",pUserCharNo));
			}
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}


	public void RefuseMainte(string pSiteCd,string pUserSeq,string pPartnerUserSeq,string pPartnerUserCharNo,string pRefuseType,int pDelFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("REFUSE_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_SEX_CD",DbSession.DbType.VARCHAR2,ViCommConst.OPERATOR);
			db.ProcedureInParm("PPARTNER_USER_SEQ",DbSession.DbType.VARCHAR2,pPartnerUserSeq);
			db.ProcedureInParm("PPARTNER_USER_CHAR_NO",DbSession.DbType.VARCHAR2,pPartnerUserCharNo);
			db.ProcedureInParm("PREFUSE_TYPE",DbSession.DbType.VARCHAR2,pRefuseType);
			db.ProcedureInParm("PREFUSE_COMMENT",DbSession.DbType.VARCHAR2,"");
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}


	public bool GetOne(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerUserSeq,string pPartnerUserCharNo) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;

		conn = DbConnect();

		string sSql = "SELECT " +
							"REFUSE_TYPE " +
						 "FROM T_REFUSE " +
							"WHERE " +
								"SITE_CD				=:SITE_CD			AND " +
								"USER_SEQ				=:USER_SEQ			AND " +
								"USER_CHAR_NO			=:USER_CHAR_NO		AND " +
								"PARTNER_USER_SEQ		=:PARTNER_USER_SEQ	AND " +
								"PARTNER_USER_CHAR_NO	=:PARTNER_USER_CHAR_NO ";

		using (cmd = CreateSelectCommand(sSql,conn))
		using (ds = new DataSet()) {
			cmd.Parameters.Add("SITE_CD",pSiteCd);
			cmd.Parameters.Add("USER_SEQ",pUserSeq);
			cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
			cmd.Parameters.Add("PARTNER_USER_SEQ",pPartnerUserSeq);
			cmd.Parameters.Add("PARTNER_USER_CHAR_NO",pPartnerUserCharNo);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"T_REFUSE");
				if (ds.Tables["T_REFUSE"].Rows.Count != 0) {
					dr = ds.Tables["T_REFUSE"].Rows[0];
					refuseType = dr["REFUSE_TYPE"].ToString();
					bExist = true;
				}
			}
		}
		conn.Close();
		return bExist;
	}
}
