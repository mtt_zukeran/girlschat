﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Admin
--	Title			: キャラクターログイン履歴
--	Progaram ID		: LoginCharacter
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class LoginCharacter:DbSession {

	public LoginCharacter() {
	}

	public int GetPageCount(string pSiteCD,string pUserSeq,string pUserCharNo,string pReportDayFrom,string pReportDayTo) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;

		conn = DbConnect();

		string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_LOGIN_CHARACTER01 ";

		string sWhere = "";
		OracleParameter[] objParms = CreateWhere(pSiteCD,pUserSeq,pUserCharNo,pReportDayFrom,pReportDayTo,ref sWhere);
		sSql = sSql + sWhere;

		using (cmd = CreateSelectCommand(sSql,conn))
		using (da = new OracleDataAdapter(cmd))
		using (ds = new DataSet()) {
			for (int i = 0;i < objParms.Length;i++) {
				cmd.Parameters.Add((OracleParameter)objParms[i]);
			}

			da.Fill(ds);
			if (ds.Tables[0].Rows.Count != 0) {
				dr = ds.Tables[0].Rows[0];
				iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
			}
		}

		conn.Close();
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCD,string pUserSeq,string pUserCharNo,string pReportDayFrom,string pReportDayTo,int startRowIndex,int maximumRows) {
		DataSet ds;
		conn = DbConnect();
		ds = new DataSet();

		string sOrder = "ORDER BY LOGIN_SEQ DESC,SITE_CD,USER_CHAR_NO ";
		string sSql = "SELECT " +
						"LOGIN_SEQ				," +
						"USER_CHAR_NO			," +
						"START_DATE				," +
						"LOGOFF_DATE			," +
						"SITE_NM				," +
						"SITE_CD				," +
						"PRV_TV_TALK_COUNT		," +
						"PRV_TV_TALK_MIN		," +
						"PUB_TV_TALK_COUNT		," +
						"PUB_TV_TALK_MIN		," +
						"PRV_VOICE_TALK_COUNT	," +
						"PRV_VOICE_TALK_MIN		," +
						"PUB_VOICE_TALK_COUNT	," +
						"PUB_VOICE_TALK_MIN		," +
						"VIEW_TALK_MIN			," +
						"VIEW_TALK_COUNT		," +
						"WIRETAP_COUNT			," +
						"WIRETAP_MIN			," +
						"VIEW_BROADCAST_MIN		," +
						"VIEW_BROADCAST_COUNT " +
					"FROM(" +
						" SELECT VW_LOGIN_CHARACTER01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_LOGIN_CHARACTER01  ";

		string sWhere = "";
		OracleParameter[] objParms = CreateWhere(pSiteCD,pUserSeq,pUserCharNo,pReportDayFrom,pReportDayTo,ref sWhere);
		sSql = sSql + sWhere;

		sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
		sSql = sSql + sOrder;

		using (cmd = CreateSelectCommand(sSql,conn)) {
			for (int i = 0;i < objParms.Length;i++) {
				cmd.Parameters.Add((OracleParameter)objParms[i]);
			}

			cmd.Parameters.Add("FIRST_ROW",startRowIndex);
			cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"VW_LOGIN_CHARACTER01");
			}
		}
		conn.Close();
		return ds;
	}


	private OracleParameter[] CreateWhere(string pSiteCD,string pUserSeq,string pUserCharNo,string pReportDayFrom,string pReportDayTo,ref string pWhere) {
		ArrayList list = new ArrayList();

		iBridCommLib.SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
		list.Add(new OracleParameter("SITE_CD",pSiteCD));

		iBridCommLib.SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref pWhere);
		list.Add(new OracleParameter("USER_SEQ",pUserSeq));

		iBridCommLib.SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref pWhere);
		list.Add(new OracleParameter("USER_CHAR_NO",pUserCharNo));

		if ((!pReportDayFrom.Equals("")) && (!pReportDayTo.Equals(""))) {
			DateTime dtForm = System.DateTime.ParseExact(pReportDayFrom + " 00:00:00","yyyy/MM/dd HH:mm:ss",System.Globalization.DateTimeFormatInfo.InvariantInfo,System.Globalization.DateTimeStyles.None);
			DateTime dtTo = System.DateTime.ParseExact(pReportDayTo + " 00:00:00","yyyy/MM/dd HH:mm:ss",System.Globalization.DateTimeFormatInfo.InvariantInfo,System.Globalization.DateTimeStyles.None);
			iBridCommLib.SysPrograms.SqlAppendWhere("START_DATE >= :START_DATE_FROM AND START_DATE < :START_DATE_TO",ref pWhere);
			list.Add(new OracleParameter("START_DATE_FROM",dtForm));
			list.Add(new OracleParameter("START_DATE_TO",dtTo));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

}
