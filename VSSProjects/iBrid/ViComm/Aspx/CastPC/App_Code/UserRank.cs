﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ユーザーランク
--	Progaram ID		: UserRank
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;

public class UserRank:DbSession {

	public string userRank;
	public string userRanNm;
	public string colorBack;
	public string colorChar;
	public string colorLine;
	public string colorLink;
	public string colorALink;
	public string colorVLink;
	public int sizeLine;
	public int sizeChar;

	public UserRank() {
	}

	public UserRank(string pSiteCd,string pUserRank) {
		userRank = pUserRank;
		userRanNm = "";
		colorBack = "";
		colorChar = "";
		colorLine = "";
		colorLink = "";
		colorALink = "";
		colorVLink = "";
		sizeLine = 0;
		sizeChar = 0;
		GetOne(pSiteCd,pUserRank);
	}


	public bool GetOne(string pSiteCd,string pUserRank) {
		DataSet ds;
		DataRow dr;

		bool bExist = false;

		conn = DbConnect("UserRank.GetOne");

		string sSql = "SELECT " +
						"USER_RANK			," +
						"COLOR_LINE			," +
						"COLOR_INDEX		," +
						"COLOR_CHAR			," +
						"COLOR_BACK			," +
						"COLOR_LINK			," +
						"COLOR_ALINK		," +
						"COLOR_VLINK		," +
						"SIZE_LINE			," +
						"SIZE_CHAR			," +
						"USER_RANK_NM		 " +
					"FROM " +
						"VW_USER_RANK01 " +
					"WHERE " +
						"SITE_CD = :SITE_CD AND USER_RANK = :USER_RANK";

		using (cmd = CreateSelectCommand(sSql,conn))
		using (ds = new DataSet()) {
			cmd.Parameters.Add("SITE_CD",pSiteCd);
			cmd.Parameters.Add("USER_RANK",pUserRank);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"VW_USER_RANK01");

				if (ds.Tables["VW_USER_RANK01"].Rows.Count != 0) {
					dr = ds.Tables["VW_USER_RANK01"].Rows[0];
					userRank = dr["USER_RANK"].ToString();
					userRanNm = dr["USER_RANK_NM"].ToString();
					colorBack = dr["COLOR_BACK"].ToString();
					colorChar = dr["COLOR_CHAR"].ToString();
					colorLine = dr["COLOR_LINE"].ToString();
					colorLink = dr["COLOR_LINK"].ToString();
					colorALink = dr["COLOR_ALINK"].ToString();
					colorVLink = dr["COLOR_VLINK"].ToString();
					sizeChar = int.Parse(dr["SIZE_CHAR"].ToString());
					sizeLine = int.Parse(dr["SIZE_LINE"].ToString());
					bExist = true;
				}
			}
		}
		conn.Close();
		return bExist;
	}
}
