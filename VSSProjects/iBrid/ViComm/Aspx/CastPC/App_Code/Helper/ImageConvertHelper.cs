﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 画像変換機能を提供するヘルパークラス

--	Progaram ID		: ImageConvertHelper
--
--  Creation Date	: 2010.08.18
--  Creater			: K.Itoh
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------
  Date			Updater			Update Explain

-------------------------------------------------------------------------*/
using System;
using System.IO;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;


/// <summary>
/// ImageHelper の概要の説明です
/// </summary>
public class ImageHelper {
	public static void SetCopyRight(string pSiteCd, string pInDirPath, string pInFileName) {
		string sMailParseDir = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["MailParseDir"]);

		string sStampFileNm = string.Empty;
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("GET_MAILPARSE_USE");
			db.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, pSiteCd);
			db.ProcedureOutParm("pSTAMP_FILE_NM", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTAMP_SAMPLE_FILE_NM", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pPIC_SIZE_LARGE", DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pPIC_SIZE_SMALL", DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pSCALE_TYPE", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pRECORD_COUNT", DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			sStampFileNm = db.GetStringValue("pSTAMP_FILE_NM");
		}

		string sStampFilePath = Path.Combine(sMailParseDir, sStampFileNm);
		using (localhost.Service objWebSrv = new localhost.Service()) {
			objWebSrv.SetCopyRight(sMailParseDir, pInDirPath, pInFileName, sStampFilePath);
		}
	}

	public static void ConvertMobileImage(string pSiteCd, string pInputDir, string pOutputDir, string pFileNm) {
		string sMailParseDir = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["MailParseDir"]);
		using (localhost.Service objWebSrv = new localhost.Service()) {
			objWebSrv.ConvertMobileImage(sMailParseDir,pSiteCd, pInputDir, pOutputDir, pFileNm);
		}
	}

}
