/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: 男性会員キャラクター
--	Progaram ID		: UserManCharacter
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class UserManCharacter:DbSession {

	public UserManCharacter() {
	}

	public DataSet GetOne(string pSiteCd,string pUserSeq,string pCastSeq,string pCastCharNo) {
		DataSet ds;
		conn = DbConnect();
		ds = new DataSet();
		string sSql = "SELECT " + QueryFields() +
						"FROM VW_USER_MAN_CHARACTER03 WHERE SITE_CD = :SITE_CD AND USER_SEQ =:USER_SEQ";

		using (cmd = CreateSelectCommand(sSql,conn)) {
			cmd.Parameters.Add("SITE_CD",pSiteCd);
			cmd.Parameters.Add("USER_SEQ",pUserSeq);
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds);
			}
		}
		conn.Close();


		ds.Tables[0].Columns.Add(new DataColumn("LAST_TALK_DATE",System.Type.GetType("System.String")));
		ds.Tables[0].Columns.Add(new DataColumn("TALK_COUNT",System.Type.GetType("System.String")));
		ds.Tables[0].Columns.Add(new DataColumn("REFUSE_FLAG",System.Type.GetType("System.String")));
		ds.Tables[0].Columns.Add(new DataColumn("RELATION_TYPE",System.Type.GetType("System.String")));

		int iTalkCount;
		string sLastTalkDay;
		bool bRelation = false;

		using (TalkHistory oTalkHis = new TalkHistory()) {
			foreach (DataRow dr in ds.Tables[0].Rows) {
				oTalkHis.GetTalkCount(pSiteCd,pUserSeq,ViCommConst.MAIN_CHAR_NO,pCastSeq,pCastCharNo,out iTalkCount,out sLastTalkDay);

				dr["LAST_TALK_DATE"] = sLastTalkDay;
				dr["TALK_COUNT"] = iTalkCount;
				if (iTalkCount > 0) {
					bRelation = true;
					dr["RELATION_TYPE"] = ViCommConst.REL_TALK;
				} else {
					dr["RELATION_TYPE"] = "";
				}
			}
		}

		if (!bRelation) {
			using (Favorit oFavorit = new Favorit()) {
				foreach (DataRow dr in ds.Tables[0].Rows) {
					if (oFavorit.GetOne(pSiteCd,pUserSeq,ViCommConst.MAIN_CHAR_NO,pCastSeq,pCastCharNo)) {
						dr["RELATION_TYPE"] = ViCommConst.REL_FAVOTIR;
						bRelation = true;
					}
				}
			}
		}

		if (!bRelation) {
			using (Marking oMarking = new Marking()) {
				foreach (DataRow dr in ds.Tables[0].Rows) {
					if (oMarking.GetOne(pSiteCd,pUserSeq,ViCommConst.MAIN_CHAR_NO,pCastSeq,pCastCharNo)) {
						dr["RELATION_TYPE"] = ViCommConst.REL_MARKING;
					}
				}
			}
		}

		using (Refuse oRefuse = new Refuse()) {
			foreach (DataRow dr in ds.Tables[0].Rows) {
				if (oRefuse.GetOne(pSiteCd,pCastSeq,pCastCharNo,pUserSeq,ViCommConst.MAIN_CHAR_NO)) {
					dr["REFUSE_FLAG"] = "1";
				} else {
					dr["REFUSE_FLAG"] = "";
				}
			}
		}

		if (ds.Tables[0].Rows.Count != 0) {
			using (UserManAttrValue oAttr = new UserManAttrValue()) {
				DataSet dsSub = oAttr.GetList(pSiteCd,ds.Tables[0].Rows[0]["USER_SEQ"].ToString(),ViCommConst.MAIN_CHAR_NO);
				int i = 0;
				foreach (DataRow dr in dsSub.Tables[0].Rows) {
					ds.Tables[0].Columns.Add(new DataColumn("MAN_ATTR_TYPE_NM" + i.ToString(),System.Type.GetType("System.String")));
					ds.Tables[0].Columns.Add(new DataColumn("DISPLAY_VALUE" + i.ToString(),System.Type.GetType("System.String")));
					ds.Tables[0].Rows[0]["MAN_ATTR_TYPE_NM" + i.ToString()] = dr["MAN_ATTR_TYPE_NM"];
					ds.Tables[0].Rows[0]["DISPLAY_VALUE" + i.ToString()] = dr["DISPLAY_VALUE"];
					i = i + 1;
				}
				ds.Tables[0].Columns.Add(new DataColumn("COUNT",System.Type.GetType("System.String")));
				ds.Tables[0].Rows[0]["COUNT"] = i;
			}
		}
		return ds;
	}

	private string QueryFields() {
		return "SITE_CD							," +
				"USER_SEQ						," +
				"USER_CHAR_NO			        ," +    // add UCN
				"BAL_POINT						," +
				"SERVICE_POINT					," +
				"SERVICE_POINT_EFFECTIVE_DATE	," +
				"LIMIT_POINT					," +
				"FIRST_LOGIN_DATE				," +
				"LAST_LOGIN_DATE				," +
				"FIRST_POINT_USED_DATE          ," +
				"LAST_POINT_USED_DATE			," +
				"FIRST_RECEIPT_DAY				," +
				"LAST_RECEIPT_DATE				," +
				"RECEIPT_LIMIT_DAY				," +
				"BILL_AMT						," +
				"TOTAL_RECEIPT_AMT				," +
				"TOTAL_RECEIPT_COUNT			," +
				"EXIST_CREDIT_DEAL_FLAG			," +
				"BLACK_DAY						," +
				"BLACK_COUNT					," +
				"BLACK_TIME						," +
				"LAST_BLACK_DAY					," +
				"LAST_BLACK_TIME				," +
				"CAST_MAIL_RX_TYPE				," +
				"INFO_MAIL_RX_TYPE				," +
				"LOGIN_MAIL_RX_FLAG1			," +
				"LOGIN_MAIL_RX_FLAG2			," +
				"AD_CD							," +
				"PROFILE_OK_FLAG				," +
				"SEX_CD							," +
				"USE_TERMINAL_TYPE				," +
				"URI							," +
				"TERMINAL_PASSWORD				," +
				"TEL							," +
				"EMAIL_ADDR						," +
				"LOGIN_ID						," +
				"LOGIN_PASSWORD					," +
				"USER_STATUS					," +
				"TALKING_FLAG					," +
				"TALK_LOCK_FLAG					," +
				"TALK_LOCK_DATE					," +
				"CALL_STATUS_SEQ				," +
				"IVP_REQUEST_SEQ				," +
				"LOGIN_SEQ						," +
				"MOBILE_CARRIER_CD				," +
				"MOBILE_TERMINAL_SHORT_NM		," +
				"MOBILE_TERMINAL_NM				," +
				"TERMINAL_UNIQUE_ID				," +
				"TEL_ATTESTED_FLAG				," +
				"NON_EXIST_MAIL_ADDR_FLAG		," +
				"REMARKS1						," +
				"REMARKS2						," +
				"REMARKS3						," +
				"REGIST_DATE					," +
				"UPDATE_DATE					," +
				"REVISION_NO					," +
				"REVISION_NO_MAN				," +
				"HANDLE_NM						," +
				"BIRTHDAY						," +
				"AGE							," +
				"USER_RANK						," +
				"CHARACTER_ONLINE_STATUS		," +
				"URGE_LEVEL						," +
				"MAX_URGE_COUNT					," +
				"URGE_COUNT_TODAY				," +
				"URGE_RESTART_DAY				," +
				"URGE_END_DAY					," +
				"URGE_EXEC_LAST_DATE			," +
				"URGE_EXEC_LAST_STATUS			," +
				"URGE_EXEC_COUNT				," +
				"URGE_CONNECT_COUNT				," +
				"REVISION_NO_URGE				," +
				"AD_NM							," +
				"MOBILE_CARRIER_NM				";
	}


	public int GetPageCount(
		string pSiteCd,
		int pConditionFlag,
		string pHandleNm,
		string pRegistDayType,
		string pUserSeq,
		string pUserCharNo,
		string pManAttrValue1,
		string pManAttrValue2,
		string pManAttrValue3,
		string pManAttrValue4
	) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;

		string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_USER_MAN_CHARACTER03 P";

		string[] sManAttrValue = { pManAttrValue1,pManAttrValue2,pManAttrValue3,pManAttrValue4 };
		if ((pConditionFlag & ViCommConst.SearchManStatusOption.SEL_MAN_CONDITION) != 0) {
			for (int i = 0;i < ViCommConst.MAX_MAN_INQUIRY_ITEMS;i++) {
				if (!sManAttrValue[i].Equals("")) {
					sSql = sSql + string.Format(",VW_USER_MAN_ATTR_VALUE01 AT{0} ",i + 1);
				}
			}
		}

		string sWhere = "";
		OracleParameter[] objParms = CreateWhere(pSiteCd,pConditionFlag,pHandleNm,pRegistDayType,pUserSeq,pUserCharNo,pManAttrValue1,pManAttrValue2,pManAttrValue3,pManAttrValue4,ref sWhere);
		sSql = sSql + sWhere;

		conn = DbConnect();
		using (cmd = CreateSelectCommand(sSql,conn))
		using (da = new OracleDataAdapter(cmd))
		using (ds = new DataSet()) {
			for (int i = 0;i < objParms.Length;i++) {
				cmd.Parameters.Add((OracleParameter)objParms[i]);
			}

			da.Fill(ds);
			if (ds.Tables[0].Rows.Count != 0) {
				dr = ds.Tables[0].Rows[0];
				iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
			}
		}

		conn.Close();
		return iPageCount;
	}


	public DataSet GetPageCollection(
		string pSiteCd,
		int pConditionFlag,
		string pHandleNm,
		string pRegistDayType,
		string pUserSeq,
		string pUserCharNo,
		string pManAttrValue1,
		string pManAttrValue2,
		string pManAttrValue3,
		string pManAttrValue4,
		int startRowIndex,
		int maximumRows
	) {

		string sOrder = "ORDER BY SITE_CD,LAST_ACTION_DATE,USER_SEQ,USER_CHAR_NO ";
		string sSql = "SELECT " +
							"SITE_CD				," +
							"USER_SEQ				," +
							"USER_CHAR_NO			," +    // add UCN
							"BAL_POINT				," +
							"CHARACTER_ONLINE_STATUS," +
							"BIRTHDAY				," +
							"LAST_LOGIN_DATE		," +
							"HANDLE_NM				" +
						"FROM(" +
						" SELECT P.*, ROW_NUMBER() OVER ( ORDER BY P.SITE_CD,LAST_ACTION_DATE,P.USER_SEQ,P.USER_CHAR_NO ) AS RNUM FROM VW_USER_MAN_CHARACTER03 P";

		string[] sManAttrValue = { pManAttrValue1,pManAttrValue2,pManAttrValue3,pManAttrValue4 };
		if ((pConditionFlag & ViCommConst.SearchManStatusOption.SEL_MAN_CONDITION) != 0) {
			for (int i = 0;i < ViCommConst.MAX_MAN_INQUIRY_ITEMS;i++) {
				if (!sManAttrValue[i].Equals("")) {
					sSql = sSql + string.Format(",VW_USER_MAN_ATTR_VALUE01 AT{0} ",i + 1);
				}
			}
		}

		string sWhere = "";
		OracleParameter[] objParms = CreateWhere(pSiteCd,pConditionFlag,pHandleNm,pRegistDayType,pUserSeq,pUserCharNo,pManAttrValue1,pManAttrValue2,pManAttrValue3,pManAttrValue4,ref sWhere);

		DataSet ds;
		conn = DbConnect();
		ds = new DataSet();

		sSql = sSql + sWhere;
		sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
		sSql = sSql + sOrder;

		using (cmd = CreateSelectCommand(sSql,conn)) {
			for (int i = 0;i < objParms.Length;i++) {
				cmd.Parameters.Add((OracleParameter)objParms[i]);
			}

			cmd.Parameters.Add("FIRST_ROW",startRowIndex);
			cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"VW_USER_MAN_CHARACTER03");
			}
		}
		conn.Close();


		ds.Tables[0].Columns.Add(new DataColumn("INQ_MAN_VALUE1",System.Type.GetType("System.String")));
		ds.Tables[0].Columns.Add(new DataColumn("INQ_MAN_VALUE2",System.Type.GetType("System.String")));
		ds.Tables[0].Columns.Add(new DataColumn("INQ_MAN_VALUE3",System.Type.GetType("System.String")));
		ds.Tables[0].Columns.Add(new DataColumn("INQ_MAN_VALUE4",System.Type.GetType("System.String")));
		using (SiteManagement oSiteManage = new SiteManagement())
		using (UserManAttrValue oAttrValue = new UserManAttrValue()) {
			oSiteManage.GetOne(pSiteCd);
			foreach (DataRow dr in ds.Tables[0].Rows) {
				for (int i = 0;i < ViCommConst.MAX_MAN_INQUIRY_ITEMS;i++) {
					dr[string.Format("INQ_MAN_VALUE{0}",i + 1)] = oAttrValue.GetAttrValue(pSiteCd,dr["USER_SEQ"].ToString(),ViCommConst.MAIN_CHAR_NO,oSiteManage.inqManTypeSeq[i]);
				}
				if ((pConditionFlag & ViCommConst.SearchManStatusOption.SEL_MAN_CONDITION) != 0) {
				}
			}
		}

		return ds;
	}


	private OracleParameter[] CreateWhere(
		string pSiteCd,
		int pConditionFlag,
		string pHandleNm,
		string pRegistDayType,
		string pUserSeq,
		string pUserCharNo,
		string pManAttrValue1,
		string pManAttrValue2,
		string pManAttrValue3,
		string pManAttrValue4,
		ref string pWhere
	) {
		ArrayList list = new ArrayList();
		string[] sManAttrValue = { pManAttrValue1,pManAttrValue2,pManAttrValue3,pManAttrValue4 };

		if (!pSiteCd.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("P.SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		iBridCommLib.SysPrograms.SqlAppendWhere("P.USER_STATUS IN (:USER_STATUS1,:USER_STATUS2,:USER_STATUS3,:USER_STATUS4,:USER_STATUS5,:USER_STATUS6)",ref pWhere);
		list.Add(new OracleParameter("USER_STATUS1",ViCommConst.USER_MAN_NORMAL));
		list.Add(new OracleParameter("USER_STATUS2",ViCommConst.USER_MAN_TEL_NO_AUTH));
		list.Add(new OracleParameter("USER_STATUS3",ViCommConst.USER_MAN_NEW));
		list.Add(new OracleParameter("USER_STATUS4",ViCommConst.USER_MAN_NO_RECEIPT));
		list.Add(new OracleParameter("USER_STATUS5",ViCommConst.USER_MAN_NO_USED));
		list.Add(new OracleParameter("USER_STATUS6",ViCommConst.USER_MAN_DELAY));

		if ((pConditionFlag & ViCommConst.SearchManStatusOption.SEL_MAN_LOGINED) != 0) {
			iBridCommLib.SysPrograms.SqlAppendWhere("P.CHARACTER_ONLINE_STATUS = :CHARACTER_ONLINE_STATUS",ref pWhere);
			list.Add(new OracleParameter("CHARACTER_ONLINE_STATUS",ViCommConst.USER_LOGINED));
		}

		if ((pConditionFlag & ViCommConst.SearchManStatusOption.SEL_MAN_ALL) != 0) {
			using (SiteManagement oManage = new SiteManagement()) {
				if (oManage.GetOne(pSiteCd)) {
					DateTime Dt = DateTime.Today.AddDays(oManage.findManElapsedDays * -1);
					iBridCommLib.SysPrograms.SqlAppendWhere("P.REGIST_DATE >= :REGIST_DATE",ref pWhere);
					list.Add(new OracleParameter("REGIST_DATE",Dt));
				}
			}
		}

		if ((pConditionFlag & ViCommConst.SearchManStatusOption.SEL_MAN_CONDITION) != 0) {
			if (!pHandleNm.Equals("")) {
				iBridCommLib.SysPrograms.SqlAppendWhere("P.HANDLE_NM LIKE :HANDLE_NM",ref pWhere);
				list.Add(new OracleParameter("HANDLE_NM",pHandleNm + "%"));
			}
			DataSet ds = new DataSet();
			using (UserManAttrType oUserManAttrType = new UserManAttrType()) {
				ds = oUserManAttrType.GetInqManAttrType(pSiteCd);
			}

			for (int i = 0;i < ViCommConst.MAX_MAN_INQUIRY_ITEMS;i++) {
				if (!sManAttrValue[i].Equals("")) {
					pWhere = pWhere + string.Format(" AND AT{0}.SITE_CD				= P.SITE_CD				",i + 1);
					pWhere = pWhere + string.Format(" AND AT{0}.USER_SEQ			= P.USER_SEQ			",i + 1);
					pWhere = pWhere + string.Format(" AND AT{0}.USER_CHAR_NO		= P.USER_CHAR_NO		",i + 1);
					pWhere = pWhere + string.Format(" AND AT{0}.MAN_ATTR_TYPE_SEQ	= :MAN_ATTR_TYPE_SEQ{0} ",i + 1);
					list.Add(new OracleParameter(string.Format("MAN_ATTR_TYPE_SEQ{0}",i + 1),ds.Tables[0].Rows[0][string.Format("MAN_ATTR_TYPE_SEQ{0}",i + 1)]));
					if (ds.Tables[0].Rows[0][string.Format("GROUPING_CATEGORY_CD{0}",i + 1)].ToString().Equals("")) {
						pWhere = pWhere + string.Format(" AND AT{0}.MAN_ATTR_SEQ = :MAN_ATTR_SEQ{0}		",i + 1);
						list.Add(new OracleParameter(string.Format("MAN_ATTR_SEQ{0}",i + 1),sManAttrValue[i]));
					} else {
						pWhere = pWhere + string.Format(" AND AT{0}.GROUPING_CD		= :GROUPING_CD{0}	",i + 1);
						list.Add(new OracleParameter(string.Format("GROUPING_CD{0}",i + 1),sManAttrValue[i]));
					}
				}
			}

			if (!pRegistDayType.Equals("")) {
				DateTime Dt;
				switch (pRegistDayType) {
					case "1":
						iBridCommLib.SysPrograms.SqlAppendWhere("P.REGIST_DATE >= :REGIST_DATE",ref pWhere);
						list.Add(new OracleParameter("REGIST_DATE",DateTime.Today));
						break;
					case "2":
						Dt = DateTime.Today.AddDays(-3);
						iBridCommLib.SysPrograms.SqlAppendWhere("P.REGIST_DATE >= :REGIST_DATE",ref pWhere);
						list.Add(new OracleParameter("REGIST_DATE",Dt));
						break;
					case "3":
						Dt = DateTime.Today.AddDays(-7);
						iBridCommLib.SysPrograms.SqlAppendWhere("P.REGIST_DATE >= :REGIST_DATE",ref pWhere);
						list.Add(new OracleParameter("REGIST_DATE",Dt));
						break;
					case "4":
						Dt = DateTime.Today.AddMonths(-1);
						iBridCommLib.SysPrograms.SqlAppendWhere("P.REGIST_DATE >= :REGIST_DATE",ref pWhere);
						list.Add(new OracleParameter("REGIST_DATE",Dt));
						break;
					case "5":
						Dt = DateTime.Today.AddMonths(-3);
						iBridCommLib.SysPrograms.SqlAppendWhere("P.REGIST_DATE >= :REGIST_DATE",ref pWhere);
						list.Add(new OracleParameter("REGIST_DATE",Dt));
						break;
				}
			}

		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

}
