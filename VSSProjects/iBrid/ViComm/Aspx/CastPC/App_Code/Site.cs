﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: サイト
--	Progaram ID		: Site
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using iBridCommLib;

public class Site:DbSession {

	public string siteCd;
	public string siteNm;
	public string webPhisicalDir;
	public string url;
	public string colorBack;
	public string colorChar;
	public string colorIndex;
	public string colorLink;
	public string colorLine;
	public int sizeLine = 2;
	public string mailHost;
	public string supportEmailAddr;
	public string supportTel;
	public int supportChgMonitorToTalk;
	public string ivpLocCd;
	public string ivpSiteCd;
	public string siteType;
	public int enablePrivteTalkMenuFlag;
	public int useOtherSysInfoFlag;
	public int castPcModifyNaFlag;
	public int bbsPicAttrFlag;
	public int bbsMovieAttrFlag;
	
	private Hashtable siteSupplement;

	public Site() {
		siteSupplement = new Hashtable();
	}

	public DataSet GetList() {
		DataSet ds;

		conn = DbConnect();
		ds = new DataSet();

		string sSql = "SELECT SITE_CD,SITE_NM,SITE_MARK FROM T_SITE WHERE NA_FLAG = 0 AND USE_OTHER_SYS_INFO_FLAG = 0";
		sSql = sSql + " ORDER BY PRIORITY,SITE_CD ";

		using (cmd = CreateSelectCommand(sSql,conn)) {
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"T_SITE");
			}
		}
		conn.Close();
		return ds;
	}

	public bool GetValue(string pSiteCd,string pItem,ref string pValue) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;

		conn = DbConnect();
		string sSql = "SELECT " +
							"SITE_CD			," +
							"SITE_NM			," +
							"HOST_NM			," +
							"MAILER_FTP_ID		," +
							"MAILER_FTP_PW		," +
							"TX_HI_MAILER_IP	," +
							"USE_OTHER_SYS_INFO_FLAG," +
							"WEB_PHISICAL_DIR	," +
                            "MULTI_CHAR_FLAG	" +
						"FROM " +
							"T_SITE " +
						"WHERE " +
							"SITE_CD = :SITE_CD ";

		using (cmd = CreateSelectCommand(sSql,conn))
		using (ds = new DataSet()) {

			cmd.Parameters.Add("SITE_CD",pSiteCd);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"T_SITE");
				if (ds.Tables["T_SITE"].Rows.Count != 0) {
					dr = ds.Tables["T_SITE"].Rows[0];
					pValue = dr[pItem].ToString();
					bExist = true;
				}
			}
		}
		conn.Close();
		return bExist;
	}

	public bool GetOne(string pSiteCd) {
		DataSet ds;
		bool bExist = false;

		conn = DbConnect();

		using (cmd = CreateSelectCommand("SELECT * FROM VW_SITE01 WHERE SITE_CD = :SITE_CD",conn))
		using (ds = new DataSet()) {
			cmd.Parameters.Add("SITE_CD",pSiteCd);
			bExist = SetData(ds);
		}
		conn.Close();

		return bExist;
	}

	public DataSet GetNewVerList() {
		DataSet ds;

		conn = DbConnect();
		ds = new DataSet();

		string sSql = "SELECT SITE_CD,SITE_NM,SITE_MARK FROM T_SITE WHERE NA_FLAG = 0 AND USE_OTHER_SYS_INFO_FLAG = 0";
		sSql = sSql + " ORDER BY PRIORITY,SITE_CD ";

		using (cmd = CreateSelectCommand(sSql,conn)) {
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"T_SITE");
			}
		}
		conn.Close();
		return ds;
	}


	private bool SetData(DataSet ds) {
		DataRow dr;
		bool bRet = false;
		using (da = new OracleDataAdapter(cmd)) {
			da.Fill(ds,"VW_SITE01");
			if (ds.Tables["VW_SITE01"].Rows.Count != 0) {
				dr = ds.Tables["VW_SITE01"].Rows[0];
				siteCd = dr["SITE_CD"].ToString();
				siteNm = dr["SITE_NM"].ToString();
				webPhisicalDir = dr["WEB_PHISICAL_DIR"].ToString();
				url = dr["URL"].ToString();
				colorBack = dr["COLOR_BACK"].ToString();
				colorChar = dr["COLOR_CHAR"].ToString();
				colorIndex = dr["COLOR_INDEX"].ToString();
				colorLink = dr["COLOR_LINK"].ToString();
				colorLine = dr["COLOR_LINE"].ToString();
				mailHost = dr["MAIL_HOST"].ToString();
				supportEmailAddr = dr["SUPPORT_EMAIL_ADDR"].ToString();
				supportTel = dr["SUPPORT_TEL"].ToString();
				ivpLocCd = dr["IVP_LOC_CD"].ToString();
				ivpSiteCd = dr["IVP_SITE_CD"].ToString();
				siteType = dr["SITE_TYPE"].ToString();
				enablePrivteTalkMenuFlag = int.Parse(dr["ENABLE_PRIVATE_TALK_MENU_FLAG"].ToString());
				supportChgMonitorToTalk = int.Parse(dr["SUPPORT_CHG_MONITOR_TO_TALK"].ToString());
				useOtherSysInfoFlag = int.Parse(dr["USE_OTHER_SYS_INFO_FLAG"].ToString());
				castPcModifyNaFlag = int.Parse(dr["CAST_PC_MODIFY_NA_FLAG"].ToString());
				bbsPicAttrFlag = int.Parse(dr["BBS_PIC_ATTR_FLAG"].ToString());
				bbsMovieAttrFlag = int.Parse(dr["BBS_MOVIE_ATTR_FLAG"].ToString());
				bRet = true;
			}
		}
		return bRet;
	}

    public bool IsActiveMulitCharFlag(string pSiteCD){

        conn = DbConnect();
        DataSet ds = new DataSet();
        StringBuilder sSql = new StringBuilder();
        sSql.Append("SELECT ");
        sSql.Append("   COUNT(SITE_CD) AS IS_ACTIVE ");
        sSql.Append("FROM ");
        sSql.Append("   T_SITE ");
        sSql.Append("WHERE ");
        sSql.Append("   SITE_CD = :SITE_CD AND ");
        sSql.Append("   MULTI_CHAR_FLAG = :MULTI_CHAR_FLAG ");

		using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {

            cmd.Parameters.Add("SITE_CD", pSiteCD);
            cmd.Parameters.Add("MULTI_CHAR_FLAG", ViComm.ViCommConst.FLAG_ON);
            
            using(da = new OracleDataAdapter(cmd)){
                da.Fill(ds);
            }
        }
        conn.Close();
        
        return Convert.ToInt32(ds.Tables[0].Rows[0]["IS_ACTIVE"]) != 0;
    }
}
