﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: 出演者キャラクター
--	Progaram ID		: CastCharacter
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  2009/12/02  tokunaga   userCharNo追加

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class CastCharacter:DbSession {
	public string userSeq;
	public string handleNm;

	public CastCharacter() {
	}


	public DataSet GetCharacterList(string pSiteCD,string pUserSeq) {
		DataSet ds;

		conn = DbConnect();
		ds = new DataSet();

		string addWhere = string.IsNullOrEmpty(pSiteCD) ?
			 " AND USER_CHAR_NO = :USER_CHAR_NO " : " AND SITE_CD = :SITE_CD ";

		string sSql = "SELECT " +
							"SITE_CD				," +
							"USER_SEQ				," +
							"USER_CHAR_NO			," +
							"CAST_NM				," +
							"HANDLE_NM				," +
							"SITE_NM				," +
							"SMALL_PHOTO_IMG_PATH	," +
							"PIC_SEQ				," +
							"ACT_CATEGORY_SEQ		," +
							"ACT_CATEGORY_NM		," +
							"USE_OTHER_SYS_INFO_FLAG," +
							"SUBSTRB(COMMENT_LIST,1,80) COMMENT_LIST," +
							"NA_MARK " +
						"FROM " +
							"VW_CAST01 WHERE USER_SEQ = :USER_SEQ AND SITE_NA_FLAG = 0 AND NA_FLAG = 0 " +
							addWhere +
						"ORDER BY USE_OTHER_SYS_INFO_FLAG ,SITE_PRIORITY,SITE_CD,USER_CHAR_NO ";

		using (cmd = CreateSelectCommand(sSql,conn)) {
			cmd.Parameters.Add("USER_SEQ",pUserSeq);
			if (string.IsNullOrEmpty(pSiteCD)) {
				cmd.Parameters.Add("USER_CHAR_NO",ViComm.ViCommConst.MAIN_CHAR_NO);
			} else {
				cmd.Parameters.Add("SITE_CD",pSiteCD);
			}
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds);
			}
		}
		conn.Close();

		return ds;
	}

	public DataSet GetLoginCharacterList(string pSiteCD,string pUserSeq) {
		DataSet ds;

		conn = DbConnect();
		ds = new DataSet();
		
		string sSql = "SELECT " +
							"SITE_CD			," +
							"USER_SEQ			," +
							"USER_CHAR_NO		," +
							"CONNECT_TYPE		," +
							"HANDLE_NM			," +
							"HANDLE_KANA_NM		," +
							"COMMENT_LIST		," +
							"COMMENT_DETAIL		," +
							"STANDBY_END_DATE	," +
							"NA_FLAG			," +
							"CHARACTER_ONLINE_STATUS " +
						"FROM " +
							"VW_CAST_CHARACTER03 " +
						"WHERE " +
							"SITE_CD	= :SITE_CD	AND " +
							"USER_SEQ	= :USER_SEQ AND " +
							"NA_FLAG	= :NA_FLAG		" +
						"ORDER BY SITE_CD,USER_SEQ,USER_CHAR_NO ";

		using (cmd = CreateSelectCommand(sSql,conn)) {
			cmd.Parameters.Add("SITE_CD",pSiteCD);
			cmd.Parameters.Add("USER_SEQ",pUserSeq);
			cmd.Parameters.Add("NA_FLAG",ViCommConst.FLAG_OFF);
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds);
			}
		}
		conn.Close();

		return ds;
	}
}
