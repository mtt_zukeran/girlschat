﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: NGワード
--	Progaram ID		: NGWord
--
--  Creation Date	: 2009.11.17
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text.RegularExpressions;
using iBridCommLib;
using ViComm;

public class NGWord:DbSession {

	public NGWord() {
	}

	public bool VaidateDoc(string pSiteCd,string pDoc,out string pNGWord) {
		bool bOK = true;
		string sRegNGWord = "";
		ArrayList arrStrNGWord = new ArrayList();
		
		pNGWord = "";
		sRegNGWord = getNGWordReg(pSiteCd);
		
		string sDoc = HttpUtility.HtmlDecode(pDoc);
		
		Regex rgxHtmlTag = new Regex(@"<(""[^""]*""|'[^']*'|[^'"">])*>");
		sDoc = rgxHtmlTag.Replace(sDoc,"");
		//正規表現用NGワード
		if (sRegNGWord.Equals("") == false) {
			Regex rgx = new Regex(sRegNGWord);
			Match rgxMatch = rgx.Match(sDoc);
			if (rgxMatch.Success) {
				bOK = false;
				pNGWord = string.Format("「{0}」の単語が不正です。<BR />",rgxMatch.Value);
			}
		}
		//通常文章用NGワード
		if (bOK) {
			arrStrNGWord = getNgWordStr(pSiteCd);
			if (arrStrNGWord.Count > 0) {
				for (int i = 0;i < arrStrNGWord.Count;i++) {
					if (sDoc.IndexOf(arrStrNGWord[i].ToString()) >= 0) {
						bOK = false;
						pNGWord = string.Format("「{0}」の単語が不正です。<BR />",arrStrNGWord[i].ToString());
						break;
					}
				}
			}
		}

		return bOK;
	}

	public string getNGWordReg(string pSiteCd) {
		string sRegNGWord;
		sRegNGWord = "";
		DataSet ds;
		conn = DbConnect("NGWord.NGWord");
		using (cmd = CreateSelectCommand("SELECT NG_WORD FROM T_NG_WORD WHERE SITE_CD = :SITE_CD AND NG_WORD_MARK_TYPE = :NG_WORD_MARK_TYPE",conn))
		using (ds = new DataSet())
		using (da = new OracleDataAdapter(cmd)) {
			cmd.Parameters.Add("SITE_CD",pSiteCd);
			cmd.Parameters.Add("NG_WORD_MARK_TYPE",ViCommConst.NG_WORD_MARK_TYPE_REGULAR);
			da.Fill(ds,"T_NG_WORD");
			foreach (DataRow dr in ds.Tables[0].Rows) {
				sRegNGWord += dr["NG_WORD"].ToString() + "|";

			}
		}
		if (sRegNGWord.Length > 1) {
			sRegNGWord = sRegNGWord.Substring(0,sRegNGWord.Length - 1);
		}
		conn.Close();

		return sRegNGWord;
	}

	public ArrayList getNgWordStr(string pSiteCd) {
		DataSet strDs;
		ArrayList arrNGWord = new ArrayList();
		arrNGWord.Clear();

		conn = DbConnect("NGWord.NGWord");
		using (cmd = CreateSelectCommand("SELECT NG_WORD FROM T_NG_WORD WHERE SITE_CD = :SITE_CD AND NG_WORD_MARK_TYPE =: NG_WORD_MARK_TYPE",conn))
		using (strDs = new DataSet())
		using (da = new OracleDataAdapter(cmd)) {
			cmd.Parameters.Add("SITE_CD",pSiteCd);
			cmd.Parameters.Add("NG_WORD_MARK_TYPE",ViCommConst.NG_WORD_MARK_TYPE_STRING);
			da.Fill(strDs,"T_NG_WORD");
			foreach (DataRow dr in strDs.Tables[0].Rows) {
				arrNGWord.Add(dr["NG_WORD"].ToString());
			}
		}
		conn.Close();

		return arrNGWord;
	}
}
