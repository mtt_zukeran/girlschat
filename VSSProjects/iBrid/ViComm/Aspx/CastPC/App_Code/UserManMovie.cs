﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Cast PC
--	Title			: 男性会員動画
--	Progaram ID		: UserManMovie
--
--  Creation Date	: 2010.06.08
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  XXXX/XX/XX  XXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using ViComm;

public class UserManMovie:DbSession {

	public string siteCd;
	public string loginId;
	public string userSeq;
	public string userCharNo;
	public string movieSeq;
	public string movieTitle;
	public int movieType;

	public UserManMovie() {
	}

	public bool GetOne(string pSiteCd,string pUserSeq,string pUserCharNo,string pMovieSeq) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;

		conn = DbConnect();

		string sSql = "SELECT " +
		                    "SITE_CD			," +
		                    "USER_SEQ			," +
		                    "USER_CHAR_NO		," +
		                    "LOGIN_ID			," +
		                    "MOVIE_SEQ			," +
		                    "MOVIE_TITLE		," +
							"MOVIE_TYPE			" +
		                "FROM " +
		                    "VW_USER_MAN_MOVIE01 " +
						"WHERE " +
							"SITE_CD		= :SITE_CD		AND " +
							"USER_SEQ		= :USER_SEQ		AND " +
							"USER_CHAR_NO	= :USER_CHAR_NO	AND " +
							"MOVIE_SEQ		= :MOVIE_SEQ ";

		using (cmd = CreateSelectCommand(sSql,conn))
		using (ds = new DataSet()) {
			cmd.Parameters.Add("SITE_CD",pSiteCd);
			cmd.Parameters.Add("USER_SEQ",pUserSeq);
			cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
			cmd.Parameters.Add("MOVIE_SEQ",pMovieSeq);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"VW_USER_MAN_MOVIE01");
				if (ds.Tables["VW_USER_MAN_MOVIE01"].Rows.Count != 0) {
					dr = ds.Tables["VW_USER_MAN_MOVIE01"].Rows[0];
					siteCd = dr["SITE_CD"].ToString();
					userSeq = dr["USER_SEQ"].ToString();
					userCharNo = dr["USER_CHAR_NO"].ToString();
					loginId = dr["LOGIN_ID"].ToString();
					movieSeq = dr["MOVIE_SEQ"].ToString();
					movieTitle = dr["MOVIE_TITLE"].ToString();
					movieType = int.Parse(dr["MOVIE_TYPE"].ToString());
					bExist = true;
				}
			}
		}
		conn.Close();
		return bExist;
	}
}