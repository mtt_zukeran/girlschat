/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: 出演者カテゴリー
--	Progaram ID		: ActCategory
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class ActCategory:DbSession {

	public ActCategory() {
	}


	public DataSet GetList(string pSiteCd) {
		DataSet ds;

		conn = DbConnect();
		ds = new DataSet();

		string sSql = "SELECT * FROM VW_ACT_CATEGORY01 WHERE SITE_CD = :SITE_CD AND NA_FLAG = 0 AND ACT_CATEGORY_SEQ != :ACT_CATEGORY_SEQ";
		 
		sSql = sSql + " ORDER BY SITE_CD,PRIORITY";

		using (cmd = CreateSelectCommand(sSql,conn)) {
			cmd.Parameters.Add("SITE_CD",pSiteCd);
			cmd.Parameters.Add("ACT_CATEGORY_SEQ",ViCommConst.DEFAULT_ACT_CATEGORY_SEQ);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds);
			}
		}
		conn.Close();
		return ds;
	}

}
