/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: 出演者--	Progaram ID		: Cast
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  2009/12/01  tokunaga   userCharNoの追加

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class Cast:DbSession {
	public string userSeq;
	public string userCharNo;
	public string loginId;
	public string uri;
	public bool onlineLiveEnableFlag;

	public Cast() {
	}

	public DataSet GetOne(string pLoginID) {
		DataSet ds;
		conn = DbConnect();
		ds = new DataSet();
		string sSql = "SELECT " +
						"USER_SEQ				," +
						"PRODUCTION_SEQ			," +
						"MANAGER_SEQ			," +
						"CAST_NM				," +
						"CAST_KANA_NM			," +
						"LOGIN_ID				," +
						"LOGIN_PASSWORD			," +
						"USER_STATUS_NM			," +
						"SUBSTRB(EMAIL_ADDR,1,30) EMAIL_ADDR," +
						"USE_TERMINAL_TYPE_NM	," +
						"URI					," +
						"TEL					," +
						"ONLINE_LIVE_ENABLE_FLAG," +
						"PRODUCTION_NM			," +
						"MANAGER_NM				," +
						"END_DATE				," +
						"UPDATE_DATE			," +
						"REGIST_DATE			 " +
						"FROM VW_CAST06 WHERE LOGIN_ID =:LOGIN_ID";
		using (cmd = CreateSelectCommand(sSql,conn)) {
			cmd.Parameters.Add("LOGIN_ID",pLoginID);
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds);
			}
		}
		conn.Close();
		return ds;
	}

	public void SetTalkTatus(string pUserSeq,int pTalkOnFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SET_TALK_STATUS");
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PTALK_ON_FLAG",DbSession.DbType.NUMBER,pTalkOnFlag);
			db.ProcedureInParm("pUPDATE_SITE_CD",DbSession.DbType.VARCHAR2,string.Empty);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public string UriCreate(string pUserSeq) {
		string sIvpLocCd = "";
		string sIvpSiteCd = "";
		string sUrl = "";
		string sSipId = "";
		string sSipPw = "";
		string sResult = string.Empty;

		using (Sys oSys = new Sys()) {
			oSys.GetValue("SIP_IVP_LOC_CD",out sIvpLocCd);
			oSys.GetValue("SIP_IVP_SITE_CD",out sIvpSiteCd);
			oSys.GetValue("SIP_REGIST_URL",out sUrl);

			if ((!sIvpLocCd.Equals("")) && (!sIvpSiteCd.Equals("")) && (!sUrl.Equals(""))) {
				sResult = ViCommInterface.RegistSIPTerminal(string.Format("{0}?loc={1}&site={2}",sUrl,sIvpLocCd,sIvpSiteCd),ref sSipId,ref sSipPw);
			}

		}
		if (sResult.Equals("0")) {
			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("URI_MAINTE");
				db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
				db.ProcedureInParm("PURI",DbSession.DbType.VARCHAR2,sSipId);
				db.ProcedureInParm("PTERMINAL_PASSWORD",DbSession.DbType.VARCHAR2,sSipPw);
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
				db.ExecuteProcedure();
			}
		}

		return sSipId;
	}
}
