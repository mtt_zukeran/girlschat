﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: 出演者属性種別値
--	Progaram ID		: CastAttrTypeValue
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class CastAttrTypeValue:DbSession {

	public CastAttrTypeValue() {
	}

	public DataSet GetList(string pSiteCd,string pCastAttrTypeSeq) {
		DataSet ds;

		conn = DbConnect();
		ds = new DataSet();

		string sSql = "SELECT * FROM T_CAST_ATTR_TYPE_VALUE WHERE  SITE_CD =:SITE_CD AND CAST_ATTR_TYPE_SEQ = :CAST_ATTR_TYPE_SEQ "
				+ " ORDER BY SITE_CD,CAST_ATTR_TYPE_SEQ,PRIORITY";

		using (cmd = CreateSelectCommand(sSql,conn)) {
			cmd.Parameters.Add("SITE_CD",pSiteCd);
			cmd.Parameters.Add("CAST_ATTR_TYPE_SEQ",pCastAttrTypeSeq);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds);
			}
		}

		conn.Close();

		return ds;
	}

}
