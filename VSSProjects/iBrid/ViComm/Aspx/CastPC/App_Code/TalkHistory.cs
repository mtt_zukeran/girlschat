﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: 会話履歴
--	Progaram ID		: TalkHistory
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class TalkHistory:DbSession {

	public TalkHistory() {
	}

	public int GetPageCount(string pSiteCd,string pUserSeq,string pUserCharNo) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;

		conn = DbConnect();

		string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_TALK_HISTORY02 ";

		string sWhere = "";
		OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,ref sWhere);
		sSql = sSql + sWhere;

		using (cmd = CreateSelectCommand(sSql,conn))
		using (da = new OracleDataAdapter(cmd))
		using (ds = new DataSet()) {
			for (int i = 0;i < objParms.Length;i++) {
				cmd.Parameters.Add((OracleParameter)objParms[i]);
			}

			da.Fill(ds);
			if (ds.Tables[0].Rows.Count != 0) {
				dr = ds.Tables[0].Rows[0];
				iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
			}
		}

		conn.Close();
		return iPageCount;
	}


	public DataSet GetPageCollection(string pSiteCd,string pUserSeq,string pUserCharNo,int startRowIndex,int maximumRows) {
		DataSet ds;
		conn = DbConnect();
		ds = new DataSet();

		string sOrder = "ORDER BY SITE_CD,PARTNER_USER_SEQ,PARTNER_USER_CHAR_NO,USER_CHAR_NO,TALK_START_DATE DESC ";
		string sSql = "SELECT " +
							"SITE_CD				," +
							"USER_SEQ				," +
							"USER_CHAR_NO			," +
							"PARTNER_USER_SEQ		," +
							"PARTNER_USER_CHAR_NO	," +
							"BAL_POINT				," +
							"CHARACTER_ONLINE_STATUS," +
							"TALK_START_DATE		," +
							"TALK_END_DATE			," +
							"LAST_LOGIN_DATE		," +
							"HANDLE_NM	" +
						"FROM(" +
						" SELECT VW_TALK_HISTORY02.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_TALK_HISTORY02  ";

		string sWhere = "";
		OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,ref sWhere);
		sSql = sSql + sWhere;

		sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
		sSql = sSql + sOrder;

		using (cmd = CreateSelectCommand(sSql,conn)) {
			for (int i = 0;i < objParms.Length;i++) {
				cmd.Parameters.Add((OracleParameter)objParms[i]);
			}

			cmd.Parameters.Add("FIRST_ROW",startRowIndex);
			cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"VW_TALK_HISTORY02");
			}
		}
		conn.Close();
		return ds;
	}


	private OracleParameter[] CreateWhere(string pSiteCd,string pUserSeq,string pUserCharNo,ref string pWhere) {
		ArrayList list = new ArrayList();

		if (!pSiteCd.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if (!pUserSeq.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("PARTNER_USER_SEQ = :PARTNER_USER_SEQ",ref pWhere);
			list.Add(new OracleParameter("PARTNER_USER_SEQ",pUserSeq));
			if (!string.IsNullOrEmpty(pUserCharNo)) {
				iBridCommLib.SysPrograms.SqlAppendWhere("PARTNER_USER_CHAR_NO = :PARTNER_USER_CHAR_NO",ref pWhere);
				list.Add(new OracleParameter("PARTNER_USER_CHAR_NO",pUserCharNo));
			}
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}


	public void GetTalkCount(string pSiteCd,string pUserSeq,string pUserCharNo,string pCastSeq,string pCastCharNo,out int pTalkCount,out string pLastTalkDay) {

		string sTalkHistory = "SELECT NVL(COUNT(*),0) AS CNT, TO_CHAR(MAX(TALK_START_DATE),'YY/MM/DD HH24:MI') AS LAST_DATE  FROM T_TALK_HISTORY " +
			   "WHERE " +
					"SITE_CD				= :SITE_CD			AND " +
					"USER_SEQ				= :USER_SEQ			AND " +
					"USER_CHAR_NO			= :USER_CHAR_NO		AND " +
					"PARTNER_USER_SEQ		= :PARTNER_USER_SEQ	AND " +
					"PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO ";

		pTalkCount = 0;
		pLastTalkDay = "";

		conn = DbConnect();

		using (DataSet ds = new DataSet())
		using (cmd = CreateSelectCommand(sTalkHistory,conn)) {
			cmd.Parameters.Add("SITE_CD",pSiteCd);
			cmd.Parameters.Add("USER_SEQ",pUserSeq);
			cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
			cmd.Parameters.Add("PARTNER_USER_SEQ",pCastSeq);
			cmd.Parameters.Add("PARTNER_USER_CHAR_NO",pCastCharNo);
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"T_TALK_HISTORY");
				if (ds.Tables[0].Rows.Count != 0) {
					DataRow dr = ds.Tables[0].Rows[0];
					pTalkCount = int.Parse(dr["CNT"].ToString());
					pLastTalkDay = dr["LAST_DATE"].ToString();
				}
			}
		}
		conn.Close();
	}
}
