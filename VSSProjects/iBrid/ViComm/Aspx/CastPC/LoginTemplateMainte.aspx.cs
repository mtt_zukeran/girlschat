﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: ログインメールメンテナンス
--	Progaram ID		: LoginTemplateMainte
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class LoginTemplateMainte:System.Web.UI.Page {
	private SessionObjs userObjs;

	protected void Page_Load(object sender,EventArgs e) {
		userObjs = (SessionObjs)Session["objs"];
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.DOCOMO);

		if (Request.UrlReferrer != null) {
			txtHtmlDoc.EditorDirectory = string.Format("http://{0}:{1}/pinEdit/editor/",Request.UrlReferrer.Host,Request.UrlReferrer.Port);
		}
		if (!IsPostBack) {
			FirstLoad();
			InitPage();
		}
	}

	private void FirstLoad() {
		grdTemplate.PageSize = 999;
	}

	private void InitPage() {
		lblHtmlDocSeq.Text = "";
		lblMailTemplateNo.Text = "";
		ClearField();
		pnlDtl.Visible = false;
	}

	private void ClearField() {
		txtHtmlDocTitle.Text = "";
		txtHtmlDoc.Text = "";
	}

	private void GetList() {
		grdTemplate.PageIndex = 0;
		grdTemplate.DataBind();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		EndEdit();
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void btnTxMail_Click(object sender,EventArgs e) {

		if (IsValid) {
			DataSet dsSite;
			using (Site oSite = new Site()) {
				dsSite = oSite.GetNewVerList();
			}
			using (MailLog oMailLog = new MailLog()) {
				foreach (DataRow dr in dsSite.Tables[0].Rows) {
					oMailLog.TxLoginMail(dr["SITE_CD"].ToString(),userObjs.cast.userSeq,ViComm.ViCommConst.MAIN_CHAR_NO); 
				}
			}
			Response.Redirect("~/Cast/MainView.aspx");
		}
	}

	private void GetData() {
		string sSubSeq = "";

		using (MailTemplate oTemplate = new MailTemplate()) {
			sSubSeq = oTemplate.GetModifiableSubSeq(userObjs.site.siteCd,lblMailTemplateNo.Text);
		}

		using (DbSession db = new DbSession()) {

			db.PrepareProcedure("CAST_MAIL_TEMPLATE_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,userObjs.site.siteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,userObjs.cast.userSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,userObjs.cast.userCharNo);
			db.ProcedureInParm("PMAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2,lblMailTemplateNo.Text);
			db.ProcedureInParm("PHTML_DOC_SUB_SEQ",DbSession.DbType.VARCHAR2,sSubSeq);
			db.ProcedureOutParm("PMAIL_TITLE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PHTML_DOC_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PHTML_DOC_TITLE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutArrayParm("PHTML_DOC",DbSession.DbType.VARCHAR2,SysConst.MAX_HTML_BLOCKS);
			db.ProcedureOutParm("PROWID_VIEW",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO_VIEW",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID_MANAGE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO_MANAGE",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID_DOC",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO_DOC",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);

			db.ExecuteProcedure();

			ViewState["REVISION_NO_VIEW"] = db.GetStringValue("PREVISION_NO_VIEW");
			ViewState["ROWID_VIEW"] = db.GetStringValue("PROWID_VIEW");

			ViewState["REVISION_NO_MANAGE"] = db.GetStringValue("PREVISION_NO_MANAGE");
			ViewState["ROWID_MANAGE"] = db.GetStringValue("PROWID_MANAGE");

			ViewState["REVISION_NO_DOC"] = db.GetStringValue("PREVISION_NO_DOC");
			ViewState["ROWID_DOC"] = db.GetStringValue("PROWID_DOC");

			lblHtmlDocSeq.Text = db.GetStringValue("PHTML_DOC_SEQ");

			txtHtmlDoc.Text = "";
			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtMailTitle.Text = db.GetStringValue("PMAIL_TITLE");
				txtHtmlDocTitle.Text = db.GetStringValue("PHTML_DOC_TITLE");
				for (int i = 0;i < SysConst.MAX_HTML_BLOCKS;i++) {
					txtHtmlDoc.Text = txtHtmlDoc.Text + db.GetArryStringValue("PHTML_DOC",i);
				}
			} else {
				ClearField();
			}
		}

		pnlDtl.Visible = true;
		pnlKey.Enabled = false;
	}

	private void UpdateData(int pDelFlag) {
		string[] sDoc;
		int iDocCount;

		SysPrograms.SeparateHtml(HttpUtility.HtmlDecode(txtHtmlDoc.Text),ViCommConst.MAX_ORG_MAIL_BLOCKS,out sDoc,out iDocCount);

		string sSubSeq = "";

		using (MailTemplate oTemplate = new MailTemplate()) {
			sSubSeq = oTemplate.GetModifiableSubSeq(userObjs.site.siteCd,lblMailTemplateNo.Text);
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_MAIL_TEMPLATE_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,userObjs.site.siteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,userObjs.cast.userSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,userObjs.cast.userCharNo);     // modify UCN
			db.ProcedureInParm("PMAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2,lblMailTemplateNo.Text);
			db.ProcedureInParm("PHTML_DOC_SUB_SEQ",DbSession.DbType.VARCHAR2,sSubSeq);
			db.ProcedureInParm("PMAIL_TITLE",DbSession.DbType.VARCHAR2,txtMailTitle.Text);
			db.ProcedureInParm("PHTML_DOC_TITLE",DbSession.DbType.VARCHAR2,txtHtmlDocTitle.Text);
			db.ProcedureInArrayParm("PHTML_DOC",DbSession.DbType.VARCHAR2,iDocCount,sDoc);
			db.ProcedureInParm("PHTML_DOC_COUNT",DbSession.DbType.NUMBER,iDocCount);
			db.ProcedureInParm("PROWID_VIEW",DbSession.DbType.VARCHAR2,ViewState["ROWID_VIEW"].ToString());
			db.ProcedureInParm("PREVISION_NO_VIEW",DbSession.DbType.NUMBER,ViewState["REVISION_NO_VIEW"].ToString());
			db.ProcedureInParm("PROWID_MANAGE",DbSession.DbType.VARCHAR2,ViewState["ROWID_MANAGE"].ToString());
			db.ProcedureInParm("PREVISION_NO_MANAGE",DbSession.DbType.NUMBER,ViewState["REVISION_NO_MANAGE"].ToString());
			db.ProcedureInParm("PROWID_DOC",DbSession.DbType.VARCHAR2,ViewState["ROWID_DOC"].ToString());
			db.ProcedureInParm("PREVISION_NO_DOC",DbSession.DbType.NUMBER,ViewState["REVISION_NO_DOC"].ToString());
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PHTML_DOC_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			lblHtmlDocSeq.Text = db.GetStringValue("PHTML_DOC_SEQ");
		}
		EndEdit();
	}

	protected void lnkTemplate_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		userObjs.site.GetOne(sKeys[0]);
		lblHtmlDocSeq.Text = string.Concat(sKeys[1], "-", sKeys[3]);
		lblMailTemplateNo.Text = sKeys[2];
        userObjs.cast.userCharNo = sKeys[3];
		pnlMainte.Visible = true;
		pnlDtl.Visible = false;
		GetData();
	}

	protected void lnkDoc_Command(object sender,CommandEventArgs e) {
		GetData();
	}

	protected void dsSiteHtmlDoc_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lblHtmlDocSeq.Text;
	}

	protected void dsMailTemplate_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = "";
		e.InputParameters[1] = userObjs.cast.userSeq;
		e.InputParameters[2] = "";
		e.InputParameters[3] = ViCommConst.MAIL_TP_NOTIFY_LOGIN;
	}

	private void EndEdit() {
		ClearField();
		pnlDtl.Visible = false;
		pnlKey.Enabled = true;
		grdTemplate.DataBind();
	}
}
