﻿<%@ Application Language="C#" %>
<%@ Import Namespace="System.Diagnostics" %>

<script RunAt="server">
	void Application_Start(object sender,EventArgs e) {
		DateTime dt = DateTime.Parse("2008/06/11 14:00:28");
	}

		void Application_End(object sender,EventArgs e) {
		}

		void Application_Error(object sender,EventArgs e) {
			string sRoot = ConfigurationManager.AppSettings["Root"].ToString().ToLower();
			string sSource = "ViCOMM";
			string sLog = "Application";
			Exception objErr = Server.GetLastError().GetBaseException();

			string err = "ViCOMM CastPC Error Caught in Application_Error event\n" +
			"Error in: " + Request.Url.ToString() +
			"\nError Message:" + objErr.Message.ToString() +
			"\nStack Trace:" + objErr.StackTrace.ToString();

			if (!EventLog.SourceExists(sSource)) {
				EventLog.CreateEventSource(sSource,sLog);
			}

			EventLog.WriteEntry(sSource,err,EventLogEntryType.Error);
			Server.ClearError();
			Response.Redirect(sRoot + "/error/WebError.aspx?&errtype=" + ViComm.ViCommConst.USER_INFO_INTERNAL_ERROR);
		}

		void Session_Start(object sender,EventArgs e) {
			string sRoot = ConfigurationManager.AppSettings["Root"].ToString().ToLower();
			string sUrl = Request.Url.ToString().ToLower();
			if (sUrl.IndexOf(sRoot) > 0) {
				if ((sUrl.ToString().IndexOf("login.aspx") < 0) && ((sUrl.ToString().IndexOf("SipCommStatus.aspx.aspx") < 0))) {
					if (Session["site"] == null) {
						Response.Redirect(sRoot + "/error/WebError.aspx?errtype=" + ViComm.ViCommConst.USER_INFO_TIMEOUT);
					}
				}
			}
		}

		void Session_End(object sender,EventArgs e) {
		}
       
</script>

