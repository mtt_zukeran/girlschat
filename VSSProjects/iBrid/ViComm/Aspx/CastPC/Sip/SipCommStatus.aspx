<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SipCommStatus.aspx.cs" Inherits="Sip_SipCommStatus" Title="ＳＩＰ通話状態" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="ＳＩＰ通話状態"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnkMain">
			<asp:FormView ID="FormUserInfo" runat="server" DataSourceID="dsIVPRequest" OnDataBound="FormView1_DataBound">
				<ItemTemplate>
					<table>
						<tr>
							<td valign="top">
								<table border="0" style="width: 140px" class="tableStyle">
									<tr>
										<td align="center">
											<asp:Label ID="lblSiteNm" runat="server" Text='<%# Eval("SITE_NM")%>' Font-Bold="True" ForeColor="#FF8000"></asp:Label>
											<asp:Label ID="lblUserManSeq" runat="server" Text='<%# Eval("MAN_USER_SEQ")%>'></asp:Label>
										</td>
									</tr>
									<tr>
										<td align="center">
											<asp:Label ID="lblCastHandelNm" runat="server" Text='<%# Eval("CAST_HANDLE_NM") %>' Font-Bold="True" Font-Italic="False" Font-Strikeout="False" Font-Underline="True"
												ForeColor="Red"></asp:Label>
										</td>
									</tr>
									<tr>
										<td align="center">
											<asp:Label ID="lblCallStatus" runat="server" Text='<%#GetCallStatus() %>' Font-Bold='<%#IsBold()%>' ForeColor='<%# GetForeColor() %>'></asp:Label><br />
											<asp:LinkButton ID="lnkRecoding" runat="server" Text="<%# GetRecTitle() %>" Visible='<%# CanRecording(Eval("CHARGE_TYPE")) %>' CommandArgument='<%#string.Format("{0}:{1}",Eval("IVP_ACCEPT_SEQ"),Eval("REQUEST_SITE_CD")) %>'
												OnCommand="lnkRecoding_Command"></asp:LinkButton>
										</td>
									</tr>
								</table>
							</td>
							<td width="4">
							</td>
							<td valign="top">
								<asp:Panel ID="pnlManInfo" runat="server">
									<table border="0" style="width: 500px" class="tableStyle">
										<tr>
											<td class="tdHeaderSmallStyle">
												ﾕｰｻﾞｰﾗﾝｸ
											</td>
											<td class="tdDataStyle">
												<asp:Label ID="Label6" runat="server" Text='<%# Eval("USER_RANK_NM") %>'></asp:Label>
											</td>
											<td class="tdHeaderSmallStyle">
												ﾊﾝﾄﾞﾙ名
											</td>
											<td class="tdDataStyle">
												<asp:LinkButton ID="lnkHandleNm" runat="server" Text='<%# Eval("HANDLE_NM") %>' OnClientClick='<%# GetManUrl(Eval("REQUEST_SITE_CD"),Eval("MAN_USER_SEQ"),Eval("CAST_CHAR_NO")) %>'></asp:LinkButton>
											</td>
											<td class="tdHeaderSmallStyle">
												生年月日
											</td>
											<td class="tdDataStyle">
												<asp:Label ID="Label5" runat="server" Text='<%# Eval("BIRTHDAY") %>'></asp:Label>
											</td>
										</tr>
										<tr>
											<td class="tdHeaderSmallStyle">
												会話種別
											</td>
											<td class="tdDataStyle">
												<asp:Label ID="Label7" runat="server" Text='<%# Eval("CHARGE_TYPE_NM") %>'></asp:Label>
											</td>
											<td class="tdHeaderSmallStyle">
												<%# Eval("MAN_ATTR_TYPE_NM1")%>
											</td>
											<td class="tdDataStyle">
												<asp:Label ID="Label1" runat="server" Text='<%# Eval("MAN_ATTR_VALUE1") %>'></asp:Label>
											</td>
											<td class="tdHeaderSmallStyle">
												登録日
											</td>
											<td class="tdDataStyle">
												<asp:Label ID="Label9" runat="server" Text='<%# Eval("REGIST_DATE") %>' DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False"></asp:Label>
											</td>
										</tr>
										<tr>
											<td class="tdHeaderSmallStyle2">
												<%# Eval("MAN_ATTR_TYPE_NM2")%>
											</td>
											<td class="tdDataStyle">
												<asp:Label ID="Label2" runat="server" Text='<%# Eval("MAN_ATTR_VALUE2") %>'></asp:Label>
											</td>
											<td class="tdHeaderSmallStyle2">
												会話回数
											</td>
											<td class="tdDataStyle">
												<asp:Label ID="Label4" runat="server" Text='<%# string.Format("{0}回",Eval("TALK_COUNT")) %>'></asp:Label>
											</td>
											<td class="tdHeaderSmallStyle">
												最終会話
											</td>
											<td class="tdDataStyle">
												<asp:Label ID="Label3" runat="server" Text='<%# Eval("LAST_TALK_DATE") %>' DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False"></asp:Label>
											</td>
										</tr>
										<tr>
											<td class="tdHeaderSmallStyle">
												ｺﾒﾝﾄ
											</td>
											<td class="tdDataStyle" colspan="4">
												<asp:Label ID="lblComment" runat="server" Text='<%# Eval("FAVORIT_COMMENT") %>'></asp:Label>
											</td>
											<td align="center">
												<asp:LinkButton ID="lnkHide" runat="server" Text="非表示" OnCommand="lnkHide_Command"></asp:LinkButton>
											</td>
										</tr>
									</table>
								</asp:Panel>
							</td>
						</tr>
					</table>
				</ItemTemplate>
			</asp:FormView>
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsIVPRequest" runat="server" SelectMethod="GetOutgoingSession" TypeName="IvpRequest" OnSelecting="dsIVPRequest_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pTerminalId" Type="String" />
			<asp:Parameter Name="pSessionFlag" Type="Int16" />
			<asp:Parameter Name="pIvpRequestSeq" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>
