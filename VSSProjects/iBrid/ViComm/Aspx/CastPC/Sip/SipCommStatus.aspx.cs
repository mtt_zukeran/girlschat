﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: SIP通話状態
--	Progaram ID		: SipCommStatus
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net;
using System.Drawing;
using System.IO;
using iBridCommLib;
using ViComm;

public partial class Sip_SipCommStatus:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			FirstLoad();
			InitPage();
		}
	}

	private void FirstLoad() {
		ViewState["SIP_ID"] = iBridUtil.GetStringValue(Request.QueryString["sipid"]);
		ViewState["STATUS"] = iBridUtil.GetStringValue(Request.QueryString["status"]);
	}

	private void InitPage() {
		ClearField();
		DataBind();
	}

	private void ClearField() {
	}

	protected void dsIVPRequest_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = ViewState["SIP_ID"].ToString();

		switch (ViewState["STATUS"].ToString()) {
			case "1":
				e.InputParameters[1] = 1;
				e.InputParameters[2] = "";
				break;
			case "2":
				e.InputParameters[1] = 1;
				e.InputParameters[2] = iBridUtil.GetStringValue(Session["REQUEST_SEQ"]);
				break;
			case "3":
				e.InputParameters[1] = 0;
				e.InputParameters[2] = iBridUtil.GetStringValue(Session["REQUEST_SEQ"]);
				break;
		}
	}

	protected string GetManUrl(object pSiteCd,object pManUserSeq,object pCastCharNo) {
		return string.Format("top.cast.location.href='../man/ManView.aspx?sitecd={0}&manuserseq={1}&castcharno={2}';return;",pSiteCd.ToString(),pManUserSeq.ToString(),pCastCharNo.ToString());
	}

	protected string GetCallStatus() {
		string sStatus = "";
		switch (ViewState["STATUS"].ToString()) {
			case "1":
				sStatus = "呼出中";
				break;
			case "2":
				sStatus = "会話中";
				break;
			case "3":
				sStatus = "終了";
				break;
		}
		return sStatus;
	}

	protected bool IsBold() {
		if (!ViewState["STATUS"].ToString().Equals("3")) {
			return true;
		} else {
			return false;
		}
	}

	protected Color GetForeColor() {
		Color oColor;
		switch (ViewState["STATUS"].ToString()) {
			case "1":
				oColor = Color.Red;
				break;
			case "2":
				oColor = Color.Blue;
				break;
			default:
				oColor = Color.Black;
				break;
		}
		return oColor;
	}

	protected void FormView1_DataBound(object sender,EventArgs e) {
		object oItem = ((FormView)(sender)).DataItem;

		if (oItem == null) {
			return;
		}

		Session["REQUEST_SEQ"] = ((DataRowView)(((FormView)(sender)).DataItem)).DataView.Table.Rows[0]["IVP_REQUEST_SEQ"].ToString();
		string sManSeq = ((DataRowView)(((FormView)(sender)).DataItem)).DataView.Table.Rows[0]["MAN_USER_SEQ"].ToString();
		Panel pnl = (Panel)FormUserInfo.FindControl("pnlManInfo");

		if (sManSeq.Equals("")) {
			pnl.Visible = false;
		} else {
			pnl.Visible = true;
		}
	}

	protected bool CanRecording(object pChargeType) {
		if (ViewState["STATUS"].ToString().Equals("2")) {
			if (pChargeType.ToString().Equals(ViCommConst.CHARGE_TALK_PUBLIC)) {
				return true;
			}
		}
		return false;
	}

	protected string GetRecTitle() {
		if (ViewState["STATUS"].ToString().Equals("2")) {
			if (iBridUtil.GetStringValue(ViewState["RECORDING"]).Equals("1")) {
				return "録画の停止はここ";
			} else {
				return "録画の開始はここ";
			}
		}
		return "";
	}

	protected void lnkRecoding_Command(object sender,CommandEventArgs e) {
		int iStopFlag = 0;
		if (iBridUtil.GetStringValue(ViewState["RECORDING"]).Equals("1")) {
			iStopFlag = 1;
		}

		LinkButton lnkRec = (LinkButton)FormUserInfo.FindControl("lnkRecoding");
		Label lblStatus = (Label)FormUserInfo.FindControl("lblCallStatus");

		using (IvpRequest oRequest = new IvpRequest()) {
			string[] sKeys = e.CommandArgument.ToString().Split(':');

			if (oRequest.RecordingRequest(sKeys[1],sKeys[0],iStopFlag)) {
				if (iStopFlag == 0) {
					ViewState["RECORDING"] = "1";
					lnkRec.Text = "録画の停止はここ";
					lblStatus.Text = "会話録画中";
					lblStatus.ForeColor = Color.Red;
				} else {
					ViewState["RECORDING"] = "0";
					lnkRec.Text = "録画の開始はここ";
					lblStatus.Text = "会話中";
					lblStatus.ForeColor = Color.Blue;
				}
			}
		}
	}

	protected string GetCurPoint(object pBalPoint,object pServicePoint,object pEffectDate,object pInviteTalkServicePoint) {
		if (pBalPoint.ToString().Equals("")) {
			return "";
		}

		int iPoint = int.Parse(pBalPoint.ToString()) + int.Parse(pInviteTalkServicePoint.ToString());
		if (!pEffectDate.ToString().Equals("")) {
			DateTime dtEffect = DateTime.Parse(pEffectDate.ToString());
			if (dtEffect > DateTime.Now) {
				iPoint += int.Parse(pServicePoint.ToString());
			}
		}
		return string.Format("{0}Ｐ",iPoint);
	}

	protected void lnkHide_Command(object sender,CommandEventArgs e) {
		//		Session.RemoveAll();
		Response.Redirect("~/Default.aspx");
	}

}
