﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: 会員詳細
--	Progaram ID		: ManView
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------
  Date        Updater    Update Explain
-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using iBridCommLib;
using ViComm;

public partial class Man_ManView:System.Web.UI.Page {
	private SessionObjs userObjs;
	protected void Page_Load(object sender,EventArgs e) {
		userObjs = (SessionObjs)Session["objs"];
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.DOCOMO);
		Response.Cache.SetNoStore();
		txtMailDoc.EditorDirectory = string.Format("http://{0}:{1}/pinEdit/editor/",Request.UrlReferrer.Host,Request.UrlReferrer.Port);

		if (!IsPostBack) {
			FirstLoad();
			InitPage();
		}
		userObjs.site.GetOne(iBridUtil.GetStringValue(ViewState["SITE_CD"]));
	}

	private void FirstLoad() {
		ViewState["SITE_CD"] = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		ViewState["MAN_USER_SEQ"] = iBridUtil.GetStringValue(Request.QueryString["manuserseq"]);
		ViewState["CAST_CHAR_NO"] = iBridUtil.GetStringValue(Request.QueryString["castcharno"]);
		ViewState["RETURN"] = iBridUtil.GetStringValue(Request.QueryString["return"]);

		if (userObjs.site.GetOne(iBridUtil.GetStringValue(ViewState["SITE_CD"]))) {
			lblSiteNm.Text = userObjs.site.siteNm;
			if (userObjs.site.IsActiveMulitCharFlag(ViewState["SITE_CD"].ToString())) {
				lblSiteNm.Text += string.Concat("-",ViewState["CAST_CHAR_NO"].ToString());
			}
		}

		using (Marking oMarking = new Marking()) {
			oMarking.MarkingMainte(
				ViewState["SITE_CD"].ToString(),
				userObjs.cast.userSeq,
				ViewState["CAST_CHAR_NO"].ToString(),
				ViewState["MAN_USER_SEQ"].ToString(),
				ViCommConst.MAIN_CHAR_NO,
				0);
		}

		lstMailTemplate.DataBind();
		lstMailTemplate.Items.Insert(0,new ListItem("",""));
		lstMailTemplate.DataSourceID = "";
		lstMailTemplate.SelectedIndex = 0;
	}

	private void InitPage() {
		this.Title = DisplayWordUtil.Replace(this.Title);
		this.lblPgmTitle.Text = DisplayWordUtil.Replace(this.lblPgmTitle.Text);
		this.Panel1.GroupingText = DisplayWordUtil.Replace(this.Panel1.GroupingText);

		ClearField();
		DataBind();

		using (Favorit oFavorit = new Favorit()) {
			if (oFavorit.GetOne(ViewState["SITE_CD"].ToString(),userObjs.cast.userSeq,ViewState["CAST_CHAR_NO"].ToString(),ViewState["MAN_USER_SEQ"].ToString(),ViCommConst.MAIN_CHAR_NO)) {
				txtCommentList.Text = oFavorit.favoritComment;
				lbRegistDay.Text = oFavorit.registDay;
				btnReleaseBookMark.Visible = true;
			} else {
				btnReleaseBookMark.Visible = false;
			}
		}
		int iCompanyMask = int.Parse(iBridUtil.GetStringValue(Session["CompanyMask"]));
		dvwUserMan.Rows[6].Visible = ((iCompanyMask & ViCommConst.MSK_COMPANY_MASHUP) <= 0);
	}

	private void ClearField() {
		lbRegistDay.Text = "";
		txtCommentList.Text = "";
		pnlFavorit.Visible = true;
		pnlMail.Visible = false;
		pnlService.Visible = false;
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		if (ViewState["RETURN"].Equals("") == false) {
			Server.Transfer(ViewState["RETURN"].ToString());
		} else {
			Server.Transfer("../Cast/MainView.aspx");
		}
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		DataBind();
		int iCompanyMask = int.Parse(iBridUtil.GetStringValue(Session["CompanyMask"]));
		dvwUserMan.Rows[6].Visible = ((iCompanyMask & ViCommConst.MSK_COMPANY_MASHUP) <= 0);
	}


	protected void btnBookMark_Click(object sender,EventArgs e) {
		UpdateFavorit(0);
	}

	protected void btnReleaseBookMark_Click(object sender,EventArgs e) {
		UpdateFavorit(1);
	}

	//拒否リスト追加用
	protected void lnkRefuse_Command(object sender,CommandEventArgs e) {
		if (e.CommandArgument.ToString().Equals("1")) {
			UpdateRefuse(1);
		} else {
			UpdateRefuse(0);
		}
	}

	protected void Page_Prerender(object sender,EventArgs e) {
		SetProfile();
	}

	protected void lstMailTemplate_SelectedIndexChanged(object sender,EventArgs e) {
		pnlService.Visible = false;

		using (CastMailTemplate oCastTemplate = new CastMailTemplate()) {
			if (oCastTemplate.GetOne(ViewState["SITE_CD"].ToString(),userObjs.cast.userSeq,ViewState["CAST_CHAR_NO"].ToString(),lstMailTemplate.SelectedValue)) {
				txtMailTitle.Text = oCastTemplate.mailTitle;
				txtMailDoc.Text = oCastTemplate.htmlDoc;

				if (oCastTemplate.mailAttachedObjType.Equals(ViCommConst.ATTACH_PIC_INDEX.ToString())) {
					//画像 
					grdPic.Visible = true;
					grdMovie.Visible = false;
					trMailDoc.Visible = false;
					imgAttachedPic.Visible = false;
					lblMovieTitle.Visible = false;
					btnMail.Enabled = false;
					ViewState["MAIL_DOC"] = txtMailDoc.Text;
				} else if (oCastTemplate.mailAttachedObjType.Equals(ViCommConst.ATTACH_MOVIE_INDEX.ToString())) {
					//動画 
					grdPic.Visible = false;
					grdMovie.Visible = true;
					trMailDoc.Visible = false;
					imgAttachedPic.Visible = false;
					lblMovieTitle.Visible = false;
					btnMail.Enabled = false;
					ViewState["ATTACHED_OBJ_TYPE"] = ViCommConst.ATTACH_MOVIE_INDEX.ToString();
					ViewState["PIC_SEQ"] = "";
					ViewState["MOVIE_SEQ"] = "";
					ViewState["MAIL_DOC"] = txtMailDoc.Text;
				} else {
					//なんもなし 
					grdPic.Visible = false;
					grdMovie.Visible = false;
					trMailDoc.Visible = true;
					imgAttachedPic.Visible = false;
					lblMovieTitle.Visible = false;
					btnMail.Enabled = true;
					ViewState["ATTACHED_OBJ_TYPE"] = "";
					ViewState["PIC_SEQ"] = "";
					ViewState["MOVIE_SEQ"] = "";
				}
			} else {
				using (MailTemplate oTemplate = new MailTemplate()) {
					if (oTemplate.GetOne(ViewState["SITE_CD"].ToString(),lstMailTemplate.SelectedValue)) {
						txtMailTitle.Text = oTemplate.mailTitle;
						txtMailDoc.Text = oTemplate.htmlDoc;

						if (oTemplate.mailTemplateType.Equals(ViCommConst.MAIL_TP_INVITE_TALK)) {
							pnlService.Visible = true;
							lstPoint.SelectedIndex = 0;
						}
						if (oTemplate.mailAttachedObjType.Equals(ViCommConst.ATTACH_PIC_INDEX.ToString())) {
							//画像 
							grdPic.Visible = true;
							grdMovie.Visible = false;
							trMailDoc.Visible = false;
							imgAttachedPic.Visible = false;
							lblMovieTitle.Visible = false;
							btnMail.Enabled = false;
							ViewState["MAIL_DOC"] = txtMailDoc.Text;
						} else if (oTemplate.mailAttachedObjType.Equals(ViCommConst.ATTACH_MOVIE_INDEX.ToString())) {
							//動画 
							grdPic.Visible = false;
							grdMovie.Visible = true;
							trMailDoc.Visible = false;
							imgAttachedPic.Visible = false;
							lblMovieTitle.Visible = false;
							btnMail.Enabled = false;
							ViewState["ATTACHED_OBJ_TYPE"] = ViCommConst.ATTACH_MOVIE_INDEX.ToString();
							ViewState["PIC_SEQ"] = "";
							ViewState["MOVIE_SEQ"] = "";
							ViewState["MAIL_DOC"] = txtMailDoc.Text;
						} else {
							//なんもなし 
							grdPic.Visible = false;
							grdMovie.Visible = false;
							trMailDoc.Visible = true;
							imgAttachedPic.Visible = false;
							lblMovieTitle.Visible = false;
							btnMail.Enabled = true;
							ViewState["ATTACHED_OBJ_TYPE"] = "";
							ViewState["PIC_SEQ"] = "";
							ViewState["MOVIE_SEQ"] = "";
						}
					} else {
						grdPic.Visible = false;
						grdMovie.Visible = false;
						trMailDoc.Visible = true;
						imgAttachedPic.Visible = false;
						lblMovieTitle.Visible = false;
						btnMail.Enabled = true;
						ViewState["ATTACHED_OBJ_TYPE"] = "";
						ViewState["PIC_SEQ"] = "";
						ViewState["MOVIE_SEQ"] = "";
						txtMailTitle.Text = "";
						txtMailDoc.Text = "";
					}
				}
			}
			if (oCastTemplate.mailTemplateType.Equals(ViCommConst.MAIL_TP_INVITE_TALK)) {
				pnlService.Visible = true;
				lstPoint.SelectedIndex = 0;
			}
		}
		if (lstMailTemplate.SelectedIndex == 0) {
			btnMail.Enabled = false;
		}
	}

	protected void btnChangeFavorit_Click(object sender,EventArgs e) {
		pnlFavorit.Visible = true;
		pnlMail.Visible = false;
		txtCommentList.Text = "";
		using (Favorit oFavorit = new Favorit()) {
			if (oFavorit.GetOne(ViewState["SITE_CD"].ToString(),userObjs.cast.userSeq,ViewState["CAST_CHAR_NO"].ToString(),ViewState["MAN_USER_SEQ"].ToString(),ViCommConst.MAIN_CHAR_NO)) {
				txtCommentList.Text = oFavorit.favoritComment;
			}
		}
	}

	protected void btnChangeMail_Click(object sender,EventArgs e) {
		pnlFavorit.Visible = false;
		pnlMail.Visible = true;
		lstMailTemplate.SelectedIndex = 0;
		txtMailTitle.Text = "";
		txtMailDoc.Text = "";
		grdPic.Visible = false;
		grdMovie.Visible = false;
		trMailDoc.Visible = true;
		imgAttachedPic.Visible = false;
		lblMovieTitle.Visible = false;
		btnMail.Enabled = false;
	}

	protected void vdcCheckMail_ServerValidate(object source,ServerValidateEventArgs args) {
		string sExisMsg;
		vdcCheckMail.ErrorMessage = "";


		if (txtMailTitle.Text.Equals("")) {
			args.IsValid = false;
			vdcCheckMail.ErrorMessage += "タイトルを入力してください。<BR />";
		}
		if (txtMailDoc.Text.Equals("")) {
			args.IsValid = false;
			vdcCheckMail.ErrorMessage += "本文を入力してください。<BR />";
		}
		using (NGWord oNGWord = new NGWord()) {
			string sDoc = HttpUtility.HtmlDecode(txtMailDoc.Text);
			if (!oNGWord.VaidateDoc(ViewState["SITE_CD"].ToString(),sDoc,out sExisMsg)) {
				args.IsValid = false;
				vdcCheckMail.ErrorMessage += sExisMsg;
			}
		}
		using (MailLog oMailLog = new MailLog()) {
			if (!oMailLog.CheckTxMail(
					ViewState["SITE_CD"].ToString(),
					userObjs.cast.userSeq,
					ViewState["CAST_CHAR_NO"].ToString(),
					ViewState["MAN_USER_SEQ"].ToString(),
					lstMailTemplate.SelectedValue,out sExisMsg)
			) {
				args.IsValid = false;
				vdcCheckMail.ErrorMessage += sExisMsg;
			}
		}
	}

	protected void btnMail_Click(object sender,EventArgs e) {
		int iServicePoint = 0;

		if (IsValid) {
			if (pnlService.Visible) {
				iServicePoint = int.Parse(lstPoint.SelectedValue);
			}

			using (MailLog oMailLog = new MailLog()) {
				oMailLog.TxCastToManMail(
						ViewState["SITE_CD"].ToString(),
						lstMailTemplate.SelectedValue,
						userObjs.cast.userSeq,
						iBridUtil.GetStringValue(ViewState["CAST_CHAR_NO"]),
						new string[] { ViewState["MAN_USER_SEQ"].ToString() },
						iServicePoint,
						txtMailTitle.Text,
						txtMailDoc.Text,
						iBridUtil.GetStringValue(ViewState["ATTACHED_OBJ_TYPE"]),
						iBridUtil.GetStringValue(ViewState["PIC_SEQ"]),
						iBridUtil.GetStringValue(ViewState["MOVIE_SEQ"])
				);
			}
			InitPage();
		}
	}

	protected string GetSysImage(object pOnlienStatus) {
		int iOnlineStatus = int.Parse(pOnlienStatus.ToString());

		switch (iOnlineStatus) {
			case ViCommConst.USER_LOGINED:
				return "../image/sys_logined.jpg";
			case ViCommConst.USER_TALKING:
				return "../image/sys_talking.jpg";
			case ViCommConst.USER_OFFLINE:
				return "../image/sys_offline.jpg";
		}
		return "";
	}


	protected string GetRefuseTitle(object pRefuseFlag) {
		if (pRefuseFlag.ToString().Equals("1")) {
			return "削除する";
		} else {
			return "追加する";
		}
	}

	//拒否リスト追加用
	protected string GetClientMsg(object pRefuseFlag) {
		if (pRefuseFlag.ToString().Equals("1")) {
			return "return confirm(\"拒否リストから削除しますか？\");";
		} else {
			return "return confirm(\"拒否リストに追加しますか？\");";
		}
	}


	protected string GetRelationTypeNm(object pRelationType) {
		string sValue = "";
		btnChangeFavorit.Visible = true;
		btnChangeMail.Visible = true;
		pnlFavorit.Visible = true;
		pnlMail.Visible = false;

		switch (pRelationType.ToString()) {
			case ViCommConst.REL_TALK:
				sValue = "会話履歴あり";
				break;
			case ViCommConst.REL_FAVOTIR:
				sValue = "お気に入り登録あり";
				break;
			case ViCommConst.REL_MARKING:
				sValue = "足あとあり";
				break;
			default:
				using (SiteManagement oSiteManage = new SiteManagement()) {
					oSiteManage.GetOne(ViewState["SITE_CD"].ToString());
					if (oSiteManage.txMailRefMarking != 0) {
						btnChangeFavorit.Visible = false;
						btnChangeMail.Visible = false;
						pnlFavorit.Visible = false;
						pnlMail.Visible = false;
					}
				}
				break;
		}
		return sValue;
	}

	protected string GetCurPoint(object pBalPoint,object pServicePoint,object pEffectDate) {
		int iPoint = int.Parse(pBalPoint.ToString());
		if (!pEffectDate.ToString().Equals("")) {
			DateTime dtEffect = DateTime.Parse(pEffectDate.ToString());
			if (dtEffect > DateTime.Now) {
				iPoint += int.Parse(pServicePoint.ToString());
			}
		}
		return string.Format("{0}Ｐ",iPoint);
	}

	protected void dsUserManCharacter_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = ViewState["SITE_CD"].ToString();
		e.InputParameters[1] = ViewState["MAN_USER_SEQ"].ToString();
		e.InputParameters[2] = userObjs.cast.userSeq;
		e.InputParameters[3] = iBridUtil.GetStringValue(ViewState["CAST_CHAR_NO"]);
	}

	protected void dsTemplate_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = ViewState["SITE_CD"].ToString();
	}

	protected string GetAge(object pBirthDay) {
		int iAge = ViCommPrograms.Age(pBirthDay.ToString());
		return iAge.ToString();
	}


	private void SetProfile() {
		DataRowView drv = (DataRowView)(dvwUserMan.DataItem);
		if (drv != null) {
			int count = int.Parse(drv.Row["COUNT"].ToString());
			for (int i = 0;i < count;i++) {
				Label lblUserManAttrNm = (Label)plcHolder.FindControl(string.Format("lblUserManAttrNm{0}",i)) as Label;
				lblUserManAttrNm.Text = drv.Row[string.Format("MAN_ATTR_TYPE_NM{0}",i)].ToString();
				Label lblUserManAttrValue = (Label)plcHolder.FindControl(string.Format("lblUserManAttrSeq{0}",i)) as Label;
				lblUserManAttrValue.Text = drv.Row[string.Format("DISPLAY_VALUE{0}",i)].ToString();
			}
			for (int i = count;i < ViCommConst.MAX_ATTR_COUNT;i++) {
				TableRow row;
				row = (TableRow)plcHolder.FindControl(string.Format("rowAttr{0}",i)) as TableRow;
				row.Visible = false;
			}
		}
	}

	private void UpdateFavorit(int pDelFlag) {
		using (Favorit oFavorit = new Favorit()) {
			oFavorit.FavoritMainte(
					ViewState["SITE_CD"].ToString(),
					userObjs.cast.userSeq,
					ViewState["CAST_CHAR_NO"].ToString(),
					ViewState["MAN_USER_SEQ"].ToString(),
					txtCommentList.Text,
					pDelFlag
					);
		}
		InitPage();
	}

	private void UpdateRefuse(int pDelFlag) {
		using (Refuse oRefuse = new Refuse()) {
			oRefuse.RefuseMainte(
					ViewState["SITE_CD"].ToString(),
					userObjs.cast.userSeq,
					iBridUtil.GetStringValue(ViewState["MAN_USER_SEQ"].ToString()),
					ViCommConst.MAIN_CHAR_NO,
					"9",
					pDelFlag
					);
		}
		InitPage();
	}

	protected void dsCastPic_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = ViewState["SITE_CD"].ToString();
		e.InputParameters[1] = userObjs.cast.userSeq;
		e.InputParameters[2] = ViewState["CAST_CHAR_NO"].ToString();
		e.InputParameters[3] = ViCommConst.ATTACHED_MAIL.ToString();
	}
	protected void dsCastMovie_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = ViewState["SITE_CD"].ToString();
		e.InputParameters[1] = userObjs.cast.userSeq;
		e.InputParameters[2] = ViewState["CAST_CHAR_NO"].ToString();
		e.InputParameters[3] = ViCommConst.ATTACHED_MAIL.ToString();
	}

	protected void btnAttachedPic_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		string sPicSeq = sKeys[0];
		string sObjPhotoImgPath = sKeys[1];
		
		ViewState["ATTACHED_OBJ_TYPE"] = ViCommConst.ATTACH_PIC_INDEX.ToString();
		ViewState["MOVIE_SEQ"] = "";
		ViewState["PIC_SEQ"] = sPicSeq;

		grdPic.Visible = false;
		imgAttachedPic.ImageUrl = string.Format("../{0}",sObjPhotoImgPath);
		
		imgAttachedPic.Visible = true;
		trMailDoc.Visible = true;
		btnMail.Enabled = true;
		txtMailDoc.Text = iBridUtil.GetStringValue(ViewState["MAIL_DOC"]);
	}


	protected void lnkManView_Command(object sender,CommandEventArgs e) {
		string sMovieSeq = e.CommandArgument.ToString();
		ViewState["ATTACHED_OBJ_TYPE"] = ViCommConst.ATTACH_MOVIE_INDEX.ToString();
		ViewState["MOVIE_SEQ"] = sMovieSeq;
		ViewState["PIC_SEQ"] = "";

		grdMovie.Visible = false;

		using (CastMovie oCastMovie = new CastMovie()) {
			oCastMovie.GetOne(sMovieSeq);
			lblMovieTitle.Text = oCastMovie.movieTitle;
			lblMovieTitle.Visible = true;
		}
		trMailDoc.Visible = true;
		btnMail.Enabled = true;
		txtMailDoc.Text = iBridUtil.GetStringValue(ViewState["MAIL_DOC"]);
	}
	protected void btnMailCancel_Click(object sender,EventArgs e) {
		InitPage();
	}
}
