<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ManView.aspx.cs" Inherits="Man_ManView" Title="男性会員詳細" EnableEventValidation="false" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="男性会員詳細"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<asp:Panel ID="Panel1" runat="server" GroupingText="男性会員詳細" Width="706px">
		<table align="center">
			<tr>
				<td valign="top">
					<table border="1">
						<tr>
							<td width="240" align="center">
								<asp:Label ID="lblSiteNm" runat="server" Font-Bold="True" Font-Underline="False" ForeColor="#FF8000"></asp:Label>
							</td>
						</tr>
						<tr>
							<td height="310" valign="top">
								<asp:DetailsView ID="dvwUserMan" runat="server" DataSourceID="dsUserManCharacter" AutoGenerateRows="False" SkinID="DetailsView" Width="346px">
									<Fields>
										<asp:TemplateField HeaderText="状態">
											<ItemTemplate>
												<asp:Image ID="Image2" runat="server" ImageUrl='<%# GetSysImage(Eval("CHARACTER_ONLINE_STATUS") )%>' />
											</ItemTemplate>
										</asp:TemplateField>
										<asp:BoundField DataField="HANDLE_NM" HeaderText="ハンドル名" />
										<asp:TemplateField HeaderText="残りポイント">
											<ItemTemplate>
												<asp:Label ID="lblBalPoint" runat="server" Text='<%# GetCurPoint(Eval("BAL_POINT"),Eval("SERVICE_POINT"),Eval("SERVICE_POINT_EFFECTIVE_DATE")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="生年月日">
											<ItemTemplate>
												<asp:Label ID="lblBirthDay" runat="server" Text='<%# string.Format("{0}({1})",Eval("BIRTHDAY"),GetAge(Eval("BIRTHDAY"))) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="会話回数">
											<ItemTemplate>
												<asp:Label ID="Label2" runat="server" Text='<%# Eval("TALK_COUNT","{0}回") %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:BoundField DataField="LAST_TALK_DATE" HeaderText="最終会話日" />
										<asp:TemplateField HeaderText="拒否リスト">
											<ItemTemplate>
												<asp:LinkButton ID="lnkRefuse" runat="server" Text='<%# GetRefuseTitle(Eval("REFUSE_FLAG")) %>' OnClientClick='<%#GetClientMsg(Eval("REFUSE_FLAG")) %>'
													CommandArgument='<%#Eval("REFUSE_FLAG") %>' OnCommand="lnkRefuse_Command"></asp:LinkButton>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:BoundField DataField="" HeaderText="" Visible="false" />
										<asp:TemplateField HeaderText="関係">
											<ItemTemplate>
												<asp:Label ID="lblRelationTypeNm" runat="server" Text='<%# GetRelationTypeNm(Eval("RELATION_TYPE")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
									</Fields>
									<HeaderStyle Wrap="False" />
								</asp:DetailsView>
								[詳細プロフィール]
								<asp:PlaceHolder ID="plcHolder" runat="server">
									<asp:Table ID="Table1" runat="server" CssClass="tableStyle tdDetailsViewStyle" Width="346px">
										<asp:TableRow ID="rowAttr0" runat="server">
											<asp:TableCell ID="celUserManAttrNm0" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblUserManAttrNm0" runat="server" Text='Label'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celUserManAttrSeq0" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Label ID="lblUserManAttrSeq0" runat="server" Text="Label"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowAttr1" runat="server">
											<asp:TableCell ID="celUserManAttrNm1" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblUserManAttrNm1" runat="server" Text='Label'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celUserManAttrSeq1" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Label ID="lblUserManAttrSeq1" runat="server" Text="Label"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowAttr2" runat="server">
											<asp:TableCell ID="celUserManAttrNm2" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblUserManAttrNm2" runat="server" Text='Label'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celUserManAttrSeq2" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Label ID="lblUserManAttrSeq2" runat="server" Text="Label"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowAttr3" runat="server">
											<asp:TableCell ID="celUserManAttrNm3" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblUserManAttrNm3" runat="server" Text='Label'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celUserManAttrSeq3" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Label ID="lblUserManAttrSeq3" runat="server" Text="Label"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowAttr4" runat="server">
											<asp:TableCell ID="celUserManAttrNm4" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblUserManAttrNm4" runat="server" Text='Label'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celUserManAttrSeq4" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Label ID="lblUserManAttrSeq4" runat="server" Text="Label"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowAttr5" runat="server">
											<asp:TableCell ID="celUserManAttrNm5" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblUserManAttrNm5" runat="server" Text='Label'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celUserManAttrSeq5" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Label ID="lblUserManAttrSeq5" runat="server" Text="Label"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowAttr6" runat="server">
											<asp:TableCell ID="celUserManAttrNm6" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblUserManAttrNm6" runat="server" Text='Label'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celUserManAttrSeq6" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Label ID="lblUserManAttrSeq6" runat="server" Text="Label"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowAttr7" runat="server">
											<asp:TableCell ID="celUserManAttrNm7" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblUserManAttrNm7" runat="server" Text='Label'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celUserManAttrSeq7" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Label ID="lblUserManAttrSeq7" runat="server" Text="Label"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowAttr8" runat="server">
											<asp:TableCell ID="celUserManAttrNm8" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblUserManAttrNm8" runat="server" Text='Label'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celUserManAttrSeq8" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Label ID="lblUserManAttrSeq8" runat="server" Text="Label"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowAttr9" runat="server">
											<asp:TableCell ID="celUserManAttrNm9" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblUserManAttrNm9" runat="server" Text='Label'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celUserManAttrSeq9" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Label ID="lblUserManAttrSeq9" runat="server" Text="Label"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowAttr10" runat="server">
											<asp:TableCell ID="celUserManAttrNm10" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblUserManAttrNm10" runat="server" Text='Label'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celUserManAttrSeq10" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Label ID="lblUserManAttrSeq10" runat="server" Text="Label"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowAttr11" runat="server">
											<asp:TableCell ID="celUserManAttrNm11" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblUserManAttrNm11" runat="server" Text='Label'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celUserManAttrSeq11" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Label ID="lblUserManAttrSeq11" runat="server" Text="Label"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowAttr12" runat="server">
											<asp:TableCell ID="celUserManAttrNm12" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblUserManAttrNm12" runat="server" Text='Label'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celUserManAttrSeq12" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Label ID="lblUserManAttrSeq12" runat="server" Text="Label"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowAttr13" runat="server">
											<asp:TableCell ID="celUserManAttrNm13" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblUserManAttrNm13" runat="server" Text='Label'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celUserManAttrSeq13" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Label ID="lblUserManAttrSeq13" runat="server" Text="Label"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowAttr14" runat="server">
											<asp:TableCell ID="celUserManAttrNm14" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblUserManAttrNm14" runat="server" Text='Label'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celUserManAttrSeq14" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Label ID="lblUserManAttrSeq14" runat="server" Text="Label"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowAttr15" runat="server">
											<asp:TableCell ID="celUserManAttrNm15" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblUserManAttrNm15" runat="server" Text='Label'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celUserManAttrSeq15" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Label ID="lblUserManAttrSeq15" runat="server" Text="Label"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowAttr16" runat="server">
											<asp:TableCell ID="celUserManAttrNm16" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblUserManAttrNm16" runat="server" Text='Label'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celUserManAttrSeq16" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Label ID="lblUserManAttrSeq16" runat="server" Text="Label"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowAttr17" runat="server">
											<asp:TableCell ID="celUserManAttrNm17" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblUserManAttrNm17" runat="server" Text='Label'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celUserManAttrSeq17" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Label ID="lblUserManAttrSeq17" runat="server" Text="Label"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowAttr18" runat="server">
											<asp:TableCell ID="celUserManAttrNm18" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblUserManAttrNm18" runat="server" Text='Label'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celUserManAttrSeq18" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Label ID="lblUserManAttrSeq18" runat="server" Text="Label"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowAttr19" runat="server">
											<asp:TableCell ID="celUserManAttrNm19" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblUserManAttrNm19" runat="server" Text='Label'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celUserManAttrSeq19" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Label ID="lblUserManAttrSeq19" runat="server" Text="Label"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
									</asp:Table>
								</asp:PlaceHolder>
								<asp:Button runat="server" ID="btnCancel" Text="戻る" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
								<asp:Button runat="server" ID="btnSeek" Text="再表示" CssClass="seekbutton" OnClick="btnSeek_Click" CausesValidation="False" />
								<asp:Button runat="server" ID="btnChangeMail" Text="メール作成表示" CssClass="seekbutton" CausesValidation="False" OnClick="btnChangeMail_Click" Width="104px" />
								<asp:Button runat="server" ID="btnChangeFavorit" Text="お気に入り登録表示" CssClass="seekbutton" CausesValidation="False" OnClick="btnChangeFavorit_Click" Width="124px" />
							</td>
						</tr>
					</table>
				</td>
				<td valign="top" width="320">
					<asp:Panel ID="pnlFavorit" runat="server">
						<table border="1">
							<tr>
								<td class="tdHeaderSmallStyle">
									登録日
								</td>
								<td class="tdDataStyle" align="center">
									<asp:Label ID="lbRegistDay" runat="server" Text=""></asp:Label>
								</td>
								<td class="tdHeaderStyle">
									メモ
								</td>
							</tr>
							<tr>
								<td class="tdDataStyle" valign="top" colspan="3">
									<asp:TextBox ID="txtCommentList" runat="server" Width="316px" TextMode="MultiLine" Rows="7" Columns="60" Height="248px"></asp:TextBox>
									<asp:Button runat="server" ID="btnBookMark" Text="お気に入り追加／メモ更新" CssClass="seekbutton" OnClick="btnBookMark_Click" CausesValidation="False" />
									<asp:Button runat="server" ID="btnReleaseBookMark" Text="お気に入り解除" CssClass="seekbutton" OnClick="btnReleaseBookMark_Click" CausesValidation="False" />
								</td>
							</tr>
						</table>
					</asp:Panel>
					<asp:Panel ID="pnlMail" runat="server">
						<table class="smallTableStyle">
							<tr>
								<td class="tdHeaderStyle">
									ﾒｰﾙ種別
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstMailTemplate" runat="server" DataSourceID="dsTemplate" DataTextField="TEMPLATE_NM" DataValueField="MAIL_TEMPLATE_NO" Width="250px"
										AutoPostBack="True" OnSelectedIndexChanged="lstMailTemplate_SelectedIndexChanged">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾒｰﾙﾀｲﾄﾙ
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtMailTitle" runat="server" MaxLength="60" Width="180px"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdDataStyle" valign="top" colspan="2">
									<asp:GridView ID="grdMovie" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsCastMovie" AllowSorting="True" SkinID="GridView">
										<Columns>
											<asp:TemplateField HeaderText="添付動画タイトル">
												<ItemStyle HorizontalAlign="Left" Width="140px" />
												<ItemTemplate>
													<asp:LinkButton ID="lnkManView" runat="server" Text='<%# Eval("MOVIE_TITLE") %>' CommandArgument='<%# Eval("MOVIE_SEQ") %>' OnCommand="lnkManView_Command"></asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateField>
										</Columns>
										<PagerSettings Mode="NumericFirstLast" />
									</asp:GridView>
								</td>
							</tr>
							<tr>
								<td class="tdDataStyle" valign="top" colspan="2">
									<asp:GridView ID="grdPic" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsCastPic" AllowSorting="True" SkinID="GridView">
										<Columns>
											<asp:TemplateField HeaderText="添付画像">
												<ItemTemplate>
													<asp:ImageButton ID="btnPicMainte" runat="server" Width="50" Height="50" BorderWidth="2px" BorderStyle="solid" BorderColor="#00C0C0" ImageUrl='<%# Eval("OBJ_SMALL_PHOTO_IMG_PATH","../{0}") %>'
														CommandArgument='<%# string.Format("{0}:{1}",Eval("PIC_SEQ"),Eval("OBJ_SMALL_PHOTO_IMG_PATH")) %>' OnCommand="btnAttachedPic_Command" />
												</ItemTemplate>
											</asp:TemplateField>
										</Columns>
										<PagerSettings Mode="NumericFirstLast" />
									</asp:GridView>
								</td>
							</tr>
							<asp:Panel ID="pnlService" runat="server">
								<tr>
									<td class="tdHeaderStyle2">
										ｻｰﾋﾞｽﾎﾟｲﾝﾄ
									</td>
									<td class="tdDataStyle">
										<asp:DropDownList ID="lstPoint" runat="server" DataSourceID="dsPoint" DataTextField="CODE_NM" DataValueField="CODE" Width="100px">
										</asp:DropDownList>
									</td>
								</tr>
							</asp:Panel>
							<tr runat="Server" id="trMailDoc">
								<td class="tdDataStyle" valign="top" colspan="2">
									$NO_TRANS_START;
									<pin:pinEdit ID="txtMailDoc" runat="server" UserMode="HTML" DisplayStyle="Style5" DocumentView="True" HtmlMode="Body" Ruler="False" ReturnMode="BR" SpellMode="Inline"
										ToolbarConfiguration="ToolbarDesigner" Toolbar="T222324272829303536" FormatMode="Classic" BorderWidth="1px" Height="220px" Width="320px" DocumentWidth="230"
										Font-Bold="False" FontSizeType="px" RelativeBaseImageUrl="/" RelativeBaseLinkUrl="/" RelativeImageRoot="/" Font-Size="Smaller">
									</pin:pinEdit>
									$NO_TRANS_END;
									<asp:Button runat="server" ID="btnMail" Text="メール送信" CssClass="seekbutton" OnClientClick='return confirm("メールを送信しますか？");' OnClick="btnMail_Click" ValidationGroup="TxMail" />
									<asp:CustomValidator ID="vdcCheckMail" runat="server" ErrorMessage="エラー" OnServerValidate="vdcCheckMail_ServerValidate" ValidationGroup="TxMail"></asp:CustomValidator>
								</td>
							</tr>
							<tr>
								<td class="tdDataStyle" valign="top" colspan="2">
									<asp:Button ID="btnMailCancel" runat="server" CssClass="seekbutton" OnClick="btnMailCancel_Click" Text="キャンセル" />
								</td>
							</tr>
							<tr>
								<td class="tdDataStyle" valign="top" colspan="2">
									<asp:Image ID="imgAttachedPic" runat="server" BorderStyle="Solid" BorderWidth="2" Width="50" Height="50" /><br />
								</td>
							</tr>
							<tr>
								<td class="tdDataStyle" valign="top" colspan="2">
									<asp:Label ID="lblMovieTitle" runat="server" Text=""></asp:Label>
								</td>
							</tr>
						</table>
					</asp:Panel>
				</td>
			</tr>
		</table>
	</asp:Panel>
	<asp:ObjectDataSource ID="dsUserManCharacter" runat="server" SelectMethod="GetOne" TypeName="UserManCharacter" OnSelecting="dsUserManCharacter_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pCastSeq" Type="String" />
			<asp:Parameter Name="pCastCharNo" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsPoint" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="79" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsTemplate" runat="server" SelectMethod="GetCastMailList" TypeName="MailTemplate" OnSelecting="dsTemplate_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCastMovie" runat="server" SelectMethod="GetPageCollection" TypeName="CastMovie" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelecting="dsCastMovie_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
			<asp:Parameter Name="pMovieType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCastPic" runat="server" SelectMethod="GetPageCollection" TypeName="CastPic" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelecting="dsCastPic_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
			<asp:Parameter Name="pPicType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>
