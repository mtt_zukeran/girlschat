<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ManConditionList.aspx.cs" Inherits="Man_ManConditionList"
	Title="j«ουΪΧυ" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="j«ουΪΧυ"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<fieldset class="fieldset">
		<legend>[<%= DisplayWordUtil.Replace("j«ουΪΧυ") %>]</legend>
		<table border="1">
			<tr>
				<td width="216" align="center">
					<asp:Label ID="lblSiteNm" runat="server" Font-Bold="True" Font-Underline="False" ForeColor="#FF8000"></asp:Label>
				</td>
			</tr>
		</table>
		<table border="0" class="tableStyle">
			<tr>
				<td class="tdHeaderStyle">
					ΚέΔήΩΘ°Ρ
				</td>
				<td class="tdDataStyle">
					<asp:TextBox ID="txtHandleNm" runat="server" Width="100px"></asp:TextBox>
				</td>
				<td class="tdHeaderStyle" width="138">
					o^ϊ
				</td>
				<td class="tdDataStyle">
					<asp:DropDownList ID="lstRegistDay" runat="server" Width="100px" DataSourceID="dsRegsitDay" DataTextField="CODE_NM" DataValueField="CODE">
					</asp:DropDownList>
				</td>
			</tr>
			<asp:PlaceHolder ID="plcHolder" runat="server">
				<asp:TableRow ID="rowAttr0" runat="server">
					<asp:TableCell ID="celManAttrNm0" runat="server" CssClass="tdHeaderStyle">
						<asp:Label ID="lblManAttrNm0" runat="server" Text="Label"></asp:Label>
					</asp:TableCell>
					<asp:TableCell ID="celManAttrSeq0" runat="server" CssClass="tdDataStyle">
						<asp:DropDownList ID="lstManAttrSeq0" runat="server" Width="100px">
						</asp:DropDownList>
					</asp:TableCell>
					<asp:TableCell ID="celManAttrNm1" runat="server" CssClass="tdHeaderStyle">
						<asp:Label ID="lblManAttrNm1" runat="server" Text="Label"></asp:Label>
					</asp:TableCell>
					<asp:TableCell ID="celManAttrSeq1" runat="server" CssClass="tdDataStyle">
						<asp:DropDownList ID="lstManAttrSeq1" runat="server" Width="100px">
						</asp:DropDownList>
					</asp:TableCell>
				</asp:TableRow>
				<asp:TableRow ID="rowAttr1" runat="server">
					<asp:TableCell ID="celManAttrNm2" runat="server" CssClass="tdHeaderStyle2">
						<asp:Label ID="lblManAttrNm2" runat="server" Text="Label"></asp:Label>
					</asp:TableCell>
					<asp:TableCell ID="celManAttrSeq2" runat="server" CssClass="tdDataStyle">
						<asp:DropDownList ID="lstManAttrSeq2" runat="server" Width="100px">
						</asp:DropDownList>
					</asp:TableCell>
					<asp:TableCell ID="celManAttrNm3" runat="server" CssClass="tdHeaderStyle2">
						<asp:Label ID="lblManAttrNm3" runat="server" Text="Label"></asp:Label>
					</asp:TableCell>
					<asp:TableCell ID="celManAttrSeq3" runat="server" CssClass="tdDataStyle">
						<asp:DropDownList ID="lstManAttrSeq3" runat="server" Width="100px">
						</asp:DropDownList>
					</asp:TableCell>
				</asp:TableRow>
			</asp:PlaceHolder>
		</table>
		<asp:Button runat="server" ID="btnSeek" Text="υ" CssClass="seekmidlebutton" OnClick="btnSeek_Click" />
		<asp:Button runat="server" ID="btnReturn" Text="ίι" CssClass="seekmidlebutton" OnClick="btnCancel_Click" />
		<br clear="all" />
		<asp:GridView ID="grdMan" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsUserManCharacter" AllowSorting="True" SkinID="GridView" HeaderStyle-Font-Size="X-Small">
			<Columns>
				<asp:TemplateField>
					<ItemStyle Width="120px" />
					<ItemTemplate>
						<asp:HyperLink ID="lnkManView" runat="server" NavigateUrl='<%# string.Format("~/Man/ManView.aspx?sitecd={0}&manuserseq={1}&castcharno={2}&return=../Man/ManOnLineList.aspx",Eval("SITE_CD"),Eval("USER_SEQ"),GetCurCharNo()) %>'
							Text='<%# Eval("HANDLE_NM") %>'></asp:HyperLink>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField>
					<ItemTemplate>
						<asp:Label ID="lblAge" runat="server" Text='<%# CalcAge(Eval("BIRTHDAY")) %>'></asp:Label>
					</ItemTemplate>
					<ItemStyle HorizontalAlign="Center" />
				</asp:TemplateField>
				<asp:TemplateField>
					<ItemTemplate>
						<asp:Label ID="lblDisplayValue1" runat="server" Text='<%# Eval("INQ_MAN_VALUE1") %>'></asp:Label>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField>
					<ItemTemplate>
						<asp:Label ID="lblDisplayValue2" runat="server" Text='<%# Eval("INQ_MAN_VALUE2") %>'></asp:Label>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField>
					<ItemTemplate>
						<asp:Label ID="lblDisplayValue3" runat="server" Text='<%# Eval("INQ_MAN_VALUE3") %>'></asp:Label>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField>
					<ItemTemplate>
						<asp:Label ID="lblDisplayValue4" runat="server" Text='<%# Eval("INQ_MAN_VALUE4") %>'></asp:Label>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:BoundField DataField="BAL_POINT">
					<ItemStyle HorizontalAlign="Right" />
				</asp:BoundField>
				<asp:BoundField DataField="LAST_LOGIN_DATE" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
					<ItemStyle HorizontalAlign="Center" />
				</asp:BoundField>
				<asp:TemplateField>
					<ItemTemplate>
						<asp:Image ID="Image2" runat="server" ImageUrl='<%# GetSysImage(Eval("CHARACTER_ONLINE_STATUS") )%>' />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
			<PagerSettings Mode="NumericFirstLast" />
		</asp:GridView>
	</fieldset>
	<asp:ObjectDataSource ID="dsUserManCharacter" runat="server" SelectMethod="GetPageCollection" TypeName="UserManCharacter" SelectCountMethod="GetPageCount"
		EnablePaging="True" OnSelecting="dsUserManCharacter_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pConditionFlag" Type="Int16" />
			<asp:Parameter Name="pHandleNm" Type="String" />
			<asp:Parameter Name="pRegistDayType" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
			<asp:Parameter Name="pManAttrValue1" Type="String" />
			<asp:Parameter Name="pManAttrValue2" Type="String" />
			<asp:Parameter Name="pManAttrValue3" Type="String" />
			<asp:Parameter Name="pManAttrValue4" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsRegsitDay" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="77" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>
