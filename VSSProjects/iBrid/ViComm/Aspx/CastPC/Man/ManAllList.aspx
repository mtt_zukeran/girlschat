<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ManAllList.aspx.cs" Inherits="Man_ManAllList" Title="男性会員全体検索" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="男性会員全体検索"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<fieldset class="fieldset">
		<legend>[<%= DisplayWordUtil.Replace("男性会員全体検索結果") %>]</legend>
		<table border="1">
			<tr>
				<td width="240" align="center">
					<asp:Label ID="lblSiteNm" runat="server" Font-Bold="True" Font-Underline="False" ForeColor="#FF8000"></asp:Label>
				</td>
				<td align="center" width="30">
					<asp:Button runat="server" ID="btnSeek" Text="検索" OnClick="btnSeek_Click" />
				</td>
				<td align="center">
					<asp:Button runat="server" ID="btnReturn" Text="戻る" OnClick="btnCancel_Click" />
				</td>
			</tr>
		</table>
		<asp:GridView ID="grdMan" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsUserManCharacter" AllowSorting="True" SkinID="GridView">
			<Columns>
				<asp:TemplateField>
					<ItemStyle HorizontalAlign="Left" Width="120px" />
					<ItemTemplate>
						<asp:HyperLink ID="lnkManView" runat="server" NavigateUrl='<%# string.Format("~/Man/ManView.aspx?sitecd={0}&manuserseq={1}&castcharno={2}&return=../Man/ManOnLineList.aspx",Eval("SITE_CD"),Eval("USER_SEQ"),GetCurCharNo()) %>'
							Text='<%# Eval("HANDLE_NM") %>'></asp:HyperLink>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField>
					<ItemTemplate>
						<asp:Label ID="lblAge" runat="server" Text='<%# CalcAge(Eval("BIRTHDAY")) %>'></asp:Label>
					</ItemTemplate>
					<ItemStyle HorizontalAlign="Center" Width="24px" />
				</asp:TemplateField>
				<asp:TemplateField>
					<ItemTemplate>
						<asp:Label ID="lblDisplayValue1" runat="server" Text='<%# Eval("INQ_MAN_VALUE1") %>'></asp:Label>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField>
					<ItemTemplate>
						<asp:Label ID="lblDisplayValue2" runat="server" Text='<%# Eval("INQ_MAN_VALUE2") %>'></asp:Label>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField>
					<ItemTemplate>
						<asp:Label ID="lblDisplayValue3" runat="server" Text='<%# Eval("INQ_MAN_VALUE3") %>'></asp:Label>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField>
					<ItemTemplate>
						<asp:Label ID="lblDisplayValue4" runat="server" Text='<%# Eval("INQ_MAN_VALUE4") %>'></asp:Label>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:BoundField DataField="BAL_POINT">
					<ItemStyle HorizontalAlign="Right" />
				</asp:BoundField>
				<asp:BoundField DataField="LAST_LOGIN_DATE" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
					<ItemStyle HorizontalAlign="Center" />
				</asp:BoundField>
				<asp:TemplateField>
					<ItemTemplate>
						<asp:Image ID="Image2" runat="server" ImageUrl='<%# GetSysImage(Eval("CHARACTER_ONLINE_STATUS") )%>' />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
			<PagerSettings Mode="NumericFirstLast" />
			<HeaderStyle Font-Size="X-Small" />
		</asp:GridView>
	</fieldset>
	<asp:ObjectDataSource ID="dsUserManCharacter" runat="server" SelectMethod="GetPageCollection" TypeName="UserManCharacter" SelectCountMethod="GetPageCount"
		EnablePaging="True" OnSelecting="dsUserManCharacter_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pConditionFlag" Type="Int16" />
			<asp:Parameter Name="pHandleNm" Type="String" />
			<asp:Parameter Name="pRegistDayType" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
			<asp:Parameter Name="pManAttrValue1" Type="String" />
			<asp:Parameter Name="pManAttrValue2" Type="String" />
			<asp:Parameter Name="pManAttrValue3" Type="String" />
			<asp:Parameter Name="pManAttrValue4" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>
