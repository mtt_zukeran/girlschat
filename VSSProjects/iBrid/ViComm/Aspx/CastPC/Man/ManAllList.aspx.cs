﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: 男性会員全体検索
--	Progaram ID		: ManAllList
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net;
using System.IO;
using iBridCommLib;
using ViComm;

public partial class Man_ManAllList:System.Web.UI.Page {
	private SessionObjs userObjs;

	protected void Page_Load(object sender,EventArgs e) {
		userObjs = (SessionObjs)Session["objs"];
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.DOCOMO);
		if (!IsPostBack) {
			FirstLoad();
			InitPage();
		}
		if (userObjs.site.GetOne(iBridUtil.GetStringValue(ViewState["SITE_CD"]))) {
			lblSiteNm.Text = userObjs.site.siteNm;
			if (userObjs.site.IsActiveMulitCharFlag(ViewState["SITE_CD"].ToString())) {
				lblSiteNm.Text += string.Concat("-",ViewState["CAST_CHAR_NO"].ToString());
			}
		}
	}

	private void FirstLoad() {
		ViewState["SITE_CD"] = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		ViewState["CAST_CHAR_NO"] = iBridUtil.GetStringValue(Request.QueryString["castcharno"]);
	}

	private void InitPage() {
		this.Title = DisplayWordUtil.Replace(this.Title);
		this.lblPgmTitle.Text = DisplayWordUtil.Replace(this.lblPgmTitle.Text);

		ClearField();
		DataBind();
		AddHeader();
	}

	private void ClearField() {
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		Server.Transfer("../cast/MainView.aspx");
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		DataBind();
	}

	protected string GetCurCharNo() {
		return ViewState["CAST_CHAR_NO"].ToString();
	}

	protected string CalcAge(object pBirthDay) {
		int iAge = ViCommPrograms.Age(pBirthDay.ToString());
		return iAge.ToString();
	}

	protected string GetSysImage(object pOnlienStatus) {
		int iOnlineStatus = int.Parse(pOnlienStatus.ToString());

		switch (iOnlineStatus) {
			case ViCommConst.USER_LOGINED:
				return "../image/sys_logined.jpg";
			case ViCommConst.USER_TALKING:
				return "../image/sys_talking.jpg";
			case ViCommConst.USER_OFFLINE:
				return "../image/sys_offline.jpg";
		}
		return "";
	}


	protected void dsUserManCharacter_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = ViewState["SITE_CD"].ToString();
		e.InputParameters[1] = ViCommConst.SearchManStatusOption.SEL_MAN_ALL;
		e.InputParameters[2] = "";
		e.InputParameters[3] = "";
		e.InputParameters[4] = userObjs.cast.userSeq;
		e.InputParameters[5] = ViewState["CAST_CHAR_NO"].ToString();
	}

	private void AddHeader() {
		grdMan.Columns[0].HeaderText = "ハンドルネーム";
		grdMan.Columns[1].HeaderText = "年齢";

		using (UserManAttrType oUserManAttrType = new UserManAttrType()) {
			DataSet ds = oUserManAttrType.GetInqManAttrType(iBridUtil.GetStringValue(ViewState["SITE_CD"]));

			if (ds.Tables[0].Rows[0]["MAN_ATTR_TYPE_SEQ1"].ToString() != "") {
				grdMan.Columns[2].HeaderText = ds.Tables[0].Rows[0]["MAN_ATTR_TYPE_NM1"].ToString();
			} else {
				grdMan.Columns[2].Visible = false;
			}
			if (ds.Tables[0].Rows[0]["MAN_ATTR_TYPE_SEQ2"].ToString() != "") {
				grdMan.Columns[3].HeaderText = ds.Tables[0].Rows[0]["MAN_ATTR_TYPE_NM2"].ToString();
			} else {
				grdMan.Columns[3].Visible = false;
			}
			if (ds.Tables[0].Rows[0]["MAN_ATTR_TYPE_SEQ3"].ToString() != "") {
				grdMan.Columns[4].HeaderText = ds.Tables[0].Rows[0]["MAN_ATTR_TYPE_NM3"].ToString();
			} else {
				grdMan.Columns[4].Visible = false;
			}
			if (ds.Tables[0].Rows[0]["MAN_ATTR_TYPE_SEQ4"].ToString() != "") {
				grdMan.Columns[5].HeaderText = ds.Tables[0].Rows[0]["MAN_ATTR_TYPE_NM4"].ToString();
			} else {
				grdMan.Columns[5].Visible = false;
			}
		}
		grdMan.Columns[6].HeaderText = "残ﾎﾟｲﾝﾄ";
		grdMan.Columns[7].HeaderText = "最終ﾛｸﾞｲﾝ日";
		grdMan.Columns[8].HeaderText = "状態";
	}
}
