﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: ログイン
--	Progaram ID		: Login
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Data;
using iBridCommLib;
using ViComm;

public partial class Login:System.Web.UI.Page {
	private string loginId;
	private string password;
	private string SiteCd {
		get {
			return this.ViewState["SiteCd"] as string;
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			using (Sys oSys = new Sys()) {
				string sSiteCd = null;
				oSys.GetValue("ONLINE_MONITOR_SITE_CD",out sSiteCd);
				this.SiteCd = sSiteCd;
			}

			this.Label1.Text = DisplayWordUtil.Replace(this.Label1.Text);
			this.ctlLogin.TitleText = DisplayWordUtil.Replace(this.ctlLogin.TitleText);

			loginId = iBridUtil.GetStringValue(Request.QueryString["userid"]);
			password = iBridUtil.GetStringValue(Request.QueryString["password"]);
			if ((!loginId.Equals("")) && (!password.Equals(""))) {
				if (SignOn()) {
					Response.Redirect("LoginTemplateMainte.aspx");
				}
			}
		}
	}

	protected void ctlLogin_Authenticate(object sender,AuthenticateEventArgs e) {
		loginId = ctlLogin.UserName;
		password = ctlLogin.Password;
		e.Authenticated = SignOn();
		if (e.Authenticated) {
			Response.Redirect("LoginTemplateMainte.aspx");
		}
	}

	private bool SignOn() {
		string sSiteCd;

		using (Sys oSys = new Sys()) {
			oSys.GetValue("ONLINE_MONITOR_SITE_CD",out sSiteCd);
		}

		Session["CompanyMask"] = "0";
		using (ManageCompany oCompany = new ManageCompany()) {
			if (oCompany.GetOne()) {
				Session["CompanyMask"] = oCompany.companyMask.ToString();
			}
		}

		using (Cast oCast = new Cast()) {
			DataSet ds = oCast.GetOne(loginId);
			if (ds.Tables[0].Rows.Count > 0) {
				DataRow dr = ds.Tables[0].Rows[0];
				string sUri = iBridUtil.GetStringValue(dr["URI"]);
				if (dr["LOGIN_PASSWORD"].ToString().Equals(password)) {
					//ログインしたキャストがURIを所持していなかったら登録する
					if (sUri.Equals("")) {
						sUri = oCast.UriCreate(dr["USER_SEQ"].ToString());
					}
					Session["PageSize"] = ConfigurationManager.AppSettings["PageSize"];
					Session["Root"] = ConfigurationManager.AppSettings["Root"];
					Session["Login"] = "1";
					Session["SysLocNm"] = "ViCOMM Site Manage";

					SessionObjs userObjs = new SessionObjs();
					Session["objs"] = userObjs;

					ParseViComm oParseViComm = new ParseViComm(
							@"(\$\w{1,};|\$\w{1,}\(.[^()""]+?\);)",
							RegexOptions.IgnoreCase,
							ViCommConst.DOCOMO,
							userObjs);

					Session["parse"] = new ParseHTML(oParseViComm);
					oParseViComm.parseContainer = (ParseHTML)Session["parse"];
					oParseViComm.parseContainer.parseUser.isPreviousVer = true;

					userObjs.site.GetOne(sSiteCd);
					userObjs.cast.userSeq = dr["USER_SEQ"].ToString();
					userObjs.cast.loginId = dr["LOGIN_ID"].ToString();
					userObjs.cast.uri = sUri;
					userObjs.cast.onlineLiveEnableFlag = dr["ONLINE_LIVE_ENABLE_FLAG"].ToString().Equals("1");
					Session["DISP_MODE"] = "1";

					using (Access oAccess = new Access()) {
						/*
						 * memo:
						 * 　"/ViComm/woman/LoginUser.aspx"でﾛｸﾞｲﾝしたものとみなし
						 * 　ﾛｸﾞｲﾝｶｳﾝﾄを加算する;
						 */
						oAccess.AccessPage(
						   userObjs.site.siteCd,
						   ViCommConst.ProgramRoot.Woman,
						   ViCommConst.Program.LoginUser,
						   ViCommConst.ProgramAction.LOGIN,
						   userObjs.cast.userSeq);
					}


					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		}
	}
}
