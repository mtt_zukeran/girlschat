<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CastProfileMovieList.aspx.cs" Inherits="Cast_CastProfileMovieList"
	Title="ムービー一覧" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="動画一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<asp:Panel runat="server" ID="pnlDtl">
				<fieldset class="fieldset">
					<legend>
						<asp:Label ID="lblTitleMainte" runat="server" Text="[プロフィールタイトル編集]" />
					</legend>
					<table border="0" style="width: 620px" class="tableStyle">
						<asp:PlaceHolder ID="plcHolder" runat="server">
							<tr>
								<td class="tdHeaderStyle">
									動画属性
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstCastMovieAttrType" runat="server" DataSourceID="dsCastMovieAttrType" DataTextField="CAST_MOVIE_ATTR_TYPE_NM" DataValueField="CAST_MOVIE_ATTR_TYPE_SEQ"
										AutoPostBack="True" OnSelectedIndexChanged="lstCastMovieAttrType_SelectedIndexChanged">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									動画属性値
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstCastMovieAttrTypeValue" runat="server" DataSourceID="dsCastMovieAttrTypeValue" DataTextField="CAST_MOVIE_ATTR_NM" DataValueField="CAST_MOVIE_ATTR_SEQ">
									</asp:DropDownList>
								</td>
							</tr>
						</asp:PlaceHolder>
						<tr>
							<td class="tdHeaderStyle">
								動画タイトル
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtMovieTitle" runat="server" MaxLength="30" Width="313px"></asp:TextBox>
							</td>
						</tr>
						<tr runat="server" id="trMovieDoc">
							<td class="tdHeaderStyle">
								動画本文
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtMovieDoc" runat="server" MaxLength="3000" Width="313px" Height="91px" TextMode="MultiLine"></asp:TextBox>
							</td>
						</tr>
					</table>
					<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" OnClientClick='return confirm("更新を行いますか？");' />
					<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" OnClientClick='return confirm("削除を行いますか？");' />
					<asp:Button runat="server" ID="btnCancel" Text="戻る" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
				</fieldset>
			</asp:Panel>
		</asp:Panel>
		<fieldset class="fieldset">
			<legend>
				<asp:Label ID="lblMovieList" runat="server" Text="[プロフィール動画一覧]" />
			</legend>
			<table border="1" width="630px">
				<tr>
					<td width="240" align="center">
						<asp:Label ID="lblSiteNm" runat="server" Font-Bold="True" Font-Underline="False" ForeColor="#FF8000"></asp:Label>
					</td>
					<td align="center">
						<asp:Button runat="server" ID="btnRecording" Text="プロフィール録画開始" OnClick="btnRecording_Click" CausesValidation="False" />
					</td>
					<td align="center">
						<asp:Button runat="server" ID="btnSeek" Text="検索" OnClick="btnSeek_Click" />
					</td>
					<td align="center">
						<asp:Button runat="server" ID="btnReturn" Text="戻る" OnClick="btnCancel_Click" />
					</td>
					<td width="240" align="center">
						<asp:ListBox ID="lstMovieType" runat="server" Rows="1" AutoPostBack="True" OnSelectedIndexChanged="lstMovieType_SelectedIndexChanged"></asp:ListBox></td>
				</tr>
			</table>
			<asp:GridView ID="grdProfileMovie" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsCastMovie" AllowSorting="True" SkinID="GridView">
				<Columns>
					<asp:TemplateField HeaderText="動画タイトル">
						<ItemStyle Width="220px" />
						<ItemTemplate>
							<asp:LinkButton ID="lnkEditTitle" runat="server" CommandArgument='<%# Eval("MOVIE_SEQ") %>' OnCommand="lnkEditTitle_Command" Text='<%# Eval("MOVIE_TITLE") %>'></asp:LinkButton>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:BoundField DataField="UPLOAD_DATE" HeaderText="アップロード日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False">
						<ItemStyle HorizontalAlign="Center" />
					</asp:BoundField>
					<asp:BoundField DataField="CAST_MOVIE_ATTR_TYPE_NM" HeaderText="属性区分名">
						<ItemStyle HorizontalAlign="Center" Width="80px" />
					</asp:BoundField>
					<asp:BoundField DataField="CAST_MOVIE_ATTR_NM" HeaderText="属性値名">
						<ItemStyle HorizontalAlign="Center" Width="80px" />
					</asp:BoundField>
					<asp:BoundField DataField="NOT_APPROVE_MARK" HeaderText="認証">
						<ItemStyle HorizontalAlign="Center" />
					</asp:BoundField>
					<asp:TemplateField HeaderText="再生">
						<ItemStyle HorizontalAlign="Center" />
						<ItemTemplate>
							<asp:LinkButton ID="lnkProfileQuickTime" runat="server" CommandArgument='<%# Eval("MOVIE_SEQ") %>' OnCommand="lnkProfileQuickTime_Command" Text="再生"></asp:LinkButton>
						</ItemTemplate>
					</asp:TemplateField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
		</fieldset>
	</div>
	<asp:Panel runat="server" ID="pnlHideValue" Visible="false">
		<asp:TextBox ID="txtChargePoint" runat="server"></asp:TextBox>
		<asp:TextBox ID="txtObjNotApproveFlag" runat="server"></asp:TextBox>
		<asp:TextBox ID="txtObjNotPublishFlag" runat="server"></asp:TextBox>
	</asp:Panel>
	<asp:ObjectDataSource ID="dsCastMovie" runat="server" SelectMethod="GetPageCollection" TypeName="CastMovie" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelecting="dsCastMovie_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
			<asp:Parameter Name="pMovieType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCastMovieAttrType" runat="server" SelectMethod="GetList" TypeName="CastMovieAttrType" OnSelecting="dsCastMovieAttrType_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCastMovieAttrTypeValue" runat="server" SelectMethod="GetList" TypeName="CastMovieAttrTypeValue" OnSelecting="dsCastMovieAttrTypeValue_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pCastMovieAttrTypeSeq" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>
