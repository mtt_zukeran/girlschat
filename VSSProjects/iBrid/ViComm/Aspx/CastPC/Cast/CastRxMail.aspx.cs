﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: 受信メールＢＯＸ
--	Progaram ID		: CastRxMail
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net;
using System.IO;
using iBridCommLib;
using ViComm;

public partial class Cast_CastRxMail:System.Web.UI.Page {
	private SessionObjs userObjs;

	protected void Page_Load(object sender,EventArgs e) {
		userObjs = (SessionObjs)Session["objs"];
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.DOCOMO);
		if (!IsPostBack) {
			InitPage();
		}
		if (userObjs.site.GetOne(iBridUtil.GetStringValue(ViewState["SITE_CD"]))) {
			lblSiteNm.Text = userObjs.site.siteNm;
            if(userObjs.site.IsActiveMulitCharFlag(ViewState["SITE_CD"].ToString())){
                lblSiteNm.Text += string.Concat("-", ViewState["CAST_CHAR_NO"].ToString());
            }
		}
	}

	private void SetViewState() {
		ViewState["SITE_CD"] = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
        ViewState["CAST_CHAR_NO"] = iBridUtil.GetStringValue(Request.QueryString["castcharno"]);
	}

	private void InitPage() {
        SetViewState();
		ClearField();
		DataBind();
	}

	private void ClearField() {
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		Server.Transfer("MainView.aspx");
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		DataBind();
	}

	protected void lnkDel_Command(object sender,CommandEventArgs e) {
		string sMailSeq = e.CommandArgument.ToString();
		using(MailBox oMailBox = new MailBox()){
			oMailBox.UpdateMailDel(sMailSeq,ViCommConst.RX);
		}
		DataBind();
	}

	protected void lnkAttached_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		string pSiteCd = ViewState["SITE_CD"].ToString();
		string pUserSeq = sKeys[0];
		string pUserCharNo = sKeys[1];
		string pAttachedType = sKeys[2];
		string pPicSeq = sKeys[3];
		string pMovieSeq = sKeys[4];
		
		if (pAttachedType.Equals(ViCommConst.ATTACH_PIC_INDEX.ToString())) {
			using (UserManPic oUserManPic = new UserManPic()) {
				if (oUserManPic.GetOne(pSiteCd,pUserSeq,pUserCharNo,pPicSeq)) {
					string sRoot = ConfigurationManager.AppSettings["Root"];
					string sURL = "http://" + Request.Url.Authority + sRoot + ViCommConst.WEB_PIC_PATH + "/" + oUserManPic.siteCd + string.Format("/man/{0}{1}",iBridUtil.addZero(oUserManPic.picSeq,ViCommConst.OBJECT_NM_LENGTH),ViCommConst.PIC_FOODER);
					string sScripts = string.Format("<script language=\"javascript\">window.open('{0}','','width=300,height=300,resizable=no,directories=no,scrollbars=no' , false);</script>",sURL);
					ClientScript.RegisterStartupScript(Page.GetType(),"OpenNewWindow",sScripts);
				}
			}
		} else if (pAttachedType.Equals(ViCommConst.ATTACH_MOVIE_INDEX.ToString())) {
			using (UserManMovie oUserManMovie = new UserManMovie()) {
				if (oUserManMovie.GetOne(pSiteCd,pUserSeq,pUserCharNo,pMovieSeq)) {
					string sRoot = ConfigurationManager.AppSettings["Root"];
					string sURL = "http://" + Request.Url.Authority + sRoot + ViCommConst.WEB_MOVIE_PATH + "/" + oUserManMovie.siteCd + string.Format("/man/{0}{1}",iBridUtil.addZero(oUserManMovie.movieSeq,ViCommConst.OBJECT_NM_LENGTH),ViCommConst.MOVIE_FOODER);
					string sScripts = string.Format("<script language=\"javascript\">window.open('{0}','','width=300,height=300,resizable=no,directories=no,scrollbars=no' , false);</script>",sURL);
					ClientScript.RegisterStartupScript(Page.GetType(),"OpenNewWindow",sScripts);
				}
			}
		}
	}

	protected void grdMail_PreRender(object sender,EventArgs e) {
		foreach (GridViewRow gr in grdMail.Rows) {
			LinkButton lnkAttached = (LinkButton)gr.FindControl("lnkAttached") as LinkButton;
			string[] sKeys = lnkAttached.CommandArgument.ToString().Split(':');
			string pAttachedType = sKeys[2];
			
			if (pAttachedType.Equals(string.Empty)) {
				lnkAttached.Text = string.Empty;
			} else if (pAttachedType.Equals(ViCommConst.ATTACH_PIC_INDEX.ToString())) {
				lnkAttached.Text = "画像";
			} else if (pAttachedType.Equals(ViCommConst.ATTACH_MOVIE_INDEX.ToString())) {
				lnkAttached.Text = "動画";
			}

		}
	}

	protected void dsMailBox_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = ViewState["SITE_CD"].ToString();
		e.InputParameters[1] = userObjs.cast.userSeq;
		e.InputParameters[2] = ViewState["CAST_CHAR_NO"].ToString();
		e.InputParameters[3] = ViCommConst.MAIL_BOX_USER;
		e.InputParameters[4] = ViCommConst.RX;
	}
}