<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CastBbsPicMainte.aspx.cs" Inherits="Cast_CastBbsPicMainte"
	Title="掲示板画像設定" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="掲示板画像設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMain">
			<asp:Panel runat="server" ID="pnlDtl">
				<fieldset class="fieldset">
					<legend>[掲示板画像設定]</legend>
					<table border="0" style="width: 640px" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle">
								サイト
							</td>
							<td class="tdDataStyle">
								<asp:Label ID="lblSiteNm" runat="server" Text="">
								</asp:Label>
							</td>
						</tr>
						<asp:PlaceHolder ID="plcHolderUpload" runat="server">
							<tr>
								<td class="tdHeaderStyle">
									写真属性
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstCastPicAttrTypeUpload" runat="server" DataSourceID="dsCastPicAttrType" DataTextField="CAST_PIC_ATTR_TYPE_NM" DataValueField="CAST_PIC_ATTR_TYPE_SEQ"
										AutoPostBack="True" OnSelectedIndexChanged="lstCastPicAttrTypeUpload_SelectedIndexChanged">
									</asp:DropDownList>
									<asp:RequiredFieldValidator ID="vdrCastPicAttrTypeUpload" runat="server" ErrorMessage="写真属性を選択して下さい。" ControlToValidate="lstCastPicAttrTypeUpload" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									写真属性値
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstCastPicAttrTypeValueUpload" runat="server" DataSourceID="dsCastPicAttrTypeValueUpload" DataTextField="CAST_PIC_ATTR_NM" DataValueField="CAST_PIC_ATTR_SEQ">
									</asp:DropDownList>
									<asp:RequiredFieldValidator ID="vdrCastPicAttrTypeValueUpload" runat="server" ErrorMessage="写真属性値を選択して下さい。" ControlToValidate="lstCastPicAttrTypeValueUpload"
										ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
						</asp:PlaceHolder>
						<tr>
							<td class="tdHeaderStyle">
								掲示板タイトル
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtTitleUpload" runat="server" MaxLength="30" Width="313px" ValidationGroup="Upload"></asp:TextBox>
								<asp:RequiredFieldValidator ID="rfvTitleUpload" runat="server" ControlToValidate="txtTitleUpload" ErrorMessage="掲示板タイトルを入力してください。" SetFocusOnError="True"
									ValidationGroup="Upload">*</asp:RequiredFieldValidator></td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								掲示板本文
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtDocUpload" runat="server" Height="95px" MaxLength="100" TextMode="MultiLine" Width="313px" ValidationGroup="Upload"></asp:TextBox>
								<asp:RequiredFieldValidator ID="rfvDocUpload" runat="server" ControlToValidate="txtDocUpload" ErrorMessage="掲示板本文を入力してください。" SetFocusOnError="True" ValidationGroup="Upload">*</asp:RequiredFieldValidator></td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								写真アップロード
							</td>
							<td class="tdDataStyle">
								<asp:FileUpload ID="uldCastPic" runat="server" Width="400px" />
								<asp:RequiredFieldValidator ID="rfvUplod" runat="server" ErrorMessage="写真ファイルを指定して下さい。" SetFocusOnError="True" ValidationGroup="Upload" ControlToValidate="uldCastPic">*</asp:RequiredFieldValidator>
							</td>
						</tr>
					</table>
					<asp:Button ID="btnUpload" runat="server" ValidationGroup="Upload" Text="実行" CssClass="seekbutton" OnClick="btnUpload_Click" />
					<asp:Button runat="server" ID="btnCancel" Text="戻る" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					<br clear="all" />
					<asp:Panel runat="server" ID="pnlErrorMsg" CssClass="fieldset-inner">
						<asp:ValidationSummary ID="vdSummary" runat="server" ValidationGroup="Upload">
						</asp:ValidationSummary>
					</asp:Panel>
				</fieldset>
			</asp:Panel>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlMainte">
			<asp:Panel runat="server" ID="Panel2">
				<fieldset class="fieldset">
					<legend>[掲示板文章編集]</legend>
					<table border="0" style="width: 640px" class="tableStyle">
						<asp:PlaceHolder ID="plcHolder" runat="server">
							<tr>
								<td class="tdHeaderStyle">
									写真属性
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstCastPicAttrType" runat="server" DataSourceID="dsCastPicAttrType" DataTextField="CAST_PIC_ATTR_TYPE_NM" DataValueField="CAST_PIC_ATTR_TYPE_SEQ"
										AutoPostBack="True" OnSelectedIndexChanged="lstCastPicAttrType_SelectedIndexChanged">
									</asp:DropDownList>
									<asp:RequiredFieldValidator ID="vdrCastPicAttrType" runat="server" ErrorMessage="写真属性を選択して下さい。" ControlToValidate="lstCastPicAttrType" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									写真属性値
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstCastPicAttrTypeValue" runat="server" DataSourceID="dsCastPicAttrTypeValue" DataTextField="CAST_PIC_ATTR_NM" DataValueField="CAST_PIC_ATTR_SEQ">
									</asp:DropDownList>
									<asp:RequiredFieldValidator ID="vdrCastPicAttrTypeValue" runat="server" ErrorMessage="写真属性値を選択して下さい。" ControlToValidate="lstCastPicAttrTypeValue" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
						</asp:PlaceHolder>
						<tr>
							<td class="tdHeaderStyle">
								掲示板タイトル
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtPicTitle" runat="server" MaxLength="30" Width="313px"></asp:TextBox>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle" style="height: 74px">
								掲示板本文
							</td>
							<td class="tdDataStyle" style="height: 74px">
								<asp:TextBox ID="txtPicDoc" runat="server" MaxLength="100" Width="313px" Height="95px" TextMode="MultiLine"></asp:TextBox>
							</td>
						</tr>
					</table>
					<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" OnClientClick='return confirm("更新を行いますか？");' />
					<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" OnClientClick='return confirm("削除を行いますか？");' />
					<asp:Button runat="server" ID="btnMainteCancel" Text="戻る" CssClass="seekbutton" OnClick="btnMainteCancel_Click" CausesValidation="False" />
				</fieldset>
			</asp:Panel>
		</asp:Panel>
		<fieldset class="fieldset">
			<legend>[掲示板画像一覧]</legend>
			<asp:GridView ID="grdBbsPic" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsCastPic" AllowSorting="True" SkinID="GridView">
				<Columns>
					<asp:TemplateField HeaderText="設定">
						<ItemStyle HorizontalAlign="Center" />
						<ItemTemplate>
							<asp:LinkButton ID="lnkEditTitle" runat="server" CommandArgument='<%# Eval("PIC_SEQ") %>' OnCommand="lnkEditTitle_Command" Text="設定"></asp:LinkButton>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:BoundField DataField="PIC_TITLE" HeaderText="掲示板タイトル">
						<ItemStyle HorizontalAlign="Left" Width="140px" />
					</asp:BoundField>
					<asp:BoundField DataField="PIC_DOC" HeaderText="掲示板本文">
						<ItemStyle HorizontalAlign="Left" Width="180px" />
					</asp:BoundField>
					<asp:BoundField DataField="UPLOAD_DATE" HeaderText="アップロード日">
						<ItemStyle HorizontalAlign="Center" />
					</asp:BoundField>
					<asp:BoundField DataField="NOT_APPROVE_MARK" HeaderText="認証">
						<ItemStyle HorizontalAlign="Center" />
					</asp:BoundField>
					<asp:ImageField DataImageUrlField="OBJ_SMALL_PHOTO_IMG_PATH" HeaderText="写真" DataImageUrlFormatString="../{0}" SortExpression="PIC_SEQ">
					</asp:ImageField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<asp:Panel runat="server" ID="pnlHideValue" Visible="false">
				<asp:TextBox ID="txtObjNotApproveFlag" runat="server"></asp:TextBox>
				<asp:TextBox ID="txtObjNotPublishFlag" runat="server"></asp:TextBox>
			</asp:Panel>
		</fieldset>
	</div>
	<asp:ObjectDataSource ID="dsCastPic" runat="server" SelectMethod="GetPageCollection" TypeName="CastPic" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelecting="dsCastPic_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
			<asp:Parameter Name="pPicType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCastPicAttrType" runat="server" SelectMethod="GetList" TypeName="CastPicAttrType" OnSelecting="dsCastPicAttrType_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCastPicAttrTypeValue" runat="server" SelectMethod="GetList" TypeName="CastPicAttrTypeValue" OnSelecting="dsCastPicAttrTypeValue_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pCastPicAttrTypeSeq" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCastPicAttrTypeValueUpload" runat="server" SelectMethod="GetList" TypeName="CastPicAttrTypeValue" OnSelecting="dsCastPicAttrTypeValueUpload_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pCastPicAttrTypeSeq" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>
