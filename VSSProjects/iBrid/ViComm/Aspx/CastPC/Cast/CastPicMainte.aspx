<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CastPicMainte.aspx.cs" Inherits="Cast_CastPicMainte" Title="プロフィール写真設定" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="プロフィール写真設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<asp:Panel runat="server" ID="pnlDtl">
				<fieldset class="fieldset">
					<legend>[プロフィール写真設定]</legend>
					<table border="0" style="width: 640px" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle">
								サイト
							</td>
							<td class="tdDataStyle">
								<asp:Label ID="lblSiteNm" runat="server" Text="">
								</asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								写真アップロード
							</td>
							<td class="tdDataStyle">
								<asp:FileUpload ID="uldCastPic" runat="server" Width="400px" />
								<asp:RequiredFieldValidator ID="rfvUplod" runat="server" ErrorMessage="写真ファイルを指定して下さい。" SetFocusOnError="True" ValidationGroup="Upload" ControlToValidate="uldCastPic">*</asp:RequiredFieldValidator>
							</td>
						</tr>
					</table>
					<asp:Button ID="btnUpload" runat="server" ValidationGroup="Upload" Text="実行" CssClass="seekbutton" OnClick="btnUpload_Click" />
					<asp:Button runat="server" ID="btnCancel" Text="戻る" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					<br clear="all" />
					<asp:Panel runat="server" ID="pnlErrorMsg" CssClass="fieldset-inner">
						<asp:ValidationSummary ID="vdSummary" runat="server" ValidationGroup="Upload">
						</asp:ValidationSummary>
					</asp:Panel>
				</fieldset>
			</asp:Panel>
		</asp:Panel>
		<fieldset class="fieldset">
			<legend>[写真一覧]</legend>
			<asp:Label ID="lblStatus3" runat="server" Text="青枠" ForeColor="Blue"></asp:Label><asp:Label ID="lblStatus4" runat="server" Text="で囲まれた画像が"></asp:Label>
			<asp:Label ID="lblStatus5" runat="server" Text="現在プロフィールに指定し<br/>てある画像です。"></asp:Label>
			<asp:DataList ID="lstPic" runat="server" DataSourceID="dsCastPic" Width="300px">
				<ItemTemplate>
					<asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("OBJ_PHOTO_IMG_PATH","../{0}") %>' BorderColor='<%# GetBorderColor(Eval("PROFILE_PIC_FLAG")) %>' BorderStyle="Solid"
						BorderWidth="2" Width="50" Height="50" /><br />
					<asp:ImageButton ID="btnUp" runat="server" ImageUrl="../image/up.gif" OnClientClick="return confirm('表示を上にしますか？');" OnCommand="btnUp_Command" CommandArgument='<%# Eval("PIC_SEQ") %>'
						Visible='<%# Eval("UP_BTN_FLAG") %>' />&nbsp;
					<asp:ImageButton ID="btnDown" runat="server" ImageUrl="../image/down.gif" OnClientClick="return confirm('表示を下にしますか？');" OnCommand="btnDown_Command" CommandArgument='<%# Eval("PIC_SEQ") %>'
						Visible='<%# Eval("DOWN_BTN_FLAG") %>' />&nbsp;
					<asp:ImageButton ID="btnDelete" runat="server" ImageUrl="../image/del.gif" CommandArgument='<%# Eval("PIC_SEQ") %>' OnClientClick="return confirm('削除を行いますか？');"
						OnCommand="btnDelete_Command" /><br />
					<asp:ImageButton ID="btnProfile" runat="server" ImageUrl="../image/prof.gif" CommandArgument='<%# Eval("PIC_SEQ") %>' OnClientClick="return confirm('設定を行いますか？');"
						OnCommand="btnProfile_Command" /><br />
					<br />
				</ItemTemplate>
			</asp:DataList>
		</fieldset>
	</div>
	<asp:ObjectDataSource ID="dsCastPic" runat="server" SelectMethod="GetList" TypeName="CastPic" OnSelecting="dsCastPic_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
			<asp:Parameter Name="pPicType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>
