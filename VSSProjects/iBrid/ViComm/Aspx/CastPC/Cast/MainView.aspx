<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MainView.aspx.cs" Inherits="Cast_MainView" Title="トップメニュー" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmNm" runat="server" Text="トップメニュー"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HolderContent" runat="Server">
	<asp:Panel runat="server" ID="pnlSeek">
		<asp:Panel runat="server" ID="pnlKey">
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlDtl" HorizontalAlign="Left">
			<table border="0">
				<tr>
					<td>
						<asp:Panel runat="server" ID="pnlInfo">
							<asp:DetailsView ID="dvwCast" runat="server" DataSourceID="dsCast" AutoGenerateRows="False" SkinID="DetailsView" Width="345px">
								<Fields>
									<asp:TemplateField HeaderText="IＤ・ﾊﾟｽﾜｰﾄﾞ">
										<ItemTemplate>
											<asp:Label ID="Label1" runat="server" Text='<%# String.Format("{0}({1})",Eval("LOGIN_ID"),Eval("LOGIN_PASSWORD")) %>'></asp:Label>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:BoundField DataField="CAST_NM" HeaderText="名前" />
									<asp:TemplateField HeaderText="業務終了予定">
										<ItemTemplate>
											<asp:Label ID="Label2" runat="server" Text='<%# String.Format("{0:MM/dd HH:mm}",Eval("END_DATE")) %>'></asp:Label>
											<asp:Button ID="btnDummyTalk" runat="server" Text="<%#GetDummyTalkGuidance() %>" Width="80px" OnClick="btnDummyTalk_Click" />
										</ItemTemplate>
									</asp:TemplateField>
								</Fields>
								<HeaderStyle Wrap="False" />
							</asp:DetailsView>
						</asp:Panel>
					</td>
					<td width="10">
					</td>
					<td valign="top">
						<asp:Button ID="btnChangeCharacter" runat="server" Text="キャラクター設定" Width="120px" OnClick="btnChangeCharacter_Click" /><br />
						<asp:Button ID="btnChangeSeekMan" runat="server" Text="男性会員検索" Width="120px" OnClick="btnChangeSeekMan_Click" /><br />
						<asp:Button ID="btnChanegMailBox" runat="server" Text="メールＢＯＸ" Width="120px" OnClick="btnChanegMailBox_Click" />
					</td>
					<td width="10">
					</td>
					<td valign="top">
						<asp:Button ID="btnOnlineMonitor" runat="server" Text="オンラインモニタ開始" Width="120px" OnClick="btnOnlineMonitor_Click" /><br />
						<asp:Button ID="btnPicMovie" runat="server" Text="画像・動画設定" Width="120px" OnClick="btnPicMovie_Click" /><br />
						<asp:Button ID="btnPerformance" runat="server" Text="稼動集計" Width="120px" OnClick="btnPerformance_Click" />
					</td>
					<td width="10">
					</td>
					<td valign="top">
						<asp:Button ID="btnLiveTimer" runat="server" Text="ライブタイマー設定" Width="120px" OnClick="btnLiveTimer_Click" /><br />
						<asp:Button ID="btnReSeek" runat="server" Text="再表示" Width="120px" OnClick="btnReSeek_Click" />
					</td>
				</tr>
			</table>
			<asp:Panel ID="pnlDummyTalk" runat="server" Height="50px">
				<div align="center">
					<br />
					<asp:Label ID="Label3" runat="server" Text="只今、休憩中です。　　只今、休憩中です。　　只今、休憩中です。" BackColor="#E0E0E0" BorderColor="White" BorderStyle="Solid" BorderWidth="1px"
						Font-Bold="True" Font-Size="Large" ForeColor="Red"></asp:Label>
				</div>
			</asp:Panel>
			<asp:Panel runat="server" ID="pnlCharacter">
				<fieldset class="fieldset">
					<legend>[<asp:Label ID="lblInfoTitle" runat="server" Text="キャラクター設定"></asp:Label>]</legend>
					<asp:GridView ID="grdCharacter" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsCharacter" AllowSorting="True" SkinID="GridView"
						OnDataBinding="grdCharacter_DataBinding">
						<Columns>
							<asp:TemplateField HeaderText="ｻｲﾄ">
								<ItemTemplate>
									<asp:HyperLink ID="lnkSiteCd" runat="server" Text='<%# Eval("SITE_CD") %>' NavigateUrl='<%# string.Format("MainView.aspx?sitecd={0}",Eval("SITE_CD")) %>'
										Enabled='<%# IsMultiCharFlg(Eval("SITE_CD")) %>' />
									<asp:HyperLink ID="lnkLoginHistory" runat="server" NavigateUrl='<%# string.Format("CastLoginHistory.aspx?sitecd={0}&castcharno={1}",Eval("SITE_CD"),Eval("USER_CHAR_NO")) %>'
										Text="稼動" Visible='<%#IsNewSite(Eval("USE_OTHER_SYS_INFO_FLAG")) %>'></asp:HyperLink>
									<asp:HyperLink ID="lnkLoginCharacterSelect" runat="server" NavigateUrl='<%# string.Format("LoginCharacterSelect.aspx?sitecd={0}",Eval("SITE_CD")) %>' Text="ｷｬﾗ選択"
										Visible="false"></asp:HyperLink><!--'<%#IsSelectChar(Eval("SITE_CD")) %>'-->
									<br />
									<asp:Label ID="lblSiteNm" runat="server" Text='<%# Bind("SITE_NM") %>'></asp:Label>
									<asp:Label ID="lblUserCharNo" runat="server" Text='<%# SetUserCharNo(Eval("SITE_CD"),Eval("USER_SEQ"),Eval("USER_CHAR_NO")) %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾊﾝﾄﾞﾙ名/ｶﾃｺﾞﾘ">
								<ItemTemplate>
									<asp:HyperLink ID="lnkCharacterMainte" runat="server" NavigateUrl='<%# string.Format("CastCharacterMainte.aspx?sitecd={0}&castcharno={1}",Eval("SITE_CD"),Eval("USER_CHAR_NO")) %>'
										Text='<%# Eval("HANDLE_NM")%>'></asp:HyperLink>
									<br>
									<asp:Label ID="Label4" runat="server" Text='<%# Eval("ACT_CATEGORY_NM") %>'></asp:Label><br>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾌﾟﾛﾌｨｰﾙ<br>写真">
								<ItemTemplate>
									<asp:ImageButton ID="btnPicMainte" runat="server" Width="50" Height="50" BorderWidth="2px" BorderStyle="solid" BorderColor="#00C0C0" ImageUrl='<%# Eval("SMALL_PHOTO_IMG_PATH","../{0}") %>'
										PostBackUrl='<%# string.Format("CastPicMainte.aspx?sitecd={0}&castcharno={1}",Eval("SITE_CD"),Eval("USER_CHAR_NO")) %>' />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="動画<br>撮影">
								<ItemTemplate>
									<asp:HyperLink ID="lnkProfileMovie" runat="server" NavigateUrl='<%# string.Format("CastProfileMovieList.aspx?sitecd={0}&castcharno={1}",Eval("SITE_CD"),Eval("USER_CHAR_NO")) %>'
										Text="選択"></asp:HyperLink>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" Width="46px" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="日記<br>書込">
								<ItemTemplate>
									<asp:HyperLink ID="lnk1" runat="server" NavigateUrl='<%# string.Format("CastDiaryMainte.aspx?sitecd={0}&userseq={1}&castcharno={2}",Eval("SITE_CD"),Eval("USER_SEQ"),Eval("USER_CHAR_NO")) %>'
										Text="編集" Visible='<%#IsNewSite(Eval("USE_OTHER_SYS_INFO_FLAG")) %>'></asp:HyperLink>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" Width="46px" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="気に入<br>られ">
								<ItemTemplate>
									<asp:HyperLink ID="lnkLikeMeList" runat="server" NavigateUrl='<%# string.Format("CastLikeMeList.aspx?sitecd={0}&castcharno={1}",Eval("SITE_CD"),Eval("USER_CHAR_NO")) %>'
										Text="一覧" Visible='<%#IsNewSite(Eval("USE_OTHER_SYS_INFO_FLAG"))%>'></asp:HyperLink>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" Width="46px" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="お気に<br>入り">
								<ItemTemplate>
									<asp:HyperLink ID="lnkFavorit" runat="server" NavigateUrl='<%# string.Format("CastFavoritList.aspx?sitecd={0}&castcharno={1}",Eval("SITE_CD"),Eval("USER_CHAR_NO")) %>'
										Text="一覧" Visible='<%#IsNewSite(Eval("USE_OTHER_SYS_INFO_FLAG")) %>'></asp:HyperLink>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" Width="46px" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="会話<br>履歴">
								<ItemTemplate>
									<asp:HyperLink ID="lnkTalkHistory" runat="server" NavigateUrl='<%# string.Format("CastTalkHistoryList.aspx?sitecd={0}&castcharno={1}",Eval("SITE_CD"),Eval("USER_CHAR_NO")) %>'
										Text="一覧" Visible='<%#IsNewSite(Eval("USE_OTHER_SYS_INFO_FLAG")) %>'></asp:HyperLink>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" Width="46px" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="足あと">
								<ItemTemplate>
									<asp:HyperLink ID="lnkMarking" runat="server" NavigateUrl='<%# string.Format("CastMarkingList.aspx?sitecd={0}&castcharno={1}",Eval("SITE_CD"),Eval("USER_CHAR_NO")) %>'
										Text="一覧" Visible='<%#IsNewSite(Eval("USE_OTHER_SYS_INFO_FLAG"))%>'></asp:HyperLink>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" Width="46px" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="拒否">
								<ItemTemplate>
									<asp:HyperLink ID="lnkRefuse" runat="server" NavigateUrl='<%# string.Format("CastRefuseList.aspx?sitecd={0}&castcharno={1}",Eval("SITE_CD"),Eval("USER_CHAR_NO")) %>'
										Text="一覧" Visible='<%#IsNewSite(Eval("USE_OTHER_SYS_INFO_FLAG"))%>'></asp:HyperLink>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" Width="46px" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ｵﾝﾗｲﾝ<br>検索">
								<ItemTemplate>
									<asp:HyperLink ID="lnkManOnline" runat="server" NavigateUrl='<%# string.Format("../Man/ManOnlineList.aspx?sitecd={0}&castcharno={1}",Eval("SITE_CD"),Eval("USER_CHAR_NO")) %>'
										Text="検索" Visible='<%#IsNewSite(Eval("USE_OTHER_SYS_INFO_FLAG"))%>'></asp:HyperLink>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" Width="46px" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="条件<br>検索">
								<ItemTemplate>
									<asp:HyperLink ID="lnkManCondition" runat="server" NavigateUrl='<%# string.Format("../Man/ManConditionList.aspx?sitecd={0}&castcharno={1}",Eval("SITE_CD"),Eval("USER_CHAR_NO")) %>'
										Text="検索" Visible='<%#IsNewSite(Eval("USE_OTHER_SYS_INFO_FLAG"))%>'></asp:HyperLink>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" Width="46px" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="全体<br>検索">
								<ItemTemplate>
									<asp:HyperLink ID="lnkAll" runat="server" NavigateUrl='<%# string.Format("../Man/ManAllList.aspx?sitecd={0}&castcharno={1}",Eval("SITE_CD"),Eval("USER_CHAR_NO")) %>'
										Text="検索" Visible='<%#IsNewSite(Eval("USE_OTHER_SYS_INFO_FLAG"))%>'></asp:HyperLink>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" Width="46px" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="受信<br>BOX">
								<ItemTemplate>
									<asp:HyperLink ID="lnkRxBox" runat="server" NavigateUrl='<%# string.Format("CastRxMail.aspx?sitecd={0}&castcharno={1}",Eval("SITE_CD"),Eval("USER_CHAR_NO")) %>'
										Text="一覧" Visible='<%#IsNewSite(Eval("USE_OTHER_SYS_INFO_FLAG"))%>'></asp:HyperLink>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" Width="46px" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="送信<br>BOX">
								<ItemTemplate>
									<asp:HyperLink ID="lnkTxBox" runat="server" NavigateUrl='<%# string.Format("CastTxMail.aspx?sitecd={0}&castcharno={1}",Eval("SITE_CD"),Eval("USER_CHAR_NO")) %>'
										Text="一覧" Visible='<%#IsNewSite(Eval("USE_OTHER_SYS_INFO_FLAG"))%>'></asp:HyperLink>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" Width="46px" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="定型<br>文章">
								<ItemTemplate>
									<asp:LinkButton ID="lnkMailTemplate" runat="server" CausesValidation="false" CommandArgument='<%#string.Format("{0}:{1}",Eval("SITE_CD"),Eval("USER_CHAR_NO")) %>'
										CommandName="" OnCommand="lnkMailTemplate_Command" Text="編集" Visible='<%#IsNewSite(Eval("USE_OTHER_SYS_INFO_FLAG"))%>'></asp:LinkButton>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" Width="46px" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="メール添付<br>画像設定">
								<ItemTemplate>
									<asp:HyperLink ID="lnkMailPicMainte" runat="server" NavigateUrl='<%# string.Format("CastMailPicMainte.aspx?sitecd={0}&castcharno={1}",Eval("SITE_CD"),Eval("USER_CHAR_NO")) %>'
										Text="選択" Visible='<%#IsNewSite(Eval("USE_OTHER_SYS_INFO_FLAG"))%>'></asp:HyperLink>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" Width="66px" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="掲示板<br>画像設定">
								<ItemTemplate>
									<asp:HyperLink ID="lnkBbsPicMainte" runat="server" NavigateUrl='<%# string.Format("CastBbsPicMainte.aspx?sitecd={0}&castcharno={1}",Eval("SITE_CD"),Eval("USER_CHAR_NO")) %>'
										Text="選択" Visible='<%#IsNewSite(Eval("USE_OTHER_SYS_INFO_FLAG"))%>'></asp:HyperLink>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" Width="46px" />
							</asp:TemplateField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</fieldset>
			</asp:Panel>
		</asp:Panel>
	</asp:Panel>
	<asp:ObjectDataSource ID="dsCast" runat="server" SelectMethod="GetOne" TypeName="Cast" OnSelecting="dsCast_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pLoginID" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCharacter" runat="server" SelectMethod="GetCharacterList" TypeName="CastCharacter" OnSelecting="dsCharacter_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCD" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>
