﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: 出演者ムービー一覧
--	Progaram ID		: CastProfileMovieList
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date			Updater		Update Explain
  2010/06/11	M.Horie		属性区分名、属性値名を一覧に表示させる（条件付）  2010/07/16	Koyanagi	非公開ﾌﾗｸﾞ対応


-------------------------------------------------------------------------*/
using System;
using System.Configuration;
using System.Web.UI.WebControls;
using iBridCommLib;
using ViComm;

public partial class Cast_CastProfileMovieList:System.Web.UI.Page {
	private SessionObjs userObjs;
	private string sRecordProfile = "プロフィール動画録画";
	private string sRecordBbs = "掲示板動画録画";
	private string sRecordMail = "メール添付動画録画";

	protected void Page_Load(object sender,EventArgs e) {
		userObjs = (SessionObjs)Session["objs"];
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.DOCOMO);
		if (!IsPostBack) {
			FirstLoad();
			InitPage();
		}
		if (userObjs.site.GetOne(iBridUtil.GetStringValue(ViewState["SITE_CD"]))) {
			lblSiteNm.Text = userObjs.site.siteNm;
			if (userObjs.site.IsActiveMulitCharFlag(ViewState["SITE_CD"].ToString())) {
				lblSiteNm.Text += string.Concat("-",ViewState["CAST_CHAR_NO"].ToString());
			}
		}
	}

	private void FirstLoad() {
		CreateMovieTypeList();
		btnRecording.Text = sRecordProfile;
		ViewState["DISP_MOVIE_MODE"] = "1";
	}

	private void SetViewState() {
		ViewState["SITE_CD"] = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		ViewState["CAST_CHAR_NO"] = iBridUtil.GetStringValue(Request.QueryString["castcharno"]);
		using (SiteManagement oSiteManagement = new SiteManagement()) {
			oSiteManagement.GetOne(ViewState["SITE_CD"].ToString());
			ViewState["ATTR_FLAG"] = oSiteManagement.bbsMovieAttrFlag.ToString();
		}
	}

	private void InitPage() {
		SetViewState();
		ClearField();
		pnlMainte.Visible = false;
		switch (iBridUtil.GetStringValue(ViewState["DISP_MOVIE_MODE"])) {
			case "1":
				lblTitleMainte.Text = "プロフィールタイトル編集";
				lblMovieList.Text = "プロフィール動画一覧";
				btnRecording.Text = sRecordProfile;
				plcHolder.Visible = false;
				break;
			case "2":
				lblTitleMainte.Text = "掲示板文章編集";
				lblMovieList.Text = "掲示板動画一覧";
				btnRecording.Text = sRecordBbs;
				if (!ViewState["ATTR_FLAG"].ToString().Equals(ViCommConst.FLAG_OFF)) {
					plcHolder.Visible = false;
				} else {
					plcHolder.Visible = true;
				}
				break;
			case "3":
				lblTitleMainte.Text = "メール添付動画タイトル編集";
				lblMovieList.Text = "メール添付動画一覧";
				btnRecording.Text = sRecordMail;
				plcHolder.Visible = false;
				break;
		}
		if (ViewState["ATTR_FLAG"].ToString().Equals(ViCommConst.FLAG_OFF.ToString())) {
			grdProfileMovie.Columns[2].Visible = false;
			grdProfileMovie.Columns[3].Visible = false;
		} else {
			grdProfileMovie.Columns[2].Visible = true;
			grdProfileMovie.Columns[3].Visible = true;
		}
		DataBind();
	}

	private void ClearField() {
	}

	private void CreateMovieTypeList() {
		lstMovieType.Items.Clear();
		lstMovieType.Items.Add(new ListItem("プロフィール",ViCommConst.ATTACHED_PROFILE.ToString()));
		lstMovieType.Items.Add(new ListItem("掲示板",ViCommConst.ATTACHED_BBS.ToString()));
		lstMovieType.Items.Add(new ListItem("メール添付",ViCommConst.ATTACHED_MAIL.ToString()));

	}
	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		Server.Transfer("MainView.aspx");
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		ViewState["DISP_MOVIE_MODE"] = lstMovieType.SelectedValue.ToString();
		InitPage();
	}

	protected void lnkEditTitle_Command(object sender,CommandEventArgs e) {
		ViewState["MOVIE_SEQ"] = e.CommandArgument.ToString();
		GetData();
	}

	protected void lnkProfileQuickTime_Command(object sender,CommandEventArgs e) {
		using (CastMovie oMovie = new CastMovie()) {
			if (oMovie.GetOne(e.CommandArgument.ToString())) {
				string sRoot = ConfigurationManager.AppSettings["Root"];
				string sURL = "http://" + Request.Url.Authority + sRoot + ViCommConst.WEB_MOVIE_PATH + "/" + oMovie.siteCd + string.Format("/operator/{0}/{1}{2}",oMovie.loginId,iBridUtil.addZero(oMovie.movieSeq,ViCommConst.OBJECT_NM_LENGTH),ViCommConst.MOVIE_FOODER);
				string sScripts = string.Format("<script language=\"javascript\">window.open('{0}','','width=300,height=300,resizable=no,directories=no,scrollbars=no' , false);</script>",sURL);
				ClientScript.RegisterStartupScript(Page.GetType(),"OpenNewWindow",sScripts);
			}
		}
	}
	protected void dsCastMovie_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = ViewState["SITE_CD"].ToString();
		e.InputParameters[1] = userObjs.cast.userSeq;
		e.InputParameters[2] = ViewState["CAST_CHAR_NO"].ToString();
		e.InputParameters[3] = ViewState["DISP_MOVIE_MODE"].ToString();
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {

			db.PrepareProcedure("CAST_MOVIE_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,ViewState["SITE_CD"].ToString());
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,userObjs.cast.userSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,ViewState["CAST_CHAR_NO"].ToString());
			db.ProcedureInParm("PMOVIE_SEQ",DbSession.DbType.VARCHAR2,ViewState["MOVIE_SEQ"].ToString());
			db.ProcedureOutParm("PMOVIE_TITLE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMOVIE_DOC",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCHARGE_POINT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("POBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("POBJ_NOT_PUBLISH_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PUPLOAD_DATE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMOVIE_TYPE",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCAST_MOVIE_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCAST_MOVIE_ATTR_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPLAY_TIME",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMOVIE_SERIES",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSAMPLE_MOVIE_SEQ",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PTHUMBNAIL_PIC_SEQ",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");
			ViewState["DISP_MOVIE_MODE"] = db.GetStringValue("PMOVIE_TYPE");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtMovieTitle.Text = db.GetStringValue("PMOVIE_TITLE");
				txtMovieDoc.Text = db.GetStringValue("PMOVIE_DOC");
				txtChargePoint.Text = db.GetStringValue("PCHARGE_POINT");
				txtObjNotApproveFlag.Text = db.GetStringValue("POBJ_NOT_APPROVE_FLAG");
				txtObjNotPublishFlag.Text = db.GetStringValue("POBJ_NOT_PUBLISH_FLAG");
			} else {
				ClearField();
			}

			switch (ViewState["DISP_MOVIE_MODE"].ToString()) {
				case "1":
					txtMovieDoc.Visible = false;
					trMovieDoc.Visible = false;
					plcHolder.Visible = false;
					ViewState["ATTR_TYPE"] = ViCommConst.DEFAULT_CAST_MOVIE_ATTR_TYPE_SEQ.ToString();
					ViewState["ATTR_TYPE_VALUE"] = ViCommConst.DEFAULT_CAST_MOVIE_ATTR_SEQ.ToString();
					break;
				case "2":
					txtMovieDoc.Visible = true;
					trMovieDoc.Visible = true;
					ViewState["ATTR_TYPE"] = db.GetStringValue("PCAST_MOVIE_ATTR_TYPE_SEQ");
					ViewState["ATTR_TYPE_VALUE"] = db.GetStringValue("PCAST_MOVIE_ATTR_SEQ");
					if (ViewState["ATTR_FLAG"].ToString().Equals(ViCommConst.FLAG_OFF.ToString())) {
						plcHolder.Visible = false;
					} else {
						plcHolder.Visible = true;
					}
					break;
				case "3":
					txtMovieDoc.Visible = false;
					trMovieDoc.Visible = false;
					plcHolder.Visible = false;
					ViewState["ATTR_TYPE"] = ViCommConst.DEFAULT_CAST_MOVIE_ATTR_TYPE_SEQ.ToString();
					ViewState["ATTR_TYPE_VALUE"] = ViCommConst.DEFAULT_CAST_MOVIE_ATTR_SEQ.ToString();
					break;
			}
		}
		if (ViewState["DISP_MOVIE_MODE"].ToString().Equals(ViCommConst.ATTACHED_BBS.ToString())) {
			if (!ViewState["ATTR_TYPE"].ToString().Equals(ViCommConst.DEFAULT_CAST_MOVIE_ATTR_TYPE_SEQ.ToString())) {
				lstCastMovieAttrType.DataSourceID = "dsCastMovieAttrType";
				lstCastMovieAttrType.DataBind();
				lstCastMovieAttrType.DataSourceID = "";
				lstCastMovieAttrType.SelectedValue = ViewState["ATTR_TYPE"].ToString();
				lstCastMovieAttrTypeValue.DataSourceID = "dsCastMovieAttrTypeValue";
				lstCastMovieAttrTypeValue.DataBind();
				lstCastMovieAttrTypeValue.DataSourceID = "";
				lstCastMovieAttrTypeValue.SelectedValue = ViewState["ATTR_TYPE_VALUE"].ToString();
			} else {
				lstCastMovieAttrType.DataSourceID = "dsCastMovieAttrType";
				lstCastMovieAttrType.DataBind();
				lstCastMovieAttrType.DataSourceID = "";
				lstCastMovieAttrTypeValue.DataSourceID = "dsCastMovieAttrTypeValue";
				lstCastMovieAttrTypeValue.DataBind();
				lstCastMovieAttrTypeValue.DataSourceID = "";
			}
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
	}

	private void UpdateData(int pDelFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_MOVIE_MAINTE");
			db.ProcedureInParm("PMOVIE_SEQ",DbSession.DbType.NUMBER,decimal.Parse(ViewState["MOVIE_SEQ"].ToString()));
			db.ProcedureInParm("PMOVIE_TITLE",DbSession.DbType.VARCHAR2,txtMovieTitle.Text);
			db.ProcedureInParm("PMOVIE_DOC",DbSession.DbType.VARCHAR2,txtMovieDoc.Text);
			db.ProcedureInParm("PCHARGE_POINT",DbSession.DbType.NUMBER,decimal.Parse(txtChargePoint.Text));
			db.ProcedureInParm("POBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER,decimal.Parse(txtObjNotApproveFlag.Text));
			db.ProcedureInParm("POBJ_NOT_PUBLISH_FLAG",DbSession.DbType.NUMBER,decimal.Parse(txtObjNotPublishFlag.Text));
			db.ProcedureInParm("PMOVIE_TYPE",DbSession.DbType.NUMBER,ViewState["DISP_MOVIE_MODE"].ToString());
			if (ViewState["ATTR_FLAG"].ToString().Equals(ViCommConst.FLAG_OFF.ToString())) {
				db.ProcedureInParm("PCAST_MOVIE_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,ViewState["ATTR_TYPE"].ToString());
				db.ProcedureInParm("PCAST_MOVIE_ATTR_SEQ",DbSession.DbType.VARCHAR2,ViewState["ATTR_TYPE_VALUE"].ToString());
			} else {
				db.ProcedureInParm("PCAST_MOVIE_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,lstCastMovieAttrType.SelectedValue);
				db.ProcedureInParm("PCAST_MOVIE_ATTR_SEQ",DbSession.DbType.VARCHAR2,lstCastMovieAttrTypeValue.SelectedValue);
			}
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureInParm("PPLAY_TIME",DbSession.DbType.VARCHAR2,null);
			db.ProcedureInParm("PMOVIE_SERIES_SEQ",DbSession.DbType.NUMBER,null);
			db.ProcedureInParm("PTHUMBNAIL_PIC_SEQ",DbSession.DbType.NUMBER,null);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}

		if (pDelFlag == 1) {
			string sWebPhisicalDir = "";
			using (Site oSite = new Site()) {
				oSite.GetValue(ViewState["SITE_CD"].ToString(),"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
			}
			string sMovie = sWebPhisicalDir +
								ViCommConst.MOVIE_DIRECTRY + "\\" + ViewState["SITE_CD"].ToString() + "\\Operator\\" + userObjs.cast.loginId + "\\" +
								ViCommConst.MOVIE_HEADER + iBridUtil.addZero(ViewState["MOVIE_SEQ"].ToString(),ViCommConst.OBJECT_NM_LENGTH) +
								ViCommConst.MOVIE_FOODER;

			using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
				if (System.IO.File.Exists(sMovie)) {
					System.IO.File.Delete(sMovie);
				}

				sMovie = sWebPhisicalDir +
									ViCommConst.MOVIE_DIRECTRY + "\\" + ViewState["SITE_CD"].ToString() + "\\Operator\\" + userObjs.cast.loginId + "\\" +
									ViCommConst.MOVIE_HEADER + iBridUtil.addZero(ViewState["MOVIE_SEQ"].ToString(),ViCommConst.OBJECT_NM_LENGTH) +
									".3g2";

				if (System.IO.File.Exists(sMovie)) {
					System.IO.File.Delete(sMovie);
				}
			}
		}
		InitPage();
	}

	protected void btnRecording_Click(object sender,EventArgs e) {
		string sObjectSendMailAddr;
		string sMovieType;

		if (btnRecording.Text.Equals(sRecordProfile)) {
			sMovieType = ViCommConst.ATTACHED_PROFILE.ToString();
		} else if (btnRecording.Text.Equals(sRecordBbs)) {
			sMovieType = ViCommConst.ATTACHED_BBS.ToString();
		} else {
			sMovieType = ViCommConst.ATTACHED_MAIL.ToString();
		}

		sObjectSendMailAddr = ViCommConst.MOVIE_URL_HEADER2 + sMovieType + ViewState["SITE_CD"].ToString() + "_" + userObjs.cast.loginId + ViewState["CAST_CHAR_NO"].ToString() + "_@" + userObjs.site.mailHost;
		using (Site oSite = new Site())
		using (IvpRequest oIvpRequest = new IvpRequest()) {
			if (oSite.GetOne(iBridUtil.GetStringValue(ViewState["SITE_CD"]))) {
				string sResult = oIvpRequest.Request(
										oSite.siteCd,
										oSite.ivpLocCd,
										oSite.ivpSiteCd,
										ViCommConst.REQUEST_DIALOUT_RECORDING,
										userObjs.cast.uri,
										userObjs.cast.userSeq,
										ViewState["CAST_CHAR_NO"].ToString(),
										"01",	// VIDEO
										sObjectSendMailAddr,
										0,
										"",
										ViCommConst.UseCrosmile.USE_UAC_AND_UAS);
				if (sResult.Equals(ViCommConst.SP_RESULT_OK)) {
				} else {
				}
			}
		}
	}
	protected void lstMovieType_SelectedIndexChanged(object sender,EventArgs e) {
		ViewState["DISP_MOVIE_MODE"] = lstMovieType.SelectedValue.ToString();
		InitPage();
	}
	protected void lstCastMovieAttrType_SelectedIndexChanged(object sender,EventArgs e) {
		lstCastMovieAttrTypeValue.DataSourceID = "dsCastMovieAttrTypeValue";
		lstCastMovieAttrTypeValue.DataBind();
		lstCastMovieAttrTypeValue.DataSourceID = "";
	}

	protected void dsCastMovieAttrType_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = ViewState["SITE_CD"].ToString();
	}

	protected void dsCastMovieAttrTypeValue_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = ViewState["SITE_CD"].ToString();
		e.InputParameters[1] = lstCastMovieAttrType.SelectedValue;
	}
}
