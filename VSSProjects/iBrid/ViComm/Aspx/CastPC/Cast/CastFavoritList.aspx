<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CastFavoritList.aspx.cs" Inherits="Cast_CastFavoritList" Title="お気に入り一覧" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="お気に入り一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<fieldset class="fieldset">
		<legend>[お気に入り一覧]</legend>
		<table border="1">
			<tr>
				<td width="240" align="center">
					<asp:Label ID="lblSiteNm" runat="server" Font-Bold="True" Font-Underline="False" ForeColor="#FF8000"></asp:Label>
				</td>
				<td align="center" width="30">
					<asp:Button runat="server" ID="btnSeek" Text="検索" OnClick="btnSeek_Click" />
				</td>
				<td align="center">
					<asp:Button runat="server" ID="btnReturn" Text="戻る" OnClick="btnCancel_Click" />
				</td>
			</tr>
		</table>
		<asp:GridView ID="grdProfileMovie" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsFavorit" AllowSorting="True" SkinID="GridView">
			<Columns>
				<asp:BoundField DataField="FAVORIT_REGIST_DATE" HeaderText="お気に入り登録日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
					<ItemStyle HorizontalAlign="Center" />
					<HeaderStyle HorizontalAlign="Center" />
				</asp:BoundField>
				<asp:TemplateField HeaderText="ハンドル名">
					<ItemStyle HorizontalAlign="Left" Width="200px" />
					<ItemTemplate>
						<asp:HyperLink ID="lnkManView" runat="server" NavigateUrl='<%# string.Format("~/Man/ManView.aspx?sitecd={0}&manuserseq={1}&castcharno={2}&return=../Cast/CastFavoritList.aspx",Eval("SITE_CD"),Eval("PARTNER_USER_SEQ"),Eval("USER_CHAR_NO")) %>'
							Text='<%# Eval("HANDLE_NM") %>'></asp:HyperLink>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:BoundField DataField="LAST_TALK_DATE" HeaderText="最終会話日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
					<ItemStyle HorizontalAlign="Center" />
				</asp:BoundField>
				<asp:BoundField DataField="TALK_COUNT" HeaderText="会話数">
					<ItemStyle HorizontalAlign="Right" Width="40px" />
				</asp:BoundField>
				<asp:BoundField DataField="LAST_LOGIN_DATE" HeaderText="最終ﾛｸﾞｲﾝ日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
					<ItemStyle HorizontalAlign="Center" />
				</asp:BoundField>
				<asp:TemplateField HeaderText="状態">
					<ItemTemplate>
						<asp:Image ID="Image2" runat="server" ImageUrl='<%# GetSysImage(Eval("CHARACTER_ONLINE_STATUS") )%>' />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
			<PagerSettings Mode="NumericFirstLast" />
		</asp:GridView>
	</fieldset>
	<asp:ObjectDataSource ID="dsFavorit" runat="server" SelectMethod="GetPageCollection" TypeName="Favorit" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelecting="dsFavorit_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
			<asp:Parameter DefaultValue="0" Name="pLikeMeFlag" Type="Int16" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>
