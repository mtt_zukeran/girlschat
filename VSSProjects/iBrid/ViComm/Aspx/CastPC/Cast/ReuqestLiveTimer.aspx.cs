﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: ライブタイマー開始要求
--	Progaram ID		: ReuqestLiveTimer
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using iBridCommLib;
using ViComm;

public partial class Cast_ReuqestLiveTimer:System.Web.UI.Page {
	private SessionObjs userObjs;


	protected void Page_Load(object sender,EventArgs e) {
		userObjs = (SessionObjs)Session["objs"];
		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
		ClearField();
		DataBind();
		if (!IsPostBack) {
		}
	}

	private void ClearField() {
	}

	protected void btnOnlineMonitor_Click(object sender,EventArgs e) {
		string sSiteCd = "";
		using (Sys oSys = new Sys()) {
			oSys.GetValue("ONLINE_MONITOR_SITE_CD",out sSiteCd);
		}
		userObjs.site.GetOne(sSiteCd);

		if (IsValid) {
			int iSec;
			int.TryParse(txtBroadcastMin.Text,out iSec);
			iSec = iSec * 60;

			using (IvpRequest oIvpRequest = new IvpRequest()) {
				string sResult = oIvpRequest.Request(
					sSiteCd,
					userObjs.site.ivpLocCd,
					userObjs.site.ivpSiteCd,
					ViCommConst.REQUEST_BROADCASTING,
					userObjs.cast.uri,
					userObjs.cast.userSeq,
					ViComm.ViCommConst.MAIN_CHAR_NO,
					"01",	// VIDEO
					"",
					iSec,
					userObjs.site.supportChgMonitorToTalk.ToString(),
					ViCommConst.UseCrosmile.USE_UAC_AND_UAS);
				if (sResult.Equals(ViCommConst.SP_RESULT_OK)) {
				} else {
				}
			}
		}
	}

}
