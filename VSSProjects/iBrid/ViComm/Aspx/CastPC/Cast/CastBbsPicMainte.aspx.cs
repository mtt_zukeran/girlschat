﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: 出演者掲示板画像設定--	Progaram ID		: CastBbsPicMainte
--
--  Creation Date	: 2009.09.10
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date			Updater		Update Explain
  2010/07/16	Koyanagi	非公開ﾌﾗｸﾞ対応

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using iBridCommLib;
using ViComm;

public partial class Cast_CastBbsPicMainte:System.Web.UI.Page {
	private SessionObjs userObjs;

	protected void Page_Load(object sender,EventArgs e) {
		userObjs = (SessionObjs)Session["objs"];
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.DOCOMO);
		if (!IsPostBack) {
			InitPage();
		}
		if (userObjs.site.GetOne(iBridUtil.GetStringValue(ViewState["SITE_CD"]))) {
			lblSiteNm.Text = userObjs.site.siteNm;
			if (userObjs.site.IsActiveMulitCharFlag(ViewState["SITE_CD"].ToString())) {
				lblSiteNm.Text += string.Concat("-",ViewState["CAST_CHAR_NO"].ToString());
			}
		}
	}

	private void SetViewState() {
		ViewState["SITE_CD"] = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		ViewState["CAST_CHAR_NO"] = iBridUtil.GetStringValue(Request.QueryString["castcharno"]);
		using (SiteManagement oSiteManagement = new SiteManagement()) {
			oSiteManagement.GetOne(ViewState["SITE_CD"].ToString());
			ViewState["ATTR_FLAG"] = oSiteManagement.bbsPicAttrFlag.ToString();
		}
	}

	private void InitPage() {
		SetViewState();
		ClearField();
		pnlMain.Visible = true;
		pnlMainte.Visible = false;
		if (ViewState["ATTR_FLAG"].ToString().Equals(ViCommConst.FLAG_OFF.ToString())) {
			plcHolderUpload.Visible = false;
		} else {
			plcHolderUpload.Visible = true;
			lstCastPicAttrTypeUpload.DataSourceID = "dsCastPicAttrType";
			lstCastPicAttrTypeUpload.DataBind();
			lstCastPicAttrTypeUpload.DataSourceID = string.Empty;
			lstCastPicAttrTypeValueUpload.DataSourceID = "dsCastPicAttrTypeValueUpload";
			lstCastPicAttrTypeValueUpload.DataBind();
			lstCastPicAttrTypeValueUpload.DataSourceID = string.Empty;
		}
		DataBind();
	}

	private void ClearField() {
	}

	protected void btnUpload_Click(object sender,EventArgs e) {
		UploadPicture();
		Server.Transfer(string.Format("CastBbsPicMainte.aspx?sitecd={0}&castcharno={1}",ViewState["SITE_CD"].ToString(),ViewState["CAST_CHAR_NO"].ToString()));
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		Server.Transfer("MainView.aspx");
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		DataBind();
	}

	protected void lnkEditTitle_Command(object sender,CommandEventArgs e) {
		ViewState["PIC_SEQ"] = e.CommandArgument.ToString();
		GetData();
	}

	protected void dsCastPic_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = ViewState["SITE_CD"].ToString();
		e.InputParameters[1] = userObjs.cast.userSeq;
		e.InputParameters[2] = ViewState["CAST_CHAR_NO"].ToString();
		e.InputParameters[3] = ViCommConst.ATTACHED_BBS.ToString();
	}

	private void UploadPicture() {
		string sWebPhisicalDir = string.Empty,sDomain = string.Empty;
		using (Site oSite = new Site()) {
			oSite.GetValue(ViewState["SITE_CD"].ToString(),"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
			oSite.GetValue(ViewState["SITE_CD"].ToString(),"HOST_NM",ref sDomain);
		}

		string sSiteCd = ViewState["SITE_CD"].ToString();
		string sFileNm = string.Empty,sPath = string.Empty,sFullPath = string.Empty;
		decimal dNo = 0;
		
		if (uldCastPic.HasFile) {
			using (CastPic objPic = new CastPic()) {
				dNo = objPic.GetPicNo();
			}
			sFileNm = ViCommConst.PIC_HEADER + iBridUtil.addZero(dNo.ToString(),ViCommConst.OBJECT_NM_LENGTH);
			sPath = sWebPhisicalDir + ViCommConst.PIC_DIRECTRY + "\\" + ViewState["SITE_CD"].ToString() + "\\Operator\\" + userObjs.cast.loginId;
			sFullPath = sPath + "\\" + sFileNm + ViCommConst.PIC_FOODER;

			using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,string.Empty,ViCommConst.FILE_UPLOAD_PASSWORD)) {
				if (!System.IO.File.Exists(sFullPath)) {
					uldCastPic.SaveAs(sFullPath);

					using (Bitmap bmSrc = new Bitmap(sFullPath)) {

						int iWidth = bmSrc.Width / 2;
						int iHight = bmSrc.Height / 2;

						using (Bitmap bmDest = new Bitmap(bmSrc,iWidth,iHight)) {
							string sSmallPic = sWebPhisicalDir +
										ViCommConst.PIC_DIRECTRY + "\\" + ViewState["SITE_CD"].ToString() + "\\Operator\\" + userObjs.cast.loginId + "\\" +
										sFileNm +
										ViCommConst.PIC_FOODER_SMALL;

							bmDest.Save(sSmallPic,ImageFormat.Jpeg);
						}
					}
					string sInputDir = ConfigurationManager.AppSettings["MailParseDir"] + "\\";
					uldCastPic.SaveAs(Path.Combine(sInputDir, sFileNm + ViCommConst.PIC_FOODER));
					ImageHelper.ConvertMobileImage(sSiteCd, sInputDir, sPath, sFileNm);
					
					using (DbSession db = new DbSession()) {
						db.PrepareProcedure("CAST_PIC_UPLOAD");
						db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,ViewState["SITE_CD"].ToString());
						db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.NUMBER,userObjs.cast.userSeq);
						db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,ViewState["CAST_CHAR_NO"].ToString()); // modify UCN
						db.ProcedureInParm("PPIC_SEQ",DbSession.DbType.NUMBER,dNo);
						db.ProcedureInParm("PPIC_TYPE",DbSession.DbType.NUMBER,ViCommConst.ATTACHED_BBS);
						db.ProcedureInParm("PPIC_TITLE",DbSession.DbType.VARCHAR2,txtTitleUpload.Text);
						db.ProcedureInParm("PPIC_DOC",DbSession.DbType.VARCHAR2,txtDocUpload.Text);
						db.ProcedureInParm("POBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_ON);
						if (ViewState["ATTR_FLAG"].ToString().Equals(ViCommConst.FLAG_OFF.ToString())) {
							db.ProcedureInParm("PCAST_PIC_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,ViCommConst.DEFAULT_CAST_PIC_ATTR_TYPE_SEQ.ToString());
							db.ProcedureInParm("PCAST_PIC_ATTR_SEQ",DbSession.DbType.VARCHAR2,ViCommConst.DEFAULT_CAST_PIC_ATTR_SEQ.ToString());
						} else {
							db.ProcedureInParm("PCAST_PIC_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,lstCastPicAttrType.SelectedValue);
							db.ProcedureInParm("PCAST_PIC_ATTR_SEQ",DbSession.DbType.VARCHAR2,lstCastPicAttrTypeValue.SelectedValue);
						}
						db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
						db.ExecuteProcedure();
					}
				}
			}
			ImageHelper.SetCopyRight(ViewState["SITE_CD"].ToString(),sPath,sFileNm + ViCommConst.PIC_FOODER);
		}
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {

			db.PrepareProcedure("CAST_PIC_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,ViewState["SITE_CD"].ToString());
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.NUMBER,decimal.Parse(userObjs.cast.userSeq));
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,ViewState["CAST_CHAR_NO"].ToString()); // modify UCN
			db.ProcedureInParm("PPIC_SEQ",DbSession.DbType.NUMBER,decimal.Parse(ViewState["PIC_SEQ"].ToString()));
			db.ProcedureOutParm("PPIC_TITLE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPIC_DOC",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("POBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("POBJ_NOT_PUBLISH_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PUPLOAD_DATE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPIC_TYPE",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCAST_PIC_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCAST_PIC_ATTR_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtPicTitle.Text = db.GetStringValue("PPIC_TITLE");
				txtPicDoc.Text = db.GetStringValue("PPIC_DOC");
				txtObjNotApproveFlag.Text = db.GetStringValue("POBJ_NOT_APPROVE_FLAG");
				txtObjNotPublishFlag.Text = db.GetStringValue("POBJ_NOT_PUBLISH_FLAG");

				ViewState["ATTR_TYPE"] = db.GetStringValue("PCAST_PIC_ATTR_TYPE_SEQ");
				ViewState["ATTR_TYPE_VALUE"] = db.GetStringValue("PCAST_PIC_ATTR_SEQ");
				if (ViewState["ATTR_FLAG"].ToString().Equals(ViCommConst.FLAG_OFF.ToString())) {
					plcHolder.Visible = false;
				} else {
					plcHolder.Visible = true;
				}
			} else {
				ClearField();
			}
		}

		if (!ViewState["ATTR_TYPE"].ToString().Equals(ViCommConst.DEFAULT_CAST_PIC_ATTR_TYPE_SEQ.ToString())) {
			lstCastPicAttrType.DataSourceID = "dsCastPicAttrType";
			lstCastPicAttrType.DataBind();
			lstCastPicAttrType.DataSourceID = string.Empty;
			lstCastPicAttrType.SelectedValue = ViewState["ATTR_TYPE"].ToString();
			lstCastPicAttrTypeValue.DataSourceID = "dsCastPicAttrTypeValue";
			lstCastPicAttrTypeValue.DataBind();
			lstCastPicAttrTypeValue.DataSourceID = string.Empty;
			lstCastPicAttrTypeValue.SelectedValue = ViewState["ATTR_TYPE_VALUE"].ToString();
		} else {
			lstCastPicAttrType.DataSourceID = "dsCastPicAttrType";
			lstCastPicAttrType.DataBind();
			lstCastPicAttrType.DataSourceID = string.Empty;
			lstCastPicAttrTypeValue.DataSourceID = "dsCastPicAttrTypeValue";
			lstCastPicAttrTypeValue.DataBind();
			lstCastPicAttrTypeValue.DataSourceID = string.Empty;
		}
		pnlMain.Visible = false;
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	private void UpdateData(int pDelFlag) {
		if (pDelFlag == 1) {
			string sFileNm = string.Empty;
			string sWebPhisicalDir = string.Empty;
			using (Site oSite = new Site()) {
				oSite.GetValue(ViewState["SITE_CD"].ToString(),"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
			}

			sFileNm = iBridUtil.addZero(ViewState["PIC_SEQ"].ToString(),ViCommConst.OBJECT_NM_LENGTH);
			
			string sFullPath = sWebPhisicalDir +
								ViCommConst.PIC_DIRECTRY + "\\" + ViewState["SITE_CD"].ToString() + "\\Operator\\" + userObjs.cast.loginId + "\\" +
								sFileNm + ViCommConst.PIC_FOODER;
			using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,string.Empty,ViCommConst.FILE_UPLOAD_PASSWORD)) {
				if (System.IO.File.Exists(sFullPath)) {
					System.IO.File.Delete(sFullPath);
				}
				sFullPath = sWebPhisicalDir +
									ViCommConst.PIC_DIRECTRY + "\\" + ViewState["SITE_CD"].ToString() + "\\Operator\\" + userObjs.cast.loginId + "\\" +
									sFileNm + ViCommConst.PIC_FOODER_SMALL;

				if (System.IO.File.Exists(sFullPath)) {
					System.IO.File.Delete(sFullPath);
				}
			}
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_PIC_MAINTE");
			db.ProcedureInParm("PPIC_SEQ",DbSession.DbType.NUMBER,decimal.Parse(ViewState["PIC_SEQ"].ToString()));
			db.ProcedureInParm("PPIC_TITLE",DbSession.DbType.VARCHAR2,txtPicTitle.Text);
			db.ProcedureInParm("PPIC_DOC",DbSession.DbType.VARCHAR2,txtPicDoc.Text);
			db.ProcedureInParm("POBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER,decimal.Parse(txtObjNotApproveFlag.Text));
			db.ProcedureInParm("POBJ_NOT_PUBLISH_FLAG",DbSession.DbType.NUMBER,decimal.Parse(txtObjNotPublishFlag.Text));
			db.ProcedureInParm("PPIC_TYPE",DbSession.DbType.NUMBER,ViCommConst.ATTACHED_BBS);
			if (ViewState["ATTR_FLAG"].ToString().Equals(ViCommConst.FLAG_OFF.ToString())) {
				db.ProcedureInParm("PCAST_PIC_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,ViewState["ATTR_TYPE"].ToString());
				db.ProcedureInParm("PCAST_PIC_ATTR_SEQ",DbSession.DbType.VARCHAR2,ViewState["ATTR_TYPE_VALUE"].ToString());
			} else {
				db.ProcedureInParm("PCAST_PIC_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,lstCastPicAttrType.SelectedValue);
				db.ProcedureInParm("PCAST_PIC_ATTR_SEQ",DbSession.DbType.VARCHAR2,lstCastPicAttrTypeValue.SelectedValue);
			}
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		InitPage();
	}
	protected void btnMainteCancel_Click(object sender,EventArgs e) {
		pnlMain.Visible = true;
		pnlMainte.Visible = false;
	}

	protected void lstCastPicAttrType_SelectedIndexChanged(object sender,EventArgs e) {
		lstCastPicAttrTypeValue.DataSourceID = "dsCastPicAttrTypeValue";
		lstCastPicAttrTypeValue.DataBind();
		lstCastPicAttrTypeValue.DataSourceID = string.Empty;
	}

	protected void lstCastPicAttrTypeUpload_SelectedIndexChanged(object sender,EventArgs e) {
		lstCastPicAttrTypeValueUpload.DataSourceID = "dsCastPicAttrTypeValueUpload";
		lstCastPicAttrTypeValueUpload.DataBind();
		lstCastPicAttrTypeValueUpload.DataSourceID = string.Empty;
	}

	protected void dsCastPicAttrType_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = ViewState["SITE_CD"].ToString();
	}

	protected void dsCastPicAttrTypeValue_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = ViewState["SITE_CD"].ToString();
		e.InputParameters[1] = lstCastPicAttrType.SelectedValue;
	}
	protected void dsCastPicAttrTypeValueUpload_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = ViewState["SITE_CD"].ToString();
		e.InputParameters[1] = lstCastPicAttrTypeUpload.SelectedValue;
	}
}
