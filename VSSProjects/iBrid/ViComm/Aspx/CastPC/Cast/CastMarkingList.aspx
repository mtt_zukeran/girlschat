<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CastMarkingList.aspx.cs" Inherits="Cast_CastMarkingList" Title="足あと一覧" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="足あと一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[足あと一覧]</legend>
			<table border="1">
				<tr>
					<td width="240" align="center">
						<asp:Label ID="lblSiteNm" runat="server" Font-Bold="True" Font-Underline="False" ForeColor="#FF8000"></asp:Label>
					</td>
					<td align="center" width="30">
						<asp:Button runat="server" ID="btnSeek" Text="検索" OnClick="btnSeek_Click" />
					</td>
					<td align="center">
						<asp:Button runat="server" ID="btnReturn" Text="戻る" OnClick="btnCancel_Click" />
					</td>
				</tr>
			</table>
			<asp:GridView ID="grdMarking" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsMarking" AllowSorting="True" SkinID="GridView">
				<Columns>
					<asp:BoundField DataField="MARKING_DATE" HeaderText="足あと日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
						<ItemStyle HorizontalAlign="Center" />
					</asp:BoundField>
					<asp:TemplateField HeaderText="ハンドル名">
						<ItemStyle HorizontalAlign="Left" Width="200px" />
						<ItemTemplate>
							<asp:HyperLink ID="lnkManView" runat="server" NavigateUrl='<%# string.Format("~/Man/ManView.aspx?sitecd={0}&manuserseq={1}&castcharno={2}&return=../Cast/CastMarkingList.aspx",Eval("SITE_CD"),Eval("USER_SEQ"),Eval("PARTNER_USER_CHAR_NO")) %>'
								Text='<%# Eval("HANDLE_NM") %>'></asp:HyperLink>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:BoundField DataField="BAL_POINT" HeaderText="残Ｐ">
						<ItemStyle HorizontalAlign="Right" Width="40px" />
					</asp:BoundField>
					<asp:BoundField DataField="LAST_LOGIN_DATE" HeaderText="最終ﾛｸﾞｲﾝ日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
						<ItemStyle HorizontalAlign="Center" />
					</asp:BoundField>
					<asp:TemplateField HeaderText="状態">
						<ItemTemplate>
							<asp:Image ID="Image2" runat="server" ImageUrl='<%# GetSysImage(Eval("CHARACTER_ONLINE_STATUS") )%>' />
						</ItemTemplate>
					</asp:TemplateField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
		</fieldset>
	</div>
	<asp:ObjectDataSource ID="dsMarking" runat="server" SelectMethod="GetPageCollection" TypeName="Marking" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelecting="dsMarking_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
			<asp:Parameter Name="pPartnerUserSeq" Type="String" />
			<asp:Parameter Name="pPartnerUserCharNo" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>
