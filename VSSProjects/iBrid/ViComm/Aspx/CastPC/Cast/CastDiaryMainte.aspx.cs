﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: 日記編集
--	Progaram ID		: CastDiaryMainte
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Web.UI.WebControls;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class Cast_CastDiaryMainte:System.Web.UI.Page {
	private SessionObjs userObjs;

	protected void Page_Load(object sender,EventArgs e) {
		userObjs = (SessionObjs)Session["objs"];
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.DOCOMO);
		if (!IsPostBack) {
			InitPage();
		}
	}

	private void SetViewState() {
		ViewState["SITE_CD"] = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		ViewState["USER_SEQ"] = iBridUtil.GetStringValue(Request.QueryString["userseq"]);
		ViewState["CAST_CHAR_NO"] = iBridUtil.GetStringValue(Request.QueryString["castcharno"]);
		ViewState["REPORT_DAY"] = string.Format("{0:yyyy/MM/dd}",DateTime.Now);
		ViewState["HANDLE_NM"] = "";

		string[] SitNum = new string[] { "","[","メイン","]"," : ","" };
		if (userObjs.site.GetOne(iBridUtil.GetStringValue(ViewState["SITE_CD"]))) {
			SitNum[0] = userObjs.site.siteNm;
			if (!ViewState["CAST_CHAR_NO"].ToString().Equals("00")) {
				SitNum[2] = "サブ";
			}

			using (CastCharacter oCastCharacter = new CastCharacter()) {
				DataSet ds = oCastCharacter.GetCharacterList(ViewState["SITE_CD"].ToString(),ViewState["USER_SEQ"].ToString());
				DataRow dr;
				for (int i = 0;i < ds.Tables[0].Rows.Count;i++) {
					dr = ds.Tables[0].Rows[i];
					if (dr["USER_CHAR_NO"].ToString().Equals(ViewState["CAST_CHAR_NO"])) {
						ViewState["HANDLE_NM"] = dr["HANDLE_NM"].ToString();
					}
				}
			}
			SitNum[5] = ViewState["HANDLE_NM"].ToString();
		}
		lblSiteNm.Text = string.Concat(SitNum);
	}

	private void InitPage() {
		SetViewState();
		ClearField();
		lblDay.Text = ViewState["REPORT_DAY"].ToString();
		SysPrograms.SetupFromToDay(lstFromYYYY,lstFromMM,lstFromDD,lstToYYYY,lstToMM,lstToDD,true);
		lstFromYYYY.SelectedIndex = 0;
		lstToYYYY.SelectedIndex = 0;
		lstFromMM.SelectedValue = DateTime.Now.ToString("MM");
		lstToMM.SelectedValue = DateTime.Now.ToString("MM");
		lstFromDD.SelectedValue = string.Format("{0:dd}",DateTime.Now.AddDays(1 - DateTime.Now.Day));
		lstToDD.SelectedValue = DateTime.Now.ToString("dd");
		DataBind();
		GetData();
		GetList();
	}

	private void ClearField() {

	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		Server.Transfer("MainView.aspx");
	}

	protected void objCalender_SelectionChanged(object sender,EventArgs e) {
		ViewState["REPORT_DAY"] = string.Format("{0:yyyy/MM/dd}",objCalender.SelectedDate);
		lblDay.Text = ViewState["REPORT_DAY"].ToString();
		GetData();
	}

	protected void objCalender_DayRender(object sender,DayRenderEventArgs e) {
		using (CastDiary oDiary = new CastDiary()) {
			if (oDiary.GetDiaryInfo(ViewState["SITE_CD"].ToString(),userObjs.cast.userSeq,ViewState["CAST_CHAR_NO"].ToString(),string.Format("{0:yyyy/MM/dd}",e.Day.Date))) {
				e.Cell.BackColor = Color.Lavender;
			}
		}
	}
	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnDiarySeek_Click(object sender,EventArgs e) {
		if (IsValid) {
			GetList();
		}
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_DIARY_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,ViewState["SITE_CD"].ToString());
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,userObjs.cast.userSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,ViewState["CAST_CHAR_NO"].ToString()); // modify UCN
			db.ProcedureInParm("PREPORT_DAY",DbSession.DbType.VARCHAR2,ViewState["REPORT_DAY"].ToString());
			db.ProcedureOutParm("PDIARY_TITLE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PHTML_DOC1",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PHTML_DOC2",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PHTML_DOC3",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PHTML_DOC4",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPIC_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");
			ViewState["PIC_SEQ"] = db.GetStringValue("PPIC_SEQ");

			txtTitle.Text = db.GetStringValue("PDIARY_TITLE");
			txtHtmlDoc.Text = db.GetStringValue("PHTML_DOC1");
		}
		using (CastDiary oCastDiary = new CastDiary()) {
			bool bExist = oCastDiary.GetDiaryInfo(iBridUtil.GetStringValue(ViewState["SITE_CD"].ToString()),
													userObjs.cast.userSeq,
													iBridUtil.GetStringValue(ViewState["CAST_CHAR_NO"].ToString()),
													iBridUtil.GetStringValue(ViewState["REPORT_DAY"].ToString()));

			string sObjPhotoImgPath = "";
			if (bExist) {
				sObjPhotoImgPath = oCastDiary.objPhotoImgPath;
			}
			//ｷｬｽﾄPCの日記画像は未UPの場合、nopic.gifを表示させる
			if (iBridUtil.GetStringValue(ViewState["PIC_SEQ"]).Equals("") || (bExist == false)) {
				imgPic.ImageUrl = string.Format("../image/{0}/nopic.gif",ViewState["SITE_CD"].ToString());
			} else {
				imgPic.ImageUrl = string.Format("../{0}",sObjPhotoImgPath);
			}
		}
	}

	private void UpdateData(int pDelModeFlag) {
		string sTitle = Mobile.EmojiToCommTag(ViCommConst.DOCOMO,txtTitle.Text);
		string sDoc = Mobile.EmojiToCommTag(ViCommConst.DOCOMO,txtHtmlDoc.Text);

		string sPicSeq = "";

		if (pDelModeFlag == 0) {
			if (!uldPic.HasFile) {
				sPicSeq = ViewState["PIC_SEQ"].ToString();
			} else {
				if (!ViewState["PIC_SEQ"].ToString().Equals("")) {
					DeletePicture(ViewState["PIC_SEQ"].ToString());
				}
			}
		} else {
			if (!ViewState["PIC_SEQ"].ToString().Equals("")) {
				DeletePicture(ViewState["PIC_SEQ"].ToString());
			}
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_DIARY_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,ViewState["SITE_CD"].ToString());
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.NUMBER,decimal.Parse(userObjs.cast.userSeq));
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,ViewState["CAST_CHAR_NO"].ToString()); // modify UCN
			db.ProcedureInParm("PREPORT_DAY",DbSession.DbType.VARCHAR2,ViewState["REPORT_DAY"].ToString());
			db.ProcedureInParm("PDIARY_TITLE",DbSession.DbType.VARCHAR2,sTitle);
			db.ProcedureInParm("PHTML_DOC1",DbSession.DbType.VARCHAR2,sDoc);
			db.ProcedureInParm("PHTML_DOC2",DbSession.DbType.VARCHAR2,"");
			db.ProcedureInParm("PHTML_DOC3",DbSession.DbType.VARCHAR2,"");
			db.ProcedureInParm("PHTML_DOC4",DbSession.DbType.VARCHAR2,"");
			db.ProcedureInParm("PPIC_SEQ",DbSession.DbType.VARCHAR2,sPicSeq);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelModeFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}

		if (pDelModeFlag == 0) {
			UploadPicture();
		}

		GetData();
		GetList();
	}

	private void UploadPicture() {
		string sWebPhisicalDir = "";
		string sDomain = "";
		using (Site oSite = new Site()) {
			oSite.GetValue(ViewState["SITE_CD"].ToString(),"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
			oSite.GetValue(ViewState["SITE_CD"].ToString(),"HOST_NM",ref sDomain);
		}


		string sFileNm = "",sPath = "",sFullPath = "";
		decimal dNo = 0;

		if (uldPic.HasFile) {
			using (CastPic objPic = new CastPic()) {
				dNo = objPic.GetPicNo();
			}
			sFileNm = ViCommConst.PIC_HEADER + iBridUtil.addZero(dNo.ToString(),ViCommConst.OBJECT_NM_LENGTH);
			sPath = sWebPhisicalDir + ViCommConst.PIC_DIRECTRY + "\\" + ViewState["SITE_CD"].ToString() + "\\Operator\\" + userObjs.cast.loginId;
			sFullPath = sPath + "\\" + sFileNm + ViCommConst.PIC_FOODER;

			using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
				if (!System.IO.File.Exists(sFullPath)) {
					uldPic.SaveAs(sFullPath);

					using (Bitmap bmSrc = new Bitmap(sFullPath)) {

						int iWidth = bmSrc.Width / 2;
						int iHight = bmSrc.Height / 2;

						using (Bitmap bmDest = new Bitmap(bmSrc,iWidth,iHight)) {
							string sSmallPic = sWebPhisicalDir +
										ViCommConst.PIC_DIRECTRY + "\\" + ViewState["SITE_CD"].ToString() + "\\Operator\\" + userObjs.cast.loginId + "\\" +
										sFileNm +
										ViCommConst.PIC_FOODER_SMALL;

							bmDest.Save(sSmallPic,ImageFormat.Jpeg);
						}
					}
					string sInputDir = ConfigurationManager.AppSettings["MailParseDir"] + "\\";
					uldPic.SaveAs(Path.Combine(sInputDir, sFileNm + ViCommConst.PIC_FOODER));
					ImageHelper.ConvertMobileImage(ViewState["SITE_CD"].ToString(), sInputDir, sPath, sFileNm);

					using (DbSession db = new DbSession()) {
						db.PrepareProcedure("CAST_DIARY_PIC_UPLOAD");
						db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,ViewState["SITE_CD"].ToString());
						db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,userObjs.cast.userSeq);
						db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,ViewState["CAST_CHAR_NO"].ToString()); // modify UCN
						db.ProcedureInParm("PREPORT_DAY",DbSession.DbType.VARCHAR2,ViewState["REPORT_DAY"]);
						db.ProcedureInParm("PPIC_SEQ",DbSession.DbType.NUMBER,dNo);
						db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
						db.ExecuteProcedure();
					}
				}
			}
			ImageHelper.SetCopyRight(ViewState["SITE_CD"].ToString(),sPath,sFileNm + ViCommConst.PIC_FOODER);
		}
	}

	private void DeletePicture(string pFileSeq) {
		string sWebPhisicalDir = "";
		using (Site oSite = new Site()) {
			oSite.GetValue(ViewState["SITE_CD"].ToString(),"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
		}

		string sFileNm = iBridUtil.addZero(pFileSeq,ViCommConst.OBJECT_NM_LENGTH);

		string sFullPath = sWebPhisicalDir +
							ViCommConst.PIC_DIRECTRY + "\\" + ViewState["SITE_CD"].ToString() + "\\Operator\\" + userObjs.cast.loginId + "\\" +
							sFileNm + ViCommConst.PIC_FOODER;

		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
			if (File.Exists(sFullPath)) {
				File.Delete(sFullPath);
			}


			sFullPath = sWebPhisicalDir +
								ViCommConst.PIC_DIRECTRY + "\\" + ViewState["SITE_CD"].ToString() + "\\Operator\\" + userObjs.cast.loginId + "\\" +
								sFileNm + ViCommConst.PIC_FOODER_SMALL;

			if (File.Exists(sFullPath)) {
				File.Delete(sFullPath);
			}
		}
	}

	private void GetList() {
		ClearField();
		grdDiary.DataSourceID = "dsDiary";
		grdDiary.DataBind();
	}

	public string GetHandlNm() {
		return ViewState["HANDLE_NM"].ToString();
	}

	public string CnvHTML(object pStr) {
		return pStr.ToString().Replace(System.Environment.NewLine,"<br>");
	}
	protected void dsDiary_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = ViewState["SITE_CD"].ToString();
		e.InputParameters[1] = userObjs.cast.userSeq;
		e.InputParameters[2] = ViewState["CAST_CHAR_NO"].ToString();
		e.InputParameters[3] = lstFromYYYY.SelectedValue + "/" + lstFromMM.SelectedValue + "/" + lstFromDD.SelectedValue;
		e.InputParameters[4] = lstToYYYY.SelectedValue + "/" + lstToMM.SelectedValue + "/" + lstToDD.SelectedValue;
	}

	protected void vdcHtmlDoc_ServerValidate(object source,ServerValidateEventArgs args) {
		if (!this.IsValid) {
			return;
		}

		args.IsValid = this.txtHtmlDoc.Text.Length <= this.txtHtmlDoc.MaxLength;
	}
}
