<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReuqestLiveTimer.aspx.cs" Inherits="Cast_ReuqestLiveTimer"
	Title="ライブタイマー設定" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="ライブタイマー"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<asp:Panel runat="server" ID="pnlDtl">
				<fieldset>
					<legend>[配信時間設定]</legend>
					<table border="0" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle">
								配信分数
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtBroadcastMin" runat="server" Width="20px" MaxLength="3"></asp:TextBox>分
								<asp:RequiredFieldValidator ID="vdrBroadcastMin" runat="server" ErrorMessage="配信分数を入力して下さい。" ControlToValidate="txtBroadcastMin" ValidationGroup="check"></asp:RequiredFieldValidator>
								<asp:RegularExpressionValidator ID="vdeBroadcastMin" runat="server" ErrorMessage="配信分数は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtBroadcastMin" ValidationGroup="check"></asp:RegularExpressionValidator>
							</td>
						</tr>
					</table>
					<asp:Button ID="btnOnlineMonitor" runat="server" Text="ライブ配信開始" Width="120px" OnClick="btnOnlineMonitor_Click" OnClientClick='return confirm("ライブ配信を開始しますか？");' ValidationGroup="check" /><br />
				</fieldset>
			</asp:Panel>
		</asp:Panel>
	</div>
</asp:Content>
