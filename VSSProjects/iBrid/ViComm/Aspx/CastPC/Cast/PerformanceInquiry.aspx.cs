﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: 稼動集計
--	Progaram ID		: PerformanceInquiry
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using iBridCommLib;
using ViComm;

public partial class Cast_PerformanceInquiry:System.Web.UI.Page {
	private SessionObjs userObjs;

	private int tvPrvMin;
	private int tvPubMin;
	private int voicePrvMin;
	private int voicePubMin;
	private int viewTalkMin;
	private int viewBroadcastMin;
	private int wiretapMin;
	private int totalMin;
	private int movieMin;
	private int mailCount;

	protected void Page_Load(object sender,EventArgs e) {
		userObjs = (SessionObjs)Session["objs"];
		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
		grdPerformace.DataSourceID = "";
		ClearField();
		DataBind();
		if (!IsPostBack) {
			SysPrograms.SetupFromToDayTime(lstFromYYYY,lstFromMM,lstFromDD,lstFromHH,lstToYYYY,lstToMM,lstToDD,lstToHH,true);
			lstFromYYYY.SelectedIndex = 0;
			lstToYYYY.SelectedIndex = 0;
			lstFromMM.SelectedValue = DateTime.Now.ToString("MM");
			lstToMM.SelectedValue = DateTime.Now.ToString("MM");
			lstFromDD.SelectedValue = DateTime.Now.ToString("dd");
			lstToDD.SelectedValue = DateTime.Now.ToString("dd");
			lstFromHH.SelectedIndex = 0;
			lstToHH.SelectedIndex = 0;
		}
	}

	private void ClearField() {
		tvPrvMin = 0;
		tvPubMin = 0;
		voicePrvMin = 0;
		voicePubMin = 0;
		viewTalkMin = 0;
		viewBroadcastMin = 0;
		wiretapMin = 0;
		movieMin = 0;
		mailCount = 0;
		totalMin = 0;
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}


	protected void grdPerformace_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			tvPrvMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PRV_TV_TALK_MIN"));
			tvPubMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PUB_TV_TALK_MIN"));
			voicePrvMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PRV_VOICE_TALK_MIN"));
			voicePubMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PUB_VOICE_TALK_MIN"));
			viewTalkMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"VIEW_TALK_MIN"));
			viewBroadcastMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"VIEW_BROADCAST_MIN"));
			wiretapMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"WIRETAP_MIN"));
			totalMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"TALK_MIN"));
			movieMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"MOVIE_MIN"));
			mailCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"USER_MAIL_COUNT"));
		} else if (e.Row.RowType == DataControlRowType.Footer) {
			e.Row.Cells[1].Text = tvPrvMin.ToString() + "分";
			e.Row.Cells[2].Text = tvPubMin.ToString() + "分";
			e.Row.Cells[3].Text = voicePrvMin.ToString() + "分";
			e.Row.Cells[4].Text = voicePubMin.ToString() + "分";
			e.Row.Cells[5].Text = viewTalkMin.ToString() + "分";
			e.Row.Cells[6].Text = wiretapMin.ToString() + "分";
			e.Row.Cells[7].Text = viewBroadcastMin.ToString() + "分";
			e.Row.Cells[8].Text = totalMin.ToString() + "分";
			e.Row.Cells[9].Text = mailCount.ToString() + "件";
		}
	}

	private void GetList() {
		ClearField();
		grdPerformace.DataSourceID = "dsTimeOperation";
		DataBind();
	}
}
