<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CastRefuseList.aspx.cs" Inherits="Cast_RefuseList" Title="Ūź" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="Ūź"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<fieldset class="fieldset">
		<legend>[Ūź]</legend>
		<table border="1">
			<tr>
				<td width="240" align="center">
					<asp:Label ID="lblSiteNm" runat="server" Font-Bold="True" Font-Underline="False" ForeColor="#FF8000"></asp:Label>
				</td>
				<td align="center" width="30">
					<asp:Button runat="server" ID="btnSeek" Text="õ" OnClick="btnSeek_Click" />
				</td>
				<td align="center">
					<asp:Button runat="server" ID="btnReturn" Text="ßé" OnClick="btnCancel_Click" />
				</td>
			</tr>
		</table>
		<asp:GridView ID="grdRefuse" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsRefuse" AllowSorting="True" SkinID="GridView">
			<Columns>
				<asp:BoundField DataField="REFUSE_DATE" HeaderText="Ūo^ś" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
					<ItemStyle HorizontalAlign="Center" />
					<HeaderStyle HorizontalAlign="Center" />
				</asp:BoundField>
				<asp:TemplateField HeaderText="nh¼">
					<ItemStyle HorizontalAlign="Left" Width="200px" />
					<ItemTemplate>
						<asp:HyperLink ID="lnkManView" runat="server" NavigateUrl='<%# string.Format("~/Man/ManView.aspx?sitecd={0}&manuserseq={1}&castcharno={2}&return=../Cast/CastRefuseList.aspx",Eval("SITE_CD"),Eval("PARTNER_USER_SEQ"),Eval("USER_CHAR_NO")) %>'
							Text='<%# Eval("HANDLE_NM") %>'></asp:HyperLink>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:BoundField DataField="LAST_TALK_DATE" HeaderText="ÅIļbś" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
					<ItemStyle HorizontalAlign="Center" />
				</asp:BoundField>
				<asp:BoundField DataField="TALK_COUNT" HeaderText="ļb">
					<ItemStyle HorizontalAlign="Right" Width="48px" />
				</asp:BoundField>
				<asp:BoundField DataField="LAST_LOGIN_DATE" HeaderText="ÅIŪøŽ²Żś" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
					<ItemStyle HorizontalAlign="Center" />
				</asp:BoundField>
				<asp:TemplateField HeaderText="óŌ">
					<ItemTemplate>
						<asp:Image ID="Image2" runat="server" ImageUrl='<%# GetSysImage(Eval("CHARACTER_ONLINE_STATUS") )%>' />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
			<PagerSettings Mode="NumericFirstLast" />
		</asp:GridView>
	</fieldset>
	<asp:ObjectDataSource ID="dsRefuse" runat="server" SelectMethod="GetPageCollection" TypeName="Refuse" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelecting="dsRefuse_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>
