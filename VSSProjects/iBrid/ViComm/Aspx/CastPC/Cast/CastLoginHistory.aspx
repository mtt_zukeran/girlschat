<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CastLoginHistory.aspx.cs" Inherits="Cast_CastLoginHistory"
	Title="ログイン履歴" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="ログイン履歴"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<fieldset class="fieldset">
		<legend>[ログイン履歴]</legend>
		<table border="1">
			<tr>
				<td width="240" align="center">
					<asp:Label ID="lblSiteNm" runat="server" Font-Bold="True" Font-Underline="False" ForeColor="#FF8000"></asp:Label>
				</td>
				<td align="center" width="30">
					<asp:Button runat="server" ID="btnSeek" Text="検索" OnClick="btnSeek_Click" />
				</td>
				<td align="center">
					<asp:Button runat="server" ID="btnReturn" Text="戻る" OnClick="btnCancel_Click" />
				</td>
			</tr>
		</table>
		<asp:GridView ID="grdLogin" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsLoginCharacter" AllowSorting="True" SkinID="GridView"
			PageSize="12">
			<Columns>
				<asp:BoundField DataField="START_DATE" HeaderText="開始" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
					<ItemStyle HorizontalAlign="Center" />
				</asp:BoundField>
				<asp:BoundField DataField="LOGOFF_DATE" HeaderText="終了" DataFormatString="{0:HH:mm}" HtmlEncode="False">
					<ItemStyle HorizontalAlign="Center" />
				</asp:BoundField>
				<asp:BoundField DataField="SITE_CD" HeaderText="ｻｲﾄ">
					<ItemStyle HorizontalAlign="Center" />
				</asp:BoundField>
				<asp:TemplateField HeaderText="TV<BR/>分/回">
					<ItemTemplate>
						<asp:Label ID="Label7" runat="server" Text='<%#string.Format("{0}({1,2})",Eval("PRV_TV_TALK_MIN"),Eval("PRV_TV_TALK_COUNT")) %>' Width="50px"></asp:Label>
					</ItemTemplate>
					<ItemStyle HorizontalAlign="Right" />
					<HeaderStyle HorizontalAlign="Center" />
				</asp:TemplateField>
				<asp:TemplateField HeaderText="TVﾁｬｯﾄ<BR/>分/回">
					<ItemTemplate>
						<asp:Label ID="Label8" runat="server" Text='<%#string.Format("{0}({1,2})",Eval("PUB_TV_TALK_MIN"),Eval("PUB_TV_TALK_COUNT")) %>' Width="50px"></asp:Label>
					</ItemTemplate>
					<ItemStyle HorizontalAlign="Right" />
					<HeaderStyle HorizontalAlign="Center" />
				</asp:TemplateField>
				<asp:TemplateField HeaderText="音声<BR/>分/回">
					<ItemTemplate>
						<asp:Label ID="Label8" runat="server" Text='<%#string.Format("{0}({1,2})",Eval("PRV_VOICE_TALK_MIN"),Eval("PRV_VOICE_TALK_COUNT")) %>' Width="50px"></asp:Label>
					</ItemTemplate>
					<ItemStyle HorizontalAlign="Right" />
					<HeaderStyle HorizontalAlign="Center" />
				</asp:TemplateField>
				<asp:TemplateField HeaderText="音声ﾁｬｯﾄ<BR/>分/回">
					<ItemTemplate>
						<asp:Label ID="Label8" runat="server" Text='<%#string.Format("{0}({1,2})",Eval("PUB_VOICE_TALK_MIN"),Eval("PUB_VOICE_TALK_COUNT")) %>' Width="50px"></asp:Label>
					</ItemTemplate>
					<ItemStyle HorizontalAlign="Right" />
					<HeaderStyle HorizontalAlign="Center" />
				</asp:TemplateField>
				<asp:TemplateField HeaderText="会話ﾓﾆﾀ<BR/>分/回">
					<ItemTemplate>
						<asp:Label ID="Label8" runat="server" Text='<%#string.Format("{0}({1,2})",Eval("VIEW_TALK_MIN"),Eval("VIEW_TALK_COUNT")) %>' Width="50px"></asp:Label>
					</ItemTemplate>
					<ItemStyle HorizontalAlign="Right" />
					<HeaderStyle HorizontalAlign="Center" />
				</asp:TemplateField>
				<asp:TemplateField HeaderText="音声ﾓﾆﾀ<BR/>分/回">
					<ItemTemplate>
						<asp:Label ID="Label8" runat="server" Text='<%#string.Format("{0}({1,2})",Eval("WIRETAP_MIN"),Eval("WIRETAP_COUNT")) %>' Width="50px"></asp:Label>
					</ItemTemplate>
					<ItemStyle HorizontalAlign="Right" />
					<HeaderStyle HorizontalAlign="Center" />
				</asp:TemplateField>
				<asp:TemplateField HeaderText="部屋ﾓﾆﾀ<BR/>分/回">
					<ItemTemplate>
						<asp:Label ID="Label8" runat="server" Text='<%#string.Format("{0}({1,2})",Eval("VIEW_BROADCAST_MIN"),Eval("VIEW_BROADCAST_COUNT")) %>' Width="50px"></asp:Label>
					</ItemTemplate>
					<ItemStyle HorizontalAlign="Right" />
					<HeaderStyle HorizontalAlign="Center" />
				</asp:TemplateField>
			</Columns>
			<PagerSettings Mode="NumericFirstLast" />
		</asp:GridView>
	</fieldset>
	<asp:ObjectDataSource ID="dsLoginCharacter" runat="server" SelectMethod="GetPageCollection" TypeName="LoginCharacter" SelectCountMethod="GetPageCount"
		EnablePaging="True" OnSelecting="dsLoginCharacter_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCD" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
			<asp:Parameter Name="pReportDayFrom" Type="String" />
			<asp:Parameter Name="pReportDayTo" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>
