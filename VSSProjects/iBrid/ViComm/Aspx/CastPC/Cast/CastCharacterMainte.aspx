<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CastCharacterMainte.aspx.cs" Inherits="Cast_CastCharacterMainte"
	Title="キャラクター設定" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="キャラクター設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<asp:Panel runat="server" ID="pnlDtl">
				<fieldset class="fieldset">
					<legend>[キャラクター設定]</legend>
					<table border="0" style="width: 640px" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle">
								サイト
							</td>
							<td class="tdDataStyle">
								<asp:Label ID="lblSiteNm" runat="server" Text="">
								</asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								ハンドルネーム
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtHandleNm" runat="server" MaxLength="20" Width="200px"></asp:TextBox>
								<asp:RequiredFieldValidator ID="vdrHandleNm" runat="server" ErrorMessage="ハンドルネームを入力して下さい。" ControlToValidate="txtHandleNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								<%= DisplayWordUtil.ReplaceWordCommentList("一覧用コメント") %>
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtCommentList" runat="server" MaxLength="60" Width="497px" TextMode="MultiLine" Rows="3" Columns="60"></asp:TextBox>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								詳細用コメント
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtCommentDetail" runat="server" Columns="60" MaxLength="200" Rows="3" TextMode="MultiLine" Width="497px"></asp:TextBox>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								待機コメント
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtWaitingComment" runat="server" Columns="60" MaxLength="200" Rows="3" TextMode="MultiLine" Width="497px"></asp:TextBox>
							</td>
						</tr>
						<asp:PlaceHolder ID="plcBirthDay" runat="server">
							<tr>
								<td class="tdHeaderStyle">
									生年月日
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstBirthYear" runat="server">
									</asp:DropDownList>
									<asp:Label ID="lblBirthYear" runat="server">年</asp:Label>
									<asp:DropDownList ID="lstBirthMonth" runat="server">
									</asp:DropDownList>
									<asp:Label ID="lblBirthMonth" runat="server">月</asp:Label>
									<asp:DropDownList ID="lstBirthDay" runat="server">
									</asp:DropDownList>
									<asp:Label ID="lblBirthDay" runat="server">日</asp:Label>
									<asp:TextBox ID="txtBirthDay" runat="server" MaxLength="10" Width="100px" Visible="false"></asp:TextBox>
									<asp:CustomValidator ID="vdcBirthDay" runat="server" ErrorMessage="正しい日付で18歳以上の生年月日を入力してください。" OnServerValidate="vdcBirthday_ServerValidate" ValidationGroup="Detail"
										Display="Dynamic">*</asp:CustomValidator>
								</td>
							</tr>
						</asp:PlaceHolder>
						<tr>
							<td class="tdHeaderStyle">
								ログイン通知メール<br />
								(気に入られている)
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstLoginMailTemplateNo" runat="server" Width="206px" DataSourceID="dsLoginMail" DataTextField="TEMPLATE_NM" DataValueField="MAIL_TEMPLATE_NO">
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								ログイン通知メール<br />
								(気にいっている)
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstLoginMailTemplateNo2" runat="server" Width="206px" DataSourceID="dsLoginMail" DataTextField="TEMPLATE_NM" DataValueField="MAIL_TEMPLATE_NO">
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle2">
								Thank Youメール<br />
								標準テンプレート
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstLogoutMailTemplateNo" runat="server" Width="206px" DataSourceID="dsLogoutMail" DataTextField="TEMPLATE_NM" DataValueField="MAIL_TEMPLATE_NO">
								</asp:DropDownList>
							</td>
						</tr>
					</table>
					<asp:PlaceHolder ID="plcOfflineMailRx" runat="server">
						<asp:Label ID="Label2" runat="server" Text="「ｵﾌﾗｲﾝ時ﾒｰﾙ受信設定」"></asp:Label>
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									男性メール受信区分
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstManMailRxType" runat="server" Width="206px" DataSourceID="dsMailRxType" DataTextField="CODE_NM" DataValueField="CODE">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									お知らせメール受信区分
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstInfoMailRxType" runat="server" Width="206px" DataSourceID="dsInfoMailRxType" DataTextField="CODE_NM" DataValueField="CODE">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									携帯ﾒｰﾙ受信時間帯
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstMobileMailRxStartTime" runat="server" Width="60px" DataSourceID="dsTime" DataTextField="CODE_NM" DataValueField="CODE_NM">
									</asp:DropDownList>〜
									<asp:DropDownList ID="lstMobileMailRxEndTime" runat="server" Width="60px" DataSourceID="dsTime" DataTextField="CODE_NM" DataValueField="CODE_NM">
									</asp:DropDownList>
								</td>
							</tr>
						</table>
					</asp:PlaceHolder>
					<asp:PlaceHolder ID="plcOkList" runat="server">「<%= DisplayWordUtil.ReplaceOkList("ＯＫリスト")%>」
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td>
									<asp:CheckBox ID="chkOKFlag0" runat="server" />
								</td>
								<td>
									<asp:CheckBox ID="chkOKFlag1" runat="server" />
								</td>
								<td>
									<asp:CheckBox ID="chkOKFlag2" runat="server" />
								</td>
								<td>
									<asp:CheckBox ID="chkOKFlag3" runat="server" />
								</td>
								<td>
									<asp:CheckBox ID="chkOKFlag4" runat="server" />
								</td>
							</tr>
							<tr>
								<td>
									<asp:CheckBox ID="chkOKFlag5" runat="server" />
								</td>
								<td>
									<asp:CheckBox ID="chkOKFlag6" runat="server" />
								</td>
								<td>
									<asp:CheckBox ID="chkOKFlag7" runat="server" />
								</td>
								<td>
									<asp:CheckBox ID="chkOKFlag8" runat="server" />
								</td>
								<td>
									<asp:CheckBox ID="chkOKFlag9" runat="server" />
								</td>
							</tr>
							<tr>
								<td>
									<asp:CheckBox ID="chkOKFlag10" runat="server" />
								</td>
								<td>
									<asp:CheckBox ID="chkOKFlag11" runat="server" />
								</td>
								<td>
									<asp:CheckBox ID="chkOKFlag12" runat="server" />
								</td>
								<td>
									<asp:CheckBox ID="chkOKFlag13" runat="server" />
								</td>
								<td>
									<asp:CheckBox ID="chkOKFlag14" runat="server" />
								</td>
							</tr>
							<tr>
								<td>
									<asp:CheckBox ID="chkOKFlag15" runat="server" />
								</td>
								<td>
									<asp:CheckBox ID="chkOKFlag16" runat="server" />
								</td>
								<td>
									<asp:CheckBox ID="chkOKFlag17" runat="server" />
								</td>
								<td>
									<asp:CheckBox ID="chkOKFlag18" runat="server" />
								</td>
								<td>
									<asp:CheckBox ID="chkOKFlag19" runat="server" />
								</td>
							</tr>
							<tr>
								<td>
									<asp:CheckBox ID="chkOKFlag20" runat="server" />
								</td>
								<td>
									<asp:CheckBox ID="chkOKFlag21" runat="server" />
								</td>
								<td>
									<asp:CheckBox ID="chkOKFlag22" runat="server" />
								</td>
								<td>
									<asp:CheckBox ID="chkOKFlag23" runat="server" />
								</td>
								<td>
									<asp:CheckBox ID="chkOKFlag24" runat="server" />
								</td>
							</tr>
							<tr>
								<td>
									<asp:CheckBox ID="chkOKFlag25" runat="server" />
								</td>
								<td>
									<asp:CheckBox ID="chkOKFlag26" runat="server" />
								</td>
								<td>
									<asp:CheckBox ID="chkOKFlag27" runat="server" />
								</td>
								<td>
									<asp:CheckBox ID="chkOKFlag28" runat="server" />
								</td>
								<td>
									<asp:CheckBox ID="chkOKFlag29" runat="server" />
								</td>
							</tr>
							<tr>
								<td>
									<asp:CheckBox ID="chkOKFlag30" runat="server" />
								</td>
								<td>
									<asp:CheckBox ID="chkOKFlag31" runat="server" />
								</td>
								<td>
									<asp:CheckBox ID="chkOKFlag32" runat="server" />
								</td>
								<td>
									<asp:CheckBox ID="chkOKFlag33" runat="server" />
								</td>
								<td>
									<asp:CheckBox ID="chkOKFlag34" runat="server" />
								</td>
							</tr>
							<tr>
								<td>
									<asp:CheckBox ID="chkOKFlag35" runat="server" />
								</td>
								<td>
									<asp:CheckBox ID="chkOKFlag36" runat="server" />
								</td>
								<td>
									<asp:CheckBox ID="chkOKFlag37" runat="server" />
								</td>
								<td>
									<asp:CheckBox ID="chkOKFlag38" runat="server" />
								</td>
								<td>
									<asp:CheckBox ID="chkOKFlag39" runat="server" />
								</td>
							</tr>
						</table>
					</asp:PlaceHolder>
					｢プロフィール｣<br />
					<asp:PlaceHolder ID="plcHolder" runat="server">
						<asp:Table ID="Table1" runat="server" CssClass="tableStyle" Width="640px">
							<asp:TableRow ID="rowAttr0" runat="server">
								<asp:TableCell ID="celCastAttrNm0" runat="server" CssClass="tdHeaderStyle">
									<asp:Label ID="lblCastAttrNm0" runat="server" Text="Label"></asp:Label>
								</asp:TableCell>
								<asp:TableCell ID="celCastAttrSeq0" runat="server" CssClass="tdDataStyle">
									<asp:DropDownList ID="lstCastAttrSeq0" runat="server" Width="206px">
									</asp:DropDownList>
									<asp:TextBox ID="txtAttrInputValue0" runat="server" Width="200px" MaxLength="40"></asp:TextBox>
								</asp:TableCell>
							</asp:TableRow>
							<asp:TableRow ID="rowAttr1" runat="server">
								<asp:TableCell ID="celCastAttrNm1" runat="server" CssClass="tdHeaderStyle">
									<asp:Label ID="lblCastAttrNm1" runat="server" Text="Label"></asp:Label>
								</asp:TableCell>
								<asp:TableCell ID="celCastAttrSeq1" runat="server" CssClass="tdDataStyle">
									<asp:DropDownList ID="lstCastAttrSeq1" runat="server" Width="206px">
									</asp:DropDownList>
									<asp:TextBox ID="txtAttrInputValue1" runat="server" Width="200px" MaxLength="40"></asp:TextBox>
								</asp:TableCell>
							</asp:TableRow>
							<asp:TableRow ID="rowAttr2" runat="server">
								<asp:TableCell ID="celCastAttrNm2" runat="server" CssClass="tdHeaderStyle">
									<asp:Label ID="lblCastAttrNm2" runat="server" Text="Label"></asp:Label>
								</asp:TableCell>
								<asp:TableCell ID="celCastAttrSeq2" runat="server" CssClass="tdDataStyle">
									<asp:DropDownList ID="lstCastAttrSeq2" runat="server" Width="206px">
									</asp:DropDownList>
									<asp:TextBox ID="txtAttrInputValue2" runat="server" Width="200px" MaxLength="40"></asp:TextBox>
								</asp:TableCell>
							</asp:TableRow>
							<asp:TableRow ID="rowAttr3" runat="server">
								<asp:TableCell ID="celCastAttrNm3" runat="server" CssClass="tdHeaderStyle">
									<asp:Label ID="lblCastAttrNm3" runat="server" Text="Label"></asp:Label>
								</asp:TableCell>
								<asp:TableCell ID="celCastAttrSeq3" runat="server" CssClass="tdDataStyle">
									<asp:DropDownList ID="lstCastAttrSeq3" runat="server" Width="206px">
									</asp:DropDownList>
									<asp:TextBox ID="txtAttrInputValue3" runat="server" Width="200px" MaxLength="40"></asp:TextBox>
								</asp:TableCell>
							</asp:TableRow>
							<asp:TableRow ID="rowAttr4" runat="server">
								<asp:TableCell ID="celCastAttrNm4" runat="server" CssClass="tdHeaderStyle">
									<asp:Label ID="lblCastAttrNm4" runat="server" Text="Label"></asp:Label>
								</asp:TableCell>
								<asp:TableCell ID="celCastAttrSeq4" runat="server" CssClass="tdDataStyle">
									<asp:DropDownList ID="lstCastAttrSeq4" runat="server" Width="206px">
									</asp:DropDownList>
									<asp:TextBox ID="txtAttrInputValue4" runat="server" Width="200px" MaxLength="40"></asp:TextBox>
								</asp:TableCell>
							</asp:TableRow>
							<asp:TableRow ID="rowAttr5" runat="server">
								<asp:TableCell ID="celCastAttrNm5" runat="server" CssClass="tdHeaderStyle">
									<asp:Label ID="lblCastAttrNm5" runat="server" Text="Label"></asp:Label>
								</asp:TableCell>
								<asp:TableCell ID="celCastAttrSeq5" runat="server" CssClass="tdDataStyle">
									<asp:DropDownList ID="lstCastAttrSeq5" runat="server" Width="206px">
									</asp:DropDownList>
									<asp:TextBox ID="txtAttrInputValue5" runat="server" Width="200px" MaxLength="40"></asp:TextBox>
								</asp:TableCell>
							</asp:TableRow>
							<asp:TableRow ID="rowAttr6" runat="server">
								<asp:TableCell ID="celCastAttrNm6" runat="server" CssClass="tdHeaderStyle">
									<asp:Label ID="lblCastAttrNm6" runat="server" Text="Label"></asp:Label>
								</asp:TableCell>
								<asp:TableCell ID="celCastAttrSeq6" runat="server" CssClass="tdDataStyle">
									<asp:DropDownList ID="lstCastAttrSeq6" runat="server" Width="206px">
									</asp:DropDownList>
									<asp:TextBox ID="txtAttrInputValue6" runat="server" Width="200px" MaxLength="40"></asp:TextBox>
								</asp:TableCell>
							</asp:TableRow>
							<asp:TableRow ID="rowAttr7" runat="server">
								<asp:TableCell ID="celCastAttrNm7" runat="server" CssClass="tdHeaderStyle">
									<asp:Label ID="lblCastAttrNm7" runat="server" Text="Label"></asp:Label>
								</asp:TableCell>
								<asp:TableCell ID="celCastAttrSeq7" runat="server" CssClass="tdDataStyle">
									<asp:DropDownList ID="lstCastAttrSeq7" runat="server" Width="206px">
									</asp:DropDownList>
									<asp:TextBox ID="txtAttrInputValue7" runat="server" Width="200px" MaxLength="40"></asp:TextBox>
								</asp:TableCell>
							</asp:TableRow>
							<asp:TableRow ID="rowAttr8" runat="server">
								<asp:TableCell ID="celCastAttrNm8" runat="server" CssClass="tdHeaderStyle">
									<asp:Label ID="lblCastAttrNm8" runat="server" Text="Label"></asp:Label>
								</asp:TableCell>
								<asp:TableCell ID="celCastAttrSeq8" runat="server" CssClass="tdDataStyle">
									<asp:DropDownList ID="lstCastAttrSeq8" runat="server" Width="206px">
									</asp:DropDownList>
									<asp:TextBox ID="txtAttrInputValue8" runat="server" Width="200px" MaxLength="40"></asp:TextBox>
								</asp:TableCell>
							</asp:TableRow>
							<asp:TableRow ID="rowAttr9" runat="server">
								<asp:TableCell ID="celCastAttrNm9" runat="server" CssClass="tdHeaderStyle">
									<asp:Label ID="lblCastAttrNm9" runat="server" Text="Label"></asp:Label>
								</asp:TableCell>
								<asp:TableCell ID="celCastAttrSeq9" runat="server" CssClass="tdDataStyle">
									<asp:DropDownList ID="lstCastAttrSeq9" runat="server" Width="206px">
									</asp:DropDownList>
									<asp:TextBox ID="txtAttrInputValue9" runat="server" Width="200px" MaxLength="40"></asp:TextBox>
								</asp:TableCell>
							</asp:TableRow>
							<asp:TableRow ID="rowAttr10" runat="server">
								<asp:TableCell ID="celCastAttrNm10" runat="server" CssClass="tdHeaderStyle">
									<asp:Label ID="lblCastAttrNm10" runat="server" Text="Label"></asp:Label>
								</asp:TableCell>
								<asp:TableCell ID="celCastAttrSeq10" runat="server" CssClass="tdDataStyle">
									<asp:DropDownList ID="lstCastAttrSeq10" runat="server" Width="206px">
									</asp:DropDownList>
									<asp:TextBox ID="txtAttrInputValue10" runat="server" Width="200px" MaxLength="40"></asp:TextBox>
								</asp:TableCell>
							</asp:TableRow>
							<asp:TableRow ID="rowAttr11" runat="server">
								<asp:TableCell ID="celCastAttrNm11" runat="server" CssClass="tdHeaderStyle">
									<asp:Label ID="lblCastAttrNm11" runat="server" Text="Label"></asp:Label>
								</asp:TableCell>
								<asp:TableCell ID="celCastAttrSeq11" runat="server" CssClass="tdDataStyle">
									<asp:DropDownList ID="lstCastAttrSeq11" runat="server" Width="206px">
									</asp:DropDownList>
									<asp:TextBox ID="txtAttrInputValue11" runat="server" Width="200px" MaxLength="40"></asp:TextBox>
								</asp:TableCell>
							</asp:TableRow>
							<asp:TableRow ID="rowAttr12" runat="server">
								<asp:TableCell ID="celCastAttrNm12" runat="server" CssClass="tdHeaderStyle">
									<asp:Label ID="lblCastAttrNm12" runat="server" Text="Label"></asp:Label>
								</asp:TableCell>
								<asp:TableCell ID="celCastAttrSeq12" runat="server" CssClass="tdDataStyle">
									<asp:DropDownList ID="lstCastAttrSeq12" runat="server" Width="206px">
									</asp:DropDownList>
									<asp:TextBox ID="txtAttrInputValue12" runat="server" Width="200px" MaxLength="40"></asp:TextBox>
								</asp:TableCell>
							</asp:TableRow>
							<asp:TableRow ID="rowAttr13" runat="server">
								<asp:TableCell ID="celCastAttrNm13" runat="server" CssClass="tdHeaderStyle">
									<asp:Label ID="lblCastAttrNm13" runat="server" Text="Label"></asp:Label>
								</asp:TableCell>
								<asp:TableCell ID="celCastAttrSeq13" runat="server" CssClass="tdDataStyle">
									<asp:DropDownList ID="lstCastAttrSeq13" runat="server" Width="206px">
									</asp:DropDownList>
									<asp:TextBox ID="txtAttrInputValue13" runat="server" Width="200px" MaxLength="40"></asp:TextBox>
								</asp:TableCell>
							</asp:TableRow>
							<asp:TableRow ID="rowAttr14" runat="server">
								<asp:TableCell ID="celCastAttrNm14" runat="server" CssClass="tdHeaderStyle">
									<asp:Label ID="lblCastAttrNm14" runat="server" Text="Label"></asp:Label>
								</asp:TableCell>
								<asp:TableCell ID="celCastAttrSeq14" runat="server" CssClass="tdDataStyle">
									<asp:DropDownList ID="lstCastAttrSeq14" runat="server" Width="206px">
									</asp:DropDownList>
									<asp:TextBox ID="txtAttrInputValue14" runat="server" Width="200px" MaxLength="40"></asp:TextBox>
								</asp:TableCell>
							</asp:TableRow>
							<asp:TableRow ID="rowAttr15" runat="server">
								<asp:TableCell ID="celCastAttrNm15" runat="server" CssClass="tdHeaderStyle">
									<asp:Label ID="lblCastAttrNm15" runat="server" Text="Label"></asp:Label>
								</asp:TableCell>
								<asp:TableCell ID="celCastAttrSeq15" runat="server" CssClass="tdDataStyle">
									<asp:DropDownList ID="lstCastAttrSeq15" runat="server" Width="206px">
									</asp:DropDownList>
									<asp:TextBox ID="txtAttrInputValue15" runat="server" Width="200px" MaxLength="40"></asp:TextBox>
								</asp:TableCell>
							</asp:TableRow>
							<asp:TableRow ID="rowAttr16" runat="server">
								<asp:TableCell ID="celCastAttrNm16" runat="server" CssClass="tdHeaderStyle">
									<asp:Label ID="lblCastAttrNm16" runat="server" Text="Label"></asp:Label>
								</asp:TableCell>
								<asp:TableCell ID="celCastAttrSeq16" runat="server" CssClass="tdDataStyle">
									<asp:DropDownList ID="lstCastAttrSeq16" runat="server" Width="206px">
									</asp:DropDownList>
									<asp:TextBox ID="txtAttrInputValue16" runat="server" Width="200px" MaxLength="40"></asp:TextBox>
								</asp:TableCell>
							</asp:TableRow>
							<asp:TableRow ID="rowAttr17" runat="server">
								<asp:TableCell ID="celCastAttrNm17" runat="server" CssClass="tdHeaderStyle">
									<asp:Label ID="lblCastAttrNm17" runat="server" Text="Label"></asp:Label>
								</asp:TableCell>
								<asp:TableCell ID="celCastAttrSeq17" runat="server" CssClass="tdDataStyle">
									<asp:DropDownList ID="lstCastAttrSeq17" runat="server" Width="206px">
									</asp:DropDownList>
									<asp:TextBox ID="txtAttrInputValue17" runat="server" Width="200px" MaxLength="40"></asp:TextBox>
								</asp:TableCell>
							</asp:TableRow>
							<asp:TableRow ID="rowAttr18" runat="server">
								<asp:TableCell ID="celCastAttrNm18" runat="server" CssClass="tdHeaderStyle">
									<asp:Label ID="lblCastAttrNm18" runat="server" Text="Label"></asp:Label>
								</asp:TableCell>
								<asp:TableCell ID="celCastAttrSeq18" runat="server" CssClass="tdDataStyle">
									<asp:DropDownList ID="lstCastAttrSeq18" runat="server" Width="206px">
									</asp:DropDownList>
									<asp:TextBox ID="txtAttrInputValue18" runat="server" Width="200px" MaxLength="40"></asp:TextBox>
								</asp:TableCell>
							</asp:TableRow>
							<asp:TableRow ID="rowAttr19" runat="server">
								<asp:TableCell ID="celCastAttrNm19" runat="server" CssClass="tdHeaderStyle">
									<asp:Label ID="lblCastAttrNm19" runat="server" Text="Label"></asp:Label>
								</asp:TableCell>
								<asp:TableCell ID="celCastAttrSeq19" runat="server" CssClass="tdDataStyle">
									<asp:DropDownList ID="lstCastAttrSeq19" runat="server" Width="206px">
									</asp:DropDownList>
									<asp:TextBox ID="txtAttrInputValue19" runat="server" Width="200px" MaxLength="40"></asp:TextBox>
								</asp:TableCell>
							</asp:TableRow>
						</asp:Table>
					</asp:PlaceHolder>
					<asp:ValidationSummary ID="vdSummary" runat="server" ValidationGroup="Detail">
					</asp:ValidationSummary>
					<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" OnClientClick='return confirm("更新を行いますか？");' />
					<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
				</fieldset>
			</asp:Panel>
		</asp:Panel>
	</div>
	<asp:Panel runat="server" ID="pnlHideVale" Visible="false">
		<asp:Label ID="lblCastNm" runat="server" Text="Label"></asp:Label>
		<asp:DropDownList ID="lstActCategorySeq" runat="server" Width="206px" DataSourceID="dsActCategory" DataTextField="ACT_CATEGORY_NM" DataValueField="ACT_CATEGORY_SEQ">
		</asp:DropDownList>
		<asp:DropDownList ID="lstConnectType" runat="server" Width="206px" DataSourceID="dsConnectType" DataTextField="CODE_NM" DataValueField="CODE">
		</asp:DropDownList>
		<asp:TextBox ID="txtHandleKanaNm" runat="server" MaxLength="20" Width="200px"></asp:TextBox>
		<asp:TextBox ID="txtPriority" runat="server" MaxLength="3" Width="35px"></asp:TextBox>
		<asp:TextBox ID="txtStartPerformDay" runat="server" MaxLength="10" Width="100px"></asp:TextBox>
		<asp:CheckBox ID="chkOfflineDisplayFlag" runat="server" />
		<asp:CheckBox ID="chkNaFlag" runat="server" />
		<asp:Label ID="lblFriendIntroCd" runat="server" Text="Label" />
	</asp:Panel>
	<asp:ObjectDataSource ID="dsActCategory" runat="server" SelectMethod="GetList" TypeName="ActCategory">
		<SelectParameters>
			<asp:QueryStringParameter Name="pSiteCd" QueryStringField="sitecd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsLoginMail" runat="server" SelectMethod="GetListByTemplateType" TypeName="MailTemplate">
		<SelectParameters>
			<asp:QueryStringParameter Name="pSiteCd" QueryStringField="sitecd" Type="String" />
			<asp:Parameter DefaultValue="08" Name="pMailTemplateType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsLogoutMail" runat="server" SelectMethod="GetListByTemplateType" TypeName="MailTemplate">
		<SelectParameters>
			<asp:QueryStringParameter Name="pSiteCd" QueryStringField="sitecd" Type="String" />
			<asp:Parameter DefaultValue="16" Name="pMailTemplateType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsTime" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="68" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsConnectType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="61" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsMailRxType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="66" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsInfoMailRxType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="15" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="68" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>
