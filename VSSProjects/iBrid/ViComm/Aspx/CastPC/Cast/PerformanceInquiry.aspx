<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PerformanceInquiry.aspx.cs" Inherits="Cast_PerformanceInquiry"
	Title="稼動集計" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="稼動集計"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<asp:Panel runat="server" ID="pnlDtl">
				<fieldset>
					<legend>[期間指定]</legend>
					<table border="0" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle">
								集計開始日
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstFromYYYY" runat="server" Width="60px">
								</asp:DropDownList>年
								<asp:DropDownList ID="lstFromMM" runat="server" Width="44px">
								</asp:DropDownList>月
								<asp:DropDownList ID="lstFromDD" runat="server" Width="44px">
								</asp:DropDownList>日
								<asp:DropDownList ID="lstFromHH" runat="server" Width="44px">
								</asp:DropDownList>時
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								集計終了日
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstToYYYY" runat="server" Width="60px">
								</asp:DropDownList>年
								<asp:DropDownList ID="lstToMM" runat="server" Width="44px">
								</asp:DropDownList>月
								<asp:DropDownList ID="lstToDD" runat="server" Width="44px">
								</asp:DropDownList>日
								<asp:DropDownList ID="lstToHH" runat="server" Width="44px">
								</asp:DropDownList>時
							</td>
						</tr>
					</table>
					<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" />
				</fieldset>
				<fieldset>
					<legend>[稼動集計]</legend>
					<asp:GridView ID="grdPerformace" runat="server" AutoGenerateColumns="False" DataSourceID="dsTimeOperation" ShowFooter="True" AllowSorting="True" SkinID="GridView"
						OnRowDataBound="grdPerformace_RowDataBound">
						<Columns>
							<asp:TemplateField FooterText="合計" HeaderText="ｻｲﾄ">
								<ItemTemplate>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("SITE_NM") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Center" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="TV">
								<ItemTemplate>
									<asp:Label ID="Label2" runat="server" Text='<%# Eval("PRV_TV_TALK_MIN", "{0}分 ") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" />
								<ItemStyle HorizontalAlign="Right" Width="46px" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="TV<BR/>ﾁｬｯﾄ">
								<ItemTemplate>
									<asp:Label ID="Label3" runat="server" Text='<%# Eval("PUB_TV_TALK_MIN", "{0}分 ") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" />
								<ItemStyle HorizontalAlign="Right" Width="46px" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="音声">
								<ItemTemplate>
									<asp:Label ID="Label4" runat="server" Text='<%# Eval("PRV_VOICE_TALK_MIN", "{0}分 ") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" />
								<ItemStyle HorizontalAlign="Right" Width="46px" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="音声<BR/>ﾁｬｯﾄ">
								<ItemTemplate>
									<asp:Label ID="Label5" runat="server" Text='<%# Eval("PUB_VOICE_TALK_MIN", "{0}分 ") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" />
								<ItemStyle HorizontalAlign="Right" Width="46px" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="会話<BR/>ﾓﾆﾀ">
								<ItemTemplate>
									<asp:Label ID="Label6" runat="server" Text='<%# Eval("VIEW_TALK_MIN", "{0}分 ") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" />
								<ItemStyle HorizontalAlign="Right" Width="46px" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="音声<BR/>ﾓﾆﾀ">
								<ItemTemplate>
									<asp:Label ID="Label9" runat="server" Text='<%# Eval("WIRETAP_MIN", "{0}分 ") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" />
								<ItemStyle HorizontalAlign="Right" Width="46px" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="部屋<BR/>ﾓﾆﾀ">
								<ItemTemplate>
									<asp:Label ID="Label7" runat="server" Text='<%# Eval("VIEW_BROADCAST_MIN", "{0}分 ") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" />
								<ItemStyle HorizontalAlign="Right" Width="46px" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="合計分">
								<ItemTemplate>
									<asp:Label ID="Label8" runat="server" Text='<%# Eval("TALK_MIN", "{0}分 ") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" />
								<ItemStyle HorizontalAlign="Right" Width="46px" />
							</asp:TemplateField>
							<asp:BoundField DataField="USER_MAIL_COUNT" HeaderText="ﾒｰﾙ" DataFormatString="{0}件 ">
								<ItemStyle HorizontalAlign="Right" Width="46px" />
								<FooterStyle HorizontalAlign="Right" />
							</asp:BoundField>
						</Columns>
						<FooterStyle ForeColor="Black" BackColor="LightYellow" />
					</asp:GridView>
				</fieldset>
			</asp:Panel>
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsTimeOperation" runat="server" SelectMethod="GetList" TypeName="TimeOperation">
		<SelectParameters>
			<asp:ControlParameter ControlID="lstFromYYYY" Name="pFromYYYY" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstFromMM" Name="pFromMM" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstFromDD" Name="pFromDD" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstFromHH" Name="pFromHH" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstToYYYY" Name="pToYYYY" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstToMM" Name="pToMM" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstToDD" Name="pToDD" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstToHH" Name="pToHH" PropertyName="SelectedValue" Type="String" />
			<asp:QueryStringParameter Name="pUserSeq" Type="String" QueryStringField="userseq" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>
