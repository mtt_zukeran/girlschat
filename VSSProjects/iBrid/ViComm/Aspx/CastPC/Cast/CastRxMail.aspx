<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CastRxMail.aspx.cs" Inherits="Cast_CastRxMail" Title="��M���[���a�n�w" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="��M���[���a�n�w"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<fieldset class="fieldset">
		<legend>[��M���[���a�n�w]</legend>
		<table border="1">
			<tr>
				<td width="240" align="center">
					<asp:Label ID="lblSiteNm" runat="server" Font-Bold="True" Font-Underline="False" ForeColor="#FF8000"></asp:Label>
				</td>
				<td align="center" width="30">
					<asp:Button runat="server" ID="btnSeek" Text="����" OnClick="btnSeek_Click" />
				</td>
				<td align="center">
					<asp:Button runat="server" ID="btnReturn" Text="�߂�" OnClick="btnCancel_Click" />
				</td>
			</tr>
		</table>
		<asp:GridView ID="grdMail" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsMailBox" AllowSorting="True" SkinID="GridView"
			PageSize="5" OnPreRender="grdMail_PreRender">
			<Columns>
				<asp:TemplateField HeaderText="���M�Җ�">
					<ItemTemplate>
						<asp:HyperLink ID="lnkManView" runat="server" NavigateUrl='<%# string.Format("~/Man/ManView.aspx?sitecd={0}&manuserseq={1}&castcharno={2}&return=../Cast/CastRxMail.aspx",Eval("SITE_CD"),Eval("TX_USER_SEQ"),Eval("USER_CHAR_NO")) %>'
							Text='<%# Eval("HANDLE_NM") %>'></asp:HyperLink>
					</ItemTemplate>
					<ItemStyle HorizontalAlign="Left" Width="120px" />
				</asp:TemplateField>
				<asp:BoundField DataField="MAIL_TITLE" HeaderText="����">
					<ItemStyle HorizontalAlign="Left" Width="150px" />
				</asp:BoundField>
				<asp:TemplateField HeaderText="���e">
					<ItemTemplate>
						<asp:Label ID="lblDoc" runat="server" Text='<%# string.Format("{0}{1}{2}{3}", Eval("MAIL_DOC1"),Eval("MAIL_DOC2"),Eval("MAIL_DOC3"),Eval("MAIL_DOC4"),Eval("MAIL_DOC5")) %>'></asp:Label>
					</ItemTemplate>
					<ItemStyle HorizontalAlign="Left" Width="220px" />
				</asp:TemplateField>
				<asp:TemplateField HeaderText="�Y�t">
					<ItemTemplate>
						<asp:LinkButton ID="lnkAttached" runat="server" CommandArgument='<%# string.Format("{0}:{1}:{2}:{3}:{4}",Eval("TX_USER_SEQ"),Eval("TX_USER_CHAR_NO"),Eval("ATTACHED_OBJ_TYPE"),Eval("PIC_SEQ"),Eval("MOVIE_SEQ"))%>'
							Text="�Y�t" OnCommand="lnkAttached_Command">
						</asp:LinkButton>
					</ItemTemplate>
					<ItemStyle HorizontalAlign="Center" />
				</asp:TemplateField>
				<asp:BoundField DataField="CREATE_DATE" HeaderText="��M��" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
					<ItemStyle HorizontalAlign="Center" />
				</asp:BoundField>
				<asp:TemplateField HeaderText="�폜">
					<ItemTemplate>
						<asp:LinkButton ID="lnkDel" runat="server" CommandArgument='<%# Eval("MAIL_SEQ") %>' Text="�폜" OnCommand="lnkDel_Command"></asp:LinkButton>
					</ItemTemplate>
					<ItemStyle HorizontalAlign="Center" />
				</asp:TemplateField>
			</Columns>
			<PagerSettings Mode="NumericFirstLast" />
		</asp:GridView>
	</fieldset>
	<asp:ObjectDataSource ID="dsMailBox" runat="server" SelectMethod="GetPageCollection" TypeName="MailBox" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelecting="dsMailBox_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
			<asp:Parameter Name="pMailBoxType" Type="String" />
			<asp:Parameter Name="pTxRxType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>
