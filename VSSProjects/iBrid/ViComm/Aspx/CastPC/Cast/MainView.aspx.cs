﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: メインビュー
--	Progaram ID		: MainView
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Cast_MainView:System.Web.UI.Page {
	private SessionObjs userObjs;

	protected void Page_Load(object sender,EventArgs e) {
		userObjs = (SessionObjs)Session["objs"];
		if ((!IsPostBack) || (iBridUtil.GetStringValue(Request["refresh"]).Equals("1"))) {
			FirstLoad();
			InitPage();
		}
	}

	private void FirstLoad() {
		ViewState["SITE_CD"] = iBridUtil.GetStringValue(Request["sitecd"]);
		btnOnlineMonitor.Visible = userObjs.cast.onlineLiveEnableFlag;
		int iCompanyMask = int.Parse(iBridUtil.GetStringValue(Session["CompanyMask"]));
		btnLiveTimer.Visible = ((iCompanyMask & ViCommConst.MSK_COMPANY_ARCHE) > 0);
	}

	private void InitPage() {
		this.btnChangeSeekMan.Text = DisplayWordUtil.Replace(this.btnChangeSeekMan.Text);
		switch (iBridUtil.GetStringValue(Session["DISP_MODE"])) {
			case "1":
				lblInfoTitle.Text = "キャラクター設定";
				break;
			case "2":
				lblInfoTitle.Text = DisplayWordUtil.Replace("男性会員検索");
				break;
			case "3":
				lblInfoTitle.Text = "メールＢＯＸ";
				break;
			case "4":
				lblInfoTitle.Text = "画像・動画設定";
				break;
		}
		DataBind();
	}

	protected void btnReSeek_Click(object sender,EventArgs e) {
		DataBind();
	}

	protected void btnChangeCharacter_Click(object sender,EventArgs e) {
		Session["DISP_MODE"] = "1";
		lblInfoTitle.Text = "キャラクター設定";
		DataBind();
	}

	protected void btnChangeSeekMan_Click(object sender,EventArgs e) {
		Session["DISP_MODE"] = "2";
		lblInfoTitle.Text = DisplayWordUtil.Replace("男性会員検索");
		DataBind();
	}
	protected void btnChanegMailBox_Click(object sender,EventArgs e) {
		Session["DISP_MODE"] = "3";
		lblInfoTitle.Text = "メールＢＯＸ";
		DataBind();
	}

	protected void btnPicMovie_Click(object sender,EventArgs e) {
		Session["DISP_MODE"] = "4";
		lblInfoTitle.Text = "画像・動画設定";
		DataBind();
	}
	protected void lnkTalkMovie_Command(object sender,CommandEventArgs e) {
		//		DisplayTalkMovieInfo(e.CommandArgument.ToString());
	}

	protected void lnkMailTemplate_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		Server.Transfer(string.Format("CastMailTemplateList.aspx?sitecd={0}&castcharno={1}",sKeys[0],sKeys[1]));
	}

	protected void btnOnlineMonitor_Click(object sender,EventArgs e) {
		string sSiteCd = "";
		using (Sys oSys = new Sys()) {
			oSys.GetValue("ONLINE_MONITOR_SITE_CD",out sSiteCd);
		}
		userObjs.site.GetOne(sSiteCd);

		using (IvpRequest oIvpRequest = new IvpRequest()) {
			string sResult = oIvpRequest.Request(
					sSiteCd,
					userObjs.site.ivpLocCd,
					userObjs.site.ivpSiteCd,
					ViCommConst.REQUEST_BROADCASTING,
					userObjs.cast.uri,
					userObjs.cast.userSeq,
					ViCommConst.MAIN_CHAR_NO,
					"01",	// VIDEO
					"",
					0,
					userObjs.site.supportChgMonitorToTalk.ToString(),
					ViCommConst.UseCrosmile.USE_UAC_AND_UAS);

			if (sResult.Equals(ViCommConst.SP_RESULT_OK)) {
			} else {
			}
		}
	}

	protected void btnLiveTimer_Click(object sender,EventArgs e) {
		Server.Transfer("ReuqestLiveTimer.aspx");
	}

	protected void btnPerformance_Click(object sender,EventArgs e) {
		Server.Transfer(string.Format("PerformanceInquiry.aspx?userseq={0}",userObjs.cast.userSeq));
	}

	protected void btnDummyTalk_Click(object sender,EventArgs e) {
		string sDummyTalk = iBridUtil.GetStringValue(Session["DUMMY_TALK"]);
		if (sDummyTalk.Equals("1")) {
			Session["DUMMY_TALK"] = "0";
		} else {
			Session["DUMMY_TALK"] = "1";
		}
		using (Cast oCast = new Cast()) {
			oCast.SetTalkTatus(userObjs.cast.userSeq,int.Parse(Session["DUMMY_TALK"].ToString()));
		}
		DataBind();
	}

	public string GetDummyTalkGuidance() {
		string sDummyTalk = iBridUtil.GetStringValue(Session["DUMMY_TALK"]);
		if (sDummyTalk.Equals("1")) {
			pnlDummyTalk.Visible = true;
			return "休憩終了";
		} else {
			pnlDummyTalk.Visible = false;
			return "一時休憩";
		}
	}

	protected bool IsNewSite(object pUseOtherInfoFlag) {
		return (pUseOtherInfoFlag.ToString().Equals("1") == false);
	}

	protected void dsCast_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = userObjs.cast.loginId;
	}

	protected void dsCharacter_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = ViewState["SITE_CD"];
		e.InputParameters[1] = userObjs.cast.userSeq;
	}

	protected object IsMultiCharFlg(object siteCD) {
		string strMultiCharFlag = string.Empty;
		(new Site()).GetValue(siteCD.ToString(),"MULTI_CHAR_FLAG",ref strMultiCharFlag);
		return strMultiCharFlag == "1";
	}

	protected object IsSelectChar(object siteCD) {
		int iTvCount = 0;
		int iVoiceCount = 0;
		string sConnectType = "";
		bool bSelectChar = false;
		using (CastCharacter oCastCharacter = new CastCharacter()) {
			DataSet ds = oCastCharacter.GetLoginCharacterList(siteCD.ToString(),userObjs.cast.userSeq);
			DataRow dr;
			for (int i = 0;i < ds.Tables[0].Rows.Count;i++) {
				dr = ds.Tables[0].Rows[0];
				sConnectType = iBridUtil.GetStringValue(dr["CONNECT_TYPE"]);
				if (sConnectType.Equals(ViCommConst.WAIT_TYPE_BOTH) || sConnectType.Equals(ViCommConst.WAIT_TYPE_VIDEO)) {
					iTvCount++;
				} else if (sConnectType.Equals(ViCommConst.WAIT_TYPE_VOICE)) {
					iVoiceCount++;
				}
			}
		}
		if (iVoiceCount > 1 || iTvCount > 1) {
			bSelectChar = true;
		}
		return bSelectChar;
	}

	protected object SetUserCharNo(object siteCD,object userSeq,object userCharNo) {

		string strUserCharNo = string.Empty;
		string strMultiCharFlag = string.Empty;
		(new Site()).GetValue(siteCD.ToString(),"MULTI_CHAR_FLAG",ref strMultiCharFlag);
		if (strMultiCharFlag == "1") {
			strUserCharNo = userCharNo.ToString();
		}
		return strUserCharNo;
	}


	protected void grdCharacter_DataBinding(object sender,EventArgs e) {
		int iCompanyMask = int.Parse(iBridUtil.GetStringValue(Session["CompanyMask"]));

		switch (iBridUtil.GetStringValue(Session["DISP_MODE"])) {
			case "1":
				grdCharacter.Columns[3].Visible = true;
				grdCharacter.Columns[4].Visible = true;
				grdCharacter.Columns[5].Visible = true;
				grdCharacter.Columns[6].Visible = true;
				grdCharacter.Columns[7].Visible = true;
				grdCharacter.Columns[8].Visible = true;
				grdCharacter.Columns[9].Visible = ((iCompanyMask & ViCommConst.MSK_COMPANY_MASHUP) <= 0);
				grdCharacter.Columns[10].Visible = false;
				grdCharacter.Columns[11].Visible = false;
				grdCharacter.Columns[12].Visible = false;
				grdCharacter.Columns[13].Visible = false;
				grdCharacter.Columns[14].Visible = false;
				grdCharacter.Columns[15].Visible = false;
				grdCharacter.Columns[16].Visible = false;
				grdCharacter.Columns[17].Visible = false;
				//				grdCharacter.Width = 706;
				break;
			case "2":
				grdCharacter.Columns[3].Visible = false;
				grdCharacter.Columns[4].Visible = false;
				grdCharacter.Columns[5].Visible = false;
				grdCharacter.Columns[6].Visible = false;
				grdCharacter.Columns[7].Visible = false;
				grdCharacter.Columns[8].Visible = false;
				grdCharacter.Columns[9].Visible = false;
				grdCharacter.Columns[10].Visible = true;
				grdCharacter.Columns[11].Visible = true;
				grdCharacter.Columns[12].Visible = true;
				grdCharacter.Columns[13].Visible = false;
				grdCharacter.Columns[14].Visible = false;
				grdCharacter.Columns[15].Visible = false;
				grdCharacter.Columns[16].Visible = false;
				grdCharacter.Columns[17].Visible = false;
				//				grdCharacter.Width = 608;
				break;
			case "3":
				grdCharacter.Columns[3].Visible = false;
				grdCharacter.Columns[4].Visible = false;
				grdCharacter.Columns[5].Visible = false;
				grdCharacter.Columns[6].Visible = false;
				grdCharacter.Columns[7].Visible = false;
				grdCharacter.Columns[8].Visible = false;
				grdCharacter.Columns[9].Visible = false;
				grdCharacter.Columns[10].Visible = false;
				grdCharacter.Columns[11].Visible = false;
				grdCharacter.Columns[12].Visible = false;
				grdCharacter.Columns[13].Visible = true;
				grdCharacter.Columns[14].Visible = true;
				grdCharacter.Columns[15].Visible = true;
				grdCharacter.Columns[16].Visible = false;
				grdCharacter.Columns[17].Visible = false;
				//				grdCharacter.Width = 608;
				break;
			case "4":
				grdCharacter.Columns[3].Visible = true;
				grdCharacter.Columns[4].Visible = false;
				grdCharacter.Columns[5].Visible = false;
				grdCharacter.Columns[6].Visible = false;
				grdCharacter.Columns[7].Visible = false;
				grdCharacter.Columns[8].Visible = false;
				grdCharacter.Columns[9].Visible = false;
				grdCharacter.Columns[10].Visible = false;
				grdCharacter.Columns[11].Visible = false;
				grdCharacter.Columns[12].Visible = false;
				grdCharacter.Columns[13].Visible = false;
				grdCharacter.Columns[14].Visible = false;
				grdCharacter.Columns[15].Visible = false;
				grdCharacter.Columns[16].Visible = true;
				grdCharacter.Columns[17].Visible = true;
				//				grdCharacter.Width = 706;
				break;
		}
	}
}
