﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: ログイン履歴
--	Progaram ID		: CastLoginHistory
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net;
using System.IO;
using iBridCommLib;
using ViComm;

public partial class Cast_CastLoginHistory:System.Web.UI.Page {
	private SessionObjs userObjs;

	protected void Page_Load(object sender,EventArgs e) {
		userObjs = (SessionObjs)Session["objs"];
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.DOCOMO);
		if (!IsPostBack) {
			InitPage();
		}

		if (userObjs.site.GetOne(iBridUtil.GetStringValue(ViewState["SITE_CD"]))) {
			lblSiteNm.Text = userObjs.site.siteNm;
			if (userObjs.site.IsActiveMulitCharFlag(ViewState["SITE_CD"].ToString())) {
				lblSiteNm.Text += string.Concat("-",ViewState["CAST_CHAR_NO"].ToString());
			}
		}
	}

	private void SetViewState() {
		ViewState["SITE_CD"] = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		ViewState["CAST_CHAR_NO"] = iBridUtil.GetStringValue(Request.QueryString["castcharno"]);
		ViewState["REPORT_DAY_FROM"] = DateTime.Now.AddMonths(-1).ToString("yyyy/MM/dd");
		ViewState["REPORT_DAY_TO"] = DateTime.Now.AddDays(1).ToString("yyyy/MM/dd");
	}

	private void InitPage() {
		SetViewState();
		ClearField();
		DataBind();
	}

	private void ClearField() {
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		Server.Transfer("MainView.aspx");
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		DataBind();
	}

	protected void dsLoginCharacter_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = iBridUtil.GetStringValue(ViewState["SITE_CD"]);
		e.InputParameters[1] = userObjs.cast.userSeq;
		e.InputParameters[2] = iBridUtil.GetStringValue(ViewState["CAST_CHAR_NO"]);
		e.InputParameters[3] = iBridUtil.GetStringValue(ViewState["REPORT_DAY_FROM"]);
		e.InputParameters[4] = iBridUtil.GetStringValue(ViewState["REPORT_DAY_TO"]);
	}
}
