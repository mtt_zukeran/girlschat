<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CastDiaryMainte.aspx.cs" Inherits="Cast_CastDiaryMainte" Title="日記編集" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="日記編集"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<asp:Panel runat="server" ID="pnlDtl">
				<fieldset class="fieldset">
					<legend>[日記設定]</legend>
					<table border="1">
						<tr>
							<td width="245" align="center">
								<asp:Label ID="lblSiteNm" runat="server" Font-Bold="True" Font-Underline="False" ForeColor="#FF8000"></asp:Label>
							</td>
							<td width="332" align="center">
								文章
							</td>
						</tr>
					</table>
					<table border="0">
						<tr>
							<td>
								<asp:Calendar ID="objCalender" runat="server" BackColor="White" BorderColor="#999999" CellPadding="4" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt"
									ForeColor="Black" Height="228px" Width="248px" OnSelectionChanged="objCalender_SelectionChanged" OnDayRender="objCalender_DayRender">
									<SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
									<SelectorStyle BackColor="#CCCCCC" />
									<WeekendDayStyle BackColor="#FFFFCC" />
									<TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
									<OtherMonthDayStyle ForeColor="Gray" />
									<NextPrevStyle VerticalAlign="Bottom" />
									<DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
									<TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
								</asp:Calendar>
							</td>
							<td class="tdDataStyle">
								<table border="0">
									<tr>
										<td>
											<asp:Label ID="lblDay" runat="server" Font-Bold="True" Font-Underline="False" ForeColor="#FF8000"></asp:Label>
										</td>
									</tr>
									<tr>
										<td>
											<asp:TextBox ID="txtTitle" runat="server" MaxLength="30" Width="180px"></asp:TextBox><br />
											<asp:RequiredFieldValidator ID="vdrTitle" runat="server" ErrorMessage="日記のタイトルを入力して下さい。" ControlToValidate="txtTitle" ValidationGroup="Detail" Display="Dynamic">*</asp:RequiredFieldValidator>
										</td>
									</tr>
									<tr>
										<td valign="top" style="height: 176px">
											<asp:TextBox ID="txtHtmlDoc" runat="server" MaxLength="1000" Width="180px" Columns="30" Rows="13" TextMode="MultiLine"></asp:TextBox>
											<asp:RequiredFieldValidator ID="vdrHtmlDoc" runat="server" ErrorMessage="日記の文章を入力して下さい。" ControlToValidate="txtHtmlDoc"  ValidationGroup="Detail" Display="Dynamic">*</asp:RequiredFieldValidator>
                                            <asp:CustomValidator ID="vdcHtmlDoc" runat="server" ErrorMessage='<%# string.Format("日記の文章は{0}文字まで入力可能です。",txtHtmlDoc.MaxLength) %>'
                                                ValidationGroup="Detail" ControlToValidate="txtHtmlDoc" OnServerValidate="vdcHtmlDoc_ServerValidate">*</asp:CustomValidator>
											<asp:Image ID="imgPic" runat="server" Width="133">
											</asp:Image>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td align="right">
								写真アップロード：
								<asp:FileUpload ID="uldPic" runat="server" Width="220px" />
							</td>
						</tr>
					</table>
					<asp:ValidationSummary ID="vdSummary" runat="server" ValidationGroup="Detail">
					</asp:ValidationSummary>
					<asp:Button runat="server" ID="btnCancel" Text="戻る" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" OnClientClick='return confirm("更新を行いますか？");' />
					<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Detail" OnClientClick='return confirm("削除を行いますか？");' />
				</fieldset>
			</asp:Panel>
			<asp:Panel runat="server" ID="pnlDiary">
				<fieldset class="fieldset">
					<legend>[過去日記一覧表示]</legend>
					<table border="0">
						<tr>
							<td>
								作成日
							</td>
							<td>
								<asp:DropDownList ID="lstFromYYYY" runat="server" Width="56px">
								</asp:DropDownList>年
								<asp:DropDownList ID="lstFromMM" runat="server" Width="40px">
								</asp:DropDownList>月
								<asp:DropDownList ID="lstFromDD" runat="server" Width="40px">
								</asp:DropDownList>日&nbsp;〜
								<asp:DropDownList ID="lstToYYYY" runat="server" Width="56px">
								</asp:DropDownList>年
								<asp:DropDownList ID="lstToMM" runat="server" Width="40px">
								</asp:DropDownList>月
								<asp:DropDownList ID="lstToDD" runat="server" Width="40px">
								</asp:DropDownList>日
							</td>
							<td>
								<asp:Button runat="server" ID="btnDiarySeek" Text="検索" CssClass="seekbutton" OnClick="btnDiarySeek_Click" CausesValidation="True" ValidationGroup="Key" />
							</td>
						</tr>
					</table>
					<asp:GridView ID="grdDiary" runat="server" AutoGenerateColumns="False" AllowSorting="True" SkinID="GridView" AllowPaging="True">
						<Columns>
							<asp:BoundField HeaderText="作成日" DataField="REPORT_DAY">
								<ItemStyle HorizontalAlign="Center" />
							</asp:BoundField>
							<asp:TemplateField HeaderText="作成者">
								<ItemTemplate>
									<asp:Label ID="lblHandlNm" runat="server" Text='<%# GetHandlNm() %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField HeaderText="タイトル" DataField="DIARY_TITLE">
								<ItemStyle Width="150px" />
							</asp:BoundField>
							<asp:TemplateField HeaderText="内容">
								<ItemTemplate>
									<asp:Label ID="lblHtmldoc1" runat="server" Text='<%# string.Format("{0}{1}{2}{3}",CnvHTML(Eval("HTML_DOC1")), Eval("HTML_DOC2"), Eval("HTML_DOC3"), Eval("HTML_DOC4")) %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle Width="300px" />
							</asp:TemplateField>
						</Columns>
					</asp:GridView>
				</fieldset>
			</asp:Panel>
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsDiary" runat="server" SelectMethod="GetDiaryList" TypeName="CastDiary" OnSelecting="dsDiary_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" />
			<asp:Parameter Name="pCastSeq" />
			<asp:Parameter Name="pCastCharNo" />
			<asp:Parameter Name="pReportDayFrom" />
			<asp:Parameter Name="pReportDayTo" />
		</SelectParameters>
	</asp:ObjectDataSource>
	&nbsp; &nbsp; &nbsp;
</asp:Content>
