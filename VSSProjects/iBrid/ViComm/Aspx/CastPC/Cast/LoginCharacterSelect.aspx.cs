﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: 出演者キャラクター設定
--	Progaram ID		: LoginCharacterSelect
--
--  Creation Date	: 2010.01.19
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class Cast_LoginCharacterSelect:System.Web.UI.Page {
	private SessionObjs userObjs;

	protected void Page_Load(object sender,EventArgs e) {
		userObjs = (SessionObjs)Session["objs"];
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.DOCOMO);
		if (!IsPostBack) {
			InitPage();
		}
	}

	private void SetViewState() {
		ViewState["SITE_CD"] = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
	}

	private void InitPage() {
		SetViewState();
		ClearField();
		DataBind();
		int iRowCount = grdCharacter.Rows.Count;

		for (int i = 0;i < iRowCount;i++) {
			if (grdCharacter.DataKeys[i].Value.Equals(ViCommConst.WAIT_TYPE_VIDEO) || grdCharacter.DataKeys[i].Value.Equals(ViCommConst.WAIT_TYPE_BOTH)) {
				grdCharacter.Rows[i].Visible = false;
			} else {
				grdCharacter.Rows[i].Visible = true;
			}
		}
	}

	private void ClearField() {
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		int iRowCount = grdCharacter.Rows.Count;
		string sCharLoginEndDate = string.Empty;
		using (Cast oCast = new Cast()) {
			DataSet ds = oCast.GetOne(userObjs.cast.loginId);
			sCharLoginEndDate = iBridUtil.GetStringValue(ds.Tables[0].Rows[0]["END_DATE"]);
		}
		DateTime dtEndDate;
		DateTime.TryParseExact(sCharLoginEndDate,"yyyy/MM/dd HH:mm:ss",System.Globalization.DateTimeFormatInfo.InvariantInfo,System.Globalization.DateTimeStyles.None,out dtEndDate);
		string sSiteCd = ViewState["SITE_CD"].ToString();
		string sUserSeq = userObjs.cast.userSeq;

		for (int i = 0;i < iRowCount;i++) {
			RadioButton rdoLoginCharacterSelect = (RadioButton)grdCharacter.Rows[i].FindControl("rdoLoginCharacterSelect") as RadioButton;

			if (grdCharacter.Rows[i].Visible) {
				string sUserCharNo = grdCharacter.Rows[i].Cells[1].Text;

				if (rdoLoginCharacterSelect.Checked) {
					//ログイン
					using (DbSession db = new DbSession()) {
						db.PrepareProcedure("START_WAITTING");
						db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,sSiteCd);
						db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,sUserSeq);
						db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,sUserCharNo);
						db.ProcedureInParm("PSTANDBY_END_DATE",DbSession.DbType.DATE,dtEndDate);
						db.ProcedureInParm("PCONNECT_TYPE",DbSession.DbType.VARCHAR2,string.Empty);
						db.ProcedureInParm("PMONITOR_ENABLE_FLAG",DbSession.DbType.NUMBER,0);
						db.ProcedureInParm("PMOBILE_TERMINAL_NM",DbSession.DbType.VARCHAR2,string.Empty);
						db.ProcedureInParm("PDUMMY_TALK_FLAG",DbSession.DbType.NUMBER,0);
						db.ProcedureInParm("PWAITING_COMMENT",DbSession.DbType.VARCHAR2,string.Empty);
						db.ProcedureInParm("PCHANGE_RANK",DbSession.DbType.VARCHAR2,string.Empty);
						db.ProcedureInParm("PSTANDBY_TIME_TYPE",DbSession.DbType.VARCHAR2,string.Empty);
						db.ProcedureInParm("PADMIN_FLAG",DbSession.DbType.VARCHAR2,ViCommConst.FLAG_OFF_STR);
						db.ProcedureInParm("PLOGIN_BY_TEL_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_ON);
						db.ProcedureOutParm("PLOGIN_SEQ",DbSession.DbType.VARCHAR2);
						db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
						db.ExecuteProcedure();
					}
				} else {
					//ログアウト
					string[] arrUserCharNo = { sUserCharNo };
					using (DbSession db = new DbSession()) {
						db.PrepareProcedure("CAST_CHARACTER_LOGOFF");
						db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,sSiteCd);
						db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,sUserSeq);
						db.ProcedureInArrayParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,1,arrUserCharNo);
						db.ProcedureInParm("PUSER_CHAR_RECORD_COUNT",DbSession.DbType.NUMBER,1);
						db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
						db.ExecuteProcedure();
					}
				}
			}
		}
		Server.Transfer("MainView.aspx");
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		Server.Transfer("MainView.aspx");
	}

	protected object IsStnandByCharacter(object pCharacterOnlineStatus) {
		return iBridUtil.GetStringValue(pCharacterOnlineStatus).Equals(ViCommConst.USER_WAITING.ToString()) || iBridUtil.GetStringValue(pCharacterOnlineStatus).Equals(ViCommConst.USER_TALKING.ToString());
	}

	protected void dsCharacter_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = ViewState["SITE_CD"];
		e.InputParameters[1] = userObjs.cast.userSeq;
	}

	protected void grdCharacter_DataBinding(object sender,EventArgs e) {
	}

	protected void rdoLoginCharacterSelect_CheckedChanged(object sender,System.EventArgs e) {
		int iRowCount = grdCharacter.Rows.Count;
		RadioButton rdoSender = (RadioButton)sender as RadioButton;

		for (int i = 0;i < iRowCount;i++) {
			RadioButton rdoLoginCharacterSelect = (RadioButton)grdCharacter.Rows[i].FindControl("rdoLoginCharacterSelect") as RadioButton;
			if (!(rdoLoginCharacterSelect.ClientID).Equals(rdoSender.ClientID)) {
				rdoLoginCharacterSelect.Checked = false;
			} else {
				rdoLoginCharacterSelect.Checked = true;
			}
		}
	}
}
