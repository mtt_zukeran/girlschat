<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="LoginCharacterSelect.aspx.cs" Inherits="Cast_LoginCharacterSelect"
	Title="ログインキャラクター選択" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="ログインキャラクター選択"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<asp:Panel runat="server" ID="pnlDtl">
				<fieldset class="fieldset">
					<legend>[ログインキャラクター選択]</legend>
					<asp:Panel runat="server" ID="pnlCharacter">
						<fieldset class="fieldset">
							<legend>[<asp:Label ID="lblInfoTitle" runat="server" Text="キャラクター設定"></asp:Label>]</legend>
							<asp:GridView ID="grdCharacter" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsCharacter" AllowSorting="True" SkinID="GridView"
								DataKeyNames="CONNECT_TYPE" OnDataBinding="grdCharacter_DataBinding">
								<Columns>
									<asp:TemplateField HeaderText="選択">
										<ItemStyle HorizontalAlign="Center" Width="30px" />
										<ItemTemplate>
											<asp:RadioButton ID="rdoLoginCharacterSelect" runat="server" GroupName="rdoLoginChar" Checked='<%#IsStnandByCharacter(Eval("CHARACTER_ONLINE_STATUS"))%>' OnCheckedChanged="rdoLoginCharacterSelect_CheckedChanged"
												AutoPostBack="True">
											</asp:RadioButton>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:BoundField DataField="USER_CHAR_NO" HeaderText="ｷｬﾗNo">
										<ItemStyle HorizontalAlign="Center" Width="50px" />
									</asp:BoundField>
									<asp:BoundField DataField="HANDLE_NM" HeaderText="ﾊﾝﾄﾞﾙ名">
										<ItemStyle HorizontalAlign="Left" Width="200px" />
									</asp:BoundField>
								</Columns>
								<PagerSettings Mode="NumericFirstLast" />
							</asp:GridView>
						</fieldset>
					</asp:Panel>
					<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" OnClientClick='return confirm("更新を行いますか？");' />
					<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
				</fieldset>
			</asp:Panel>
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsTime" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="68" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCharacter" runat="server" SelectMethod="GetLoginCharacterList" TypeName="CastCharacter" OnSelecting="dsCharacter_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCD" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>
