﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: 出演者写真設定
--	Progaram ID		: CastPictureMainte
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using iBridCommLib;
using ViComm;

public partial class Cast_CastPicMainte:System.Web.UI.Page {
	private SessionObjs userObjs;

	protected void Page_Load(object sender,EventArgs e) {
		userObjs = (SessionObjs)Session["objs"];
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.DOCOMO);
		if (!IsPostBack) {
			InitPage();
		}
		if (userObjs.site.GetOne(iBridUtil.GetStringValue(ViewState["SITE_CD"]))) {
			lblSiteNm.Text = userObjs.site.siteNm;
			if (userObjs.site.IsActiveMulitCharFlag(ViewState["SITE_CD"].ToString())) {
				lblSiteNm.Text += string.Concat("-",ViewState["CAST_CHAR_NO"].ToString());
			}
		}
	}

	private void SetViewState() {
		ViewState["SITE_CD"] = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		ViewState["CAST_CHAR_NO"] = iBridUtil.GetStringValue(Request.QueryString["castcharno"]);
	}

	private void InitPage() {
		SetViewState();
		ClearField();
		DataBind();
	}

	private void ClearField() {
	}


	protected void btnUp_Command(object sender,CommandEventArgs e) {
		SetPicPostion(decimal.Parse(e.CommandArgument.ToString()),-1);
	}

	protected void btnDown_Command(object sender,CommandEventArgs e) {
		SetPicPostion(decimal.Parse(e.CommandArgument.ToString()),1);
	}

	protected void btnUpload_Click(object sender,EventArgs e) {
		UploadPicture();
		Server.Transfer(string.Format("CastPicMainte.aspx?sitecd={0}&castcharno={1}",ViewState["SITE_CD"].ToString(),ViewState["CAST_CHAR_NO"].ToString()));
	}

	protected void btnProfile_Command(object sender,CommandEventArgs e) {
		UpdateProfilePicture(iBridUtil.GetStringValue(e.CommandArgument),"0","1");
		FTPUpload(iBridUtil.GetStringValue(e.CommandArgument));
		Server.Transfer(string.Format("CastPicMainte.aspx?sitecd={0}&castcharno={1}",ViewState["SITE_CD"].ToString(),ViewState["CAST_CHAR_NO"].ToString()));
	}

	protected void btnDelete_Command(object sender,CommandEventArgs e) {
		DeletePicture(e.CommandArgument.ToString());
		Server.Transfer(string.Format("CastPicMainte.aspx?sitecd={0}&castcharno={1}",ViewState["SITE_CD"].ToString(),ViewState["CAST_CHAR_NO"].ToString()));
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		Server.Transfer("MainView.aspx");
	}

	private void UpdateProfilePicture(string pPicSeq,string pDelFlag,string pProfilePicFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_PIC_PROFILE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,ViewState["SITE_CD"].ToString());
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.NUMBER,userObjs.cast.userSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,ViewState["CAST_CHAR_NO"].ToString()); // modify UCN
			db.ProcedureInParm("PPIC_SEQ",DbSession.DbType.NUMBER,pPicSeq);
			db.ProcedureInParm("pDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureInParm("pPROFILE_PIC_FLAG",DbSession.DbType.NUMBER,pProfilePicFlag);
			db.ProcedureInParm("pPIC_TYPE",DbSession.DbType.NUMBER,ViCommConst.ATTACHED_PROFILE);
			db.ProcedureInParm("POBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
			db.ProcedureInParm("POBJ_NOT_PUBLISH_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
			db.ProcedureInParm("PPIC_TITLE",DbSession.DbType.VARCHAR2,null);
			db.ProcedureInParm("PPIC_DOC",DbSession.DbType.VARCHAR2,null);
			db.ProcedureInParm("PCAST_PIC_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,ViCommConst.DEFAULT_CAST_PIC_ATTR_TYPE_SEQ.ToString());
			db.ProcedureInParm("PCAST_PIC_ATTR_SEQ",DbSession.DbType.VARCHAR2,ViCommConst.DEFAULT_CAST_PIC_ATTR_SEQ.ToString());
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	private void SetPicPostion(Decimal pPicSeq,int pMove) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SET_PIC_POSITION");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,ViewState["SITE_CD"].ToString());
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.NUMBER,userObjs.cast.userSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,ViewState["CAST_CHAR_NO"].ToString());  // modify UCN
			db.ProcedureInParm("PPIC_SEQ",DbSession.DbType.NUMBER,pPicSeq);
			db.ProcedureInParm("PPIC_TYPE",DbSession.DbType.NUMBER,ViCommConst.ATTACHED_PROFILE);
			db.ProcedureInParm("PMOVE_POS",DbSession.DbType.NUMBER,pMove);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		Server.Transfer(string.Format("CastPicMainte.aspx?sitecd={0}&castcharno={1}",ViewState["SITE_CD"].ToString(),ViewState["CAST_CHAR_NO"].ToString()));
	}

	protected Color GetBorderColor(object pProfileFlag) {
		if (pProfileFlag.ToString().Equals("1")) {
			return Color.Blue;
		} else {
			return Color.Empty;
		}
	}

	protected void dsCastPic_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = ViewState["SITE_CD"].ToString();
		e.InputParameters[1] = userObjs.cast.userSeq;
		e.InputParameters[2] = ViewState["CAST_CHAR_NO"].ToString();
		e.InputParameters[3] = ViCommConst.ATTACHED_PROFILE.ToString();
	}

	private void UploadPicture() {
		string sWebPhisicalDir = "";
		string sDomain = "";
		using (Site oSite = new Site()) {
			oSite.GetValue(ViewState["SITE_CD"].ToString(),"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
			oSite.GetValue(ViewState["SITE_CD"].ToString(),"HOST_NM",ref sDomain);
		}


		string sFileNm = "",sPath = "",sFullPath = "";
		decimal dNo = 0;

		if (uldCastPic.HasFile) {

			using (CastPic objPic = new CastPic()) {
				dNo = objPic.GetPicNo();
			}

			sFileNm = ViCommConst.PIC_HEADER + iBridUtil.addZero(dNo.ToString(),ViCommConst.OBJECT_NM_LENGTH);
			sPath = sWebPhisicalDir + ViCommConst.PIC_DIRECTRY + "\\" + ViewState["SITE_CD"].ToString() + "\\Operator\\" + userObjs.cast.loginId;
			sFullPath = sPath + "\\" + sFileNm + ViCommConst.PIC_FOODER;

			using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
				if (!System.IO.File.Exists(sFullPath)) {
					uldCastPic.SaveAs(sFullPath);

					using (Bitmap bmSrc = new Bitmap(sFullPath)) {

						int iWidth = bmSrc.Width / 2;
						int iHight = bmSrc.Height / 2;

						using (Bitmap bmDest = new Bitmap(bmSrc,iWidth,iHight)) {
							string sSmallPic = sWebPhisicalDir +
										ViCommConst.PIC_DIRECTRY + "\\" + ViewState["SITE_CD"].ToString() + "\\Operator\\" + userObjs.cast.loginId + "\\" +
										sFileNm +
										ViCommConst.PIC_FOODER_SMALL;

							bmDest.Save(sSmallPic,ImageFormat.Jpeg);
						}
					}
					string sInputDir = ConfigurationManager.AppSettings["MailParseDir"] + "\\";
					uldCastPic.SaveAs(Path.Combine(sInputDir, sFileNm + ViCommConst.PIC_FOODER));
					ImageHelper.ConvertMobileImage(ViewState["SITE_CD"].ToString(), sInputDir, sPath, sFileNm);

					using (DbSession db = new DbSession()) {
						db.PrepareProcedure("CAST_PIC_UPLOAD");
						db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,ViewState["SITE_CD"]);
						db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.NUMBER,userObjs.cast.userSeq);
						db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,ViewState["CAST_CHAR_NO"].ToString()); // modify UCN
						db.ProcedureInParm("PPIC_SEQ",DbSession.DbType.NUMBER,dNo);
						db.ProcedureInParm("PPIC_TYPE",DbSession.DbType.NUMBER,ViCommConst.ATTACHED_PROFILE);
						db.ProcedureInParm("PPIC_TITLE",DbSession.DbType.VARCHAR2,"");
						db.ProcedureInParm("PPIC_DOC",DbSession.DbType.VARCHAR2,"");
						db.ProcedureInParm("POBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
						db.ProcedureInParm("PCAST_PIC_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,ViCommConst.DEFAULT_CAST_PIC_ATTR_TYPE_SEQ.ToString());
						db.ProcedureInParm("PCAST_PIC_ATTR_SEQ",DbSession.DbType.VARCHAR2,ViCommConst.DEFAULT_CAST_PIC_ATTR_SEQ.ToString());
						db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
						db.ExecuteProcedure();
					}
				}
			}
			ImageHelper.SetCopyRight(ViewState["SITE_CD"].ToString(),sPath,sFileNm + ViCommConst.PIC_FOODER);
		}
	}

	private void DeletePicture(string pFileSeq) {
		string sFileNm = "";
		string sWebPhisicalDir = "";
		using (Site oSite = new Site()) {
			oSite.GetValue(ViewState["SITE_CD"].ToString(),"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
		}

		sFileNm = iBridUtil.addZero(pFileSeq,ViCommConst.OBJECT_NM_LENGTH);

		string sFullPath = sWebPhisicalDir +
							ViCommConst.PIC_DIRECTRY + "\\" + ViewState["SITE_CD"].ToString() + "\\Operator\\" + userObjs.cast.loginId + "\\" +
							sFileNm + ViCommConst.PIC_FOODER;
		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
			if (System.IO.File.Exists(sFullPath)) {
				System.IO.File.Delete(sFullPath);
			}


			sFullPath = sWebPhisicalDir +
								ViCommConst.PIC_DIRECTRY + "\\" + ViewState["SITE_CD"].ToString() + "\\Operator\\" + userObjs.cast.loginId + "\\" +
								sFileNm + ViCommConst.PIC_FOODER_SMALL;

			if (System.IO.File.Exists(sFullPath)) {
				System.IO.File.Delete(sFullPath);
			}
		}
		UpdateProfilePicture(pFileSeq,"1","");
	}


	private void FTPUpload(string pFileSeq) {
		string sServerType;
		using (Sys oSys = new Sys()) {
			oSys.GetValue("DECOMAIL_SERVER_TYPE",out sServerType);
		}
		if (!sServerType.Equals(ViCommConst.DECO_SRV_OLD)) {
			return;
		}

		string sWebPhisicalDir = "";
		string sMailerIP = "";
		string sMailerFtpId = "";
		string sMailerFtpPw = "";
		string sFileNm;
		using (Site oSite = new Site()) {
			oSite.GetValue(ViewState["SITE_CD"].ToString(),"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
			oSite.GetValue(ViewState["SITE_CD"].ToString(),"TX_HI_MAILER_IP",ref sMailerIP);
			oSite.GetValue(ViewState["SITE_CD"].ToString(),"MAILER_FTP_ID",ref sMailerFtpId);
			oSite.GetValue(ViewState["SITE_CD"].ToString(),"MAILER_FTP_PW",ref sMailerFtpPw);
		}

		sFileNm = iBridUtil.addZero(pFileSeq,ViCommConst.OBJECT_NM_LENGTH);

		string sFullPath = sWebPhisicalDir +
							ViCommConst.PIC_DIRECTRY + "\\" + ViewState["SITE_CD"].ToString() + "\\Operator\\" + userObjs.cast.loginId + "\\" +
							sFileNm + ViCommConst.PIC_FOODER;
		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
			SysInterface.FTPUpload(sFullPath,sMailerIP,ViewState["SITE_CD"].ToString() + "_" + userObjs.cast.loginId + ViewState["CAST_CHAR_NO"].ToString() + ".jpg",sMailerFtpId,sMailerFtpPw);
		}
	}
}
