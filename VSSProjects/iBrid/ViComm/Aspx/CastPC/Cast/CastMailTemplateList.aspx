<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CastMailTemplateList.aspx.cs" Inherits="Cast_CastMailTemplateList"
	Title="メールテンプレート設定" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="メールテンプレート設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<asp:Panel runat="server" ID="pnlMainte">
		<asp:Label ID="lblHtmlDocSeq" runat="server" Text="" Visible="false"></asp:Label>
		<asp:TextBox ID="txtHtmlDocTitle" runat="server" Visible="false"></asp:TextBox>
		<fieldset class="fieldset">
			<legend>[設定]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 640px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							テンプレート種別
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstMailTemplateType" runat="server" DataSourceID="dsOrgTemplate" DataTextField="MAIL_TITLE" DataValueField="MAIL_TEMPLATE_NO" Width="240px">
							</asp:DropDownList>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnSeek" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click" ValidationGroup="Key" />
				<asp:Button runat="server" ID="btnSeekCancel" Text="戻る" CssClass="seekbutton" OnClick="btnSeekCancel_Click" CausesValidation="False" />
			</asp:Panel>
			<br clear="all" />
			<asp:Panel runat="server" ID="pnlDtl">
				<fieldset class="fieldset">
					<legend>[文章編集]</legend>
					<table border="0" style="width: 640px" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle">
								タイトル</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtMailTitle" runat="server" MaxLength="60" Width="180px"></asp:TextBox>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								文章</td>
							<td class="tdDataStyle">
								$NO_TRANS_START;
								<pin:pinEdit ID="txtHtmlDoc" runat="server" UserMode="HTML" DisplayStyle="Style5" DocumentView="True" HtmlMode="Body" Ruler="False" ReturnMode="BR" SpellMode="Inline"
									ToolbarConfiguration="ToolbarDesigner" Toolbar="T222324272829303536" FormatMode="Classic" BorderWidth="1px" Height="250px" Width="500px" DocumentWidth="230"
									Font-Bold="False" FontSizeType="px" RelativeBaseImageUrl="/" RelativeBaseLinkUrl="/" RelativeImageRoot="/">
								</pin:pinEdit>
								$NO_TRANS_END;
							</td>
						</tr>
					</table>
					<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" OnClientClick='return confirm("更新を行いますか？");' />
					<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" OnClientClick='return confirm("削除を行いますか？");' />
					<asp:Button runat="server" ID="btnCancel" Text="戻る" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
				</fieldset>
			</asp:Panel>
		</fieldset>
	</asp:Panel>
	<fieldset>
		<legend>[メールテンプレート一覧]</legend>
		<table border="1" width="250">
			<tr>
				<td width="240" align="center">
					<asp:Label ID="lblSiteNm" runat="server" Font-Bold="True" Font-Underline="False" ForeColor="#FF8000"></asp:Label>
				</td>
			</tr>
		</table>
		<asp:GridView ID="grdTemplate" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsMailTemplate" AllowSorting="True" SkinID="GridView">
			<Columns>
				<asp:TemplateField HeaderText="タイトル">
					<ItemTemplate>
						<asp:LinkButton ID="lnkTemplate" runat="server" Text='<%# string.Format("{0} {1}",Eval("MAIL_TEMPLATE_NO"),Eval("MAIL_TITLE")) %>' CommandArgument='<%# string.Format("{0}:{1}",Eval("MAIL_TEMPLATE_NO"),Eval("HTML_DOC_SEQ"))  %>'
							OnCommand="lnkTemplate_Command" CausesValidation="False">
						</asp:LinkButton>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:BoundField DataField="DOC" HeaderText="HTML文章">
					<ItemStyle HorizontalAlign="Left" Wrap="True" />
				</asp:BoundField>
			</Columns>
			<PagerSettings Mode="NumericFirstLast" />
		</asp:GridView>
	</fieldset>
	<br />
	<asp:Panel runat="server" ID="pnlHideValue" Visible="false">
		<asp:TextBox ID="txtDecomeTemplateNo" runat="server"></asp:TextBox>
	</asp:Panel>
	<asp:ObjectDataSource ID="dsOrgTemplate" runat="server" SelectMethod="GetCastCreateOriginal" TypeName="MailTemplate" OnSelecting="dsOrgTemplate_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSiteHtmlDoc" runat="server" SelectMethod="GetListByDocSeq" TypeName="SiteHtmlDoc" OnSelecting="dsSiteHtmlDoc_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pHtmlDocSeq" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsMailTemplate" runat="server" SelectMethod="GetPageCollection" TypeName="CastMailTemplate" SelectCountMethod="GetPageCount"
		EnablePaging="True" OnSelecting="dsMailTemplate_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
			<asp:Parameter Name="pMailTemplateType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>
