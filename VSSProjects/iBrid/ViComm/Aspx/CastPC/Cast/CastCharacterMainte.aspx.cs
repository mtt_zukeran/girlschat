﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: 出演者キャラクター設定
--	Progaram ID		: CastCharacterMainte
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class Cast_CastCharacterMainte:System.Web.UI.Page {
	private SessionObjs userObjs;

	protected void Page_Load(object sender,EventArgs e) {
		userObjs = (SessionObjs)Session["objs"];
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.DOCOMO);
		if (!IsPostBack) {
			InitPage();
		}
		if (userObjs.site.GetOne(iBridUtil.GetStringValue(ViewState["SITE_CD"]))) {
			lblSiteNm.Text = userObjs.site.siteNm;
			// MUTLI_CHAR_FLG対応




			if (userObjs.site.IsActiveMulitCharFlag(ViewState["SITE_CD"].ToString())) {
				lblSiteNm.Text += string.Concat("-",ViewState["CAST_CHAR_NO"].ToString());
			}
			if (userObjs.site.useOtherSysInfoFlag.Equals("1")) {
				plcOfflineMailRx.Visible = false;
			} else {
				plcOfflineMailRx.Visible = true;
			}
			if (userObjs.site.castPcModifyNaFlag == ViCommConst.FLAG_ON) {
				ModifyNa();
			}
		}
	}

	private void SetViewState() {
		ViewState["SITE_CD"] = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		ViewState["CAST_CHAR_NO"] = iBridUtil.GetStringValue(Request.QueryString["castcharno"]);
	}

	private void InitPage() {
		SetViewState();
		ClearField();
		DataBind();
		CreateBirthList();
		GetData();
		SetBirthDay();

		//キャストが営業用誕生日を使用する場合、生年月日欄を表示する
		using (ManageCompany oCompany = new ManageCompany()) {
			plcBirthDay.Visible = oCompany.IsAvailableService(ViCommConst.RELEASE_CAST_USE_DUMMY_BIRTHDAY);
		}
	}

	private void ClearField() {
		lstActCategorySeq.SelectedIndex = 0;
		lblCastNm.Text = "";
		txtHandleNm.Text = "";
		txtHandleKanaNm.Text = "";
		txtCommentList.Text = "";
		txtCommentDetail.Text = "";
		txtWaitingComment.Text = string.Empty;
		txtBirthDay.Text = "";
		txtPriority.Text = "";
		txtStartPerformDay.Text = "";
		chkNaFlag.Checked = false;
		chkOfflineDisplayFlag.Checked = false;
		if (lstLoginMailTemplateNo.Items.Count > 0) {
			lstLoginMailTemplateNo.SelectedIndex = 0;
		}
		if (lstLoginMailTemplateNo2.Items.Count > 0) {
			lstLoginMailTemplateNo2.SelectedIndex = 0;
		}
		if (lstLogoutMailTemplateNo.Items.Count > 0) {
			lstLogoutMailTemplateNo.SelectedIndex = 0;
		}
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetData();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		Server.Transfer("MainView.aspx");
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_CHARACTER_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,ViewState["SITE_CD"].ToString());
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.NUMBER,userObjs.cast.userSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,ViewState["CAST_CHAR_NO"].ToString()); // modify UCN
			db.ProcedureOutParm("PLOGIN_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCAST_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PACT_CATEGORY_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCONNECT_TYPE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCHARACTER_ONLINE_STATUS",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PHANDLE_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PHANDLE_KANA_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCOMMENT_LIST",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCOMMENT_DETAIL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCOMMENT_ADMIN",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PWAITING_COMMENT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPRIORITY",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PLAST_ACTION_DATE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTART_PERFORM_DAY",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PBIRTHDAY",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("POK_PLAY_MASK",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("POFFLINE_DISPLAY_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PLOGIN_MAIL_TEMPALTE_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PLOGIN_MAIL_TEMPALTE_NO2",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PLOGOUT_MAIL_TEMPALTE_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMAN_MAIL_RX_TYPE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PINFO_MAIL_RX_TYPE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMOBILE_MAIL_RX_START_TIME",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMOBILE_MAIL_RX_END_TIME",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PNA_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PFRIEND_INTRO_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PINTRODUCER_FRIEND_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCAST_TO_MAN_BATCH_MAIL_NA_FLG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCAST_TO_MAN_BATCH_MAIL_LIMIT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCAST_TO_MAN_BATCH_MAIL_LIMITS",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PBATCH_MAIL_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PINVITE_MAIL_LIMIT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PINVITE_MAIL_LIMITS",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PINVITE_MAIL_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PAD_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PUSER_DEFINE_MASK",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);

			db.ProcedureOutArrayParm("POK_PLAY_NM",DbSession.DbType.VARCHAR2,ViCommConst.MAX_PLAY_COUNT);
			db.ProcedureOutParm("POK_PLAY_RECORD_COUNT",DbSession.DbType.NUMBER);

			db.ProcedureOutArrayParm("PCAST_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,ViCommConst.MAX_ATTR_COUNT);
			db.ProcedureOutArrayParm("PCAST_ATTR_TYPE_NM",DbSession.DbType.VARCHAR2,ViCommConst.MAX_ATTR_COUNT);
			db.ProcedureOutArrayParm("PATTR_INPUT_TYPE",DbSession.DbType.VARCHAR2,ViCommConst.MAX_ATTR_COUNT);
			db.ProcedureOutArrayParm("PCAST_ATTR_SEQ",DbSession.DbType.VARCHAR2,ViCommConst.MAX_ATTR_COUNT);
			db.ProcedureOutArrayParm("PATTR_INPUT_VALUE",DbSession.DbType.VARCHAR2,ViCommConst.MAX_ATTR_COUNT);
			db.ProcedureOutArrayParm("PATTR_ROWID",DbSession.DbType.VARCHAR2,ViCommConst.MAX_ATTR_COUNT);

			db.ProcedureOutParm("PATTR_RECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);

			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");
			ViewState["ATTR_RECORD_COUNT"] = db.GetStringValue("PATTR_RECORD_COUNT");
			ViewState["OK_PLAY_RECORD_COUNT"] = db.GetStringValue("POK_PLAY_RECORD_COUNT");
			ViewState["CHARACTER_ONLINE_STATUS"] = db.GetStringValue("PCHARACTER_ONLINE_STATUS");
			ViewState["CAST_TO_MAN_BATCH_MAIL_LIMIT"] = db.GetStringValue("PCAST_TO_MAN_BATCH_MAIL_LIMIT");
			ViewState["CAST_TO_MAN_BATCH_MAIL_NA_FLG"] = db.GetStringValue("PCAST_TO_MAN_BATCH_MAIL_NA_FLG");
			ViewState["BATCH_MAIL_COUNT"] = db.GetStringValue("PBATCH_MAIL_COUNT");
			ViewState["INVITE_MAIL_LIMIT"] = db.GetStringValue("PINVITE_MAIL_LIMIT");
			ViewState["INVITE_MAIL_LIMITS"] = db.GetStringValue("PINVITE_MAIL_LIMITS");
			ViewState["INVITE_MAIL_COUNT"] = db.GetStringValue("PINVITE_MAIL_COUNT");
			ViewState["USER_DEFINE_MASK"] = db.GetStringValue("PUSER_DEFINE_MASK");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				lblCastNm.Text = db.GetStringValue("PCAST_NM");
				txtHandleNm.Text = db.GetStringValue("PHANDLE_NM");
				txtHandleKanaNm.Text = db.GetStringValue("PHANDLE_KANA_NM");
				txtCommentList.Text = db.GetStringValue("PCOMMENT_LIST");
				txtCommentDetail.Text = db.GetStringValue("PCOMMENT_DETAIL");
				ViewState["COMMENT_ADMIN"] = db.GetStringValue("PCOMMENT_ADMIN");
				txtWaitingComment.Text = db.GetStringValue("PWAITING_COMMENT");
				txtPriority.Text = db.GetStringValue("PPRIORITY");
				txtStartPerformDay.Text = db.GetStringValue("PSTART_PERFORM_DAY");
				txtBirthDay.Text = db.GetStringValue("PBIRTHDAY");
				lstActCategorySeq.SelectedValue = db.GetStringValue("PACT_CATEGORY_SEQ");
				lstConnectType.SelectedValue = db.GetStringValue("PCONNECT_TYPE");
				chkNaFlag.Checked = (db.GetStringValue("PNA_FLAG").Equals("1"));
				lblFriendIntroCd.Text = db.GetStringValue("PFRIEND_INTRO_CD");
				ViewState["AD_CD"] = db.GetStringValue("PAD_CD");

				if (!db.GetStringValue("PLOGIN_MAIL_TEMPALTE_NO").Equals("")) {
					lstLoginMailTemplateNo.SelectedValue = db.GetStringValue("PLOGIN_MAIL_TEMPALTE_NO");
				}
				if (!db.GetStringValue("PLOGIN_MAIL_TEMPALTE_NO2").Equals("")) {
					lstLoginMailTemplateNo2.SelectedValue = db.GetStringValue("PLOGIN_MAIL_TEMPALTE_NO2");
				}
				if (!db.GetStringValue("PLOGOUT_MAIL_TEMPALTE_NO").Equals("")) {
					lstLogoutMailTemplateNo.SelectedValue = db.GetStringValue("PLOGOUT_MAIL_TEMPALTE_NO");
				}
				if (!db.GetStringValue("PMAN_MAIL_RX_TYPE").Equals("")) {
					lstManMailRxType.SelectedValue = db.GetStringValue("PMAN_MAIL_RX_TYPE");
				}
				if (!db.GetStringValue("PINFO_MAIL_RX_TYPE").Equals("")) {
					lstInfoMailRxType.SelectedValue = db.GetStringValue("PINFO_MAIL_RX_TYPE");
				}
				if (!db.GetStringValue("PMOBILE_MAIL_RX_START_TIME").Equals("")) {
					lstMobileMailRxStartTime.SelectedValue = db.GetStringValue("PMOBILE_MAIL_RX_START_TIME");
				}
				if (!db.GetStringValue("PMOBILE_MAIL_RX_END_TIME").Equals("")) {
					lstMobileMailRxEndTime.SelectedValue = db.GetStringValue("PMOBILE_MAIL_RX_END_TIME");
				}
			} else {
				ClearField();
				lblCastNm.Text = db.GetStringValue("PCAST_NM");
				txtPriority.Text = db.GetStringValue("PPRIORITY");
			}
			db.conn.Close();

			if (db.GetIntValue("POK_PLAY_RECORD_COUNT") != 0) {
				Int64 iOkMask = Int64.Parse(db.GetStringValue("POK_PLAY_MASK"));
				Int64 iMaskValue = 1;
				for (int i = 0;i < db.GetIntValue("POK_PLAY_RECORD_COUNT");i++) {
					CheckBox chkOk = (CheckBox)plcOkList.FindControl(string.Format("chkOKFlag{0}",i)) as CheckBox;
					chkOk.Text = db.GetArryStringValue("POK_PLAY_NM",i);
					if ((iOkMask & iMaskValue) != 0) {
						chkOk.Checked = true;
					}
					iMaskValue = iMaskValue << 1;
				}
				for (int i = db.GetIntValue("POK_PLAY_RECORD_COUNT");i < ViCommConst.MAX_PLAY_COUNT;i++) {
					CheckBox chkOk = (CheckBox)plcOkList.FindControl(string.Format("chkOKFlag{0}",i)) as CheckBox;
					chkOk.Visible = false;
				}
			} else {
				plcOkList.Visible = false;
			}

			using (CastAttrTypeValue oCastAttrTypeValue = new CastAttrTypeValue())
			using (CastAttrType oCastAttrType = new CastAttrType()) {
				for (int i = 0;i < db.GetIntValue("PATTR_RECORD_COUNT");i++) {
					Label lblCastAttrNm = (Label)plcHolder.FindControl(string.Format("lblCastAttrNm{0}",i)) as Label;
					lblCastAttrNm.Text = db.GetArryStringValue("PCAST_ATTR_TYPE_NM",i);

					DropDownList lstCastAttrSeq = (DropDownList)plcHolder.FindControl(string.Format("lstCastAttrSeq{0}",i)) as DropDownList;
					TextBox txtInputValue = (TextBox)plcHolder.FindControl(string.Format("txtAttrInputValue{0}",i)) as TextBox;
					DataSet ds = oCastAttrTypeValue.GetList(ViewState["SITE_CD"].ToString(),db.GetArryStringValue("PCAST_ATTR_TYPE_SEQ",i));
					foreach (DataRow dr in ds.Tables[0].Rows) {
						lstCastAttrSeq.Items.Add(new ListItem(dr["CAST_ATTR_NM"].ToString(),dr["CAST_ATTR_SEQ"].ToString()));
					}

					if (db.GetArryStringValue("PATTR_INPUT_TYPE",i).Equals(ViCommConst.INPUT_TYPE_TEXT)) {
						txtInputValue.Text = db.GetArryStringValue("PATTR_INPUT_VALUE",i);
						lstCastAttrSeq.Visible = false;
					} else {
						if (db.GetArryStringValue("PCAST_ATTR_SEQ",i) != "") {
							lstCastAttrSeq.SelectedValue = db.GetArryStringValue("PCAST_ATTR_SEQ",i);
						}
						txtInputValue.Visible = false;
					}

					ViewState["ATTR_ROWID" + i.ToString()] = db.GetArryStringValue("PATTR_ROWID",i);
					ViewState["CAST_ATTR_TYPE_SEQ" + i.ToString()] = db.GetArryStringValue("PCAST_ATTR_TYPE_SEQ",i);

					if (oCastAttrType.GetOne(ViewState["SITE_CD"].ToString(),db.GetArryStringValue("PCAST_ATTR_TYPE_SEQ",i))) {
						TableRow rowAttr = (TableRow)plcHolder.FindControl(string.Format("rowAttr{0}",i)) as TableRow;
						if (oCastAttrType.naFlag) {
							rowAttr.Visible = false;
						} else {
							rowAttr.Visible = true;
						}
					}
				}

				for (int i = db.GetIntValue("PATTR_RECORD_COUNT");i < ViCommConst.MAX_ATTR_COUNT;i++) {
					TableCell cel;
					cel = (TableCell)plcHolder.FindControl(string.Format("celCastAttrNm{0}",i)) as TableCell;
					cel.Visible = false;
					cel = (TableCell)plcHolder.FindControl(string.Format("celCastAttrSeq{0}",i)) as TableCell;
					cel.Visible = false;
				}
			}

		}
		pnlDtl.Visible = true;
	}

	private void UpdateData(int pDelFlag) {
		string sHandelNm = Mobile.EmojiToCommTag(ViCommConst.DOCOMO,txtHandleNm.Text);
		string sHandelNmKana = Mobile.EmojiToCommTag(ViCommConst.DOCOMO,txtHandleKanaNm.Text);
		string sList = Mobile.EmojiToCommTag(ViCommConst.DOCOMO,txtCommentList.Text.Replace("\r\n",""));
		string sDetail = Mobile.EmojiToCommTag(ViCommConst.DOCOMO,txtCommentDetail.Text.Replace("\r\n",""));
		string sWaiting = Mobile.EmojiToCommTag(ViCommConst.DOCOMO,txtWaitingComment.Text.Replace("\r\n",string.Empty));

		string sBirthDay;
		if (lstBirthYear.SelectedValue.Equals(string.Empty) && lstBirthMonth.SelectedValue.Equals(string.Empty) && lstBirthDay.SelectedValue.Equals(string.Empty)) {
			sBirthDay = string.Empty;
		} else {
			sBirthDay = lstBirthYear.SelectedValue + "/" + lstBirthMonth.SelectedValue + "/" + lstBirthDay.SelectedValue;
		}

		Int64 iOkMask = 0;
		Int64 iMaskValue = 1;
		int iMaskCount = int.Parse(ViewState["OK_PLAY_RECORD_COUNT"].ToString());
		for (int i = 0;i < iMaskCount;i++) {
			CheckBox chkOk = (CheckBox)plcOkList.FindControl(string.Format("chkOKFlag{0}",i)) as CheckBox;
			if (chkOk.Checked) {
				iOkMask = iOkMask + iMaskValue;
			}
			iMaskValue = iMaskValue << 1;
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_CHARACTER_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,ViewState["SITE_CD"].ToString());
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.NUMBER,userObjs.cast.userSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,ViewState["CAST_CHAR_NO"].ToString());
			db.ProcedureInParm("PACT_CATEGORY_SEQ",DbSession.DbType.VARCHAR2,lstActCategorySeq.SelectedValue);
			db.ProcedureInParm("PCONNECT_TYPE",DbSession.DbType.VARCHAR2,lstConnectType.SelectedValue);
			db.ProcedureInParm("PCHARACTER_ONLINE_STATUS",DbSession.DbType.VARCHAR2,ViewState["CHARACTER_ONLINE_STATUS"].ToString());
			db.ProcedureInParm("PHANDLE_NM",DbSession.DbType.VARCHAR2,sHandelNm);
			db.ProcedureInParm("PHANDLE_KANA_NM",DbSession.DbType.VARCHAR2,sHandelNmKana);
			db.ProcedureInParm("PCOMMENT_LIST",DbSession.DbType.VARCHAR2,sList);
			db.ProcedureInParm("PCOMMENT_DETAIL",DbSession.DbType.VARCHAR2,sDetail);
			db.ProcedureInParm("PCOMMENT_ADMIN",DbSession.DbType.VARCHAR2,ViewState["COMMENT_ADMIN"]);
			db.ProcedureInParm("PWAITING_COMMENT",DbSession.DbType.VARCHAR2,sWaiting);
			db.ProcedureInParm("PPRIORITY",DbSession.DbType.NUMBER,decimal.Parse(txtPriority.Text));
			db.ProcedureInParm("PSTART_PERFORM_DAY",DbSession.DbType.VARCHAR2,txtStartPerformDay.Text);
			db.ProcedureInParm("PBIRTHDAY",DbSession.DbType.VARCHAR2,sBirthDay);
			db.ProcedureInParm("POK_PLAY_MASK",DbSession.DbType.NUMBER,iOkMask);
			db.ProcedureInParm("POFFLINE_DISPLAY_FLAG",DbSession.DbType.NUMBER,chkOfflineDisplayFlag.Checked);
			db.ProcedureInParm("PLOGIN_MAIL_TEMPALTE_NO",DbSession.DbType.VARCHAR2,lstLoginMailTemplateNo.SelectedValue);
			db.ProcedureInParm("PLOGIN_MAIL_TEMPALTE_NO2",DbSession.DbType.VARCHAR2,lstLoginMailTemplateNo2.SelectedValue);
			db.ProcedureInParm("PLOGOUT_MAIL_TEMPALTE_NO",DbSession.DbType.VARCHAR2,lstLogoutMailTemplateNo.SelectedValue);
			db.ProcedureInParm("PMAN_MAIL_RX_TYPE",DbSession.DbType.VARCHAR2,lstManMailRxType.SelectedValue);
			db.ProcedureInParm("PINFO_MAIL_RX_TYPE",DbSession.DbType.VARCHAR2,lstInfoMailRxType.SelectedValue);
			db.ProcedureInParm("PMOBILE_MAIL_RX_START_TIME",DbSession.DbType.VARCHAR2,lstMobileMailRxStartTime.SelectedValue);
			db.ProcedureInParm("PMOBILE_MAIL_RX_END_TIME",DbSession.DbType.VARCHAR2,lstMobileMailRxEndTime.SelectedValue);
			db.ProcedureInParm("PNA_FLAG",DbSession.DbType.NUMBER,chkNaFlag.Checked);
			db.ProcedureInParm("PFRIEND_INTRO_CD",DbSession.DbType.VARCHAR2,lblFriendIntroCd.Text);
			db.ProcedureInParm("pCAST_TO_MAN_BATCH_MAIL_NA_FLG",DbSession.DbType.NUMBER,int.Parse(ViewState["CAST_TO_MAN_BATCH_MAIL_NA_FLG"].ToString()));
			db.ProcedureInParm("PCAST_TO_MAN_BATCH_MAIL_LIMIT",DbSession.DbType.NUMBER,int.Parse(ViewState["CAST_TO_MAN_BATCH_MAIL_LIMIT"].ToString()));
			db.ProcedureInParm("PBATCH_MAIL_COUNT",DbSession.DbType.NUMBER,int.Parse(ViewState["BATCH_MAIL_COUNT"].ToString()));
			db.ProcedureInParm("PINVITE_MAIL_LIMIT",DbSession.DbType.NUMBER,int.Parse(ViewState["INVITE_MAIL_LIMIT"].ToString()));
			db.ProcedureInParm("PINVITE_MAIL_COUNT",DbSession.DbType.NUMBER,int.Parse(ViewState["INVITE_MAIL_COUNT"].ToString()));
			db.ProcedureInParm("PAD_CD",DbSession.DbType.VARCHAR2,ViewState["AD_CD"].ToString());
			db.ProcedureInParm("PUSER_DEFINE_MASK",DbSession.DbType.NUMBER,int.Parse(ViewState["USER_DEFINE_MASK"].ToString()));
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);

			int iRecCount = int.Parse(ViewState["ATTR_RECORD_COUNT"].ToString());

			string[] sCastAttrTypeSeq = new string[ViCommConst.MAX_ATTR_COUNT];
			string[] sCastAttrSeq = new string[ViCommConst.MAX_ATTR_COUNT];
			string[] sAttrInputValue = new string[ViCommConst.MAX_ATTR_COUNT];
			string[] sAttrRowID = new string[ViCommConst.MAX_ATTR_COUNT];

			for (int i = 0;i < iRecCount;i++) {
				DropDownList lstCastAttrSeq = (DropDownList)plcHolder.FindControl(string.Format("lstCastAttrSeq{0}",i)) as DropDownList;
				sCastAttrSeq[i] = lstCastAttrSeq.SelectedValue;

				TextBox txtInputValue = (TextBox)plcHolder.FindControl(string.Format("txtAttrInputValue{0}",i)) as TextBox;
				sAttrInputValue[i] = txtInputValue.Text;

				sCastAttrTypeSeq[i] = (string)ViewState["CAST_ATTR_TYPE_SEQ" + i.ToString()];
				sAttrRowID[i] = (string)ViewState["ATTR_ROWID" + i.ToString()];
			}

			db.ProcedureInArrayParm("PCAST_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,iRecCount,sCastAttrTypeSeq);
			db.ProcedureInArrayParm("PCAST_ATTR_SEQ",DbSession.DbType.VARCHAR2,iRecCount,sCastAttrSeq);
			db.ProcedureInArrayParm("PATTR_INPUT_VALUE",DbSession.DbType.VARCHAR2,iRecCount,sAttrInputValue);
			db.ProcedureInArrayParm("PATTR_ROWID",DbSession.DbType.VARCHAR2,iRecCount,sAttrRowID);

			db.ProcedureInParm("PATTR_RECORD_COUNT",DbSession.DbType.NUMBER,iRecCount);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);

			db.ExecuteProcedure();
		}

		string sWebPhisicalDir = "";
		using (Site oSite = new Site()) {
			oSite.GetValue(ViewState["SITE_CD"].ToString(),"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
		}

		if (pDelFlag == 0) {
			string sDir = "";
			using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
				sDir = sWebPhisicalDir + ViCommConst.PIC_DIRECTRY + "\\" + ViewState["SITE_CD"].ToString() + string.Format("\\Operator\\{0}",userObjs.cast.loginId);
				Directory.CreateDirectory(sDir);

				sDir = sWebPhisicalDir + ViCommConst.MOVIE_DIRECTRY + "\\" + ViewState["SITE_CD"].ToString() + string.Format("\\Operator\\{0}",userObjs.cast.loginId);
				Directory.CreateDirectory(sDir);
			}
		}

		Server.Transfer("MainView.aspx");
	}

	private void ModifyNa() {
		txtHandleNm.Enabled = false;
		lstLoginMailTemplateNo.Enabled = false;
		lstLoginMailTemplateNo2.Enabled = false;
		lstLogoutMailTemplateNo.Enabled = false;
		lstManMailRxType.Enabled = false;
		lstInfoMailRxType.Enabled = false;
		lstMobileMailRxStartTime.Enabled = false;
		lstMobileMailRxEndTime.Enabled = false;
		for (int i = 0;i < ViCommConst.MAX_PLAY_COUNT;i++) {
			CheckBox chkOk = (CheckBox)plcOkList.FindControl(string.Format("chkOKFlag{0}",i)) as CheckBox;
			chkOk.Enabled = false;
		}
		for (int i = 0;i < ViCommConst.MAX_ATTR_COUNT;i++) {
			DropDownList lstCastAttrSeq = (DropDownList)plcHolder.FindControl(string.Format("lstCastAttrSeq{0}",i)) as DropDownList;
			lstCastAttrSeq.Enabled = false;

			TextBox txtInputValue = (TextBox)plcHolder.FindControl(string.Format("txtAttrInputValue{0}",i)) as TextBox;
			txtInputValue.Enabled = false;
		}
		lstBirthYear.Enabled = false;
		lstBirthMonth.Enabled = false;
		lstBirthDay.Enabled = false;
	}

	private void CreateBirthList() {
		int iYear = DateTime.Today.Year - 18;

		lstBirthYear.Items.Add(new ListItem("",""));
		for (int i = 0;i < 82;i++) {
			lstBirthYear.Items.Add(new ListItem(iYear.ToString("d4"),iYear.ToString("d4")));
			iYear -= 1;
		}
		lstBirthMonth.Items.Add(new ListItem("",""));
		for (int i = 1;i <= 12;i++) {
			lstBirthMonth.Items.Add(new ListItem(i.ToString("d2"),i.ToString("d2")));
		}
		lstBirthDay.Items.Add(new ListItem("",""));
		for (int i = 1;i <= 31;i++) {
			lstBirthDay.Items.Add(new ListItem(i.ToString("d2"),i.ToString("d2")));
		}
	}

	private void SetBirthDay() {
		if (txtBirthDay.Text.Equals(string.Empty)) {
			lstBirthYear.SelectedValue = "";
			lstBirthMonth.SelectedValue = "";
			lstBirthDay.SelectedValue = "";
		} else {
			lstBirthYear.SelectedValue = txtBirthDay.Text.Substring(0,4);
			lstBirthMonth.SelectedValue = txtBirthDay.Text.Substring(5,2);
			lstBirthDay.SelectedValue = txtBirthDay.Text.Substring(8,2);
		}
	}

	protected void vdcBirthday_ServerValidate(object source,ServerValidateEventArgs args) {
		if (lstBirthYear.SelectedValue.Equals(string.Empty) && lstBirthMonth.SelectedValue.Equals(string.Empty) && lstBirthDay.SelectedValue.Equals(string.Empty)) {
			txtBirthDay.Text = "";
			args.IsValid = true;
			return;
		} else {
			try {
				DateTime dtBiathday = new DateTime(int.Parse(lstBirthYear.SelectedValue),int.Parse(lstBirthMonth.SelectedValue),int.Parse(lstBirthDay.SelectedValue));
				txtBirthDay.Text = dtBiathday.ToString("yyyy/MM/dd");
			} catch {
				args.IsValid = false;
				return;
			}
		}

		if (args.IsValid) {
			int iOkAge;
			int.TryParse(iBridUtil.GetStringValue(ConfigurationManager.AppSettings["RefuseAge"]),out iOkAge);
			if (iOkAge == 0) {
				iOkAge = 18;
			}
			args.IsValid = (ViCommPrograms.Age(txtBirthDay.Text) >= iOkAge);
		}
	}
}
