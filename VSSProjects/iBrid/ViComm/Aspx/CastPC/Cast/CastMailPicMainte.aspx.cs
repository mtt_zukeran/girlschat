﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: 出演者添付メール画像設定
--	Progaram ID		: CastMailPicMainte
--
--  Creation Date	: 2009.09.10
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using iBridCommLib;
using ViComm;

public partial class Cast_CastMailPicMainte:System.Web.UI.Page {
	private SessionObjs userObjs;

	protected void Page_Load(object sender,EventArgs e) {
		userObjs = (SessionObjs)Session["objs"];
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.DOCOMO);
		if (!IsPostBack) {
			InitPage();
		}
		if (userObjs.site.GetOne(iBridUtil.GetStringValue(ViewState["SITE_CD"]))) {
			lblSiteNm.Text = userObjs.site.siteNm;
			if (userObjs.site.IsActiveMulitCharFlag(ViewState["SITE_CD"].ToString())) {
				lblSiteNm.Text += string.Concat("-",ViewState["CAST_CHAR_NO"].ToString());
			}
		}
	}

	private void SetViewState() {
		ViewState["SITE_CD"] = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		ViewState["CAST_CHAR_NO"] = iBridUtil.GetStringValue(Request.QueryString["castcharno"]);
	}

	private void InitPage() {
		SetViewState();
		ClearField();
		DataBind();
	}

	private void ClearField() {
	}

	protected void btnUpload_Click(object sender,EventArgs e) {
		UploadPicture();
		Server.Transfer(string.Format("CastMailPicMainte.aspx?sitecd={0}&userseq={1}&castcharno={2}",ViewState["SITE_CD"].ToString(),userObjs.cast.userSeq,ViewState["CAST_CHAR_NO"].ToString()));  // add UCN
	}

	protected void btnDelete_Command(object sender,CommandEventArgs e) {
		DeletePicture(e.CommandArgument.ToString());
		Server.Transfer(string.Format("CastMailPicMainte.aspx?sitecd={0}&userseq={1}&castcharno={2}",ViewState["SITE_CD"].ToString(),userObjs.cast.userSeq,ViewState["CAST_CHAR_NO"].ToString()));  // add UCN
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		Server.Transfer("MainView.aspx");
	}

	protected void dsCastPic_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = ViewState["SITE_CD"].ToString();
		e.InputParameters[1] = userObjs.cast.userSeq;
		e.InputParameters[2] = ViCommConst.ATTACHED_MAIL.ToString();
		e.InputParameters[3] = ViewState["CAST_CHAR_NO"].ToString();
	}

	private void UploadPicture() {
		string sWebPhisicalDir = "";
		string sDomain = "";

		using (Site oSite = new Site()) {
			oSite.GetValue(ViewState["SITE_CD"].ToString(),"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
			oSite.GetValue(ViewState["SITE_CD"].ToString(),"HOST_NM",ref sDomain);
		}

		string sFileNm = "",sPath = "",sFullPath = "";
		decimal dNo = 0;

		if (uldCastPic.HasFile) {
			using (CastPic objPic = new CastPic()) {
				dNo = objPic.GetPicNo();
			}

			sFileNm = ViCommConst.PIC_HEADER + iBridUtil.addZero(dNo.ToString(),ViCommConst.OBJECT_NM_LENGTH);
			sPath = sWebPhisicalDir + ViCommConst.PIC_DIRECTRY + "\\" + ViewState["SITE_CD"].ToString() + "\\Operator\\" + userObjs.cast.loginId;
			sFullPath = sPath + "\\" + sFileNm + ViCommConst.PIC_FOODER;

			using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
				if (!System.IO.File.Exists(sFullPath)) {
					uldCastPic.SaveAs(sFullPath);

					using (Bitmap bmSrc = new Bitmap(sFullPath)) {

						int iWidth = bmSrc.Width / 2;
						int iHight = bmSrc.Height / 2;

						using (Bitmap bmDest = new Bitmap(bmSrc,iWidth,iHight)) {
							string sSmallPic = sWebPhisicalDir +
										ViCommConst.PIC_DIRECTRY + "\\" + ViewState["SITE_CD"].ToString() + "\\Operator\\" + userObjs.cast.loginId + "\\" +
										sFileNm +
										ViCommConst.PIC_FOODER_SMALL;

							bmDest.Save(sSmallPic,ImageFormat.Jpeg);

						}
					}
					string sInputDir = ConfigurationManager.AppSettings["MailParseDir"] + "\\";
					uldCastPic.SaveAs(Path.Combine(sInputDir, sFileNm + ViCommConst.PIC_FOODER));
					ImageHelper.ConvertMobileImage(ViewState["SITE_CD"].ToString(), sInputDir, sPath, sFileNm);

					using (DbSession db = new DbSession()) {
						db.PrepareProcedure("CAST_PIC_UPLOAD");
						db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,ViewState["SITE_CD"].ToString());
						db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.NUMBER,userObjs.cast.userSeq);
						db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,ViewState["CAST_CHAR_NO"].ToString()); // modify UCN
						db.ProcedureInParm("PPIC_SEQ",DbSession.DbType.NUMBER,dNo);
						db.ProcedureInParm("PPIC_TYPE",DbSession.DbType.NUMBER,ViCommConst.ATTACHED_MAIL);
						db.ProcedureInParm("PPIC_TITLE",DbSession.DbType.VARCHAR2,"");
						db.ProcedureInParm("PPIC_DOC",DbSession.DbType.VARCHAR2,"");
						db.ProcedureInParm("POBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
						db.ProcedureInParm("PCAST_PIC_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,ViCommConst.DEFAULT_CAST_PIC_ATTR_TYPE_SEQ.ToString());
						db.ProcedureInParm("PCAST_PIC_ATTR_SEQ",DbSession.DbType.VARCHAR2,ViCommConst.DEFAULT_CAST_PIC_ATTR_SEQ.ToString());
						db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
						db.ExecuteProcedure();
					}
				}
			}
			ImageHelper.SetCopyRight(ViewState["SITE_CD"].ToString(),sPath,sFileNm + ViCommConst.PIC_FOODER);
		}
	}

	private void DeletePicture(string pFileSeq) {
		UpdatePicture(pFileSeq,"1","0");
	}

	private void UpdatePicture(string pPicSeq,string pDelFlag,string pProfilePicFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_PIC_PROFILE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,ViewState["SITE_CD"].ToString());
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.NUMBER,userObjs.cast.userSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,ViewState["CAST_CHAR_NO"].ToString()); // modify UCN
			db.ProcedureInParm("PPIC_SEQ",DbSession.DbType.NUMBER,pPicSeq);
			db.ProcedureInParm("pDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureInParm("pPROFILE_PIC_FLAG",DbSession.DbType.NUMBER,pProfilePicFlag);
			db.ProcedureInParm("pPIC_TYPE",DbSession.DbType.NUMBER,ViCommConst.ATTACHED_MAIL);
			db.ProcedureInParm("POBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
			db.ProcedureInParm("POBJ_NOT_PUBLISH_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
			db.ProcedureInParm("PPIC_TITLE",DbSession.DbType.VARCHAR2,null);
			db.ProcedureInParm("PPIC_DOC",DbSession.DbType.VARCHAR2,null);
			db.ProcedureInParm("PCAST_PIC_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,ViCommConst.DEFAULT_CAST_PIC_ATTR_TYPE_SEQ.ToString());
			db.ProcedureInParm("PCAST_PIC_ATTR_SEQ",DbSession.DbType.VARCHAR2,ViCommConst.DEFAULT_CAST_PIC_ATTR_SEQ.ToString());
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}
}
