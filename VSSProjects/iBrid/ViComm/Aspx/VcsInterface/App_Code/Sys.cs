﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Vcs Interface
--	Title			: システム設定
--	Progaram ID		: Sys
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class Sys:DbSession {

	public string vcsFtpIp;
	public string vcsFtpId;
	public string vcsFtpPw;
	public string vcsRequestUrl;

	public Sys() {
	}

	public bool GetOne() {
		DataSet ds;
		DataRow dr;
		bool bExist = false;

		conn = DbConnect();

		string sSql = "SELECT " +
						"VCS_REQUEST_URL," +
						"VCS_FTP_IP		," +
						"VCS_FTP_ID		," +
						"VCS_FTP_PW	" +
					"FROM " +
						"T_SYS ";

		using (cmd = new OracleCommand(sSql,conn))
		using (ds = new DataSet()) {

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"T_SYS");
				if (ds.Tables["T_SYS"].Rows.Count != 0) {
					dr = ds.Tables["T_SYS"].Rows[0];
					vcsFtpIp = dr["VCS_FTP_IP"].ToString();
					vcsFtpId = dr["VCS_FTP_ID"].ToString();
					vcsFtpPw = dr["VCS_FTP_PW"].ToString();
					vcsRequestUrl = dr["VCS_REQUEST_URL"].ToString();
					bExist = true;
				}
			}
		}
		conn.Close();
		return bExist;
	}


	public bool GetValue(string pItem,out string pValue) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		pValue = "";
		conn = DbConnect();
		string sSql = "SELECT " +
							"SIP_IVP_LOC_CD		," +
							"SIP_IVP_SITE_CD	," +
							"SIP_REGIST_URL		," +
							"IVP_ADMIN_URL		," +
							"IVP_REQUEST_URL	," +
							"IVP_STARTUP_CAMERA_URL," +
							"LIVE_KEY			," +
							"SIP_DOMAIN			," +
							"VCS_REQUEST_URL	," +
							"IVP_ISSUE_LIVE_KEY_URL " +
						"FROM " +
							"T_SYS ";

		using (cmd = new OracleCommand(sSql,conn))
		using (ds = new DataSet()) {

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"T_SYS");
				if (ds.Tables["T_SYS"].Rows.Count != 0) {
					dr = ds.Tables["T_SYS"].Rows[0];
					pValue = dr[pItem].ToString();
					bExist = true;
				}
			}
		}
		conn.Close();
		return bExist;
	}
}
