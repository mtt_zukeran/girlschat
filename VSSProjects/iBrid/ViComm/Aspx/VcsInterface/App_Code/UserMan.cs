﻿using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Web;
using ViComm;

public class UserMan:DbSession {
	public string siteCd;
	public string userSeq;
	public string userCharNo;
	public int balPoint;
	public int limitPoint;
	public string userStatus;
	public string userRankCd;
	public string adCd;
	public string adGroupCd;

	public UserMan() {
		siteCd = "";
		userSeq = "";
		userCharNo = "";
		balPoint = 0;
		limitPoint = 0;
		userStatus = "";
		userRankCd = "";
		adCd = "";
	}

	public bool GetCurrentInfo(string pSiteCd,string pLoginId) {
		DataSet ds;
		DataRow dr;

		bool bExist = false;

		conn = DbConnect("UserMan.GetCurrentInfo");

		string sSql = "SELECT " +
							"SITE_CD			," +
							"USER_SEQ			," +
							"USER_CHAR_NO		," +
							"USER_RANK			," +
							"USER_STATUS		," +
							"BAL_POINT			," +
							"LIMIT_POINT		," +
							"SERVICE_POINT		," +
							"SERVICE_POINT_EFFECTIVE_DATE," +
							"AD_CD				" +
						"FROM " +
						" VW_USER_MAN_CHARACTER03 " +
						"WHERE " +
						" SITE_CD = :SITE_CD AND LOGIN_ID = :LOGIN_ID AND USER_CHAR_NO = :USER_CHAR_NO";

		using (cmd = new OracleCommand(sSql,conn))
		using (ds = new DataSet()) {

			cmd.Parameters.Add("SITE_CD",pSiteCd);
			cmd.Parameters.Add("USER_SEQ",pLoginId);
			cmd.Parameters.Add("USER_CHAR_NO",ViCommConst.MAIN_CHAR_NO);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"VW_USER_MAN_CHARACTER03");

				if (ds.Tables["VW_USER_MAN_CHARACTER03"].Rows.Count != 0) {
					dr = ds.Tables["VW_USER_MAN_CHARACTER03"].Rows[0];
					siteCd = dr["SITE_CD"].ToString();
					userSeq = dr["USER_SEQ"].ToString();
					userCharNo = dr["USER_CHAR_NO"].ToString();
					balPoint = int.Parse(dr["BAL_POINT"].ToString());
					limitPoint = int.Parse(dr["LIMIT_POINT"].ToString());
					userStatus = dr["USER_STATUS"].ToString();

					if (!dr["SERVICE_POINT_EFFECTIVE_DATE"].ToString().Equals("")) {
						DateTime dtEffect = DateTime.Parse(dr["SERVICE_POINT_EFFECTIVE_DATE"].ToString());
						if (dtEffect > DateTime.Now) {
							balPoint += int.Parse(dr["SERVICE_POINT"].ToString());
						}
					}
					userRankCd = dr["USER_RANK"].ToString();
					adCd = dr["AD_CD"].ToString();
					bExist = true;
				}
			}
		}
		conn.Close();

		return bExist;
	}
}
