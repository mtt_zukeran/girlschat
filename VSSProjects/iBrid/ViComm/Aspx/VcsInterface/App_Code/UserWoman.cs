﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: VcsInterface
--	Title			: サイト--	Progaram ID		: Site
--
--  Creation Date	: 2009.12.22
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Web;
using ViComm;

public class UserWoman:DbSession {
	public string userSeq;
	public string userStatus;
	public string characterOnlineStatus;
	public string useTerminalType;
	public string recSeq;

	public UserWoman() {
		userSeq = "";
		userStatus = "";
		characterOnlineStatus = "";
		useTerminalType = "";
		recSeq = "";
	}

	public bool IsExistCharacter(string pSiteCd,string pLoginID,string pUserCharNo) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;

		conn = DbConnect("UserWoman.IsExistCharacter");
		ds = new DataSet();

		string sSql = "SELECT " +
						" USER_SEQ			, " +
						" USER_STATUS		, " +
						" CHARACTER_ONLINE_STATUS, " +
						" USE_TERMINAL_TYPE	, " +
						" REC_SEQ			 " +
						"FROM " +
						" VW_CAST_CHARACTER00 " +
						"WHERE " +
						" SITE_CD		= :SITE_CD AND	" +
						" LOGIN_ID		= :LOGIN_ID AND	" +
						" USER_CHAR_NO	= :USER_CHAR_NO ";

		using (cmd = new OracleCommand(sSql,conn)) {
			cmd.Parameters.Add("SITE_CD",pSiteCd);
			cmd.Parameters.Add("LOGIN_ID",pLoginID);
			cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"VW_CAST_CHARACTER00");

				if (ds.Tables["VW_CAST_CHARACTER00"].Rows.Count != 0) {
					dr = ds.Tables["VW_CAST_CHARACTER00"].Rows[0];
					userSeq = dr["USER_SEQ"].ToString();
					userStatus = dr["USER_STATUS"].ToString();
					characterOnlineStatus = dr["CHARACTER_ONLINE_STATUS"].ToString();
					useTerminalType = dr["USE_TERMINAL_TYPE"].ToString();
					recSeq = dr["REC_SEQ"].ToString();
					bExist = true;
				}
			}
		}
		conn.Close();
		return bExist;
	}

	public bool GetItemCd(string pSiteCd,string pUserSeq,string pUserCharNo,string pItemNo,out string pItemCd) {
		pItemCd = "";
		DataSet ds;
		DataRow dr;
		bool bExist = false;

		conn = DbConnect("UserWoman.GetItemCd");
		ds = new DataSet();

		string sSql = " SELECT " +
						" ITEM_CD		, " +
						" INPUT_TYPE	" +
						"FROM " +
						" VW_CAST_ATTR_VALUE01 " +
						"WHERE " +
						" SITE_CD			= :SITE_CD			AND	" +
						" USER_SEQ			= :USER_SEQ			AND	" +
						" USER_CHAR_NO		= :USER_CHAR_NO		AND	" +
						" ITEM_NO			= :ITEM_NO			AND	" +
						" INPUT_TYPE	   != :INPUT_TYPE		AND " +
						" NA_FLAG			= :NA_FLAG	";

		using (cmd = new OracleCommand(sSql,conn)) {
			cmd.Parameters.Add("SITE_CD",pSiteCd);
			cmd.Parameters.Add("USER_SEQ",pUserSeq);
			cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
			cmd.Parameters.Add("ITEM_NO",pItemNo);
			cmd.Parameters.Add("INPUT_TYPE",ViCommConst.INPUT_TYPE_TEXT);
			cmd.Parameters.Add("NA_FLAG",ViCommConst.FLAG_OFF);
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"VW_CAST_ATTR_VALUE01");

				if (ds.Tables["VW_CAST_ATTR_VALUE01"].Rows.Count != 0) {
					dr = ds.Tables["VW_CAST_ATTR_VALUE01"].Rows[0];
					pItemCd = dr["ITEM_CD"].ToString();
					bExist = true;
				}
			}
		}
		conn.Close();
		return bExist;
	}

	public bool IsLoginCharacter(string pSiteCd,string pUserSeq,string pUserCharNo) {
		DataSet ds;
		bool bExist = false;

		conn = DbConnect("UserWoman.IsLoginCharacter");
		ds = new DataSet();

		string sSql = "SELECT " +
						" CHARACTER_ONLINE_STATUS " +
						"FROM " +
						" T_CAST_CHARACTER " +
						"WHERE " +
						" SITE_CD		= :SITE_CD AND	" +
						" USER_SEQ		= :USER_SEQ AND	" +
						" USER_CHAR_NO	= :USER_CHAR_NO ";

		using (cmd = new OracleCommand(sSql,conn)) {
			cmd.Parameters.Add("SITE_CD",pSiteCd);
			cmd.Parameters.Add("USER_SEQ",pUserSeq);
			cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"T_CAST_CHARACTER");
				if (ds.Tables["T_CAST_CHARACTER"].Rows.Count != 0) {
					bExist = ds.Tables["T_CAST_CHARACTER"].Rows[0]["CHARACTER_ONLINE_STATUS"].ToString().Equals(ViCommConst.USER_WAITING.ToString());
				}
			}
		}
		conn.Close();
		return bExist;
	}
}
