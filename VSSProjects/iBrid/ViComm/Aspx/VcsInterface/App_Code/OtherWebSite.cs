﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Interface
--	Title			: 他システムWEB情報
--	Progaram ID		: OtherWebSite
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;

public class OtherWebSite:DbSession {
	public string siteNm;
	public string mayPageUrl;
	public string userInfoUrl;
	public string loginUrl;
	public string topPageUlr;

	public OtherWebSite() {
	}

	public bool GetOne(string pOtherWebSiteCd) {
		DataSet ds;
		DataRow dr;

		bool bExist = false;

		conn = DbConnect("OtherWebSite.GetOne");

		string sSql = "SELECT " +
							"MY_PAGE_URL	," +
							"USER_INFO_URL	," +
							"TOP_PAGE_URL	," +
							"LOGIN_URL		," +
							"SITE_NM " +
						"FROM " +
							"T_OTHER_WEB_SITE " +
						"WHERE " +
							"OTHER_WEB_SYS_ID = :OTHER_WEB_SYS_ID ";

		using (cmd = new OracleCommand(sSql,conn))
		using (ds = new DataSet()) {
			cmd.Parameters.Add("OTHER_WEB_SYS_ID",pOtherWebSiteCd);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"T_OTHER_WEB_SITE");

				if (ds.Tables["T_OTHER_WEB_SITE"].Rows.Count != 0) {
					dr = ds.Tables["T_OTHER_WEB_SITE"].Rows[0];

					mayPageUrl = dr["MY_PAGE_URL"].ToString();
					userInfoUrl = dr["USER_INFO_URL"].ToString();
					topPageUlr = dr["TOP_PAGE_URL"].ToString();
					loginUrl = dr["LOGIN_URL"].ToString();
					bExist = true;
				}
			}
		}

		conn.Close();

		return bExist;
	}

}
