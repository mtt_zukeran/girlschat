﻿/*************************************************************************
--	System			: ViCOMM Site
--	Sub System Name	: VCS I/F
--	Title			: 督促Call要求
--	Progaram ID		: RequestUrgeCall
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class RequestUrgeCall:DbSession {


	public RequestUrgeCall() {
	}

	public DataSet GetList(string pRevisionNo) {
		DataSet ds;

		conn = DbConnect();
		ds = new DataSet();

		string sSql = "SELECT " +
						"SITE_CD			," +
						"IVP_REQUEST_SEQ	," +
						"URGE_LEVEL			," +
						"USER_SEQ			," +
						"BILL_AMT			," +
						"TEL				," +
						"LOGIN_ID " +
					"FROM " +
						"T_REQUEST_URGE_CALL " +
					"WHERE " +
						"REVISION_NO = :REVISION_NO " +
					"ORDER BY " +
						"SITE_CD,IVP_REQUEST_SEQ";

		using (cmd = new OracleCommand(sSql,conn)) {
			cmd.Parameters.Add("REVISION_NO",pRevisionNo);
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds);
			}
		}
		conn.Close();
		return ds;
	}
}
