﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: VcsInterface
--	Title			: サイト--	Progaram ID		: Site
--
--  Creation Date	: 2009.12.22
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using iBridCommLib;

public class Site:DbSession {

	public string webPhisicalDir;
	public string siteCd;
	public string charNoItemNm;
	public string ivpLocCd;
	public string ivpSiteCd;
	public string subHostNm;

	public Site() {
	}

	public bool GetOneByVcsWebLocCd(string pVcsWebLocCd) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;

		conn = DbConnect();

		string sSql = "SELECT " +
						"SITE_CD		," +
						"IVP_LOC_CD		," +
						"IVP_SITE_CD	," +
						"WEB_PHISICAL_DIR " +
					"FROM " +
						"T_SITE " +
					"WHERE " +
						"VCS_WEB_LOC_CD =:VCS_WEB_LOC_CD";

		using (cmd = new OracleCommand(sSql,conn))
		using (ds = new DataSet()) {
			cmd.Parameters.Add("VCS_WEB_LOC_CD",pVcsWebLocCd);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"T_SITE");
				if (ds.Tables["T_SITE"].Rows.Count != 0) {
					dr = ds.Tables["T_SITE"].Rows[0];
					siteCd = dr["SITE_CD"].ToString();
					webPhisicalDir = dr["WEB_PHISICAL_DIR"].ToString();
					ivpLocCd = dr["IVP_LOC_CD"].ToString();
					ivpSiteCd = dr["IVP_SITE_CD"].ToString();
					bExist = true;
				}
			}
		}
		conn.Close();
		return bExist;
	}

	public bool GetOneBySiteCd(string pSiteCd) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;

		conn = DbConnect();

		string sSql = "SELECT " +
						"SITE_CD		," +
						"CHAR_NO_ITEM_NM," +
						"SUB_HOST_NM	" +
					"FROM " +
						"T_SITE " +
					"WHERE " +
						"SITE_CD =:SITE_CD";

		using (cmd = new OracleCommand(sSql,conn))
		using (ds = new DataSet()) {
			cmd.Parameters.Add("SITE_CD",pSiteCd);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"T_SITE");
				if (ds.Tables["T_SITE"].Rows.Count != 0) {
					dr = ds.Tables["T_SITE"].Rows[0];
					siteCd = dr["SITE_CD"].ToString();
					subHostNm = dr["SUB_HOST_NM"].ToString();
					charNoItemNm = dr["CHAR_NO_ITEM_NM"].ToString();
					bExist = true;
				}
			}
		}
		conn.Close();
		return bExist;
	}

}
