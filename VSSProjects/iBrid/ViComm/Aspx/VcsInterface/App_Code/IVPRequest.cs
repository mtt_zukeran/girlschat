﻿using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;
using ViComm;

/// <summary>
/// Production の概要の説明です



/// </summary>
public class IVPRequest:DbSession {

	public decimal ivpRequestSeq;
	public int chargeUnitSec;
	public int chargePoint;
	public string manUserSeq;
	public string otherWebSysId;
	public string otherWebUserId;
	public int useOtherSysInfoFlag;
	public string requestSiteCd;
	public string requestId;
	public string actCategorySeq;

	public IVPRequest() {

	}

	public bool GetRequest(string pIvpRequestSeq) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;

		conn = DbConnect();

		string sSql = "SELECT " +
						"IVP_REQUEST_SEQ	," +
						"REQUEST_SITE_CD	," +
						"MAN_USER_SEQ		," +
						"CHARGE_UNIT_SEC	," +
						"CHARGE_POINT		," +
						"REQUEST_ID			," +
						"OTHER_WEB_SYS_ID	," +
						"OTHER_WEB_USER_ID	," +
						"USE_OTHER_SYS_INFO_FLAG," +
						"ACT_CATEGORY_SEQ	" +
					"FROM " +
						"VW_IVP_REQUEST01 " +
					"WHERE " +
						"IVP_REQUEST_SEQ =:IVP_REQUEST_SEQ";

		using (cmd = new OracleCommand(sSql,conn))
		using (ds = new DataSet()) {
			cmd.Parameters.Add("IVP_REQUEST_SEQ",pIvpRequestSeq);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"IVP_REQUEST");
				if (ds.Tables["IVP_REQUEST"].Rows.Count != 0) {
					dr = ds.Tables["IVP_REQUEST"].Rows[0];

					ivpRequestSeq = decimal.Parse(dr["IVP_REQUEST_SEQ"].ToString());
					requestSiteCd = dr["REQUEST_SITE_CD"].ToString();
					manUserSeq = dr["MAN_USER_SEQ"].ToString();
					chargeUnitSec = int.Parse(dr["CHARGE_UNIT_SEC"].ToString());
					chargePoint = int.Parse(dr["CHARGE_POINT"].ToString());
					requestId = dr["REQUEST_ID"].ToString();
					actCategorySeq = dr["ACT_CATEGORY_SEQ"].ToString();
					otherWebSysId = dr["OTHER_WEB_SYS_ID"].ToString();
					otherWebUserId = dr["OTHER_WEB_USER_ID"].ToString();
					useOtherSysInfoFlag = int.Parse(dr["USE_OTHER_SYS_INFO_FLAG"].ToString());
					bExist = true;
				}
			}
		}

		conn.Close();

		return bExist;
	}

}
