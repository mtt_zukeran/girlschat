﻿using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Web;
using ViComm;

public class User:DbSession {
	public string userSeq;
	public string userStatus;
	public string sexCd;
	public string tel;
	public int useFreeDailFlag;
	public int telAttestedFlag;
	public string loginId;
	public string loginPassword;


	public User() {
		userSeq = "";
		userStatus = "";
		sexCd = "";
		tel = "";
		useFreeDailFlag = 0;
		telAttestedFlag = 0;
		loginId = "";
		loginPassword = "";
	}

	public bool GetCurrentInfo(string pLoginId) {
		DataSet ds;
		DataRow dr;

		bool bExist = false;

		conn = DbConnect("User.GetCurrentInfo");

		string sSql = "SELECT " +
							"USER_SEQ			," +
							"USER_STATUS		," +
							"SEX_CD				," +
							"TEL				," +
							"TEL_ATTESTED_FLAG	," +
							"USE_FREE_DIAL_FLAG	," +
							"LOGIN_ID			," +
							"LOGIN_PASSWORD		" +
						"FROM " +
						" T_USER " +
						"WHERE " +
						" LOGIN_ID = :LOGIN_ID ";

		using (cmd = new OracleCommand(sSql,conn))
		using (ds = new DataSet()) {
			cmd.Parameters.Add("LOGIN_ID",pLoginId);
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"T_USER");

				if (ds.Tables["T_USER"].Rows.Count != 0) {
					dr = ds.Tables["T_USER"].Rows[0];
					userSeq = dr["USER_SEQ"].ToString();
					userStatus = dr["USER_STATUS"].ToString();
					sexCd = dr["SEX_CD"].ToString();
					tel = dr["TEL"].ToString();
					telAttestedFlag = int.Parse(dr["TEL_ATTESTED_FLAG"].ToString());
					useFreeDailFlag = int.Parse(dr["USE_FREE_DIAL_FLAG"].ToString());
					loginId = dr["LOGIN_ID"].ToString();
					loginPassword = dr["LOGIN_PASSWORD"].ToString();
					bExist = true;
				}
			}
		}
		conn.Close();

		return bExist;
	}
	
}
