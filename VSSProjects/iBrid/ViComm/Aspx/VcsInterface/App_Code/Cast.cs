/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Interface
--	Title			: 出演者
--	Progaram ID		: Cast
--
--  Creation Date	: 2010.06.02
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class Cast:DbSession {
	public string userSeq;
	public string castNm;

	public Cast() {
	}

	public DataSet GetOnlineCast(string pSiteCd) {
		DataSet ds = new DataSet();
		conn = DbConnect();

		string sSql = "SELECT " +
							"SITE_CD		," +
							"USER_SEQ		," +
							"USER_CHAR_NO	," +
							"CRYPT_VALUE	," +
							"LOGIN_ID		," +
							"PHOTO_IMG_PATH	," +
							"HANDLE_NM		," +
							"OK_PLAY_MASK	," +
							"AGE			," +
							"COMMENT_LIST	" +
						"FROM VW_CAST01 " +
						"WHERE SITE_CD = :SITE_CD AND CHARACTER_ONLINE_STATUS IN (:CHARACTER_ONLINE_STATUS1,:CHARACTER_ONLINE_STATUS2)";

		using (cmd = new OracleCommand(sSql,conn)) {
			cmd.Parameters.Add("SITE_CD",pSiteCd);
			cmd.Parameters.Add("CHARACTER_ONLINE_STATUS1",ViCommConst.USER_WAITING);
			cmd.Parameters.Add("CHARACTER_ONLINE_STATUS2",ViCommConst.USER_TALKING);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"VW_CAST01");
			}
		}
		conn.Close();

		AppendCastAttr(ds);

		return ds;
	}

	private void AppendCastAttr(DataSet pDS) {

		string sSql = "SELECT DISPLAY_VALUE,ITEM_NO FROM VW_CAST_ATTR_VALUE01 " +
					"WHERE " +
						"SITE_CD		= :SITE_CD	AND " +
						"USER_SEQ		= :USER_SEQ AND " +
						"USER_CHAR_NO	= :USER_CHAR_NO ";

		string sFavoritSql = "SELECT NVL(COUNT(*),0) AS CNT FROM T_FAVORIT " +
				   "WHERE " +
						"SITE_CD				= :SITE_CD			AND " +
						"USER_SEQ				= :USER_SEQ			AND " +
						"USER_CHAR_NO			= :USER_CHAR_NO		    ";


		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		DataColumn col;
		for (int i = 1;i <= ViCommConst.MAX_ATTR_COUNT;i++) {
			col = new DataColumn(string.Format("CAST_ATTR_VALUE{0:D2}",i),System.Type.GetType("System.String"));
			pDS.Tables[0].Columns.Add(col);
		}
		col = new DataColumn("FAVORIT_CNT",System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		col = new DataColumn("OK_LIST",System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		foreach (DataRow dr in pDS.Tables[0].Rows) {

			for (int i = 1;i <= ViCommConst.MAX_ATTR_COUNT;i++) {
				dr[string.Format("CAST_ATTR_VALUE{0:D2}",i)] = "";
			}
			using (DataSet dsSub = new DataSet()) {
				using (cmd = new OracleCommand(sSql,conn)) {
					cmd.Parameters.Add("SITE_CD",dr["SITE_CD"].ToString());
					cmd.Parameters.Add("USER_SEQ",dr["USER_SEQ"].ToString());
					cmd.Parameters.Add("USER_CHAR_NO",dr["USER_CHAR_NO"].ToString());
					using (da = new OracleDataAdapter(cmd)) {
						da.Fill(dsSub,"VW_CAST_ATTR_VALUE01");
					}
				}
				foreach (DataRow drSub in dsSub.Tables[0].Rows) {
					dr[string.Format("CAST_ATTR_VALUE{0}",drSub["ITEM_NO"].ToString())] = drSub["DISPLAY_VALUE"].ToString();
				}
			}

			using (DataSet dsSub = new DataSet())
			using (cmd = new OracleCommand(sFavoritSql,conn)) {
				cmd.Parameters.Add("SITE_CD",dr["SITE_CD"].ToString());
				cmd.Parameters.Add("USER_SEQ",dr["USER_SEQ"].ToString());
				cmd.Parameters.Add("USER_CHAR_NO",dr["USER_CHAR_NO"].ToString());
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(dsSub,"T_FAVORIT");
					if (dsSub.Tables[0].Rows.Count != 0) {
						DataRow drSub = dsSub.Tables[0].Rows[0];
						dr["FAVORIT_CNT"] = drSub["CNT"].ToString();
					}
				}
			}

			//OKリスト

			string sSqlOk = "SELECT OK_PLAY,OK_PLAY_NM FROM T_OK_PLAY_LIST " +
							"WHERE SITE_CD	= :SITE_CD " +
							"ORDER BY SITE_CD,OK_PLAY";

			DataSet dsOkList = new DataSet();
			using (cmd = new OracleCommand(sSqlOk,conn)) {
				cmd.Parameters.Add("SITE_CD",dr["SITE_CD"].ToString());
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(dsOkList,"T_OK_PLAY_LIST");
				}
			}

			if (dsOkList.Tables[0].Rows.Count != 0) {
				int iOkMask = int.Parse(dr["OK_PLAY_MASK"].ToString());
				int iMaskValue = 1;
				string sOkList = "";
				foreach (DataRow drOkList in dsOkList.Tables[0].Rows) {
					if ((iOkMask & iMaskValue) != 0) {
						sOkList += drOkList["OK_PLAY_NM"].ToString() + ",";
					}
					iMaskValue = iMaskValue << 1;
				}
				if (sOkList.Length != 0) {
					sOkList = sOkList.Substring(0,sOkList.Length - 1);
				}
				dr["OK_LIST"] = sOkList;
			}
		}
	}
}
