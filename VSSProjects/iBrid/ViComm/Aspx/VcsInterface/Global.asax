﻿<%@ Application Language="C#" %>
<%@ Import Namespace="System.Diagnostics" %>

<script RunAt="server">

	void Application_Start(object sender,EventArgs e) {
	}

	void Application_End(object sender,EventArgs e) {
	}

	void Application_Error(object sender,EventArgs e) {
		string sRoot = ConfigurationManager.AppSettings["Root"].ToString().ToLower();
		string sSource = "ViCOMM";
		string sLog = "Application";
		Exception objErr = Server.GetLastError().GetBaseException();

		string err = "ViCOMM IPV Interface Error Caught in Application_Error event\n" +
				  "Error in: " + Request.Url.ToString() +
				  "\nError Message:" + objErr.Message.ToString() +
				  "\nStack Trace:" + objErr.StackTrace.ToString();

		if (!EventLog.SourceExists(sSource)) {
			EventLog.CreateEventSource(sSource,sLog);
		}

		EventLog.WriteEntry(sSource,err,EventLogEntryType.Error);
		Server.ClearError();
	}

	void Session_Start(object sender,EventArgs e) {
	}

	void Session_End(object sender,EventArgs e) {

	}
       
       
</script>

