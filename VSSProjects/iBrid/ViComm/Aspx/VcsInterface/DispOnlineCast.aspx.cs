﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: VcsInterface
--	Title			: オンラインキャスト表示(JSON形式)

--	Progaram ID		: DispOnlineCast
--
--  Creation Date	: 2010.06.02
--  Creater			: iBrid(A.Koyanagi)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Web;
using iBridCommLib;
using ViComm;

public partial class DispOnlineCast:System.Web.UI.Page {
	string sSiteCd;

	protected void Page_Load(object sender,EventArgs e) {

		SetOnlineCast();
	}

	private void SetOnlineCast() {

		DataSet ds = new DataSet();
		sSiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);

		using (Cast oCast = new Cast()) {
			ds = oCast.GetOnlineCast(sSiteCd);
		}

		Response.ContentType = "text/html";
		Response.Write(CreateJsonString(ds));
		Response.End();
	}

	private string CreateJsonString(DataSet ds) {

		string sRet = "";

		if (ds.Tables[0].Rows.Count == 0) {
			return sRet;
		}

		string sSubHostNm = string.Empty;
		string sCharNoItemNm = string.Empty;

		using (Site oSite = new Site()) {
			oSite.GetOneBySiteCd(sSiteCd);
			sSubHostNm = oSite.subHostNm;
			sCharNoItemNm = oSite.charNoItemNm;
		}

		sRet = "\"onlinegirl\":{" + "\r\n";
		int i = 0;
		foreach (DataRow dr in ds.Tables[0].Rows) {
			sRet += " \"girl" + i.ToString() + "\":[" + "\r\n";
			sRet += "  {" + "\r\n";
			sRet += "   \"id\":\"" + dr["LOGIN_ID"].ToString() + "\"," + "\r\n";							// ID
			sRet += "   \"image\":\"" + string.Format(
											"http://{0}/user/vicomm/woman/{1}",
											sSubHostNm,
											dr["PHOTO_IMG_PATH"].ToString()) + "\"," + "\r\n";				// ﾒｲﾝ画像のURL
			sRet += "   \"link\":\"" + string.Format(
											"http://{0}/user/start.aspx?goto=ProfileDirect.aspx&castid={1}&{2}={3}",
											sSubHostNm,
											dr["LOGIN_ID"].ToString(),
											sCharNoItemNm,
											dr["CRYPT_VALUE"].ToString()) + "\"," + "\r\n";					// 詳細ﾍﾟｰｼﾞのURL
			sRet += "   \"nickname\":\"" + dr["HANDLE_NM"].ToString() + "\"," + "\r\n";						// ﾊﾝﾄﾞﾙﾈｰﾑ

			sRet += "   \"area\":\"" + dr["CAST_ATTR_VALUE01"].ToString() + "\"," + "\r\n";					// 01:地域

			sRet += "   \"age\":\"" + dr["AGE"].ToString() + "\"," + "\r\n";								// 02:年齢
			sRet += "   \"style\":\"" + dr["CAST_ATTR_VALUE03"].ToString() + "\"," + "\r\n";				// 03:ｽﾀｲﾙ

			sRet += "   \"type\":\"" + dr["CAST_ATTR_VALUE04"].ToString() + "\"," + "\r\n";					// 04:ﾀｲﾌﾟ

			sRet += "   \"height\":\"" + dr["CAST_ATTR_VALUE05"].ToString() + "\"," + "\r\n";				// 05:身長
			sRet += "   \"size\":[{\"bust\":\"" + dr["CAST_ATTR_VALUE06"].ToString() + "\"}," +
								 "{\"west\":\"" + dr["CAST_ATTR_VALUE07"].ToString() + "\"}," +
								 "{\"hip\":\"" + dr["CAST_ATTR_VALUE08"].ToString() + "\"}],\r\n";			// 06:ﾊﾞｽﾄ 07:ｳｴｽﾄ 08:ﾋｯﾌﾟ

			sRet += "   \"freq\":\"" + dr["CAST_ATTR_VALUE10"].ToString() + "\"," + "\r\n";					// 10:H度
			sRet += "   \"hstyle\":\"" + dr["CAST_ATTR_VALUE12"].ToString() + "\"," + "\r\n";					// 10:H度
			sRet += "   \"favo\":\"" + dr["FAVORIT_CNT"].ToString() + "\"," + "\r\n";						// お気に入り登録人数
			sRet += "   \"oklist\":\"" + dr["OK_LIST"].ToString() + "\"," + "\r\n";							// OKﾘｽﾄ

			sRet += "   \"msg\":\"" + dr["COMMENT_LIST"].ToString() + "\"" + "\r\n";						// ｺﾒﾝﾄ

			sRet += "  }" + "\r\n";

			if (i == ds.Tables[0].Rows.Count - 1) {
				sRet += " ]" + "\r\n";
			} else {
				sRet += " ]," + "\r\n";
			}
			i++;
		}

		sRet += "}";
		return sRet;
	}
}
