﻿using System;
using System.Threading;
using System.IO;
using System.Data;
using System.Text;
using System.Collections;
using System.Configuration;
using iBridCommLib;
using ViComm;

public partial class Dispatch:System.Web.UI.Page {


	public const int GET_USER_INFO = 11;			// 会員情報要求 
	public const int RPT_START_OUTGOING = 12;		// 発信開始 
	public const int RPT_START_TALK = 13;			// 会話開始 
	public const int RPT_USED_INFO = 16;			// 利用明細報告 
	public const int PRT_GET_OPE_PERFORMANCE = 21;	// オペレータ稼動状況照会 

	public const int RPT_TEL_AUTH = 1;				// 電話番号認証結果 
	public const int PRT_CLEAR_OPE_PERFORMANCE = 22;// オペレータ稼動状況クリア 
	public const int RPT_TERMINATE_INFO = 3;		// ユーザー回線切断情報 
	public const int PRT_CHAR_EXIST = 70;			// キャラクター存在確認 
	public const int PRT_CHAR_LOGIN = 71;			// キャラクターログイン 
	public const int PRT_CHAR_LOGOFF = 72;			// キャラクターログオフ 
	public const int REQ_GET_AVAILABLE_SEC = 73;	// VICOMM利用可能秒数取得 
	public const int RPT_RECORDING = 74;			// VICOMM録音報告 
	public const int REQ_NEW_IVP_REQUEST = 75;		// 新規IVP要求作成 
	public const int RPT_WIRETAPPING = 76;			// 盗聴報告(未使用) 
	public const int RPT_PLAY_PV_MSG = 77;			// PV伝言再生報告 

	public const int RPT_RX_NOTIFY_TALK_STATUS = 80;// SSDBから通話ステータス変更要求 

	public const int REQ_URGE_CALL = 100001;
	public const int REQ_GET_URGE_RESULT = 60;

	protected void Page_Load(object sender,EventArgs e) {
		int iReq;
		try {
			iReq = int.Parse(Request.QueryString["request"]);
		} catch {
			iReq = 0;
		}

		switch (iReq) {

			case GET_USER_INFO:
				GetUserInfo();
				break;

			case RPT_USED_INFO:
				ReportIvpUsedSec();
				break;

			case PRT_GET_OPE_PERFORMANCE:
				GetOpePerformance();
				break;

			case PRT_CLEAR_OPE_PERFORMANCE:
				ClearOpePerformance();
				break;

			case RPT_START_TALK:
			case RPT_START_OUTGOING:
			case RPT_TERMINATE_INFO:
				ReportCallStatus(iReq);
				break;

			case PRT_CHAR_EXIST:
				ExistCharacter();
				break;

			case PRT_CHAR_LOGIN:
				LoginCharacter();
				break;

			case PRT_CHAR_LOGOFF:
				LogoffCharacter();
				break;

			case RPT_RECORDING:
				Recording();
				break;

			case RPT_PLAY_PV_MSG:
				PlayPvMsg();
				break;

			case REQ_URGE_CALL:
				ExecUrgeCall();
				break;

			case REQ_GET_URGE_RESULT:
				GetUrgeResult();
				break;

			case REQ_NEW_IVP_REQUEST:
				NewIvpRequest();
				break;

			case REQ_GET_AVAILABLE_SEC:
				GetNewChargeAvalilableSec();
				break;

			case RPT_TEL_AUTH:
				SmartPhoneTelCertify();
				break;

			case RPT_RX_NOTIFY_TALK_STATUS:
				RxNotifyTalkStatus();
				break;

			default:
				Response.ContentType = "text/html";
				Response.Write(string.Format("result={0}","9"));
				Response.End();
				break;
		}
	}

	private void GetUserInfo() {
		string sSiteCd = "";
		string sLoginId = iBridUtil.GetStringValue(Request.QueryString["userid"]);
		string sIvpRequestSeq = iBridUtil.GetStringValue(Request.QueryString["sid"]);
		string sProcSeq = "-1";
		string sResult = "1";
		string sUrl;
		int iSec = 0;

		using (Site oSite = new Site()) {
			oSite.GetOneByVcsWebLocCd(iBridUtil.GetStringValue(Request.QueryString["webloc"]));
			sSiteCd = oSite.siteCd;
		}

		sResult = "0";
		using (UserMan oUserMan = new UserMan())
		using (IVPRequest oRequest = new IVPRequest()) {
			if (oUserMan.GetCurrentInfo(sSiteCd,sLoginId)) {
				StartIVPService(sIvpRequestSeq,"",sProcSeq,out iSec,out sResult);
			} else {

				if (oRequest.GetRequest(sIvpRequestSeq)) {
					if (oRequest.useOtherSysInfoFlag != 0) {
						using (OtherWebSite oOtherSys = new OtherWebSite()) {
							oOtherSys.GetOne(oRequest.otherWebSysId);
							sUrl = oOtherSys.userInfoUrl;
						}
						int iBalPoint = 0;
						string pTel = "",pPassword = "",pCondition = "",pRetCd = "";
						if (ViCommInterface.GetWebUserInfo(sUrl,oRequest.otherWebUserId,out iBalPoint,out  pTel,out  pPassword,out  pCondition,out  pRetCd) == ViCommConst.USER_INFO_OK) {
							if (oRequest.chargePoint > 0) {
								iSec = (iBalPoint / oRequest.chargePoint) * oRequest.chargeUnitSec;
								if (iSec > 86400) {
									iSec = 86400;
								}
							} else {
								iSec = 86400;
							}
						}
					}
				}
			}
		}

		Response.ContentType = "text/html";
		Response.Write(string.Format("result={0}&point={1}&limit={2}&avasec={3}",sResult,0,0,iSec));
		Response.End();
	}

	private void StartIVPService(string pIvpRequestSeq,string pAddIvpRequestSeq,string pCallStatuSeq,out int pAvaSec,out string pResult) {
		string sCID = iBridUtil.GetStringValue(Request.QueryString["cid"]);
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_INCOMMING_CALL_STATUS");
			db.ProcedureInParm("PIVP_REQUEST_SEQ",DbSession.DbType.VARCHAR2,pIvpRequestSeq);
			db.ProcedureInParm("PDISCONNECT_FLAG",DbSession.DbType.NUMBER,0);
			db.ProcedureInParm("PCID",DbSession.DbType.VARCHAR2,sCID);
			db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			pResult = db.GetStringValue("PRESULT");
		}
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("START_IVP_SERVICE");
			db.ProcedureInParm("PIVP_REQUEST_SEQ",DbSession.DbType.VARCHAR2,pIvpRequestSeq);
			db.ProcedureInParm("PADD_IVP_REQUEST_SEQ",DbSession.DbType.VARCHAR2,pAddIvpRequestSeq);
			db.ProcedureInParm("PCALL_STATUS_SEQ",DbSession.DbType.VARCHAR2,pCallStatuSeq);
			db.ProcedureOutParm("PAVAILABLE_SEC",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			pAvaSec = db.GetIntValue("PAVAILABLE_SEC");
		}

	}

	private void RxNotifyTalkStatus() {
		string sLoginId = iBridUtil.GetStringValue(Request.QueryString["userid"]);
		int iTalkFlag = int.Parse(iBridUtil.GetStringValue(Request.QueryString["talk"]));
		string sPriority = iBridUtil.GetStringValue(Request.QueryString["priority"]);

		if (!sPriority.Equals("0")) {
			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("RX_NOTIFY_TALK_STATUS");
				db.ProcedureInParm("pLOGIN_ID",DbSession.DbType.VARCHAR2,sLoginId);
				db.ProcedureInParm("pTALK_ON_FLAG",DbSession.DbType.NUMBER,iTalkFlag);
				db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
				db.ExecuteProcedure();
			}
		}
		
		Response.ContentType = "text/html";
		Response.Write(string.Format("result={0}","0"));
		Response.End();
	}

	private void ReportIvpUsedSec() {
		string sIvpRequestSeq = iBridUtil.GetStringValue(Request.QueryString["sid"]);
		string sStartDate = iBridUtil.GetStringValue(Request.QueryString["start"]);
		string sEndDate = iBridUtil.GetStringValue(Request.QueryString["end"]);
		string sChargeType = iBridUtil.GetStringValue(Request.QueryString["corner"]);
		string sChargeSec = iBridUtil.GetStringValue(Request.QueryString["sec"]);
		string sAddIvpReuqestSeq = iBridUtil.GetStringValue(Request.QueryString["subsid"]);
		string sUrl;
		int iUsedPoint;

		//2012/05/22 MASHuemura san
		string sUserId = iBridUtil.GetStringValue(Request.QueryString["userid"]);
		string sSexCd = string.Empty;
		using (User oUser = new User()) {
			oUser.GetCurrentInfo(sUserId);
			sSexCd = oUser.sexCd;
		}

		if (sSexCd.Equals(ViCommConst.MAN)) {
			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("CREATE_IVP_USED_REPORT");
				db.ProcedureInParm("PIVP_REQUEST_SEQ",DbSession.DbType.VARCHAR2,sIvpRequestSeq);
				db.ProcedureInParm("PADD_IVP_REQUEST_SEQ",DbSession.DbType.VARCHAR2,sAddIvpReuqestSeq);
				db.ProcedureInParm("PSTART_DATE",DbSession.DbType.VARCHAR2,sStartDate);
				db.ProcedureInParm("PEND_DATE",DbSession.DbType.VARCHAR2,sEndDate);
				db.ProcedureInParm("PCHARGE_TYPE",DbSession.DbType.VARCHAR2,sChargeType);
				db.ProcedureInParm("PCHARGE_SEC",DbSession.DbType.NUMBER,int.Parse(sChargeSec));
				db.ProcedureInParm("PVCS_FLAG",DbSession.DbType.NUMBER,1);
				db.ProcedureOutParm("PCHARGE_POINT",DbSession.DbType.NUMBER);
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
				db.ExecuteProcedure();
				iUsedPoint = int.Parse(db.GetStringValue("PCHARGE_POINT"));
			}

			using (IVPRequest oRequest = new IVPRequest()) {
				if (oRequest.GetRequest(sIvpRequestSeq)) {
					if (oRequest.useOtherSysInfoFlag != 0) {
						using (OtherWebSite oOtherSys = new OtherWebSite()) {
							oOtherSys.GetOne(oRequest.otherWebSysId);
							sUrl = oOtherSys.userInfoUrl;
						}
						ViCommInterface.TransUsedInfo(sUrl,oRequest.requestSiteCd,oRequest.otherWebUserId,iUsedPoint,sChargeType);
					}
				}
			}
		}
		Response.ContentType = "text/html";
		Response.Write(string.Format("result={0}","0"));
		Response.End();
	}

	private void GetOpePerformance() {
		string sLoginId = iBridUtil.GetStringValue(Request.QueryString["userid"]);
		string sNoDate = iBridUtil.GetStringValue(Request.QueryString["no_date"]);
		string sStartDate = iBridUtil.GetStringValue(Request.QueryString["start"]);
		string sEndDate = iBridUtil.GetStringValue(Request.QueryString["end"]);
		string sResult = "";

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("WEB_GET_OPE_PERFORMANCE");
			db.ProcedureInParm("PLOGIN_ID",DbSession.DbType.VARCHAR2,sLoginId);
			db.ProcedureInParm("PNO_DATE_FLAG",DbSession.DbType.NUMBER,int.Parse(sNoDate));
			db.ProcedureInParm("PFROM_DATE",DbSession.DbType.VARCHAR2,sStartDate);
			db.ProcedureInParm("PTO_DATE",DbSession.DbType.VARCHAR2,sEndDate);
			db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			sResult = db.GetStringValue("PRESULT");
		}
		Response.ContentType = "text/html";
		Response.Write(string.Format("result={0}{1}","0",sResult));
		Response.End();
	}

	private void ClearOpePerformance() {
		string sLoginId = iBridUtil.GetStringValue(Request.QueryString["userid"]);

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("WEB_CLEAR_OPE_PERFORMANCE");
			db.ProcedureInParm("PLOGIN_ID",DbSession.DbType.VARCHAR2,sLoginId);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		string sResult = "0";

		Response.ContentType = "text/html";
		Response.Write(string.Format("result={0}",sResult));
		Response.End();
	}

	private void ReportCallStatus(int pRequest) {
		string sIvpRequestSeq = iBridUtil.GetStringValue(Request.QueryString["sid"]);
		string sProcSeq = "-1";
		string sStatus = iBridUtil.GetStringValue(Request.QueryString["status"]);
		string sLoginId = iBridUtil.GetStringValue(Request.QueryString["userid"]);
		string sSexCd = iBridUtil.GetStringValue(Request.QueryString["sex"]);
		string sUserSeq = "";
		string sSiteCd = "";
		string sResult = "9";
		string sCastLoginId = string.Empty;
		string sCastUseTerminalType = string.Empty;

		using (Site oSite = new Site()) {
			oSite.GetOneByVcsWebLocCd(iBridUtil.GetStringValue(Request.QueryString["webloc"]));
			sSiteCd = oSite.siteCd;
		}

		if (sSexCd.Equals(ViCommConst.MAN)) {
			using (UserMan oUserMan = new UserMan()) {
				if (oUserMan.GetCurrentInfo(sSiteCd,sLoginId)) {
					sUserSeq = oUserMan.userSeq;
				}
			}
		} else {
			using (UserWoman oUserWoman = new UserWoman()) {
				if (oUserWoman.IsExistCharacter(sSiteCd,sLoginId,ViCommConst.MAIN_CHAR_NO)) {
					sUserSeq = oUserWoman.userSeq;
				}
			}
		}

		if (sIvpRequestSeq.Equals("") == false) {
			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("UPDATE_TALK_STATUS");
				db.ProcedureInParm("PIVP_REQUEST_SEQ",DbSession.DbType.VARCHAR2,sIvpRequestSeq);
				db.ProcedureInParm("PCALL_STATUS_SEQ",DbSession.DbType.VARCHAR2,sProcSeq);
				db.ProcedureInParm("PCALL_STATUS",DbSession.DbType.VARCHAR2,sStatus);
				db.ProcedureInParm("PTALK_END_REASON",DbSession.DbType.VARCHAR2,"");
				db.ProcedureInParm("PDISCONNECT_REASON",DbSession.DbType.VARCHAR2,"");
				db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,sUserSeq);
				db.ProcedureOutParm("pCAST_LOGIN_ID",DbSession.DbType.VARCHAR2);
				db.ProcedureOutParm("pCAST_USE_TERMINAL_TYPE",DbSession.DbType.VARCHAR2);
				db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
				db.ExecuteProcedure();
				sResult = db.GetStringValue("PRESULT");
				sCastLoginId = db.GetStringValue("pCAST_LOGIN_ID");
				sCastUseTerminalType = db.GetStringValue("pCAST_USE_TERMINAL_TYPE");
			}

			if (pRequest == RPT_TERMINATE_INFO) {
				using (DbSession db = new DbSession()) {
					db.PrepareProcedure("UPDATE_INCOMMING_CALL_STATUS");
					db.ProcedureInParm("PIVP_REQUEST_SEQ",DbSession.DbType.VARCHAR2,sIvpRequestSeq);
					db.ProcedureInParm("PDISCONNECT_FLAG",DbSession.DbType.NUMBER,1);
					db.ProcedureInParm("PCID",DbSession.DbType.VARCHAR2,"");
					db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
					db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
					db.ExecuteProcedure();
				}
			}

			#region MASHUP SSDB用処理
			string sSsdbUrl = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["ssdbUrl"]);
			//SIPかつWEBCONFIG設定済の場合のみﾘｸｴｽﾄ送信
			if (!sSsdbUrl.Equals(string.Empty) && sCastUseTerminalType.Equals(ViCommConst.TERM_SIP)) {
				string sRequest = string.Format(sSsdbUrl,sCastLoginId,sStatus.Equals(ViCommConst.CALL_RPT_DISCONNECT) ? "0" : "1");

				ViCommInterface.TransToParent(sRequest,true);
			}
			#endregion

		}
		Response.ContentType = "text/html";
		Response.Write(string.Format("result={0}",sResult));
		Response.End();
	}

	private void ExistCharacter() {
		string sSiteCd = "";
		string sLoginId = iBridUtil.GetStringValue(Request.QueryString["userid"]);
		string sCharNo = iBridUtil.GetStringValue(Request.QueryString["char"]);
		string sItemNo = iBridUtil.GetStringValue(Request.QueryString["item"]);
		string sItemCd = "";
		string sRec = "";
		string sResult = "1";

		using (Site oSite = new Site()) {
			oSite.GetOneByVcsWebLocCd(iBridUtil.GetStringValue(Request.QueryString["webloc"]));
			sSiteCd = oSite.siteCd;
		}

		using (UserWoman oUserWoman = new UserWoman()) {
			if (oUserWoman.IsExistCharacter(sSiteCd,sLoginId,sCharNo)) {
				sRec = oUserWoman.recSeq;
				if (oUserWoman.userStatus.Equals(ViCommConst.USER_WOMAN_NORMAL)) {
					if (oUserWoman.GetItemCd(sSiteCd,oUserWoman.userSeq,sCharNo,sItemNo,out sItemCd)) {
						sResult = "0";
					} else {
						sResult = "2";
					}
				}
			}
		}
		Response.ContentType = "text/html";
		if (sResult.Equals("0")) {
			Response.Write(string.Format("result={0}&itemvalue={1}&rec={2}",sResult,sItemCd,sRec));
		} else {
			Response.Write(string.Format("result={0}",sResult));
		}
		Response.End();
	}

	private void LoginCharacter() {
		string sSiteCd = "";
		string sLoginId = iBridUtil.GetStringValue(Request.QueryString["userid"]);
		string sCharNo = iBridUtil.GetStringValue(Request.QueryString["char"]);
		string sEndDate = iBridUtil.GetStringValue(Request.QueryString["end"]);
		string sRec = iBridUtil.GetStringValue(Request.QueryString["rec"]);
		string sTel = iBridUtil.GetStringValue(Request.QueryString["tel"]);
		string sResult = "1";

		using (Site oSite = new Site()) {
			oSite.GetOneByVcsWebLocCd(iBridUtil.GetStringValue(Request.QueryString["webloc"]));
			sSiteCd = oSite.siteCd;
		}

		using (UserWoman oUserWoman = new UserWoman()) {
			if (oUserWoman.IsExistCharacter(sSiteCd,sLoginId,sCharNo)) {
				if (oUserWoman.userStatus.Equals(ViCommConst.USER_WOMAN_NORMAL)) {
					DateTime dtEndDate;
					DateTime.TryParseExact(sEndDate,"yyyyMMddHHmmss",System.Globalization.DateTimeFormatInfo.InvariantInfo,System.Globalization.DateTimeStyles.None,out dtEndDate);

					if (dtEndDate > DateTime.Now) {
						using (DbSession db = new DbSession()) {
							db.PrepareProcedure("UPDATE_CAST_TEL");
							db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,oUserWoman.userSeq);
							db.ProcedureInParm("PTEL",DbSession.DbType.VARCHAR2,sTel);
							db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
							db.ExecuteProcedure();
						}

						using (DbSession db = new DbSession()) {
							db.PrepareProcedure("START_WAITTING");
							db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,sSiteCd);
							db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,oUserWoman.userSeq);
							db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,sCharNo);
							db.ProcedureInParm("PSTANDBY_END_DATE",DbSession.DbType.DATE,dtEndDate);
							db.ProcedureInParm("PCONNECT_TYPE",DbSession.DbType.VARCHAR2,string.Empty);
							db.ProcedureInParm("PMONITOR_ENABLE_FLAG",DbSession.DbType.NUMBER,0);
							db.ProcedureInParm("PMOBILE_TERMINAL_NM",DbSession.DbType.VARCHAR2,string.Empty);
							db.ProcedureInParm("PDUMMY_TALK_FLAG",DbSession.DbType.NUMBER,0);
							db.ProcedureInParm("PWAITING_COMMENT",DbSession.DbType.VARCHAR2,string.Empty);
							db.ProcedureInParm("PCHANGE_RANK",DbSession.DbType.VARCHAR2,string.Empty);
							db.ProcedureInParm("PSTANDBY_TIME_TYPE",DbSession.DbType.VARCHAR2,string.Empty);
							db.ProcedureInParm("PADMIN_FLAG",DbSession.DbType.VARCHAR2,ViCommConst.FLAG_OFF_STR);
							db.ProcedureInParm("PLOGIN_BY_TEL_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_ON);
							db.ProcedureOutParm("PLOGIN_SEQ",DbSession.DbType.VARCHAR2);
							db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
							db.ExecuteProcedure();
						}

						using (DbSession db = new DbSession()) {
							db.PrepareProcedure("UPDATE_CAST_REC_SEQ");
							db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,sSiteCd);
							db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,oUserWoman.userSeq);
							db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,sCharNo);
							db.ProcedureInParm("PREC_SEQ",DbSession.DbType.VARCHAR2,sRec);
							db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
							db.ExecuteProcedure();
						}

						using (DbSession db = new DbSession()) {
							db.PrepareProcedure("TX_LOGIN_MAIL");
							db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,sSiteCd);
							db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,oUserWoman.userSeq);
							db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,sCharNo);
							db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
							db.ExecuteProcedure();
						}
						sResult = "0";
					}
				}
			}
		}
		Response.ContentType = "text/html";
		Response.Write(string.Format("result={0}",sResult));
		Response.End();
	}

	private void LogoffCharacter() {
		string sSiteCd = "";
		string sLoginId = iBridUtil.GetStringValue(Request.QueryString["userid"]);
		string sCharNo = iBridUtil.GetStringValue(Request.QueryString["char"]);
		string sResult = "1";

		using (Site oSite = new Site()) {
			oSite.GetOneByVcsWebLocCd(iBridUtil.GetStringValue(Request.QueryString["webloc"]));
			sSiteCd = oSite.siteCd;
		}

		using (UserWoman oUserWoman = new UserWoman()) {
			if (oUserWoman.IsExistCharacter(sSiteCd,sLoginId,sCharNo)) {
				if (oUserWoman.userStatus.Equals(ViCommConst.USER_WOMAN_NORMAL) && oUserWoman.IsLoginCharacter(sSiteCd,oUserWoman.userSeq,sCharNo)) {
					string[] sUserCharNo = { sCharNo };

					using (DbSession db = new DbSession()) {
						db.PrepareProcedure("CAST_CHARACTER_LOGOFF");
						db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,sSiteCd);
						db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,oUserWoman.userSeq);
						db.ProcedureInArrayParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,1,sUserCharNo);
						db.ProcedureInParm("PUSER_CHAR_RECORD_COUNT",DbSession.DbType.NUMBER,1);
						db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
						db.ExecuteProcedure();
					}
					sResult = "0";
				}
			}
		}
		Response.ContentType = "text/html";
		Response.Write(string.Format("result={0}",sResult));
		Response.End();
	}

	private void Recording() {
		string sSiteCd = "";
		string sUserId = iBridUtil.GetStringValue(Request.QueryString["userid"]);
		string sUserSeq = "";
		string sUserCharNo = iBridUtil.GetStringValue(Request.QueryString["charno"]);
		string sPartnerUserId = iBridUtil.GetStringValue(Request.QueryString["partnerid"]);
		string sPartnerUserSeq = "";
		string sPartnerUserCharNo = iBridUtil.GetStringValue(Request.QueryString["pcharno"]);
		string sRecSeq = iBridUtil.GetStringValue(Request.QueryString["recseq"]);
		string sRecType = iBridUtil.GetStringValue(Request.QueryString["rectype"]);
		string sSexCd = iBridUtil.GetStringValue(Request.QueryString["sex"]);
		string sTel = iBridUtil.GetStringValue(Request.QueryString["tel"]);
		int iDelFlag = ViCommConst.FLAG_OFF;
		string sResult = "1";

		using (Site oSite = new Site()) {
			oSite.GetOneByVcsWebLocCd(iBridUtil.GetStringValue(Request.QueryString["webloc"]));
			sSiteCd = oSite.siteCd;
		}

		if (sUserCharNo.Equals("")) {
			sUserCharNo = ViCommConst.MAIN_CHAR_NO;
		}
		if (sPartnerUserCharNo.Equals("")) {
			sPartnerUserCharNo = ViCommConst.MAIN_CHAR_NO;
		}

		if (sSexCd.Equals(ViCommConst.MAN)) {
			using (UserMan oUserMan = new UserMan()) {
				if (oUserMan.GetCurrentInfo(sSiteCd,sUserId)) {
					sUserSeq = oUserMan.userSeq;
					sUserCharNo = ViCommConst.MAIN_CHAR_NO;
				}
			}
			using (UserWoman oUserWoman = new UserWoman()) {
				if (oUserWoman.IsExistCharacter(sSiteCd,sPartnerUserId,sPartnerUserCharNo)) {
					sPartnerUserSeq = oUserWoman.userSeq;
				}
			}
		} else {
			using (UserWoman oUserWoman = new UserWoman()) {
				if (oUserWoman.IsExistCharacter(sSiteCd,sUserId,sPartnerUserCharNo)) {
					sUserSeq = oUserWoman.userSeq;
				}
			}
			using (UserMan oUserMan = new UserMan()) {
				if (oUserMan.GetCurrentInfo(sSiteCd,sPartnerUserId)) {
					sPartnerUserSeq = oUserMan.userSeq;
					sPartnerUserCharNo = ViCommConst.MAIN_CHAR_NO;
				}
			}
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("RECORDING_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,sSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,sUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,sUserCharNo);
			db.ProcedureInParm("PPARTNER_USER_SEQ",DbSession.DbType.VARCHAR2,sPartnerUserSeq);
			db.ProcedureInParm("PPARTNER_USER_CHAR_NO",DbSession.DbType.VARCHAR2,sPartnerUserCharNo);
			db.ProcedureInParm("PREC_SEQ",DbSession.DbType.VARCHAR2,sRecSeq);
			db.ProcedureInParm("PREC_TYPE",DbSession.DbType.VARCHAR2,sRecType);
			db.ProcedureInParm("PFOWARD_TEL_NO",DbSession.DbType.VARCHAR2,sTel);
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,iDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		sResult = "0";

		Response.ContentType = "text/html";
		Response.Write(string.Format("result={0}",sResult));
		Response.End();
	}

	private void PlayPvMsg() {
		string sIvpRequestSeq = iBridUtil.GetStringValue(Request.QueryString["sid"]);
		string sLoginId = iBridUtil.GetStringValue(Request.QueryString["userid"]);
		string sCharNo = iBridUtil.GetStringValue(Request.QueryString["char"]);
		string sRecNo = iBridUtil.GetStringValue(Request.QueryString["recno"]);

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_PLAY_PV_STATUS");
			db.ProcedureInParm("PIVP_REQUEST_SEQ",DbSession.DbType.VARCHAR2,sIvpRequestSeq);
			db.ProcedureInParm("PLOGIN_ID",DbSession.DbType.VARCHAR2,sLoginId);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,sCharNo);
			db.ProcedureInParm("PREC_SEQ",DbSession.DbType.VARCHAR2,sRecNo);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		Response.ContentType = "text/html";
		Response.Write("result=0");
		Response.End();
	}

	private void GetNewChargeAvalilableSec() {
		string sSiteCd = "";
		string sIvpRequestSeq = iBridUtil.GetStringValue(Request.QueryString["sid"]);
		string sLoginId = iBridUtil.GetStringValue(Request.QueryString["userid"]);
		string sCharge = iBridUtil.GetStringValue(Request.QueryString["charge"]);
		string sSubSid = "";
		string sUserSeq = "";
		string sResult = "1";
		int iAvaSec = 0;

		using (Site oSite = new Site()) {
			oSite.GetOneByVcsWebLocCd(iBridUtil.GetStringValue(Request.QueryString["webloc"]));
			sSiteCd = oSite.siteCd;
		}
		using (UserMan oUserMan = new UserMan()) {
			if (oUserMan.GetCurrentInfo(sSiteCd,sLoginId)) {
				sUserSeq = oUserMan.userSeq;
			}
		}
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CALC_AVAILABLE_SEC");
			db.ProcedureInParm("PIVP_REQUEST_SEQ",DbSession.DbType.VARCHAR2,sIvpRequestSeq);
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,sSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,sUserSeq);
			db.ProcedureInParm("PCHARGE_TYPE",DbSession.DbType.VARCHAR2,sCharge);
			db.ProcedureOutParm("PCHARGE_UNIT_SEC",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCHARGE_POINT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PAVAILABLE_SEC",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PADD_IVP_REQUEST_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			iAvaSec = db.GetIntValue("PAVAILABLE_SEC");
			sSubSid = db.GetStringValue("PADD_IVP_REQUEST_SEQ");
		}
		if (iAvaSec > 0) {
			sResult = "0";
		} else {
			iAvaSec = 1;
			sResult = "1";
		}
		Response.ContentType = "text/html";
		Response.Write(string.Format("result={0}&sec={1}&subsid={2}",sResult,iAvaSec,sSubSid));
		Response.End();
	}


	private void ExecUrgeCall() {
		string sWebloc = iBridUtil.GetStringValue(Request.QueryString["webloc"]);
		string sRevNo = iBridUtil.GetStringValue(Request.QueryString["revno"]);
		int iExecMin;
		string sWebPhisicalDir = "";
		string sVcsFtpIp = "";
		string sVcsFtpId = "";
		string sVcsFtpPw = "";
		string sVcsRequestUrl = "";
		string sUploadPath = "";
		string sResult = "9";

		using (Sys oSys = new Sys()) {
			oSys.GetOne();
			sVcsFtpIp = oSys.vcsFtpIp;
			sVcsFtpId = oSys.vcsFtpId;
			sVcsFtpPw = oSys.vcsFtpPw;
			sVcsRequestUrl = oSys.vcsRequestUrl;
		}

		using (Site oSite = new Site()) {
			oSite.GetOneByVcsWebLocCd(sWebloc);
			sWebPhisicalDir = oSite.webPhisicalDir;
		}

		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
			string sFile = "REQ_URGE_" + sRevNo + ".txt";
			sUploadPath = sWebPhisicalDir + "\\Text\\" + sFile;
			StreamWriter writer = new StreamWriter(sUploadPath,false);
			using (RequestUrgeCall oCall = new RequestUrgeCall()) {
				DataSet ds = oCall.GetList(sRevNo);
				foreach (DataRow dr in ds.Tables[0].Rows) {
					writer.WriteLine(
						sWebloc + "," +
						dr["TEL"].ToString() + "," +
						dr["LOGIN_ID"].ToString() + "," +
						dr["BILL_AMT"].ToString() + "," +
						dr["URGE_LEVEL"].ToString() + "," +
						dr["IVP_REQUEST_SEQ"].ToString());
				}
			}
			writer.Close();

			if (SysInterface.FTPUpload(sUploadPath,sVcsFtpIp,sFile,sVcsFtpId,sVcsFtpPw)) {
				int.TryParse(iBridUtil.GetStringValue(Request.QueryString["min"]),out iExecMin);
				string sUrl = string.Format("{0}?request={1}&file={2}&min={3}&webloc={4}",sVcsRequestUrl,REQ_GET_URGE_RESULT,sFile,iExecMin,sWebloc);
				string sResponse = "";
				SysInterface.SendHttpRequest(sUrl,ref sResponse);
			}
			Response.ContentType = "text/html";
			Response.Write(string.Format("result={0}",sResult));
			Response.End();
		}
	}

	private void GetUrgeResult() {
		string sWebloc = iBridUtil.GetStringValue(Request.QueryString["webloc"]);
		string sFile = iBridUtil.GetStringValue(Request.QueryString["file"]);
		string sWebPhisicalDir = "";
		string sVcsFtpIp = "";
		string sVcsFtpId = "";
		string sVcsFtpPw = "";
		string sDownloadPath = "";
		string sResult = "9";


		using (Sys oSys = new Sys()) {
			oSys.GetOne();
			sVcsFtpIp = oSys.vcsFtpIp;
			sVcsFtpId = oSys.vcsFtpId;
			sVcsFtpPw = oSys.vcsFtpPw;
		}

		using (Site oSite = new Site()) {
			oSite.GetOneByVcsWebLocCd(sWebloc);
			sWebPhisicalDir = oSite.webPhisicalDir;
		}

		ArrayList oListReqNo = new ArrayList();
		ArrayList oListStatus = new ArrayList();
		string[] sIvpRequestSeq;
		string[] sCallStatus;
		string sLine;
		int iCount = 0;
		sDownloadPath = sWebPhisicalDir + "\\Text\\" + sFile;
		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
			if (SysInterface.FtpDownload(sDownloadPath,sVcsFtpIp,sFile,sVcsFtpId,sVcsFtpPw)) {
				using (StreamReader sr = new StreamReader(sDownloadPath,Encoding.GetEncoding("Shift-JIS"))) {
					while ((sLine = sr.ReadLine()) != null) {
						string[] sValues = sLine.Split(',');
						oListReqNo.Add(sValues[5]);
						oListStatus.Add(sValues[6]);
						iCount++;
					}
					sIvpRequestSeq = (string[])oListReqNo.ToArray(typeof(string));
					sCallStatus = (string[])oListStatus.ToArray(typeof(string));
				}
				using (DbSession db = new DbSession()) {
					db.PrepareProcedure("UPDATE_URGE_STATUS");
					db.ProcedureInArrayParm("PIVP_REQUEST_SEQ",DbSession.DbType.VARCHAR2,iCount,sIvpRequestSeq);
					db.ProcedureInArrayParm("PCALL_STATUS",DbSession.DbType.VARCHAR2,iCount,sCallStatus);
					db.ProcedureInParm("PRECORD_COUNT",DbSession.DbType.NUMBER,iCount);
					db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
					db.ExecuteProcedure();
				}
				sResult = "0";
			}
			Response.ContentType = "text/html";
			Response.Write(string.Format("result={0}",sResult));
			Response.End();
		}
	}

	private void NewIvpRequest() {
		string sVcsMenuId = iBridUtil.GetStringValue(Request.QueryString["menu"]);
		string sCastLoginId = iBridUtil.GetStringValue(Request.QueryString["castid"]);
		string sCastCharNo = iBridUtil.GetStringValue(Request.QueryString["castchar"]);
		string sCastTel = iBridUtil.GetStringValue(Request.QueryString["casttel"]);
		string sCastSeq = "";

		string sManId = iBridUtil.GetStringValue(Request.QueryString["manid"]);
		string sManTel = iBridUtil.GetStringValue(Request.QueryString["mantel"]);
		string sManSeq = "";

		string sRecNo = iBridUtil.GetStringValue(Request.QueryString["recno"]);

		string sResult = "";
		string sUrl = "";
		string sRequestSeq = "";

		string sSiteCd = "";
		string sIvpLocCd = "";
		string sIvpSiteCd = "";

		using (Site oSite = new Site()) {
			oSite.GetOneByVcsWebLocCd(iBridUtil.GetStringValue(Request.QueryString["webloc"]));
			sSiteCd = oSite.siteCd;
			sIvpLocCd = oSite.ivpLocCd;
			sIvpSiteCd = oSite.ivpSiteCd;
		}

		using (UserWoman oUserWoman = new UserWoman()) {
			if (oUserWoman.IsExistCharacter(sSiteCd,sCastLoginId,sCastCharNo)) {
				sCastSeq = oUserWoman.userSeq;
			}
		}
		using (UserMan oUserMan = new UserMan()) {
			if (oUserMan.GetCurrentInfo(sSiteCd,sManId)) {
				sManSeq = oUserMan.userSeq;
			}
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SET_IVP_REQUEST");
			db.ProcedureInParm("PREQUEST_SRC_ID",DbSession.DbType.VARCHAR2,sCastTel);
			db.ProcedureInParm("PCARRIER_TYPE",DbSession.DbType.VARCHAR2,"");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,sSiteCd);
			db.ProcedureBothParm("PREQUEST_ID",DbSession.DbType.VARCHAR2,ViCommConst.REQUEST_VCS_MENU);
			db.ProcedureInParm("PMENU_ID",DbSession.DbType.VARCHAR2,sVcsMenuId);
			db.ProcedureInParm("PREQUESTER_SEX_CD",DbSession.DbType.VARCHAR2,ViCommConst.MAN);
			db.ProcedureInParm("PMAN_USER_SEQ",DbSession.DbType.VARCHAR2,sManSeq);
			db.ProcedureInParm("PCAST_USER_SEQ",DbSession.DbType.VARCHAR2,sCastSeq);
			db.ProcedureInParm("PCAST_CHAR_NO",DbSession.DbType.VARCHAR2,sCastCharNo);
			db.ProcedureInParm("PLIVE_SEQ",DbSession.DbType.VARCHAR2,"");
			db.ProcedureInParm("PCAMERA_ID",DbSession.DbType.VARCHAR2,"");
			db.ProcedureInParm("PMOVIE_SEQ",DbSession.DbType.VARCHAR2,sRecNo);
			db.ProcedureInParm("PMOVIE_PART_NO",DbSession.DbType.VARCHAR2,"");
			db.ProcedureInParm("PINVITE_TALK_KEY",DbSession.DbType.VARCHAR2,"");
			db.ProcedureInParm("PBROADCASTING_SEC",DbSession.DbType.NUMBER,0);
			db.ProcedureInParm("PPARTNER_SHARE_LIVE_ACCEPT_NO",DbSession.DbType.VARCHAR2,"");
			db.ProcedureInParm("PPARTNER_SHARE_LIVE_KEY",DbSession.DbType.VARCHAR2,"");
			db.ProcedureInParm("PMEETING_KEY",DbSession.DbType.VARCHAR2,string.Empty);
			db.ProcedureOutParm("PIVP_REQUEST_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PDEST_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PDEST_TYPE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSHARED_MEDIA_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCONNECT_TYPE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PTALK_TYPE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCHARGE_UNIT_SEC",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCHARGE_POINT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PIVP_MENU_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PIVP_ACTION_DTMF",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PIVP_ACCEPT_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPLAY_FILE_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPLAY_OFFSET_SEC",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPLAY_SEC",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PDIAL_OUT_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTART_GUIDANCE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSRC_USER_AGENT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PDEST_USER_AGENT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREMARKS",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PVCS_REQUEST_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSHARE_LIVE_TALK_KEY",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSHARE_LIVE_TALK_END_DATE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCAST_LOGIN_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREC_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PUAS_SIP_URI",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);

			db.ExecuteProcedure();

			sResult = db.GetStringValue("PRESULT");
			sRequestSeq = db.GetStringValue("PIVP_REQUEST_SEQ");
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_INCOMMING_CALL_STATUS");
			db.ProcedureInParm("PIVP_REQUEST_SEQ",DbSession.DbType.VARCHAR2,sRequestSeq);
			db.ProcedureInParm("PDISCONNECT_FLAG",DbSession.DbType.NUMBER,0);
			db.ProcedureInParm("PCID",DbSession.DbType.VARCHAR2,"");
			db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}

		using (Sys oSys = new Sys()) {
			string sRequestUrl = "";
			oSys.GetValue("IVP_REQUEST_URL",out sRequestUrl);

			sUrl = string.Format(
						"{0}?tel={1}&sid={2}&carrier={3}&req={4}&menu={5}&desttype={6}&destno={7}&loc={8}&site={9}&acceptonly=1",
						sRequestUrl,
						sCastTel,
						sRequestSeq,
						ViCommConst.DOCOMO,
						ViCommConst.REQUEST_VCS_MENU,
						sVcsMenuId,
						ViCommConst.TERM_MOBILE,
						sManTel,
						sIvpLocCd,
						sIvpSiteCd
					);
		}

		if (sResult.Equals(ViCommConst.SP_RESULT_OK)) {
			sResult = ExecRequest(ViCommConst.OPERATOR,sUrl,sRequestSeq);
		}

		Response.ContentType = "text/html";
		Response.Write(string.Format("result={0}&sid={1}",sResult,sRequestSeq));
		Response.End();
	}

	string ExecRequest(string pSexCd,string pUrl,string pRequestSeq) {

		string sResult = "1";
		string sAcceptSeq = "";
		string sDialNo = "";
		string sVideoPrefix = "";
		string sAudioPrefix = "";
		string sInterfaceResult = "";

		if (ViCommInterface.TransIVPUserRequest(pUrl,ref sAcceptSeq,ref sDialNo,ref sVideoPrefix,ref sAudioPrefix,ref sInterfaceResult)) {
			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("UPDATE_ACCEPT_SEQ");
				db.ProcedureInParm("PREQUESTER_SEX_CD",DbSession.DbType.VARCHAR2,pSexCd);
				db.ProcedureInParm("PIVP_REQUEST_SEQ",DbSession.DbType.VARCHAR2,pRequestSeq);
				db.ProcedureInParm("PIVP_ACCEPT_SEQ",DbSession.DbType.VARCHAR2,sAcceptSeq);
				db.ProcedureInParm("PDIALIN_NO",DbSession.DbType.VARCHAR2,sDialNo);
				db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
				db.ExecuteProcedure();
				sResult = db.GetStringValue("PRESULT");
				if (sResult.Equals(ViCommConst.SP_RESULT_OK)) {
					sResult = "0";
				}
			}
		}
		return sResult;
	}

	private void SmartPhoneTelCertify() {
		string sIvpRequestSeq = iBridUtil.GetStringValue(Request.QueryString["sid"]);
		string sTel = iBridUtil.GetStringValue(Request.QueryString["tel"]);
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CERTIFY_SMART_PHONE");
			db.ProcedureInParm("PIVP_REQUEST_SEQ",DbSession.DbType.VARCHAR2,sIvpRequestSeq);
			db.ProcedureInParm("PTEL",DbSession.DbType.VARCHAR2,sTel);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		Response.ContentType = "text/html";
		Response.Write(string.Format("result={0}","0"));
		Response.End();
	}

}
