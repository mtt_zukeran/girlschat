﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class SendSmsPassword:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		string sTel = iBridUtil.GetStringValue(Request.QueryString["tel"]);
		string sSiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		string sPassword = string.Empty;
		double dTel = 0;
		double.TryParse(sTel,out dTel);

		DataSet ds = null;
		DataRow dr = null;
		using (DbSession db = new DbSession()) {
			// 入力された電話番号によりパスワードを取得
			User oUser = new User();
			ds = oUser.GetOneByTel(sTel,ViCommConst.MAN);
			if (ds.Tables[0].Rows.Count == 0) {
				// 電話番号未登録
				this.response("0");
				return;
			}
			dr = ds.Tables[0].Rows[0];
			sPassword = dr["LOGIN_PASSWORD"].ToString();

			// サイト構成拡張設定からログインパスワード送信SMS本文を取得
			string sBodyText = string.Empty;
			SiteManagementEx oSiteManagementEx = new SiteManagementEx();
			if (!oSiteManagementEx.GetOne(sSiteCd)) {
				// サイト構成拡張設定未登録
				this.response("0");
				return;
			}
			sBodyText = oSiteManagementEx.loginReminderSmsDoc;

			// システム設定からTwilio情報を取得
			Sys oSys = new Sys();
			if (!oSys.GetOne()) {
				// システム設定未登録
				this.response("0");
				return;
			}

			// SMS送信APIのURLを生成
			string sUrl = string.Format("https://api.twilio.com/2010-04-01/Accounts/{0}/Messages.json",oSys.twilioAccountSid);

			try {
				// POSTパラメータ
				System.Collections.Specialized.NameValueCollection ps = new System.Collections.Specialized.NameValueCollection();
				ps.Add("To",string.Format("+81{0}",dTel.ToString()));
				ps.Add("From",string.Format("+{0}",oSys.twilioSmsTelNo));
				ps.Add("Body",string.Format(sBodyText,sPassword));
				// SMS送信APIにPOST送信
				System.Net.WebClient wc = new System.Net.WebClient();
				wc.Credentials = new System.Net.NetworkCredential(oSys.twilioAccountSid,oSys.twilioAuthToken);
				byte[] resData = wc.UploadValues(sUrl,ps);
				wc.Dispose();
			} catch (Exception ee) {
				// エラー時
				this.response("0");
			}
		}
		this.response("1");
	}

	private void response(string result) {
		Response.ContentType = "text/html";
		Response.Write("result=" + result);
		Response.End();
	}
}
