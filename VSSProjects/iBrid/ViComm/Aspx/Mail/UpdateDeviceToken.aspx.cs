﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Mail
--	Title			: デバイストークン更新
--	Progaram ID		: UpdateDeviceToken
--  Creation Date	: 2014.03.21
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.IO;
using System.Data;
using System.Web;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Configuration;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class UpdateDeviceToken:System.Web.UI.Page {
	protected void Page_Load(object sender,EventArgs e) {
		string sUserSeq = iBridUtil.GetStringValue(Request.QueryString["userseq"]);
		string sDeviceToken = iBridUtil.GetStringValue(Request.QueryString["token"]);

		if (string.IsNullOrEmpty(sUserSeq) || string.IsNullOrEmpty(sDeviceToken)) {
			Response.ContentType = "text/html";
			Response.Write("-1");
			Response.End();
			return;
		}

		using (User oUser = new User()) {
			oUser.UpdateDeviceToken(sUserSeq,sDeviceToken);
		}

		Response.ContentType = "text/html";
		Response.Write("0");
		Response.End();
		return;
	}
}
