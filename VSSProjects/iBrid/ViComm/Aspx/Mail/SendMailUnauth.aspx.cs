﻿using System;
using System.IO;
using System.Data;
using System.Web;
using System.Net;
using Agiletech.Net.Mail;
using System.Text;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Configuration;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class SendMailUnauth:System.Web.UI.Page {


	protected void Page_Load(object sender,EventArgs e) {
		string sEmailAddr = iBridUtil.GetStringValue(Request.QueryString["emailaddr"]);
		string sLoginId = iBridUtil.GetStringValue(Request.QueryString["loginId"]);

		if ((!string.IsNullOrEmpty(sEmailAddr)) && (!string.IsNullOrEmpty(sLoginId))) {
			string sTitle = CreateMailTitle();
			string sDoc = CreateMailDoc(sLoginId);

			// ﾒｰﾙ送信 
			if (!string.IsNullOrEmpty(sDoc)) {
				SendMail(sEmailAddr,sEmailAddr,sTitle,sDoc);
				Response.ContentType = "text/html";
				Response.Write("result=0");
				Response.End();
			}
		}

		Response.ContentType = "text/html";
		Response.Write("result=-1");
		Response.End();
	}

	private void SendMail(string fromAddress,string toAddress,string title,string contents) {
		string sSendMailEncode = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["SendMailEncode"]).Trim();
		if (string.IsNullOrEmpty(sSendMailEncode)) {
			sSendMailEncode = "Shift_JIS";
		}
		Encoding enc = Encoding.GetEncoding(sSendMailEncode);

		Agiletech.Net.Mail.MailMessage msg = new Agiletech.Net.Mail.MailMessage();

		msg.SubjectEncoding = enc;
		msg.BodyEncoding = enc;
		msg.From = new Agiletech.Net.Mail.MailAddress(fromAddress);
		msg.To.Add(new Agiletech.Net.Mail.MailAddress(toAddress));
		msg.Subject = title;
		msg.Body = contents;
		msg.Headers.HeaderItems.Add("Date",DateTime.Now.ToString("R").Replace("GMT","+0900"));

		Agiletech.Net.Mail.SMTPClient sc = new Agiletech.Net.Mail.SMTPClient("127.0.0.1");
		sc.SendMail(msg);
	}

	private string CreateMailDoc(string pLoginId) {
		string sType = iBridUtil.GetStringValue(Request.QueryString["sendtype"]);

		StringBuilder oStringBuilder = new StringBuilder();

		switch (sType) {
			case ViCommConst.SendMailUnauthType.MAN_PROFILE_PIC:
				oStringBuilder.AppendLine("男性プロフィール写真が投稿されました。");
				break;
			case ViCommConst.SendMailUnauthType.CAST_PROFILE_PIC:
				oStringBuilder.AppendLine("女性プロフィール写真が投稿されました。");
				break;
			case ViCommConst.SendMailUnauthType.MAN_STOCK_MAIL_PIC:
				oStringBuilder.AppendLine("男性メールストック写真が投稿されました。");
				break;
			case ViCommConst.SendMailUnauthType.MAN_STOCK_MAIL_MOVIE:
				oStringBuilder.AppendLine("男性メールストック動画が投稿されました。");
				break;
			case ViCommConst.SendMailUnauthType.MAN_MAILER_MAIL_PIC:
			case ViCommConst.SendMailUnauthType.MAN_MAILER_MAIL_MOVIE:
			case ViCommConst.SendMailUnauthType.CAST_MAILER_MAIL_PIC:
			case ViCommConst.SendMailUnauthType.CAST_MAILER_MAIL_MOVIE:
				oStringBuilder.AppendLine("添付メールが送信されました。");
				break;
			case ViCommConst.SendMailUnauthType.CAST_STOCK_MAIL_PIC:
				oStringBuilder.AppendLine("女性メールストック写真が投稿されました。");
				break;
			case ViCommConst.SendMailUnauthType.CAST_STOCK_MAIL_MOVIE:
				oStringBuilder.AppendLine("女性メールストック動画が投稿されました。");
				break;
			case ViCommConst.SendMailUnauthType.CAST_PROFILE_MOVIE:
				oStringBuilder.AppendLine("女性プロフィール動画が投稿されました。");
				break;
			case ViCommConst.SendMailUnauthType.CAST_BBS_PIC:
				oStringBuilder.AppendLine("女性掲示板写真が投稿されました。");
				break;
			case ViCommConst.SendMailUnauthType.CAST_BBS_MOVIE:
				oStringBuilder.AppendLine("女性掲示板動画が投稿されました。");
				break;
			default:
				break;
		}
		oStringBuilder.AppendLine(string.Format("■ﾛｸﾞｲﾝID:{0}",pLoginId));

		return oStringBuilder.ToString();
	}

	private string CreateMailTitle() {
		string sType = iBridUtil.GetStringValue(Request.QueryString["sendtype"]);

		string sTitle = string.Empty;

		switch (sType) {
			case ViCommConst.SendMailUnauthType.MAN_PROFILE_PIC:
				sTitle = "男性プロフィール写真投稿通知";
				break;
			case ViCommConst.SendMailUnauthType.CAST_PROFILE_PIC:
				sTitle = "女性プロフィール写真投稿通知";
				break;
			case ViCommConst.SendMailUnauthType.MAN_STOCK_MAIL_PIC:
				sTitle = "男性メールストック写真投稿通知";
				break;
			case ViCommConst.SendMailUnauthType.MAN_STOCK_MAIL_MOVIE:
				sTitle = "男性メールストック動画投稿通知";
				break;
			case ViCommConst.SendMailUnauthType.MAN_MAILER_MAIL_PIC:
			case ViCommConst.SendMailUnauthType.MAN_MAILER_MAIL_MOVIE:
			case ViCommConst.SendMailUnauthType.CAST_MAILER_MAIL_PIC:
			case ViCommConst.SendMailUnauthType.CAST_MAILER_MAIL_MOVIE:
				sTitle = "添付メール送信通知";
				break;
			case ViCommConst.SendMailUnauthType.CAST_STOCK_MAIL_PIC:
				sTitle = "女性メールストック写真投稿通知";
				break;
			case ViCommConst.SendMailUnauthType.CAST_STOCK_MAIL_MOVIE:
				sTitle = "女性メールストック動画投稿通知";
				break;
			case ViCommConst.SendMailUnauthType.CAST_PROFILE_MOVIE:
				sTitle = "女性プロフィール動画投稿通知";
				break;
			case ViCommConst.SendMailUnauthType.CAST_BBS_PIC:
				sTitle = "女性掲示板写真投稿通知";
				break;
			case ViCommConst.SendMailUnauthType.CAST_BBS_MOVIE:
				sTitle = "女性掲示板動画投稿通知";
				break;
			default:
				break;
		}

		return sTitle;
	}
}
