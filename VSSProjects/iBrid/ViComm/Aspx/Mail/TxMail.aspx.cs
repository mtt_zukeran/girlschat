﻿using System;
using System.IO;
using System.Data;
using System.Web;
using System.Net;
using Agiletech.Net.Mail;
using System.Text;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Configuration;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class TxMail:System.Web.UI.Page {
	private string sSiteCd = "";
	private string sMailNo = "";
	private string sUserSeq = "";

	protected void Page_Load(object sender,EventArgs e) {
		sSiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		sMailNo = iBridUtil.GetStringValue(Request.QueryString["mno"]);
		sUserSeq = iBridUtil.GetStringValue(Request.QueryString["useq"]);

		if ((!string.IsNullOrEmpty(sMailNo)) && (!string.IsNullOrEmpty(sUserSeq))) {
			DataSet ds;
			DataRow dr;
			SiteManagement sm;
			string sTitle = string.Empty;
			string sDoc = string.Empty;
			string sFromMailAddr = string.Empty;
			string sToMailAddr = string.Empty;
			if (string.IsNullOrEmpty(sSiteCd)) {
				sSiteCd = PwViCommConst.MAIN_SITE_CD;
			}

			switch (sMailNo) {
				// 手動対応の出演者退会
				case "1":
					CastCharacter cc = new CastCharacter();
					ds = cc.GetOneByUserSeq(sSiteCd,sUserSeq);
					if (ds.Tables[0].Rows.Count > 0) {
						dr = ds.Tables[0].Rows[0];
						sTitle = string.Format("{0}退会申請",iBridUtil.GetStringValue(dr["CAST_NM"]));
						sDoc = string.Format("退会者ログインID:{0}\n",iBridUtil.GetStringValue(dr["LOGIN_ID"]));
						sDoc += string.Format("退会者メールアドレス：{0}\n",iBridUtil.GetStringValue(dr["EMAIL_ADDR"]));
						sDoc += string.Format("退会者ハンドルネーム：{0}\n\n",iBridUtil.GetStringValue(dr["HANDLE_NM"]));
						sm = new SiteManagement();
						sFromMailAddr = sm.GetInfoEmailAddr(sSiteCd);
						sToMailAddr = sFromMailAddr;
					}
					break;
				// 手動対応の会員退会
				case "2":
					UserMan um = new UserMan();
					bool bResult = um.GetCurrentInfo(sSiteCd,sUserSeq);
					if (bResult) {
						sTitle = "会員退会申請";
						sDoc = string.Format("退会者ログインID:{0}\n",um.loginId);
						sDoc += string.Format("退会者メールアドレス：{0}\n",um.emailAddr);
						sDoc += string.Format("退会者ハンドルネーム：{0}\n\n",um.handleNm);
						sm = new SiteManagement();
						sFromMailAddr = sm.GetInfoEmailAddr(sSiteCd);
						sToMailAddr = sFromMailAddr;
					}
					break;
				default:
					break;
			}

			// ﾒｰﾙ送信
			if (!string.IsNullOrEmpty(sToMailAddr) && !string.IsNullOrEmpty(sDoc)) {
				SendMail(sFromMailAddr,sToMailAddr,sTitle,sDoc);
				Response.ContentType = "text/html";
				Response.Write("result=0");
				Response.End();
			}
		}

		Response.ContentType = "text/html";
		Response.Write("result=-1");
		Response.End();
	}


	private void SendMail(string fromAddress,string toAddress,string title,string contents) {
		string sTestHost = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["SmtpTestHost"]);
		string sSendMailEncode = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["SendMailEncode"]).Trim();
		if (string.IsNullOrEmpty(sSendMailEncode)) {
			sSendMailEncode = "Shift_JIS";
		}
		Encoding enc = Encoding.GetEncoding(sSendMailEncode);

		Agiletech.Net.Mail.MailMessage msg = new Agiletech.Net.Mail.MailMessage();

		msg.SubjectEncoding = enc;
		msg.BodyEncoding = enc;
		msg.From = new Agiletech.Net.Mail.MailAddress(fromAddress);
		msg.To.Add(new Agiletech.Net.Mail.MailAddress(toAddress));
		msg.Subject = title;
		msg.Body = contents;
		msg.Headers.HeaderItems.Add("Date",DateTime.Now.ToString("R").Replace("GMT","+0900"));

		Agiletech.Net.Mail.SMTPClient sc = new Agiletech.Net.Mail.SMTPClient(string.Empty);
		if (sTestHost.Equals(string.Empty)) {
			sc.Server = "127.0.0.1";
		} else {
			sc.Server = sTestHost;
		}
		sc.SendMail(msg);
	}

}
