﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Mail
--	Title			: プッシュ通知一括送信
--	Progaram ID		: SendMailAppBatch
--  Creation Date	: 2014.03.21
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.IO;
using System.Data;
using System.Web;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Runtime.Serialization.Json;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class SendMailAppBatch:System.Web.UI.Page {
	private static readonly Regex regEmoji = new Regex(@"(\$x.{4};|&#\d+;)",RegexOptions.Compiled);
	private static readonly Regex regHandleNm = new Regex("(%%4%%)",RegexOptions.Compiled);

	protected void Page_Load(object sender,EventArgs e) {
		string sRequestTxMailSeq = iBridUtil.GetStringValue(Request.QueryString["reqseq"]);
		string sMailAppBatchPath = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["MailAppBatchPath"]);
		string sMailAppNm = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["MailAppNm"]);
		string sMailAppLogFlag = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["MailAppLogFlag"]);
		DataSet dsMailLog;

		if (string.IsNullOrEmpty(sRequestTxMailSeq) || string.IsNullOrEmpty(sMailAppBatchPath) || string.IsNullOrEmpty(sMailAppNm)) {
			Response.ContentType = "text/html";
			Response.Write("-1");
			Response.End();
			return;
		}

		string sUrl = string.Format("http://{0}",sMailAppBatchPath);

		using (MailLog oMailLog = new MailLog()) {
			dsMailLog = oMailLog.GetMailAppList(sRequestTxMailSeq);
		}

		if (dsMailLog.Tables[0].Rows.Count > 0) {
			string sTxHandleNm = iBridUtil.GetStringValue(dsMailLog.Tables[0].Rows[0]["TX_HANDLE_NM"]);
			string sTxUserImgPath = iBridUtil.GetStringValue(dsMailLog.Tables[0].Rows[0]["TX_USER_IMG_PATH"]);

			sTxHandleNm = regEmoji.Replace(sTxHandleNm,"[絵]");
			sTxUserImgPath = "/User/ViComm/man/" + sTxUserImgPath;

			DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(MailAppInfo));
			MailAppInfo oMailAppInfo = new MailAppInfo();

			oMailAppInfo.app = sMailAppNm;
			oMailAppInfo.tag = sTxHandleNm;
			oMailAppInfo.img_url = sTxUserImgPath;
			oMailAppInfo.users = new List<MailAppUsers>();

			for (int i = 0;i < dsMailLog.Tables[0].Rows.Count;i++) {
				string sRxUserSeq = iBridUtil.GetStringValue(dsMailLog.Tables[0].Rows[i]["RX_USER_SEQ"]);
				string sRxHandleNm = iBridUtil.GetStringValue(dsMailLog.Tables[0].Rows[i]["RX_HANDLE_NM"]);
				string sOriginalDoc1 = iBridUtil.GetStringValue(dsMailLog.Tables[0].Rows[i]["ORIGINAL_DOC1"]);
				string sMobileMailGeneral3 = iBridUtil.GetStringValue(dsMailLog.Tables[0].Rows[i]["MOBILE_MAIL_GENERAL3"]);
				string sDeviceToken = iBridUtil.GetStringValue(dsMailLog.Tables[0].Rows[i]["DEVICE_TOKEN"]);

				sOriginalDoc1 = sOriginalDoc1.Replace(System.Environment.NewLine," ");
				sOriginalDoc1 = regHandleNm.Replace(sOriginalDoc1,sRxHandleNm);
				sOriginalDoc1 = regEmoji.Replace(sOriginalDoc1,"[絵]");
				sOriginalDoc1 = SysPrograms.Substring(sOriginalDoc1,20);

				MailAppUsers oMailAppUsers = new MailAppUsers();
				oMailAppUsers.token_id = sDeviceToken;
				oMailAppUsers.msg = sOriginalDoc1;
				oMailAppUsers.userseq = sRxUserSeq;
				oMailAppUsers.detail_url = sMobileMailGeneral3;

				oMailAppInfo.users.Add(oMailAppUsers);
			}

			MemoryStream stream = new MemoryStream();
			serializer.WriteObject(stream,oMailAppInfo);

			stream.Position = 0;
			StreamReader reader = new StreamReader(stream);
			string sPostData = reader.ReadToEnd();

			bool bLogging = false;

			if (sMailAppLogFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				bLogging = true;
			}

			ViCommInterface.PostToParent(sUrl,sPostData,true,bLogging,0);

			Response.ContentType = "text/html";
			Response.Write("0");
			Response.End();
			return;
		} else {
			Response.ContentType = "text/html";
			Response.Write("-2");
			Response.End();
			return;
		}
	}
}
