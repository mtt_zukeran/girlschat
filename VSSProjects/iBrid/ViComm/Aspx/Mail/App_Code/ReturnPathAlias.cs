﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: Return-Pathエイリアス設定--	Progaram ID		: ReturnPathAlias
--  Creation Date	: 2015.07.09
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;

public class ReturnPathAlias:DbSession {
	public ReturnPathAlias() {
	}

	public string GetAliasNameRandom() {
		string sAliasName = string.Empty;

		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	ALIAS_NAME");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	(SELECT");
		oSqlBuilder.AppendLine("		ALIAS_NAME");
		oSqlBuilder.AppendLine("	FROM");
		oSqlBuilder.AppendLine("		T_RETURN_PATH_ALIAS");
		oSqlBuilder.AppendLine("	WHERE");
		oSqlBuilder.AppendLine("		USED_NOW_FLAG = 1");
		oSqlBuilder.AppendLine("	ORDER BY DBMS_RANDOM.RANDOM)");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("  ROWNUM <= 1");

		DataSet ds = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		if (ds.Tables[0].Rows.Count > 0) {
			sAliasName = iBridUtil.GetStringValue(ds.Tables[0].Rows[0]["ALIAS_NAME"]);
		}

		return sAliasName;
	}
}
