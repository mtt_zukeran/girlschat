﻿/*************************************************************************
--	System			: ViCOMM Site
--	Sub System Name	: Mail管理

--	Title			: メール記録
--	Progaram ID		: MailLog
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class MailLog:DbSession {

	public MailLog() {
	}

	public void UpdateMailDoc(string pMailSeq,string pMailTitle,string pMailDoc,long pMimeSize,string pTextMailFlag){
		string[] sDoc;
		int iDocCount;

		SysPrograms.SeparateHtml(pMailDoc,ViCommConst.MAX_ORG_MAIL_BLOCKS,  out sDoc,out iDocCount);

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_MAIL_DOC");
			db.ProcedureInParm("PMAIL_SEQ",DbSession.DbType.VARCHAR2,pMailSeq);
			db.ProcedureInParm("PMAIL_TITLE",DbSession.DbType.VARCHAR2,pMailTitle);
			db.ProcedureInArrayParm("PMAIL_DOC",DbSession.DbType.VARCHAR2,iDocCount,sDoc);
			db.ProcedureInParm("PMAIL_DOC_COUNT", DbSession.DbType.NUMBER, iDocCount);
			db.ProcedureInParm("pMIME_SIZE", DbSession.DbType.NUMBER, pMimeSize);
			db.ProcedureInParm("pTEXT_MAIL_FLAG",DbSession.DbType.NUMBER,pTextMailFlag);
			db.ProcedureOutParm("PSTATUS", DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public DataSet GetMailAppData(string pMailSeq) {
		StringBuilder sSql = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		bool bIsJealousyProfilePic = false;
		using (ManageCompany oManageCompany = new ManageCompany()) {
			bIsJealousyProfilePic = oManageCompany.IsAvailableService(ViCommConst.RELEASE_ENABLED_JEALOUSY_PROFILE_PIC,2);
		}

		sSql.AppendLine("SELECT");
		sSql.AppendLine("	M.RX_USER_SEQ,");
		sSql.AppendLine("	M.TX_HANDLE_NM,");
		if (bIsJealousyProfilePic) {
			sSql.AppendLine("	CASE");
			sSql.AppendLine("		WHEN M.TX_SEX_CD = '3' AND F.PROFILE_PIC_NON_PUBLISH_FLAG = '2' THEN");
			sSql.AppendLine("			GET_PHOTO_IMG_PATH(M.TX_SITE_CD,NULL,NULL)");
			sSql.AppendLine("	ELSE");
			sSql.AppendLine("		M.TX_USER_IMG_PATH");
			sSql.AppendLine("	END	TX_USER_IMG_PATH,");
		} else {
			sSql.AppendLine("	M.TX_USER_IMG_PATH,");
		}
		sSql.AppendLine("	M.ORIGINAL_DOC1,");
		sSql.AppendLine("	M.MOBILE_MAIL_GENERAL3,");
		sSql.AppendLine("	U.DEVICE_TOKEN");
		sSql.AppendLine("FROM");
		sSql.AppendLine("	(SELECT");
		sSql.AppendLine("		RX_SITE_CD,");
		sSql.AppendLine("		RX_USER_SEQ,");
		sSql.AppendLine("		RX_USER_CHAR_NO,");
		sSql.AppendLine("		TX_SITE_CD,");
		sSql.AppendLine("		TX_SEX_CD,");
		sSql.AppendLine("		TX_USER_SEQ,");
		sSql.AppendLine("		TX_USER_CHAR_NO,");
		sSql.AppendLine("		TX_HANDLE_NM,");
		sSql.AppendLine("		TX_USER_IMG_PATH,");
		sSql.AppendLine("		ORIGINAL_DOC1,");
		sSql.AppendLine("		MOBILE_MAIL_GENERAL3");
		sSql.AppendLine("	FROM");
		sSql.AppendLine("		VW_MAIL_LOG01");
		sSql.AppendLine("	WHERE");
		sSql.AppendLine("		MAIL_SEQ = :MAIL_SEQ");
		sSql.AppendLine("	) M,");
		sSql.AppendLine("	(SELECT");
		sSql.AppendLine("		USER_SEQ,");
		sSql.AppendLine("		DEVICE_TOKEN");
		sSql.AppendLine("	FROM");
		sSql.AppendLine("		T_USER");
		sSql.AppendLine("	WHERE");
		sSql.AppendLine("		DEVICE_TOKEN IS NOT NULL");
		sSql.AppendLine("	) U");
		if (bIsJealousyProfilePic) {
			sSql.AppendLine(",T_FAVORIT F");
		}
		sSql.AppendLine("WHERE");
		sSql.AppendLine("	M.RX_USER_SEQ = U.USER_SEQ");
		if (bIsJealousyProfilePic) {
			sSql.AppendLine("	AND M.RX_SITE_CD = F.SITE_CD (+)");
			sSql.AppendLine("	AND M.RX_USER_SEQ = F.PARTNER_USER_SEQ (+)");
			sSql.AppendLine("	AND M.RX_USER_CHAR_NO = F.PARTNER_USER_CHAR_NO (+)");
			sSql.AppendLine("	AND M.TX_USER_SEQ = F.USER_SEQ (+)");
			sSql.AppendLine("	AND M.TX_USER_CHAR_NO = F.USER_CHAR_NO (+)");
		}

		oParamList.Add(new OracleParameter(":MAIL_SEQ",pMailSeq));

		DataSet oDataSet = ExecuteSelectQueryBase(sSql,oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetMailAppList(string pRequestTxMailSeq) {
		StringBuilder sSql = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		bool bIsJealousyProfilePic = false;
		using (ManageCompany oManageCompany = new ManageCompany()) {
			bIsJealousyProfilePic = oManageCompany.IsAvailableService(ViCommConst.RELEASE_ENABLED_JEALOUSY_PROFILE_PIC,2);
		}

		sSql.AppendLine("SELECT");
		sSql.AppendLine("	M.RX_USER_SEQ,");
		sSql.AppendLine("	M.TX_HANDLE_NM,");
		sSql.AppendLine("	M.RX_HANDLE_NM,");
		if (bIsJealousyProfilePic) {
			sSql.AppendLine("	CASE");
			sSql.AppendLine("		WHEN M.TX_SEX_CD = '3' AND F.PROFILE_PIC_NON_PUBLISH_FLAG = '2' THEN");
			sSql.AppendLine("			GET_PHOTO_IMG_PATH(M.TX_SITE_CD,NULL,NULL)");
			sSql.AppendLine("	ELSE");
			sSql.AppendLine("		M.TX_USER_IMG_PATH");
			sSql.AppendLine("	END	TX_USER_IMG_PATH,");
		} else {
			sSql.AppendLine("	M.TX_USER_IMG_PATH,");
		}
		sSql.AppendLine("	M.ORIGINAL_DOC1,");
		sSql.AppendLine("	M.MOBILE_MAIL_GENERAL3,");
		sSql.AppendLine("	U.DEVICE_TOKEN");
		sSql.AppendLine("FROM");
		sSql.AppendLine("	(SELECT");
		sSql.AppendLine("		RX_SITE_CD,");
		sSql.AppendLine("		RX_USER_SEQ,");
		sSql.AppendLine("		RX_USER_CHAR_NO,");
		sSql.AppendLine("		RX_HANDLE_NM,");
		sSql.AppendLine("		TX_SITE_CD,");
		sSql.AppendLine("		TX_SEX_CD,");
		sSql.AppendLine("		TX_USER_SEQ,");
		sSql.AppendLine("		TX_USER_CHAR_NO,");
		sSql.AppendLine("		TX_HANDLE_NM,");
		sSql.AppendLine("		TX_USER_IMG_PATH,");
		sSql.AppendLine("		ORIGINAL_DOC1,");
		sSql.AppendLine("		MOBILE_MAIL_GENERAL3");
		sSql.AppendLine("	FROM");
		sSql.AppendLine("		VW_MAIL_LOG01");
		sSql.AppendLine("	WHERE");
		sSql.AppendLine("		REQUEST_TX_MAIL_SEQ = :REQUEST_TX_MAIL_SEQ");
		sSql.AppendLine("	) M,");
		sSql.AppendLine("	(SELECT");
		sSql.AppendLine("		USER_SEQ,");
		sSql.AppendLine("		DEVICE_TOKEN");
		sSql.AppendLine("	FROM");
		sSql.AppendLine("		T_USER");
		sSql.AppendLine("	WHERE");
		sSql.AppendLine("		DEVICE_TOKEN IS NOT NULL");
		sSql.AppendLine("	) U");
		if (bIsJealousyProfilePic) {
			sSql.AppendLine(",T_FAVORIT F");
		}
		sSql.AppendLine("WHERE");
		sSql.AppendLine("	M.RX_USER_SEQ = U.USER_SEQ");
		if (bIsJealousyProfilePic) {
			sSql.AppendLine("	AND M.RX_SITE_CD = F.SITE_CD (+)");
			sSql.AppendLine("	AND M.RX_USER_SEQ = F.PARTNER_USER_SEQ (+)");
			sSql.AppendLine("	AND M.RX_USER_CHAR_NO = F.PARTNER_USER_CHAR_NO (+)");
			sSql.AppendLine("	AND M.TX_USER_SEQ = F.USER_SEQ (+)");
			sSql.AppendLine("	AND M.TX_USER_CHAR_NO = F.USER_CHAR_NO (+)");
		}

		oParamList.Add(new OracleParameter(":REQUEST_TX_MAIL_SEQ",pRequestTxMailSeq));

		DataSet oDataSet = ExecuteSelectQueryBase(sSql,oParamList.ToArray());

		return oDataSet;
	}
}
