﻿/*************************************************************************
--	System			: ViCOMM Site
--	Sub System Name	: Mail管理
--	Title			: メール配信要求明細
--	Progaram ID		: ReqTxMailDtl
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using iBridCommLib;
using ViComm;

public class ReqTxMailDtl:DbSession {


	public ReqTxMailDtl() {
	}

	public DataSet GetList(string pRequestTxMailSeq) {
		DataSet ds;

		bool bIsJealousyProfilePic = false;
		using (ManageCompany oManageCompany = new ManageCompany()) {
			bIsJealousyProfilePic = oManageCompany.IsAvailableService(ViCommConst.RELEASE_ENABLED_JEALOUSY_PROFILE_PIC,2);
		}

		conn = DbConnect();
		ds = new DataSet();

 		StringBuilder sSql = new StringBuilder();
		sSql.Append("SELECT								").AppendLine();
		sSql.Append("	T1.REQUEST_TX_MAIL_SEQ			,").AppendLine();
		sSql.Append("	T1.REQUEST_TX_MAIL_SUBSEQ		,").AppendLine();
		sSql.Append("	T1.USER_MAIL_RX_TYPE			,").AppendLine();
		sSql.Append("	T1.INFO_MAIL_RX_TYPE			,").AppendLine();
		sSql.Append("	T1.NON_EXIST_MAIL_ADDR_FLAG		,").AppendLine();
		sSql.Append("	T1.TX_MULLER3_NG_MAIL_ADDR_FLAG	,").AppendLine();
		sSql.Append("	T1.USER_STATUS					,").AppendLine();
		sSql.Append("	T1.REQUEST_TX_MAIL_SEQ			,").AppendLine();
		sSql.Append("	T1.REQUEST_TX_MAIL_SUBSEQ		,").AppendLine();
		sSql.Append("	T1.USER_MAIL_RX_TYPE			,").AppendLine();
		sSql.Append("	T1.INFO_MAIL_RX_TYPE			,").AppendLine();
		sSql.Append("	T1.BLOG_MAIL_RX_TYPE			,").AppendLine();
		sSql.Append("	T1.MOBILE_MAIL_RX_TIME_USE_FLAG	,").AppendLine();
		sSql.Append("	T1.MOBILE_MAIL_RX_START_TIME	,").AppendLine();
		sSql.Append("	T1.MOBILE_MAIL_RX_END_TIME		,").AppendLine();
		sSql.Append("	T1.MOBILE_MAIL_RX_TIME_USE_FLAG2,").AppendLine();
		sSql.Append("	T1.MOBILE_MAIL_RX_START_TIME2	,").AppendLine();
		sSql.Append("	T1.MOBILE_MAIL_RX_END_TIME2		,").AppendLine();
		sSql.Append("	T1.RX_SITE_CD					,").AppendLine();
		sSql.Append("	T1.RX_EMAIL_ADDR				,").AppendLine();
		sSql.Append("	T1.RX_LOGIN_ID					,").AppendLine();
		sSql.Append("	T1.RX_LOGIN_PASSWORD			,").AppendLine();
		sSql.Append("	NVL(T1.RX_HANDLE_NM,RX_CAST_NM) AS RX_HANDLE_NM	,").AppendLine();
		sSql.Append("	T1.RX_USER_SEQ					,").AppendLine();
		sSql.Append("	T1.RX_USER_CHAR_NO				,").AppendLine();
		sSql.Append("	T1.RX_SEX_CD					,").AppendLine();
		sSql.Append("	T1.TX_HANDLE_NM					,").AppendLine();
		sSql.Append("	T1.TX_LOGIN_ID					,").AppendLine();
		sSql.Append("	T1.TX_USER_CHAR_NO				,").AppendLine();
		sSql.Append("	T1.TX_IMG_PATH					,").AppendLine();
		if (bIsJealousyProfilePic) {
			sSql.Append("	CASE							").AppendLine();
			sSql.Append("		WHEN T1.TX_SEX_CD = '3' AND T3.PROFILE_PIC_NON_PUBLISH_FLAG = '2' THEN	").AppendLine();
			sSql.Append("			'http://' || HOST_NM || '/user/vicomm/woman/' || GET_PHOTO_IMG_PATH(T1.TX_SITE_CD,NULL,NULL)	").AppendLine();
			sSql.Append("	ELSE							").AppendLine();
			sSql.Append("		T1.TX_USER_IMG_FULL_PATH	").AppendLine();
			sSql.Append("	END	TX_USER_IMG_FULL_PATH		,").AppendLine();
			sSql.Append("	T3.PROFILE_PIC_NON_PUBLISH_FLAG	,").AppendLine();
		} else {
			sSql.Append("	T1.TX_USER_IMG_FULL_PATH		,").AppendLine();
			sSql.Append("	'0'	AS PROFILE_PIC_NON_PUBLISH_FLAG	,").AppendLine();
		}
		sSql.Append("	T1.TX_ATTR1						,").AppendLine();
		sSql.Append("	T1.TX_ATTR2						,").AppendLine();
		sSql.Append("	T1.TX_AGE						,").AppendLine();
		sSql.Append("	T1.OBJ_SEQ						,").AppendLine();
		sSql.Append("	T1.TX_SEX_CD					,").AppendLine();
		sSql.Append("	T1.CRYPT_VALUE					,").AppendLine();
		sSql.Append("	T1.CHAR_NO_ITEM_NM				,").AppendLine();
		sSql.Append("	T1.GENERAL1						,").AppendLine();
		sSql.Append("	T1.GENERAL2						,").AppendLine();
		sSql.Append("	T1.GENERAL3						,").AppendLine();
		sSql.Append("	T1.GENERAL4						,").AppendLine();
		sSql.Append("	T1.GENERAL5						,").AppendLine();
		sSql.Append("	T1.WEB_MAIL_GENERAL1			,").AppendLine();
		sSql.Append("	T1.WEB_MAIL_GENERAL2			,").AppendLine();
		sSql.Append("	T1.WEB_MAIL_GENERAL3			,").AppendLine();
		sSql.Append("	T1.MOBILE_MAIL_GENERAL1			,").AppendLine();
		sSql.Append("	T1.MOBILE_MAIL_GENERAL2			,").AppendLine();
		sSql.Append("	T1.MOBILE_MAIL_GENERAL3			,").AppendLine();
		sSql.Append("	T1.MAIL_SEQ						,").AppendLine();
		sSql.Append("	T1.SMTP_SERVER_NO				,").AppendLine();
		sSql.Append("	T1.FORCE_TX_MOBILE_MAIL_FLAG	,").AppendLine();
		sSql.Append("	T1.MAIL_TEMPLATE_NO				,").AppendLine();
		sSql.Append("	T2.REGIST_DATE	AS FAVORIT_REGIST_DATE,").AppendLine();
		sSql.Append("	T2.DEL_FLAG		AS FAVORIT_DEL_FLAG	  ").AppendLine();
		sSql.Append("FROM").AppendLine();
		sSql.Append("	VW_REQ_TX_MAIL_DTL01 T1,T_FAVORIT T2");
		if (bIsJealousyProfilePic) {
			sSql.Append(",T_FAVORIT T3").AppendLine();
		} else {
			sSql.AppendLine();
		}
		sSql.Append("WHERE								").AppendLine();
		sSql.Append("	T1.REQUEST_TX_MAIL_SEQ= :REQUEST_TX_MAIL_SEQ AND").AppendLine();
		sSql.Append("	T1.RX_SITE_CD		= T2.SITE_CD				(+)	AND").AppendLine();
		sSql.Append("	T1.RX_USER_SEQ		= T2.USER_SEQ				(+)	AND").AppendLine();
		sSql.Append("	T1.RX_USER_CHAR_NO	= T2.USER_CHAR_NO			(+)	AND").AppendLine();
		sSql.Append("	T1.TX_USER_SEQ		= T2.PARTNER_USER_SEQ		(+)	AND").AppendLine();
		sSql.Append("	T1.TX_USER_CHAR_NO	= T2.PARTNER_USER_CHAR_NO	(+)	");
		if (bIsJealousyProfilePic) {
			sSql.AppendLine("AND");
			sSql.Append("	T1.RX_SITE_CD		= T3.SITE_CD				(+)	AND").AppendLine();
			sSql.Append("	T1.RX_USER_SEQ		= T3.PARTNER_USER_SEQ		(+)	AND").AppendLine();
			sSql.Append("	T1.RX_USER_CHAR_NO	= T3.PARTNER_USER_CHAR_NO	(+)	AND").AppendLine();
			sSql.Append("	T1.TX_USER_SEQ		= T3.USER_SEQ				(+)	AND").AppendLine();
			sSql.Append("	T1.TX_USER_CHAR_NO	= T3.USER_CHAR_NO			(+)").AppendLine();
		} else {
			sSql.AppendLine();
		}
		sSql.Append("ORDER BY").AppendLine();
		sSql.Append("	T1.REQUEST_TX_MAIL_SEQ,T1.REQUEST_TX_MAIL_SUBSEQ").AppendLine();

		using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
			cmd.Parameters.Add("REQUEST_TX_MAIL_SEQ",pRequestTxMailSeq);
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds);
			}
		}
		conn.Close();
		return ds;
	}

	public void RefuseTxMail(string pRequestTxMailSeq,string pRequestTxMailSubSeq) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("REFUSE_TX_MAIL");
			db.ProcedureInParm("PREQUEST_TX_MAIL_SEQ",DbSession.DbType.VARCHAR2,pRequestTxMailSeq);
			db.ProcedureInParm("PREQUEST_TX_MAIL_SUBSEQ",DbSession.DbType.VARCHAR2,pRequestTxMailSubSeq);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}
}
