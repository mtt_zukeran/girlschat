﻿/*************************************************************************
--	System			: ViCOMM Site
--	Sub System Name	: Mail管理
--	Title			: メールテンプレート
--	Progaram ID		: MailTemplate
--
--  Creation Date	: 2011.05.26
--  Creater			: kito
--
**************************************************************************/
using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

public class MailTemplate :DbSession {
	public MailTemplate() {}

	public void ModifyMailTemplateSize(string pSiteCd, string pMailTemplateNo, long pMimeSize) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MODIFY_MAIL_TEMPLATE_SIZE");
			db.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, pSiteCd);
			db.ProcedureInParm("pMAIL_TEMPLATE_NO", DbSession.DbType.VARCHAR2, pMailTemplateNo);
			db.ProcedureInParm("pMIME_SIZE", DbSession.DbType.NUMBER, pMimeSize);
			db.ProcedureOutParm("PSTATUS", DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}	
}
