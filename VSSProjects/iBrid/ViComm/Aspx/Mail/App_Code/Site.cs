﻿/*************************************************************************
--	System			: ViCOMM Site
--	Sub System Name	: Mail管理
--	Title			: サイト
--	Progaram ID		: Site
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class Site:DbSession {
	public string txHiMailerIP;
	public string txLoMailerIP;
	public string url;
	public string subHostNm;
	public int mailDocNotUseWebFaceFlag;

	public Site() {
	}

	public bool GetOne(string pSiteCd) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;

		conn = DbConnect();
		string sSql = "SELECT " +
							"TX_HI_MAILER_IP	," +
							"TX_LO_MAILER_IP	," +
							"SUB_HOST_NM		," + 
							"URL				," +
							"MAIL_DOC_NOT_USE_WEB_FACE_FLAG	" +
						"FROM " +
							"T_SITE " +
						"WHERE " +
							"SITE_CD = :SITE_CD ";

		using (cmd = CreateSelectCommand(sSql,conn))
		using (ds = new DataSet()) {

			cmd.Parameters.Add("SITE_CD",pSiteCd);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"T_SITE");
				if (ds.Tables["T_SITE"].Rows.Count != 0) {
					dr = ds.Tables["T_SITE"].Rows[0];
					txHiMailerIP = dr["TX_HI_MAILER_IP"].ToString();
					txLoMailerIP = dr["TX_LO_MAILER_IP"].ToString();
					subHostNm = dr["SUB_HOST_NM"].ToString();
					url = dr["URL"].ToString();
					mailDocNotUseWebFaceFlag = int.Parse(dr["MAIL_DOC_NOT_USE_WEB_FACE_FLAG"].ToString());
					bExist = true;
				}
			}
		}
		conn.Close();
		return bExist;
	}

	public bool GetValue(string pSiteCd,string pItem,ref string pValue) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;

		conn = DbConnect();
		string sSql = "SELECT " +
							"T_SITE.SITE_CD				," +
							"T_SITE.SITE_NM				," +
							"T_SITE.MAIL_HOST			," +
							"T_SITE.MAILER_FTP_ID		," +
							"T_SITE.MAILER_FTP_PW		," +
							"T_SITE.TX_HI_MAILER_IP		," +
							"T_SITE.WEB_PHISICAL_DIR	," +
							"T_SITE_MANAGEMENT_EX.USER_MAIL_NOTICE_LENGTH	" +
						"FROM " +
							"T_SITE						," +
							"T_SITE_MANAGEMENT_EX		" +
						"WHERE " +
							"T_SITE.SITE_CD = :SITE_CD AND T_SITE.SITE_CD = T_SITE_MANAGEMENT_EX.SITE_CD (+)";

		using (cmd = CreateSelectCommand(sSql,conn))
		using (ds = new DataSet()) {

			cmd.Parameters.Add("SITE_CD",pSiteCd);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"T_SITE");
				if (ds.Tables["T_SITE"].Rows.Count != 0) {
					dr = ds.Tables["T_SITE"].Rows[0];
					pValue = dr[pItem].ToString();
					bExist = true;
				}
			}
		}
		conn.Close();
		return bExist;
	}


}
