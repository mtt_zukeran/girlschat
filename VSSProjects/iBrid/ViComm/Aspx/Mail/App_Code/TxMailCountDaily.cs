﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Mail
--	Title			: 日別メール送信数データクラス
--	Progaram ID		: TxMailCountDaily
--  Creation Date	: 2015.06.02
--  Creater			: M&TT Y.Ikemiya
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using iBridCommLib;
using ViComm;

public class TxMailCountDaily:DbSession {
	public TxMailCountDaily() {
	}

	public void RegistTxMailCountDaily(string pSiteCd,string pMailTemplateNo,int pTxMailCount,string pSexCd) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("REGIST_TX_MAIL_COUNT_DAILY");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pMAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2,pMailTemplateNo);
			db.ProcedureInParm("pTX_MAIL_COUNT",DbSession.DbType.NUMBER,pTxMailCount);
			db.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,pSexCd);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();
		}
	}
}
