﻿/*************************************************************************
--	System			: ViCOMM Site
--	Sub System Name	: Mail管理
--	Title			: 仮登録
--	Progaram ID		: TempRegist
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Text;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class TempRegist : DbSession {
    public string emailAddr;
    public string siteCd;
    public string tel;
    public string castNm;
    public string castKanaNm;
    public string birthday;
    public string adNm;
    public string questionDoc;
    public string friendCd;
    public string friendLoginId;

    public string tempRegistId;
    public string registAffiliateCd;
    public string trackingUrl;
    public string trackingAdditionInfo;
    public string loginId;
    public string registStatus;
    public string registIpAddr;
    public string imodeId;
    public string terminalUniqueId;
    public string carrierCd;

    public TempRegist() {
    }

    public bool GetOne(string pTempRegistId) {
        DataSet ds;
        DataRow dr;
        bool bExist = false;

        conn = DbConnect();
        StringBuilder sSql = new StringBuilder();
        sSql.Append("SELECT ").AppendLine();
        sSql.Append("	T1.TEMP_REGIST_ID			,").AppendLine();
        sSql.Append("	T1.EMAIL_ADDR				,").AppendLine();
        sSql.Append("	T1.SITE_CD					,").AppendLine();
        sSql.Append("	T1.AD_CD					,").AppendLine();
        sSql.Append("	T1.EMAIL_ADDR				,").AppendLine();
        sSql.Append("	T1.MOBILE_CARRIER_CD		,").AppendLine();
        sSql.Append("	T1.MOBILE_TERMINAL_NM		,").AppendLine();
        sSql.Append("	T1.REGIST_APPLY_DATE		,").AppendLine();
        sSql.Append("	T1.REGIST_COMPLITE_DATE		,").AppendLine();
        sSql.Append("	T1.USER_SEQ					,").AppendLine();
        sSql.Append("	T1.REGIST_AFFILIATE_CD		,").AppendLine();
        sSql.Append("	T1.AFFILIATER_CD			,").AppendLine();
        sSql.Append("	T1.TEL						,").AppendLine();
        sSql.Append("	T1.CAST_NM					,").AppendLine();
        sSql.Append("	T1.CAST_KANA_NM				,").AppendLine();
        sSql.Append("	T1.BIRTHDAY					,").AppendLine();
        sSql.Append("	T1.INTRODUCER_FRIEND_CD		,").AppendLine();
        sSql.Append("	T1.QUESTION_DOC				,").AppendLine();
        sSql.Append("	T2.AD_NM					 ").AppendLine();
        sSql.Append("FROM ").AppendLine();
        sSql.Append("	T_TEMP_REGIST T1,T_AD T2").AppendLine();
        sSql.Append("WHERE ").AppendLine();
        sSql.Append("	T1.TEMP_REGIST_ID	= :TEMP_REGIST_ID AND").AppendLine();
        sSql.Append("	T1.AD_CD			= T2.AD_CD	(+)	  ").AppendLine();

        using (cmd = CreateSelectCommand(sSql.ToString(),conn))
        using (ds = new DataSet()) {

            cmd.Parameters.Add("TEMP_REGIST_ID",pTempRegistId);

            using (da = new OracleDataAdapter(cmd)) {
                da.Fill(ds,"T_TEMP_REGIST");
                if (ds.Tables["T_TEMP_REGIST"].Rows.Count != 0) {
                    dr = ds.Tables["T_TEMP_REGIST"].Rows[0];
                    siteCd = dr["SITE_CD"].ToString();
                    emailAddr = dr["EMAIL_ADDR"].ToString();
                    tel = dr["TEL"].ToString();
                    castNm = dr["CAST_NM"].ToString();
                    castKanaNm = dr["CAST_KANA_NM"].ToString();
                    birthday = dr["BIRTHDAY"].ToString();
                    adNm = dr["AD_NM"].ToString();
                    friendCd = dr["INTRODUCER_FRIEND_CD"].ToString();
                    questionDoc = dr["QUESTION_DOC"].ToString();
                    friendLoginId = "";
                    bExist = true;
                }
            }
        }
        conn.Close();
        if (!friendCd.Equals(string.Empty)) {
            using (CastCharacter oCharacter = new CastCharacter()) {
                friendLoginId = oCharacter.GetLoginIdByFriendCd(friendCd);
            }
        }

        return bExist;
    }

    public bool GetOneByUserSeq(string pUserSeq) {
        DataSet ds;
        DataRow dr;

        bool bExist = false;
        try {
            conn = DbConnect("TempRegist.GetOne");

            StringBuilder sSql = new StringBuilder();
            sSql.Append("SELECT ").AppendLine();
            sSql.Append("	REGIST_AFFILIATE_CD		,").AppendLine();
            sSql.Append("	TRACKING_URL  			,").AppendLine();
            sSql.Append("	TRACKING_ADDITION_INFO	,").AppendLine();
            sSql.Append("	REGIST_STATUS			,").AppendLine();
            sSql.Append("   REGIST_IP_ADDR          ,").AppendLine();
            sSql.Append("   TERMINAL_UNIQUE_ID      ,").AppendLine();
            sSql.Append("   IMODE_ID                ,").AppendLine();
            sSql.Append("   MOBILE_CARRIER_CD       ,").AppendLine();
            sSql.Append("	LOGIN_ID				").AppendLine();
            sSql.Append("FROM	").AppendLine();
            sSql.Append("	VW_TEMP_REGIST03		").AppendLine();
            sSql.Append("WHERE	").AppendLine();
            sSql.Append("	USER_SEQ = :USER_SEQ ");

            using (cmd = CreateSelectCommand(sSql.ToString(),conn))
            using (ds = new DataSet()) {
                cmd.Parameters.Add("USER_SEQ",pUserSeq);

                using (da = new OracleDataAdapter(cmd)) {
                    da.Fill(ds,"VW_TEMP_REGIST03");

                    if (ds.Tables["VW_TEMP_REGIST03"].Rows.Count != 0) {
                        dr = ds.Tables["VW_TEMP_REGIST03"].Rows[0];
                        registAffiliateCd = dr["REGIST_AFFILIATE_CD"].ToString();
                        trackingUrl = dr["TRACKING_URL"].ToString();
                        trackingAdditionInfo = dr["TRACKING_ADDITION_INFO"].ToString();
                        loginId = dr["LOGIN_ID"].ToString();
                        registStatus = dr["REGIST_STATUS"].ToString();
                        registIpAddr = dr["REGIST_IP_ADDR"].ToString();
                        terminalUniqueId = dr["TERMINAL_UNIQUE_ID"].ToString();
                        imodeId = dr["IMODE_ID"].ToString();
                        carrierCd = dr["MOBILE_CARRIER_CD"].ToString();
                        bExist = true;
                    }
                }
            }
        } finally {
            conn.Close();
        }
        return bExist;
    }

    public DataSet GetBatchList(DateTime pFromDate) {
        DataSet ds = new DataSet();

        try {
            conn = DbConnect("TempRegist.GetBatchList");

            StringBuilder sSql = new StringBuilder();
            sSql.Append("SELECT ").AppendLine();
            sSql.Append("	TEMP_REGIST_ID			,").AppendLine();
            sSql.Append("	REGIST_AFFILIATE_CD		,").AppendLine();
            sSql.Append("	TRACKING_URL  			,").AppendLine();
            sSql.Append("	TRACKING_ADDITION_INFO	,").AppendLine();
            sSql.Append("	REGIST_STATUS			,").AppendLine();
            sSql.Append("	TERMINAL_UNIQUE_ID		,").AppendLine();
            sSql.Append("	IMODE_ID    			,").AppendLine();
            sSql.Append("   REGIST_IP_ADDR          ,").AppendLine();
            sSql.Append("   MOBILE_CARRIER_CD       ,").AppendLine();
            sSql.Append("	LOGIN_ID				").AppendLine();
            sSql.Append("FROM	").AppendLine();
            sSql.Append("	VW_TEMP_REGIST03		").AppendLine();
            sSql.Append("WHERE	").AppendLine();
            sSql.Append("	REGIST_STATUS				= :REGIST_STATUS				AND").AppendLine();
            sSql.Append("	REGIST_MAIL_SEND_FAILED_FLAG= :REGIST_MAIL_SEND_FAILED_FLAG	AND").AppendLine();
            sSql.Append("	SEX_CD						= :SEX_CD						AND").AppendLine();
            sSql.Append("	MAN_REGIST_REPORT_TIMING	= :MAN_REGIST_REPORT_TIMING		AND").AppendLine();
            sSql.Append("	REGIST_DATE					<= :REGIST_DATE_FROM			AND").AppendLine();
            sSql.Append("	MOBILE_CARRIER_CD NOT IN (:MOBILE_CARRIER_CD1,:MOBILE_CARRIER_CD2)").AppendLine();


            using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
                cmd.Parameters.Add("REGIST_STATUS",ViCommConst.REGIST_COMPLITE_NO_REPORT);
                cmd.Parameters.Add("REGIST_MAIL_SEND_FAILED_FLAG",ViCommConst.FLAG_OFF);
                cmd.Parameters.Add("SEX_CD",ViCommConst.MAN);
                cmd.Parameters.Add("MAN_REGIST_REPORT_TIMING",ViCommConst.MAN_AFFILIATE_REPORT_MAIL_OK);
                cmd.Parameters.Add(new OracleParameter("REGIST_DATE_FROM",OracleDbType.Date,pFromDate,ParameterDirection.Input));
                cmd.Parameters.Add("MOBILE_CARRIER_CD1",ViCommConst.ANDROID);
                cmd.Parameters.Add("MOBILE_CARRIER_CD2",ViCommConst.IPHONE);

                using (da = new OracleDataAdapter(cmd)) {
                    da.Fill(ds,"VW_TEMP_REGIST03");
                }
            }
        } finally {
            conn.Close();
        }
        return ds;
    }


    public void CompliteTrackingReport(
            string pKey,int pIsTempId
        ) {
        using (DbSession db = new DbSession()) {
            db.PrepareProcedure("UPDATE_TEMP_REGIST_STATUS");
            db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pKey);
            db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
            db.ProcedureInParm("PIS_TEMP_ID",DbSession.DbType.NUMBER,pIsTempId);

            db.ExecuteProcedure();
        }
    }
}
