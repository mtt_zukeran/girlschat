﻿using System;
using System.Web;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Collections.Specialized;
using System.Collections.Generic;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;

public class SiteManagement:DbSession {
	public SiteManagement() {
	}

	/// <summary>
	/// infoメールアドレス取得
	/// </summary>
	/// <param name="pSiteCd"></param>
	/// <returns></returns>
	public string GetInfoEmailAddr(string pSiteCd) {
		string sEmailAddr = string.Empty;

		if (string.IsNullOrEmpty(pSiteCd)) {
			return sEmailAddr;
		}

		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	INFO_EMAIL_ADDR");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_SITE_MANAGEMENT");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		if (oDataSet.Tables[0].Rows.Count > 0) {
			DataRow dr = oDataSet.Tables[0].Rows[0];
			sEmailAddr = iBridUtil.GetStringValue(dr["INFO_EMAIL_ADDR"]);
		}

		return sEmailAddr;
	}
}
