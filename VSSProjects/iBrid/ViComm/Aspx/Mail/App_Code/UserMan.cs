﻿using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Web;
using System.Configuration;
using ViComm;

public class UserMan:DbSession {
	public string siteCd;
	public string userSeq;
	public int balPoint;
	public string userRankCd;
	public string adCd;
	public string tel;
	public string uri;
	public int telAttestedFlag;
	public string webLocCd;
	public string webUserId;
	public bool logined;
	public string tempRegistId;
	public string loginId;
	public string loginPassword;
	public string emailAddr;
	public string rankNm;
	public string handleNm;
	public string birthday;
	public string areaCd;
	public string bloodTypeCd;
	public string favoritTypeCd;
	public string freeTimeCd;
	public string character;
	public string selfIntroduction;

	public UserMan() {
		siteCd = "";
		userSeq = "";
		balPoint = 0;
		userRankCd = "";
		adCd = "";
		tel = "";
		telAttestedFlag = 0;
		logined = false;
		loginId = "";
		loginPassword = "";
		emailAddr="";
		webLocCd = "";
		webUserId = "";
		tempRegistId = "";
		rankNm = "";
		handleNm = "";
		birthday = "";
		areaCd = "";
		bloodTypeCd = "";
		favoritTypeCd = "";
		freeTimeCd = "";
		character = "";
		selfIntroduction = "";
	}

	public bool GetCurrentInfo(string pSiteCd,string pUserSeq) {
		DataSet ds;
		DataRow dr;

		bool bExist = false;

		conn = DbConnect();

		string sSql = "SELECT " +
							"SITE_CD			," +
							"USER_SEQ			," +
							"USER_RANK			," +
							"TEL				," +
							"URI				," +
							"TEL_ATTESTED_FLAG	," +
							"LOGIN_ID			," +
							"LOGIN_PASSWORD		," +
							"EMAIL_ADDR			," +
							"BAL_POINT			," +
							"HANDLE_NM			," +
							//"AREA_CD			," +
							"BIRTHDAY			," +
							//"BLOOD_TYPE_CD		," +
							//"FAVORIT_TYPE_CD	," +
							//"FREE_TIME_CD		," +
							//"CHARACTER			," +
							//"SELF_INTRODUCTION	," +
							"USER_RANK			," +
							"AD_CD				," +
							"USER_RANK_NM " +
						"FROM " +
						" VW_USER_MAN_CHARACTER03 " +
						"WHERE " +
						" SITE_CD = :SITE_CD AND USER_SEQ = :USER_SEQ";

		using (cmd = CreateSelectCommand(sSql,conn))
		using (ds = new DataSet()) {

			cmd.Parameters.Add("SITE_CD",pSiteCd);
			cmd.Parameters.Add("USER_SEQ",pUserSeq);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"VW_USER_MAN_CHARACTER03");

				if (ds.Tables["VW_USER_MAN_CHARACTER03"].Rows.Count != 0) {
					dr = ds.Tables["VW_USER_MAN_CHARACTER03"].Rows[0];
					siteCd = dr["SITE_CD"].ToString();
					userSeq = dr["USER_SEQ"].ToString();
					balPoint = int.Parse(dr["BAL_POINT"].ToString());
					userRankCd = dr["USER_RANK"].ToString();
					adCd = dr["AD_CD"].ToString();
					tel = dr["TEL"].ToString();
					telAttestedFlag = int.Parse(dr["TEL_ATTESTED_FLAG"].ToString());
					loginId = dr["LOGIN_ID"].ToString();
					loginPassword = dr["LOGIN_PASSWORD"].ToString();
					emailAddr = dr["EMAIL_ADDR"].ToString();
					handleNm = dr["HANDLE_NM"].ToString();
					rankNm = dr["USER_RANK_NM"].ToString();
					birthday = dr["BIRTHDAY"].ToString();
					//areaCd = dr["AREA_CD"].ToString();
					//bloodTypeCd = dr["BLOOD_TYPE_CD"].ToString();
					//favoritTypeCd = dr["FAVORIT_TYPE_CD"].ToString();
					//freeTimeCd = dr["FREE_TIME_CD"].ToString();
					//character = dr["CHARACTER"].ToString();
					//selfIntroduction = dr["SELF_INTRODUCTION"].ToString();
					bExist = true;
				}
			}
		}
		conn.Close();
		return bExist;
	}

}
