﻿/*************************************************************************
--	System			: ViCOMM Site
--	Sub System Name	: Mail管理
--	Title			: キャストキャラクター

--	Progaram ID		: CastCharacter
--
--  Creation Date	: 2009.09.08
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Web;
using System.Web.UI;
using System;
using iBridCommLib;
using ViComm;

public class CastCharacter:DbSession {

	public CastCharacter() {
	}

	public bool CheckMobileMailRx(string pSiteCd,string pUserSeq,string pUserCharNo,bool pInfoMail) {
		string sMailRxType = "";
		string sMobileMailRxStartTime = "";
		string sMobileMailRxEndTime = "";
		string sCastOnlineMailRxFlag = "";
		string sOnlineStatus = "";

		DataSet ds;
		DataRow dr;
		bool bExist = false;

		conn = DbConnect();
		string sSql = "SELECT " +
							"CHARACTER_ONLINE_STATUS        ," +
							"MAN_MAIL_RX_TYPE				," +
							"INFO_MAIL_RX_TYPE				," +
							"MOBILE_MAIL_RX_START_TIME		," +
							"MOBILE_MAIL_RX_END_TIME		," +
							"CAST_ONLINE_MAIL_RX_FLAG		" +
						"FROM " +
							"VW_CAST03" +
						"WHERE " +
							"SITE_CD		= :SITE_CD			AND " +
							"USER_SEQ		= :USER_SEQ			AND " +
							"USER_CHAR_NO	= :USER_CHAR_NO	";

		using (cmd = CreateSelectCommand(sSql,conn))
		using (ds = new DataSet()) {

			cmd.Parameters.Add("SITE_CD",pSiteCd);
			cmd.Parameters.Add("USER_SEQ",pUserSeq);
			cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"T_CAST_CHARACTER");
			}
		}
		conn.Close();

		if (ds.Tables["T_CAST_CHARACTER"].Rows.Count != 0) {
			dr = ds.Tables["T_CAST_CHARACTER"].Rows[0];
			sMobileMailRxStartTime = dr["OFFLINE_MAIL_RX_START_TIME"].ToString();
			sMobileMailRxEndTime = dr["OFFLINE_MAIL_RX_END_TIME"].ToString();
			sCastOnlineMailRxFlag = dr["CAST_ONLINE_MAIL_RX_FLAG"].ToString();
			sOnlineStatus = dr["CHARACTER_ONLINE_STATUS"].ToString();

			if (sMobileMailRxEndTime == "00:00") {
				sMobileMailRxEndTime = "24:00";
			}

			if (pInfoMail) {
				sMailRxType = dr["INFO_MAIL_RX_TYPE"].ToString();
			} else {
				sMailRxType = dr["MAN_MAIL_RX_TYPE"].ToString();
			}

			if (sMailRxType.Equals(ViCommConst.MAIL_RX_BOTH)) {
				if (sMobileMailRxStartTime.CompareTo(sMobileMailRxEndTime) > 0) {
					if (DateTime.Now.ToString("HH:mm").CompareTo(sMobileMailRxStartTime) >= 0) {
						bExist = true;
					} else if (DateTime.Now.ToString("HH:mm").CompareTo(sMobileMailRxEndTime) <= 0) {
						bExist = true;
					}
				} else if (DateTime.Now.ToString("HH:mm").CompareTo(sMobileMailRxStartTime) >= 0 && DateTime.Now.ToString("HH:mm").CompareTo(sMobileMailRxEndTime) <= 0) {
					bExist = true;
				}
			}

			//ｵﾝﾗｲﾝ時携帯ﾒｰﾙ送信ﾌﾗｸﾞがOFFの場合、ｵﾝﾗｲﾝだったら携帯にﾒｰﾙを送信しない

			if (bExist == true) {
				if (!sCastOnlineMailRxFlag.Equals("1")) {
					if (sOnlineStatus.Equals(ViCommConst.USER_WAITING.ToString()) ||
						sOnlineStatus.Equals(ViCommConst.USER_TALKING.ToString())) {
						bExist = false;
					}
				}
			}
		}

		return bExist;
	}

	public string GetLoginIdByFriendCd(string pFriendIntroCd) {
		DataSet ds;
		DataRow dr;
		string sLoginId = "";
		conn = DbConnect();

		string sSql = "SELECT LOGIN_ID FROM VW_CAST_CHARACTER01 WHERE FRIEND_INTRO_CD = :FRIEND_INTRO_CD";
		using (cmd = CreateSelectCommand(sSql,conn))
		using (ds = new DataSet()) {
			cmd.Parameters.Add("FRIEND_INTRO_CD",pFriendIntroCd);
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"VW_CAST_CHARACTER01");
				if (ds.Tables["VW_CAST_CHARACTER01"].Rows.Count != 0) {
					dr = ds.Tables["VW_CAST_CHARACTER01"].Rows[0];
					sLoginId = dr["LOGIN_ID"].ToString();
				}
			}
		}
		conn.Close();
		return sLoginId;
	}

	/// <summary>
	/// ユーザSEQからユーザ情報を取得
	/// </summary>
	/// <param name="pSiteCd"></param>
	/// <param name="pUserSeq"></param>
	/// <returns></returns>
	public DataSet GetOneByUserSeq(string pSiteCd,string pUserSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	CC.SITE_CD,");
		oSqlBuilder.AppendLine("	CC.USER_SEQ,");
		oSqlBuilder.AppendLine("	CC.HANDLE_NM,");
		oSqlBuilder.AppendLine("	C.CAST_NM,");
		oSqlBuilder.AppendLine("	U.EMAIL_ADDR,");
		oSqlBuilder.AppendLine("	U.LOGIN_ID");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_USER U");
		oSqlBuilder.AppendLine("	INNER JOIN T_CAST_CHARACTER CC ON U.USER_SEQ = CC.USER_SEQ");
		oSqlBuilder.AppendLine("	INNER JOIN T_CAST C ON U.USER_SEQ = C.USER_SEQ");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	CC.SITE_CD = :SITE_CD");
		oSqlBuilder.AppendLine("	AND U.USER_SEQ = :USER_SEQ");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));

		DataSet ds = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return ds;
	}
}
