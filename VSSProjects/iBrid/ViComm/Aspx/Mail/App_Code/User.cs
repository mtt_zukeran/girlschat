﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Mail
--	Title			: ユーザー
--	Progaram ID		: User
--  Creation Date	: 2014.03.20
--  Creater			: K.Miyazato
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using iBridCommLib;
using ViComm;

public class User:DbSession {
	public User() {
	}

	public void UpdateDeviceToken(string pUserSeq,string pDeviceToken) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_DEVICE_TOKEN");
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pDEVICE_TOKEN",DbSession.DbType.VARCHAR2,pDeviceToken);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();
		}
	}

	public void DeleteDeviceToken(string pUserSeq) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("DELETE_DEVICE_TOKEN");
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();
		}
	}

	/// <summary>
	/// 電話番号、性別CDを元にログインパスワードを取得する
	/// </summary>
	/// <param name="pTel"></param>
	/// <param name="pSexCd"></param>
	/// <returns></returns>
	public DataSet GetOneByTel(string pTel,string pSexCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	LOGIN_PASSWORD,");
		oSqlBuilder.AppendLine("	USER_STATUS");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_USER");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	TEL = :TEL");
		oSqlBuilder.AppendLine("	AND SEX_CD = :SEX_CD");

		oParamList.Add(new OracleParameter(":TEL",pTel));
		oParamList.Add(new OracleParameter(":SEX_CD",pSexCd));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}
}
