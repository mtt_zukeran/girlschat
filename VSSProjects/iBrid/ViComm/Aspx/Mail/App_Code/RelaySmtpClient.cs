﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.IO;

using iBridCommLib;
using MobileLib;

/// <summary>
/// RelaySmtpClient の概要の説明です

/// </summary>
public class RelaySmtpClient:SmtpClientBase {

	public RelaySmtpClient() : base() {
	}
	public RelaySmtpClient(bool pIsTestMode) : base(pIsTestMode) {
	}

	public long SendMail(string pMailFrom,string pMailRturnTo,string pSubjectTemplate,string pHtmlTemplate,string pTextTemplate,List<DataRow> pReqTxMailDtlDataRowList,MailBodyHelper.MailDataFormatter pFormatter) {
		long lDataLength = 0;

		string sMailServer = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["RelayMailServer"]);
		int iMailPort = int.Parse(iBridUtil.GetStringValue(ConfigurationManager.AppSettings["RelayMailPort"]));

		// TEXTメールのBODY
		if (string.IsNullOrEmpty(pTextTemplate.Trim())) {
			pTextTemplate = MailBodyHelper.ConvertHtml2Text(pHtmlTemplate);
		}

		TcpClient oTcpClient = new TcpClient();
		try {
			// メールサーバに接続 
			oTcpClient.Connect(sMailServer,iMailPort);
			using (Stream oStream = oTcpClient.GetStream()) {

				// HELO
				SendHelo(oStream,"localhost");

				foreach (DataRow oReqTxMailDtlDataRow in pReqTxMailDtlDataRowList) {
					try {						
						string sMailTo = oReqTxMailDtlDataRow["RX_EMAIL_ADDR"].ToString();
						string sSubject = pFormatter.FormatVariable(pSubjectTemplate,oReqTxMailDtlDataRow,true);
						string sBody = pFormatter.FormatVariable(pTextTemplate,oReqTxMailDtlDataRow,sSubject,true);
						//絵文字の変換を行います。 
						string sCarrierCd = EMailAddrUtil.GetCarrierCd(sMailTo);
						if (sCarrierCd.Equals(Mobile.SOFTBANK)) {
							sCarrierCd = Mobile.SOFTBANK_MAIL;
						}
						ParseMobile oParseMobile = new ParseMobile(@"\$x\w{1,};",false,string.Empty,string.Empty,string.Empty,RegexOptions.IgnoreCase,sCarrierCd);
						ParseHTML oParser = new ParseHTML(oParseMobile);
						sSubject = oParser.Parse(Mobile.ConvertSpecialChar(sSubject));
						sBody = oParser.Parse(Mobile.ConvertSpecialChar(sBody));
						
						string sBase64Subject = ConvertToBase64(sSubject);					
						
						// RSET
						SendRset(oStream);

						// MAIL FROM 注）ここはリターン用のメールアドレスを指定

						SendMailFrom(oStream,pMailRturnTo);
						// MAIL TO
						SendRcptTo(oStream,sMailTo);
						// DATA
						SendDataCmd(oStream);

						SendMsg(oStream,string.Format("From:{0}\r\n",pMailFrom));
						SendMsg(oStream,string.Format("To:{0}\r\n",sMailTo));
						SendMsg(oStream,string.Format("Subject:=?shift_jis?B?{0}?=\r\n",sBase64Subject));

						lDataLength += SendMsg(oStream,"MIME-Version:1.0\r\n");
						lDataLength += SendMsg(oStream,"Content-Type: text/plain; charset=Shift_JIS\r\n");
						lDataLength += SendMsg(oStream,"Content-Transfer-Encoding: 8bit\r\n");

						lDataLength += SendMsg(oStream,"\r\n");   // 本文開始前に改行

						lDataLength += SendMultiLineMsg(oStream,sBody,true);
						lDataLength += SendMsg(oStream,"\r\n");

						// .(DATA END)
						SendDataEndCmd(oStream);

						// RSET
						SendRset(oStream);
					} catch (Exception ex) {
						EventLogWriter.Error(ex);
					}
				}
				// QUIT
				SendQuit(oStream);

			}
		} finally {
			oTcpClient.Close();
		}

		return lDataLength;
	}


}
