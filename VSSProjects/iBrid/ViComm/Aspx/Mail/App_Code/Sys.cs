﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Mail Interface
--	Title			: システム設定
--	Progaram ID		: Sys
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class Sys:DbSession {
	public string twilioAccountSid;
	public string twilioAuthToken;
	public string twilioTelNo;
	public string twilioFreedialTelNo;
	public string twilioWhiteplanTelNo;
	public string twilioSmsTelNo;

	public Sys() {
	}

	public bool GetValue(string pItem,out string pValue) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		pValue = "";
		conn = DbConnect();
		string sSql = "SELECT " +
							"DECOMAIL_SERVER_TYPE	," +
							"MAIL_TX_UNIT			," +
							"MAIL_TX_CYCLE_SEC	" +
						"FROM " +
							"T_SYS ";

		using (cmd = CreateSelectCommand(sSql,conn))
		using (ds = new DataSet()) {

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"T_SYS");
				if (ds.Tables["T_SYS"].Rows.Count != 0) {
					dr = ds.Tables["T_SYS"].Rows[0];
					pValue = dr[pItem].ToString();
					bExist = true;
				}
			}
		}
		conn.Close();
		return bExist;
	}

	public bool GetOne() {
		bool bExist = false;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	TWILIO_ACCOUNT_SID,");
		oSqlBuilder.AppendLine("	TWILIO_AUTH_TOKEN,");
		oSqlBuilder.AppendLine("	TWILIO_TEL_NO,");
		oSqlBuilder.AppendLine("	TWILIO_FREEDIAL_TEL_NO,");
		oSqlBuilder.AppendLine("	TWILIO_WHITEPLAN_TEL_NO,");
		oSqlBuilder.AppendLine("	TWILIO_SMS_TEL_NO");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_SYS");

		DataSet ds = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (ds.Tables[0].Rows.Count > 0) {
			DataRow dr = ds.Tables[0].Rows[0];
			twilioAccountSid = iBridUtil.GetStringValue(dr["TWILIO_ACCOUNT_SID"]);
			twilioAuthToken = iBridUtil.GetStringValue(dr["TWILIO_AUTH_TOKEN"]);
			twilioTelNo = iBridUtil.GetStringValue(dr["TWILIO_TEL_NO"]);
			twilioFreedialTelNo = iBridUtil.GetStringValue(dr["TWILIO_FREEDIAL_TEL_NO"]);
			twilioWhiteplanTelNo = iBridUtil.GetStringValue(dr["TWILIO_WHITEPLAN_TEL_NO"]);
			twilioSmsTelNo = iBridUtil.GetStringValue(dr["TWILIO_SMS_TEL_NO"]);
			bExist = true;
		}

		return bExist;
	}

}
