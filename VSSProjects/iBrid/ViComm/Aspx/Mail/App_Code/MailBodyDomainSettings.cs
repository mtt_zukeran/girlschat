﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: メール文章ドメイン設定
--	Progaram ID		: MailBodyDomainSettings
--
--  Creation Date	: 2010.07.20
--  Creater			: R.Suzuki
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;

/// <summary>MAIL_BODY_DOMAIN_SETTINGS テーブルへのデータアクセスを提供するクラス。</summary>
public class MailBodyDomainSettings:DbSession {
	public MailBodyDomainSettings() {
	}

	//ドメイン取得
	public string GetReplaceDomain(string pSiteCd) {
		string sReplaceDomain = "";
		string sSortExpression = " ORDER BY SITE_CD,MAIL_BODY_DOMAIN_SETTINGS_SEQ";

		StringBuilder objSqlBuilder = new StringBuilder();
		objSqlBuilder.Append(" SELECT                           		").AppendLine();
		objSqlBuilder.Append("     REPLACE_DOMAIN               		").AppendLine();
		objSqlBuilder.Append(" FROM                             		").AppendLine();
		objSqlBuilder.Append("     T_MAIL_BODY_DOMAIN_SETTINGS  		").AppendLine();
		objSqlBuilder.Append(" WHERE									").AppendLine();
		objSqlBuilder.Append("	   SITE_CD	     = :SITE_CD			AND	").AppendLine();
		objSqlBuilder.Append("     USED_NOW_FLAG = :USED_NOW_FLAG		").AppendLine();

		objSqlBuilder.Append(sSortExpression);

		DataSet ds = new DataSet();

		conn = DbConnect();

		using (cmd = CreateSelectCommand(objSqlBuilder.ToString(),conn))
		using (da = new OracleDataAdapter(cmd))
		using (ds = new DataSet()) {
			cmd.Parameters.Add("SITE_CD",pSiteCd);
			cmd.Parameters.Add("USED_NOW_FLAG",ViCommConst.FLAG_ON);
			da.Fill(ds,"T_MAIL_BODY_DOMAIN_SETTINGS");

			if (ds.Tables[0].Rows.Count != 0) {
				sReplaceDomain = ds.Tables[0].Rows[0]["REPLACE_DOMAIN"].ToString();
			}
		}
		conn.Close();

		return sReplaceDomain;
	}

	public string GetReplaceDomainRandom(string pSiteCd) {
		string sReplaceDomain = string.Empty;

		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	REPLACE_DOMAIN");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	(SELECT");
		oSqlBuilder.AppendLine("		REPLACE_DOMAIN");
		oSqlBuilder.AppendLine("	FROM");
		oSqlBuilder.AppendLine("		T_MAIL_BODY_DOMAIN_SETTINGS");
		oSqlBuilder.AppendLine("	WHERE");
		oSqlBuilder.AppendLine("		SITE_CD = :SITE_CD AND");
		oSqlBuilder.AppendLine("		USED_RANDOM_FLAG = 1");
		oSqlBuilder.AppendLine("	ORDER BY DBMS_RANDOM.RANDOM)");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("  ROWNUM <= 1");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));

		DataSet ds = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		if (ds.Tables[0].Rows.Count > 0) {
			sReplaceDomain = iBridUtil.GetStringValue(ds.Tables[0].Rows[0]["REPLACE_DOMAIN"]);
		}

		return sReplaceDomain;
	}
}
