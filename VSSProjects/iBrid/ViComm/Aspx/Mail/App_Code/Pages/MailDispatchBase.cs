﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;

public abstract class MailDispatchBase:System.Web.UI.Page {
	public MailDispatchBase() {
	}

	public bool CheckMobileMailRx(DataRow pTxBase,DataRow pTxDetail) {
		string sMailRxType = "";
		string sMobileMailRxTimeUseFlag = ViCommConst.FLAG_OFF_STR;
		string sMobileMailRxStartTime = "";
		string sMobileMailRxEndTime = "";
		string sMobileMailRxTimeUseFlag2 = ViCommConst.FLAG_OFF_STR;
		string sMobileMailRxStartTime2 = "";
		string sMobileMailRxEndTime2 = "";
		bool bExist = false;

		sMobileMailRxTimeUseFlag = pTxDetail["MOBILE_MAIL_RX_TIME_USE_FLAG"].ToString();
		sMobileMailRxStartTime = pTxDetail["MOBILE_MAIL_RX_START_TIME"].ToString();
		sMobileMailRxEndTime = pTxDetail["MOBILE_MAIL_RX_END_TIME"].ToString();

		sMobileMailRxTimeUseFlag2 = pTxDetail["MOBILE_MAIL_RX_TIME_USE_FLAG2"].ToString();
		sMobileMailRxStartTime2 = pTxDetail["MOBILE_MAIL_RX_START_TIME2"].ToString();
		sMobileMailRxEndTime2 = pTxDetail["MOBILE_MAIL_RX_END_TIME2"].ToString();

		if (sMobileMailRxEndTime == "00:00") {
			sMobileMailRxEndTime = "24:00";
		}

		if (sMobileMailRxEndTime2 == "00:00") {
			sMobileMailRxEndTime2 = "24:00";
		}

		if (pTxBase["MAIL_TYPE"].ToString().Equals(ViCommConst.MAIL_TP_NOTIFY_LOGIN)) {
			// Login通知はﾚｺｰﾄﾞが作成されている時点で携帯での受信希望 
			// 送信時間のﾁｪｯｸのみ行う。

			sMailRxType = ViCommConst.MAIL_RX_BOTH;
		} else if (pTxBase["MAIL_TYPE"].ToString().Equals(ViCommConst.MAIL_TP_NOTIFY_TWEET_COMMENT)) {
			// つぶやきｺﾒﾝﾄ通知はﾚｺｰﾄﾞが作成されている時点で携帯での受信希望 
			// 送信時間のﾁｪｯｸのみ行う。
			
			sMailRxType = ViCommConst.MAIL_RX_BOTH;
		} else {
			if (pTxBase["MAIL_BOX_TYPE"].ToString().Equals(ViCommConst.MAIL_BOX_USER)) {
				sMailRxType = pTxDetail["USER_MAIL_RX_TYPE"].ToString();
			} else if (pTxBase["MAIL_BOX_TYPE"].ToString().Equals(ViCommConst.MAIL_BOX_BLOG)) {
				sMailRxType = pTxDetail["BLOG_MAIL_RX_TYPE"].ToString();
			} else {
				sMailRxType = pTxDetail["INFO_MAIL_RX_TYPE"].ToString();
			}
		}

		if (pTxDetail["FORCE_TX_MOBILE_MAIL_FLAG"].ToString().Equals(ViCommConst.FLAG_ON_STR)) {
			// 強制送信 
			bExist = true;
		} else {
			switch (sMailRxType) {
				case ViCommConst.MAIL_RX_BOTH:
				case ViCommConst.MAIL_RX_ONLY_FAVORIT:
					
					if (sMobileMailRxTimeUseFlag.Equals(ViCommConst.FLAG_ON_STR)) {
						if (sMobileMailRxStartTime.CompareTo(sMobileMailRxEndTime) >= 0) {
							if (DateTime.Now.ToString("HH:mm").CompareTo(sMobileMailRxStartTime) >= 0) {
								bExist = true;
							} else if (DateTime.Now.ToString("HH:mm").CompareTo(sMobileMailRxEndTime) <= 0) {
								bExist = true;
							}
						} else if (DateTime.Now.ToString("HH:mm").CompareTo(sMobileMailRxStartTime) >= 0 && DateTime.Now.ToString("HH:mm").CompareTo(sMobileMailRxEndTime) <= 0) {
							bExist = true;
						}
					}
					
					if (sMobileMailRxTimeUseFlag2.Equals(ViCommConst.FLAG_ON_STR)) {
						if (sMobileMailRxStartTime2.CompareTo(sMobileMailRxEndTime2) >= 0) {
							if (DateTime.Now.ToString("HH:mm").CompareTo(sMobileMailRxStartTime2) >= 0) {
								bExist = true;
							} else if (DateTime.Now.ToString("HH:mm").CompareTo(sMobileMailRxEndTime2) <= 0) {
								bExist = true;
							}
						} else if (DateTime.Now.ToString("HH:mm").CompareTo(sMobileMailRxStartTime2) >= 0 && DateTime.Now.ToString("HH:mm").CompareTo(sMobileMailRxEndTime2) <= 0) {
							bExist = true;
						}
					}

					if (!sMobileMailRxTimeUseFlag.Equals(ViCommConst.FLAG_ON_STR) && !sMobileMailRxTimeUseFlag2.Equals(ViCommConst.FLAG_ON_STR)) {
						bExist = true;
					}
					
					if (bExist && sMailRxType.Equals(ViCommConst.MAIL_RX_ONLY_FAVORIT)) {
						bExist = (!pTxDetail["FAVORIT_REGIST_DATE"].ToString().Equals(string.Empty) && !pTxDetail["FAVORIT_DEL_FLAG"].ToString().Equals("1"));
					}
					break;
			}

			//ｵﾝﾗｲﾝ時携帯ﾒｰﾙ送信ﾌﾗｸﾞがOFFの場合、ｵﾝﾗｲﾝだったら携帯にﾒｰﾙを送信しない

			if (bExist == true && pTxDetail["RX_SEX_CD"].ToString().Equals(ViCommConst.OPERATOR)) {
				if (!pTxBase["CAST_ONLINE_MAIL_RX_FLAG"].ToString().Equals("1")) {
					string sOnlineStatus = pTxDetail["CHARACTER_ONLINE_STATUS"].ToString();
					if (sOnlineStatus.Equals(ViCommConst.USER_WAITING.ToString()) ||
						sOnlineStatus.Equals(ViCommConst.USER_TALKING.ToString())) {
						bExist = false;
					}
				}
			}
		}
		return bExist;
	}
}
