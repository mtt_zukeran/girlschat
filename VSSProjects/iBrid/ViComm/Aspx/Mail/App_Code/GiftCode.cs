﻿/*************************************************************************
--	System			: ViCOMM Site
--	Sub System Name	: Mail管理

--	Title			: ギフトコード
--	Progaram ID		: GiftCode
--
--  Creation Date	: 2017.04.14
--  Creater			: M&TT Zukeran
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class GiftCode:DbSession {
	public GiftCode() {
	}

	/// <summary>
	/// ギフトコードリストタグを変換
	/// </summary>
	/// <param name="pMailBoxDoc"></param>
	/// <param name="pUserSeq"></param>
	/// <param name="pPaymentDate"></param>
	/// <returns></returns>
	public string ReplaceGiftCodeList(string pMailBoxDoc,string pUserSeq,string pPaymentDate) {
		// 変換対象の独自タグ
		string sTag = "$MAIL_GIFT_CD_LIST";

		// 定義されたHTMLテキストを取得
		UserDefTag oUserDefTag = new UserDefTag();
		string sDoc = oUserDefTag.GetHtmlDoc(sTag);

		// 発行されたギフトコードを取得
		DataSet oDS = this.GetPaymentGiftCode(pUserSeq,pPaymentDate);

		// タグの中身を変換
		StringBuilder oMailDoc = new StringBuilder();
		string sMailDoc = string.Empty;
		if (oDS.Tables[0].Rows.Count > 0) {
			for (int i=0;i<oDS.Tables[0].Rows.Count;i++) {
				sMailDoc = sDoc.Replace(
					"$GIFT_CODE;"
					,oDS.Tables[0].Rows[i]["GIFT_CODE"].ToString()
				)
				.Replace(
					"$GIFT_EXPIRATION_DATE;"
					,oDS.Tables[0].Rows[i]["EXPIRATION_DATE"].ToString()
				);
				oMailDoc.AppendLine(sMailDoc);
			}
		}
		// ギフトコード一覧の独自タグを変換したテキストを返す
		return pMailBoxDoc.Replace(sTag + ";",oMailDoc.ToString());
	}

	/// <summary>
	/// ギフトコード一覧の取得
	/// </summary>
	/// <param name="pUserSeq"></param>
	/// <param name="pPaymentDate"></param>
	/// <returns></returns>
	public DataSet GetPaymentGiftCode(string pUserSeq,string pPaymentDate) {
		StringBuilder sSql = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		sSql.AppendLine("SELECT");
		sSql.AppendLine("	GIFT_CODE");
		sSql.AppendLine("	,TO_CHAR(EXPIRATION_DATE,'YYYY/MM/DD') AS EXPIRATION_DATE");
		sSql.AppendLine("FROM");
		sSql.AppendLine("	T_GIFT_CODE_MANAGE");
		sSql.AppendLine("WHERE");
		sSql.AppendLine("	USER_SEQ = :USER_SEQ");
		sSql.AppendLine("	AND PAYMENT_DATE = TO_DATE(:PAYMENT_DATE,'YYYYMMDDHH24MISS')");

		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":PAYMENT_DATE",pPaymentDate));

		return ExecuteSelectQueryBase(sSql,oParamList.ToArray());
	}

}
