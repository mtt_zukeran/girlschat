﻿/*************************************************************************
--	System			: ViCOMM Site
--	Sub System Name	: Mail管理
--	Title			: ＷＥＢデザイン設定
--	Progaram ID		: WebFace
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class WebFace:DbSession {

	public string colorBack;
	public string colorChar;
	public string colorLink;
	public string colorVLink;
	public string sizeChar;

	public WebFace() {
	}

	public bool GetOne(string pWebFaceSeq) {
		DataSet ds;
		DataRow dr;

		bool bExist = false;

		conn = DbConnect();

		string sSql = "SELECT " +
						"SIZE_CHAR		," +
						"COLOR_LINE		," +
						"COLOR_CHAR		," +
						"COLOR_BACK		," +
						"COLOR_VLINK	," +
						"COLOR_LINK		 " +
					"FROM " +
						"T_WEB_FACE " +
					"WHERE " +
						"WEB_FACE_SEQ = :WEB_FACE_SEQ";

		using (cmd = CreateSelectCommand(sSql,conn))
		using (ds = new DataSet()) {
			cmd.Parameters.Add("WEB_FACE_SEQ",pWebFaceSeq);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"T_WEB_FACE");

				if (ds.Tables["T_WEB_FACE"].Rows.Count != 0) {
					dr = ds.Tables["T_WEB_FACE"].Rows[0];
					colorBack = dr["COLOR_BACK"].ToString();
					colorChar = dr["COLOR_CHAR"].ToString();
					colorLink = dr["COLOR_LINK"].ToString();
					colorVLink = dr["COLOR_VLINK"].ToString();
					sizeChar = dr["SIZE_CHAR"].ToString();

					bExist = true;
				}
			}
		}
		conn.Close();
		return bExist;
	}
}
