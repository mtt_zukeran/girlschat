﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using iBridCommLib;
using ViComm;

public class ApplicationObj {
	private ApplicationObj() {}
	private static ApplicationObj _current = null;
	public static ApplicationObj Current{
		get{
			if (_current == null) {
				Initialize();
			}
			return _current;
		}
		private set { _current = value; }		
	}
	private static void Initialize() {
		ApplicationObj oObj = new ApplicationObj();

		int iTmpLogCount;
		if (!int.TryParse(iBridUtil.GetStringValue(ConfigurationManager.AppSettings["LoggingCount"]), out iTmpLogCount)) {
			iTmpLogCount = 300;
		}
		oObj.loggingCount = iTmpLogCount;

		int iTmpThreadCount;
		if (!int.TryParse(iBridUtil.GetStringValue(ConfigurationManager.AppSettings["ThreadCount"]), out iTmpThreadCount)) {
			iTmpThreadCount = 0;
		}
		oObj.threadCount = iTmpThreadCount;
		oObj.enabledTxUserImg2Small = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnabledTxUserImg2Small"]).Equals(ViCommConst.FLAG_ON_STR);
		oObj.enabledPictograph = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["enabledPictograph"]).Equals(ViCommConst.FLAG_ON_STR);

		Current = oObj;
	}
	
	
	
	private bool enabledTxUserImg2Small = false;
	private bool enabledPictograph = false;
	private int loggingCount;
	private int threadCount;


	public bool EnabledTxUserImg2Small {
		get { return this.enabledTxUserImg2Small; }
	}
	public bool EnabledPictograph {
		get { return this.enabledPictograph; }
	}
	
	public int LoggingCount{
		get{return this.loggingCount;}
	}
	public int ThreadCount {
		get { return this.threadCount; }
	}
}
