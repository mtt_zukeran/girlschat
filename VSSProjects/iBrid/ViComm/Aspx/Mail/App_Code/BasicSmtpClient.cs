﻿/*************************************************************************
--	System			: ViCOMM Site
--	Sub System Name	: Mail管理--	Title			: SMTPメール送信
--	Progaram ID		: BasicSmtpClient
--  Creation Date	: 2015.02.12
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net.Sockets;
using iBridCommLib;
using System.Text;
using System.IO;
using System.Collections.Generic;
using ViComm;
using MobileLib;

public class BasicSmtpClient:SmtpClientBase {
	public BasicSmtpClient()
		: base(false) {
	}

	public BasicSmtpClient(bool pIsTestMode)
		: base(pIsTestMode) {
	}

	public long SendMail(
		string pMailHost,
		string pEnvelopeFrom,
		string pHeaderFrom,
		string pMailTitle,
		string pHtmlMailDoc,
		string pTextMailDoc,
		int pMailSendType,
		List<DataRow> pReqTxMailDtlDataRowList,
		Dictionary<string, string> pBase64ImageDict,
		Dictionary<string, string> pExtensionDict,
		MailBodyHelper.MailDataFormatter pFormatter,
		string pProfileContentId,
		string pMailServer
	) {
		long lDataLength = 0;
		string sServerAddress = string.Empty;
		string sServerPort = string.Empty;
		int iServerPort;
		string sMailFromName = string.Empty;

		if (pMailServer.Equals(ViCommConst.MailServer.MULLER3)) {
			sServerAddress = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["SubServerAddress"]);
			sServerPort = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["SubServerPort"]);
		} else {
			sServerAddress = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["MainServerAddress"]);
			sServerPort = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["MainServerPort"]);
		}

		if (string.IsNullOrEmpty(sServerAddress) || !int.TryParse(sServerPort,out iServerPort)) {
			return lDataLength;
		}

		if (string.IsNullOrEmpty(pTextMailDoc.Trim())) {
			pTextMailDoc = MailBodyHelper.ConvertHtml2Text(pHtmlMailDoc);
		}
		
		if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["UseMailFromNameFlag"]).Equals(ViCommConst.FLAG_ON_STR)) {
			sMailFromName = "ガールズチャット";
		}

		EventLogWriter.Debug("SVR:{0},PORT:{1}",sServerAddress,iServerPort);

		TcpClient oTcpClient = new TcpClient();

		try {
			// メールサーバに接続 
			oTcpClient.Connect(sServerAddress,iServerPort);

			using (Stream oStream = oTcpClient.GetStream()) {
				// HELO
				SendHelo(oStream,pMailHost);

				foreach (DataRow oReqTxMailDtlDataRow in pReqTxMailDtlDataRowList) {
					try {
						string sMailTo = oReqTxMailDtlDataRow["RX_EMAIL_ADDR"].ToString();

						//Toアドレスの形式チェック
						if (!EMailAddrUtil.IsCorrectEmailAddress(sMailTo)) {
							using (DbSession db = new DbSession()) {
								db.PrepareProcedure("REGIST_INCORRECT_EMAIL_ADDR");
								db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,oReqTxMailDtlDataRow["RX_SITE_CD"].ToString());
								db.ProcedureInParm("pEMAIL_ADDR",DbSession.DbType.VARCHAR2,sMailTo);
								db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.NUMBER,int.Parse(oReqTxMailDtlDataRow["RX_USER_SEQ"].ToString()));
								db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
								db.cmd.BindByName = true;
								db.ExecuteProcedure();
							}

							continue;
						}

						string sCarrierCd = EMailAddrUtil.GetCarrierCd(sMailTo);

						if (sCarrierCd.Equals(Mobile.SOFTBANK)) {
							sCarrierCd = Mobile.SOFTBANK_MAIL;
						}

						ParseMobile oParseMobile = new ParseMobile(@"\$x\w{1,};",false,string.Empty,string.Empty,string.Empty,RegexOptions.IgnoreCase,sCarrierCd);
						ParseHTML oParser = new ParseHTML(oParseMobile);

						string sSubject = pFormatter.FormatVariable(pMailTitle,oReqTxMailDtlDataRow,true);
						string sTextBody = pFormatter.FormatVariable(pTextMailDoc,oReqTxMailDtlDataRow,sSubject,true);
						string sHtmlBody = pFormatter.FormatVariable(pHtmlMailDoc,oReqTxMailDtlDataRow,sSubject,true,pProfileContentId);
						
						string sUniqueId = CreateUniqueIdStr();
						string sMessageId = string.Format("{0}@{1}.{2}",sUniqueId,GetRandomStr(6),GetRandomStr(3));

						sSubject = oParser.Parse(Mobile.ConvertSpecialChar(sSubject));
						sTextBody = oParser.Parse(Mobile.ConvertSpecialChar(sTextBody));
						sHtmlBody = oParser.Parse(Mobile.ConvertSpecialChar(sHtmlBody));

						sSubject = ConvertToBase64(sSubject);

						// RSET
						SendRset(oStream);

						// MAIL From: 
						SendMailFrom(oStream,pEnvelopeFrom);

						// RCPT To:
						SendRcptTo(oStream,sMailTo);

						// DATA
						SendDataCmd(oStream);

						if (!string.IsNullOrEmpty(sMailFromName)) {
							SendMsg(oStream,string.Format("From: =?shift_jis?B?{0}?=<{1}>\r\n",ConvertToBase64(sMailFromName),pHeaderFrom));
						} else {
							SendMsg(oStream,string.Format("From: {0}\r\n",pHeaderFrom));
						}
						SendMsg(oStream,string.Format("To: {0}\r\n",sMailTo));
						SendMsg(oStream,string.Format("Subject: =?shift_jis?B?{0}?=\r\n",sSubject));
						SendMsg(oStream,string.Format("Message-Id: <{0}>\r\n",sMessageId));

						if (pMailSendType.Equals(ViCommConst.MailSendType.TEXT)) {
							lDataLength += SendMsg(oStream,"MIME-Version: 1.0\r\n");
							lDataLength += SendMsg(oStream,"Content-Type: text/plain; charset=\"Shift_JIS\"\r\n");
							lDataLength += SendMsg(oStream,"Content-Transfer-Encoding: 8bit\r\n");
							lDataLength += SendMsg(oStream,"\r\n");
							lDataLength += SendMultiLineMsg(oStream,sTextBody,true);
							lDataLength += SendMsg(oStream,"\r\n");

							// .
							SendDataEndCmd(oStream);

							// RSET
							SendRset(oStream);

						} else if (pMailSendType.Equals(ViCommConst.MailSendType.DECO_MAIL)) {
							lDataLength += SendMsg(oStream,"MIME-Version: 1.0\r\n");
							lDataLength += SendMsg(oStream,"Content-Type: multipart/related; boundary=\"mimemk00_0\"\r\n");
							lDataLength += SendMsg(oStream,"Content-Transfer-Encoding: 7bit\r\n");
							lDataLength += SendMsg(oStream,"\r\n");

							lDataLength += SendMsg(oStream,"--mimemk00_0\r\n");
							lDataLength += SendMsg(oStream,"Content-Type: multipart/alternative; boundary=\"mimemk01\"\r\n");
							lDataLength += SendMsg(oStream,"\r\n");

							lDataLength += SendMsg(oStream,"--mimemk01\r\n");
							lDataLength += SendMsg(oStream,"Content-Type: text/plain; charset=\"Shift_JIS\"\r\n");
							lDataLength += SendMsg(oStream,"Content-Transfer-Encoding: 8bit\r\n");
							lDataLength += SendMsg(oStream,"\r\n");
							lDataLength += SendMultiLineMsg(oStream,sTextBody,true);
							lDataLength += SendMsg(oStream,"\r\n");

							lDataLength += SendMsg(oStream,"--mimemk01\r\n");
							lDataLength += SendMsg(oStream,"Content-Type: text/html; charset=\"Shift_JIS\"\r\n");
							lDataLength += SendMsg(oStream,"Content-Transfer-Encoding: 8bit\r\n");
							lDataLength += SendMsg(oStream,"\r\n");
							lDataLength += SendMultiLineMsg(oStream,sHtmlBody,true);
							lDataLength += SendMsg(oStream,"\r\n");

							lDataLength += SendMsg(oStream,"--mimemk01--\r\n");

							int i = 0;

							foreach (KeyValuePair<string,string> idImagePair in pBase64ImageDict) {
								lDataLength += SendMsg(oStream,"\r\n");
								lDataLength += SendMsg(oStream,"--mimemk00_0\r\n");

								if (pExtensionDict[idImagePair.Key] == "jpg") {
									lDataLength += SendMsg(oStream,string.Format("Content-Type: image/jpeg; name=\"test{0:D2}.{1}\"\r\n",i,pExtensionDict[idImagePair.Key]));
								} else {
									lDataLength += SendMsg(oStream,string.Format("Content-Type: image/{0}; name=\"test{1:D2}.{2}\"\r\n",pExtensionDict[idImagePair.Key],i,pExtensionDict[idImagePair.Key]));
								}
								lDataLength += SendMsg(oStream,"Content-Transfer-Encoding: base64\r\n");
								lDataLength += SendMsg(oStream,"Content-ID: <" + idImagePair.Key + ">\r\n");
								lDataLength += SendMsg(oStream,"\r\n");

								List<string> substValue = substringAtCount(idImagePair.Value,limitLengthOfSendByte);

								foreach (string value in substValue) {
									lDataLength += SendMsg(oStream,value + "\r\n");
								}

								i++;
							}

							lDataLength += SendMsg(oStream,"\r\n");
							lDataLength += SendMsg(oStream,"--mimemk00_0--\r\n");

							// .
							SendDataEndCmd(oStream);
						}

						// RSET
						SendRset(oStream);

					} catch (Exception ex) {
						EventLogWriter.Error(ex);
					}
				}

				// QUIT
				SendQuit(oStream);
			}
		} finally {
			try {
				oTcpClient.Close();
			} catch (Exception ex) {
				EventLogWriter.Error(ex);
			}
		}

		return lDataLength;
	}

	/// <summary>
	/// 文字列を指定した文字数で分割した配列にします。
	/// </summary>
	/// <param name="source">元の文字列</param>
	/// <param name="count">分割する文字数</param>
	/// <returns>分割した配列</returns>
	private List<string> substringAtCount(string source,int count) {
		List<string> substList = new List<string>();
		int length = (int)Math.Ceiling((double)source.Length / (double)count);

		for (int i = 0;i < length;i++) {
			int start = count * i;
			if (start >= source.Length) {
				break;
			}
			if (start + count > source.Length) {
				substList.Add(source.Substring(start));
			} else {
				substList.Add(source.Substring(start,count));
			}
		}

		return substList;
	}

	private string GetRandomStr(int length) {
		string passwordChars = "abcdefghijklmnopqrstuvwxyz";
		StringBuilder sb = new StringBuilder(length);
		Random r = new Random();

		for (int i = 0;i < length;i++) {
			int pos = r.Next(passwordChars.Length);
			char c = passwordChars[pos];
			sb.Append(c);
		}

		return sb.ToString();
	}
	
	private string CreateUniqueIdStr() {
		string sUniqueIdStr = string.Empty;
		
		Random r = new Random();
		int iUniqueIdLength = r.Next(16,19);
		
		sUniqueIdStr = GetRandomStr(iUniqueIdLength);
		
		return sUniqueIdStr;
	}
}
