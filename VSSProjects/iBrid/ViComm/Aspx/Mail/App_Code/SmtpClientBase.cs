﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Text;

using iBridCommLib;

/// <summary>
/// SmtpClientBase の概要の説明です

/// </summary>
public abstract class SmtpClientBase {

	private static readonly Encoding encSHIFT_JIS = Encoding.GetEncoding("Shift_JIS");
	private bool isTestMode = false;
	protected int limitLengthOfSendByte = 600;		//1000バイトが上限(600なのはアクセルメールが不正に500を返してくるので暫定対応として少なく設定)

	public SmtpClientBase()
		: this(false) {
	}
	public SmtpClientBase(bool pIsTestMode) {
		this.isTestMode = pIsTestMode;
		int iLimit = 0;
		int.TryParse(iBridUtil.GetStringValue(ConfigurationManager.AppSettings["LimitLength"]),out iLimit);
		if (iLimit > 0) {
			limitLengthOfSendByte = iLimit;
		}
	}

	#region □■□ SMTP コマンド □■□ ===========================================================
	protected virtual void SendRcptTo(Stream pStream,string pMailTo) {
		SendMsg(pStream,string.Format("RCPT TO:{0}\r\n",pMailTo));

		string sResponseMessage = ReadMsg(pStream);
		if (!CheckSmtpStatus(sResponseMessage)) {
			throw (new IOException(sResponseMessage));
		}
	}
	protected virtual void SendMailFrom(Stream pStream,string pMailFrom) {
		SendMsg(pStream,string.Format("MAIL FROM:{0}\r\n",pMailFrom));

		string sResponseMessage = ReadMsg(pStream);
		if (!CheckSmtpStatus(sResponseMessage)) {
			throw (new IOException(sResponseMessage));
		}
	}

	protected virtual void SendHelo(Stream pStream,string pFqdn) {
		SendMsg(pStream,string.Format("HELO {0}\r\n",pFqdn));

		string sResponseMessage = ReadMsg(pStream);
		if (!CheckSmtpStatus(sResponseMessage)) {
			throw (new IOException(sResponseMessage));
		}
	}
	protected virtual void SendRset(Stream pStream) {
		SendMsg(pStream,"RSET\r\n");

		string sResponseMessage = ReadMsg(pStream);
		if (!CheckSmtpStatus(sResponseMessage)) {
			throw (new IOException(sResponseMessage));
		}
	}
	protected virtual void SendQuit(Stream pStream) {
		SendMsg(pStream,"QUIT\r\n");
	}
	protected virtual void SendDataCmd(Stream pStream) {
		SendMsg(pStream,"DATA\r\n");
	}
	protected virtual void SendDataEndCmd(Stream pStream) {
		SendMsg(pStream,".\r\n");
		string sResponseMessage = ReadMsg(pStream);
		if (!CheckSmtpStatus(sResponseMessage)) {
			throw (new IOException(sResponseMessage));
		}
	}
	#endregion ====================================================================================

	protected int SendMsg(Stream pStream,string sMessage) {
		return this.SendMsg(pStream,sMessage,Encoding.ASCII);
	}
	protected int SendMsg(Stream pStream,string sMessage,Encoding pEncoding) {
		//byte型配列に変換
		byte[] oMessageBytes = pEncoding.GetBytes(sMessage);
		if (!isTestMode) {
			pStream.Write(oMessageBytes,0,oMessageBytes.Length);
		}
		if (oMessageBytes.Length > limitLengthOfSendByte + 2) {
			EventLogWriter.Debug(sMessage);
		}
		return oMessageBytes.Length;
	}
	protected long SendMultiLineMsg(Stream pStream,string pBody,bool pNeedSplit) {
		long lDataLength = 0;
		string sMessage = string.Empty;

		int iSplitSize = (pNeedSplit) ? limitLengthOfSendByte : int.MaxValue;
		foreach (string sLine in pBody.Split(new Char[] { '\n' })) {

			// 送信上限のバイト数で分割
			foreach (string sSubLine in SysPrograms.SplitBytes(encSHIFT_JIS,sLine,iSplitSize,true)) {
				sMessage = sSubLine;
				if (!sSubLine.EndsWith("\r")) {
					sMessage += "\r";
				}
				sMessage += "\n";
				lDataLength += SendMsg(pStream,sMessage,encSHIFT_JIS);
			}
		}
		return lDataLength;
	}

	protected string ReadMsg(Stream pStream) {
		if (this.isTestMode)
			return string.Empty;

		int nBufSize = 1024;
		byte[] data = new byte[nBufSize + 1];
		string sMsg = string.Empty;
		int nLength;

		using (MemoryStream ms = new System.IO.MemoryStream()) {
			nLength = pStream.Read(data,0,nBufSize);
			ms.Write(data,0,nLength);
			sMsg = Encoding.ASCII.GetString(ms.ToArray(),0,nLength);
			ms.Close();
		}
		return sMsg;
	}

	private bool CheckSmtpStatus(string sResponse) {
		if (this.isTestMode)
			return true;

		if (sResponse.StartsWith("2"))
			return true;      // 2XX のとき、コマンド成功 

		if (sResponse.StartsWith("3"))
			return true;      // 3XX のとき、要求待ち（本文送信時） 

		return false;      // コマンド失敗

	}

	protected string ConvertToBase64(string sBuf) {
		if (sBuf.Length <= 0) {
			return string.Empty;
		}

		byte[] sEncordWorking = encSHIFT_JIS.GetBytes(sBuf);
		return System.Convert.ToBase64String(sEncordWorking);
	}
}
