﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 画像変換機能を提供するヘルパークラス

--	Progaram ID		: ImageConvertHelper
--
--  Creation Date	: 2012.09.11
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------
  Date			Updater			Update Explain

-------------------------------------------------------------------------*/
using System;
using System.IO;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm.localhost;


/// <summary>
/// ImageHelper の概要の説明です

/// </summary>
public class ImageHelper {

	public static void ConvertMobileImage(string pSiteCd,string pInputDir,string pOutputDir,string pFileNm) {
		string sMailParseDir = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["MailParseDir"]);
		using (ViComm.localhost.Service objWebSrv = new ViComm.localhost.Service()) {
			objWebSrv.ConvertMobileImage(sMailParseDir,pSiteCd,pInputDir,pOutputDir,pFileNm);
		}
	}

}