﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: メール画像Helper
--	Progaram ID		: MailImageHelper
--
--  Creation Date	: 2010.10.20
--  Creater			: iBrid(K.Itoh)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater		Update Explain
  2010.10.20  K.Itoh		Dispatch2.aspxから移植。変数名の修正などリファクタリング。							実装上問題がある箇所を修正。

-------------------------------------------------------------------------*/
using System;
using System.IO;
using System.Net;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using ViComm;


public class MailImageHelper {

	/// <summary>「src」を探し出す正規表現を作成</summary> 
	private static readonly Regex _srcAttributeRegex
		= new Regex("src\\s*=\\s*(?:\"(?<1>[^\"]*)\"|(?<1>\\S+))",RegexOptions.IgnoreCase | RegexOptions.Compiled);

	/// <summary>コンストラクタ</summary>
	public MailImageHelper() {
	}

	public string SearchImageFilePath(string pMailDoc,string pImgPathBase,string pMailHost,Dictionary<string,string> pBase64ImageDict,Dictionary<string,string> pExtensionDict) {
		// メールドキュメントから画像パスを正規表現で抽出

		Match oSrcMatch;
		int i = 0;
		List<string> oPathList = new List<string>();

		for (oSrcMatch = _srcAttributeRegex.Match(pMailDoc);oSrcMatch.Success;oSrcMatch = oSrcMatch.NextMatch()) {
			// 「src=""」の「""」の中身を取得 
			string sValue = oSrcMatch.Groups[1].Value;

			// 拡張子をチェック
			if (this.IsImageFile(sValue)) {

				// ローカルディスクに保存するときの名前を作成 (?)
				string sLocalFileName = sValue;

				// アドレスが既に存在する場合は取得済なのでパスします。 
				if (oPathList.Contains(sLocalFileName)) {
					continue;
				}
				// 1回取得した画像パスを保存しておきます。 
				oPathList.Add(sLocalFileName);

				try {
					// コンテンツIDの作成
					string sContentsId = string.Format("{0:D2}@{1}{2}",i,DateTime.Now.ToString("yyyyMMddHHmmssfff"),pMailHost);

					// 画像データコレクションに追加
					pBase64ImageDict.Add(sContentsId,this.ConvertImage2Base64String(sValue,pImgPathBase));

					// 拡張子をリストに追加
					pExtensionDict.Add(sContentsId,this.GetFileExtension(sValue));

					// 抽出したものをCONTENTS_IDに置換します。 
					pMailDoc = Regex.Replace(pMailDoc,sValue,string.Format("cid:{0}",sContentsId));

					i++;
				} catch (Exception ex) {
					// 画像の読み込みに失敗しても継続します。 
					ViCommInterface.WriteIFLog("MailDispatch","SearchImageFilePath " + ex.Message + " " + sValue);
				}
			}
		}

		// 置換したドキュメントを戻します。 
		return pMailDoc;
	}


	/// <summary>
	/// 指定された画像をBase64文字列化する。

	/// </summary>
	/// <param name="pImageFile">画像ファイルUrl</param>
	/// <returns>指定された画像をBase64文字列化した文字列</returns>
	public string ConvertImage2Base64String(string pImageFileUrl,string pImagePathBase) {

		pImageFileUrl = pImageFileUrl.Replace("%%12%%",pImagePathBase);
		if (!pImageFileUrl.ToLower().StartsWith("http") && !pImageFileUrl.ToLower().StartsWith("https")) {
			HttpRequest oRequest = HttpContext.Current.Request;

			pImageFileUrl = string.Format("{0}://{1}:{2}{3}",oRequest.Url.Scheme,oRequest.Url.DnsSafeHost,oRequest.Url.Port,pImageFileUrl);
		}

		List<byte> oByteList = new List<byte>();
		using (WebClient oWebClient = new WebClient())
		using (Stream oImageFileStream = oWebClient.OpenRead(pImageFileUrl)) {
			int iByte;
			while ((iByte = oImageFileStream.ReadByte()) != -1) {
				oByteList.Add((byte)iByte);
			}
			oImageFileStream.Close();
		}

		return Convert.ToBase64String(oByteList.ToArray());
	}


	/// <summary>
	/// 指定されたURLから拡張子を抽出する。

	/// </summary>
	/// <param name="pUrl">対象URL</param>
	/// <returns>抽出した拡張子</returns>
	public string GetFileExtension(string pUrl) {
		return pUrl.ToLower().Substring(pUrl.Length - 3,3);
	}



	/// <summary>
	/// 指定されたファイルがイメージファイルかどうかを示す値を取得する

	/// </summary>
	/// <param name="pFileName">指定されたファイル</param>
	/// <returns>
	/// 指定されたファイルがイメージファイルの場合はtrue。それ以外はfalse。

	/// </returns>
	private bool IsImageFile(string pFileName) {
		bool bIsImageFile = false;
		bIsImageFile = bIsImageFile || pFileName.ToLower().EndsWith(".jpg");
		bIsImageFile = bIsImageFile || pFileName.ToLower().EndsWith(".gif");
		bIsImageFile = bIsImageFile || pFileName.ToLower().EndsWith(".png");
		return bIsImageFile;
	}

}
