﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Text.RegularExpressions;

/// <summary>
/// MailBodyHelper の概要の説明です




/// </summary>
public class MailBodyHelper {
	private static readonly Regex regexHtml2Text01
		= new Regex("<(?!A)(?!BR)(\"[^\"]*\"|'[^']*'|[^'\">])*((?<!/A)>)",RegexOptions.IgnoreCase | RegexOptions.Compiled);
	private static readonly Regex regexHtml2Text02
		= new Regex(@"<a\s+[^>]*href\s*=\s*(?:(?<quot>[""'])(?<url>.*?)\k<quot>|(?<url>[^\s>]+))[^>]*>(?<text>.*?)</a>",RegexOptions.IgnoreCase | RegexOptions.Compiled);
	private static readonly Regex regexHtml2Text03
		= new Regex("<br[ ]?[/]?>",RegexOptions.IgnoreCase | RegexOptions.Compiled);

	public static string GetBuyPointUserList(string pReqSeq,string pReqSubSeq) {
		StringBuilder sValue = new StringBuilder();

		using (ReqTxMailDtlEx oDtlEx = new ReqTxMailDtlEx()) {
			DataSet ds = oDtlEx.GetList(pReqSeq,pReqSubSeq);
			foreach (DataRow oRow in ds.Tables[0].Rows) {
				if (sValue.Length != 0) {
					sValue.AppendLine();
				}
				sValue.Append(oRow["HANDLE_NM"].ToString());
			}
		}
		return sValue.ToString();
	}

	public static string ConvertHtml2Text(string pHtml) {
		string sTextBody = pHtml.Replace(Environment.NewLine,string.Empty);
		sTextBody = regexHtml2Text01.Replace(sTextBody,string.Empty);
		sTextBody = regexHtml2Text02.Replace(sTextBody,"${text}=>[ ${url} ]");
		sTextBody = regexHtml2Text03.Replace(sTextBody,Environment.NewLine);
		return sTextBody;
	}

	public class MailDataFormatter {
		private static readonly Regex _regex = new Regex(@"(%%\d{1,2}%%)",RegexOptions.Compiled);

		string url = null;
		public MailDataFormatter(string pUrl) {
			this.url = pUrl;
		}

		public string FormatVariable(string pValue,DataRow pDataRow,bool pRelayFlag) {
			return FormatVariable(pValue,pDataRow,string.Empty,pRelayFlag,string.Empty);
		}

		public string FormatVariable(string pValue,DataRow pDataRow,string pSubject,bool pRelayFlag) {
			return FormatVariable(pValue,pDataRow,pSubject,pRelayFlag,string.Empty);
		}

		public string FormatVariable(string pValue,DataRow pDataRow,string pSubject,bool pRelayFlag,string pProfileContentId) {
			return _regex.Replace(pValue,new MatchEvaluator(delegate(Match match) {
				string sValue = string.Empty;

				switch (match.Value) {
					case "%%0%%":
						sValue = pDataRow["RX_EMAIL_ADDR"].ToString();
						break;
					case "%%1%%":
						sValue = pDataRow["RX_EMAIL_ADDR"].ToString();
						break;
					case "%%2%%":
						sValue = pDataRow["RX_LOGIN_ID"].ToString();
						break;
					case "%%3%%":
						sValue = pDataRow["RX_LOGIN_PASSWORD"].ToString();
						break;
					case "%%4%%":
						sValue = pDataRow["RX_HANDLE_NM"].ToString();
						break;
					case "%%5%%":
						sValue = pDataRow["TX_HANDLE_NM"].ToString();
						break;
					case "%%6%%":
						sValue = pDataRow["TX_LOGIN_ID"].ToString();
						break;
					case "%%7%%":
						if (!string.IsNullOrEmpty(pProfileContentId)) {
							sValue = string.Format("cid:{0}",pProfileContentId);
						} else {
							sValue = pDataRow["TX_IMG_PATH"].ToString();
						}
						break;
					case "%%8%%":
						sValue = pDataRow["TX_ATTR1"].ToString();
						break;
					case "%%9%%":
						sValue = pDataRow["TX_ATTR2"].ToString();
						break;
					case "%%10%%":
						sValue = pDataRow["GENERAL1"].ToString();
						break;
					case "%%11%%":
						sValue = pDataRow["GENERAL2"].ToString();
						break;
					case "%%12%%":
						sValue = this.url;
						break;
					case "%%13%%":
						sValue = pRelayFlag ? this.url + pDataRow["MOBILE_MAIL_GENERAL1"].ToString() : pDataRow["WEB_MAIL_GENERAL1"].ToString();
						break;
					case "%%14%%":
						sValue = pDataRow["CHAR_NO_ITEM_NM"].ToString();
						break;
					case "%%15%%":
						sValue = pDataRow["CRYPT_VALUE"].ToString();
						break;
					case "%%16%%":
						sValue = pRelayFlag ? this.url + pDataRow["MOBILE_MAIL_GENERAL2"].ToString() : pDataRow["WEB_MAIL_GENERAL2"].ToString();
						break;
					case "%%17%%":
						sValue = pRelayFlag ? this.url + pDataRow["MOBILE_MAIL_GENERAL3"].ToString() : pDataRow["WEB_MAIL_GENERAL3"].ToString();
						break;
					case "%%18%%":
						sValue = pDataRow["GENERAL3"].ToString();
						break;
					case "%%19%%":
						sValue = pDataRow["GENERAL4"].ToString();
						break;
					case "%%20%%":
						sValue = pDataRow["GENERAL5"].ToString();
						break;
					case "%%21%%":
						sValue = pDataRow["TX_AGE"].ToString();
						break;
					case "%%22%%":
						sValue = MailBodyHelper.GetBuyPointUserList(pDataRow["REQUEST_TX_MAIL_SEQ"].ToString(),pDataRow["REQUEST_TX_MAIL_SUBSEQ"].ToString());
						break;
					case "%%23%%":
						sValue = pSubject;
						break;
					case "%%24%%":
						sValue = pDataRow["MAIL_SEQ"].ToString();
						break;
					case "%%25%%":
						sValue = pDataRow["OBJ_SEQ"].ToString();
						break;
					case "%%26%%":
						sValue = pDataRow["REQUEST_TX_MAIL_SEQ"].ToString() + "-" + pDataRow["REQUEST_TX_MAIL_SUBSEQ"].ToString();
						break;
					case "%%27%%":
						sValue = pDataRow["REQUEST_TX_MAIL_SEQ"].ToString();
						break;
					case "%%28%%":
						sValue = pDataRow["MAIL_TEMPLATE_NO"].ToString() + "_" + DateTime.Now.ToString("yyyyMMdd");
						break;
				}
				return sValue;
			}));
		}

	}

}
