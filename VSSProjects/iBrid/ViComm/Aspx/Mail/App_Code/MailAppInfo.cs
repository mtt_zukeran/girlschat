﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;

[DataContract]
public class MailAppInfo {
	[DataMember]
	public string app;
	[DataMember]
	public string tag;
	[DataMember]
	public string img_url;
	[DataMember]
	public List<MailAppUsers> users;
}

[DataContract]
public class MailAppUsers {
	[DataMember]
	public string token_id;
	[DataMember]
	public string msg;
	[DataMember]
	public string userseq;
	[DataMember]
	public string detail_url;
}
