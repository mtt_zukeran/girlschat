﻿/*************************************************************************
--	System			: ViCOMM Site
--	Sub System Name	: Mail管理
--	Title			: アクセルメールメール送信
--	Progaram ID		: AccelMailSmtpClient
--
--  Creation Date	: 2010.03.26
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date			Updater		Update Explain
  2010/10/21	K.Itoh		変数名の修正。コメントの追加。その他リファクタ

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net.Sockets;
using iBridCommLib;
using System.Text;
using System.IO;
using System.Collections.Generic;

using ViComm;

/// <summary>
/// アクセルメールを使用してメール送信を実行します。/// </summary>
public class AccelMailSmtpClient : SmtpClientBase {

	public const int DEFAULT_SVR_NUMBER = 1;
	private class ServerInfo {
		private string serverName = string.Empty;
		private int port = int.MinValue;
		
		public  string ServerName{
			get{ return this.serverName;}
		}
		
		public int Port{
			get{return this.port;}
		}

		private ServerInfo(string pServerName,int pPort) {
			this.serverName = pServerName;
			this.port = pPort;
		}
		
		public static ServerInfo CreateServerInfo(int pSvrNumber){
			string sAccelMailServer = iBridUtil.GetStringValue(ConfigurationManager.AppSettings[string.Format("AccelMailServer{0}",pSvrNumber)]);
			string sAccelMailPort = iBridUtil.GetStringValue(ConfigurationManager.AppSettings[string.Format("AccelMailPort{0}",pSvrNumber)]);
			
			if(string.IsNullOrEmpty(sAccelMailServer) && string.IsNullOrEmpty(sAccelMailPort)){
				sAccelMailServer = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["AccelMailServer"]);
				sAccelMailPort = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["AccelMailPort"]);
			}
			
			return new ServerInfo(sAccelMailServer,int.Parse(sAccelMailPort));
		}
		
		
		
	}

	
	public AccelMailSmtpClient():base(false) {}
	public AccelMailSmtpClient(bool pIsTestMode):base(pIsTestMode){}

	public long SendAccelMail(int pSvrNumber, string pMailFrom, List<string> pMailToList, string pMailRturnTo, string pSubject, string pBody, Dictionary<string, string> pBase64ImageDict, Dictionary<string, string> pExtensionDict) {
		return this.SendAccelMail(pSvrNumber, pMailFrom, pMailToList, pMailRturnTo, pSubject, pBody, string.Empty, pBase64ImageDict, pExtensionDict, ViCommConst.MailSendType.DECO_MAIL);
	}

	public long SendAccelMail(int pSvrNumber, string pMailFrom, List<string> pMailToList, string pMailRturnTo, string pSubject, string pBody, string pTextBody, Dictionary<string, string> pBase64ImageDict, Dictionary<string, string> pExtensionDict, int pMailSendType) {
		NetworkStream sr;
		string sMsg;
		StringBuilder sResMsgHolder = new StringBuilder();
		long lDataLength = 0;

		// TEXTメールのBODY
		if(string.IsNullOrEmpty(pTextBody.Trim())){
			pTextBody = MailBodyHelper.ConvertHtml2Text(pBody);
		}
		
		// HTMLメールのBODY
		string sHtmlBody = pBody;
	
		ServerInfo oSvrInfo = ServerInfo.CreateServerInfo(pSvrNumber);
		string sAccelMailServer = oSvrInfo.ServerName;
		int iAccelMailPort = oSvrInfo.Port;
		
		EventLogWriter.Debug("SVR:{0},POAT:{1}",sAccelMailServer,iAccelMailPort);
	
		TcpClient oTcpClient = new TcpClient();
		try{
			// メールサーバに接続 
			oTcpClient.Connect(sAccelMailServer,iAccelMailPort);

			sr = oTcpClient.GetStream();

			sMsg = ReadMsg(sr);
			sResMsgHolder.Append(sMsg);
			
			// HELO
			this.SendHelo(sr, "localhost");
			// MAIL FROM:
			this.SendMailFrom(sr, string.Format("<{0}>",pMailFrom));
			// MAIL TO:
			this.SendRcptTo(sr, "<douhou>");

			/******** START DATA コマンド ***************************************************************/
			// 本文転送コマンド 
			this.SendDataCmd(sr);
		
			// 本文転送（メールヘッダ） 
			sMsg = "From:<" + pMailFrom + ">\r\n";
			SendMsg(sr, sMsg);

			sMsg = "To:\r\n";
			SendMsg(sr, sMsg);

			// 同報送信用に送信先を設定する 
			int i = 1;
			foreach (string eachTo in pMailToList) {
				sMsg = string.Format("X-To{0}:{1}\r\n",i,eachTo);
				SendMsg(sr, sMsg);
				i++;
			}

			sMsg = "Subject:=?shift_jis?B?" + ConvertToBase64(pSubject) + "?=\r\n";   // BASE64変換した日本語タイトルサポート
			//TODO DATE
			SendMsg(sr, sMsg);
			
			if(pMailSendType == ViCommConst.MailSendType.TEXT){
				lDataLength += SendMsg(sr, "MIME-Version:1.0\r\n");
				lDataLength += SendMsg(sr, "Content-Type: text/plain; charset=Shift_JIS\r\n");
				lDataLength += SendMsg(sr, "Content-Transfer-Encoding: 8bit\r\n");
				lDataLength += SendMsg(sr, "X-Amconverter:emoji\r\n");
				lDataLength += SendMsg(sr, string.Format("X-AM-Envelope-From: {0}\r\n", pMailRturnTo));
				lDataLength += SendMsg(sr, "\r\n");   // 本文開始前に改行
				lDataLength += SendMultiLineMsg(sr, pTextBody, true);
				lDataLength += SendMsg(sr, "\r\n");

				SendDataEndCmd(sr);
			} else if (pMailSendType == ViCommConst.MailSendType.DECO_MAIL) {
				sMsg = "MIME-Version:1.0\r\n";
				lDataLength += SendMsg(sr, sMsg);
				sMsg = "Content-Type:multipart/related;boundary=\"mimemk00_0\"\r\n";
				lDataLength += SendMsg(sr, sMsg);
				sMsg = "Content-Transfer-Encoding:7bit\r\n";
				lDataLength += SendMsg(sr, sMsg);
				sMsg = "X-Amconverter:emoji,decomail\r\n";
				lDataLength += SendMsg(sr, sMsg);
				sMsg = string.Format("X-AM-Envelope-From: {0}\r\n", pMailRturnTo);
				lDataLength += SendMsg(sr, sMsg);

				// MIME部分 
				lDataLength += SendMsg(sr, "\r\n");
				sMsg = "--mimemk00_0\r\n";
				lDataLength += SendMsg(sr, sMsg);
				sMsg = "Content-Type:multipart/alternative;boundary=\"mimemk01\"\r\n";
				lDataLength += SendMsg(sr, sMsg);
				lDataLength += SendMsg(sr, "\r\n");

				// text/plainパート 
				sMsg = "--mimemk01\r\n";
				lDataLength += SendMsg(sr, sMsg);
				sMsg = "Content-Type:text/plain;charset=\"Shift_JIS\"\r\n";
				lDataLength += SendMsg(sr, sMsg);
				sMsg = "Content-Transfer-Encoding:8bit\r\n";
				lDataLength += SendMsg(sr, sMsg);
				lDataLength += SendMsg(sr, "\r\n");   // 本文開始前に改行
				lDataLength += SendMultiLineMsg(sr, pTextBody, true);		
				lDataLength += SendMsg(sr, "\r\n");

				// text/htmlパート
				lDataLength += SendMsg(sr, "--mimemk01\r\n");
				lDataLength += SendMsg(sr, "Content-Type:text/html;charset=Shift_JIS\r\n");
				lDataLength += SendMsg(sr, "Content-Transfer-Encoding:8bit\r\n");
				lDataLength += SendMsg(sr, "\r\n");   // 本文開始前に改行 
				lDataLength += SendMultiLineMsg(sr, sHtmlBody, true);
				lDataLength += SendMsg(sr, "\r\n");
				
				lDataLength += SendMsg(sr, "--mimemk01--\r\n");

				// 画像パート 
				i = 0;

				foreach (System.Collections.Generic.KeyValuePair<string,string> idImagePair in pBase64ImageDict) {
					lDataLength += SendMsg(sr, "\r\n");
					lDataLength += SendMsg(sr, "--mimemk00_0\r\n");

					if (pExtensionDict[idImagePair.Key] == "jpg") {
						lDataLength += SendMsg(sr, "Content-Type:image/jpeg;\r\n");
					} else {
						lDataLength += SendMsg(sr, string.Format("Content-Type:image/{0};\r\n", pExtensionDict[idImagePair.Key]));
					}
					lDataLength += SendMsg(sr, string.Format(" name=\"test{0:D2}.{1}\"\r\n", i, pExtensionDict[idImagePair.Key]));
					lDataLength += SendMsg(sr, "Content-Transfer-Encoding:base64\r\n");
					lDataLength += SendMsg(sr, "Content-ID:<" + idImagePair.Key + ">\r\n");
					lDataLength += SendMsg(sr, "\r\n");   // 本文開始前に改行 

					List<string> substValue = substringAtCount(idImagePair.Value,limitLengthOfSendByte);

					foreach (string value in substValue) {
						lDataLength += SendMsg(sr, value + "\r\n");
					}
					i++;
				}

				lDataLength += SendMsg(sr, "\r\n");
				lDataLength += SendMsg(sr, "--mimemk00_0--\r\n");



				SendDataEndCmd(sr);
			} else {
				throw new NotSupportedException(string.Format("不正なメール送信種別です。{0}",pMailSendType));
			}
				
			/******** END DATA コマンド ***************************************************************/

			// QUIT
			SendQuit(sr);

		}finally{
			try {
				oTcpClient.Close();
			} catch(Exception ex) {
				EventLogWriter.Error(ex);
			}		
		}
		
		return lDataLength;
	}

	/// <summary>
	/// 文字列を指定した文字数で分割した配列にします。	/// </summary>
	/// <param name="source">元の文字列</param>
	/// <param name="count">分割する文字数</param>
	/// <returns>分割した配列</returns>
	private List<string> substringAtCount(string source,int count) {
		List<string> substList = new List<string>();
		int length = (int)Math.Ceiling((double)source.Length / (double)count);

		for (int i = 0;i < length;i++) {
			int start = count * i;
			if (start >= source.Length) {
				break;
			}
			if (start + count > source.Length) {
				substList.Add(source.Substring(start));
			} else {
				substList.Add(source.Substring(start,count));
			}
		}

		return substList;
	}
}
