﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: User
--	Title			: サイト構成文章
--	Progaram ID		: SiteHtmlDoc
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;
using ViComm;

public class SiteHtmlDoc:DbSession {

	public SiteHtmlDoc() {
	}


	public bool GetOne(string pSiteHtmlDocSeq,ref string[] pHtml,ref string[] pText, ref int[] pNonDispInMailBox,ref int[] pNonDispInDecoMail) {
		DataSet ds;
		conn = DbConnect();
		ds = new DataSet();

		for (int i = 0;i < 10;i++) {
			pHtml[i] = "";
			pNonDispInMailBox[i] = 0;
			pNonDispInDecoMail[i] = 0;
		}

		string sSql = "SELECT " +
							"HTML_DOC_SUB_SEQ," +
							"HTML_DOC1," +
							"HTML_DOC2," +
							"HTML_DOC3," +
							"HTML_DOC4," +
							"HTML_DOC5," +
							"HTML_DOC6," +
							"HTML_DOC7," +
							"HTML_DOC8," +
							"HTML_DOC9," +
							"HTML_DOC10," +
							"HTML_DOC11," +
							"HTML_DOC12," +
							"HTML_DOC13," +
							"HTML_DOC14," +
							"HTML_DOC15," +
							"HTML_DOC16," +
							"HTML_DOC17," +
							"HTML_DOC18," +
							"HTML_DOC19," +
							"HTML_DOC20," +
							"NON_DISP_IN_MAIL_BOX," +
							"NON_DISP_IN_DECOMAIL " +
						"FROM " +
						" T_SITE_HTML_DOC " +
						"WHERE " +
						" HTML_DOC_SEQ		= :HTML_DOC_SEQ	 ";


		sSql = sSql + " ORDER BY HTML_DOC_SEQ,HTML_DOC_SUB_SEQ ";

		using (cmd = CreateSelectCommand(sSql,conn)) {
			cmd.Parameters.Add("HTML_DOC_SEQ",pSiteHtmlDocSeq);
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds);
			}
		}
		conn.Close();

		int iIdx;
		foreach (DataRow dr in ds.Tables[0].Rows) {
			iIdx = int.Parse(dr["HTML_DOC_SUB_SEQ"].ToString());
			pHtml[iIdx] = dr["HTML_DOC1"].ToString() +
							dr["HTML_DOC2"].ToString() +
							dr["HTML_DOC3"].ToString() +
							dr["HTML_DOC4"].ToString() +
							dr["HTML_DOC5"].ToString() +
							dr["HTML_DOC6"].ToString() +
							dr["HTML_DOC7"].ToString() +
							dr["HTML_DOC8"].ToString() +
							dr["HTML_DOC9"].ToString() +
							dr["HTML_DOC10"].ToString();
			pText[iIdx] = dr["HTML_DOC11"].ToString() +
							dr["HTML_DOC12"].ToString() +
							dr["HTML_DOC13"].ToString() +
							dr["HTML_DOC14"].ToString() +
							dr["HTML_DOC15"].ToString() +
							dr["HTML_DOC16"].ToString() +
							dr["HTML_DOC17"].ToString() +
							dr["HTML_DOC18"].ToString() +
							dr["HTML_DOC19"].ToString() +
							dr["HTML_DOC20"].ToString(); 
							
			pNonDispInMailBox[iIdx] = int.Parse(dr["NON_DISP_IN_MAIL_BOX"].ToString());
			pNonDispInDecoMail[iIdx] = int.Parse(dr["NON_DISP_IN_DECOMAIL"].ToString());
		}

		return (ds.Tables[0].Rows.Count > 0);
	}

}
