﻿/*************************************************************************
--	System			: ViCOMM Site
--	Sub System Name	: Mail管理
--	Title			: メール配信要求
--	Progaram ID		: ReqTxMail
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date			Updater		Update Explain
  2010/10/20	K.Itoh		接続の開放のタイミングや変数のスコープなど修正

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text.RegularExpressions;
using System.Configuration;
using iBridCommLib;
using ViComm;

public class ReqTxMail:DbSession {
	public string mailTitle;
	public string webMailDoc;
	public string decoMailDoc;
	public string textMailDoc;
	public string webFaceSeq;
	public string txMailerIp;
	public string url;
	public string htmlDocSeq;
	public string mailType;
	public string mailHost;
	public string mailTemplateNo;
	public string registTrackingUrl;
	public string txSiteCd;
	public string rxSexCd;
	public string textMailFlag;
	public string fromEmailAddr;
	public int mobilMailFontSize;
	public string mobilMailBackColor;
	public int batchMailFlag;
	public DataRow dr;

	public ReqTxMail() {
		mailTitle = "";
		webMailDoc = "";
		decoMailDoc = "";
		textMailDoc = "";
		txMailerIp = "";
		htmlDocSeq = "";
		webFaceSeq = "";
		mailType = "";
		mailHost = "";
		mailTemplateNo = string.Empty;
		registTrackingUrl = "";
		txSiteCd = "";
		textMailFlag = string.Empty;
		fromEmailAddr = string.Empty;
		mobilMailFontSize = 0;
		mobilMailBackColor = string.Empty;
		batchMailFlag = 0;
	}

	public bool GetOne(string pRequestTxMailSeq,int pSendType) {
		DataSet ds;
		bool bExist = false;

		string sSql = "SELECT " +
							"REQUEST_TX_MAIL_SEQ	," +
							"HTML_DOC_SEQ			," +
							"WEB_FACE_SEQ			," +
							"MAIL_TITLE				," +
							"MAIL_TYPE				," +
							"MAIL_BOX_TYPE			," +
							"MAIL_TEMPLATE_NO		," +
							"URL					," +
							"TX_SITE_CD				," +
							"RX_SEX_CD				," +
							"ORIGINAL_DOC1			," +
							"ORIGINAL_DOC2			," +
							"ORIGINAL_DOC3			," +
							"ORIGINAL_DOC4			," +
							"ORIGINAL_DOC5			," +
							"TX_LO_MAILER_IP		," +
							"TX_HI_MAILER_IP		," +
							"MAIL_HOST				," +
							"TEXT_MAIL_FLAG			," +
							"EMAIL_ADDR				," +
							"MOBILE_MAIL_FONT_SIZE	," +
							"MOBILE_MAIL_BACK_COLOR	," +
							"REGIST_TRACKING_URL	," +
							"CAST_ONLINE_MAIL_RX_FLAG," +
							"CAST_HTML_DOC_SEQ		," +
							"CAST_CREATE_ORG_SUB_SEQ," +
							"OBJ_SEQ				," +
							"BATCH_MAIL_FLAG		," +
							"MAIL_DOC_SHORTEN_FLAG	" +
						"FROM " +
							"VW_REQ_TX_MAIL01 " +
						"WHERE " +
							"REQUEST_TX_MAIL_SEQ = :REQUEST_TX_MAIL_SEQ ";
		using (ds = new DataSet()) {

			try {
				conn = DbConnect();
				using (cmd = CreateSelectCommand(sSql,conn)) {

					cmd.Parameters.Add("REQUEST_TX_MAIL_SEQ",pRequestTxMailSeq);

					using (da = new OracleDataAdapter(cmd)) {
						da.Fill(ds,"VW_REQ_TX_MAIL01");
						if (ds.Tables["VW_REQ_TX_MAIL01"].Rows.Count != 0) {
							dr = ds.Tables["VW_REQ_TX_MAIL01"].Rows[0];
							mailTitle = dr["MAIL_TITLE"].ToString();
							webFaceSeq = dr["WEB_FACE_SEQ"].ToString();
							txMailerIp = dr["TX_HI_MAILER_IP"].ToString();
							htmlDocSeq = dr["HTML_DOC_SEQ"].ToString();
							url = dr["URL"].ToString();
							mailType = dr["MAIL_TYPE"].ToString();
							mailHost = dr["MAIL_HOST"].ToString();
							mailTemplateNo = dr["MAIL_TEMPLATE_NO"].ToString();
							registTrackingUrl = dr["REGIST_TRACKING_URL"].ToString();
							txSiteCd = dr["TX_SITE_CD"].ToString();
							rxSexCd = dr["RX_SEX_CD"].ToString();
							textMailFlag = iBridUtil.GetStringValue(dr["TEXT_MAIL_FLAG"]);
							fromEmailAddr = iBridUtil.GetStringValue(dr["EMAIL_ADDR"]);
							mobilMailFontSize = int.Parse((dr["MOBILE_MAIL_FONT_SIZE"].ToString()));
							mobilMailBackColor = dr["MOBILE_MAIL_BACK_COLOR"].ToString();
							int.TryParse(dr["BATCH_MAIL_FLAG"].ToString(),out batchMailFlag);
							bExist = true;
						}
					}
				}
			} finally {
				conn.Close();
			}

			string[] sDoc = new string[11];
			string[] sTextDoc = new string[11];
			int[] iNonDispInMailBox = new int[11];
			int[] iNonDispInDecomail = new int[11];
			string[] sCastDoc = new string[11];
			string[] sCastTextDoc = new string[11];
			
			string sShortenDoc = string.Empty;
			string sShortenTextDoc = string.Empty;

			if (bExist) {
				string sOriginalDoc = dr["ORIGINAL_DOC1"].ToString() + dr["ORIGINAL_DOC2"].ToString() + dr["ORIGINAL_DOC3"].ToString() + dr["ORIGINAL_DOC4"].ToString() + dr["ORIGINAL_DOC5"].ToString();
				string sCastHtmlDocSeq = dr["CAST_HTML_DOC_SEQ"].ToString();
				int iCastCreateOrgSubSeq = int.Parse(dr["CAST_CREATE_ORG_SUB_SEQ"].ToString());
				int iMailDocShortenFlag = int.Parse(dr["MAIL_DOC_SHORTEN_FLAG"].ToString());
				
				int iUserMailNoticeLength = 0;
				string sUserMailNoticeLength = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["UserMailNoticeLength"]);
				int.TryParse(sUserMailNoticeLength,out iUserMailNoticeLength);

				using (SiteHtmlDoc oDoc = new SiteHtmlDoc()) {
					if (oDoc.GetOne(htmlDocSeq,ref sDoc,ref sTextDoc,ref iNonDispInMailBox,ref iNonDispInDecomail)) {
						if (sOriginalDoc.Equals("")) {
							if (!sCastHtmlDocSeq.Equals("")) {
								if (oDoc.GetOne(sCastHtmlDocSeq,ref sCastDoc,ref sCastTextDoc,ref iNonDispInMailBox,ref iNonDispInDecomail)) {
									sDoc[iCastCreateOrgSubSeq] = sCastDoc[iCastCreateOrgSubSeq];
									sTextDoc[iCastCreateOrgSubSeq] = sCastTextDoc[iCastCreateOrgSubSeq];
								}
							}
						} else {
							switch (mailType) {
								case ViCommConst.MAIL_TP_INFO:
								case ViCommConst.MAIL_TP_CAST_MAIL_MAGAZINE:
									sDoc[1] = sOriginalDoc;
									sTextDoc[1] = MailBodyHelper.ConvertHtml2Text(sOriginalDoc);
									break;
								default:
									if (iMailDocShortenFlag == ViCommConst.FLAG_ON) {
										//絵文字の置換文字列に対応(絵文字は1文字として扱う)
										int iMatchIndex = 0;
										Regex rgx = new Regex(@"\$x.{4};",RegexOptions.Compiled);
										Match m = rgx.Match(sOriginalDoc);
										foreach (Match match in rgx.Matches(sOriginalDoc)) {
											iMatchIndex = match.Index;
											
											if (iMatchIndex + 1 <= iUserMailNoticeLength) {
												iUserMailNoticeLength = iUserMailNoticeLength + match.Value.Length - 1;
											} else {
												break;
											}
										}
										
										//一括送信時のﾊﾝﾄﾞﾙ名置換文字列に対応(ﾊﾝﾄﾞﾙ名の文字数は置換文字列の文字数として扱う)
										iMatchIndex = 0;
										rgx = new Regex(@"%%\d{1,2}%%",RegexOptions.Compiled);
										foreach (Match match in rgx.Matches(sOriginalDoc)) {
											iMatchIndex = match.Index;

											if (iMatchIndex + 1 <= iUserMailNoticeLength) {
												if (iMatchIndex + 1 + match.Length >= iUserMailNoticeLength) {
													iUserMailNoticeLength = iMatchIndex + match.Value.Length;
													break;
												}
											} else {
												break;
											}
										}
										
										string sUserMailNoticeDoc = string.Empty;
										if (sOriginalDoc.Length > iUserMailNoticeLength) {
											sUserMailNoticeDoc = string.Format("{0}…",sOriginalDoc.Substring(0,iUserMailNoticeLength));
										} else {
											sUserMailNoticeDoc = sOriginalDoc;
										}
										
										sUserMailNoticeDoc = sUserMailNoticeDoc.Replace(System.Environment.NewLine,"&nbsp;");
										sShortenDoc = sUserMailNoticeDoc;
										sShortenTextDoc = MailBodyHelper.ConvertHtml2Text(sUserMailNoticeDoc);
									}
									
									sOriginalDoc = sOriginalDoc.Replace(System.Environment.NewLine,"<br />");
									sDoc[iCastCreateOrgSubSeq] = sOriginalDoc;
									sTextDoc[iCastCreateOrgSubSeq] = MailBodyHelper.ConvertHtml2Text(sOriginalDoc);
									break;
							}
						}
						for (int i = 1;i <= 10;i++) {
							if (iNonDispInMailBox[i] == 0) {
								switch (pSendType) {
									case ViCommConst.MailSendType.NONE:
										if (!textMailFlag.Equals("0")) {
											webMailDoc += sTextDoc[i];
										} else {
											webMailDoc += sDoc[i];
										}
										break;
									case ViCommConst.MailSendType.TEXT:
										webMailDoc += sTextDoc[i];
										break;
									default:
										webMailDoc += sDoc[i];
										break;
								}
							}
							if (iNonDispInDecomail[i] == 0) {
								if (i == iCastCreateOrgSubSeq && iMailDocShortenFlag == ViCommConst.FLAG_ON) {
									decoMailDoc += sShortenDoc;
									textMailDoc += sShortenTextDoc;
								} else {
									decoMailDoc += sDoc[i];
									textMailDoc += sTextDoc[i];
								}
							}
						}
					}
				}
			}

		}

		return bExist;
	}
}
