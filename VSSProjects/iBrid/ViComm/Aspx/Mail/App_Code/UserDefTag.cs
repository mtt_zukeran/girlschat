﻿/*************************************************************************
--	System			: ViCOMM Site
--	Sub System Name	: Mail管理

--	Title			: ユーザ定義タグ
--	Progaram ID		: UserDefTag
--
--  Creation Date	: 2017.04.14
--  Creater			: M&TT Zukeran
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class UserDefTag:DbSession {
	public UserDefTag() {
	}

	public string GetHtmlDoc(string pTag) {
		StringBuilder sSql = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		string sHtmlDoc = string.Empty;

		sSql.AppendLine("SELECT");
		sSql.AppendLine("	HTML_DOC1");
		sSql.AppendLine("FROM");
		sSql.AppendLine("	T_USER_DEFINE_TAG");
		sSql.AppendLine("WHERE");
		sSql.AppendLine("	VARIABLE_ID = :VARIABLE_ID");

		oParamList.Add(new OracleParameter(":VARIABLE_ID",pTag));

		DataSet oDataSet = ExecuteSelectQueryBase(sSql,oParamList.ToArray());
		if (oDataSet.Tables[0].Rows.Count > 0) {
			sHtmlDoc = oDataSet.Tables[0].Rows[0]["HTML_DOC1"].ToString();
		}

		return sHtmlDoc;
	}

}

