﻿/*************************************************************************
--	System			: ViCOMM Site
--	Sub System Name	: Mail管理
--	Title			: メール配信要求明細拡張
--	Progaram ID		: ReqTxMailDtlEx
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using iBridCommLib;

public class ReqTxMailDtlEx:DbSession {


	public ReqTxMailDtlEx() {
	}

	public DataSet GetList(string pRequestTxMailSeq,string pRequestTxMailSubSeq) {
		DataSet ds;

		conn = DbConnect();
		ds = new DataSet();

 		StringBuilder sSql = new StringBuilder();
		sSql.Append("SELECT			").AppendLine();
		sSql.Append("	LOGIN_ID	,").AppendLine();
		sSql.Append("	HANDLE_NM	").AppendLine();
		sSql.Append("FROM").AppendLine();
		sSql.Append("	T_REQ_TX_MAIL_DTL_EX").AppendLine();
		sSql.Append("WHERE			").AppendLine();
		sSql.Append("	REQUEST_TX_MAIL_SEQ		= :REQUEST_TX_MAIL_SEQ	AND").AppendLine();
		sSql.Append("	REQUEST_TX_MAIL_SUBSEQ	= :REQUEST_TX_MAIL_SUBSEQ").AppendLine();
		sSql.Append("ORDER BY").AppendLine();
		sSql.Append("	REQUEST_TX_MAIL_SEQ,REQUEST_TX_MAIL_SUBSEQ,EX_SEQ").AppendLine();

		using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
			cmd.Parameters.Add("REQUEST_TX_MAIL_SEQ",pRequestTxMailSeq);
			cmd.Parameters.Add("REQUEST_TX_MAIL_SUBSEQ",pRequestTxMailSubSeq);
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds);
			}
		}
		conn.Close();
		return ds;
	}

}
