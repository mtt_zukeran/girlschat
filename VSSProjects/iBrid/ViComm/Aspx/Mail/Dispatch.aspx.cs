﻿using System;
using System.IO;
using System.Data;
using System.Web;
using System.Net;
using System.Text;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Configuration;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class Dispatch:MailDispatchBase {

	private string webMailDoc = "";
	private string decoMailDoc = "";
	private string dataSet = "";
	private string subject = "";
	private string mtIP = "";
	private string url = "";
	private string webFaceSeq = "";
	private DataRow curRow;
	private string colorTag = "";
	private string mailType = "";
	private string mailHost = "";
	private string mailDocForMX = "";
	private string trackingUrl = "";
	private string txSiteCd = "";
	private string serverType = "";
	private string castPhotoImg = "";
	private int mailTxUnit = 0;
	private int mailTxCycle = 0;
	private ReqTxMail reqTxMail;

	protected void Page_Load(object sender,EventArgs e) {
		string sReqSeq = iBridUtil.GetStringValue(Request.QueryString["reqno"]);

		using (Sys oSys = new Sys()) {
			oSys.GetValue("DECOMAIL_SERVER_TYPE",out serverType);

			string sValue;
			oSys.GetValue("MAIL_TX_UNIT",out sValue);
			mailTxUnit = int.Parse(sValue);

			oSys.GetValue("MAIL_TX_CYCLE_SEC",out sValue);
			mailTxCycle = int.Parse(sValue);

		}
		int iCnt = 0;
		int iCycle = 0;

		using (reqTxMail = new ReqTxMail()) {
			if (!reqTxMail.GetOne(sReqSeq,ViComm.ViCommConst.MailSendType.DECO_MAIL)) {
				Response.ContentType = "text/html";
				Response.Write("result=-1");
				Response.End();
				return;
			} else {
				subject = reqTxMail.mailTitle;
				webMailDoc = reqTxMail.webMailDoc;
				decoMailDoc = reqTxMail.decoMailDoc;
				mtIP = reqTxMail.txMailerIp;
				url = reqTxMail.url;
				webFaceSeq = reqTxMail.webFaceSeq;
				mailType = reqTxMail.mailType;
				mailHost = reqTxMail.mailHost;
				trackingUrl = reqTxMail.registTrackingUrl;
				txSiteCd = reqTxMail.txSiteCd;
				using (WebFace oWebFace = new WebFace()) {
					if (oWebFace.GetOne(webFaceSeq)) {
						colorTag = string.Format("<table bgcolor=\"{0}\"><tr><td><font color=\"{1}\">",oWebFace.colorBack,oWebFace.colorChar);
					}
				}
			}

			using (MailBodyDomainSettings oMailBodyDomainSettings = new MailBodyDomainSettings()) {
				string sReplaceDomain = oMailBodyDomainSettings.GetReplaceDomain(txSiteCd);
				if (!sReplaceDomain.Equals("")) {
					url = "http://" + sReplaceDomain;
				}
			}

			Regex regex1 = new Regex("src=(\".*?\")",RegexOptions.IgnoreCase);
			if (serverType.Equals(ViCommConst.DECO_SRV_COCOSPACE) == false) {
				mailDocForMX = regex1.Replace(decoMailDoc,new MatchEvaluator(RegexMatchEvaluator1));
			} else {
				mailDocForMX = decoMailDoc;
			}

			mailDocForMX = mailDocForMX.Replace("<hide>","");
			mailDocForMX = mailDocForMX.Replace("</hide>","");

			bool bTxMail;
			Regex regex2 = new Regex(@"(%%\d{1,2}%%)");
			Regex regex3 = new Regex(@"(\$x\w{1,};)");
			Regex regex4 = new Regex(@"(<hide>.*<\/hide>)");

			string sMailBoxDoc = "";

			using (MailLog oMailLog = new MailLog())
			using (ReqTxMailDtl oDtl = new ReqTxMailDtl()) {

				DataSet ds = oDtl.GetList(sReqSeq);
				foreach (DataRow dr in ds.Tables[0].Rows) {
					curRow = dr;

					if (dr["NON_EXIST_MAIL_ADDR_FLAG"].ToString().Equals("0")) {
						switch (mailType) {
							case ViCommConst.MAIL_TP_APPROACH_TO_USER:
							case ViCommConst.MAIL_TP_RETURN_TO_USER:
							case ViCommConst.MAIL_TP_INVITE_TALK:
							case ViCommConst.MAIL_TP_NOTIFY_LOGOUT:
							case ViCommConst.MAIL_TP_APPROACH_ADMIN_TO_USER:
								bTxMail = dr["USER_MAIL_RX_TYPE"].ToString().Equals("2");
								break;

							case ViCommConst.MAIL_TP_INFO:
							case ViCommConst.MAIL_TP_DM:
								bTxMail = dr["INFO_MAIL_RX_TYPE"].ToString().Equals("2");
								break;

							case ViCommConst.MAIL_TP_APPROACH_TO_CAST:
							case ViCommConst.MAIL_TP_RETURN_TO_CAST:
								bTxMail = CheckMobileMailRx(reqTxMail.dr,dr);
								break;

							default:
								bTxMail = true;
								break;
						}

						switch (mailType) {
							case ViCommConst.MAIL_TP_APPROACH_TO_CAST:
							case ViCommConst.MAIL_TP_RETURN_TO_CAST:
							case ViCommConst.MAIL_TP_CAST_PAYOUT:
							case ViCommConst.MAIL_TP_CAST_MAIL_MAGAZINE:
							case ViCommConst.MAIL_TP_CAST_MODIFY_COMPLITED:
								if ((dr["USER_STATUS"].ToString().Equals(ViCommConst.USER_WOMAN_STOP)) ||
								   (dr["USER_STATUS"].ToString().Equals(ViCommConst.USER_WOMAN_RESIGNED)) ||
								   (dr["USER_STATUS"].ToString().Equals(ViCommConst.USER_WOMAN_BAN))) {
									bTxMail = false;
								}
								break;

							case ViCommConst.MAIL_TP_APPROACH_TO_USER:
							case ViCommConst.MAIL_TP_RETURN_TO_USER:
							case ViCommConst.MAIL_TP_APPROACH_ADMIN_TO_USER:
							case ViCommConst.MAIL_TP_INFO:
							case ViCommConst.MAIL_TP_NOTIFY_LOGIN:
							case ViCommConst.MAIL_TP_INVITE_TALK:
							case ViCommConst.MAIL_TP_DM:
							case ViCommConst.MAIL_TP_NOTIFY_LOGOUT:
							case ViCommConst.MAIL_TP_MODIFY_COMPLITED:
								if ((dr["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_RESIGNED)) ||
								   (dr["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_STOP)) ||
								   (dr["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_BLACK)) ||
								   (dr["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_HOLD))) {
									bTxMail = false;
								}
								break;

							case ViCommConst.MAIL_TP_URGE:
								break;

							default:
								break;
						}
					} else {
						bTxMail = false;
					}


					if (bTxMail) {
						if (serverType.Equals(ViCommConst.DECO_SRV_COCOSPACE)) {
							iCnt++;
						}

						dataSet = dataSet + dr["RX_EMAIL_ADDR"].ToString() + "\t" +
									dr["RX_EMAIL_ADDR"].ToString() + "\t" +
									dr["RX_LOGIN_ID"].ToString() + "\t" +
									dr["RX_LOGIN_PASSWORD"].ToString() + "\t" +
									dr["RX_HANDLE_NM"].ToString() + "\t" +
									dr["TX_HANDLE_NM"].ToString() + "\t" +
									dr["TX_LOGIN_ID"].ToString() + "\t";

						if (serverType.Equals(ViCommConst.DECO_SRV_COCOSPACE) == false) {
							dataSet = dataSet + "\"" + txSiteCd + "_" + dr["TX_LOGIN_ID"].ToString() + dr["TX_USER_CHAR_NO"].ToString() + ".jpg\"" + "\t";
						} else {
							castPhotoImg = dr["TX_USER_IMG_FULL_PATH"].ToString();
							dataSet = dataSet + "\t";
						}
						dataSet = dataSet + dr["TX_ATTR1"].ToString() + "\t" +
									dr["TX_ATTR2"].ToString() + "\t" +
									dr["GENERAL1"].ToString() + "\t" +
									dr["GENERAL2"].ToString() + "\t" +
									url + "\t" +
									dr["MOBILE_MAIL_GENERAL1"].ToString() + "\t" +
									dr["CHAR_NO_ITEM_NM"].ToString() + "\t" +
									dr["CRYPT_VALUE"].ToString() + "\r\n";
						dataSet = regex3.Replace(dataSet,"");
					} else {
						oDtl.RefuseTxMail(sReqSeq,dr["REQUEST_TX_MAIL_SUBSEQ"].ToString());
					}

					if (!dr["MAIL_SEQ"].ToString().Equals("")) {
						string sSubject = regex2.Replace(subject,new MatchEvaluator(RegexMatchEvaluator2));
						sMailBoxDoc = regex2.Replace(webMailDoc,new MatchEvaluator(RegexMatchEvaluator2));
						sMailBoxDoc = regex4.Replace(sMailBoxDoc,"");
						string colorEndTag = "</font></td></tr></table>";
						using (Site oSite = new Site()) {
							if (oSite.GetOne(reqTxMail.txSiteCd)) {
								if (oSite.mailDocNotUseWebFaceFlag == ViCommConst.FLAG_ON) {
									colorTag = string.Empty;
									colorEndTag = string.Empty;
								}
							}
						}
						sMailBoxDoc = colorTag + sMailBoxDoc + colorEndTag;
						oMailLog.UpdateMailDoc(dr["MAIL_SEQ"].ToString(),sSubject,sMailBoxDoc,0,reqTxMail.textMailFlag);
					}

					if (serverType.Equals(ViCommConst.DECO_SRV_COCOSPACE)) {
						subject = regex2.Replace(subject,new MatchEvaluator(RegexMatchEvaluator2));
					}

					if ((serverType.Equals(ViCommConst.DECO_SRV_COCOSPACE)) && (iCnt >= mailTxUnit)) {
						PostMT((iCycle == 0),iCycle * mailTxCycle);
						iCnt = 0;
						iCycle++;
						dataSet = "";
					}
				}
			}
		}

		if (serverType.Equals(ViCommConst.DECO_SRV_COCOSPACE)) {
			if (iCnt > 0) {
				PostMT((iCycle == 0),iCycle * mailTxCycle);
			}
		} else {
			PostMT(true,0);
		}
		Response.ContentType = "text/html";
		Response.Write("result=0");
		Response.End();
	}

	private string RegexMatchEvaluator1(Match match) {
		string sImg = match.Value.Replace("\"","");
		if (sImg.IndexOf("%%") > 0) {
			return sImg;
		}

		string[] sImgPath = sImg.Split('/');
		return "src=\"" + sImgPath[sImgPath.Length - 1] + "\"";
	}


	private string RegexMatchEvaluator2(Match match) {
		string sValue = "";

		switch (match.Value) {
			case "%%0%%":
				sValue = curRow["RX_EMAIL_ADDR"].ToString();
				break;
			case "%%1%%":
				sValue = curRow["RX_EMAIL_ADDR"].ToString();
				break;
			case "%%2%%":
				sValue = curRow["RX_LOGIN_ID"].ToString();
				break;
			case "%%3%%":
				sValue = curRow["RX_LOGIN_PASSWORD"].ToString();
				break;
			case "%%4%%":
				sValue = curRow["RX_HANDLE_NM"].ToString();
				break;
			case "%%5%%":
				sValue = curRow["TX_HANDLE_NM"].ToString();
				break;
			case "%%6%%":
				sValue = curRow["TX_LOGIN_ID"].ToString();
				break;
			case "%%7%%":
				sValue = curRow["TX_IMG_PATH"].ToString();
				break;
			case "%%8%%":
				sValue = curRow["TX_ATTR1"].ToString();
				break;
			case "%%9%%":
				sValue = curRow["TX_ATTR2"].ToString();
				break;
			case "%%10%%":
				sValue = curRow["GENERAL1"].ToString();
				break;
			case "%%11%%":
				sValue = curRow["GENERAL2"].ToString();
				break;
			case "%%12%%":
				sValue = url;
				break;
			case "%%13%%":
				sValue = curRow["WEB_MAIL_GENERAL1"].ToString();
				break;
			case "%%14%%":
				sValue = curRow["CHAR_NO_ITEM_NM"].ToString();
				break;
			case "%%15%%":
				sValue = curRow["CRYPT_VALUE"].ToString();
				break;
		}
		return sValue;
	}


	public void PostMT(bool pInit,int pSleepSec) {
		string sUrl;
		bool bLogging = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["Logging"]).Equals("1");

		if (!trackingUrl.Equals("")) {
			string[] sTrackingInfo = trackingUrl.Split(',');
			string sTrackingUrl = "";

			if (sTrackingInfo.Length == 5) {
				sTrackingUrl = string.Format(sTrackingInfo[1],sTrackingInfo[2],sTrackingInfo[3]);
				ViCommInterface.TransToParent(sTrackingUrl,true);
			}
		}

		Encoding enc = Encoding.GetEncoding("Shift_JIS");

		if (pInit) {
			using (WebFace oWebFace = new WebFace()) {
				if (oWebFace.GetOne(webFaceSeq)) {
					mailDocForMX = string.Format("<body bgcolor=\"{0}\" link=\"{1}\" vlink=\"{2}\"><font color=\"{3}\">",oWebFace.colorBack,oWebFace.colorLink,oWebFace.colorVLink,oWebFace.colorChar) + mailDocForMX + "</font></body>";
				}
			}
			ParseMobile oParseMobile = new ParseMobile(@"\$x\w{1,};",false,"","","",RegexOptions.IgnoreCase,ViCommConst.DOCOMO);
			ParseHTML oParser = new ParseHTML(oParseMobile);

			subject = oParser.Parse(subject);
			mailDocForMX = oParser.Parse(mailDocForMX);
			if (serverType.Equals(ViCommConst.DECO_SRV_COCOSPACE)) {
				mailDocForMX = mailDocForMX.Replace("<BR>","\r\n");
				mailDocForMX = mailDocForMX.Replace("<br>","\r\n");
				mailDocForMX = mailDocForMX.Replace("%%7%%",castPhotoImg);
			}
			subject = System.Web.HttpUtility.UrlEncode(subject,enc);
			mailDocForMX = System.Web.HttpUtility.UrlEncode(mailDocForMX,enc);
		}

		if (serverType.Equals(ViCommConst.DECO_SRV_COCOSPACE)) {
			sUrl = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["DecoMailUrl"]);
		} else {
			sUrl = string.Format("http://{0}/msgsend.php?",mtIP);
		}

		dataSet = System.Web.HttpUtility.UrlEncode(dataSet,enc);

		string sPostData = string.Format("data={0}&from=info@{3}&returnpath=returnmail@{3}&subject={1}&body={2}",dataSet,subject,mailDocForMX,mailHost);

		try {
			ViCommInterface.PostToParent(sUrl,sPostData,true,bLogging,pSleepSec * 1000);
		} catch (Exception e) {
			ViCommInterface.WriteIFError(e,"MailInterface",sUrl + sPostData);
		}
	}

}


