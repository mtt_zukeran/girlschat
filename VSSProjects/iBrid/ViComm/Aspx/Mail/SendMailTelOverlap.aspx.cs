﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Mail
--	Title			: 電話番号重複通知メール送信
--	Progaram ID		: SendMailTelOverlap
--  Creation Date	: 2014.09.16
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.IO;
using System.Data;
using System.Web;
using System.Net;
using Agiletech.Net.Mail;
using System.Text;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Configuration;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class SendMailTelOverlap:System.Web.UI.Page {
	protected void Page_Load(object sender,EventArgs e) {
		string sTo = iBridUtil.GetStringValue(Request.QueryString["to"]);
		string sFrom = iBridUtil.GetStringValue(Request.QueryString["from"]);
		string sTel = iBridUtil.GetStringValue(Request.QueryString["tel"]);
		string sNewId = iBridUtil.GetStringValue(Request.QueryString["newid"]);
		string sOldId = iBridUtil.GetStringValue(Request.QueryString["oldid"]);

		if (!string.IsNullOrEmpty(sTo) && !string.IsNullOrEmpty(sFrom) && !string.IsNullOrEmpty(sTel) && !string.IsNullOrEmpty(sNewId) && !string.IsNullOrEmpty(sOldId)) {
			string sTitle = "電話番号重複通知";

			StringBuilder oDoc = new StringBuilder();
			oDoc.AppendFormat("電話番号：{0}",sTel).AppendLine();
			oDoc.AppendFormat("発信者のログインID：{0}",sNewId).AppendLine();
			oDoc.AppendFormat("重複相手のログインID：{0}",sOldId).AppendLine();

			SendMail(sFrom,sTo,sTitle,oDoc.ToString());
			Response.ContentType = "text/html";
			Response.Write("result=0");
			Response.End();
		}

		Response.ContentType = "text/html";
		Response.Write("result=-1");
		Response.End();
	}

	private void SendMail(string fromAddress,string toAddress,string title,string contents) {
		string sSendMailEncode = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["SendMailEncode"]).Trim();
		if (string.IsNullOrEmpty(sSendMailEncode)) {
			sSendMailEncode = "Shift_JIS";
		}
		Encoding enc = Encoding.GetEncoding(sSendMailEncode);

		Agiletech.Net.Mail.MailMessage msg = new Agiletech.Net.Mail.MailMessage();

		msg.SubjectEncoding = enc;
		msg.BodyEncoding = enc;
		msg.From = new Agiletech.Net.Mail.MailAddress(fromAddress);
		msg.To.Add(new Agiletech.Net.Mail.MailAddress(toAddress));
		msg.Subject = title;
		msg.Body = contents;
		msg.Headers.HeaderItems.Add("Date",DateTime.Now.ToString("R").Replace("GMT","+0900"));

		Agiletech.Net.Mail.SMTPClient sc = new Agiletech.Net.Mail.SMTPClient("127.0.0.1");
		sc.SendMail(msg);
	}
}
