﻿using System;
using System.IO;
using System.Data;
using System.Web;
using System.Net;
using Agiletech.Net.Mail;
using System.Text;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Configuration;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class SendMailToAdmin:System.Web.UI.Page {

	private string toEmailAddr = "";
	private string tempRegistId = "";
	private string agreeFlag = "";

	protected void Page_Load(object sender,EventArgs e) {
		toEmailAddr = iBridUtil.GetStringValue(Request.QueryString["emailaddr"]);
		tempRegistId = iBridUtil.GetStringValue(Request.QueryString["tempregid"]);
		agreeFlag = iBridUtil.GetStringValue(Request.QueryString["agree"]);

		if ((!string.IsNullOrEmpty(toEmailAddr)) && (!string.IsNullOrEmpty(tempRegistId))) {
			string sTitle = DisplayWordUtil.Replace("オペレーター仮登録希望");
			string sFromMailAddr;
			string sDoc = CreateMailDoc(out sFromMailAddr);

			// ﾒｰﾙ送信
			if (!string.IsNullOrEmpty(sDoc)) {
				SendMail(sFromMailAddr,toEmailAddr,sTitle,sDoc);
				Response.ContentType = "text/html";
				Response.Write("result=0");
				Response.End();
			}
		}

		Response.ContentType = "text/html";
		Response.Write("result=-1");
		Response.End();
	}


	private void SendMail(string fromAddress,string toAddress,string title,string contents) {
		string sSendMailEncode = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["SendMailEncode"]).Trim();
		if (string.IsNullOrEmpty(sSendMailEncode)) {
			sSendMailEncode = "Shift_JIS";
		}
		Encoding enc = Encoding.GetEncoding(sSendMailEncode);

		Agiletech.Net.Mail.MailMessage msg = new Agiletech.Net.Mail.MailMessage();

		msg.SubjectEncoding = enc;
		msg.BodyEncoding = enc;
		msg.From = new Agiletech.Net.Mail.MailAddress(fromAddress);
		msg.To.Add(new Agiletech.Net.Mail.MailAddress(toAddress));
		msg.Subject = title;
		msg.Body = contents;
		msg.Headers.HeaderItems.Add("Date",DateTime.Now.ToString("R").Replace("GMT","+0900"));

		Agiletech.Net.Mail.SMTPClient sc = new Agiletech.Net.Mail.SMTPClient("127.0.0.1");
		sc.SendMail(msg);
	}

	private string CreateMailDoc(out string pFromMailAddr) {
		string sDoc = "";
		pFromMailAddr = "";
		using (TempRegist oTempRegist = new TempRegist()) {
			if (oTempRegist.GetOne(tempRegistId)) {
				pFromMailAddr = oTempRegist.emailAddr;
				
				sDoc += "■電話番号" + "\r\n";
				sDoc += oTempRegist.tel + "\r\n" + "\r\n";
				sDoc += "■氏名(漢字)" + "\r\n";
				sDoc += oTempRegist.castNm + "\r\n" + "\r\n";
				sDoc += "■氏名(ﾌﾘｶﾞﾅ)" + "\r\n";
				sDoc += oTempRegist.castKanaNm + "\r\n" + "\r\n";
				sDoc += "■生年月日" + "\r\n";
				sDoc += string.Format("{0}/{1}/{2} ({3}才)",oTempRegist.birthday.Substring(0,4),oTempRegist.birthday.Substring(4,2),oTempRegist.birthday.Substring(6,2),ViCommPrograms.Age(string.Format("{0}/{1}/{2}",oTempRegist.birthday.Substring(0,4),oTempRegist.birthday.Substring(4,2),oTempRegist.birthday.Substring(6,2)))) + "\r\n" + "\r\n";
				sDoc += "■広告媒体" + "\r\n";
				sDoc += oTempRegist.adNm + "\r\n" + "\r\n";
				sDoc += "■紹介元ﾛｸﾞｲﾝID" + "\r\n";
				sDoc += oTempRegist.friendLoginId + "\r\n" + "\r\n";
				sDoc += "■質問内容" + "\r\n";
				sDoc += oTempRegist.questionDoc + "\r\n" + "\r\n";
				if (agreeFlag.Equals(ViCommConst.FLAG_ON.ToString())) {
					sDoc += "■規約への同意" + "\r\n";
					sDoc += "同意します" + "\r\n" + "\r\n";
				}
			}
		}
		return sDoc;
	}
}
