﻿using System;
using System.IO;
using System.Data;
using System.Web;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Configuration;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class TrackingBatchReport : System.Web.UI.Page {

    protected void Page_Load(object sender,EventArgs e) {

        using (TempRegist oTemp = new TempRegist()) {
            int iMin;
            int.TryParse(iBridUtil.GetStringValue(Request.QueryString["min"]),out iMin);

            DataSet ds = oTemp.GetBatchList(DateTime.Now.AddMinutes(iMin * -1));
            foreach (DataRow dr in ds.Tables[0].Rows) {
                if (!dr["REGIST_AFFILIATE_CD"].ToString().Equals(string.Empty) && !dr["TRACKING_URL"].ToString().Equals(string.Empty)) {
                    string sUID = string.Empty;
                    if (dr["MOBILE_CARRIER_CD"].ToString().Equals(ViCommConst.KDDI)) {
                        sUID = dr["TERMINAL_UNIQUE_ID"].ToString();
                    } else {
                        sUID = dr["IMODE_ID"].ToString();
                    }
                    string sTrackingUrl = string.Format(dr["TRACKING_URL"].ToString() + dr["TRACKING_ADDITION_INFO"].ToString(),
                            dr["REGIST_AFFILIATE_CD"].ToString(),dr["LOGIN_ID"].ToString(),dr["REGIST_IP_ADDR"].ToString(),sUID);
                    ViCommInterface.TransToParent(sTrackingUrl,true);
                }
                oTemp.CompliteTrackingReport(dr["TEMP_REGIST_ID"].ToString(),1);
            }
        }
        Response.ContentType = "text/html";
        Response.Write("result=0");
        Response.End();
    }
}
