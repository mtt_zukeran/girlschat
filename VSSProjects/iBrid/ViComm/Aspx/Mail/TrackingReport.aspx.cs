﻿using System;
using System.IO;
using System.Data;
using System.Web;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Configuration;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class TrackingReport : System.Web.UI.Page {

    protected void Page_Load(object sender,EventArgs e) {
        string sUserSeq = iBridUtil.GetStringValue(Request.QueryString["userseq"]);

        using (TempRegist oTemp = new TempRegist()) {
            if (oTemp.GetOneByUserSeq(sUserSeq)) {
                if (oTemp.registStatus.Equals(ViCommConst.REGIST_COMPLITE_NO_REPORT)) {
                    if (!oTemp.registAffiliateCd.Equals(string.Empty) && !oTemp.trackingUrl.Equals(string.Empty)) {
                        string sUID;
                        if (oTemp.carrierCd.Equals(ViCommConst.KDDI)) {
                            sUID = oTemp.terminalUniqueId;
                        } else {
                            sUID = oTemp.imodeId;
                        }
                        string sTrackingUrl = string.Format(oTemp.trackingUrl + oTemp.trackingAdditionInfo,oTemp.registAffiliateCd,oTemp.loginId,oTemp.registIpAddr,sUID);
                        ViCommInterface.TransToParent(sTrackingUrl,true);
                    }
                    oTemp.CompliteTrackingReport(sUserSeq,0);
                }
            }
        }
        Response.ContentType = "text/html";
        Response.Write("result=0");
        Response.End();
    }
}
