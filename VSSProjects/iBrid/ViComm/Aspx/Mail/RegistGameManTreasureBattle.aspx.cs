﻿using System;
using System.Threading;
using System.IO;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Configuration;
using System.Drawing;

public partial class RegistGameManTreasureBattle:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		try {
			string sSiteCd = iBridUtil.GetStringValue(this.Request.QueryString["sitecd"]);
			string sManTreasureBattleSeq = iBridUtil.GetStringValue(this.Request.QueryString["mantreasurebattleseq"]);
			string sCastGamePicSeq = iBridUtil.GetStringValue(this.Request.QueryString["castgamepicseq"]);
			string sLoginId = iBridUtil.GetStringValue(this.Request.QueryString["loginid"]);

			string sWebPhisicalDir = string.Empty;
			string sDomain = string.Empty;

			using (Site oSite = new Site()) {
				oSite.GetValue(sSiteCd,"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
				oSite.GetValue(sSiteCd,"HOST_NM",ref sDomain);
			}

			string sFileNmOrg = string.Empty,sPathOrg = string.Empty,sFullPathOrg = string.Empty;
			string sFileNmCpy = string.Empty,sPathCpy = string.Empty,sFullPathCpy = string.Empty;

			sFileNmOrg = ViCommConst.PIC_HEADER + iBridUtil.addZero(sManTreasureBattleSeq,ViCommConst.OBJECT_NM_LENGTH);
			sPathOrg = sWebPhisicalDir + "\\Image\\" + sSiteCd + "\\game\\Operator\\" + PwViCommConst.TutorialManTreasureDir.BATTLE;
			sFullPathOrg = sPathOrg + "\\" + sFileNmOrg + ViCommConst.PIC_FOODER;

			if (File.Exists(sFullPathOrg)) {
				sFileNmCpy = ViCommConst.PIC_HEADER + iBridUtil.addZero(sCastGamePicSeq,ViCommConst.OBJECT_NM_LENGTH);
				sPathCpy = ViCommPrograms.GetCastPicDir(sWebPhisicalDir,sSiteCd,sLoginId);
				sFullPathCpy = sPathCpy + "\\" + sFileNmCpy + ViCommConst.PIC_FOODER;

				System.IO.File.Copy(sFullPathOrg,sFullPathCpy,true);

				// オリジナル画像をMailToolsディレクトリに配置
				string sInputDir = ConfigurationManager.AppSettings["MailParseDir"] + "\\";
				System.IO.File.Copy(sFullPathOrg,Path.Combine(sInputDir,sFileNmCpy + ViCommConst.PIC_FOODER),true);

				ImageHelper.ConvertMobileImage(sSiteCd,sInputDir,sPathCpy,sFileNmCpy);

			}

		} catch (Exception) {
			Response.ContentType = "text/plain";
			Response.Write(string.Format("result={0}&loginid={1}","-1",string.Empty));
		}
	}
}
