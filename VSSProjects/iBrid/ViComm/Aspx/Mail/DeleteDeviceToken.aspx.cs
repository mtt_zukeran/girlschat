﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Mail
--	Title			: デバイストークン削除
--	Progaram ID		: DeleteDeviceToken
--  Creation Date	: 2014.04.10
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.IO;
using System.Data;
using System.Web;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Configuration;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class DeleteDeviceToken:System.Web.UI.Page {
	protected void Page_Load(object sender,EventArgs e) {
		string sUserSeq = iBridUtil.GetStringValue(Request.QueryString["userseq"]);

		if (string.IsNullOrEmpty(sUserSeq)) {
			Response.ContentType = "text/html";
			Response.Write("-1");
			Response.End();
			return;
		}

		using (User oUser = new User()) {
			oUser.DeleteDeviceToken(sUserSeq);
		}

		Response.ContentType = "text/html";
		Response.Write("0");
		Response.End();
		return;
	}
}
