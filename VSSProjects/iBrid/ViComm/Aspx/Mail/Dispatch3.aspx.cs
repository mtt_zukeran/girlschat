﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Mail
--	Title			: メール送信
--	Progaram ID		: Dispatch3
--  Creation Date	: 2015.02.19
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Threading;
using System.Net;
using System.IO;

using iBridCommLib;
using MobileLib;
using ViComm;

public partial class Dispatch3:MailDispatchBase {
	public class MailQueueItem {
		private string reqNo = null;
		private int mailSendType = -1;
		private string mailReturnTo = string.Empty;
		private string mailServer = string.Empty;

		public string ReqNo {
			get {
				return this.reqNo;
			}
		}
		public int MailSendType {
			get {
				return this.mailSendType;
			}
		}
		public string MailReturnTo {
			get {
				return this.mailReturnTo;
			}
		}
		public string MailServer {
			get {
				return this.mailServer;
			}
		}

		public MailQueueItem(string pReqNo,int pMailSendType,string pMailReturnTo,string pMailServer) {
			this.reqNo = pReqNo;
			this.mailSendType = pMailSendType;
			this.mailReturnTo = pMailReturnTo;
			this.mailServer = pMailServer;
		}
	}

	private static readonly Regex _hideRegex = new Regex(@"(<hide>.*<\/hide>)");
	private static readonly Encoding DefaultToAddressEnc = Encoding.GetEncoding("Shift_JIS");
	private static readonly Queue<MailQueueItem> reqSeqQueue = new Queue<MailQueueItem>();
	private volatile static object _syncRoot = 1;
	private static Semaphore _semaphore = null;

	public string _pdate;

	static Dispatch3() {
		if (ApplicationObj.Current.ThreadCount > 0) {
			_semaphore = new Semaphore(ApplicationObj.Current.ThreadCount,ApplicationObj.Current.ThreadCount);
		}
	}


	protected void Page_Load(object sender,EventArgs e) {
		int iStatus = 0;

		// =====================
		// IN 解析 
		// =====================

		string sReqSeq = iBridUtil.GetStringValue(Request.QueryString["reqno"]);
		if (string.IsNullOrEmpty(sReqSeq)) {
			iStatus = -2;
		}

		int iMailSendType = ViCommConst.MailSendType.NONE;
		if (!int.TryParse(iBridUtil.GetStringValue(Request.QueryString["sendtype"]),out iMailSendType)) {
			iMailSendType = ViCommConst.MailSendType.NONE;
		}
		string sMailReturnTo = iBridUtil.GetStringValue(Request.QueryString["returnto"]);
		bool bIsGetSizeQuery = iBridUtil.GetStringValue(Request.QueryString["getsize"]).Equals(ViCommConst.FLAG_ON_STR);
		string sMailServer = iBridUtil.GetStringValue(Request.QueryString["server"]);

		_pdate = iBridUtil.GetStringValue(Request.QueryString["pdate"]);

		// ==================================
		//  メール送信処理		
		// ==================================
		if (iStatus == 0) {
			if (bIsGetSizeQuery || ApplicationObj.Current.ThreadCount < 1) {
				// 同期送信
				iStatus = SendMail(sReqSeq,iMailSendType,sMailReturnTo,bIsGetSizeQuery,sMailServer);
			} else {
				// 非同期送信
				SendMailAsync(sReqSeq,iMailSendType,sMailReturnTo,sMailServer);
			}
		}

		// ==================================
		//  OUT 生成 	
		// ==================================
		Response.ContentType = "text/html";
		Response.Write(string.Format("result={0}",iStatus));
		Response.End();
	}

	#region □■□ メール送信処理(非同期) □■□ ==============================================================================
	/// <summary>
	/// 非同期にメール送信処理を行う
	/// </summary>
	/// <param name="pReqSeq">メールリクエスト№</param>
	private void SendMailAsync(string pReqSeq,int pMailSendType,string pMailReturnTo,string pMailServer) {
		// 要求をキューに入れる 
		lock (_syncRoot) {
			reqSeqQueue.Enqueue(new MailQueueItem(pReqSeq,pMailSendType,pMailReturnTo,pMailServer));
		}
		// ワーカースレッド起動 
		RunSendMailThread();
	}

	/// <summary>
	/// メール送信処理用のワーカースレッドを起動する 
	/// </summary>
	private void RunSendMailThread() {
		new Thread(new ThreadStart(SendMailThreadEntry)).Start();
	}
	/// <summary>
	/// メール送信処理用用のワーカースレッドのエントリﾎﾟｲﾝﾄとなるメソッド 
	/// </summary>
	private void SendMailThreadEntry() {
		try {
			// セマフォが取得できなければ処理しない 
			if (!_semaphore.WaitOne(0))
				return;

			try {

				// ダブルチェックロッキング・イディオムで要求残数をチェック。  
				MailQueueItem oQueueItem = null;
				if (reqSeqQueue.Count > 0) {
					lock (_syncRoot) {
						if (reqSeqQueue.Count > 0) {
							oQueueItem = reqSeqQueue.Dequeue();
						}
					}
				}
				if (oQueueItem == null)
					return;

				// メール送信処理 
				SendMail(oQueueItem.ReqNo,oQueueItem.MailSendType,oQueueItem.MailReturnTo,false,oQueueItem.MailServer);
			} finally {
				_semaphore.Release();
			}

			// 処理中に要求が発生していた場合はワーカースレッド再起動。 
			if (reqSeqQueue.Count > 0) {
				lock (_syncRoot) {
					for (int i = 0;i < reqSeqQueue.Count;i++) {
						// スレッドを起こしすぎないように念のためトラップ
						if (i > ApplicationObj.Current.ThreadCount)
							break;
						RunSendMailThread();
					}
				}
			}
		} catch (Exception ex) {
			// threadのエントリとなるメソッドなので、例外情報がロストしないようここでロギング
			EventLogWriter.Error(ex);
		}
	}
	#endregion ========================================================================================================


	#region □■□ メール送信処理(同期) □■□ =============================================================================

	/// <summary>
	/// メール送信処理を行う
	/// </summary>
	/// <param name="pReqSeq">メールリクエスト№</param>
	/// <param name="pMailSendType">送信するメールの種別（デコメ/テキスト)</param>
	/// <param name="pMailReturnTo">返信先として利用するアドレス</param>
	/// <param name="bIsTestMail">テスト送信かどうかを示す値。</param>
	/// <returns>処理ステータス(0:正常終了、-1:メールリクエスト№不正</returns>
	private int SendMail(string pReqSeq,int pMailSendType,string pMailReturnTo,bool pIsGetSizeQuery,string pMailServer) {
		string sProfileContentId = string.Empty;
		long lDataLength = 0;

		using (ReqTxMail oReqTxMail = new ReqTxMail()) {
			// メール情報取得 
			if (!oReqTxMail.GetOne(pReqSeq,pMailSendType)) {
				return -1;
			}

			sProfileContentId = string.Format("prof@{0}{1}",DateTime.Now.ToString("yyyyMMddHHmmssfff"),oReqTxMail.mailHost);

			if (pMailSendType == ViCommConst.MailSendType.NONE) {
				pMailSendType = (oReqTxMail.textMailFlag.Equals(ViCommConst.FLAG_ON_STR))
										? ViCommConst.MailSendType.TEXT
										: ViCommConst.MailSendType.DECO_MAIL;
			}

			string url = oReqTxMail.url;
			bool bMailDomainRandom = false;

			using (ManageCompany oManageCompany = new ManageCompany()) {
				bMailDomainRandom = oManageCompany.IsAvailableService(ViCommConst.RELEASE_MAIL_DOMAIN_RANDOM,2);
			}

			using (MailBodyDomainSettings oMailBodyDomainSettings = new MailBodyDomainSettings()) {
				string sReplaceDomain = string.Empty;

				if (bMailDomainRandom) {
					sReplaceDomain = oMailBodyDomainSettings.GetReplaceDomainRandom(oReqTxMail.txSiteCd);
				} else {
					sReplaceDomain = oMailBodyDomainSettings.GetReplaceDomain(oReqTxMail.txSiteCd);
				}

				if (!sReplaceDomain.Equals(string.Empty)) {
					url = "http://" + sReplaceDomain;
					if (!ViCommConst.MAN.Equals(oReqTxMail.rxSexCd)) {
						url = url + "/wo";
					}
				}
			}

			string sColorStartTag = string.Empty;
			string sColorEndTag = string.Empty;

			using (WebFace oWebFace = new WebFace()) {
				if (oWebFace.GetOne(oReqTxMail.webFaceSeq)) {
					sColorStartTag = string.Format("<table bgcolor=\"{0}\"><tr><td><font color=\"{1}\" size=\"{2}\">",oWebFace.colorBack,oWebFace.colorChar,oWebFace.sizeChar);
					sColorEndTag = "</font></td></tr></table>";
				}
			}

			using (Site oSite = new Site()) {
				if (oSite.GetOne(oReqTxMail.txSiteCd)) {
					if (oSite.mailDocNotUseWebFaceFlag == ViCommConst.FLAG_ON) {
						if (!oReqTxMail.mobilMailBackColor.Equals(string.Empty)) {
							sColorStartTag = string.Format("<table bgcolor=\"{0}\"><tr><td>",oReqTxMail.mobilMailBackColor);
							sColorEndTag = "</td></tr></table>";
						} else {
							sColorStartTag = string.Empty;
							sColorEndTag = string.Empty;
						}
					}
				}
			}

			string sHtmlMailDoc = oReqTxMail.decoMailDoc;
			sHtmlMailDoc = sHtmlMailDoc.Replace("<hide>",string.Empty);
			sHtmlMailDoc = sHtmlMailDoc.Replace("</hide>",string.Empty);

			bool bTxMail = true;

			List<DataRow> oReqTxMailDtlDataRowList = new List<DataRow>();
			MailBodyHelper.MailDataFormatter oMailDataFormatter = new MailBodyHelper.MailDataFormatter(url);

			using (MailLog oMailLog = new MailLog())
			using (ReqTxMailDtl oReqTxMailDtl = new ReqTxMailDtl()) {
				DataSet oReqTxMailDtlDataSet = oReqTxMailDtl.GetList(pReqSeq);
				string sUserImageFullPath = string.Empty;

				foreach (DataRow oReqTxMailDtlDataRow in oReqTxMailDtlDataSet.Tables[0].Rows) {
					if (oReqTxMailDtlDataRow["NON_EXIST_MAIL_ADDR_FLAG"].ToString().Equals("0")) {
						bTxMail = CheckMobileMailRx(oReqTxMail.dr,oReqTxMailDtlDataRow);

						switch (oReqTxMail.mailType) {
							case ViCommConst.MAIL_TP_APPROACH_TO_CAST:
							case ViCommConst.MAIL_TP_APPROACH_ADMIN_TO_CAST:
							case ViCommConst.MAIL_TP_RETURN_TO_CAST:
							case ViCommConst.MAIL_TP_CAST_PAYOUT:
							case ViCommConst.MAIL_TP_CAST_MAIL_MAGAZINE:
							case ViCommConst.MAIL_TP_CAST_MODIFY_COMPLITED:
							case ViCommConst.MAIL_TP_BUY_POINT_NOFITY:
								if ((oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_WOMAN_STOP)) ||
								   (oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_WOMAN_RESIGNED)) ||
								   (oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_WOMAN_BAN))) {
									bTxMail = false;
								}
								break;

							case ViCommConst.MAIL_TP_APPROACH_TO_USER:
							case ViCommConst.MAIL_TP_RETURN_TO_USER:
							case ViCommConst.MAIL_TP_APPROACH_ADMIN_TO_USER:
							case ViCommConst.MAIL_TP_INFO:
							case ViCommConst.MAIL_TP_NOTIFY_LOGIN:
							case ViCommConst.MAIL_TP_INVITE_TALK:
							case ViCommConst.MAIL_TP_DM:
							case ViCommConst.MAIL_TP_NOTIFY_LOGOUT:
							case ViCommConst.MAIL_TP_MODIFY_COMPLITED:
								if ((oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_RESIGNED)) ||
								   (oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_STOP)) ||
								   (oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_BLACK)) ||
								   (oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_HOLD))) {
									bTxMail = false;
								}
								break;

							case ViCommConst.MAIL_TP_ADMIN_TO_RESIGNED:
								if ((oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_STOP)) ||
								   (oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_BLACK)) ||
								   (oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_HOLD))) {
									bTxMail = false;
								}
								break;

							case ViCommConst.MAIL_TP_URGE:
								break;
							case ViCommConst.MAIL_TP_BLOG_POST_NOFITY:
								string sBlogArticleTitle = iBridUtil.GetStringValue(oReqTxMailDtlDataRow["GENERAL5"]);
								oReqTxMailDtlDataRow["GENERAL5"] = SysPrograms.Substring(sBlogArticleTitle,80);
								break;

							default:
								break;
						}
					} else if (oReqTxMailDtlDataRow["NON_EXIST_MAIL_ADDR_FLAG"].ToString().Equals("1")) {
						if (pMailServer.Equals(ViCommConst.MailServer.MULLER3)) {
							bTxMail = true;
							switch (oReqTxMail.mailType) {
								case ViCommConst.MAIL_TP_INFO:
									if ((oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_RESIGNED)) ||
									   (oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_STOP)) ||
									   (oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_BLACK)) ||
									   (oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_HOLD))) {
										bTxMail = false;
									}
									break;
								case ViCommConst.MAIL_TP_CAST_MAIL_MAGAZINE:
									if ((oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_WOMAN_STOP)) ||
									   (oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_WOMAN_RESIGNED)) ||
									   (oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_WOMAN_BAN))) {
										bTxMail = false;
									}
									break;
								case ViCommConst.MAIL_TP_ADMIN_TO_RESIGNED:
									if (oReqTxMailDtlDataRow["RX_SEX_CD"].ToString().Equals(ViCommConst.MAN)) {
										if ((oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_STOP)) ||
										   (oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_BLACK)) ||
										   (oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_HOLD))) {
											bTxMail = false;
										}
									} else {
										if ((oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_WOMAN_STOP)) ||
											(oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_WOMAN_BAN))) {
											bTxMail = false;
										}
									}
									break;
								case ViCommConst.MAIL_TP_ACCEPT_WITHDRAWAL:
									break;
								default:
									bTxMail = false;
									break;
							}
						} else {
							switch (oReqTxMail.mailType) {
								case ViCommConst.MAIL_TP_CLEANING_ADDR:
									if (oReqTxMailDtlDataRow["RX_SEX_CD"].ToString().Equals(ViCommConst.MAN)) {
										if (
										   oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_RESIGNED) ||
										   oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_STOP) ||
										   oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_BLACK) ||
										   oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_HOLD)
										) {
											bTxMail = false;
										}
									} else {
										if (
										   oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_WOMAN_STOP) ||
										   oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_WOMAN_RESIGNED) ||
										   oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_WOMAN_BAN)
										) {
											bTxMail = false;
										}
									}

									break;

								case ViCommConst.MAIL_TP_CAST_MODIFY_COMPLITED:
								case ViCommConst.MAIL_TP_MODIFY_COMPLITED:
								case ViCommConst.MAIL_TP_RELEASE_MAIL_ADDR:
								case ViCommConst.MAIL_TP_RELEASE_ADDR_CAST:
									break;

								default:
									bTxMail = false;
									break;
							}
						}
					} else {
						bTxMail = false;
					}

					if (pMailServer.Equals(ViCommConst.MailServer.MULLER3) && oReqTxMailDtlDataRow["TX_MULLER3_NG_MAIL_ADDR_FLAG"].ToString().Equals("1")) {
						bTxMail = false;
					}

					string sSubject = oMailDataFormatter.FormatVariable(oReqTxMail.mailTitle,oReqTxMailDtlDataRow,false);

					if (pIsGetSizeQuery || bTxMail) {
						if (ApplicationObj.Current.EnabledTxUserImg2Small || iBridUtil.GetStringValue(oReqTxMail.mailType).Equals(ViCommConst.MAIL_TP_BLOG_POST_NOFITY)) {
							if (ViCommConst.OPERATOR.Equals(iBridUtil.GetStringValue(oReqTxMailDtlDataRow["TX_SEX_CD"]))) {
								oReqTxMailDtlDataRow["TX_USER_IMG_FULL_PATH"] = iBridUtil.GetStringValue(oReqTxMailDtlDataRow["TX_USER_IMG_FULL_PATH"]).Replace(".gif","_s.gif");
							}
							oReqTxMailDtlDataRow["TX_USER_IMG_FULL_PATH"] = iBridUtil.GetStringValue(oReqTxMailDtlDataRow["TX_USER_IMG_FULL_PATH"]).Replace(".jpg","_s.jpg");
							oReqTxMailDtlDataRow["TX_IMG_PATH"] = iBridUtil.GetStringValue(oReqTxMailDtlDataRow["TX_IMG_PATH"]).Replace(".jpg","_s.jpg");
						}

						if (!string.IsNullOrEmpty(oReqTxMailDtlDataRow["TX_USER_IMG_FULL_PATH"].ToString())) {
							sUserImageFullPath = oReqTxMailDtlDataRow["TX_USER_IMG_FULL_PATH"].ToString();
						}

						oReqTxMailDtlDataRowList.Add(oReqTxMailDtlDataRow);

					} else {
						oReqTxMailDtl.RefuseTxMail(pReqSeq,oReqTxMailDtlDataRow["REQUEST_TX_MAIL_SUBSEQ"].ToString());
					}

					// メール種別
					switch (oReqTxMail.mailType) {
						// 清算完了
						case ViCommConst.MAIL_TP_CAST_PAYOUT:
							// ギフトコードリストタグ変換
							GiftCode oGiftCode = new GiftCode();
							sHtmlMailDoc = oGiftCode.ReplaceGiftCodeList(
								sHtmlMailDoc
								,oReqTxMailDtlDataRow["RX_USER_SEQ"].ToString()
								,_pdate
							);
							break;
					}

					// =======================================
					//  メールログの更新 
					// =======================================

					string sMailSeq = iBridUtil.GetStringValue(oReqTxMailDtlDataRow["MAIL_SEQ"]);
					if (!sMailSeq.Equals(string.Empty)) {
						string sMailBoxDoc = oMailDataFormatter.FormatVariable(oReqTxMail.webMailDoc,oReqTxMailDtlDataRow,sSubject,false);
						if (!string.IsNullOrEmpty(sMailBoxDoc)) {
							sMailBoxDoc = _hideRegex.Replace(sMailBoxDoc,string.Empty);
							sMailBoxDoc = sColorStartTag + sMailBoxDoc + sColorEndTag;
						}
						oMailLog.UpdateMailDoc(sMailSeq,sSubject,sMailBoxDoc,0,oReqTxMail.textMailFlag);
					}

					// サイズ取得が目的の場合は1件だけ処理する 
					if (pIsGetSizeQuery) {
						break;
					}
				}

				if (oReqTxMail.batchMailFlag == 1) {
					using (DbSession db = new DbSession()) {
						db.PrepareProcedure("UPDATE_BATCH_MAIL_COUNT");
						db.ProcedureInParm("PREQUEST_TX_MAIL_SEQ",DbSession.DbType.VARCHAR2,pReqSeq);
						db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
						db.ExecuteProcedure();
					}
				}

				// =======================================
				//  送信 
				// =======================================

				if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnabledLimitMailTitle"]).Equals(ViCommConst.FLAG_ON_STR)) {
					switch (oReqTxMail.mailType) {
						case ViCommConst.MAIL_TP_APPROACH_TO_CAST:
						case ViCommConst.MAIL_TP_APPROACH_ADMIN_TO_CAST:
						case ViCommConst.MAIL_TP_RETURN_TO_CAST:
						case ViCommConst.MAIL_TP_INVITE_TALK:
						case ViCommConst.MAIL_TP_DM:
						case ViCommConst.MAIL_TP_APPROACH_TO_USER:
						case ViCommConst.MAIL_TP_RETURN_TO_USER:
						case ViCommConst.MAIL_TP_APPROACH_ADMIN_TO_USER:
							oReqTxMail.mailTitle = SysPrograms.Substring(oReqTxMail.mailTitle,30);
							oReqTxMail.mailTitle = SubSubject(oReqTxMail.mailTitle,DefaultToAddressEnc);
							break;
					}
				}

				// ループが終わってから一括でメール送信する。有効な送信先が存在する場合だけ実行する 
				if (oReqTxMailDtlDataRowList.Count > 0) {
					lDataLength = PostMailServer(
						oReqTxMail,
						sHtmlMailDoc,
						pMailSendType,
						pMailReturnTo,
						pIsGetSizeQuery,
						sUserImageFullPath,
						sProfileContentId,
						oReqTxMailDtlDataRowList,
						oMailDataFormatter,
						pMailServer
					);

					if (!string.IsNullOrEmpty(oReqTxMail.txSiteCd) && !string.IsNullOrEmpty(oReqTxMail.mailTemplateNo) && !string.IsNullOrEmpty(oReqTxMail.rxSexCd)) {
						using (TxMailCountDaily oTxMailCountDaily = new TxMailCountDaily()) {
							oTxMailCountDaily.RegistTxMailCountDaily(
								oReqTxMail.txSiteCd,
								oReqTxMail.mailTemplateNo,
								oReqTxMailDtlDataRowList.Count,
								oReqTxMail.rxSexCd
							);
						}
					}
				}
			}

			if (pIsGetSizeQuery) {
				using (MailTemplate oMailTemplate = new MailTemplate()) {
					oMailTemplate.ModifyMailTemplateSize(oReqTxMail.txSiteCd,oReqTxMail.mailTemplateNo,lDataLength);
				}
			}
		}

		return 0;
	}

	#endregion ========================================================================================================

	public long PostMailServer(
		ReqTxMail pReqTxMail,
		string pHtmlMailDoc,
		int pMailSendType,
		string pMailReturnTo,
		bool pSizeOnly,
		string pUserPhotoImgUrl,
		string pProfileContentId,
		List<DataRow> pReqTxMailDtlDataRowList,
		MailBodyHelper.MailDataFormatter pFormatter,
		string pMailServer
	) {
		if (!pReqTxMail.registTrackingUrl.Equals(string.Empty)) {
			string[] sTrackingInfo = pReqTxMail.registTrackingUrl.Split(',');
			string sTrackingUrl = string.Empty;

			//互換性を持たせるために過去の5を残しておく
			if (sTrackingInfo.Length == 5) {
				sTrackingUrl = string.Format(sTrackingInfo[1],sTrackingInfo[2],sTrackingInfo[3]);
			} else if (sTrackingInfo.Length == 7) {
				sTrackingUrl = string.Format(sTrackingInfo[1],sTrackingInfo[2],sTrackingInfo[3],sTrackingInfo[4],sTrackingInfo[5]);
			}
			if (!sTrackingUrl.Equals(string.Empty)) {
				ViCommInterface.TransToParent(sTrackingUrl,true);
			}
		}

		using (Site oSite = new Site()) {
			if (oSite.GetOne(pReqTxMail.txSiteCd)) {
				if (oSite.mailDocNotUseWebFaceFlag == ViCommConst.FLAG_OFF) {
					using (WebFace oWebFace = new WebFace()) {
						if (oWebFace.GetOne(pReqTxMail.webFaceSeq)) {
							pHtmlMailDoc = string.Format("<body bgcolor=\"{0}\" link=\"{1}\" vlink=\"{2}\"><font color=\"{3}\">",oWebFace.colorBack,oWebFace.colorLink,oWebFace.colorVLink,oWebFace.colorChar) + pHtmlMailDoc + "</font></body>";
						}
					}
				} else {
					string sBodyStart = string.Empty;
					string sBodyEnd = string.Empty;
					string sFontStart = string.Empty;
					string sFontEnd = string.Empty;

					if (!pReqTxMail.mobilMailBackColor.Equals("")) {
						sBodyStart = string.Format("<body bgcolor=\"{0}\">",pReqTxMail.mobilMailBackColor);
						sBodyEnd = "</body>";
					}
					if (pReqTxMail.mobilMailFontSize != 0) {
						sFontStart = string.Format("<font size=\"{0}\">",pReqTxMail.mobilMailFontSize);
						sFontEnd = "</font>";
					}
					pHtmlMailDoc = sBodyStart + sFontStart + pHtmlMailDoc + sFontEnd + sBodyEnd;
				}
			}
		}

		// SMTPの1行1000バイトの制限に引っかからないように改行を追加します。 
		pHtmlMailDoc = pHtmlMailDoc.Replace("<BR>","<BR>\r\n");
		pHtmlMailDoc = pHtmlMailDoc.Replace("<br>","<br>\r\n");

		// ===========================
		//  画像のコンテンツ化
		// ===========================
		MailImageHelper oMailImageHelper = new MailImageHelper();

		// コンテンツIDとBase64文字列化した画像データのリスト 
		Dictionary<string,string> oBase64ImageDict = new Dictionary<string,string>();
		// コンテンツIDとファイル拡張子のリスト 
		Dictionary<string,string> oExtensionDict = new Dictionary<string,string>();

		// インライン画像のコンテンツ化 
		pHtmlMailDoc = oMailImageHelper.SearchImageFilePath(pHtmlMailDoc,pReqTxMail.url,pReqTxMail.mailHost,oBase64ImageDict,oExtensionDict);

		// 送信者画像のコンテンツ化 
		if (!string.IsNullOrEmpty(pUserPhotoImgUrl) && Regex.IsMatch(pHtmlMailDoc,"%%7%%")) {
			try {
				oExtensionDict.Add(pProfileContentId,oMailImageHelper.GetFileExtension(pUserPhotoImgUrl));
				oBase64ImageDict.Add(pProfileContentId,oMailImageHelper.ConvertImage2Base64String(pUserPhotoImgUrl,pReqTxMail.url));
			} catch (Exception ex) {
				// キャスト画像が読み込みできない場合でも処理を続行
				ViCommInterface.WriteIFLog("PostMailServer","SearchImageFilePath " + ex.Message + " " + pUserPhotoImgUrl);
			}
		}

		string sHeaderFrom = GetHeaderFrom(pReqTxMail.fromEmailAddr,pReqTxMail.mailHost);
		string sEnvelopeFrom = GetReturnToAddr(pMailReturnTo,pReqTxMail.mailHost);
		long lDataLength = 0;

		try {
			BasicSmtpClient client = new BasicSmtpClient(pSizeOnly);

			lDataLength = client.SendMail(
				pReqTxMail.mailHost,
				sEnvelopeFrom,
				sHeaderFrom,
				pReqTxMail.mailTitle,
				pHtmlMailDoc,
				pReqTxMail.textMailDoc,
				pMailSendType,
				pReqTxMailDtlDataRowList,
				oBase64ImageDict,
				oExtensionDict,
				pFormatter,
				pProfileContentId,
				pMailServer
			);

		} catch (Exception e) {
			ViCommInterface.WriteIFError(e,"PostMailServer",sHeaderFrom);
		}

		return lDataLength;
	}

	private string GetReturnToAddr(string pMailReturnTo,string pDomain) {
		if (string.IsNullOrEmpty(pMailReturnTo)) {
			bool bReturnPathRandom = false;

			using (ManageCompany oManageCompany = new ManageCompany()) {
				bReturnPathRandom = oManageCompany.IsAvailableService(ViCommConst.RELEASE_RETURN_PATH_RANDOM,2);
			}

			if (bReturnPathRandom) {
				using (ReturnPathAlias oReturnPathAlias = new ReturnPathAlias()) {
					pMailReturnTo = oReturnPathAlias.GetAliasNameRandom();
				}
			}
		}

		if (string.IsNullOrEmpty(pMailReturnTo)) {
			pMailReturnTo = "returnmail";
		}
		return string.Format("{0}@{1}",pMailReturnTo,pDomain);
	}

	private string GetHeaderFrom(string pFromEmailAddr,string pMailHost) {
		string sHeaderFrom = string.Empty;
		bool bFromAddressRandom = false;
		string sFromUserNm = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["FromUserNm"]);

		if (sFromUserNm.Equals(string.Empty)) {
			sFromUserNm = "info";
		}

		sHeaderFrom = string.IsNullOrEmpty(pFromEmailAddr) ? string.Format("{0}@{1}",sFromUserNm,pMailHost) : pFromEmailAddr;

		using (ManageCompany oManageCompany = new ManageCompany()) {
			bFromAddressRandom = oManageCompany.IsAvailableService(ViCommConst.RELEASE_FROM_ADDRESS_RANDOM,2);
		}

		if (bFromAddressRandom) {
			if (sHeaderFrom.Equals(string.Format("info@{0}",pMailHost))) {
				string sAliasName = string.Empty;

				using (FromAddressAlias oFromAddressAlias = new FromAddressAlias()) {
					sAliasName = oFromAddressAlias.GetAliasNameRandom();
				}

				if (!string.IsNullOrEmpty(sAliasName)) {
					sHeaderFrom = string.Format("{0}@{1}",sAliasName,pMailHost);
				}
			}
		}

		return sHeaderFrom;
	}

	private string SubSubject(string pSubject,Encoding pEnc) {

		string sValue = string.Empty;
		if (pEnc.GetBytes(pSubject).Length > 30) {
			//subject = enc.GetString(pEnc.GetBytes(subject), 0, 30);

			byte[] oSubjectBytes = pEnc.GetBytes(pSubject);
			int iMaxCount = pEnc.GetCharCount(oSubjectBytes);
			int iLengthCount = 0;
			for (int i = 0;i < iMaxCount;i++) {
				char[] cOneChar = pEnc.GetChars(oSubjectBytes,iLengthCount,1);
				int iCharByteCount = pEnc.GetByteCount(cOneChar);
				if (iLengthCount + iCharByteCount > 30) {
					break;
				} else {
					iLengthCount += iCharByteCount;
				}
			}

			sValue = pEnc.GetString(oSubjectBytes,0,iLengthCount);
		} else {
			sValue = pSubject;
		}

		return sValue;
	}
}


