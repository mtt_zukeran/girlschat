﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Threading;
using System.Net;
using System.IO;

using iBridCommLib;
using MobileLib;
using ViComm;

public partial class Dispatch2:MailDispatchBase {
	public class MailQueueItem {
		private string reqNo = null;
		private int mailSendType = -1;
		private int mailServerType = -1;
		private string mailReturnTo = string.Empty;

		public string ReqNo {
			get {
				return this.reqNo;
			}
		}
		public int MailSendType {
			get {
				return this.mailSendType;
			}
		}
		public int MailServerType {
			get {
				return this.mailServerType;
			}
		}
		public string MailReturnTo {
			get {
				return this.mailReturnTo;
			}
		}

		public MailQueueItem(string pReqNo,int pMailSendType,int pMailServerType,string pMailReturnTo) {
			this.reqNo = pReqNo;
			this.mailSendType = pMailSendType;
			this.mailServerType = pMailServerType;
			this.mailReturnTo = pMailReturnTo;
		}
	}

	private static readonly Regex _hideRegex = new Regex(@"(<hide>.*<\/hide>)");
	private static readonly Encoding DefaultToAddressEnc = Encoding.GetEncoding("Shift_JIS");
	private static readonly Queue<MailQueueItem> reqSeqQueue = new Queue<MailQueueItem>();
	private volatile static object _syncRoot = 1;
	private static Semaphore _semaphore = null;

	static Dispatch2() {
		if (ApplicationObj.Current.ThreadCount > 0) {
			_semaphore = new Semaphore(ApplicationObj.Current.ThreadCount,ApplicationObj.Current.ThreadCount);
		}
	}


	protected void Page_Load(object sender,EventArgs e) {
		int iStatus = 0;

		// =====================
		// IN 解析 
		// =====================

		string sReqSeq = iBridUtil.GetStringValue(Request.QueryString["reqno"]);
		if (string.IsNullOrEmpty(sReqSeq)) {
			iStatus = -2;
		}

		int iMailSendType = ViCommConst.MailSendType.NONE;
		if (!int.TryParse(iBridUtil.GetStringValue(Request.QueryString["sendtype"]),out iMailSendType)) {
			iMailSendType = ViCommConst.MailSendType.NONE;
		}
		int iMailServerType = ViCommConst.MailServerType.Deco;
		if (!int.TryParse(iBridUtil.GetStringValue(Request.QueryString["svrtype"]),out iMailServerType)) {
			iMailServerType = ViCommConst.MailServerType.Deco;
		}
		string sMailReturnTo = iBridUtil.GetStringValue(Request.QueryString["returnto"]);
		bool bIsGetSizeQuery = iBridUtil.GetStringValue(Request.QueryString["getsize"]).Equals(ViCommConst.FLAG_ON_STR);

		// ==================================
		//  メール送信処理		
		// ==================================
		if (iStatus == 0) {
			if (bIsGetSizeQuery || ApplicationObj.Current.ThreadCount < 1) {
				// 同期送信
				iStatus = SendMail(sReqSeq,iMailSendType,iMailServerType,sMailReturnTo,bIsGetSizeQuery);
			} else {
				// 非同期送信
				SendMailAsync(sReqSeq,iMailSendType,iMailServerType,sMailReturnTo);
			}
		}

		// ==================================
		//  OUT 生成 	
		// ==================================
		Response.ContentType = "text/html";
		Response.Write(string.Format("result={0}",iStatus));
		Response.End();
	}

	#region □■□ メール送信処理(非同期) □■□ ==============================================================================
	/// <summary>
	/// 非同期にメール送信処理を行う
	/// </summary>
	/// <param name="pReqSeq">メールリクエスト№</param>
	private void SendMailAsync(string pReqSeq,int pMailSendType,int pMailServerType,string pMailReturnTo) {
		// 要求をキューに入れる 
		lock (_syncRoot) {
			reqSeqQueue.Enqueue(new MailQueueItem(pReqSeq,pMailSendType,pMailServerType,pMailReturnTo));
		}
		// ワーカースレッド起動 
		RunSendMailThread();
	}

	/// <summary>
	/// メール送信処理用のワーカースレッドを起動する 
	/// </summary>
	private void RunSendMailThread() {
		new Thread(new ThreadStart(SendMailThreadEntry)).Start();
	}
	/// <summary>
	/// メール送信処理用用のワーカースレッドのエントリﾎﾟｲﾝﾄとなるメソッド 
	/// </summary>
	private void SendMailThreadEntry() {
		try {
			// セマフォが取得できなければ処理しない 
			if (!_semaphore.WaitOne(0))
				return;

			try {

				// ダブルチェックロッキング・イディオムで要求残数をチェック。  
				MailQueueItem oQueueItem = null;
				if (reqSeqQueue.Count > 0) {
					lock (_syncRoot) {
						if (reqSeqQueue.Count > 0) {
							oQueueItem = reqSeqQueue.Dequeue();
						}
					}
				}
				if (oQueueItem == null)
					return;

				// メール送信処理 
				SendMail(oQueueItem.ReqNo,oQueueItem.MailSendType,oQueueItem.MailServerType,oQueueItem.MailReturnTo,false);
			} finally {
				_semaphore.Release();
			}

			// 処理中に要求が発生していた場合はワーカースレッド再起動。 
			if (reqSeqQueue.Count > 0) {
				lock (_syncRoot) {
					for (int i = 0;i < reqSeqQueue.Count;i++) {
						// スレッドを起こしすぎないように念のためトラップ
						if (i > ApplicationObj.Current.ThreadCount)
							break;
						RunSendMailThread();
					}
				}
			}
		} catch (Exception ex) {
			// threadのエントリとなるメソッドなので、例外情報がロストしないようここでロギング
			EventLogWriter.Error(ex);
		}
	}
	#endregion ========================================================================================================


	#region □■□ メール送信処理(同期) □■□ =============================================================================

	/// <summary>
	/// メール送信処理を行う
	/// </summary>
	/// <param name="pReqSeq">メールリクエスト№</param>
	/// <param name="pSendMailType">送信するメールの種別（デコメ/テキスト)</param>
	/// <param name="pMailServerType">送信に利用するメールサーバーの種別(AccelMail/RelayServer)</param>
	/// <param name="pMailReturnTo">返信先として利用するアドレス</param>
	/// <param name="bIsTestMail">テスト送信かどうかを示す値。</param>
	/// <returns>処理ステータス(0:正常終了、-1:メールリクエスト№不正</returns>
	private int SendMail(string pReqSeq,int pSendMailType,int pMailServerType,string pMailReturnTo,bool pIsGetSizeQuery) {

		DateTime[] oExecuteDtLogArray = new DateTime[4];	// 実行時間のを保持する配列(for ログ出力)
		bool bLoggingEnabled = false;						// ログ出力を有効化するかどうかを示す値
		string sProfileContentId = string.Empty;

		oExecuteDtLogArray[0] = DateTime.Now;

		int iCount = 0;
		long lDataLength = 0;

		using (ReqTxMail oReqTxMail = new ReqTxMail()) {
			// メール情報取得 
			if (!oReqTxMail.GetOne(pReqSeq,pSendMailType)) {
				return -1;
			}
			sProfileContentId = string.Format("prof@{0}{1}",DateTime.Now.ToString("yyyyMMddHHmmssfff"),oReqTxMail.mailHost);

			if (pSendMailType == ViCommConst.MailSendType.NONE) {
				pSendMailType = (oReqTxMail.textMailFlag.Equals(ViCommConst.FLAG_ON_STR))
										? ViCommConst.MailSendType.TEXT
										: ViCommConst.MailSendType.DECO_MAIL;
			}

			string url = oReqTxMail.url;
			using (MailBodyDomainSettings oMailBodyDomainSettings = new MailBodyDomainSettings()) {
				string sReplaceDomain = oMailBodyDomainSettings.GetReplaceDomain(oReqTxMail.txSiteCd);
				if (!sReplaceDomain.Equals(string.Empty)) {
					url = "http://" + sReplaceDomain;
					if (!ViCommConst.MAN.Equals(oReqTxMail.rxSexCd)) {
						url = url + "/wo";
					}
				}
			}

			string sColorStartTag = string.Empty;
			string sColorEndTag = "</font></td></tr></table>";
			using (WebFace oWebFace = new WebFace()) {
				if (oWebFace.GetOne(oReqTxMail.webFaceSeq)) {
					sColorStartTag = string.Format("<table bgcolor=\"{0}\"><tr><td><font color=\"{1}\" size=\"{2}\">",oWebFace.colorBack,oWebFace.colorChar,oWebFace.sizeChar);
				}
			}

			using (Site oSite = new Site()) {
				if (oSite.GetOne(oReqTxMail.txSiteCd)) {
					if (oSite.mailDocNotUseWebFaceFlag == ViCommConst.FLAG_ON) {
						if (!oReqTxMail.mobilMailBackColor.Equals(string.Empty)) {
							sColorStartTag = string.Format("<table bgcolor=\"{0}\"><tr><td>",oReqTxMail.mobilMailBackColor);
							sColorEndTag = "</td></tr></table>";
						} else {
							sColorStartTag = string.Empty;
							sColorEndTag = string.Empty;
						}
					}
				}
			}

			string sDecoMailDoc = oReqTxMail.decoMailDoc;
			sDecoMailDoc = sDecoMailDoc.Replace("<hide>",string.Empty);
			sDecoMailDoc = sDecoMailDoc.Replace("</hide>",string.Empty);

			bool bTxMail = true;
			bLoggingEnabled = false;

			Dictionary<string,List<string>> oMailToAddrRepos = new Dictionary<string,List<string>>();

			List<DataRow> oReqTxMailDtlDataRowList = new List<DataRow>();
			MailBodyHelper.MailDataFormatter oMailDataFormatter = new MailBodyHelper.MailDataFormatter(url);


			using (MailLog oMailLog = new MailLog())
			using (ReqTxMailDtl oReqTxMailDtl = new ReqTxMailDtl()) {

				DataSet oReqTxMailDtlDataSet = oReqTxMailDtl.GetList(pReqSeq);
				bLoggingEnabled = true;
				iCount = oReqTxMailDtlDataSet.Tables[0].Rows.Count;

				string sUserImageFullPath = string.Empty;
				foreach (DataRow oReqTxMailDtlDataRow in oReqTxMailDtlDataSet.Tables[0].Rows) {
					if (oReqTxMailDtlDataRow["NON_EXIST_MAIL_ADDR_FLAG"].ToString().Equals("0")) {
						bTxMail = CheckMobileMailRx(oReqTxMail.dr,oReqTxMailDtlDataRow);

						switch (oReqTxMail.mailType) {
							case ViCommConst.MAIL_TP_APPROACH_TO_CAST:
							case ViCommConst.MAIL_TP_APPROACH_ADMIN_TO_CAST:
							case ViCommConst.MAIL_TP_RETURN_TO_CAST:
							case ViCommConst.MAIL_TP_CAST_PAYOUT:
							case ViCommConst.MAIL_TP_CAST_MAIL_MAGAZINE:
							case ViCommConst.MAIL_TP_CAST_MODIFY_COMPLITED:
							case ViCommConst.MAIL_TP_BUY_POINT_NOFITY:
								if ((oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_WOMAN_STOP)) ||
								   (oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_WOMAN_RESIGNED)) ||
								   (oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_WOMAN_BAN))) {
									bTxMail = false;
								}
								break;

							case ViCommConst.MAIL_TP_APPROACH_TO_USER:
							case ViCommConst.MAIL_TP_RETURN_TO_USER:
							case ViCommConst.MAIL_TP_APPROACH_ADMIN_TO_USER:
							case ViCommConst.MAIL_TP_INFO:
							case ViCommConst.MAIL_TP_NOTIFY_LOGIN:
							case ViCommConst.MAIL_TP_INVITE_TALK:
							case ViCommConst.MAIL_TP_DM:
							case ViCommConst.MAIL_TP_NOTIFY_LOGOUT:
							case ViCommConst.MAIL_TP_MODIFY_COMPLITED:
								if ((oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_RESIGNED)) ||
								   (oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_STOP)) ||
								   (oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_BLACK)) ||
								   (oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_HOLD))) {
									bTxMail = false;
								}
								break;

							case ViCommConst.MAIL_TP_ADMIN_TO_RESIGNED:
								if ((oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_STOP)) ||
								   (oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_BLACK)) ||
								   (oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_HOLD))) {
									bTxMail = false;
								}
								break;

							case ViCommConst.MAIL_TP_URGE:
								break;
							case ViCommConst.MAIL_TP_BLOG_POST_NOFITY:
								string sBlogArticleTitle = iBridUtil.GetStringValue(oReqTxMailDtlDataRow["GENERAL5"]);
								oReqTxMailDtlDataRow["GENERAL5"] = SysPrograms.Substring(sBlogArticleTitle,80);
								break;

							default:
								break;
						}
					} else if (oReqTxMailDtlDataRow["NON_EXIST_MAIL_ADDR_FLAG"].ToString().Equals("1")) {
						switch (oReqTxMail.mailType) {
							case ViCommConst.MAIL_TP_CLEANING_ADDR:
								if (oReqTxMailDtlDataRow["RX_SEX_CD"].ToString().Equals(ViCommConst.MAN)) {
									if (
									   oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_RESIGNED) ||
									   oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_STOP) ||
									   oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_BLACK) ||
									   oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_HOLD)
									) {
										bTxMail = false;
									}
								} else {
									if (
									   oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_WOMAN_STOP) ||
									   oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_WOMAN_RESIGNED) ||
									   oReqTxMailDtlDataRow["USER_STATUS"].ToString().Equals(ViCommConst.USER_WOMAN_BAN)
									) {
										bTxMail = false;
									}
								}

								break;

							case ViCommConst.MAIL_TP_CAST_MODIFY_COMPLITED:
							case ViCommConst.MAIL_TP_MODIFY_COMPLITED:
							case ViCommConst.MAIL_TP_RELEASE_MAIL_ADDR:
							case ViCommConst.MAIL_TP_RELEASE_ADDR_CAST:
								break;

							default:
								bTxMail = false;
								break;
						}
					} else {
						bTxMail = false;
					}

					string sSubject = oMailDataFormatter.FormatVariable(oReqTxMail.mailTitle,oReqTxMailDtlDataRow,false);

					if (pIsGetSizeQuery || bTxMail) {
						if (ApplicationObj.Current.EnabledTxUserImg2Small || iBridUtil.GetStringValue(oReqTxMail.mailType).Equals(ViCommConst.MAIL_TP_BLOG_POST_NOFITY)) {
							if (ViCommConst.OPERATOR.Equals(iBridUtil.GetStringValue(oReqTxMailDtlDataRow["TX_SEX_CD"]))) {
								oReqTxMailDtlDataRow["TX_USER_IMG_FULL_PATH"] = iBridUtil.GetStringValue(oReqTxMailDtlDataRow["TX_USER_IMG_FULL_PATH"]).Replace(".gif","_s.gif");
							}
							oReqTxMailDtlDataRow["TX_USER_IMG_FULL_PATH"] = iBridUtil.GetStringValue(oReqTxMailDtlDataRow["TX_USER_IMG_FULL_PATH"]).Replace(".jpg","_s.jpg");
							oReqTxMailDtlDataRow["TX_IMG_PATH"] = iBridUtil.GetStringValue(oReqTxMailDtlDataRow["TX_IMG_PATH"]).Replace(".jpg","_s.jpg");
						}

						if (!string.IsNullOrEmpty(oReqTxMailDtlDataRow["TX_USER_IMG_FULL_PATH"].ToString())) {
							// 送信者の画像パスを読み込み
							sUserImageFullPath = oReqTxMailDtlDataRow["TX_USER_IMG_FULL_PATH"].ToString();
						}
						// 送信OKの時のみ送信先を追加 
						if (pMailServerType == ViCommConst.MailServerType.Deco || pSendMailType == ViCommConst.MailSendType.DECO_MAIL) {
							string sCarrierCd = string.Empty;
							string sMailToAddr = this.CreateToAddress(oReqTxMailDtlDataRow,url,sSubject,sProfileContentId,out sCarrierCd);
							string sSvrNumber = iBridUtil.GetStringValue(oReqTxMailDtlDataRow["SMTP_SERVER_NO"]);

							if (!sCarrierCd.Equals(ViCommConst.DOCOMO) && !sCarrierCd.Equals(ViCommConst.KDDI) && !sCarrierCd.Equals(ViCommConst.SOFTBANK)) {
								sCarrierCd = ViCommConst.CARRIER_OTHERS;
							}
							string sReposKey = string.Format("{0},{1}",sSvrNumber,sCarrierCd);
							if (!oMailToAddrRepos.ContainsKey(sReposKey)) {
								oMailToAddrRepos.Add(sReposKey,new List<string>());
							}
							oMailToAddrRepos[sReposKey].Add(sMailToAddr);
						} else {
							oReqTxMailDtlDataRowList.Add(oReqTxMailDtlDataRow);
						}
					} else {
						oReqTxMailDtl.RefuseTxMail(pReqSeq,oReqTxMailDtlDataRow["REQUEST_TX_MAIL_SUBSEQ"].ToString());
					}

					// =======================================
					//  メールログの更新 
					// =======================================

					string sMailSeq = iBridUtil.GetStringValue(oReqTxMailDtlDataRow["MAIL_SEQ"]);
					if (!sMailSeq.Equals(string.Empty)) {
						string sMailBoxDoc = oMailDataFormatter.FormatVariable(oReqTxMail.webMailDoc,oReqTxMailDtlDataRow,sSubject,false);
						if (!string.IsNullOrEmpty(sMailBoxDoc)) {
							sMailBoxDoc = _hideRegex.Replace(sMailBoxDoc,string.Empty);
							sMailBoxDoc = sColorStartTag + sMailBoxDoc + sColorEndTag;
						}
						oMailLog.UpdateMailDoc(sMailSeq,sSubject,sMailBoxDoc,0,oReqTxMail.textMailFlag);
					}

					// サイズ取得が目的の場合は1件だけ処理する 
					if (pIsGetSizeQuery) {
						break;
					}
				}

				if (oReqTxMail.batchMailFlag == 1) {
					using (DbSession db = new DbSession()) {
						db.PrepareProcedure("UPDATE_BATCH_MAIL_COUNT");
						db.ProcedureInParm("PREQUEST_TX_MAIL_SEQ",DbSession.DbType.VARCHAR2,pReqSeq);
						db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
						db.ExecuteProcedure();
					}
				}
				// =======================================
				//  送信 
				// =======================================

				if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnabledLimitMailTitle"]).Equals(ViCommConst.FLAG_ON_STR)) {
					switch (oReqTxMail.mailType) {
						case ViCommConst.MAIL_TP_APPROACH_TO_CAST:
						case ViCommConst.MAIL_TP_APPROACH_ADMIN_TO_CAST:
						case ViCommConst.MAIL_TP_RETURN_TO_CAST:
						case ViCommConst.MAIL_TP_INVITE_TALK:
						case ViCommConst.MAIL_TP_DM:
						case ViCommConst.MAIL_TP_APPROACH_TO_USER:
						case ViCommConst.MAIL_TP_RETURN_TO_USER:
						case ViCommConst.MAIL_TP_APPROACH_ADMIN_TO_USER:
							oReqTxMail.mailTitle = SysPrograms.Substring(oReqTxMail.mailTitle,30);
							oReqTxMail.mailTitle = SubSubject(oReqTxMail.mailTitle,DefaultToAddressEnc);
							break;
					}
				}


				// ループが終わってから一括でメール送信する。有効な送信先が存在する場合だけ実行する 
				if (pMailServerType == ViCommConst.MailServerType.Deco || pSendMailType == ViCommConst.MailSendType.DECO_MAIL) {
					foreach (string sReposKeyString in oMailToAddrRepos.Keys) {
						string[] sKeys = sReposKeyString.Split(',');
						string sSvrNumber = sKeys[0];
						string sCarrierCd = sKeys[1];

						int iSvrNumber = AccelMailSmtpClient.DEFAULT_SVR_NUMBER;
						if (!int.TryParse(sSvrNumber,out iSvrNumber)) {
							iSvrNumber = AccelMailSmtpClient.DEFAULT_SVR_NUMBER;
						}

						List<string> oMailToAddrList = oMailToAddrRepos[sReposKeyString] ?? new List<string>();

						if (oMailToAddrList.Count > 0) {
							lDataLength = PostMT(iSvrNumber,true,bLoggingEnabled,oExecuteDtLogArray,oReqTxMail,sDecoMailDoc,sCarrierCd,oMailToAddrList,sUserImageFullPath,pSendMailType,pMailReturnTo,pIsGetSizeQuery,sProfileContentId);
							if (pIsGetSizeQuery)
								break;
						}
					}
				} else if (pMailServerType == ViCommConst.MailServerType.Relay) {
					if (oReqTxMailDtlDataRowList.Count > 0) {
						lDataLength = PostRelayServer(true,bLoggingEnabled,oExecuteDtLogArray,oReqTxMail,sDecoMailDoc,oReqTxMailDtlDataRowList,oMailDataFormatter,pMailReturnTo);
					}
				} else {
					throw new NotSupportedException();
				}


			}

			if (pIsGetSizeQuery) {
				using (MailTemplate oMailTemplate = new MailTemplate()) {
					oMailTemplate.ModifyMailTemplateSize(oReqTxMail.txSiteCd,oReqTxMail.mailTemplateNo,lDataLength);
				}
			}
		}

		return 0;
	}

	#endregion ========================================================================================================

	public string CreateToAddress(DataRow pDr,string pUrl,string pSubject,string pProfileContentId,out string pCarrierCd) {
		string mailAddress = pDr["RX_EMAIL_ADDR"].ToString();
		pCarrierCd = EMailAddrUtil.GetCarrierCd(mailAddress);
		string sCarrierCd = pCarrierCd;
		// アクセルメール用の絵文字の変換を行います。 
		if (sCarrierCd.Equals(Mobile.SOFTBANK)) {
			sCarrierCd = Mobile.SOFTBANK_MAIL;
		}
		// ハンドル名に含まれる絵文字は削除します。 
		string userHandleNm = pDr["RX_HANDLE_NM"].ToString();
		string castHandleNm = pDr["TX_HANDLE_NM"].ToString();
		string MobileMailGeneral1 = string.Empty;
		string MobileMailGeneral2 = string.Empty;
		string MobileMailGeneral3 = string.Empty;
		if (!string.IsNullOrEmpty(pDr["MOBILE_MAIL_GENERAL1"].ToString())) {
			MobileMailGeneral1 = pUrl + pDr["MOBILE_MAIL_GENERAL1"].ToString();
		}
		if (!string.IsNullOrEmpty(pDr["MOBILE_MAIL_GENERAL2"].ToString())) {
			MobileMailGeneral2 = pUrl + pDr["MOBILE_MAIL_GENERAL2"].ToString();
		}
		if (!string.IsNullOrEmpty(pDr["MOBILE_MAIL_GENERAL3"].ToString())) {
			MobileMailGeneral3 = pUrl + pDr["MOBILE_MAIL_GENERAL3"].ToString();
		}
		ParseMobile oParseMobile = new ParseMobile(@"\$x\w{1,};",false,string.Empty,string.Empty,string.Empty,RegexOptions.IgnoreCase,sCarrierCd);
		ParseHTML oParser = new ParseHTML(oParseMobile);

		string toAddress = string.Format("{0}?1={1}&2={2}&3={3}&4={4}&5={5}&6={6}&7={7}&8={8}&9={9}&10={10}&11={11}&12={12}&13={13}&14={14}&15={15}&16={16}&17={17}&18={18}&19={19}&20={20}&21={21}&22={22}&23={23}&24={24}&25={25}&26={26}",
			mailAddress,
			HttpUtility.UrlEncode(pDr["RX_EMAIL_ADDR"].ToString(),DefaultToAddressEnc),
			HttpUtility.UrlEncode(pDr["RX_LOGIN_ID"].ToString(),DefaultToAddressEnc),
			HttpUtility.UrlEncode(pDr["RX_LOGIN_PASSWORD"].ToString(),DefaultToAddressEnc),
			HttpUtility.UrlEncode(oParser.Parse(Mobile.ConvertSpecialChar(userHandleNm)),DefaultToAddressEnc),
			HttpUtility.UrlEncode(oParser.Parse(Mobile.ConvertSpecialChar(castHandleNm)),DefaultToAddressEnc),
			HttpUtility.UrlEncode(pDr["TX_LOGIN_ID"].ToString(),DefaultToAddressEnc),
			HttpUtility.UrlEncode(string.Format("cid:{0}",pProfileContentId),DefaultToAddressEnc),
			HttpUtility.UrlEncode(oParser.Parse(Mobile.ConvertSpecialChar(pDr["TX_ATTR1"].ToString())),DefaultToAddressEnc),
			HttpUtility.UrlEncode(oParser.Parse(Mobile.ConvertSpecialChar(pDr["TX_ATTR2"].ToString())),DefaultToAddressEnc),
			HttpUtility.UrlEncode(oParser.Parse(Mobile.ConvertSpecialChar(pDr["GENERAL1"].ToString())),DefaultToAddressEnc),
			HttpUtility.UrlEncode(oParser.Parse(Mobile.ConvertSpecialChar(pDr["GENERAL2"].ToString())),DefaultToAddressEnc),
			HttpUtility.UrlEncode(pUrl,DefaultToAddressEnc),
			HttpUtility.UrlEncode(oParser.Parse(Mobile.ConvertSpecialChar(MobileMailGeneral1)),DefaultToAddressEnc),
			HttpUtility.UrlEncode(pDr["CHAR_NO_ITEM_NM"].ToString(),DefaultToAddressEnc),
			HttpUtility.UrlEncode(pDr["CRYPT_VALUE"].ToString(),DefaultToAddressEnc),
			HttpUtility.UrlEncode(oParser.Parse(Mobile.ConvertSpecialChar(MobileMailGeneral2)),DefaultToAddressEnc),
			HttpUtility.UrlEncode(oParser.Parse(Mobile.ConvertSpecialChar(MobileMailGeneral3)),DefaultToAddressEnc),
			HttpUtility.UrlEncode(oParser.Parse(Mobile.ConvertSpecialChar(pDr["GENERAL3"].ToString())),DefaultToAddressEnc),
			HttpUtility.UrlEncode(oParser.Parse(Mobile.ConvertSpecialChar(pDr["GENERAL4"].ToString())),DefaultToAddressEnc),
			HttpUtility.UrlEncode(oParser.Parse(Mobile.ConvertSpecialChar(pDr["GENERAL5"].ToString())),DefaultToAddressEnc),
			HttpUtility.UrlEncode(pDr["TX_AGE"].ToString(),DefaultToAddressEnc),
			HttpUtility.UrlEncode(oParser.Parse(MailBodyHelper.GetBuyPointUserList(pDr["REQUEST_TX_MAIL_SEQ"].ToString(),pDr["REQUEST_TX_MAIL_SUBSEQ"].ToString())),DefaultToAddressEnc),
			HttpUtility.UrlEncode(oParser.Parse(Mobile.ConvertSpecialChar(pSubject)),DefaultToAddressEnc),
			HttpUtility.UrlEncode(pDr["MAIL_SEQ"].ToString(),DefaultToAddressEnc),
			HttpUtility.UrlEncode(pDr["OBJ_SEQ"].ToString(),DefaultToAddressEnc),
			HttpUtility.UrlEncode(pDr["REQUEST_TX_MAIL_SEQ"].ToString() + "-" + pDr["REQUEST_TX_MAIL_SUBSEQ"].ToString(),DefaultToAddressEnc));

		return toAddress;
	}

	/// <summary>
	/// Relayサーバーを利用したテキストメールの送信処理 
	/// </summary>
	public long PostRelayServer(bool pInit,bool pLoggingEnabled,DateTime[] procDate,ReqTxMail pReqTxMail,string pMailDoc,List<DataRow> pReqTxMailDtlDataRowList,MailBodyHelper.MailDataFormatter pFormatter,string pReturnTo) {
		procDate[1] = DateTime.Now;
		ViCommInterface.WriteIFLog("MailDispatch","Preparation PostRelayServer");


		if (!pReqTxMail.registTrackingUrl.Equals(string.Empty)) {
			string[] sTrackingInfo = pReqTxMail.registTrackingUrl.Split(',');
			string sTrackingUrl = string.Empty;

			//互換性を持たせるために過去の5を残しておく
			if (sTrackingInfo.Length == 5) {
				sTrackingUrl = string.Format(sTrackingInfo[1],sTrackingInfo[2],sTrackingInfo[3]);
			} else if (sTrackingInfo.Length == 7) {
				sTrackingUrl = string.Format(sTrackingInfo[1],sTrackingInfo[2],sTrackingInfo[3],sTrackingInfo[4],sTrackingInfo[5]);
			}
			if (!sTrackingUrl.Equals(string.Empty)) {
				ViCommInterface.TransToParent(sTrackingUrl,true);
			}
		}


		// ===========================
		// 送信処理 
		// ===========================
		string sFromUserNm = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["FromUserNm"]);
		if (sFromUserNm.Equals(string.Empty)) {
			sFromUserNm = "info";
		}
		string sFrom = string.IsNullOrEmpty(pReqTxMail.fromEmailAddr)
							? string.Format("{0}@{1}",sFromUserNm,pReqTxMail.mailHost)
							: pReqTxMail.fromEmailAddr;
		string sReturnTo = GetReturnToAddr(pReturnTo,pReqTxMail.mailHost);

		long lDataLength = 0;
		try {
			procDate[2] = DateTime.Now;
			RelaySmtpClient oClient = new RelaySmtpClient();
			lDataLength = oClient.SendMail(sFrom,sReturnTo,pReqTxMail.mailTitle,pMailDoc,pReqTxMail.textMailDoc,pReqTxMailDtlDataRowList,pFormatter);

			procDate[3] = DateTime.Now;
		} catch (Exception e) {
			ViCommInterface.WriteIFError(e,"MailInterface",sFrom);
		}
		return lDataLength;
	}

	/// <summary>
	/// AccelMailサーバーを利用したテキストメールの送信処理 
	/// </summary>
	public long PostMT(int pSvrNumber,bool pInit,bool pLoggingEnabled,DateTime[] procDate,ReqTxMail pReqTxMail,string pMailDoc,string sCarrierCd,List<string> pToAddress,string pUserPhotoImgUrl,int pMailSendType,string pReturnTo,bool pSizeOnly,string pProfileContentId) {
		procDate[1] = DateTime.Now;
		/*
				ViCommInterface.WriteIFLog("MailDispatch","Preparation PostMT");
		*/
		string sMailTilte = string.Empty;
		string sTextMailDoc = string.Empty;

		if (!pReqTxMail.registTrackingUrl.Equals(string.Empty)) {
			string[] sTrackingInfo = pReqTxMail.registTrackingUrl.Split(',');
			string sTrackingUrl = string.Empty;

			//互換性を持たせるために過去の5を残しておく
			if (sTrackingInfo.Length == 5) {
				sTrackingUrl = string.Format(sTrackingInfo[1],sTrackingInfo[2],sTrackingInfo[3]);
			} else if (sTrackingInfo.Length == 7) {
				sTrackingUrl = string.Format(sTrackingInfo[1],sTrackingInfo[2],sTrackingInfo[3],sTrackingInfo[4],sTrackingInfo[5]);
			}
			if (!sTrackingUrl.Equals(string.Empty)) {
				ViCommInterface.TransToParent(sTrackingUrl,true);
			}
		}

		if (pInit) {
			using (Site oSite = new Site()) {
				if (oSite.GetOne(pReqTxMail.txSiteCd)) {
					if (oSite.mailDocNotUseWebFaceFlag == ViCommConst.FLAG_OFF) {
						using (WebFace oWebFace = new WebFace()) {
							if (oWebFace.GetOne(pReqTxMail.webFaceSeq)) {
								pMailDoc = string.Format("<body bgcolor=\"{0}\" link=\"{1}\" vlink=\"{2}\"><font color=\"{3}\">",oWebFace.colorBack,oWebFace.colorLink,oWebFace.colorVLink,oWebFace.colorChar) + pMailDoc + "</font></body>";
							}
						}
					} else {
						string sBodyStart = string.Empty;
						string sBodyEnd = string.Empty;
						string sFontStart = string.Empty;
						string sFontEnd = string.Empty;

						if (!pReqTxMail.mobilMailBackColor.Equals("")) {
							sBodyStart = string.Format("<body bgcolor=\"{0}\">",pReqTxMail.mobilMailBackColor);
							sBodyEnd = "</body>";
						}
						if (pReqTxMail.mobilMailFontSize != 0) {
							sFontStart = string.Format("<font size=\"{0}\">",pReqTxMail.mobilMailFontSize);
							sFontEnd = "</font>";
						}
						pMailDoc = sBodyStart + sFontStart + pMailDoc + sFontEnd + sBodyEnd;
					}
				}
			}


			// アクセルメール用の絵文字の変換を行います。 
			if (sCarrierCd.Equals(Mobile.SOFTBANK)) {
				sCarrierCd = Mobile.SOFTBANK_MAIL;
			}
			ParseMobile oParseMobile = new ParseMobile(@"\$x\w{1,};",false,string.Empty,string.Empty,string.Empty,RegexOptions.IgnoreCase,sCarrierCd);

			ParseHTML oParser = new ParseHTML(oParseMobile);
			sMailTilte = HttpUtility.HtmlDecode(oParser.Parse(Mobile.ConvertSpecialChar(pReqTxMail.mailTitle)));
			pMailDoc = HttpUtility.HtmlDecode(oParser.Parse(Mobile.ConvertSpecialChar(pMailDoc)));
			sTextMailDoc = HttpUtility.HtmlDecode(oParser.Parse(Mobile.ConvertSpecialChar(pReqTxMail.textMailDoc)));

			// SMTPの1行1000バイトの制限に引っかからないように改行を追加します。 
			pMailDoc = pMailDoc.Replace("<BR>","<BR>\r\n");
			pMailDoc = pMailDoc.Replace("<br>","<br>\r\n");

		}


		// ===========================
		//  画像のコンテンツ化
		// ===========================
		MailImageHelper oMailImageHelper = new MailImageHelper();

		// コンテンツIDとBase64文字列化した画像データのリスト 
		Dictionary<string,string> oBase64ImageDict = new Dictionary<string,string>();
		// コンテンツIDとファイル拡張子のリスト 
		Dictionary<string,string> oExtensionDict = new Dictionary<string,string>();
		//
		// 画像のコンテンツ化 
		//
		pMailDoc = oMailImageHelper.SearchImageFilePath(pMailDoc,pReqTxMail.url,pReqTxMail.mailHost,oBase64ImageDict,oExtensionDict);
		//
		// キャスト画像のコンテンツ化 
		//
		if (!string.IsNullOrEmpty(pUserPhotoImgUrl) && Regex.IsMatch(pMailDoc,"%%7%%")) {
			try {
				oExtensionDict.Add(pProfileContentId,oMailImageHelper.GetFileExtension(pUserPhotoImgUrl));
				oBase64ImageDict.Add(pProfileContentId,oMailImageHelper.ConvertImage2Base64String(pUserPhotoImgUrl,pReqTxMail.url));
			} catch (Exception) {
				// キャスト画像の読み込みできない場合でも処理を続行 
			}
		}


		// ===========================
		// 送信処理 
		// ===========================
		string sFromUserNm = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["FromUserNm"]);
		if (sFromUserNm.Equals(string.Empty)) {
			sFromUserNm = "info";
		}
		string sFrom = string.IsNullOrEmpty(pReqTxMail.fromEmailAddr)
							? string.Format("{0}@{1}",sFromUserNm,pReqTxMail.mailHost)
							: pReqTxMail.fromEmailAddr;

		string sReturnTo = GetReturnToAddr(pReturnTo,pReqTxMail.mailHost);
		long lDataLength = 0;
		try {
			EventLogWriter.Debug("SVR_NO:{0},CARRIER:{1},COUNT:{2}",pSvrNumber,sCarrierCd,pToAddress.Count);

			procDate[2] = DateTime.Now;
			AccelMailSmtpClient client = new AccelMailSmtpClient(pSizeOnly);
			lDataLength = client.SendAccelMail(pSvrNumber,sFrom,pToAddress,sReturnTo,sMailTilte,pMailDoc,sTextMailDoc,oBase64ImageDict,oExtensionDict,pMailSendType);
			procDate[3] = DateTime.Now;

		} catch (Exception e) {
			ViCommInterface.WriteIFError(e,"MailInterface",sFrom);
		}
		return lDataLength;
	}

	private string GetReturnToAddr(string pReturnTo,string pDomain) {
		if (string.IsNullOrEmpty(pReturnTo)) {
			pReturnTo = "returnmail";
		}
		return string.Format("{0}@{1}",pReturnTo,pDomain);
	}


	private string GetBuyPointUserList(string pReqSeq,string pReqSubSeq) {
		StringBuilder sValue = new StringBuilder();

		using (ReqTxMailDtlEx oDtlEx = new ReqTxMailDtlEx()) {
			DataSet ds = oDtlEx.GetList(pReqSeq,pReqSubSeq);
			foreach (DataRow oRow in ds.Tables[0].Rows) {
				sValue.Append(oRow["HANDLE_NM"].ToString()).AppendLine();
			}
		}
		return sValue.ToString();
	}

	private string SubSubject(string pSubject,Encoding pEnc) {

		string sValue = string.Empty;
		if (pEnc.GetBytes(pSubject).Length > 30) {
			//subject = enc.GetString(pEnc.GetBytes(subject), 0, 30);

			byte[] oSubjectBytes = pEnc.GetBytes(pSubject);
			int iMaxCount = pEnc.GetCharCount(oSubjectBytes);
			int iLengthCount = 0;
			for (int i = 0;i < iMaxCount;i++) {
				char[] cOneChar = pEnc.GetChars(oSubjectBytes,iLengthCount,1);
				int iCharByteCount = pEnc.GetByteCount(cOneChar);
				if (iLengthCount + iCharByteCount > 30) {
					break;
				} else {
					iLengthCount += iCharByteCount;
				}
			}

			sValue = pEnc.GetString(oSubjectBytes,0,iLengthCount);
		} else {
			sValue = pSubject;
		}

		return sValue;
	}
}


