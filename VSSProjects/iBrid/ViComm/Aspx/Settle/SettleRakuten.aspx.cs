﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Settle
--	Title			: [楽天あんしん決済]注文通知受取 I/F
--	Progaram ID		: SettleRakuten
--
--  Creation Date	: 2010.08.25
--  Creater			: K.Itoh
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Xml.XPath;
using System.Text;

using ViComm;
using ViComm.Rakuten;

public partial class SettleRakuten : System.Web.UI.Page {
	public enum Result:int{
		OK = 0,ERROR = 1
	}
	
	protected void Page_Load(object sender, EventArgs e) {
		if(!this.IsPostBack){
			Result oResult = Result.OK;

			try{
				string sConfirmId = this.Request.Params["confirmId"];
				string sOrderCompleteRequestXml = Encoding.UTF8.GetString(Convert.FromBase64String(sConfirmId));
				EventLogWriter.Debug("注文通知受信{0}{1}", Environment.NewLine, sOrderCompleteRequestXml);

				// ===========================
				//  XMLのパース
				// ===========================
				XmlDocument oXmlDoc = new XmlDocument();
				oXmlDoc.LoadXml(sOrderCompleteRequestXml);

				string sCartId = oXmlDoc.SelectNodes("//orderCompleteRequest/orderCartId")[0].InnerText.Trim();
				//string sOpenId = oXmlDoc.SelectNodes("//orderCompleteRequest/openId")[0].InnerText.Trim();
				string sItemId = oXmlDoc.SelectNodes("//orderCompleteRequest/items/item[1]/itemId")[0].InnerText.Trim();
				decimal dItemFee = decimal.Parse(oXmlDoc.SelectNodes("//orderCompleteRequest/items/item[1]/itemFee")[0].InnerText.Trim());

				// ===========================
				//  データ収集
				// ===========================
				string sSiteCd = null;
				string sSettleType = null;
				int iSalesAmt = int.MinValue;
				ViComm.Rakuten.RakutenUtil.Convert2PackKey(sItemId,out sSiteCd,out sSettleType,out iSalesAmt);

				using (Pack oPack = new Pack()){
				
					if(!oPack.GetOne(sSiteCd,sSettleType,iSalesAmt.ToString())){
						string sErrorMessage = string.Format("T_SITE_SETTLEから該当するデータが取得できませんでした。SITE_CD:{0},SETTLE_TYPE:{1},SALES_AMT:{2}",sSiteCd,sSettleType,iSalesAmt);
						throw new ApplicationException(sErrorMessage);
					}


					// ===========================
					//  決済完了処理(ポイント充填)
					// ===========================
					using (DbSession db = new DbSession()) {
						db.PrepareProcedure("LOG_SETTLE_RESULT");
						db.ProcedureInParm("PSID", DbSession.DbType.VARCHAR2, sCartId);
						db.ProcedureInParm("PUSER_SEQ", DbSession.DbType.VARCHAR2, string.Empty);	// 固定
						db.ProcedureInParm("PSETTLE_AMT", DbSession.DbType.NUMBER, dItemFee);
						db.ProcedureInParm("PSETTLE_POINT", DbSession.DbType.NUMBER, 0);
						db.ProcedureInParm("PRESPONSE", DbSession.DbType.VARCHAR2, "0");			// 固定
						db.ProcedureOutParm("PRESULT", DbSession.DbType.VARCHAR2);
						db.ProcedureOutParm("PSTATUS", DbSession.DbType.VARCHAR2);
						db.ExecuteProcedure();
						string sResult = db.GetStringValue("PRESULT");

						if (!sResult.Equals("0")) {
							throw new ApplicationException(string.Format("ERROR:LOG_SETTLE_RESULT PRESULT:{0}", sResult));
						}
					}
					
				}
				oResult = Result.OK;
			}catch(Exception ex){
				EventLogWriter.Error(ex);
				oResult = Result.ERROR;
			}
			
			StringBuilder oResponseBuilder = new StringBuilder();
			oResponseBuilder.AppendFormat("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			oResponseBuilder.AppendFormat("<orderCompleteResponse>");
			oResponseBuilder.AppendFormat("<result>{0}</result>",oResult);
			oResponseBuilder.AppendFormat("<completeTime>{0:yyyy-MM-dd HH:mm:ss}</completeTime>",DateTime.Now);
			oResponseBuilder.AppendFormat("</orderCompleteResponse>");
			
			this.Response.Clear();
			EventLogWriter.Debug("注文通知応答{0}{1}", Environment.NewLine, oResponseBuilder);
			this.Response.Write(oResponseBuilder);
			this.Response.End();
		}else{
			throw new NotSupportedException();
		}
	}
}
