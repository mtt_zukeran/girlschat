﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Settle
--	Title			: クレジット従量決済要求受付
--	Progaram ID		: CreditMeasuredRate
--
--  Creation Date	: 2010.02.28
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using iBridCommLib;
using ViComm;
using System.Data;
using System.Net;
using System.IO;

public partial class CreditMeasuredRate:System.Web.UI.Page {

	private string sSiteCd;
	private string sRevNo;
	private string sSettlecompany;

	protected void Page_Load(object sender,EventArgs e) {
		try {
			sSiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
			sRevNo = iBridUtil.GetStringValue(Request.QueryString["revno"]);
			sSettlecompany = iBridUtil.GetStringValue(Request.QueryString["settlecompany"]);

			string sSid = "";
			DataSet ds;
			using (CreditProcess oCreditProcess = new CreditProcess()) {
				ds = oCreditProcess.GetList(sRevNo);
			}
			string sContinueSettleUrl = "";
			string sCpIdNo = "";
			using (SiteSettle oSiteSettle = new SiteSettle()) {
				oSiteSettle.GetOne(sSiteCd,sSettlecompany,ViCommConst.SETTLE_CREDIT);
				sContinueSettleUrl = oSiteSettle.continueSettleUrl;
				sCpIdNo = oSiteSettle.cpIdNo;
			}
			DataRow dr;

			string sUserSeq;
			string sSettleAmt;
			string sSettlePoint;
			string sEmailAddr;
			string sUniqueId;
			string sTel;

			for (int i = 0;i < ds.Tables["T_CREDIT_PROCESS"].Rows.Count;i++) {
				dr = ds.Tables["T_CREDIT_PROCESS"].Rows[i];
				sUserSeq = dr["USER_SEQ"].ToString();
				sSettleAmt = dr["SETTLE_AMT"].ToString();
				sSettlePoint = dr["SETTLE_POINT"].ToString();
				sEmailAddr = dr["EMAIL_ADDR"].ToString();
				sUniqueId = dr["UNIQUE_ID"].ToString();
				sTel = dr["TEL"].ToString();

				using (SettleLog oLog = new SettleLog()) {
					oLog.LogSettleRequest(
						sSiteCd,
						sUserSeq,
						sSettlecompany,
						ViCommConst.SETTLE_CREDIT,
						ViCommConst.SETTLE_STAT_SETTLE_NOW,
						int.Parse(sSettleAmt),
						int.Parse(sSettlePoint),
						"",
						out sSid);
				}
				string sSettleUrl;
				switch (sSettlecompany) {
					case ViCommConst.SETTLE_CORP_ZERO:
						sSettleUrl = string.Format(sContinueSettleUrl,sCpIdNo,sEmailAddr,sUniqueId,sSid,sSettleAmt,sTel);
						break;
					default:
						sSettleUrl = string.Format(sContinueSettleUrl,sCpIdNo,sEmailAddr,sUniqueId,sSid,sSettleAmt);
						break;
				}

				if (TransQuick(sSettleUrl)) {
					using (SettleLog oLog = new SettleLog()) {
						oLog.LogSettleResult(sSid,sUserSeq,int.Parse(sSettleAmt),0,"0");
					}
				} else {
					using (SettleLog oLog = new SettleLog()) {
						oLog.LogSettleResult(sSid,sUserSeq,int.Parse(sSettleAmt),0,"9");
					}
				}
			}
			Response.ContentType = "text/html";
			Response.Write("0");
		} catch (Exception) {
			Response.ContentType = "text/html";
			Response.Write("-2");
		}
		Response.End();
	}

	public bool TransQuick(string pUrl) {
		try {
			WebRequest req = WebRequest.Create(pUrl);
			req.Timeout = 60000;
			WebResponse res = req.GetResponse();

			Stream st = res.GetResponseStream();
			using (StreamReader sr = new StreamReader(st)) {
				string sRes = sr.ReadToEnd();
				sr.Close();
				st.Close();
				switch (sSettlecompany) {
					case ViCommConst.SETTLE_CORP_ZERO:
						return sRes.Equals("Success_order");
					default:
						return sRes.Equals("Success_order");
				}
			}
		} catch (Exception e) {
			ViCommInterface.WriteIFError(e,"CreditMeasuredRate",pUrl);
			return false;
		}
	}
}
