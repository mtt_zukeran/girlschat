﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Settle
--	Title			: ASPﾎﾟｲﾝﾄﾊﾞｯｸ決済結果
--	Progaram ID		: SettlePointAffiliate
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using iBridCommLib;
using ViComm;
using System.Text.RegularExpressions;

public partial class SettlePointAffiliate:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		try {
			bool bOk = false;
			string sAspAdCd = string.Empty;
			string sLoginId = string.Empty;
			string sSettleSeq = string.Empty;
			string sPoint = string.Empty;
			string sAmt = string.Empty;
			string sUserSeq = string.Empty;
			string sSiteCd = string.Empty;
			string sSettleCompanyCd = string.Empty;
			int iPoint;
			int iExAmt = 0;

			string sTestHost = GetParam("testhost");
			string sTestSubHost = GetParam("testsubhost");
			string sRequestCompanyCd = GetParam("settlecompanycd");

			using (Site oSite = new Site()) {
				sSiteCd = oSite.GetSiteCdBySubHostNm(Request.Url.Host);
				if (sSiteCd.Equals(string.Empty)) {
					sSiteCd = oSite.GetSiteCdBySubHostNm(sTestSubHost);
				}
			}
			using (SiteSettle oSiteSettle = new SiteSettle()) {
				if (!sRequestCompanyCd.Equals(string.Empty)) {
					bOk = oSiteSettle.GetOne(sSiteCd,sRequestCompanyCd,ViCommConst.SETTLE_POINT_AFFILIATE);
				}
				if (bOk == false) {
					bOk = oSiteSettle.GetOneByHostNm(sSiteCd,Request.UserHostName,ViCommConst.SETTLE_POINT_AFFILIATE,string.Empty);
				}
				if (bOk == false) {
					bOk = oSiteSettle.GetOneByHostNm(sSiteCd,Request.UserHostName,ViCommConst.SETTLE_POINT_AFFILIATE,"2");
				}
				if (bOk == false) {
					bOk = oSiteSettle.GetOneByHostNm(sSiteCd,Request.UserHostName,ViCommConst.SETTLE_POINT_AFFILIATE,"3");
				}
				if (bOk == false && sTestHost.Equals(string.Empty) == false) {
					bOk = oSiteSettle.GetOneByHostNm(sSiteCd,sTestHost,ViCommConst.SETTLE_POINT_AFFILIATE,string.Empty);
				}

				if (bOk == true && oSiteSettle.notCreateSettleReqLogFlag.Equals(ViCommConst.FLAG_ON_STR)) {
					using (User oUser = new User()) {
						sUserSeq = oUser.ChangeLoginIdToUserSeq(GetValue(oSiteSettle.regexLoginId));

						if (oSiteSettle.settleCompanyCd.Equals(ViCommConst.SETTLE_CORP_ACT_SYSTEM)) {
							if (string.IsNullOrEmpty(sUserSeq)) {
								sUserSeq = oUser.ChangeBeforeSystemIdToUserSeq(GetValue(oSiteSettle.regexLoginId));
							}
						}
					}
					string sUniqueValue = string.Empty;
					if (oSiteSettle.settleCompanyCd.Equals(ViCommConst.SETTLE_CORP_SITE01) || oSiteSettle.settleCompanyCd.Equals(ViCommConst.SETTLE_CORP_ACT_SYSTEM)) {
						sUniqueValue = GetParam("t");
					}
					using (SettleLog oLog = new SettleLog()) {
						oLog.LogSettleRequest(
							sSiteCd,
							sUserSeq,
							oSiteSettle.settleCompanyCd,
							ViCommConst.SETTLE_POINT_AFFILIATE,
							ViCommConst.SETTLE_STAT_SETTLE_NOW,
							0,
							0,
							string.Empty,
							sUniqueValue,
							out sSettleSeq);
					}
				}
				sSettleCompanyCd = oSiteSettle.settleCompanyCd;

				if (bOk == false) {
					ViCommInterface.WriteIFLog("SettlePointAffiliate",string.Format("Not found Host {0}",Request.UserHostName));
				} else {
					sAspAdCd = GetValue(oSiteSettle.regexAspCd);
					sLoginId = GetValue(oSiteSettle.regexLoginId);
					//決済ﾛｸﾞをここで作成する場合、SIDは取得済 
					if (!oSiteSettle.notCreateSettleReqLogFlag.Equals(ViCommConst.FLAG_ON_STR)) {
						sSettleSeq = GetValue(oSiteSettle.regexSettleSeq);
					}
					sPoint = GetValue(oSiteSettle.regexPoint);
					sAmt = GetValue(oSiteSettle.regexAmt);
					using (SettleLog oLog = new SettleLog()) {
						bOk = oLog.GetOne(sSettleSeq);
						//決済ﾛｸﾞをここで作成する場合、USER_SEQは取得済 
						if (!oSiteSettle.notCreateSettleReqLogFlag.Equals(ViCommConst.FLAG_ON_STR)) {
							if (bOk) {
								bOk = oLog.loginId.Equals(sLoginId);
							}
						}
						if (bOk) {
							//決済ﾛｸﾞをここで作成する場合、USER_SEQは取得済 
							if (!oSiteSettle.notCreateSettleReqLogFlag.Equals(ViCommConst.FLAG_ON_STR)) {
								sUserSeq = oLog.userSeq;
							}

							if (sPoint.Equals(string.Empty)) {
								sPoint = oLog.affiliatePoint.ToString();
								if (sAmt.Equals(string.Empty)) {
									iExAmt = oLog.affiliateExAmt;
								} else {
									int.TryParse(sAmt,out iExAmt);
									if (sPoint.Equals("0")) {
										sPoint = (iExAmt / oLog.pointPrice).ToString();
									}
								}
							} else {
								if (sAmt.Equals(string.Empty)) {
									iExAmt = int.Parse(sPoint) * oLog.pointPrice;
								} else {
									int.TryParse(sAmt,out iExAmt);
								}
							}
						}
					}
				}
			}

			int.TryParse(sPoint,out iPoint);

			string sResult = "-1";

			if (bOk) {
				using (DbSession db = new DbSession()) {
					db.PrepareProcedure("LOG_SETTLE_RESULT");
					db.ProcedureInParm("PSID",DbSession.DbType.VARCHAR2,sSettleSeq);
					db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,sUserSeq);
					db.ProcedureInParm("PSETTLE_AMT",DbSession.DbType.NUMBER,iExAmt);
					db.ProcedureInParm("PSETTLE_POINT",DbSession.DbType.NUMBER,iPoint);
					db.ProcedureInParm("PRESPONSE",DbSession.DbType.VARCHAR2,"0");
					db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
					db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
					db.ExecuteProcedure();
					sResult = db.GetStringValue("PRESULT");
				}
			}
			Response.ContentType = "text/plain";
			Response.Charset = "shift_jis";
			if (sSettleCompanyCd.Equals(ViCommConst.SETTLE_CORP_SYMPHONY)) {
				if (sResult.Equals("0")) {
					Response.Write("success");
				} else {
					Response.Write("error");
				}
			} else {
				Response.Write(sResult);
			}

		} catch (Exception) {
			Response.ContentType = "text/plain";
			Response.Charset = "shift_jis";
			Response.Write("-1");
		}
		Response.End();
	}

	public string GetValue(string pRegex) {
		string sQuery = string.Empty;
		string sValue = string.Empty;

		if (Request.HttpMethod.Equals("POST")) {
			sQuery = Request.Form.ToString();
		} else {
			sQuery = Request.QueryString.ToString();
		}
		Regex rgx = new Regex(pRegex);
		Match rgxAMatch = rgx.Match(sQuery);
		if (rgxAMatch.Success) {
			sValue = rgxAMatch.Groups[1].Value;
		}
		return sValue;
	}

	private string GetParam(string sParam) {
		if (Request.HttpMethod.Equals("POST")) {
			return iBridUtil.GetStringValue(Request.Form[sParam]);
		} else {
			return iBridUtil.GetStringValue(Request.QueryString[sParam]);
		}
	}
}
