﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Settle
--	Title			: 商品動画購入確認
--	Progaram ID		: ProductMovie
--
--  Creation Date	: 2010.12.08
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using iBridCommLib;
using ViComm;

public partial class Product_AuthUser4AspMovie : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
		string sAuthId = iBridUtil.GetStringValue(Request.QueryString["auth_id"]);
        string sTitleSeq = iBridUtil.GetStringValue(Request.QueryString["titleSeq"]);

		bool bResult = false;
        string sSiteCd = string.Empty;
        string sProductSeq = string.Empty;
        string sUserSeq = string.Empty;

		sAuthId = Encoding.UTF8.GetString(Convert.FromBase64String(sAuthId));

		string[] oArray = sAuthId.Split(':');
		if (oArray.Length == 3)
        {
			sSiteCd = oArray[0];
			sProductSeq = oArray[1];
			sUserSeq = oArray[2];

			using (ObjUsedHistory objUsedHistory = new ObjUsedHistory()) {
				bResult = objUsedHistory.GetOne(sSiteCd, sUserSeq, ViCommConst.ATTACHED_PRODUCT.ToString(), sProductSeq);
			}
        }

		this.Response.Write(bResult ? "0" : "1");
        this.Response.End();
    }
}
