﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Settle
--	Title			: Edy決済結果(Sophia)
--	Progaram ID		: SettleEdy
--
--  Creation Date	: 2010.05.18
--  Creater			: 
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using iBridCommLib;
using ViComm;

public partial class SettleEdy:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		try {

			string sStatus = iBridUtil.GetStringValue(Request.QueryString["status"]);
			string sSid = iBridUtil.GetStringValue(Request.QueryString["orderid"]);
			string userSeq = "";
			string sMoney = "0";

			using (SettleLog oSettleLog = new SettleLog()) {
				if (oSettleLog.GetOne(sSid)) {
					sMoney = oSettleLog.settleAmt.ToString();
				}
			}

			string sResult = "0";
			string sResponse = "9";
			if (sStatus.Equals("1")) {
				sResponse = "0";
			}

			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("LOG_SETTLE_RESULT");
				db.ProcedureInParm("PSID",DbSession.DbType.VARCHAR2,sSid);
				db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,userSeq);
				db.ProcedureInParm("PSETTLE_AMT",DbSession.DbType.NUMBER,int.Parse(sMoney));
				db.ProcedureInParm("PSETTLE_POINT",DbSession.DbType.NUMBER,0);
				db.ProcedureInParm("PRESPONSE",DbSession.DbType.VARCHAR2,sResponse);
				db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
				db.ExecuteProcedure();
				sResult = db.GetStringValue("PRESULT");
			}

			Response.ContentType = "text/plain";
			Response.Charset = "shift_jis";
			if (sResult.Equals("0")) {
				Response.Write("OK");
			} else {
				Response.Write("NG");
			}

		} catch (Exception) {
			Response.ContentType = "text/plain";
			Response.Charset = "shift_jis";
			Response.Write(string.Format("{0}","NG"));
		}
		Response.End();
	}
}
