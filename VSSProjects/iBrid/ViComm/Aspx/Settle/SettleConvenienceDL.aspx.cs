﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Settle
--	Title			: コンビニＤＬ決済結果
--	Progaram ID		: SettleConvenienceDL
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using iBridCommLib;
using ViComm;

public partial class SettleConvenienceDL:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		string sResult = "9";

		try {
			string sUserSeq = iBridUtil.GetStringValue(Request.QueryString["FUKA"]);
			string sSid = iBridUtil.GetStringValue(Request.QueryString["SID"]);
			string sMoney = iBridUtil.GetStringValue(Request.QueryString["KINGAKU"]);

			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("LOG_SETTLE_RESULT");
				db.ProcedureInParm("PSID",DbSession.DbType.VARCHAR2,sSid);
				db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,sUserSeq);
				db.ProcedureInParm("PSETTLE_AMT",DbSession.DbType.NUMBER,int.Parse(sMoney));
				db.ProcedureInParm("PSETTLE_POINT",DbSession.DbType.NUMBER,0);
				db.ProcedureInParm("PRESPONSE",DbSession.DbType.VARCHAR2,"0");
				db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
				db.ExecuteProcedure();
				sResult = db.GetStringValue("PRESULT");
			}
			if (!sResult.Equals("0")) {
				sResult = "9";
			}
		} catch (Exception) {
		}
		Response.ContentType = "text/html";
		Response.Write(sResult);
		Response.End();
	}
}
