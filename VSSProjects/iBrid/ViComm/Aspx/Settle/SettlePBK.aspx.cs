﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Settle
--	Title			: ﾎﾟｲﾝﾄｱﾌﾘ決済結果
--	Progaram ID		: SettlePBK
--
--  Creation Date	: 2010.02.05
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using iBridCommLib;
using ViComm;

public partial class SettlePBK:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		try {
			string sMoney = iBridUtil.GetStringValue(Request.QueryString["commission"]);
			string sU = iBridUtil.GetStringValue(Request.QueryString["uid"]);
			string[] sInfo = sU.Split(':');

			int iMoney = 0;
			int.TryParse(sMoney,out iMoney);
			
			int iPoint = 0;
			
			using (SettleLog oLog = new SettleLog()) {
				oLog.GetOne(sInfo[0]);
				iPoint = iMoney / oLog.pointPrice;
			}
			string sResult = "0";
			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("LOG_SETTLE_RESULT");
				db.ProcedureInParm("PSID",DbSession.DbType.VARCHAR2,sInfo[0]);
				db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,sInfo[1]);
				db.ProcedureInParm("PSETTLE_AMT",DbSession.DbType.NUMBER,int.Parse(sMoney));
				db.ProcedureInParm("PSETTLE_POINT",DbSession.DbType.NUMBER,iPoint);
				db.ProcedureInParm("PRESPONSE",DbSession.DbType.VARCHAR2,"0");
				db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
				db.ExecuteProcedure();
				sResult = db.GetStringValue("PRESULT");

			}
			Response.ContentType = "text/plain";
			Response.Charset = "shift_jis";
			Response.Write("success");

		} catch (Exception) {
			Response.ContentType = "text/plain";
			Response.Charset = "shift_jis";
			Response.Write("false");
		}
		Response.End();
	}
}
