﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Settle
--	Title			: MCHA決済結果
--	Progaram ID		: SettleMCha
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using iBridCommLib;
using ViComm;

public partial class SettleMCha:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		try {
			int iPoint = int.Parse(iBridUtil.GetStringValue(Request.QueryString["pt"]));
			int iMoney = iPoint * 10;
			string sSid = iBridUtil.GetStringValue(Request.QueryString["uniq"]);
			string[] sInfo = sSid.Split(':');
			string sResult;

			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("LOG_SETTLE_RESULT");
				db.ProcedureInParm("PSID",DbSession.DbType.VARCHAR2,sInfo[0]);
				db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,sInfo[1]);
				db.ProcedureInParm("PSETTLE_AMT",DbSession.DbType.NUMBER,iMoney);
				db.ProcedureInParm("PSETTLE_POINT",DbSession.DbType.NUMBER,iPoint);
				db.ProcedureInParm("PRESPONSE",DbSession.DbType.VARCHAR2,"0");
				db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
				db.ExecuteProcedure();
				sResult = db.GetStringValue("PRESULT");
			}
			Response.ContentType = "text/plain";
			Response.Charset = "shift_jis";
			Response.Write("0");

		} catch (Exception) {
			Response.ContentType = "text/plain";
			Response.Charset = "shift_jis";
			Response.Write("-1");
		}
		Response.End();
	}
}
