﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Settle
--	Title			: 自動入金
--	Progaram ID		: SettleAutoReceipt
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using iBridCommLib;
using ViComm;

public partial class SettleAutoReceipt:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		try {
			string sSiteCd = iBridUtil.GetStringValue(Request.Form["SITECD"]);
			string sLoginId = iBridUtil.GetStringValue(Request.Form["USER"]);
			string sProcNo = iBridUtil.GetStringValue(Request.Form["PROCNO"]);
			int iMoney = int.Parse(iBridUtil.GetStringValue(Request.Form["AMOUNT"]));
			string sResult;

			sSiteCd = "M003";

			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("WEB_AUTO_RECEIPT");
				db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,sSiteCd);
				db.ProcedureInParm("PLOGIN_ID",DbSession.DbType.VARCHAR2,sLoginId);
				db.ProcedureInParm("PAMOUNT",DbSession.DbType.NUMBER,iMoney);
				db.ProcedureInParm("PPROC_NO",DbSession.DbType.VARCHAR2,sProcNo);
				db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
				db.ExecuteProcedure();
				sResult = db.GetStringValue("PRESULT");
			}
			Response.ContentType = "text/plain";
			Response.Charset = "shift_jis";
			Response.Write(string.Format("PRTCL=A\r\nRESULT={0}\r\n",sResult));

		} catch (Exception) {
			Response.ContentType = "text/plain";
			Response.Charset = "shift_jis";
			Response.Write(string.Format("PRTCL=A\r\nRESULT={0}","-1"));
		}
		Response.End();
	}
}
