﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Settle
--	Title			: 大人id決済確認
--	Progaram ID		: ConfirmAdultId
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using iBridCommLib;
using System.Web;
using System.Text;
using ViComm;

public partial class ConfirmAdultId:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		string sBackUrl = "";

		string sClientip = iBridUtil.GetStringValue(Request.Form["clcd"]);
		string sMoney = iBridUtil.GetStringValue(Request.Form["ch"]);
		string sSid = iBridUtil.GetStringValue(Request.Form["trid"]);
		string sUserSeq = iBridUtil.GetStringValue(Request.Form["c1"]);

		try {
			string sResult = "9";
			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("CHECK_SETTLE_REQUEST");
				db.ProcedureInParm("PSID",DbSession.DbType.VARCHAR2,sSid);
				db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,sUserSeq);
				db.ProcedureInParm("PSETTLE_ATM",DbSession.DbType.NUMBER,int.Parse(sMoney));
				db.ProcedureOutParm("PLOGIN_ID",DbSession.DbType.VARCHAR2);
				db.ProcedureOutParm("PLOGIN_PASSWORD",DbSession.DbType.VARCHAR2);
				db.ProcedureOutParm("PSITE_URL",DbSession.DbType.VARCHAR2);
				db.ProcedureOutParm("PBACK_URL",DbSession.DbType.VARCHAR2);
				db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
				db.ExecuteProcedure();
				sResult = db.GetStringValue("PRESULT");
				sBackUrl = db.GetStringValue("PBACK_URL");
				sBackUrl = string.Format(sBackUrl,db.GetStringValue("PSITE_URL"),db.GetStringValue("PLOGIN_ID"),db.GetStringValue("PLOGIN_PASSWORD"));
			}

			Response.ContentType = "text/plain";
			Response.Charset = "shift_jis";
			string sUrl = string.Format("rc={0}\nc1={1}\nc2=\nc3=\nu1={2}\nu2={2}\n",sResult,sUserSeq,sBackUrl);
			Response.Write(sUrl);

		} catch (Exception) {
			Response.ContentType = "text/plain";
			Response.Charset = "shift_jis";
			string sUrl = string.Format("rc={0}\nc1={1}\nc2=\nc3=\nu1={2}\nu2={2}\n","-1",sUserSeq,sBackUrl);
		}
		Response.End();
	}
}
