﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Settle
--	Title			: 出演者マイレージ（ADinsightReward）
--	Progaram ID		: CastADinsightReward
--  Creation Date	: 2014.03.10
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using iBridCommLib;
using ViComm;
using System.Text.RegularExpressions;

public partial class CastADinsightReward:System.Web.UI.Page {
	protected void Page_Load(object sender,EventArgs e) {
		try {
			string sSiteCd = string.Empty;
			string sHostNm = Request.UserHostName;
			string sLoginId = iBridUtil.GetStringValue(Request.QueryString["id"]);
			int iAddPoint = 0;
			int.TryParse(iBridUtil.GetStringValue(Request.QueryString["point"]),out iAddPoint);
			string sResult = "-1";

			string sTestHostNm = iBridUtil.GetStringValue(Request.QueryString["testhostnm"]);

			if (!string.IsNullOrEmpty(sTestHostNm)) {
				sHostNm = sTestHostNm;
			}

			using (Site oSite = new Site()) {
				sSiteCd = oSite.GetSiteCdBySubHostNm(Request.Url.Host);
			}

			if (string.IsNullOrEmpty(sSiteCd)) {
				ViCommInterface.WriteIFLog("CastADinsightReward",string.Format("Not found SiteCd {0}",Request.Url.Host));
			} else {
				using (DbSession db = new DbSession()) {
					db.PrepareProcedure("ADD_CAST_ADINSIGHT_REWARD_PT");
					db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,sSiteCd);
					db.ProcedureInParm("pHOST_NM",DbSession.DbType.VARCHAR2,sHostNm);
					db.ProcedureInParm("pLOGIN_ID",DbSession.DbType.VARCHAR2,sLoginId);
					db.ProcedureInParm("pADD_POINT",DbSession.DbType.NUMBER,iAddPoint);
					db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
					db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
					db.cmd.BindByName = true;
					db.ExecuteProcedure();
					sResult = db.GetStringValue("pRESULT");
				}
			}

			Response.ContentType = "text/plain";
			Response.Charset = "shift_jis";
			Response.Write(sResult);
		} catch (Exception) {
			Response.ContentType = "text/plain";
			Response.Charset = "shift_jis";
			Response.Write("-2");
		}

		Response.End();
	}
}
