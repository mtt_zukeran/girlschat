﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Settle
--	Title			: 商品動画
--	Progaram ID		: ProductMovie
--
--  Creation Date	: 2010.12.08
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class ProductMovie : DbSession
{
    public ProductMovie()
    { }

    public object GetObjSeq(string pSiteCd, string pProductAgentCd, string pLinkId, string pProductMovieType)
    {
        try
        {
            this.conn = this.DbConnect();

            string query =
@"
SELECT
	OBJ_SEQ
FROM 
	T_PRODUCT_MOVIE
WHERE
	SITE_CD            = :SITE_CD          AND
	PRODUCT_AGENT_CD   = :PRODUCT_AGENT_CD AND
	LINK_ID            = :LINK_ID          AND
    PRODUCT_MOVIE_TYPE = :PRODUCT_MOVIE_TYPE
";

			using (this.cmd = CreateSelectCommand(query,this.conn))
            {
                cmd.Parameters.Add(new OracleParameter("SITE_CD", pSiteCd));
                cmd.Parameters.Add(new OracleParameter("PRODUCT_AGENT_CD", pProductAgentCd));
                cmd.Parameters.Add(new OracleParameter("LINK_ID", pLinkId));
                cmd.Parameters.Add(new OracleParameter("PRODUCT_MOVIE_TYPE", pProductMovieType));

                return this.cmd.ExecuteScalar();
            }
        }
        finally
        {
            this.conn.Close();
        }
    }
}
