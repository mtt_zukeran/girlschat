﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Settle
--	Title			: オブジェクト利用履歴
--	Progaram ID		: ObjUsedHistory
--
--  Creation Date	: 2009.09.02
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class ObjUsedHistory:DbSession {

	public ObjUsedHistory() {
	}

	public bool GetOne(string pSiteCd,string pUserSeq,string pObjType,string pObjSeq) {
		DataSet ds;
		try {
			conn = DbConnect("ObjUsedHistory.GetOne");
			ds = new DataSet();

			string sSql = " SELECT " +
								" NVL(COUNT(*),0) AS CNT " +
							" FROM " +
								" T_OBJ_USED_HISTORY " +
							" WHERE " +
								" SITE_CD = :SITE_CD AND " +
								" USER_SEQ = :USER_SEQ AND " +
								" OBJ_TYPE = :OBJ_TYPE AND " +
								" OBJ_SEQ = :OBJ_SEQ ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("OBJ_TYPE",pObjType);
				cmd.Parameters.Add("OBJ_SEQ",pObjSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_OBJ_USED_HISTORY");
				}
			}
		} finally {
			conn.Close();
		}

		if (ds.Tables["T_OBJ_USED_HISTORY"].Rows[0]["CNT"].ToString().Equals("0")) {
			return false;
		} else {
			return true;
		}
	}
}
