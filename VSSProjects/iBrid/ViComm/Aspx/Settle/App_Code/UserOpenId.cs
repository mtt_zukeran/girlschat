﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: OpenId
--	Progaram ID		: Pack
--
--  Creation Date	: 2010.08.25
--  Creater			: K.Itoh
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;
using ViComm;
using iBridCommLib;
using System.Text;

public class UserOpenId :DbSession{
	public UserOpenId() {}
	
	public string openId = null;
	public string openIdType = null;
	public string userSeq = null;
	
	public bool GetOne(string pOpenId,string pOpenIdType){
		bool bExist = false;
		try{
			StringBuilder objSqlBuilder = new StringBuilder();
			objSqlBuilder.Append(" SELECT                                                                          ").AppendLine();
			objSqlBuilder.Append("     *                                                                           ").AppendLine();
			objSqlBuilder.Append(" FROM                                                                            ").AppendLine();
			objSqlBuilder.Append("     T_USER_OPEN_ID                                                              ").AppendLine();
			objSqlBuilder.Append(" WHERE                                                                           ").AppendLine();
			objSqlBuilder.Append("         OPEN_ID		= :OPEN_ID												   ").AppendLine();
			objSqlBuilder.Append("     AND OPEN_ID_TYPE = :OPEN_ID_TYPE											   ").AppendLine();		
			
			conn = base.DbConnect();

			using (cmd = CreateSelectCommand(objSqlBuilder.ToString(),conn))
			using (da = new OracleDataAdapter(cmd)) {
				cmd.Parameters.Add(":OPEN_ID", pOpenId);
				cmd.Parameters.Add(":OPEN_ID_TYPE", pOpenIdType);

				using (DataSet ds = new DataSet()) {

					da.Fill(ds, "T_USER_OPEN_ID");
					if (ds.Tables["T_USER_OPEN_ID"].Rows.Count != 0) {
						DataRow dr = ds.Tables["T_USER_OPEN_ID"].Rows[0];
						this.openId = iBridUtil.GetStringValue(dr["OPEN_ID"]);
						this.openIdType = iBridUtil.GetStringValue(dr["OPEN_ID_TYPE"]);
						this.userSeq = iBridUtil.GetStringValue(dr["USER_SEQ"]);
						bExist = true;
					}					
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}
}
