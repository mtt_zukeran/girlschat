﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Settle
--	Title			: クレジットプロセス

--	Progaram ID		: CreditProcess
--
--  Creation Date	: 2010.02.28
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;

public class CreditProcess:DbSession {
	public CreditProcess() {
	}

	public DataSet GetList(string pRevisionNo) {
		DataSet ds;

		conn = DbConnect();
		ds = new DataSet();

		string sSql = "SELECT " +
							"USER_SEQ		," +
							"UNIQUE_ID		," +
							"SETTLE_AMT		," +
							"SETTLE_POINT	," +
							"TEL			," +
							"EMAIL_ADDR		" +						
						"FROM " +
							"T_CREDIT_PROCESS " +
						"WHERE " +
							"REVISION_NO = :REVISION_NO";

		using (cmd = CreateSelectCommand(sSql,conn)) {
			cmd.Parameters.Add("REVISION_NO",pRevisionNo);
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"T_CREDIT_PROCESS");
			}
		}
		conn.Close();
		return ds;
	}
}
