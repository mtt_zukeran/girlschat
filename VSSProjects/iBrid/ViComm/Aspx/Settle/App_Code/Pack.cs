﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 料金パック
--	Progaram ID		: Pack
--
--  Creation Date	: 2010.08.25
--  Creater			: K.Itoh
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;
using ViComm;
using iBridCommLib;

public class Pack : DbSession {


	public string siteCd;
	public string settleType;
	public int salesAmt;
	public string commoditiesCd;
	public string remarks;

	public Pack() {
		settleType = "";
		salesAmt = 0;
		commoditiesCd = "";
	}

	public bool GetOne(string pSiteCd, string pSettleType, string pSalesAmt) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;

		conn = DbConnect("Pack.GetOne");

		using (cmd = CreateSelectCommand("SELECT * FROM T_PACK WHERE SITE_CD = :SITE_CD AND SETTLE_TYPE =:SETTLE_TYPE AND SALES_AMT =:SALES_AMT ",conn))
		using (ds = new DataSet()) {
			cmd.Parameters.Add("SITE_CD", pSiteCd);
			cmd.Parameters.Add("SETTLE_TYPE", pSettleType);
			cmd.Parameters.Add("SALES_AMT", pSalesAmt);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds, "T_PACK");
				if (ds.Tables["T_PACK"].Rows.Count != 0) {
					dr = ds.Tables["T_PACK"].Rows[0];
					siteCd = dr["SITE_CD"].ToString();
					settleType = dr["SETTLE_TYPE"].ToString();
					salesAmt = int.Parse(dr["SALES_AMT"].ToString());
					commoditiesCd = iBridUtil.GetStringValue(dr["COMMODITIES_CD"]);
					remarks = iBridUtil.GetStringValue(dr["REMARKS"]);
					bExist = true;
				}
			}
		}

		conn.Close();

		return bExist;
	}
}
