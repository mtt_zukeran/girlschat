﻿using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;

public class SiteSupplement : DbSession {


	public SiteSupplement() {

	}

	public string GetValue(string pSiteCd,string pSupplementCd) {

		DataSet ds;
		DataRow dr;
		string sValue = "";
		
		conn = DbConnect();

		using (cmd = CreateSelectCommand("SELECT SUPPLEMENT_VALUE FROM T_SITE_SUPPLEMENT " +
					"WHERE SITE_CD = :SITE_CD AND SUPPLEMENT_CD =:SUPPLEMENT_CD", conn))
		using (ds = new DataSet()) {
			cmd.Parameters.Add("SITE_CD", pSiteCd);
			cmd.Parameters.Add("SUPPLEMENT_CD", pSupplementCd);
          
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds, "SITE_SUPPLEMENT");
				if (ds.Tables["SITE_SUPPLEMENT"].Rows.Count != 0) {
					dr = ds.Tables["SITE_SUPPLEMENT"].Rows[0];
					sValue = dr["SUPPLEMENT_VALUE"].ToString();
				}
			}
		}

		conn.Close();

		return sValue;
	}

}
