﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Settle
--	Title			: サイト別決済方法
--	Progaram ID		: SiteSettle
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;

public class SiteSettle:DbSession {


	public string cpIdNo;
	public string cpPassword;
	public string settleCompanyCd;
	public string settleUrl;
	public string continueSettleUrl;
	public string backUrl;
	public string regexLoginId;
	public string regexAspCd;
	public string regexPoint;
	public string regexAmt;
	public string regexSettleSeq;
	public string notCreateSettleReqLogFlag;

	public SiteSettle() {
		cpIdNo = string.Empty;
		cpPassword = string.Empty;
		settleCompanyCd = string.Empty;
		settleUrl = string.Empty;
		continueSettleUrl = string.Empty;
		backUrl = string.Empty;
		regexLoginId = string.Empty;
		regexAspCd = string.Empty;
		regexPoint = string.Empty;
		regexAmt = string.Empty;
		regexSettleSeq = string.Empty;
		notCreateSettleReqLogFlag = string.Empty;
	}


	private string SelectSql() {
		return "SELECT CP_ID_NO,CP_PASSWORD,SETTLE_COMPANY_CD,SETTLE_URL,CONTINUE_SETTLE_URL,BACK_URL,REGEX_LOGIN_ID,REGEX_ASP_AD_CD,REGEX_POINT,REGEX_AMT,REGEX_SETTLE_SEQ,NOT_CREATE_SETTLE_REQ_LOG_FLAG FROM T_SITE_SETTLE ";
	}


	public bool GetOne(string pSiteCd,string pSettleCompanyCd,string pSettleType) {
		DataSet ds;
		bool bExist = false;

		conn = DbConnect("SiteSettle.GetOne");

		using (cmd = CreateSelectCommand(SelectSql() + " WHERE SITE_CD = :SITE_CD AND SETTLE_COMPANY_CD =:SETTLE_COMPANY_CD AND SETTLE_TYPE =:SETTLE_TYPE",conn))
		using (ds = new DataSet()) {
			cmd.Parameters.Add("SITE_CD",pSiteCd);
			cmd.Parameters.Add("SETTLE_COMPANY_CD",pSettleCompanyCd);
			cmd.Parameters.Add("SETTLE_TYPE",pSettleType);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"T_SETTLE_COMPANY");
				bExist = SetValue(ds);
			}
		}
		conn.Close();
		return bExist;
	}

	public bool GetOneByHostNm(string pSiteCd,string pHostNm,string pSettleType,string pHostNo) {
		DataSet ds;
		bool bExist = false;
		
		string sColHostNm = "HOST_NM";
		
		if (!string.IsNullOrEmpty(pHostNo)) {
			sColHostNm = sColHostNm + pHostNo;
		}

		conn = DbConnect("SiteSettle.GetOneByHostNm");

		using (cmd = CreateSelectCommand(SelectSql() + string.Format(" WHERE SITE_CD = :SITE_CD AND :HOST_NM LIKE {0} AND SETTLE_TYPE =:SETTLE_TYPE ",sColHostNm),conn))
		using (ds = new DataSet()) {
			cmd.Parameters.Add("SITE_CD",pSiteCd);
			cmd.Parameters.Add("HOST_NM",pHostNm);
			cmd.Parameters.Add("SETTLE_TYPE",pSettleType);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"T_SETTLE_COMPANY");
				bExist = SetValue(ds);
			}
		}
		conn.Close();
		return bExist;
	}



	private bool SetValue(DataSet pDS) {
		DataRow dr;
		if (pDS.Tables["T_SETTLE_COMPANY"].Rows.Count != 0) {
			dr = pDS.Tables["T_SETTLE_COMPANY"].Rows[0];
			cpIdNo = dr["CP_ID_NO"].ToString();
			cpPassword = dr["CP_PASSWORD"].ToString();
			settleCompanyCd = dr["SETTLE_COMPANY_CD"].ToString();
			settleUrl = dr["SETTLE_URL"].ToString();
			continueSettleUrl = dr["CONTINUE_SETTLE_URL"].ToString();
			backUrl = dr["BACK_URL"].ToString();
			regexLoginId = dr["REGEX_LOGIN_ID"].ToString();
			regexAspCd = dr["REGEX_ASP_AD_CD"].ToString();
			regexPoint = dr["REGEX_POINT"].ToString();
			regexAmt = dr["REGEX_AMT"].ToString();
			regexSettleSeq = dr["REGEX_SETTLE_SEQ"].ToString();
			notCreateSettleReqLogFlag = dr["NOT_CREATE_SETTLE_REQ_LOG_FLAG"].ToString();
			return true;
		} else {
			return false;
		}
	}
}
