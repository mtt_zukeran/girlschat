﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Settle
--	Title			: 決済ログ
--	Progaram ID		: SettleLog
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;

public class SettleLog:DbSession {


	public string backUrl;
	public string loginId;
	public string loginPassword;
	public string url;
	public string aspAdCd;
	public int affiliatePoint;
	public int affiliateExAmt;
	public int pointPrice;
	public int settleAmt;
	public string applyStartDay;
	public string applyEndDay;
	public string userSeq;

	public SettleLog() {
		backUrl = "";
		loginId = "";
		loginPassword = "";
		affiliatePoint = 0;
		affiliateExAmt = 0;
		applyStartDay = "";
		applyEndDay = "";
		userSeq = "";
		url = "";
	}

	public bool GetOne(string pSid) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;

		conn = DbConnect("SettleLog.GetOne");

		using (cmd = CreateSelectCommand("SELECT USER_SEQ,BACK_URL,LOGIN_ID,LOGIN_PASSWORD,ASP_AD_CD,AFFILIATE_POINT,AFFILIATE_EX_AMT,APPLY_START_DAY,APPLY_END_DAY,URL,POINT_PRICE,SETTLE_AMT FROM VW_SETTLE_LOG02 WHERE SID =:SID  ",conn))
		using (ds = new DataSet()) {
			cmd.Parameters.Add("SID",pSid);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"VW_SETTLE_LOG02");
				if (ds.Tables["VW_SETTLE_LOG02"].Rows.Count != 0) {
					dr = ds.Tables["VW_SETTLE_LOG02"].Rows[0];
					backUrl = dr["BACK_URL"].ToString();
					userSeq = dr["USER_SEQ"].ToString();
					loginId = dr["LOGIN_ID"].ToString();
					loginPassword = dr["LOGIN_PASSWORD"].ToString();
					url = dr["URL"].ToString();
					aspAdCd = dr["ASP_AD_CD"].ToString();
					affiliatePoint = int.Parse(dr["AFFILIATE_POINT"].ToString());
					affiliateExAmt = int.Parse(dr["AFFILIATE_EX_AMT"].ToString());
					pointPrice = int.Parse(dr["POINT_PRICE"].ToString());
					applyStartDay = dr["APPLY_START_DAY"].ToString();
					applyEndDay = dr["APPLY_END_DAY"].ToString();
					settleAmt = int.Parse(dr["SETTLE_AMT"].ToString());
					bExist = true;
				}
			}
		}
		conn.Close();
		return bExist;
	}

	public void LogSettleRequest(string pSiteCd,string pUserSeq,string pSettleCompanyCd,string pSettleType,string pSettleStatus,int pSettleAmt,int pExPoint,string pSettleID,out string pSid) {
		LogSettleRequest(pSiteCd,pUserSeq,pSettleCompanyCd,pSettleType,pSettleStatus,pSettleAmt,pExPoint,pSettleID,string.Empty,out pSid);
	}
	public void LogSettleRequest(string pSiteCd,string pUserSeq,string pSettleCompanyCd,string pSettleType,string pSettleStatus,int pSettleAmt,int pExPoint,string pSettleID,string pSettleUniqueValue,out string pSid) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("LOG_SETTLE_REQUEST");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PSETTLE_COMPNAY_CD",DbSession.DbType.VARCHAR2,pSettleCompanyCd);
			db.ProcedureInParm("PSETTLE_TYPE",DbSession.DbType.VARCHAR2,pSettleType);
			db.ProcedureInParm("PSETTLE_STATUS",DbSession.DbType.VARCHAR2,pSettleStatus);
			db.ProcedureInParm("PSETTLE_AMT",DbSession.DbType.NUMBER,pSettleAmt);
			db.ProcedureInParm("PSETTLE_POINT",DbSession.DbType.NUMBER,pExPoint);
			db.ProcedureInParm("PSETTLE_ID",DbSession.DbType.VARCHAR2,pSettleID);
			db.ProcedureInParm("PASP_AD_CD",DbSession.DbType.VARCHAR2,"");
			db.ProcedureOutParm("PSETTLE_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ProcedureInParm("PSETTLE_UNIQUE_VALUE",DbSession.DbType.VARCHAR2,pSettleUniqueValue);
			db.ExecuteProcedure();
			pSid = db.GetStringValue("PSID");
		}
	}

	public bool LogSettleResult(string pSid,string pUserSeq,int pSettleAmt,int pSettlePoint,string pResponse) {
		string sRet;
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("LOG_SETTLE_RESULT");
			db.ProcedureInParm("PSID",DbSession.DbType.VARCHAR2,pSid);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PSETTLE_AMT",DbSession.DbType.NUMBER,pSettleAmt);
			db.ProcedureInParm("PSETTLE_POINT",DbSession.DbType.NUMBER,pSettlePoint);
			db.ProcedureInParm("PRESPONSE",DbSession.DbType.VARCHAR2,pResponse);
			db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			sRet = db.GetStringValue("PRESULT");
		}
		return sRet.Equals("0");
	}

	public bool IsExistsSettleId(string pSettleId,string pSettleCompanyCd) {
		DataSet ds;
		bool bExist = false;

		conn = DbConnect("SettleLog.IsExistsSettleId");

		using (cmd = CreateSelectCommand("SELECT USER_SEQ FROM T_SETTLE_LOG WHERE SETTLE_ID =:SETTLE_ID AND SETTLE_COMPANY_CD = :SETTLE_COMPANY_CD  ",conn))
		using (ds = new DataSet()) {
			cmd.Parameters.Add("SETTLE_ID",pSettleId);
			cmd.Parameters.Add("SETTLE_COMPANY_CD",pSettleCompanyCd);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"T_SETTLE_LOG");
				if (ds.Tables["T_SETTLE_LOG"].Rows.Count != 0) {
					bExist = true;
				}
			}
		}
		conn.Close();
		return bExist;
	}

	public DataSet GetList(string pProcNo) {
		DataSet ds;

		conn = DbConnect();
		ds = new DataSet();

		string sSql = "SELECT * FROM VW_SETTLE_LOG04 WHERE FANCLUB_FEE_PROC_SEQ = :FANCLUB_FEE_PROC_SEQ";
		using (cmd = CreateSelectCommand(sSql,conn)) {
			cmd.Parameters.Add("FANCLUB_FEE_PROC_SEQ",pProcNo);
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"T_SETTLE_LOG");
			}
		}
		conn.Close();
		return ds;
	}
	
	public DataSet GetListAutoSettle(string pProcNo) {
		DataSet ds;

		conn = DbConnect();
		ds = new DataSet();

		string sSql = "SELECT * FROM VW_SETTLE_LOG04 WHERE AUTO_SETTLE_PROC_SEQ = :AUTO_SETTLE_PROC_SEQ";
		using (cmd = CreateSelectCommand(sSql,conn)) {
			cmd.Parameters.Add("AUTO_SETTLE_PROC_SEQ",pProcNo);
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"T_SETTLE_LOG");
			}
		}
		conn.Close();
		return ds;
	}
}
