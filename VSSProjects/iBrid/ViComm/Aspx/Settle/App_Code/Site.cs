﻿using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;

public class Site : DbSession {


	public Site() {

	}

	public string GetSiteCdBySubHostNm(string pSubHostNm) {

		DataSet ds;
		DataRow dr;
		string sValue = string.Empty;
		
		conn = DbConnect();

		using (cmd = CreateSelectCommand("SELECT SITE_CD FROM T_SITE " +
					"WHERE SUB_HOST_NM = :SUB_HOST_NM", conn))
		using (ds = new DataSet()) {
			cmd.Parameters.Add("SUB_HOST_NM", pSubHostNm);
			
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds, "T_SITE");
				if (ds.Tables["T_SITE"].Rows.Count != 0) {
					dr = ds.Tables["T_SITE"].Rows[0];
					sValue = dr["SITE_CD"].ToString();
				}
			}
		}

		conn.Close();

		return sValue;
	}

}
