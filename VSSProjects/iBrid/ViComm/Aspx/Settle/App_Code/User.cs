﻿using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Web;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using ViComm;

public class User:DbSession {

	public string ChangeLoginIdToUserSeq(string pLoginId) {
		string sUserSeq = string.Empty;
		DataSet ds;
		DataRow dr;

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append(" SELECT						").AppendLine();
		oSqlBuilder.Append("     USER_SEQ				").AppendLine();
		oSqlBuilder.Append(" FROM						").AppendLine();
		oSqlBuilder.Append("     T_USER					").AppendLine();
		oSqlBuilder.Append(" WHERE						").AppendLine();
		oSqlBuilder.Append("     LOGIN_ID = :LOGIN_ID").AppendLine();

		try {
			conn = this.DbConnect();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add(":LOGIN_ID",pLoginId);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_USER");
					if (ds.Tables["T_USER"].Rows.Count != 0) {
						dr = ds.Tables[0].Rows[0];
						sUserSeq = dr["USER_SEQ"].ToString();
					}
				}
			}
		} finally {
			conn.Close();
		}
		return sUserSeq;
	}

	public string ChangeBeforeSystemIdToUserSeq(string pBeforeSystemId) {
		string sUserSeq = string.Empty;
		DataSet ds;
		DataRow dr;

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append(" SELECT											").AppendLine();
		oSqlBuilder.Append("     USER_SEQ									").AppendLine();
		oSqlBuilder.Append(" FROM											").AppendLine();
		oSqlBuilder.Append("     T_USER										").AppendLine();
		oSqlBuilder.Append(" WHERE											").AppendLine();
		oSqlBuilder.Append("     BEFORE_SYSTEM_ID	= :BEFORE_SYSTEM_ID AND	").AppendLine();
		oSqlBuilder.Append("     SEX_CD				= :SEX_CD				").AppendLine();

		try {
			conn = this.DbConnect();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("BEFORE_SYSTEM_ID",pBeforeSystemId);
				cmd.Parameters.Add("SEX_CD",ViCommConst.MAN);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_USER");
					if (ds.Tables["T_USER"].Rows.Count != 0) {
						dr = ds.Tables[0].Rows[0];
						sUserSeq = dr["USER_SEQ"].ToString();
					}
				}
			}
		} finally {
			conn.Close();
		}
		return sUserSeq;
	}
}
