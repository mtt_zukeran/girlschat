﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Settle
--	Title			: C-CHECK決済結果
--	Progaram ID		: SettleCCheck
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using iBridCommLib;
using ViComm;

public partial class SettleCCheck:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		try {
			string sSid = iBridUtil.GetStringValue(Request.QueryString["SID"]);
			string sMoney = iBridUtil.GetStringValue(Request.QueryString["KINGAKU"]);
			string sFuka = iBridUtil.GetStringValue(Request.QueryString["FUKA"]);
			string[] sItem = sFuka.Split(':');

			string sResponse = "0";
			string sResult = "0";

			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("LOG_SETTLE_RESULT");
				db.ProcedureInParm("PSID",DbSession.DbType.VARCHAR2,sSid);
				db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,sItem[0]);
				db.ProcedureInParm("PSETTLE_AMT",DbSession.DbType.NUMBER,int.Parse(sMoney));
				db.ProcedureInParm("PSETTLE_POINT",DbSession.DbType.NUMBER,0);
				db.ProcedureInParm("PRESPONSE",DbSession.DbType.VARCHAR2,sResponse);
				db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
				db.ExecuteProcedure();
				sResult = db.GetStringValue("PRESULT");
				if (!sResult.Equals("0")) {
					sResult = "9";
				}
			}

			Response.ContentType = "text/plain";
			Response.Charset = "shift_jis";
			Response.Write(string.Format("{0}",sResult));

		} catch (Exception) {
			Response.ContentType = "text/plain";
			Response.Charset = "shift_jis";
			Response.Write(string.Format("{0}","9"));
		}
		Response.End();
	}
}
