﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Settle
--	Title			: SoftBank Payment Service 決済結果
--	Progaram ID		: SettleSps
--
--  Creation Date	: 2013.06.18
--  Creater			: kito@ibrid.co.jp
--
**************************************************************************/
using System;
using iBridCommLib;
using ViComm;

public partial class SettleSps:System.Web.UI.Page {
	protected void Page_Load(object sender,EventArgs e) {
		string sResult = null;
		string sResponse = "0";
		try {
			if (!this.IsPostBack) {
				string sPayMethod = this.Request.Params["pay_method"];
				string sMerchantId = this.Request.Params["merchant_id"];
				string sServiceId = this.Request.Params["service_id"];
				string sCustCode = this.Request.Params["cust_code"];
				string sOrderId = this.Request.Params["order_id"];
				string sItemId = this.Request.Params["item_id"];
				string sAmount = this.Request.Params["amount"];
				string sPayType = this.Request.Params["pay_type"];
				string sAutoChargeType = this.Request.Params["auto_charge_type"];
				string sServiceType = this.Request.Params["service_type"];
				string sDivSettle = this.Request.Params["div_settle"];

				using (DbSession db = new DbSession()) {
					db.PrepareProcedure("LOG_SETTLE_RESULT");
					db.ProcedureInParm("PSID",DbSession.DbType.VARCHAR2,sOrderId);
					db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,sCustCode);
					db.ProcedureInParm("PSETTLE_AMT",DbSession.DbType.NUMBER,int.Parse(sAmount));
					db.ProcedureInParm("PSETTLE_POINT",DbSession.DbType.NUMBER,0);
					db.ProcedureInParm("PRESPONSE",DbSession.DbType.VARCHAR2,sResponse);
					db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
					db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
					db.ExecuteProcedure();
					sResult = db.GetStringValue("PRESULT");
					if (!sResult.Equals("0")) {
						sResult = "9";
					}
				}


				Response.ContentType = "text/plain";
				Response.Charset = "shift_jis";
				Response.Clear();
				Response.Write("OK");


			} else {
				throw new InvalidOperationException();
			}
		} catch (Exception ex) {
			EventLogWriter.Error(ex);

			Response.ContentType = "text/plain";
			Response.Charset = "shift_jis";
			Response.Clear();
			Response.Write("NG");
		}

	
	}
}
