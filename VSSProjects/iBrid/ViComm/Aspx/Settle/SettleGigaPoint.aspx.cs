﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Settle
--	Title			: GIGA-POINT決済結果
--	Progaram ID		: SettleGigaPoint
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Web;
using System.Text;
using iBridCommLib;
using ViComm;

public partial class SettleGigaPoint:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		try {
			string sMoney = iBridUtil.GetStringValue(Request.Form["settle_amount"]);
			string sSid = iBridUtil.GetStringValue(Request.Form["mall_order_no"]);
			string sRxResult = iBridUtil.GetStringValue(Request.Form["status_code"]);
			string sUserSeq = "GIGAPOINT";
			int iMoney;
			int.TryParse(sMoney,out iMoney);

			ViCommInterface.WriteIFLog("GIGA-POINT SETTLE",string.Format("sid={0} money={1} status={2}",sSid,sMoney,sRxResult));

			string sResponse = "9";
			if (sRxResult.Equals("0")) {
				sResponse = "0";
			}

			string sResult = "0";
			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("LOG_SETTLE_RESULT");
				db.ProcedureInParm("PSID",DbSession.DbType.VARCHAR2,sSid);
				db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,sUserSeq);
				db.ProcedureInParm("PSETTLE_AMT",DbSession.DbType.NUMBER,iMoney);
				db.ProcedureInParm("PSETTLE_POINT",DbSession.DbType.NUMBER,0);
				db.ProcedureInParm("PRESPONSE",DbSession.DbType.VARCHAR2,sResponse);
				db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
				db.ExecuteProcedure();
				sResult = db.GetStringValue("PRESULT");
			}

			Response.ContentType = "text/plain";
			Response.Charset = "shift_jis";
			if (sResult.Equals("0")) {
				Response.Write("OK");
			} else {
				Response.Write("NG");
			}

		} catch (Exception) {
			Response.ContentType = "text/plain";
			Response.Charset = "shift_jis";
			Response.Write("EXCEPTION");
		}
		Response.End();
	}
}
