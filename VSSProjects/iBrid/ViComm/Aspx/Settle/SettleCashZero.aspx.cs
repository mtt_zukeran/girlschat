﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Settle
--	Title			: Zero 自動入金決済結果
--	Progaram ID		: SettleCashZero
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using iBridCommLib;
using ViComm;

public partial class SettleCashZero:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		try {
			string sClientip = iBridUtil.GetStringValue(Request.QueryString["clientip"]);
			string sMoney = iBridUtil.GetStringValue(Request.QueryString["money"]);
			string sEmail = iBridUtil.GetStringValue(Request.QueryString["email"]);
			string sUserSeq = iBridUtil.GetStringValue(Request.QueryString["sendid"]);
			string sSid = iBridUtil.GetStringValue(Request.QueryString["sendpoint"]);
			string sTel = iBridUtil.GetStringValue(Request.QueryString["telno"]);
			string sRxResult = iBridUtil.GetStringValue(Request.QueryString["payment"]);

			string sResponse = "9";
			if (sRxResult.Equals("01")) {
				sResponse = "0";
			}

			string sResult = "0";
			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("LOG_SETTLE_RESULT");
				db.ProcedureInParm("PSID",DbSession.DbType.VARCHAR2,sSid);
				db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,sUserSeq);
				db.ProcedureInParm("PSETTLE_AMT",DbSession.DbType.NUMBER,int.Parse(sMoney));
				db.ProcedureInParm("PSETTLE_POINT",DbSession.DbType.NUMBER,0);
				db.ProcedureInParm("PRESPONSE",DbSession.DbType.VARCHAR2,sResponse);
				db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
				db.ExecuteProcedure();
				sResult = db.GetStringValue("PRESULT");
			}
			Response.ContentType = "text/html";
			Response.Write("SuccessOK");

		} catch (Exception) {
			Response.ContentType = "text/html";
			Response.Write("SuccessOK");
		}
		Response.End();
	}
}
