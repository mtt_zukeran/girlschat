﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Settle
--	Title			: SSAD決済結果
--	Progaram ID		: SettleSSAD
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using iBridCommLib;
using ViComm;

public partial class SettleSSAD:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		try {
			string sSid = iBridUtil.GetStringValue(Request.QueryString["sendid"]);
			int iMoney = int.Parse(iBridUtil.GetStringValue(Request.QueryString["money"]));
			int iPoint = int.Parse(iBridUtil.GetStringValue(Request.QueryString["point"]));

			string sResult;
			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("LOG_SETTLE_RESULT");
				db.ProcedureInParm("PSID",DbSession.DbType.VARCHAR2,sSid);
				db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,"");
				db.ProcedureInParm("PSETTLE_AMT",DbSession.DbType.NUMBER,iMoney);
				db.ProcedureInParm("PSETTLE_POINT",DbSession.DbType.NUMBER,iPoint);
				db.ProcedureInParm("PRESPONSE",DbSession.DbType.VARCHAR2,"0");
				db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
				db.ExecuteProcedure();
				sResult = db.GetStringValue("PRESULT");
			}
			Response.ContentType = "text/plain";
			Response.Charset = "shift_jis";
			Response.Write("200:SucessOK\r\n");
			Response.Write(string.Format("clientip={0}\r\n",iBridUtil.GetStringValue(Request.QueryString["clientip"])));
			Response.Write(string.Format("auth_code={0}\r\n",iBridUtil.GetStringValue(Request.QueryString["auth_code"])));
			Response.Write(string.Format("telno={0}\r\n",iBridUtil.GetStringValue(Request.QueryString["telno"])));
			Response.Write(string.Format("email={0}\r\n",iBridUtil.GetStringValue(Request.QueryString["email"])));
			Response.Write(string.Format("sendid={0}\r\n",iBridUtil.GetStringValue(Request.QueryString["sendid"])));
			Response.Write(string.Format("money={0}\r\n",iBridUtil.GetStringValue(Request.QueryString["money"])));
			Response.Write(string.Format("point={0}\r\n",iBridUtil.GetStringValue(Request.QueryString["point"])));

			bool bNewSignal;

			using (NamedAutoResetEvent oSignal = new NamedAutoResetEvent(false,"signal-" + sSid,out bNewSignal)) {
				oSignal.Set();
			}

		} catch (Exception) {
			Response.ContentType = "text/plain";
			Response.Charset = "shift_jis";
			Response.Write("999:Error\r\n");
		}
		Response.End();
	}
}
