﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Settle
--	Title			: SMONEY決済結果
--	Progaram ID		: SettleSMoney
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using iBridCommLib;
using ViComm;

public partial class SettleSMoney:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		try {
			string sClientip = iBridUtil.GetStringValue(Request.QueryString["cid"]);
			string sMoney = iBridUtil.GetStringValue(Request.QueryString["ch"]);
			string sSid = iBridUtil.GetStringValue(Request.QueryString["sid"]);
			string sUserSeq = iBridUtil.GetStringValue(Request.QueryString["c1"]);
			string sRxResult = iBridUtil.GetStringValue(Request.QueryString["rst"]);

			string sResponse = "9";
			if (sRxResult.Equals("0")) {
				sResponse = "0";
			}

			string sResult = "0";
			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("LOG_SETTLE_RESULT");
				db.ProcedureInParm("PSID",DbSession.DbType.VARCHAR2,sSid);
				db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,sUserSeq);
				db.ProcedureInParm("PSETTLE_AMT",DbSession.DbType.NUMBER,int.Parse(sMoney));
				db.ProcedureInParm("PSETTLE_POINT",DbSession.DbType.NUMBER,0);
				db.ProcedureInParm("PRESPONSE",DbSession.DbType.VARCHAR2,sResponse);
				db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
				db.ExecuteProcedure();
				sResult = db.GetStringValue("PRESULT");
			}

			Response.ContentType = "text/plain";
			Response.Charset = "shift_jis";
			string sUrl = string.Format("rc={0}\r\nEND\r\n",sResult);
			Response.Write(sUrl);

		} catch (Exception) {
			Response.ContentType = "text/plain";
			Response.Charset = "shift_jis";
			string sUrl = string.Format("rc={0}\r\nEND\r\n","-2");
			Response.Write(sUrl);
		}
		Response.End();
	}
}
