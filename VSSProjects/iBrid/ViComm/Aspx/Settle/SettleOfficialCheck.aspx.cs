﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Settle
--	Title			: OFFICIAL-CHECK決済結果
--	Progaram ID		: SettleOfficialCheck
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using iBridCommLib;
using ViComm;

public partial class SettleOfficialCheck:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		try {
			string sMoney = iBridUtil.GetStringValue(Request.QueryString["p"]);
			string sU = iBridUtil.GetStringValue(Request.QueryString["u"]);
			string[] sInfo = sU.Split(':');

			if (sInfo.Length == 2) {
				string sResult = "0";
				using (DbSession db = new DbSession()) {
					db.PrepareProcedure("LOG_SETTLE_RESULT");
					db.ProcedureInParm("PSID",DbSession.DbType.VARCHAR2,sInfo[0]);
					db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,sInfo[1]);
					db.ProcedureInParm("PSETTLE_AMT",DbSession.DbType.NUMBER,int.Parse(sMoney));
					// ARCHE用暫定 1Pt = 1YENなのでＯＫ
					db.ProcedureInParm("PSETTLE_POINT",DbSession.DbType.NUMBER,int.Parse(sMoney));
					db.ProcedureInParm("PRESPONSE",DbSession.DbType.VARCHAR2,"0");
					db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
					db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
					db.ExecuteProcedure();
					sResult = db.GetStringValue("PRESULT");
				}
			}
			Response.ContentType = "text/plain";
			Response.Charset = "shift_jis";
			Response.Write("0");

		} catch (Exception) {
			Response.ContentType = "text/plain";
			Response.Charset = "shift_jis";
			Response.Write("-1");
		}
		Response.End();
	}
}
