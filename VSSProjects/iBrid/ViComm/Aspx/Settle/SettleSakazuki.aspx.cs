﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Settle
--	Title			: Sakazuki決済結果
--	Progaram ID		: SettleSakazuki
--
--  Creation Date	: 2010.02.05
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using iBridCommLib;
using ViComm;

public partial class SettleSakazuki:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		try {
			//現金ベースに変更
			string sPoint = iBridUtil.GetStringValue(Request.QueryString["point"]);
			string sMoney = iBridUtil.GetStringValue(Request.QueryString["commission"]);
			string sU = iBridUtil.GetStringValue(Request.QueryString["uid"]);
			string[] sInfo = sU.Split(':');

			int iMoney = 0;
			int.TryParse(sMoney,out iMoney);

			int iPoint = 0;
			if (sPoint.Equals(string.Empty)) {
				using (SettleLog oLog = new SettleLog()) {
					oLog.GetOne(sInfo[0]);
					iPoint = iMoney / oLog.pointPrice;
				}
			} else {
				int.TryParse(sPoint,out iPoint);
			}

			string sResult = "0";
			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("LOG_SETTLE_RESULT");
				db.ProcedureInParm("PSID",DbSession.DbType.VARCHAR2,sInfo[0]);
				db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,sInfo[1]);
				db.ProcedureInParm("PSETTLE_AMT",DbSession.DbType.NUMBER,iMoney);
				db.ProcedureInParm("PSETTLE_POINT",DbSession.DbType.NUMBER,iPoint);
				db.ProcedureInParm("PRESPONSE",DbSession.DbType.VARCHAR2,"0");
				db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
				db.ExecuteProcedure();
				sResult = db.GetStringValue("PRESULT");

			}
			Response.ContentType = "text/plain";
			Response.Charset = "shift_jis";
			Response.Write("success");

		} catch (Exception) {
			Response.ContentType = "text/plain";
			Response.Charset = "shift_jis";
			Response.Write("false");
		}
		Response.End();
	}
}
