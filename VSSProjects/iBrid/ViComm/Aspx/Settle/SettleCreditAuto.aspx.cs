﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Settle
--	Title			: クレジット自動決済
--	Progaram ID		: SettleCreditAuto
--
--  Creation Date	: 2016.06.08
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using iBridCommLib;
using ViComm;
using System.Data;
using System.Net;
using System.Net.Security;
using System.IO;
using System.Threading;
using System.Security.Cryptography.X509Certificates;

public class ZeroTxAutoThread {
	private string siteCd;
	private string procNo;

	public ZeroTxAutoThread(string pSiteCd,string pProcNo) {
		siteCd = pSiteCd;
		procNo = pProcNo;
	}

	public void StartTX() {
		Thread thread = new Thread(ThreadTrans);
		thread.Start();
	}

	private void ThreadTrans() {
		DataSet ds;
		using (SettleLog oSettleLog = new SettleLog()) {
			ds = oSettleLog.GetListAutoSettle(this.procNo);
		}

		string sContinueSettleUrl = "";
		string sCpIdNo = "";
		using (SiteSettle oSiteSettle = new SiteSettle()) {
			oSiteSettle.GetOne(this.siteCd,ViCommConst.SETTLE_CORP_ZERO,ViCommConst.SETTLE_CREDIT_PACK);
			sContinueSettleUrl = oSiteSettle.continueSettleUrl;
			sCpIdNo = oSiteSettle.cpIdNo;
		}
		DataRow dr;

		string sUserSeq;
		string sSettleAmt;
		string sEmailAddr;
		string sTel;
		string sSid;

		for (int i = 0;i < ds.Tables["T_SETTLE_LOG"].Rows.Count;i++) {
			dr = ds.Tables["T_SETTLE_LOG"].Rows[i];
			sUserSeq = dr["USER_SEQ"].ToString();
			sSettleAmt = dr["SETTLE_AMT"].ToString();
			sEmailAddr = dr["EMAIL_ADDR"].ToString();
			sTel = dr["TEL"].ToString();
			sSid = dr["SID"].ToString();

			string sSettleUrl = string.Format(sContinueSettleUrl,sCpIdNo,sEmailAddr,sUserSeq,sSid,sSettleAmt,sTel);

			if (TransQuick(sSettleUrl)) {
				using (SettleLog oLog = new SettleLog()) {
					oLog.LogSettleResult(sSid,sUserSeq,int.Parse(sSettleAmt),0,"0");
				}
			} else {
				using (SettleLog oLog = new SettleLog()) {
					oLog.LogSettleResult(sSid,sUserSeq,int.Parse(sSettleAmt),0,"9");
				}
			}
		}
	}

	public bool TransQuick(string pUrl) {
		try {
			ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(OnRemoteCertificateValidationCallback);
			
			WebRequest req = WebRequest.Create(pUrl);
			req.Timeout = 60000;
			WebResponse res = req.GetResponse();

			Stream st = res.GetResponseStream();
			using (StreamReader sr = new StreamReader(st)) {
				string sRes = sr.ReadToEnd();
				sr.Close();
				st.Close();
				return sRes.Equals("Success_order");
			}
		} catch (Exception e) {
			ViCommInterface.WriteIFError(e,"SettleCreditAuto",pUrl);
			return false;
		}
	}

	private bool OnRemoteCertificateValidationCallback(
	  Object sender,
	  X509Certificate certificate,
	  X509Chain chain,
	  SslPolicyErrors sslPolicyErrors
	) {
		return true;  // 「SSL証明書の使用は問題なし」と示す
	}
}

public partial class SettleCreditAuto:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		try {
			ZeroTxAutoThread oThread = new ZeroTxAutoThread(iBridUtil.GetStringValue(Request.QueryString["site"]),iBridUtil.GetStringValue(Request.QueryString["proc"]));
			oThread.StartTX();

			Response.ContentType = "text/html";
			Response.Write("0");
		} catch (Exception) {
			Response.ContentType = "text/html";
			Response.Write("-2");
		}
		Response.End();
	}
}