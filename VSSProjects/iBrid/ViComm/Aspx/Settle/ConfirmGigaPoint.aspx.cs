﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Settle
--	Title			: GIGA-POINT決済確認
--	Progaram ID		: ConfirmGigaPoint
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using iBridCommLib;
using System.Web;
using System.Text;
using ViComm;

public partial class ConfirmGigaPoint:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {

		try {
			string sResult = "9";
			string sMoney = iBridUtil.GetStringValue(Request.Form["settle_amount"]);
			string sSid = iBridUtil.GetStringValue(Request.Form["mall_order_no"]);
			string sStatus = iBridUtil.GetStringValue(Request.Form["status_code"]);
			string sUserSeq = "GIGAPOINT";

			ViCommInterface.WriteIFLog("GIGA-POINT CONFIRM",string.Format("sid={0} money={1} status={2}",sSid,sMoney,sStatus));

			if (sStatus.Equals("0")) {
				using (DbSession db = new DbSession()) {
					db.PrepareProcedure("CHECK_SETTLE_REQUEST");
					db.ProcedureInParm("PSID",DbSession.DbType.VARCHAR2,sSid);
					db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,sUserSeq);
					db.ProcedureInParm("PSETTLE_ATM",DbSession.DbType.NUMBER,int.Parse(sMoney));
					db.ProcedureOutParm("PLOGIN_ID",DbSession.DbType.VARCHAR2);
					db.ProcedureOutParm("PLOGIN_PASSWORD",DbSession.DbType.VARCHAR2);
					db.ProcedureOutParm("PSITE_URL",DbSession.DbType.VARCHAR2);
					db.ProcedureOutParm("PBACK_URL",DbSession.DbType.VARCHAR2);
					db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
					db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
					db.ExecuteProcedure();
					sResult = db.GetStringValue("PRESULT");
				}
			}

			Response.ContentType = "text/plain";
			Response.Charset = "shift_jis";
			if (sResult.Equals("0")) {
				Response.Write("OK");
			} else {
				Response.Write("NG");
			}

		} catch (Exception) {
			Response.ContentType = "text/plain";
			Response.Write("EXCEPTION");
		}
		Response.End();
	}
}
