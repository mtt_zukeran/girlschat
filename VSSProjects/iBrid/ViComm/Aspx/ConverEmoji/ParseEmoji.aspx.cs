/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �J�n����
--	Progaram ID		: ParseEmoji
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Configuration;
using System.Drawing;
using System.Web;
using System.Text.RegularExpressions;
using iBridCommLib;
using MobileLib;

public partial class ParseEmoji:System.Web.UI.MobileControls.MobilePage {

	protected void Page_Load(object sender,EventArgs e) {

		if (!IsPostBack) {
			string sCarrierType = iBridUtil.GetStringValue(Request.QueryString["carrier"]);
			string sDoc = Mobile.EmojiToCommTag(sCarrierType,iBridUtil.GetStringValue(Request.QueryString["doc"]));

			Response.ContentType = "text/plain";
			Response.Charset = "shift_jis";
			Response.Write("result=" + sDoc);
			Response.End();
		}
	}
}
