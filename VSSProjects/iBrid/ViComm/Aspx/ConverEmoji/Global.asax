﻿<%@ Application Language="C#" %>
<%@ Import Namespace="System.Diagnostics" %>

<script RunAt="server">

	void Application_Start(object sender,EventArgs e) {
	}

	void Application_End(object sender,EventArgs e) {
	}


	void Application_Error(object sender,EventArgs e) {
		Exception objLastErr = HttpContext.Current.Server.GetLastError();

		string sSource = "ViCOMM";
		string sLog = "Application";
		string err = "";

		if (objLastErr != null) {
			Exception objErr = objLastErr.GetBaseException();
			err = "ViCOMM User Error Caught in Application_Error event\n" +
				 "Error in: " + Request.Url.ToString() +
				 "\nError Message:" + objErr.Message.ToString() +
				 "\nStack Trace:" + objErr.StackTrace.ToString();
		} else {
			err = "NO ASPX ERROR";
		}

		if (!EventLog.SourceExists(sSource)) {
			EventLog.CreateEventSource(sSource,sLog);
		}

		EventLog.WriteEntry(sSource,err,EventLogEntryType.Error);
		Server.ClearError();
	}

	void Session_Start(object sender,EventArgs e) {
	}

	void Session_End(object sender,EventArgs e) {

	}
       
</script>

