using System;
using System.Collections.Generic;
using System.Collections.Specialized;

using System.Text;

namespace ViComm {

	/// <summary>
	/// URL文字列の生成機能を提供するBuilder
	/// </summary>
	public class UrlBuilder {
	
		private bool isUnicode = true;
		private string baseUrl;
		private IDictionary<string,string> parameters;
		
		public bool IsUnicode{
			get{return this.isUnicode;}
			set{this.isUnicode = value;}
		}
		
		public string BaseUrl{
			get{return this.baseUrl;}
		}		
		public IDictionary<string,string> Parameters{
			get{return this.parameters;}
		}
		
		
		/// <summary>
		/// コンストラクタ
		/// </summary>
		/// <param name="pBaseUrl">基本となるURL文字列</param>
		public UrlBuilder(string pBaseUrl) : this(pBaseUrl,new Dictionary<string,string>()){}
		/// <summary>
		/// コンストラクタ
		/// </summary>
		/// <param name="pBaseUrl">基本となるURL文字列</param>
		/// <param name="pParameters">パラメータ</param>
		public UrlBuilder(string pBaseUrl, NameValueCollection pParameters) :this(pBaseUrl,ViCommPrograms.ConvertToDictionary(pParameters)) {}
		/// <summary>
		/// コンストラクタ
		/// </summary>
		/// <param name="pBaseUrl">基本となるURL文字列</param>
		/// <param name="pParameters">パラメータ</param>
		public UrlBuilder(string pBaseUrl, IDictionary<string, string> pParameters) {
			this.baseUrl = pBaseUrl;
			this.parameters = pParameters;
		}

		public void AddParameter(string sKey,string sValue) {
			if(this.parameters.ContainsKey(sKey)){
				this.parameters.Remove(sKey);
			}
			this.parameters.Add(sKey,sValue);
		}
		public void RemoveParameter(string sKey) {
			if (this.parameters.ContainsKey(sKey)) {
				this.parameters.Remove(sKey);
			}
		}
		public void RemoveAddParameter(string sKey,string sValue) {
			this.RemoveParameter(sKey);
			this.AddParameter(sKey,sValue);
		}
		
		public override string ToString() {
			return this.ToString(false);
		}
		public string ToString(bool pWithUrlEncode) {
			StringBuilder oUrlStringBuilder = new StringBuilder(this.baseUrl);

			int iIndex = 0;

			foreach (string sParameterName in this.parameters.Keys) {
				if(string.IsNullOrEmpty(sParameterName)) continue;
				
				string sValue = this.parameters[sParameterName];
				if(string.IsNullOrEmpty(sValue)) continue;
				
				if(pWithUrlEncode) {
					if(this.isUnicode){
						sValue = System.Web.HttpUtility.UrlEncodeUnicode(sValue);
					}else{
						sValue = System.Web.HttpUtility.UrlEncode(sValue);
					}
				}
				oUrlStringBuilder.Append((iIndex == 0) ? "?" : "&");
				oUrlStringBuilder.AppendFormat("{0}={1}", sParameterName, sValue);
				iIndex += 1;
			}

			return oUrlStringBuilder.ToString();
		}	
	}
}
