using System;
using System.Collections.Generic;
using System.Text;

namespace ViComm.Extension.Pwild {
	public class PwViCommConst {
		public const int DATASET_CREDIT_AUTO_SETTLE = 40;
		public const int DATASET_RICHINO_RANK = 41;
		public const int DATASET_DEFECT_REPORT = 42;
		public const int DATASET_USER_TOP = 43;
		public const int DATASET_MAIL_LOTTERY = 44;
		public const int DATASET_LOGIN_PLAN = 45;
		public const int DATASET_ELECTION_ENTRY = 46;
		public const int DATASET_ELECTION_PERIOD = 47;
		public const int DATASET_BBS_BINGO = 48;
		public const int DATASET_GAME_NEWS = 49;
		public const int DATASET_EVENT = 50;
		public const int DATASET_WANTED_BOUNTY = 51;
		public const int DATASET_CAST_PIC = 52;
		public const int DATASET_WAIT_SCHEDULE = 53;
		public const int DATASET_PICKUP_OBJS_COMMENT = 54;
		public const int DATASET_YAKYUKEN_JYANKEN_PIC = 55;
		public const int DATASET_YAKYUKEN_PIC = 56;
		public const int DATASET_YAKYUKEN_COMMENT = 57;
		public const int DATASET_YAKYUKEN_GAME = 58;
		public const int DATASET_MAN_TWEET_LIKE = 59;
		public const int DATASET_COMMUNICATION = 60;
		public const int DATASET_MAN_TWEET_COMMENT_TERM = 61;
		public const int DATASET_MAN_TWEET_COMMENT_CNT = 62;
		public const int DATASET_MAN_TWEET = 63;
		public const int DATASET_MAN_TWEET_COMMENT = 64;
		public const int DATASET_CAST_TWEET = 65;
		public const int DATASET_CAST_TWEET_COMMENT = 66;
		public const int DATASET_CAST_PIC_ATTR_TYPE = 67;
		public const int DATASET_CAST_MOVIE_ATTR_TYPE = 68;
		public const int DATASET_BLOG_COMMENT = 69;
		public const int DATASET_GAME_QUEST_ENTRY_LOG = 70;
		public const int DATASET_GAME_LOTTERY = 84;
		public const int DATASET_FAVORIT_GROUP = 85;
		public const int DATASET_PRESENT_MAIL_ITEM = 86;
		public const int DATASET_GAME_QUEST = 87;
		public const int DATASET_REQUEST = 88;
		public const int DATASET_REQUEST_VALUE = 89;
		public const int DATASET_GAME_ITEM = 90;
		public const int DATASET_PARTNER_GAME_CHARACTER = 91;
		public const int DATASET_OBJ_REVIEW = 92;
		public const int DATASET_GAME_TREASURE = 93;
		public const int DATASET_GAME_TOWN = 94;
		public const int DATASET_GAME_DATE_PLAN = 95;
		public const int DATASET_GAME_PART_TIME_JOB_PLAN = 96;
		public const int DATASET_GAME_STAGE = 97;
		public const int DATASET_GAME_INTRO_LOG = 98;

		public const int INQUIRY_GAME_ITEM_SHOP_LIST = 2500;
		public const int INQUIRY_GAME_CHARACTER_BATTLE_LIST = 2501;
		public const int INQUIRY_GAME_CHARACTER_BATTLE_VIEW = 2502;
		public const int INQUIRY_GAME_ITEM_SHOP_VIEW = 2503;
		public const int INQUIRY_GAME_TREASURE_ATTR = 2504;
		public const int INQUIRY_GAME_TREASURE = 2505;
		public const int INQUIRY_GAME_TOWN = 2506;
		public const int INQUIRY_GAME_CHARACTER_FAVORITE_LIST = 2507;
		public const int INQUIRY_GAME_DATE_PLAN = 2508;
		public const int INQUIRY_GAME_PART_TIME_JOB_PLAN = 2509;
		public const int INQUIRY_GAME_CHARACTER_FELLOW_LIST = 2510;
		public const int INQUIRY_GAME_CHARACTER_FELLOW_APPLICATION_LIST = 2511;
		public const int INQUIRY_GAME_CHARACTER_FELLOW_HAS_APPLICATION_LIST = 2512;
		public const int INQUIRY_GAME_ITEM_VIEW = 2513;
		public const int INQUIRY_GAME_STAGE_BOSS_BATTLE = 2514;
		public const int INQUIRY_GAME_STAGE_BOSS_BATTLE_RESULT = 2515;
		public const int INQUIRY_GAME_CHARACTER_FIND_LIST = 2516;
		public const int INQUIRY_GAME_CHARACTER_BATTLE_LOSE_LIST = 2517;
		public const int INQUIRY_GAME_CHARACTER_BATTLE_COMMUNICATION_VIEW = 2518;
		public const int INQUIRY_GAME_CHARACTER_BATTLE_TREASURE_LIST = 2519;
		public const int INQUIRY_GAME_CHARACTER_FELLOW_MEMBER_LIST = 2520;
		public const int INQUIRY_GAME_STAGE = 2521;
		public const int INQUIRY_GAME_ITEM_POSSESSION_PARTNER_LIST = 2522;
		public const int INQUIRY_GAME_CHARACTER_VIEW = 2523;
		public const int INQUIRY_GAME_TREASURE_BATTLE_LIST = 2524;
		public const int INQUIRY_GAME_COMMUNICATION_PARTNER_HUG = 2525;
		public const int INQUIRY_GAME_COMP_CALENDAR = 2526;
		public const int INQUIRY_GAME_TREASURE_PARTNER = 2527;
		public const int INQUIRY_GAME_LOGIN_BONUS_MONTHLY_LIST = 2528;
		public const int INQUIRY_GAME_ITEM_SALE_LIST = 2529;
		public const int INQUIRY_GAME_ITEM_SALE_NOTICE_LIST = 2530;
		public const int INQUIRY_GAME_MOVIE = 2531;
		public const int INQUIRY_GAME_WANTED_ITEM = 2532;
		public const int INQUIRY_GAME_ITEM_WANTED = 2533;
		public const int INQUIRY_GAME_ITEM_POSSESSION_LIST = 2534;
		public const int INQUIRY_GAME_CHARACTER_DATE_FELLOW_LIST = 2535;
		public const int INQUIRY_GAME_RECOVERY_FORCE_FULL = 2536;
		public const int INQUIRY_GAME_DATE_PLAN_CHARGE = 2537;
		public const int INQUIRY_GAME_BATTLE_START = 2538;
		public const int INQUIRY_GAME_CAST_MOVIE_LIST = 2539;
		public const int INQUIRY_GAME_BATTLE_START_PARTY = 2540;
		public const int INQUIRY_GAME_BATTLE_START_SUPPORT = 2541;
		public const int INQUIRY_GAME_BATTLE_RESULT_WIN = 2542;
		public const int INQUIRY_GAME_BATTLE_RESULT_WIN_PARTY = 2543;
		public const int INQUIRY_GAME_BATTLE_RESULT_WIN_SUPPORT = 2544;
		public const int INQUIRY_GAME_BATTLE_RESULT_LOSE = 2545;
		public const int INQUIRY_GAME_BATTLE_RESULT_LOSE_PARTY = 2546;
		public const int INQUIRY_GAME_BATTLE_RESULT_LOSE_SUPPORT = 2547;
		public const int INQUIRY_GAME_CAST_MOVIE_VIEW = 2548;
		public const int INQUIRY_GAME_CAST_MOVIE_PLAY = 2549;
		public const int INQUIRY_GAME_SUPPORT_REQUEST = 2550;
		public const int INQUIRY_GAME_FELLOW_LOG = 2551;
		public const int INQUIRY_GAME_CHARACTER_FELLOW_PRESENT_LIST = 2552;
		public const int INQUIRY_GAME_LOTTERY = 2553;
		public const int INQUIRY_GAME_LOTTERY_GET_ITEM_RESULT = 2554;
		public const int INQUIRY_GAME_LOTTERY_GET_ITEM_LIST = 2555;
		public const int INQUIRY_GAME_LOTTERY_GET_ITEM_VIEW = 2556;
		public const int INQUIRY_GAME_ITEM_SHOP_PRESENT = 2557;
		public const int INQUIRY_GAME_ITEM_POSSESSION_PRESENT = 2558;
		public const int INQUIRY_GAME_CAST_PRESENT_HISTORY = 2559;
		public const int INQUIRY_GAME_BATTLE_LOG = 2560;
		public const int INQUIRY_GAME_CAST_TODAY = 2561;
		public const int INQUIRY_GAME_CHARACTER_RANK_BATTLE = 2562;

		public const int INQUIRY_GAME_FAVORITE_LOG = 2564;
		public const int INQUIRY_GAME_CHARACTER_RANK_COMP = 2565;

		public const int INQUIRY_GAME_CHARACTER_RANK_PAYMENT = 2567;
		public const int INQUIRY_GAME_MAN_PRESENT_HISTORY = 2568;
		public const int INQUIRY_GAME_TREASURE_VIEW = 2569;
		public const int INQUIRY_GAME_CLEAR_MISSION_RECOVERY_FORCE = 2570;
		public const int INQUIRY_GAME_CHARACTER_BATTLE_TUTORIAL = 2571;
		public const int INQUIRY_GAME_CHARACTER_NEW = 2572;
		public const int INQUIRY_GAME_CHARACTER_TREASURE_MOSAIC_ERASE = 2573;
		public const int INQUIRY_GAME_TREASURE_MANAGEMENT = 2574;
		public const int INQUIRY_GAME_CAST_MOVIE_BUY = 2575;
		public const int INQUIRY_GAME_COMMUNICATION = 2576;
		public const int INQUIRY_GAME_WANTED_ITEM_PARTNER = 2577;
		public const int INQUIRY_GAME_INTRO_LOG = 2578;
		public const int INQUIRY_GAME_CLEAR_TOWN_USED_ITEM_SHORTAGE_LIST = 2579;
		public const int INQUIRY_GAME_CHARACTER_MOVIE_LIST = 2580;
		public const int INQUIRY_GAME_CHARACTER_FAVORITE_DATE_LIST = 2581;
		public const int INQUIRY_GAME_ITEM_ONCE_DAY_PRESENT = 2582;
		public const int INQUIRY_GAME_CHARACTER_FAVORITE_PRESENT = 2583;
		public const int INQUIRY_GAME_TREASURE_BATTLE_STAGE_GROUP_LIST = 2584;
		public const int INQUIRY_OBJ_REVIEW_HISTORY = 2585;
		public const int INQUIRY_GAME_LOTTERY_NOT_COMP = 2586;
		public const int INQUIRY_GAME_LOTTERY_GET_ITEM_RESULT_NOT_COMP = 2587;
		public const int INQUIRY_REQUEST = 2588;
		public const int INQUIRY_REQUEST_VALUE = 2589;
		public const int INQUIRY_YESTERDAY_CAST_REPORT = 2590;
		public const int INQUIRY_GAME_POSSESSION_MAN_TREASURE = 2591;
		public const int INQUIRY_GAME_LEVEL_QUEST = 2592;
		public const int INQUIRY_GAME_OTHER_QUEST = 2593;
		public const int INQUIRY_GAME_REGIST_QUEST_ENTRY = 2594;
		public const int INQUIRY_GAME_QUEST_REWARD_GET = 2595;
		public const int INQUIRY_GAME_POSSESSION_MAN_TREASURE_BATTLE = 2596;
		public const int INQUIRY_FAVORIT_GROUP = 2597;
		public const int INQUIRY_GAME_PAYMENT_LOG_VIEW = 2598;
		public const int INQUIRY_GAME_DATE_LOG = 2599;
		public const int INQUIRY_GAME_CHARACTER_REGULAR = 2600;
		public const int INQUIRY_GAME_PAYMENT_LOG_DETAIL = 2601;
		public const int INQUIRY_GAME_JEALOUSY_MAINTE = 2602;






		public const int INQUIRY_MAIL_USER_RX = 2609;
		public const int INQUIRY_MAIL_USER_TX = 2610;
		public const int INQUIRY_BLOG_COMMENT = 2611;
		public const int INQUIRY_BLOG_NG_USER = 2612;
		public const int INQUIRY_BLOG_PIC_NON_USED = 2613;
		public const int INQUILY_BLOG_MOVIE_NON_USED = 2614;


		public const int INQUIRY_SELF_MAN_TWEET = 2617;
		public const int INQUIRY_MAN_TWEET_COMMENT = 2618;
		public const int INQUIRY_SELF_CAST_TWEET = 2619;
		public const int INQUIRY_CAST_TWEET_COMMENT = 2620;
		public const int INQUIRY_MAN_TWEET = 2621;
		public const int INQUIRY_CAST_TWEET = 2622;
		public const int INQUIRY_FANCLUB_STATUS = 2623;
		public const int INQUIRY_CAST_NEWS = 2624;
		public const int INQUIRY_MAN_TWEET_LIST = 2625;
		public const int INQUIRY_MAN_TWEET_COMMENT_TERM = 2626;
		public const int INQUIRY_MAN_TWEET_COMMENT_CNT = 2627;
		public const int INQUIRY_MAIL_ATTACK = 2628;
		public const int INQUIRY_MAIL_ATTACK_SUB = 2629;
		public const int INQUIRY_MAN_TWEET_LIKE = 2630;
		public const int INQUIRY_CAST_TWEET_LIKE = 2631;
		public const int INQUIRY_PICKUP_OBJS_COMMENT = 2632;
		public const int INQUIRY_YAKYUKEN_GAME = 2633;
		public const int INQUIRY_YAKYUKEN_COMMENT = 2634;
		public const int INQUIRY_YAKYUKEN_PIC = 2635;
		public const int INQUIRY_YAKYUKEN_PIC_VIEW = 2636;
		public const int INQUIRY_YAKYUKEN_PIC_PREVIEW = 2637;

		public const int INQUIRY_WANTED_CAUGHT_LOG = 2639;
		public const int INQUIRY_EVENT = 2640;
		public const int INQUIRY_SELF_MARKING = 2641;
		public const int INQUIRY_DRAFT_MAIL = 2642;
		public const int INQUIRY_MAN_FAVORIT_NEWS = 2643;
		public const int INQUIRY_CAST_FAVORIT_NEWS = 2644;
		public const int INQUIRY_OBJ_REVIEW_HISTORY_PIC = 2645;
		public const int INQUIRY_OBJ_REVIEW_HISTORY_MOVIE = 2646;
		public const int INQUIRY_VOTE_RANK_CURRENT = 2647;
		public const int INQUIRY_VOTE_TERM_VOTABLE = 2648;
		public const int INQUIRY_BBS_BINGO_TERM_LATEST = 2649;
		public const int INQUIRY_BBS_BINGO_LOG = 2650;
		public const int INQUIRY_BBS_BINGO_ENTRY_PRIZE = 2651;
		public const int INQUIRY_ELECTION_ENTRY_CURRENT = 2652;
		public const int INQUIRY_ELECTION_ENTRY_PAST = 2653;
		public const int INQUIRY_ELECTION_PERIOD = 2654;
		public const int INQUIRY_ELECTOIN_PERIOD_VOTABLE = 2655;
		public const int INQUIRY_SELF_CAST_MOVIE_LIKE = 2656;
		public const int INQUIRY_CAST_PROFILE_MOVIE_LIKE_WEEKLY = 2657;
		public const int INQUIRY_CAST_PROFILE_MOVIE_LIKE_MONTHLY = 2658;
		public const int INQUIRY_SELF_CAST_PIC_LIKE = 2659;
		public const int INQUIRY_CAST_PROFILE_PIC_LIKE_WEEKLY = 2660;
		public const int INQUIRY_CAST_PROFILE_PIC_LIKE_MONTHLY = 2661;
		public const int INQUIRY_CAST_MOVIE_VIEW = 2662;
		public const int INQUIRY_CAST_MOVIE_COMMENT = 2663;
		public const int INQUIRY_SELF_CAST_MOVIE_COMMENT = 2664;
		public const int INQUIRY_CAST_PIC_VIEW = 2665;
		public const int INQUIRY_CAST_PIC_COMMENT = 2666;
		public const int INQUIRY_SELF_CAST_PIC_COMMENT = 2667;
		public const int INQUIRY_BLOG_RANKING = 2668;
		public const int INQUIRY_BLOG_ARTICLE_LIKE = 2669;
		public const int INQUIRY_PERFORMANCE_DAILY = 2670;
		public const int INQUIRY_WAIT_SCHEDULE = 2671;
		public const int INQUIRY_PRESENT_MAIL_ITEM = 2672;
		public const int INQUIRY_MAIL_REQUEST_LOG = 2673;
		public const int INQUIRY_LOGIN_PLAN = 2674;
		public const int INQUIRY_MAIL_LOTTERY_TICKET_COUNT = 2675;
		public const int INQUIRY_NEIGHBOR_USER = 2676;
		public const int INQUIRY_INVESTIGATE_SCHEDULE = 2677;
		public const int INQUIRY_INVESTIGATE_TARGET = 2678;
		public const int INQUIRY_CAST_CAN_MAIL_REQUEST = 2679;
		public const int INQUIRY_CAST_DIARY_EX = 2680;
		public const int INQUIRY_USER_TOP = 2681;
		public const int INQUIRY_DEFECT_REPORT = 2682;
		public const int INQUIRY_DEFECT_REPORT_PERSONAL = 2683;
		public const int INQUIRY_CAST_MAIL_PIC_STOCK = 2684;
		public const int INQUIRY_CAST_MAIL_MOVIE_STOCK = 2685;
		public const int INQUIRY_MAIL_ATTACK_LOGIN = 2686;
		public const int INQUIRY_MAIL_ATTACK_NEW = 2687;
		public const int INQUIRY_MAN_LOGINED_MAIL_SEND = 2688;
		public const int INQUIRY_CREDIT_AUTO_SETTLE = 2689;
		public const int INQUIRY_CREDIT_AUTO_SETTLE_STATUS = 2690;
		/// <summary>アタックリスト（3分以内メール返信対象の男性一覧）</summary>
		public const int INQUIRY_MAIL_ATTACK_LIMIT2_MAIN = 2691;
		/// <summary>アタックリスト（10分以内メール返信対象の男性一覧）</summary>
		public const int INQUIRY_MAIL_ATTACK_LIMIT2_SUB = 2692;
		/// <summary>いいねした画像一覧(男性用)</summary>
		public const int INQUIRY_CAST_PIC_MY_LIKE = 2693;
		/// <summary>いいねした動画一覧(男性用)</summary>
		public const int INQUIRY_CAST_MOVIE_MY_LIKE = 2694;
		/// <summary>出演者画像新着いいね通知</summary>
		public const int INQUIRY_CAST_LIKE_NOTICE_PIC = 2695;
		/// <summary>出演者動画新着いいね通知</summary>
		public const int INQUIRY_CAST_LIKE_NOTICE_MOVIE = 2696;
		/// <summary>清算報酬(ギフト券)一覧</summary>
		public const uint INQUIRY_EXTENSION_PAYMENT_GIFT = 2697;
		/// <summary>清算報酬(ギフト券)枚数選択</summary>
		public const uint INQUIRY_EXTENSION_PAYMENT_GIFT_SELECT = 2698;
		/// <summary>清算報酬(ギフト券)交換確認</summary>
		public const uint INQUIRY_EXTENSION_PAYMENT_GIFT_CONFIRM = 2699;
		/// <summary>清算報酬(ギフト券)交換完了</summary>
		public const uint INQUIRY_EXTENSION_PAYMENT_GIFT_COMPLETE = 2700;
		/// <summary>ギフト券交換履歴</summary>
		public const uint INQUIRY_EXTENSION_PAYMENT_HISTORY_GIFT = 2701;
		/// <summary>ギフト券交換履歴詳細</summary>
		public const uint INQUIRY_EXTENSION_PAYMENT_HISTORY_GIFT_DTL = 2702;

		/// <summary>女性仮登録完了（身分証送信）</summary>
		public const string SCR_CAST_SEND_ID_MAIL = "634";

		/// <summary>お宝レビュー書込エラー（未閲覧）</summary>
		public const string SCR_MAN_WRITE_OBJ_REVIEW_ERROR01 = "1030";
		/// <summary>お宝レビュー書込エラー（レビュー済）</summary>
		public const string SCR_MAN_WRITE_OBJ_REVIEW_ERROR02 = "1031";
		/// <summary>お宝レビュー書込エラー（リッチーノ閲覧）</summary>
		public const string SCR_MAN_WRITE_OBJ_REVIEW_ERROR03 = "1032";
		/// <summary>お宝レビューコメント削除完了</summary>
		public const string SCR_CAST_DELETE_OBJ_REVIEW_COMMENT_COMPLETE = "1033";
		/// <summary>男性用エラーページ</summary>
		public const string SCR_MAN_ERROR = "1034";
		/// <summary>女性用エラーページ</summary>
		public const string SCR_CAST_ERROR = "1035";
		/// <summary>男性用おねがい投稿完了ページ</summary>
		public const string SCR_MAN_WRITE_REQUEST_COMPLETE = "1050";
		/// <summary>女性用おねがい投稿完了ページ</summary>
		public const string SCR_CAST_WRITE_REQUEST_COMPLETE = "1051";
		/// <summary>男性用お気に入りグループ設定完了</summary>
		public const string SCR_MAN_SET_FAVORIT_GROUP_COMPLETE = "1077";
		/// <summary>女性用お気に入りグループ設定完了</summary>
		public const string SCR_CAST_SET_FAVORIT_GROUP_COMPLETE = "1078";
		/// <summary>自分がヤキモチ設定しています</summary>
		public const string SCR_JEAROUSY_MAN = "1096";
		/// <summary>アイドルつぶやき削除完了</summary>
		public const string SCR_CAST_DELETE_TWEET_COMPLETE = "1503";
		/// <summary>会員つぶやき削除完了</summary>
		public const string SCR_MAN_DELETE_TWEET_COMPLETE = "1504";
		/// <summary>アイドルつぶやきコメント削除完了</summary>
		public const string SCR_CAST_DELETE_TWEET_COMMENT_COMPLETE = "1505";
		/// <summary>会員つぶやきコメント削除完了</summary>
		public const string SCR_MAN_DELETE_TWEET_COMMENT_COMPLETE = "1506";
		/// <summary>ファンクラブ入会QUICK決済成功</summary>
		public const string SCR_MAN_FC_QUICK_OK = "1507";
		/// <summary>ファンクラブ入会QUICK決済失敗</summary>
		public const string SCR_MAN_FC_QUICK_NG = "1508";
		/// <summary>【ｱｲﾄﾞﾙ】ファンクラブ拒否できないページ</summary>
		public const string SCR_CAST_REFUSE_NG_FANCLUB_MEMBER = "1513";
		/// <summary>【会員】ファンクラブ拒否できないページ</summary>
		public const string SCR_MAN_REFUSE_NG_FANCLUB_MEMBER = "1514";
		/// <summary>自分がヤキモチ設定しています(つぶやきイイネ)</summary>
		public const string SCR_JEAROUSY_MAN_TWEET_LIKE = "1519";
		/// <summary>女性用 野球拳画像投稿完了</summary>
		public const string SCR_CAST_REGIST_YAKYUKEN_PIC_TMP_COMPLETE = "1520";
		/// <summary>女性用 野球拳画像修正完了</summary>
		public const string SCR_CAST_MODIFY_YAKYUKEN_PIC_COMPLETE = "1521";
		/// <summary>女性用 メールdeガチャ完了</summary>
		public const string SCR_CAST_MAIL_LOTTERY_COMPLETE = "1522";
		/// <summary>ピックアップオブジェ投票エラー</summary>
		public const string SCR_MAN_PICKUP_OBJS_VOTE_ERROR = "1523";
		/// <summary>この娘を探せエントリーエラー</summary>
		public const string SCR_MAN_WANTED_ENTRANT_ERROR = "1524";
		/// <summary>この娘を探せ月間山分けポイント付与結果</summary>
		public const string SCR_MAN_WANTED_MONTHLY_POINT_ACQUIRED_RESULT = "1526";
		/// <summary>お宝無料開放期間終了</summary>
		public const string SCR_MAN_BBS_OBJ_SPECIAL_FREE_END = "1527";
		/// <summary>男性用 メールdeガチャ完了</summary>
		public const string SCR_MAN_MAIL_LOTTERY_COMPLETE = "1528";
		/// <summary>【会員】メール履歴削除完了</summary>
		public const string SCR_MAN_DELETED_MAIL_HISTORY = "1537";
		/// <summary>【出演者】メール履歴削除完了</summary>
		public const string SCR_CAST_DELETED_MAIL_HISTORY = "1538";
		/// <summary>【会員】投票完了</summary>
		public const string SCR_MAN_REGIST_VOTE_COMPLETE = "1548";
		/// <summary>【会員】投票券追加完了</summary>
		public const string SCR_MAN_ADD_VOTE_TICKET_COMPLETE = "1549";
		/// <summary>【会員】お宝deビンゴ賞金獲得完了</summary>
		public const string SCR_MAN_GET_BBS_BINGO_PRIZE_COMPLETE = "1553";
		/// <summary>【会員】ネット投票(エントリー制)投票完了</summary>
		public const string SCR_MAN_REGIST_ELECTION_VOTE_COMPLETE = "1554";
		/// <summary>【会員】ネット投票(エントリー制)投票権追加</summary>
		public const string SCR_MAN_ADD_ELECTION_VOTE_TICKET_COMPLETE = "1555";
		/// <summary>【出演者】ネット投票(エントリー制)エントリー完了</summary>
		public const string SCR_CAST_REGIST_ELECTION_ENTRY_COMPLETE = "1556";
		/// <summary>【会員】コメント制限中</summary>
		public const string SCR_MAN_COMMENT_NG = "1563";
		/// <summary>【出演者】ネット投票(エントリー制)エントリー失敗(プロフ画像なし)</summary>
		public const string SCR_CAST_ELECTION_ENTRY_NG_NO_PROFILE_PIC = "1564";
		/// <summary>【会員】男性会員PC登録済み</summary>
		public const string SCR_REGIST_DONE_USER_BY_PC = "1569";
		/// <summary>【会員】ビデオ通話アプリ未登録エラー</summary>
		public const string SCR_MAN_TALKAPP_UNREGISTERED = "1620";
		/// <summary>【出演者】ビデオ通話アプリ未登録エラー</summary>
		public const string SCR_CAST_TALKAPP_UNREGISTERD = "1621";
		/// <summary>【会員】オープンIDでの会員登録・メールアドレス重複エラー</summary>
		public const string SCR_MAN_EMAIL_ADDR_DUPLICATION = "1622";
		/// <summary>【会員】クレジットQUICK(自動決済) OK</summary>
		public const string SCR_CREDIT_QUICK_AUTO_OK = "1623";
		/// <summary>【会員】クレジットQUICK(自動決済) NG</summary>
		public const string SCR_CREDIT_QUICK_AUTO_NG = "1624";
		/// <summary>【出演者】メール返信ボーナスポイント付与完了</summary>
		public const string SCR_CAST_MAIL_RES_BONUS_COMPLETE = "1625";

		public const string SCR_MAN_GAME_CHAR_TYPE_SELECT = "8001";				//男性ｲｹﾒﾝﾀｲﾌﾟ選択
		public const string SCR_MAN_GAME_CHAR_TYPE_COMPLETE = "8002";			//男性ｲｹﾒﾝﾀｲﾌﾟ選択完了
		public const string SCR_CAST_GAME_CHAR_TYPE_SELECT = "8003";			//出演者美人ﾀｲﾌﾟ選択完了
		public const string SCR_CAST_GAME_CHAR_TYPE_COMPLETE = "8004";			//出演者美人ﾀｲﾌﾟ選択完了
		public const string SCR_CAST_PIC_GAME_MAILER_COMPLITE = "8005";			//出演者ｹﾞｰﾑ画像ｱｯﾌﾟﾛｰﾄﾞ 
		public const string SCR_CAST_MOVIE_GAME_MAILER_COMPLITE = "8006";		//出演者ｹﾞｰﾑ動画ｱｯﾌﾟﾛｰﾄﾞ 
		public const string SCR_MAN_GAME_ITEM_BUY_COMPLETE = "8007";
		public const string SCR_MAN_GAME_ERROR = "8008";
		public const string SCR_WOMAN_GAME_ERROR = "8009";
		public const string SCR_WOMAN_GAME_ITEM_BUY_COMPLETE = "8010";
		public const string SCR_CAST_GAME_PART_TIME_JOB_RESULT = "8011";
		//8012
		public const string SCR_MAN_GAME_FAVORITE_COMPLETE = "8013";
		//8014
		public const string SCR_MAN_GAME_APPLY_FELLOW_RESULT = "8015";
		public const string SCR_MAN_GAME_CANCEL_APPLY_FELLOW_RESULT = "8016";
		public const string SCR_MAN_GAME_REMOVE_FELLOW_RESULT = "8017";
		public const string SCR_MAN_GAME_ITEM_SALE_CLOSE = "8018";
		public const string SCR_MAN_GAME_ACCEPT_APPLICATION_FELLOW_RESULT = "8019";
		public const string SCR_MAN_GAME_RECOVERY_FORCE_FULL_COMPLETE = "8020";
		public const string SCR_MAN_GAME_WANTED_ITEM_MAINTE_RESULT = "8021";
		public const string SCR_MAN_GAME_RANKING = "8022";
		public const string SCR_MAN_GAME_BATTLE_NO_FINISH = "8023";
		public const string SCR_MAN_PRESENT_GAME_ITEM_INJUSTICE = "8024";
		public const string SCR_MAN_GAME_RECOVERY_MISSION_FORCE_FULL_COMPLETE = "8025";
		public const string SCR_WOMAN_GAME_RECOVERY_FORCE_FULL_COMPLETE = "8026";
		public const string SCR_WOMAN_GAME_RECOVERY_MISSION_FORCE_FULL_COMPLETE = "8027";
		public const string SCR_WOMAN_GAME_ITEM_SALE_CLOSE = "8028";
		public const string SCR_WOMAN_GAME_BATTLE_NO_FINISH = "8029";
		public const string SCR_WOMAN_GAME_FAVORITE_COMPLETE = "8030";
		public const string SCR_WOMAN_GAME_CANCEL_APPLY_FELLOW_RESULT = "8031";
		public const string SCR_WOMAN_GAME_REMOVE_FELLOW_RESULT = "8032";
		public const string SCR_WOMAN_GAME_APPLY_FELLOW_RESULT = "8033";
		public const string SCR_MAN_GAME_NO_POINT = "8034";
		public const string SCR_WOMAN_GAME_NO_POINT = "8035";
		public const string SCR_WOMAN_GAME_ACCEPT_APPLICATION_FELLOW_RESULT = "8036";
		public const string SCR_GAME_PIC_UPDATE_COMPLETE = "8037";
		public const string SCR_GAME_MOVIE_UPDATE_COMPLETE = "8038";
		public const string SCR_MAN_GAME_BOSS_BATTLE_NO_FINISH = "8039";
		public const string SCR_WOMAN_GAME_BOSS_BATTLE_NO_FINISH = "8040";
		
		public const string SCR_WOMAN_GAME_WANTED_ITEM_MAINTE_RESULT = "8043";
		public const string SCR_GAME_TREASURE_CONTRIBUTION = "8044";
		public const string SCR_MAN_GAME_SET_PARTY_MEMBER_ERR = "8045";
		public const string SCR_WOMAN_GAME_SET_PARTY_MEMBER_ERR = "8046";
		public const string SCR_WOMAN_GAME_RANKING = "8047";
		public const string SCR_MAN_RECOVERY_FORCE_COMP = "8048";
		public const string SCR_WOMAN_RECOVERY_FORCE_COMP = "8049";
		public const string SCR_FRIEND_INTRODUCTION = "8050";
		public const string SCR_GAME_FLASH_MOKUHYOU_SP = "8051";
		public const string SCR_GAME_FLASH_GET_MAN_TREASURE_SP = "8052";
		public const string SCR_GAME_MAN_FLASH_COMPLETE_SP = "8053";
		public const string SCR_GAME_FLASH_LEVELUP_SP = "8054";
		public const string SCR_GAME_FLASH_BATTLE_SP = "8055";
		public const string SCR_GAME_FLASH_PARTY_BATTLE_SP = "8056";
		public const string SCR_GAME_FLASH_SUPPORT_BATTLE_SP = "8057";
		public const string SCR_GAME_FLASH_BOSS_BATTLE_SP = "8058";
		public const string SCR_GAME_FLASH_LOTTERY_SP = "8059";
		public const string SCR_GAME_WOMAN_FLASH_COMPLETE_SP = "8060";
		public const string SCR_MAN_GAME_BUY_USED_ITEM_COMPLETE = "8061";
		public const string SCR_WOMAN_GAME_BUY_USED_ITEM_COMPLETE = "8062";
		public const string SCR_MAN_GAME_BUY_USED_ITEM_FAIL_NO_MONEY = "8063";
		public const string SCR_WOMAN_GAME_BUY_USED_ITEM_FAIL_NO_MONEY = "8064";
		public const string SCR_MAN_GAME_MODIFY_TEL_COMPLETE = "8065";
		public const string SCR_WOMAN_ID_PIC_MAIL = "8206"; //ゲーム用女性身分証送信ページ
		public const string SCR_MAN_GAME_NON_USER_TOP_FOR_CHAT_USER = "8106";
		public const string SCR_WOMAN_GAME_NON_USER_TOP_FOR_CHAT_USER = "8107";
		public const string SCR_WOMAN_GAME_ONCE_DAY_PRESENT_RESULT = "8066";
		public const string SCR_MAN_GAME_SESSION_TIME_OUT = "8067";
		public const string SCR_WOMAN_GAME_SESSION_TIME_OUT = "8068";
		public const string SCR_WOMAN_LIVE_CHAT_NO_APPLY_LIVE_CHAT_ACCESS = "8069";	// 女性 ﾗｲﾌﾞﾁｬｯﾄ認証がされていない
		public const string SCR_MAN_LIVE_CHAT_NO_APPLY_LIVE_CHAT_ACCESS = "8070";	// 男性 ﾗｲﾌﾞﾁｬｯﾄ登録がされていない
		public const string SCR_GAME_MAN_QUEST_ENTRY_RESULT = "8071";	// 美女コレ 男性 クエストエントリー結果
		public const string SCR_GAME_WOMAN_QUEST_ENTRY_RESULT = "8072";	// 美女コレ 女性 クスストエントリー結果
		public const string SCR_GAME_MAN_QUEST_REWARD_GET_RESULT = "8073";		// 美女コレ 男性 クエストクリア報酬受取結果
		public const string SCR_GAME_WOMAN_QUEST_REWARD_GET_RESULT = "8074";	// 美女コレ 女性 クエストクリア報酬受取結果
		public const string SCR_GAME_FLASH_TICKET_LOTTERY_SP = "8075";
		public const string SCR_GAME_MAN_TUTORIAL_EXPLAIN_TREASURE = "8076";	// 美女コレ 男性 チュートリアル説明その1
		public const string SCR_GAME_MAN_TUTORIAL_EXPLAIN_BATTLE = "8077";		// 美女コレ 男性 チュートリアル説明その2
		public const string SCR_GAME_WOMAN_TUTORIAL_EXPLAIN_LEVEL_UP = "8078";	// 美女コレ 女性 チュートリアル説明その1
		public const string SCR_GAME_WOMAN_TUTORIAL_EXPLAIN_BATTLE = "8079";	// 美女コレ 女性 チュートリアル説明その2
		public const string SCR_GAME_MAN_REFUSE_PARTNER = "8080";	// 美女コレ 男性 自分が相手を拒否している場合にリダイレクトさせる
		public const string SCR_GAME_MAN_REFUSED_BY_PARTNER = "8081";	// 美女コレ 男性 自分が相手から拒否されている場合にリダイレクトさせる
		public const string SCR_GAME_WOMAN_REFUSE_PARTNER = "8082";	// 美女コレ 女性 自分が相手を拒否している場合にリダイレクトさせる
		public const string SCR_GAME_WOMAN_REFUSED_BY_PARTNER = "8083";	//美女コレ 女性 自分が相手から拒否されている場合にリダイレクトさせる
		public const string SCR_GAME_MAN_ADD_POINT_SELECT = "8124";	// 美女コレ 男性 ポイント購入選択
		public const string SCR_GAME_MAN_ADD_POINT_MILEAGE = "8125";	//美女コレ 男性 マイレージページ
		public const string SCR_GAME_WOMAN_MODIFY_GAME_HANDLE_NM_COMPLETED = "8126";	//源氏名修正後

		/// <summary>
		/// サイトCD
		/// </summary>
		public const string MAIN_SITE_CD = "A001";
		
		/// <summary>
		/// ｹﾞｰﾑｷｬﾗｸﾀｰ種類
		/// </summary>
		public class GameCharacterType {
			/// <summary>ｶﾞｯﾁﾘ系</summary>
			public const string TYPE01 = "1";
			/// <summary>ﾓﾃﾞﾙ系</summary>
			public const string TYPE02 = "2";
			/// <summary>知的系</summary>
			public const string TYPE03 = "3";
		}

		/// <summary>
		/// 女性ｹﾞｰﾑｷｬﾗｸﾀｰ種類
		/// </summary>
		public class WomanGameCharacterType
		{
			/// <summary>小悪魔系</summary>
			public const string TYPE01 = "1";
			/// <summary>癒し系</summary>
			public const string TYPE02 = "2";
			/// <summary>ｷｬﾘｱｳｰﾏﾝ系</summary>
			public const string TYPE03 = "3";
		}
		
		public class SetupGameCharacterType {
			public const string RESULT_OK = "1";
			public const string RESULT_NG = "9";
		}

		/// <summary>
		/// ｹﾞｰﾑｷｬﾗｸﾀｰ作成状態
		/// </summary>
		public class GameCharCreate {
			public const string GAME_CHAR_CREATE_RESULT_OK = "1";
			public const string GAME_CHAR_CREATE_RESULT_NG = "9";
		}
		
		/// <summary>
		/// ｹﾞｰﾑｱｲﾃﾑ入手種別
		/// </summary>
		public class GameItemGetCd {
			public const string CHARGE = "1";
			public const string NOT_CHARGE = "2";
		}
		
		/// <summary>
		/// ｹﾞｰﾑｷｬﾗｸﾀｰ検索用定数
		/// </summary>
		public class GameCharSeek {
			public const string LEVEL_RANGE = "2";
		}

		/// <summary>
		/// ｹﾞｰﾑｷｬﾗｸﾀｰ仲間申請状態
		/// </summary>
		public class GameCharFellowApplicationCd {
			public const string APPLICATION = "1";
			public const string HAS_APPLICATION = "2";
			public const string FELLOW = "3";
			public const string REFUSED = "4";
		}

		/// <summary>
		/// ｹﾞｰﾑｱｲﾃﾑｶﾃｺﾞﾘｰ
		/// </summary>
		public class GameItemCategory {
			/// <summary>ｱｸｾｻﾘｰ</summary>
			public const string ITEM_CATEGORY_ACCESSORY = "1";
			/// <summary>服</summary>
			public const string ITEM_CATEGORY_CLOTHES = "2";
			/// <summary>乗り物</summary>
			public const string ITEM_CATEGORY_VEHICLE = "3";
			/// <summary>回復</summary>
			public const string ITEM_CATEGORY_RESTORE = "4";
			/// <summary>罠</summary>
			public const string ITEM_CATEGORY_TRAP = "5";
			/// <summary>ﾓｻﾞ消しﾂｰﾙ</summary>
			public const string ITEM_CATEGOAY_TOOL = "6";
			/// <summary>動画ﾁｹｯﾄ</summary>
			public const string ITEM_CATEGORY_TICKET = "7";
			/// <summary>ﾀﾞﾌﾞﾙ罠</summary>
			public const string ITEM_CATEGORY_DOUBLE_TRAP = "8";
			/// <summary>一輪のﾊﾞﾗ</summary>
			public const string ITEM_CATEGORY_ROSE = "9";
		}

		public class GameItemCatregoryGroup {
			public const string EQUIP = "1"; //装備系
			public const string RESTORE = "2"; //回復系
			public const string TRAP = "3"; //罠系
			public const string TOOL = "4"; //ﾓｻﾞ消しﾂｰﾙ
			public const string TICKET = "5"; //動画ﾁｹｯﾄ
		}

		/// <summary>
		/// ｹﾞｰﾑｱｲﾃﾑｿｰﾄ
		/// </summary>
		public class GameItemSort {
			public const string GAME_ITEM_SORT_NEW = "1";
			public const string GAME_ITEM_SORT_HIGH = "2";
			public const string GAME_ITEM_SORT_LOW = "3";
			public const string GAME_ITEM_SORT_ATTACK = "4";
			public const string GAME_ITEM_SORT_DEFENCE = "5";
			public const string GAME_ITEM_SORT_ATTACK_LOW = "6";
			public const string GAME_ITEM_SORT_TYPE	= "7";
		}

		/// <summary>
		/// ｹﾞｰﾑｱｲﾃﾑ性別
		/// </summary>
		public class GameItemSex {
			public const string ITEM_SEX_CD_MAN = "1";
			public const string ITEM_SEX_CD_WOMAN = "3";
		}
		
		/// <summary>
		/// ｹﾞｰﾑｱｲﾃﾑ購入数ﾌﾟﾙﾀﾞｳﾝ
		/// </summary>
		public readonly int[] BUY_ITEM_QUANTITY_LIST = { 1,2,3,4,5,10,25,50 };

		/// <summary>
		/// ｹﾞｰﾑｱｲﾃﾑ購入結果
		/// </summary>
		public class GameItemBuy {
			public const string BUY_GAME_ITEM_RESULT_OK = "0";
			public const string BUY_GAME_ITEM_RESULT_NO_MONEY = "1";
			public const string BUY_GAME_ITEM_RESULT_NG = "9";
		}

		/// <summary>
		/// エラー用定数
		/// </summary>
		public class PwErrerCode {
			public const string GAME_CHAR_CREATE = "EX33";
			public const string ERR_VALIDATE_NUMERIC_FIELD = "EX34";		//{0}を数値で入力してください
			public const string GAME_NOT_HAVE_OTHER = "EX35";				//{0}がありません。
			public const string GAME_OTHER = "EX36";						//{0}でエラーが発生しました。
			public const string GAME_USED_OTHER = "EX37";					//{0}は既に使用済みです。
			public const string GAME_LIMIT_OVER = "EX38";					//{0}までです
			public const string GAME_CAN_NOT_REGISTER = "EX39";				//{0}登録できません。
			public const string GAME_STATUS_TERM_ID_ALREADY_EXIST = "EX40"; // 固体識別登録済み
			public const string GAME_HANDLE_NM_ERR_STATUS_CAST = "EX41";	//源氏名未入力
			public const string ERR_STATUS_UNDER_14 = "EX42";				//14歳未満 (アイドルテレビ・アイドル登録時) 
			public const string ERR_STATUS_PREFECTURE = "EX43";				//現住所(都道府県)未入力
			public const string ERR_STATUS_ADDRESS1 = "EX44";				//現住所(詳細住所)未入力
			public const string ERR_GUARDIAN_NM = "EX45";					//保護者氏名未入力(18歳未満)
			public const string	ERR_GUARDIAN_TEL = "EX46";					//保護者の携帯番号未入力
			public const string ERR_GUARDIAN_TEL_NG = "EX47";				//保護者の携帯番号を正しく入力してください
			public const string ERR_HANDLE_NM_NG = "EX48";					//芸名に絵文字が含まれています。
			public const string ERR_COMMENT_LIST = "EX49";					//自己PR未入力
			public const string ERR_START_WAITTING_UNDER_18 = "EX50";		//22時以降 18歳未満の方は待機できません
			public const string ERR_BLOG_COMMENT = "EX51";					//ｺﾒﾝﾄを入力してください。
			public const string ERR_BLOG_COMMENT_OVER_50 = "EX52";			//ｺﾒﾝﾄを50文字以内で入力してください。
			public const string ERR_BLOG_LIMIT_TIME = "EX53";				//同一のﾌﾞﾛｸﾞには24時間に1回ｺﾒﾝﾄできます。
			public const string ERR_CAST_PIC_NOT_FOUND_UNUSED = "EX54";		//未閲覧のお宝写真がありません。
			public const string ERR_TWEET = "EX55";							//つぶやきを入力してください。
			public const string ERR_TWEET_COMMENT = "EX56";					//ｺﾒﾝﾄを入力してください。
			public const string ERR_TWEET_LIMIT_TIME = "EX57";				//同一のｱｲﾄﾞﾙには24時間に1回ｺﾒﾝﾄできます。
			public const string ERR_MAN_TWEET_INTERVAL_SEC = "EX58";		//前回投稿からの期間が短すぎます。
			public const string ERR_MAN_TWEET_LIMIT = "EX59";				//1日に書き込み可能な回数を超えています。
			public const string ERR_STATUS_BBS_RES_HANDLE_LENGTH = "EX60";	//ﾊﾝﾄﾞﾙﾈｰﾑを20文字以内で入力してください。
			public const string ERR_STATUS_BBS_RES_HANDLE_NG = "EX61";		//ﾊﾝﾄﾞﾙﾈｰﾑに禁止ワードが含まれています。
			public const string ERR_STATUS_BBS_THREAD_HANDLE_NM = "EX62";	//作成者を入力してください。
			public const string ERR_STATUS_BBS_THREAD_HANDLE_LENGTH = "EX63";	//作成者を20文字以内で入力してください。
			public const string ERR_STATUS_BBS_THREAD_HANDLE_NG = "EX64";	//作成者に禁止ワードが含まれています。
			public const string ERR_TWEET_COMMENT_TOO_SHORT = "EX65";		//ｺﾒﾝﾄが短すぎます。
			public const string ERR_TWEET_COMMENT_COUNT_OVER_LIMIT = "EX66";//同一のつぶやきに対してのｺﾒﾝﾄは{0}回までです。
			public const string ERR_MAIL_RX_TIME = "EX67";					//重複した時間帯を設定しています。
			public const string ERR_PICKUP_OBJS_COMMENT_LIMIT = "EX68";		//ｺﾒﾝﾄは1日に1回のみ可能です。
			public const string ERR_STATUS_CAST_TOO_LOW_AGE = "EX69";		//{0}才未満の方はご登録できません。
			public const string ERR_MAIL_DOC_LENGTH_OVER = "EX70";			//{0}文字ｵｰﾊﾞｰです。文字数は{1}文字までです。
			public const string ERR_MAIL_REQUEST_NG_COUNT_OVER = "EX71";	//1日{0}回までです。
			public const string ERR_MAIL_REQUEST_NG_RECEIVED = "EX72";		//すでにメールを受信しています。
			public const string ERR_MAIL_REQUEST_NG_REFUSE = "EX73";		//この出演者様にはメールおねだりできません。
			public const string ERR_MAN_REJECT_ICLOUD_MAIL = "EX74";		//iiCloudメールはご利用になれません。他のメールアドレスをご利用ください。
			public const string ERR_CAST_TO_MAN_BATCH_MAIL_NO_AUTH = "EX75";	//メール一括送信の利用が禁止されています。
			public const string ERR_INPUT_DOLLAR_MARK = "EX76";				//半角の$マークは使用できません
			public const string ERR_CREDIT_AUTO_SETTLE_NO_AGREEMENT = "EX77";	//同意が必要です。
			public const string ERR_STATUS_NOT_EXISTS_TEL_NO = "EX78";	// 入力された電話番号は登録されていません。電話番号を確認してください。
		}

		/// <summary>
		/// ワナ設定結果
		/// </summary>
		public class GameUseTrapResult {
			public const string RESULT_OK = "0";				//完了
			public const string RESULT_NOT_HAVE_ITEM = "1";		//ワナがたりない
			public const string RESULT_NOT_HAVE_TREASURE = "3";	//お宝がない
			public const string RESULT_OVER_USE_TRAP = "5";		//ワナを仕掛けた数が50回を超えました
			public const string RESULT_NG = "9";				//その他
		}

		/// <summary>
		/// ﾓｻﾞ消しﾂｰﾙ使用結果
		/// </summary>
		public class GameUseMosaicErase {
			public const string RESULT_OK = "0";				//完了
			public const string RESULT_NOT_HAVE_ITEM = "1";		//ﾓｻﾞ消しﾂｰﾙがたりない
			public const string RESULT_USED = "2";				//モザ消しツール利用済
			public const string RESULT_NOT_HAVE_TREASURE = "3";	//お宝がない
			public const string RESULT_NG = "9";				//その他
		}

		/// <summary>
		/// 動画ﾁｹｯﾄ使用結果
		/// </summary>
		public class GameUseMovieTicket {
			public const string RESULT_OK = "0";				//完了
			public const string RESULT_NOT_HAVE_ITEM = "1";		//動画チケットがたりない
			public const string RESULT_NG = "9";				//その他
		}

		/// <summary>
		/// 相手持ち物1ページ毎の表示数
		/// </summary>
		public const int PARTNER_POSSESSION_ITEM_PER_PAGE = 9;

		/// <summary>
		/// 因縁の相手一覧 ｿｰﾄ
		/// </summary>
		public class GameCharacterBattleLoseSort {
			public const string LAST_LOSE_DATE = "1";
			public const string LOSE_COUNT = "2";
		}

		/// <summay>
		/// ﾊﾞﾄﾙ相手一覧(因縁の相手) 最大件数
		/// </summary>
		public const string BATTLE_LOSE_LIMIT_ROW = "20";

		/// <summary>
		///	男性用お宝の属性種類数
		/// </summary>
		public const string MAN_TREASURE_ATTR_NUM = "7";

		/// <summary>
		///	女性用お宝の属性種類数
		/// </summary>
		public const int WOMAN_TREASURE_ATTR_NUM = 7;

		/// <summary>
		/// ｷｬｽﾄﾌｧｲﾙ種別
		/// </summary>
		public class CastFile {
			/// <summary>画像(男性お宝)</summary>
			public const string PIC = "1";
			/// <summary>動画</summary>
			public const string MOVIE = "2";
		}

		/// <summary>
		/// ﾛｸﾞｲﾝﾎﾞｰﾅｽ取得結果
		/// </summary>
		public class GameGetLoginBonusResult {
			public const string RESULT_OK = "0";			//完了
			public const string RESULT_GOT = "1";			//ﾎﾞｰﾅｽ取得済み
			public const string RESULT_NO_BONUS = "2";		//ﾎﾞｰﾅｽ無し
			public const string RESULT_NG = "9";			//その他
		}

		public const string BUTTON_EXTSEARCH_GAME_CHAR = "cmdExtGameCharSearch";

		/// <summary>
		/// ｹﾞｰﾑ課金種別
		/// </summary>
		public class GameCharge {
			/// <summary>アイテム</summary>
			public const string ITEM = "1";
			/// <summary>デート</summary>
			public const string DATE = "2";
			/// <summary>ガチャ</summary>
			public const string LOTTERY = "3";
		}

		/// <summary>
		/// ﾀｲﾑｾｰﾙ実施状態
		/// </summary>
		public class GameSaleState {
			/// <summary>実施中</summary>
			public const string OPEN = "1";
			/// <summary>予定</summary>
			public const string SOON = "2";
			/// <summary>予定無し</summary>
			public const string CLOSE = "3";
		}

		/// <summary>
		/// 部隊回復結果
		/// </summary>
		public class GameRecoveryForceFullStatus {
			/// <summary>完了</summary>
			public const string RESULT_OK = "0";
			/// <summary>回復ｱｲﾃﾑ無し</summary>
			public const string RESULT_NO_ITEM = "1";
			/// <summary>ﾎﾟｲﾝﾄ不足</summary>
			public const string RESULT_NO_POINT = "2";
			/// <summary>該当部隊無し</summary>
			public const string RESULT_NO_FORCE = "3";
			/// <summary>その他</summary>
			public const string RESULT_NG = "9";
		}

		/// <summary>
		/// ｽﾃｰｼﾞｸﾘｱ(BOSS戦)結果
		/// </summary>
		public class GameStageClear {
			/// <summary>正常にｸﾘｱ</summary>
			public const string RESULT_OK = "0";
			/// <summary>既にｸﾘｱ済み</summary>
			public const string RESULT_CLEARED = "1";
			/// <summary>ｸﾘｱしていない街が存在</summary>
			public const string RESULT_NOT_CLEAR_TOWN = "2";
			/// <summary>負けました</summary>
			public const string RESULT_LOSS = "3";
			/// <summary>その他エラー</summary>
			public const string RESULT_NG = "9";
		}

		/// <summary>
		/// ﾊﾞﾄﾙ結果
		/// </summary>
		public class GameBattleResult {
			/// <summary>正常にバトルが終わりました<summray>
			public const string RESULT_OK = "0";
			/// <summary>攻撃回数オーバー</summary>
			public const string RESULT_ATTACK_COUNT_OVER = "1";
			/// <summary>相手がお宝を所持していません(モザ消しツールで見えなくなった場合や女性でコンプされてしまった場合も)</summary>
			public const string RESULT_NOT_HAVE_TREASURE = "2";
			/// <summary>部隊が足りません</summary>
			public const string RESULT_NOT_HAVE_FORCE = "3";
			/// <summary>攻撃不可時間エラー</summary>
			public const string RESULT_ATTACK_INTERVAL = "4";
			/// <summary>応援済です</summary>
			public const string RESULT_SUPPORT_END_FLAG = "5";
			/// <summary>その他のエラー</summary>
			public const string RESULT_NG = "9";
		}

		public class GameClearMissionResult {
			/// <summary>正常にクリアできました</summary>
			public const string RESULT_OK = "0";
			/// <summary>アイテムが足りませんた</summary>
			public const string RESULT_NO_ITEM = "1";
			/// <summary>部隊が足りません</summary>
			public const string RESULT_NO_FORCE = "2";
			/// <summary>クリアできない街</summary>
			public const string RESULT_NG = "9";
		}

		public class GameGetTreasureResult {
			/// <summary>正常に取得できました</summary>
			public const string RESULT_OK = "0";
			/// <summary>取得済みです</summary>
			public const string RESULT_GOT = "1";
			/// <summary>不正なパラメータ</summary>
			public const string RESULT_NG = "9";
		}

		public class GameFriendlyRank {
			/// <summary>恋人<summray>
			public const int LOVER = 1;
			/// <summary>親友</summary>
			public const int BEST_FRIEND = 25;
			/// <summary>友達</summary>
			public const int FRIEND = 100;
			/// <summary>知人</summary>
			public const int ACQUAINTANCE = 200;
			/// <summary>他人</summary>
			public const int OTHERS = 201;
		}

		/// <summary>
		/// ﾊﾞﾄﾙ種別
		/// </summary>
		public class GameBattleType {
			/// <summary>シングルバトル</summary>
			public const string SINGLE = "1";
			/// <summary>パーティーバトル</summary>
			public const string PARTY = "2";
			/// <summary>応援バトル</summary>
			public const string SUPPORT = "3";
		}

		/// <summary>
		/// 応援要請結果 
		/// </summary>
		public class GameSupportRequestResult {
			/// <summary>正常に申請できました</summary>
			public const string RESULT_OK = "0";
			/// <summary>申請済みです</summary>
			public const string RESULT_REQUEST = "1";
			/// <summary>試合に勝ってます</summary>
			public const string RESULT_WIN_BATTLE = "2";
			/// <summary>その他のエラー</summary>
			public const string RESULT_NG = "9";
		}

		/// <summary>
		/// 部隊配置結果
		/// </summary>
		public class GameAddMaxForceResult {
			/// <summary>完了</summary>
			public const string RESULT_OK = "0";
			/// <summary>未割当部隊数より割当部隊数が多い</summary>
			public const string RESULT_COUNT_NG = "1";
			/// <summary>その他</summary>
			public const string RESULT_NG = "9";
		}

		/// <summary>
		/// 部隊 
		/// </summary>
		public class GameMskForce {
			///<summary>攻撃部隊</summary>
			public const int MSK_ATTACK_FORCE = 1;
			///<summary>防御部隊</summary>
			public const int MSK_DEFENCE_FORCE = 2;
			///<summary>ﾅﾝﾊﾟ部隊</summary>
			public const int MSK_MISSION_FORCE = 4;
			///<summary>全部隊</summary>
			public const int MSK_ALL_FORCE = 7;
		}

		/// <summary>
		/// ｶﾞﾁｬ結果
		/// </summary>
		public class GameLotteryResult {
			///<summary>完了</summary>
			public const string RESULT_OK = "0";
			///<summary>非開催/非公開</summary>
			public const string RESULT_NOT_PUBLISH = "1";
			///<summary>金不足</summary>
			public const string RESULT_NO_MONEY = "2";
			///<summary>その他</summary>
			public const string RESULT_NG = "9";
		}

		/// <summary>
		/// ｹﾞｰﾑﾆｭｰｽ種別
		/// </summary>
		public class GameNewsType {
			///<summary>お宝写真認証時</summary>
			public const string NEWS_PIC_UPLOAD = "1";
			///<summary>お宝動画認証時</summary>
			public const string NEWS_MOVIE_UPLOAD = "2";
			///<summary>ほしい物リスト追加</summary>
			public const string NEWS_GIVE_ME_ITEM = "3";
			///<summary>お宝コンプリート</summary>
			public const string NEWS_TREASURE_COMPLETE = "4";
			///<summary>お宝取られた</summary>
			public const string NEWS_TREASURE_LOSS = "5";
			///<summary>称号獲得</summary>
			public const string NEWS_GET_HONOR = "6";
		}

		/// <summary>
		/// ﾗﾝｸ期間
		/// </summary>
		public class GameRankPeriod {
			/// <summary>今日</summary>
			public const string TODAY = "1";
			/// <summary>1週間</summary>
			public const string WEEK = "2";
			/// <summary>1ヶ月</summary>
			public const string MONTH = "3";
		}

		/// <summary>
		/// ﾅﾝﾊﾟ部隊1隊の回復に要する時間（分）
		/// </summary>
		public const int GAME_MISSION_FORCE_RECOVERY_MINUTE = 3;

		/// <summary>
		/// ﾊｸﾞﾎﾞｰﾅｽ時間（分）
		/// </summary>
		public const int GAME_HUG_BONUS_MINUTE = 10;

		/// <summary>
		/// プレゼント結果
		/// </summary>
		public class PresentGameItemResult {
			///<summary>完了</summary>
			public const string RESULT_OK = "0";
			///<summary>アイテム不正</summary>
			public const string RESULT_NG_ITEM = "1";
			///<summary>その他</summary>
			public const string RESULT_NG = "9";
		}

		/// <summary>
		/// ｹﾞｰﾑｷｬﾗｸﾀｿｰﾄ
		/// </summary>
		public class GameCharacterSort {
			public const string GAME_CHARACTER_SORT_NEW = "1";
			public const string GAME_CHARACTER_SORT_LEVEL = "2";
			public const string GAME_CHARACTER_SORT_FRIENDLY = "3";
			public const string GAME_CHARACTER_SORT_LOGIN = "4";
			public const string GAME_CHARACTER_SORT_PROTECT_COUNT = "5";
			public const string GAME_CHARACTER_SORT_READING_COUNT = "6";
			public const string GAME_CHARACTER_SORT_RANDOM = "7";
		}

		public const string GAME_PIC_FOODER = ".gif";

		/// <summary>
		/// ﾍﾟｰｼﾞID
		/// </summary>
		public class PageId {
			public const string TRAP = "1";
			public const string BOSS_BATTLE = "2";
			public const string SUPPORT_BATTLE = "3";
		}
		
		/// <summary>
		/// ｿｰｼｬﾙｹﾞｰﾑ用Flash置場
		/// </summary>
		public const string GAME_FLASH_DIR = "Extension\\SocialGameFlash\\A001";
		
		/// <summary>
		/// ｿｰｼｬﾙｹﾞｰﾑ用ランキングタイプ
		/// </summary>
		public class RankType {
			/// <summary>ﾊﾞﾄﾙ勝利数</summary>
			public const string BATTLE_WIN = "1";
			/// <summary>獲得親密ﾎﾟｲﾝﾄ</summary>
			public const string FRIENDLY_POINT = "2";
			/// <summary>獲得経験値</summary>
			public const string EXP = "3";
			/// <summary>獲得報酬ﾎﾟｲﾝﾄ</summary>
			public const string PAYMENT = "4";
		}
		
		/// <summary>
		/// プレゼント受け取り
		/// </summary>
		public class GetPresentGameItemResult {
			///<summary>完了</summary>
			public const string RESULT_OK = "0";
			///<summary>履歴データ不正</summary>
			public const string RESULT_NG_HST = "1";
			///<summary>アイテムデータ不正</summary>
			public const string RESULT_NG_ITM = "2";
			///<summary>その他</summary>
			public const string RESULT_NG = "9";			
		}

		/// <summary>
		/// 友達紹介アイテム獲得
		/// </summary>
		public class GetGameIntroItemResult {
			///<summary>完了</summary>
			public const string RESULT_OK = "0";
			///<summary>会員データ不正</summary>
			public const string RESULT_NG_USR = "1";
			///<summary>友達紹介履歴データ不正</summary>
			public const string RESULT_NG_LOG = "2";
			///<summary>その他</summary>
			public const string RESULT_NG = "9";
		}

		/// <summary>
		/// 友達紹介記録
		/// </summary>
		public class LogGameIntroResult {
			///<summary>完了</summary>
			public const string RESULT_OK = "0";
			///<summary>会員データ不正</summary>
			public const string RESULT_NG_USR = "1";
			///<summary>紹介元データ不正</summary>
			public const string RESULT_NG_PTN = "2";
			///<summary>履歴データ登録済</summary>
			public const string RESULT_NG_LOG = "3";
			///<summary>その他</summary>
			public const string RESULT_NG = "9";
		}
		
		/// <summary>
		/// 欲しいものﾘｽﾄ更新実行結果
		/// </summary>
		public class WantedItemManteResult {
			///<summary>完了</summary>
			public const string RESULT_OK = "0";
			///<summary>設定数上限</summary>
			public const string RESULT_COUNT_MAX = "1";
			///<summary>設定不可アイテム</summay>
			public const string RESULT_NO_ITEM = "2";
			///<summary></summary>
			public const string RESULT_CANNOT_STATUS = "3";
			///<summary>その他</summary>
			public const string RESULT_NG = "9";
		}
		
		/// <summary>
		/// 女性未認証ﾍﾟｰｼﾞに遷移する場合の遷移元ID
		/// </summary>
		public class GameWomanLiveChatNoApplyAction {
			/// <summary>仲間申請</summary>
			public const string APPLY_FELLOW = "1";
			/// <summary>ﾌﾟﾚｾﾞﾝﾄ</summary>
			public const string PRESENT = "2";
			/// <summary>お気に入り登録</summary>
			public const string ADD_FAVORITE = "3";
			/// <summary>ｷｽ(旧ﾊｸﾞ)</summary>
			public const string HUG = "4";
		}
		
		public class ModifyGameUserWoman {
			public const string RESULT_OK = "0";
			public const string RESULT_NG = "9";
		}

		/// <summary>登録状態</summary>
		public class RegistStatus {
			/// <summary>ﾒｰﾙ待ち状態</summary>
			public const string REGIST_WAIT_RX_MAIL = "0";
			/// <summary>仮登録ﾒｰﾙ受信</summary>
			public const string REGIST_RX_EMAIL = "1";
			/// <summary>登録完了</summary>
			public const string REGIST_COMPLITE = "2";
			/// <summary>登録完了(成果未報告)</summary>
			public const string REGIST_COMPLITE_NO_REPORT = "3";
		}
		
		/// <summary>運営からプレゼント</summary>
		public class PresentGameItemStaffExt {
			/// <summary>完了</summary>
			public const string RESULT_OK = "0";
			/// <summary>ユーザーが見つかりません</summary>
			public const string RESULT_NG_USR = "1";
			/// <summary>運用ユーザーが見つかりません</summary>
			public const string RESULT_NG_STF = "2";
			/// <summary>アイテムが見つかりません</summary>
			public const string RESULT_NG_ITM = "3";
			/// <summary>すでにプレゼントを付与されてます</summary>
			public const string RESULT_NG_GOT = "4";
			/// <summary>アイテムと付与数の配列の要素数が合いません</summary>
			public const string RESULT_NG_ITM_CNT = "5";
			/// <summary>その他</summary>
			public const string RESULT_NG = "9";
		}

		/// <summary>1日1回無料プレゼント</summary>
		public class OnceDayPresentResult {
			/// <summary>正常にプレゼントが出来ました</summary>
			public const string RESULT_OK = "0";
			/// <summary>プレゼントアイテムではないです</summary>
			public const string RESULT_NOT_PRESENT_ITEM = "1";
			/// <summary>プレゼント済みです</summary>
			public const string RESULT_OVER_PRESENT_COUNT = "3";
			/// <summary>その他のエラー</summary>
			public const string RESULT_NG = "9";
		}

		/// <summary>男性仲間ソート</summary>
		public class ManGameCharacterFellowSort {
			/// <summary>申請日時が新しい順</summary>
			public const string APPLICATION_DATE_DESC = "1";
			/// <summary>申請許可日時が新しい順</summary>
			public const string APPLICATION_PERMIT_DATE_DESC = "2";
			/// <summary>関係が深い順</summary>
			public const string FRIENDLY_RANK_ASC = "3";
		}

		/// <summary>男性仲間メンバーソート</summary>
		public class ManGameCharacterFellowMemberSort {
			/// <summary>関係が深い順</summary>
			public const string FRIENDLY_RANK_ASC = "1";
			/// <summary>レベルが高い順</summary>
			public const string GAME_CHARACTER_LEVEL_DESC = "2";
		}

		/// <summary>男性お気に入りソート</summary>
		public class ManGameCharacterFavoriteSort {
			/// <summary>レベルが高い順</summary>
			public const string GAME_CHARACTER_LEVEL_DESC = "1";
			/// <summary>最終ログイン日時が新しい順</summary>
			public const string LAST_LOGIN_DATE_DESC = "2";
			/// <summary>お気に入り登録日時が新しい順</summary>
			public const string GAME_FAVORITE_REGIST_DATE_DESC = "3";
			/// <summary>関係が深い順</summary>
			public const string FRIENDLY_RANK_ASC = "4";
		}

		/// <summary>女性仲間メンバーソート</summary>
		public class CastGameCharacterFellowMemberSort {
			/// <summary>関係が深い順</summary>
			public const string FRIENDLY_RANK_ASC = "1";
			/// <summary>レベルが高い順</summary>
			public const string GAME_CHARACTER_LEVEL_DESC = "2";
		}

		/// <summary>女性お気に入りソート</summary>
		public class CastGameCharacterFavoriteSort {
			/// <summary>お気に入り登録日時が新しい順</summary>
			public const string GAME_FAVORITE_REGIST_DATE_DESC = "1";
			/// <summary>レベルが高い順</summary>
			public const string GAME_CHARACTER_LEVEL_DESC = "2";
			/// <summary>関係が深い順</summary>
			public const string FRIENDLY_RANK_ASC = "3";
			/// <summary>最終ログイン日時が新しい順</summary>
			public const string LAST_LOGIN_DATE_DESC = "4";
			/// <summary>ランダム</summary>
			public const string RANDOM = "5";
		}

		/// <summary>女性仲間ソート</summary>
		public class CastGameCharacterFellowSort {
			/// <summary>申請日時が新しい順</summary>
			public const string APPLICATION_DATE_DESC = "1";
			/// <summary>申請許可日時が新しい順</summary>
			public const string APPLICATION_PERMIT_DATE_DESC = "2";
			/// <summary>関係が深い順</summary>
			public const string FRIENDLY_RANK_ASC = "3";
			/// <summary>最終ログイン日時が新しい順</summary>
			public const string LAST_LOGIN_DATE_DESC = "4";
		}

		/// <summary>女性検索ソート</summary>
		public class CastGameCharacterFindSort {
			/// <summary>最終ログイン日時が新しい順</summary>
			public const string LAST_LOGIN_DATE_DESC = "1";
			/// <summary>レベルが高い順</summary>
			public const string GAME_CHARACTER_LEVEL_DESC = "2";
			/// <summary>関係が深い順</summary>
			public const string FRIENDLY_RANK_ASC = "3";
			/// <summary>ランダム</summary>
			public const string RANDOM = "4";
			/// <summary>マイページ恋人表示用(本命恋人が一番上、以降はランダム)</summary>
			public const string MOST_SWEET_HEART_NEXT_RANDOM = "5";
		}

		/// <summary>女性動画ソート</summary>
		public class CastGameCharacterMovieSort {
			/// <summary>アップロード日時が新しい順</summary>
			public const string UPLOAD_DATE_DESC = "1";
			/// <summary>アップロード日時が古い順</summary>
			public const string UPLOAD_DATE_ASC = "2";
		}

		/// <summary>お宝レビュー書込結果</summary>
		public class WriteObjReviewResult {
			/// <summary>正常</summary>
			public const string RESULT_OK = "0";
			/// <summary>既にレビュー済</summary>
			public const string RESULT_EXIST = "1";
			/// <summary>その他のエラー</summary>
			public const string RESULT_NG = "9";
		}

		public class DeleteObjReviewCommentResult {
			/// <summary>正常</summary>
			public const string RESULT_OK = "0";
			/// <summary>履歴が存在しない、削除済み</summary>
			public const string RESULT_NO_HST = "1";
			/// <summary>自身のお宝ではない</summary>
			public const string RESULT_NO_OBJ = "2";
			/// <summary>合計が存在しない</summary>
			public const string RESULT_NO_TTL = "3";
			/// <summary>その他のエラー</summary>
			public const string RESULT_NG = "9";
		}

		/// <summary>ユーザー定義ポイント追加結果</summary>
		public class AddUserDefPointResult {
			/// <summary>ポイント追加</summary>
			public const string RESULT_OK = "0";
			/// <summary>ポイント未追加</summary>
			public const string RESULT_NOT_ADD_POINT = "1";
			/// <summary>NA_FLAG 1</summary>
			public const string RESULT_NA_FLAG = "2";
			/// <summary>時間外</summary>
			public const string RESULT_TIME_OUT = "3";
			/// <summary>追加済</summary>
			public const string RESULT_ADDED = "4";
			/// <summary>T_USER_DEF_POINTにレコード存在なし</summary>
			public const string RESULT_NOT_FOUND = "9";
		}
		
		/// <summary>ピックアップオブジェ投票結果</summary>
		public class VotePickupObjResult {
			/// <summary>正常</summary>
			public const string RESULT_OK = "0";
			/// <summary>投票済み</summary>
			public const string RESULT_VOTED = "1";
			/// <summary>その他エラー</summary>
			public const string RESULT_NG = "9";
		}

		/// <summary>この娘を探せエントリー・発見・報酬獲得結果</summary>
		public class WantedEntrantResult {
			/// <summary>正常</summary>
			public const string RESULT_OK = "0";
			/// <summary>メールアドレスNG</summary>
			public const string RESULT_MAIL_ADDR_NG = "1";
			/// <summary>メール受信時間設定不足</summary>
			public const string RESULT_MAIL_RX_TIME = "2";
			/// <summary>サイトからのお知らせメール受信設定</summary>
			public const string RESULT_INFO_MAIL_RX = "3";
			/// <summary>出演者からのメール受信設定</summary>
			public const string RESULT_CAST_MAIL_RX = "4";
			/// <summary>報酬受け取り期間オーバー</summary>
			public const string RESULT_TIME_OVER = "5";
			/// <summary>その他エラー</summary>
			public const string RESULT_NG = "9";
		}

		public class ObjReviewRankType {
			/// <summary>評価日間順位</summary>
			public const string DAILY = "1";
			/// <summary>評価週別順位</summary>
			public const string WEEKLY = "2";
			/// <summary>評価月別順位</summary>
			public const string MONTHLY = "3";
			/// <summary>イイネ数週間順位</summary>
			public const string LIKE_COUNT_WEEKLY = "4";
			/// <summary>イイネ数月間順位</summary>
			public const string LIKE_COUNT_MONTHLY = "5";
		}
		
		/*----------------------------------*/
		/*　Button ｱｸｼｮﾝ	 				*/
		/*----------------------------------*/
		public const string BUTTON_SUBMIT_SET_1 = "cmdSubmitSet1";
		public const string BUTTON_SUBMIT_SET_2 = "cmdSubmitSet2";
		public const string BUTTON_SUBMIT_SET_3 = "cmdSubmitSet3";
		public const string BUTTON_PUBLISH = "cmdPublish";
		public const string BUTTON_DELETE = "cmdDelete";
		public const string BUTTON_BACK = "cmdBack";
		public const string BUTTON_CONFIRM = "cmdConfirm";
		public const string BUTTON_REGIST = "cmdRegist";
		public const string BUTTON_PRESENT = "cmdPresent";

		/// <summary>おねがいソート</summary>
		public class RequestSort {
			/// <summary>良い順</summary>
			public const string GOOD_COUNT_DESC = "1";
			/// <summary>悪い順</summary>
			public const string BAD_COUNT_DESC = "2";
			/// <summary>公開日が新しい順</summary>
			public const string PUBLIC_DATE_DESC = "3";
			/// <summary>公開日が古い順</summary>
			public const string PUBLIC_DATE_ASC = "4";
			/// <summary>評価が多い順</summary>
			public const string VALUE_COUNT_DESC = "5";
			/// <summary>評価が新しい順</summary>
			public const string LAST_VALUE_DATE_DESC = "6";
			/// <summary>評価が古い順</summary>
			public const string LAST_VALUE_DATE_ASC = "7";
		}

		/// <summary>おねがい評価</summary>
		public class RequestValue {
			/// <summary>良い</summary>
			public const string GOOD = "1";
			/// <summary>悪い</summary>
			public const string BAD = "2";
		}

		/// <summary>
		/// シンプルなSPの実行結果
		/// 実行結果が正常/エラーのみであれば、こちらを利用して下さい
		/// </summary>
		public class SimpleSPResult {
			/// <summary>正常終了</summary>
			public const string RESULT_OK = "0";
			/// <summary>エラー</summary>
			public const string RESULT_NG = "9";
		}

		/// <summary>男性用自由フラグ</summary>
		public class ManUserDefFlag {
			/// <summary>おねがい管理者</summary>
			public const string REQUEST_ADMIN = "05";
		}

		/// <summary>女性用自由フラグ</summary>
		public class CastUserDefFlag {
			/// <summary>おねがい管理者</summary>
			public const string REQUEST_ADMIN = "05";
			/// <summary>ﾛｸﾞｲﾝ中男性ｱｸｾｽ禁止</summary>
			public const string LOGIN_USER_DISABLED = "07";
		}

		/// <summary>おねがい評価投稿結果</summary>
		public class WriteRequestValueResult {
			/// <summary>正常終了</summary>
			public const string RESULT_OK = "0";
			/// <summary>既に評価済み</summary>
			public const string RESULT_EXIST = "1";
			/// <summary>エラー</summary>
			public const string RESULT_NG = "9";
		}

		/// <summary>おねがい公開状態</summary>
		public class RequestPublicStatus {
			/// <summary>申請中</summary>
			public const string APPLY = "1";
			/// <summary>公開</summary>
			public const string PUBLIC = "2";
			/// <summary>非公開</summary>
			public const string PRIVATE = "3";
		}

		/// <summary>おねがい評価コメント公開状態</summary>
		public class RequestValueCommentPublicStatus {
			/// <summary>申請中</summary>
			public const string APPLY = "1";
			/// <summary>公開</summary>
			public const string PUBLIC = "2";
			/// <summary>非公開</summary>
			public const string PRIVATE = "3";
		}

		/// <summary>男性お宝獲得ソート</summary>
		public class ManTreasureGetSort {
			/// <summary>お宝獲得新しい順</summary>
			public const string UPDATE_DATE_DESC = "1";
			/// <summary>お宝獲得古い順</summary>
			public const string UPDATE_DATE_ASC = "2";

		}
		
		/// <summary>クエスト種別</summary>
		public class GameQuestType {
			/// <summary>レベルクエスト</summary>
			public const string LEVEL_QUEST = "0";
			/// <summary>小クエスト</summary>
			public const string LITTLE_QUEST = "1";
			/// <summary>EXクエスト</summary>
			public const string EX_QUEST = "2";
			/// <summary>裏クエスト</summary>
			public const string OTHER_QUEST = "3";
		}

		/// <summary>クエストクリア条件 カウント開始タイミング</summary>
		public class GameQuestCountStartType {
			/// <summary>ゲーム登録当初から</summary>
			public const string GAME_REGIST = "0";
			/// <summary>エントリー後</summary>
			public const string QUEST_ENTRY = "1";
		}
		
		/// <summary>クエストエントリー結果</summary>
		public class GameQuestEntryResult {
			/// <summary>エントリーできました</summary>
			public const string RESULT_OK = "0";
			/// <summary>クリア済みのクエストです</summary>
			public const string RESULT_CEARED = "1";
			/// <summary>エントリーできないクエストです</summary>
			public const string RESULT_CANNOT = "2";
			/// <summary>その他エラー</summary>
			public const string RESULT_NG = "9";
		}

		/// <summary>クエストエントリー状態</summary>
		public class GameQuestEntryStatus {
			/// <summary>未エントリー</summary>
			public const string NO_ENTRY = "0";
			/// <summary>エントリー中</summary>
			public const string ENTRY = "1";
			/// <summary>失敗</summary>
			public const string FAILED = "2";
			/// <summary>クリア</summary>
			public const string CLEAR = "3";
		}
		
		/// <summary>クエスト報酬受取結果</summary>
		public class GameQuestRewardGetResult {
			/// <summary>完了</summary>
			public const string RESULT_OK = "0";
			/// <summary>報酬取得済</summary>
			public const string RESULT_GOT = "1";
			/// <summary>報酬受取期間外</summary>
			public const string RESULT_OUT_OF_TIME = "2";
			/// <summary>未クリア</summary>
			public const string RESULT_NOT_CLEAR = "3";
			/// <summary>その他エラー</summary>
			public const string RESULT_NG = "9";
		}
		
		/// <summary>クエストクリア判別結果</summary>
		public class GameQuestCheckClearResult {
			/// <summary>完了</summary>
			public const string RESULT_OK = "0";
			/// <summary>その他エラー</summary>
			public const string RESULT_NG = "9";
		}
		
		/// <summary>クエストクリア条件カテゴリ</summary>
		public class GameQuestTrialCategory {
			/// <summary>ナンパ</summary>
				public const string MISSION = "1";
			/// <summary>お宝取得</summary>
				public const string TREASURE = "2";
			/// <summary>バトル</summary>
				public const string BATTLE = "3";
			/// <summary>仲間</summary>
				public const string FELLOW = "4";
			/// <summary>ハグ（キス）</summary>
				public const string HUG = "5";
			/// <summary>プレゼント</summary>
				public const string PRESENT = "6";
			/// <summary>親密度</summary>
				public const string FRIENDLY = "7";
			/// <summary>ガチャ</summary>
				public const string LOTTERY = "8";
			/// <summary>モザ消しツール</summary>
				public const string USE_MOSAIC_ERASE = "9";
			/// <summary>動画チケット</summary>
				public const string USE_MOVIE_TICKET = "10";
			/// <summary>ボス戦</summary>
				public const string STAGE_CLEAR = "11";
			/// <summary>連携ポイント</summary>
				public const string USE_COOP_POINT = "12";
		}
		
		/// <summary>クエストクリア条件詳細</summary>
		public class GameQuestTrialCategoryDetail {
			/// <summary>X回ﾅﾝﾊﾟを実行する</summary>
				public const string MISSION = "1";
			/// <summary>自分がX人にﾊｸﾞ（ｷｽ）した</summary>
				public const string HUG = "25";
			/// <summary>自分がX人にﾊｸﾞ（ｷｽ）された</summary>
				public const string PARTNER_HUG = "26";
			/// <summary>X人恋人がいる</summary>
				public const string STEDY = "31";
			/// <summary>無料ｶﾞﾁｬをX回実行した</summary>
				public const string LOTTERY = "32";
			/// <summary>ﾓｻﾞ消しﾂｰﾙをX回使用した</summary>
				public const string USE_MOSAIC_ERACE = "33";
			/// <summary>動画ﾁｹｯﾄをX回使用した</summary>
				public const string USE_MOVIE_TICKET = "34";
			/// <summary>ﾎﾞｽをX人倒した</summary>
				public const string STAGE_CLEAR = "35";
			/// <summary>X回連携ﾎﾟｲﾝﾄで回復した</summary>
				public const string USE_COOP_POINT = "36";
		}

		public class ManTreasureCompCalendarSort {
			/// <summary>公開日が古い順</summary>
			public const string DISPLAY_DAY_ASC = "1";
			/// <summary>残りの種類数が少ない順</summary>
			public const string REST_ATTR_COUNT_ASC = "2";

		}
		
		public class TownSort {
			/// <summary>街レベルが高い順</summary>
			public const string HI = "1";
			/// <summary>街レベルが低い順</summary>
			public const string LOW = "2";
		}
		
		/// <summary>
		/// チュートリアル用男性お宝保存ディレクトリ
		/// </summary>
		public class TutorialManTreasureDir {
			/// <summary>ナンパ</summary>
			public const string MISSION = "ManTreasureMission";
			/// <summary>バトル</summary>
			public const string BATTLE = "ManTreasureBattle";
		}
		
		/// <summary>
		/// モザイクありお宝画像拡張子
		/// </summary>
		public const string PIC_FOOTER_MOSAIC = "_m.jpg";
		public const string PIC_FOOTER_MOSAIC_SMALL = "_s_m.jpg";
		
		/// <summary>
		/// 本命恋人登録結果
		/// </summary>
		public class RegistMostSweetHeartResult {
			/// <summary>成功</summary>
			public const string RESULT_OK = "0";
			/// <summary>その他エラー</summary>
			public const string RESULT_NG = "9";
		}
		
		/// <summary>
		/// パトロン一覧ソート
		/// </summary>
		public class GameManCommunicationSort {
			/// <summary>関係順</summary>
			public const string SORT_FRIENDLY_RANK = "1";
			/// <summary>プレゼントされた順</summary>
			public const string SORT_PRESENT_COUNT = "2";
			/// <summary>モザ消しツール使用順</summary>
			public const string SORT_USE_MOSAIC_ERASE = "3";
			/// <summary>お宝写真所持順</summary>
			public const string SORT_TOTAL_PIC_POSSESSION_COUNT = "4";
			/// <summary>動画チケット使用順</summary>
			public const string SORT_USE_MOVIE_TICKET = "5";
			/// <summary>指名デート回数順</summary>
			public const string SORT_CHARGE_DATE_COUNT = "6";
		}
		
		/// <summary>
		/// コンプカレンダー 1ページごとの日付表示件数
		/// </summary>
		public const int GameCompCalendarRowCountOfPage = 10;
		
		/// <summary>
		/// ハグ結果
		/// </summary>
		public class HugGameCharacterResult {
			/// <summary>成功</summary>
			public const string RESULT_OK = "0";
			/// <summary>ハグ済</summary>
			public const string RESULT_HUGGED = "1";
			/// <summary>通話中、またはオフラインに見せているためハグ不可</summary>
			public const string RESULT_CANNOT_STATUS = "2";
			/// <summary>その他エラー</summary>
			public const string RESULT_NG = "9";
		}

		/// <summary>
		/// 画像ファイルがない場合の代替JPG画像パス
		/// </summary>
		public const string NoPicJpgPath = "image/A001/nopic.jpg";
		
		/// <summary>
		/// ブログコメントソート
		/// </summary>
		public class BlogCommentSort {
			/// <summary>新着順</summary>
			public const string CREATE_DATE_DESC = "1";
			/// <summary>投稿順</summary>
			public const string CREATE_DATE_ASC = "2";
		}

		/// <summary>
		/// Flash置場
		/// </summary>
		public const string FLASH_DIR = "Extension\\Flash\\A001";

		/// <summary>
		/// FC入会結果
		/// </summary>
		public class FanClubAdmissionResult {
			/// <summary>正常終了(通常決済対応者)</summary>
			public const string RESULT_OK = "0";
			/// <summary>正常終了(QUICK決済対応者)</summary>
			public const string RESULT_OK_WITH_QUICK = "1";
			/// <summary>既存入会状態</summary>
			public const string RESULT_EXIST = "9";
		}

		/// <summary>
		/// FC入会・継続可否判定結果
		/// </summary>
		public class FanClubAdmissionEnableResult {
			/// <summary>不可</summary>
			public const string RESULT_NG = "0";
			/// <summary>可</summary>
			public const string RESULT_OK = "1";
			/// <summary>可(決済NG)</summary>
			public const string RESULT_OK_SETTLE_NG = "2";
			/// <summary>可(ポイントFC継続)</summary>
			public const string RESULT_OK_POINT = "3";
		}

		/// <summary>
		/// FC退会結果
		/// </summary>
		public class FanClubResignResult {
			/// <summary>正常</summary>
			public const string RESULT_OK = "0";
			/// <summary>退会済み</summary>
			public const string RESULT_RESIGNED = "1";
			/// <summary>存在しない</summary>
			public const string RESULT_NO_EXIST = "9";
		}

		/// <summary>
		/// メールdeビンゴ用ディレクトリ
		/// </summary>
		public const string MAIL_DE_BINGO_DIR = "Extension\\MailDeBingo\\A001";

		/// <summary>
		/// 女性PC登録受付
		/// </summary>
		public class ApplyRegistCastPCResult {
			/// <summary>正常</summary>
			public const string RESULT_OK = "0";
			/// <summary>処理済み</summary>
			public const string RESULT_DONE = "1";
			/// <summary>エラー</summary>
			public const string RESULT_NG = "9";
		}

		/// <summary>
		/// 男性会員PC登録受付
		/// </summary>
		public class ApplyRegistUserPCResult {
			/// <summary>正常</summary>
			public const string RESULT_OK = "0";
			/// <summary>処理済み</summary>
			public const string RESULT_DONE = "1";
			/// <summary>エラー</summary>
			public const string RESULT_NG = "9";
		}

		/// <summary>FC入会（ポイント消費）結果</summary>
		public class FanClubAdmissionPtResult {
			/// <summary>正常</summary>
			public const string RESULT_OK = "0";
			/// <summary>ポイント不足</summary>
			public const string RESULT_NG_POINT_LACK = "2";
			/// <summary>入会済み</summary>
			public const string RESULT_NG_EXIST = "3";
		}

		/// <summary>FC継続（ポイント消費）結果</summary>
		public class FanClubExtensionPtResult {
			/// <summary>正常</summary>
			public const string RESULT_OK = "0";
			/// <summary>ポイント不足</summary>
			public const string RESULT_NG_POINT_LACK = "2";
			/// <summary>FC会員状態不正</summary>
			public const string RESULT_NG_FC_STATUS = "3";
		}

		/// <summary>会員ニュース種別</summary>
		public class ManNewsType {
			/// <summary>ログイン</summary>
			public const int LOGIN = 1;
			/// <summary>待機開始</summary>
			public const int START_WAITING = 2;
			/// <summary>通話開始</summary>
			public const int START_TEL = 3;
			/// <summary>プロフ更新</summary>
			public const int UPDATE_PROFILE = 4;
			/// <summary>プロフ画像更新</summary>
			public const int UPDATE_PROFILE_PIC = 5;
			/// <summary>掲示板書込</summary>
			public const int WRITE_BBS = 6;
			/// <summary>つぶやき投稿</summary>
			public const int WRITE_MAN_TWEET = 7;
		}

		/// <summary>ゲーム出演者新着情報種別</summary>
		public class GameCastNewsType {
			/// <summary>指名デート</summary>
			public const int DATE_CHARGE = 1;
		}

		/// <summary>投票設定データ更新結果</summary>
		public class VoteTermMainteResult {
			/// <summary>正常終了</summary>
			public const string RESULT_OK = "0";
			/// <summary>投票期間が他と重複</summary>
			public const string RESULT_NG_DATE = "1";
		}

		/// <summary>エントリー制投票設定データ更新結果</summary>
		public class ElectionPeriodMainteResult {
			/// <summary>正常終了</summary>
			public const string RESULT_OK = "0";
			/// <summary>投票期間が他と重複</summary>
			public const string RESULT_NG_DATE = "1";
		}

		/// <summary>投票結果</summary>
		public class RegistVoteResult {
			/// <summary>正常終了</summary>
			public const string RESULT_OK = "0";
			/// <summary>投票期間外</summary>
			public const string RESULT_NG_TERM = "1";
			/// <summary>投票券不足</summary>
			public const string RESULT_NG_TICKET = "2";
			/// <summary>投票順位データが存在しない</summary>
			public const string RESULT_NG_RANK = "3";
		}

		/// <summary>ポイントで投票券追加結果</summary>
		public class AddVoteTicketByPtResult {
			/// <summary>正常終了</summary>
			public const string RESULT_OK = "0";
			/// <summary>投票期間外</summary>
			public const string RESULT_NG_TERM = "1";
			/// <summary>ポイント不足</summary>
			public const string RESULT_NG_PT_LACK = "2";
			/// <summary>ポイント減算エラー</summary>
			public const string RESULT_NG_PT_MINUS = "3";
		}

		/// <summary>コードで投票券追加結果</summary>
		public class AddVoteTicketByCodeResult {
			/// <summary>正常終了</summary>
			public const string RESULT_OK = "0";
			/// <summary>投票期間外</summary>
			public const string RESULT_NG_TERM = "1";
			/// <summary>コード不正</summary>
			public const string RESULT_NG_CODE = "2";
			/// <summary>コード使用済</summary>
			public const string RESULT_NG_USED = "3";
		}

		/// <summary>お宝deビンゴ開催設定データ更新結果</summary>
		public class BbsBingoTermMainteResult {
			/// <summary>正常終了</summary>
			public const string RESULT_OK = "0";
			/// <summary>投票期間が他と重複</summary>
			public const string RESULT_NG_DATE = "1";
		}

		/// <summary>お宝deビンゴカードデータ登録結果</summary>
		public class BbsBingoCardRegistResult {
			/// <summary>正常終了</summary>
			public const string RESULT_OK = "0";
			/// <summary>カード数オーバー</summary>
			public const string RESULT_NG_LIMIT = "1";
		}

		/// <summary>お宝deビンゴエントリー結果</summary>
		public class EntryBbsBingoResult {
			/// <summary>正常終了</summary>
			public const string RESULT_OK = "0";
			/// <summary>開催期間外</summary>
			public const string RESULT_NG_TERM = "1";
			/// <summary>エントリー済</summary>
			public const string RESULT_NG_ENTRY = "2";
			/// <summary>カード不足</summary>
			public const string RESULT_NG_CARD = "3";
		}

		/// <summary>お宝deビンゴカードデータ取得結果</summary>
		public class GetBbsBingoCardResult {
			/// <summary>正常終了</summary>
			public const string RESULT_OK = "0";
			/// <summary>開催期間外</summary>
			public const string RESULT_NG_TERM = "1";
			/// <summary>未エントリー</summary>
			public const string RESULT_NG_ENTRY = "2";
		}

		/// <summary>お宝deビンゴNo獲得結果</summary>
		public class GetBbsBingoNoResult {
			/// <summary>正常終了</summary>
			public const string RESULT_OK = "0";
			/// <summary>開催期間外</summary>
			public const string RESULT_NG_TERM = "1";
			/// <summary>未エントリー</summary>
			public const string RESULT_NG_ENTRY = "2";
			/// <summary>ビンゴ完成済</summary>
			public const string RESULT_NG_COMPLETE = "3";
			/// <summary>お宝未閲覧</summary>
			public const string RESULT_NG_NOT_VIEW = "4";
			/// <summary>無料閲覧</summary>
			public const string RESULT_NG_FREE_VIEW = "5";
			/// <summary>期間外閲覧</summary>
			public const string RESULT_NG_OUT_TERM = "6";
			/// <summary>エントリー前閲覧</summary>
			public const string RESULT_NG_OUT_ENTRY = "7";
			/// <summary>ビンゴNo獲得済</summary>
			public const string RESULT_NG_EXIST_LOG = "8";
		}

		/// <summary>お宝deビンゴ履歴データ取得結果</summary>
		public class GetBbsBingoLogResult {
			/// <summary>正常終了</summary>
			public const string RESULT_OK = "0";
			/// <summary>開催期間外</summary>
			public const string RESULT_NG_TERM = "1";
			/// <summary>確認済</summary>
			public const string RESULT_NG_CHECKED = "2";
			/// <summary>履歴なし（お宝未閲覧）</summary>
			public const string RESULT_NG_NOT_VIEW = "3";
			/// <summary>履歴なし（無料閲覧）</summary>
			public const string RESULT_NG_FREE_VIEW = "4";
			/// <summary>履歴なし（期間外閲覧）</summary>
			public const string RESULT_NG_OUT_VIEW = "5";
			/// <summary>履歴なし（未エントリー）</summary>
			public const string RESULT_NG_ENTRY = "6";
			/// <summary>履歴なし（ビンゴ完成済）</summary>
			public const string RESULT_NG_COMPLETE = "7";
			/// <summary>履歴なし（その他）</summary>
			public const string RESULT_NG_NO_LOG = "8";
		}

		/// <summary>お宝deビンゴ賞金獲得結果</summary>
		public class GetBbsBingoPrizeResult {
			/// <summary>正常終了</summary>
			public const string RESULT_OK = "0";
			/// <summary>開催期間外</summary>
			public const string RESULT_NG_TERM = "1";
			/// <summary>ビンゴ制限中</summary>
			public const string RESULT_NG_DENIED = "2";
			/// <summary>未エントリー</summary>
			public const string RESULT_NG_ENTRY = "3";
			/// <summary>ビンゴ未完成</summary>
			public const string RESULT_NG_UNCOMPLETE = "4";
			/// <summary>pt付与失敗</summary>
			public const string RESULT_NG_PRIZE = "5";
		}
		
		/// <summary>エントリー制ネット投票順位表示 期間指定</summary>
		public class ElectionPeriodStatus {
			/// <summary>1次投票</summary>
			public const string FRIST_PERIOD = "1";
			/// <summary>2次投票</summary>
			public const string SECOND_PERIOD = "2";
			/// <summary>合計</summary>
			public const string TOTAL = "3";
		}

		/// <summary>出演者動画イイネ登録結果</summary>
		public class RegistCastMovieLikeResult {
			/// <summary>正常終了</summary>
			public const string RESULT_OK = "0";
			/// <summary>動画エラー</summary>
			public const string RESULT_NG_MOVIE = "1";
			/// <summary>未閲覧エラー</summary>
			public const string RESULT_NG_VIEW = "2";
			/// <summary>イイネ済エラー</summary>
			public const string RESULT_NG_LIKED = "3";
			/// <summary>出演者拒否登録エラー</summary>
			public const string RESULT_NG_CAST_REFUSE = "4";
			/// <summary>会員拒否登録エラー</summary>
			public const string RESULT_NG_SELF_REFUSE = "5";
		}

		/// <summary>出演者動画イイネ解除結果</summary>
		public class DeregistCastMovieLikeResult {
			/// <summary>正常終了</summary>
			public const string RESULT_OK = "0";
			/// <summary>動画エラー</summary>
			public const string RESULT_NG_MOVIE = "1";
			/// <summary>未閲覧エラー</summary>
			public const string RESULT_NG_VIEW = "2";
			/// <summary>未イイネエラー</summary>
			public const string RESULT_NG_NOT_LIKED = "3";
			/// <summary>出演者拒否登録エラー</summary>
			public const string RESULT_NG_CAST_REFUSE = "4";
			/// <summary>会員拒否登録エラー</summary>
			public const string RESULT_NG_SELF_REFUSE = "5";
		}

		/// <summary>出演者画像イイネ登録結果</summary>
		public class RegistCastPicLikeResult {
			/// <summary>正常終了</summary>
			public const string RESULT_OK = "0";
			/// <summary>画像エラー</summary>
			public const string RESULT_NG_PIC = "1";
			/// <summary>イイネ済エラー</summary>
			public const string RESULT_NG_LIKED = "2";
			/// <summary>出演者拒否登録エラー</summary>
			public const string RESULT_NG_CAST_REFUSE = "3";
			/// <summary>会員拒否登録エラー</summary>
			public const string RESULT_NG_SELF_REFUSE = "4";
		}

		/// <summary>出演者画像イイネ解除結果</summary>
		public class DeregistCastPicLikeResult {
			/// <summary>正常終了</summary>
			public const string RESULT_OK = "0";
			/// <summary>画像エラー</summary>
			public const string RESULT_NG_PIC = "1";
			/// <summary>未イイネエラー</summary>
			public const string RESULT_NG_NOT_LIKED = "2";
			/// <summary>出演者拒否登録エラー</summary>
			public const string RESULT_NG_CAST_REFUSE = "3";
			/// <summary>会員拒否登録エラー</summary>
			public const string RESULT_NG_SELF_REFUSE = "4";
		}

		/// <summary>出演者動画コメント投稿結果</summary>
		public class WriteCastMovieCommentResult {
			/// <summary>正常終了</summary>
			public const string RESULT_OK = "0";
			/// <summary>動画エラー</summary>
			public const string RESULT_NG_MOVIE = "1";
			/// <summary>未閲覧エラー</summary>
			public const string RESULT_NG_VIEW = "2";
			/// <summary>コメント済エラー</summary>
			public const string RESULT_NG_COMMENTED = "3";
			/// <summary>コメント制限エラー</summary>
			public const string RESULT_NG_COMMENT_REFUSE = "4";
			/// <summary>出演者拒否登録エラー</summary>
			public const string RESULT_NG_CAST_REFUSE = "5";
			/// <summary>会員拒否登録エラー</summary>
			public const string RESULT_NG_SELF_REFUSE = "6";
		}

		/// <summary>出演者動画コメント削除結果</summary>
		public class DeleteCastMovieCommentResult {
			/// <summary>正常終了</summary>
			public const string RESULT_OK = "0";
			/// <summary>コメントエラー</summary>
			public const string RESULT_NG_COMMENT = "1";
		}

		/// <summary>出演者動画コメントソート</summary>
		public class CastMovieCommentSort {
			/// <summary>新着順</summary>
			public const string COMMENT_DATE_NEW = "1";
			/// <summary>投稿順</summary>
			public const string COMMENT_DATE_OLD = "2";
		}

		/// <summary>出演者画像コメント投稿結果</summary>
		public class WriteCastPicCommentResult {
			/// <summary>正常終了</summary>
			public const string RESULT_OK = "0";
			/// <summary>画像エラー</summary>
			public const string RESULT_NG_PIC = "1";
			/// <summary>コメント済エラー</summary>
			public const string RESULT_NG_COMMENTED = "2";
			/// <summary>コメント制限エラー</summary>
			public const string RESULT_NG_COMMENT_REFUSE = "3";
			/// <summary>出演者拒否登録エラー</summary>
			public const string RESULT_NG_CAST_REFUSE = "4";
			/// <summary>会員拒否登録エラー</summary>
			public const string RESULT_NG_SELF_REFUSE = "5";
		}

		/// <summary>出演者画像コメント削除結果</summary>
		public class DeleteCastPicCommentResult {
			/// <summary>正常終了</summary>
			public const string RESULT_OK = "0";
			/// <summary>コメントエラー</summary>
			public const string RESULT_NG_COMMENT = "1";
		}

		/// <summary>出演者画像コメントソート</summary>
		public class CastPicCommentSort {
			/// <summary>新着順</summary>
			public const string COMMENT_DATE_NEW = "1";
			/// <summary>投稿順</summary>
			public const string COMMENT_DATE_OLD = "2";
		}
		
		/// <summary></summary>
		public class SnsType {
			/// <summary>Twitter</summary>
			public const string TWITTER = "1";
			/// <summary>Google</summary>
			public const string GOOGLE = "2";
		}

		/// <summary>出演者ネット投票エントリー結果</summary>
		public class ElectionEntryResult {
			/// <summary>正常</summary>
			public const string RESULT_OK = "0";
			/// <summary>エントリー期間中のオーディションがありません</summary>
			public const string RESULT_NO_ELECTION = "1";
			/// <summary>管理者から拒否されています</summary>
			public const string RESULT_REFUSE = "2";
			/// <summary>プロフ画像が設定されていません</summary>
			public const string RESULT_NO_PROFILE_PIC = "3";
			/// <summary>その他のエラー</summary>
			public const string RESULT_NG = "9";
		}
		
		/// <summary>ページビュー一覧 アクセス種別</summary>
		public class AccessType {
			/// <summary>総アクセス数</summary>
			public const string ALL_ACCESS = "1";
			/// <summary>アクセスUU(ユニーク数)</summary>
			public const string UNIQUE_ACCESS = "2";
			/// <summary>1stアクセス</summary>
			public const string PRIORITY_1ST = "3";
			/// <summary>2ndアクセス</summary>
			public const string PRIORITY_2ND = "4";
			/// <summary>3rdアクセス</summary>
			public const string PRIORITY_3RD = "5";
			/// <summary>4thアクセス</summary>
			public const string PRIORITY_4TH = "6";
			/// <summary>5hアクセス</summary>
			public const string PRIORITY_5TH = "7";
			/// <summary>1st〜5thアクセス</summary>
			public const string PRIORITY_TOTAL = "8";
		}
		
		/// <summary>ページビュー一覧 ユーザー端末種別</summary>
		public class PageAccessCarrierType {
			/// <summary>3G端末</summary>
			public const string FEATURE_PHONE = "1";
			/// <summary>Android</summary>
			public const string ANDROID = "2";
			/// <summary>iPhone</summary>
			public const string IPHONE = "3";
		}
		
		/// <summary>ブログ記事イイネ登録結果</summary>
		public class RegistBlogArticleLikeResult {
			/// <summary>正常</summary>
			public const string RESULT_OK = "0";
			/// <summary>イイネ済み</summary>
			public const string RESULT_EXISTS_LIKE = "1";
			/// <summary>ブログ無し</summary>
			public const string RESULT_NO_BLOG = "2";
			/// <summary>ブログ記事無し</summary>
			public const string RESULT_NO_BLOG_ARTICLE = "3";
			/// <summary>その他のエラー</summary>
			public const string RESULT_NG = "9";
		}

		/// <summary>日記イイネ登録結果</summary>
		public class RegistDiaryLikeResult {
			/// <summary>正常</summary>
			public const string RESULT_OK = "0";
			/// <summary>日記無し</summary>
			public const string RESULT_NO_DIARY = "1";
			/// <summary>イイネ済み</summary>
			public const string RESULT_LIKED = "2";
			/// <summary>その他のエラー</summary>
			public const string RESULT_NG = "9";
		}

		/// <summary>ブログランキング表示タイプ</summary>
		public class BlogRankType {
			/// <summary>週間</summary>
			public const string WEEKLY = "1";
			/// <summary>月間</summary>
			public const string MONTHLY = "2";
		}

		/// <summary>
		/// 出演者ニュース種別
		/// </summary>
		public class CastNewsType {
			///<summary>ログイン</summary>
			public const string NEWS_TYPE_LOGIN = "1";
			///<summary>待機開始</summary>
			public const string NEWS_TYPE_STANDBY = "2";
			///<summary>通話開始</summary>
			public const string NEWS_TYPE_TALK = "3";
			///<summary>お宝画像投稿</summary>
			public const string NEWS_TYPE_BBS_PIC = "4";
			///<summary>お宝動画投稿</summary>
			public const string NEWS_TYPE_BBS_MOV = "5";
			///<summary>無料画像投稿</summary>
			public const string NEWS_TYPE_FREE_PIC = "6";
			///<summary>無料動画投稿</summary>
			public const string NEWS_TYPE_FREE_MOV = "7";
			///<summary>掲示板投稿</summary>
			public const string NEWS_TYPE_BBS = "8";
			///<summary>ブログ投稿</summary>
			public const string NEWS_TYPE_BLOG = "9";
			///<summary>野球拳投稿</summary>
			public const string NEWS_TYPE_YAKYUKEN = "10";
			///<summary>日記投稿</summary>
			public const string NEWS_TYPE_DIARY = "11";
		}

		/// <summary>プレゼントメール期間設定データ更新結果</summary>
		public class PresentMailTermMainteResult {
			/// <summary>正常終了</summary>
			public const string RESULT_OK = "0";
			/// <summary>投票期間が他と重複</summary>
			public const string RESULT_NG_DATE = "1";
		}

		/// <summary>
		/// プレゼントメールアイテムソート
		/// </summary>
		public class PresentMailItemSort {
			/// <summary>価格の高い順</summary>
			public const string CHARGE_POINT_DESC = "1";
			/// <summary>価格の安い順</summary>
			public const string CHARGE_POINT_ASC = "2";
		}

		/// <summary>メールおねだり送信結果</summary>
		public class RegistMailRequestLogResult {
			/// <summary>正常終了</summary>
			public const string RESULT_OK = "0";
			/// <summary>送信可能回数超過</summary>
			public const string RESULT_COUNT_OVER = "1";
			/// <summary>メール受信済み</summary>
			public const string RESULT_RECEIVED_MAIL = "2";
			/// <summary>メールおねだり拒否</summary>
			public const string RESULT_REFUSE = "3";
			/// <summary>その他エラー</summary>
			public const string RESULT_NG = "9";
		}

		/// <summary>Twilio通話発信結果</summary>
		public class TwilioCallStatus {
			/// <summary>発信待ち</summary>
			public const string QUEUED = "queued";
			/// <summary>呼出中</summary>
			public const string RINGING = "ringing";
			/// <summary>通話中</summary>
			public const string IN_PROGRESS = "in-progress";
			/// <summary>正常終了</summary>
			public const string COMPLETED = "completed";
			/// <summary>ビジー</summary>
			public const string BUSY = "busy";
			/// <summary>接続失敗</summary>
			public const string FAILED = "failed";
			/// <summary>応答なし</summary>
			public const string NO_ANSWER = "no-answer";
			/// <summary>キャンセル</summary>
			public const string CANCELED = "canceled";
		}

		/// <summary>Twilio通話切断結果</summary>
		public class TwilioDialCallStatus {
			/// <summary>接続完了</summary>
			public const string COMPLETED = "completed";
			/// <summary>通話中</summary>
			public const string BUSY = "busy";
			/// <summary>応答なし</summary>
			public const string NO_ANSWER = "no-answer";
			/// <summary>失敗</summary>
			public const string FAILED = "failed";
			/// <summary>キャンセル</summary>
			public const string CANCELED = "canceled";
		}

		/// <summary>通話切断理由</summary>
		public class TalkEndReason {
			/// <summary>接続失敗</summary>
			public const string FAILED = "0";
			/// <summary>発信側切断</summary>
			public const string OWN_DISCONNECT = "1";
			/// <summary>着信側切断</summary>
			public const string PARTNER_DISCONNECT = "2";
			/// <summary>ポイント切れ</summary>
			public const string POINT_DISCONNECT = "3";
			/// <summary>その他の原因</summary>
			public const string FAIL_OTHERS = "4";
			/// <summary>システムエラー</summary>
			public const string FAIL_SYS_ERROR = "9";
		}

		/// <summary>通話接続失敗理由</summary>
		public class DisconnectReason {
			/// <summary>応答なし。02,04も同様</summary>
			public const string NO_ANSWER = "00";
			/// <summary>通話中</summary>
			public const string BUSY = "01";
			/// <summary>電話番号不正</summary>
			public const string NO_NUMBER = "03";
			/// <summary>TEL発信なし。利用可能秒数取得タイムアウトまたは取得中にユーザーによる切断</summary>
			public const string NO_CALL = "05";
			/// <summary>通話種別間違い</summary>
			public const string NO_MACH = "06";
			/// <summary>着信拒否</summary>
			public const string REJECTED = "07";
			/// <summary>その他</summary>
			public const string FAIL_OTHERS = "08";
			/// <summary>システムエラー</summary>
			public const string FAIL_SYS_ERROR = "99";
		}

		/// <summary>メールdeガチャ設定データ更新結果</summary>
		public class MailLotteryMainteResult {
			/// <summary>正常終了</summary>
			public const string RESULT_OK = "0";
			/// <summary>期間が他と重複</summary>
			public const string RESULT_NG_DATE = "1";
		}
		
		/// <summary>メールdeガチャ実行結果</summary>
		public class GetMailLotteryResult {
			/// <summary>正常終了</summary>
			public const string RESULT_OK = "0";
			/// <summary>期間外</summary>
			public const string RESULT_NOT_PUBLISH = "1";
			/// <summary>ﾒｰﾙなし</summary>
			public const string RESULT_NO_MAIL = "2";
			/// <summary>実行済み</summary>
			public const string RESULT_GOT_LOTTERY = "3";
			/// <summary>添付ﾒｰﾙ未送信</summary>
			public const string RESULT_NO_ATTACH = "4";
			/// <summary>対象外のﾒｰﾙ</summary>
			public const string RESULT_NG_MAIL = "5";
			/// <summary>その他エラー</summary>
			public const string RESULT_NG = "9";
		}

		/// <summary>
		/// メールdeガチャ用ディレクトリ
		/// </summary>
		public const string MAIL_LOTTERY_DIR = "Extension\\MailLottery\\A001";
		
		/// <summary>アタックリストソート</summary>
		public class MailAttackSort {
			/// <summary>最終ログイン日時降順</summary>
			public const string LAST_LOGIN_DATE = "1";
			/// <summary>通話日時降順</summary>
			public const string LAST_TALK_DATE = "2";
			/// <summary>メール受信日時降順</summary>
			public const string LAST_TX_MAIL_DATE = "3";
			/// <summary>お気に入られ日時降順</summary>
			public const string REGIST_FAVORITE_DATE_BY_MAN = "4";
			/// <summary>足あとつけられた日時降順</summary>
			public const string LAST_MARKING_DATE_BY_MAN = "5";
			/// <summary>メール開封日時降順</summary>
			public const string LAST_MAIL_READ_DATE_BY_MAN = "6";
			/// <summary>会員へのメール送信回数降順</summary>
			public const string TOTAL_RX_MAIL_COUNT = "7";
			/// <summary>前回送信日時降順</summary>
			public const string LAST_RX_MAIL_DATE = "8";
			/// <summary>前回送信日時昇順</summary>
			public const string LAST_RX_MAIL_DATE_ASC = "9";
		}
		
		/// <summary>
		/// 不具合報告表示種別
		/// </summary>
		public class DefectReportDislayType {
			/// <summary>全体</summary>
			public const string PUBLIC = "1";
			/// <summary>自分の投稿した不具合報告</summary>
			public const string PERSONAL = "2";
		}

		/// <summary>
		/// 不具合報告状態
		/// </summary>
		public class DefectReportPublicStatus {
			/// <summary>申請</summary>
			public const string APPLY = "1";
			/// <summary>採用</summary>
			public const string ACCEPT = "2";
			/// <summary>公開</summary>
			public const string PUBLIC = "3";
			/// <summary>非公開</summary>
			public const string PRIVATE = "4";
			/// <summary>重複</summary>
			public const string OVERLAP = "5";
			/// <summary>移動</summary>
			public const string REMOVE = "6";
		}
		
		/// <summary>
		/// 不具合報告更新結果
		/// </summary>
		public class DefectReportMainteResult {
			/// <summary>正常</summary>
			public const string RESULT_OK = "0";
			/// <summary>重複SEQなし</summary>
			public const string RESULT_NO_OVERLAP_SEQ = "1";
			/// <summary>重複SEQ不正</summary>
			public const string RESULT_NO_OVERLAP_ORIGINAL_SEQ = "2";
			/// <summary>移動カテゴリSEQなし</summary>
			public const string RESULT_REMOVE_NO_CATEGORY = "3";
			/// <summary>その他エラー</summary>
			public const string RESULT_NG = "9";
		}
		
		/// <summary>
		/// プロフ登録完了時ポイント付与結果
		/// </summary>
		public class AddRegistProfilePointResult {
			/// <summary>正常</summary>
			public const string RESULT_OK = "0";
			/// <summary>付与済み</summary>
			public const string RESULT_ADDED = "1";
			/// <summary>その他エラー</summary>
			public const string RESULT_NG = "9";
		}
		
		/// <summary>
		/// メールアドレス重複チェック結果
		/// </summary>
		public class CheckEmailAddrDuplication {
			/// <summary>正常</summary>
			public const string RESULT_OK = "0";
			/// <summary>会員として登録済み</summary>
			public const string RESULT_EXIST_USER_MAN = "1";
			/// <summary>出演者として登録済み</summary>
			public const string RESULT_EXIST_CAST = "2";
			/// <summary>出演者として仮登録済み</summary>
			public const string RESULT_EXIST_TEMP = "3";
			/// <summary>その他エラー</summary>
			public const string RESULT_NG = "9";
		}

		/// <summary>メールアタックイベント(一定期間ログインしていない男性)ソート</summary>
		public class MailAttackLoginSort {
			/// <summary>イベント期間中のメール受信回数昇順</summary>
			public const string RX_MAIL_COUNT_ASC = "1";
			/// <summary>課金回数降順</summary>
			public const string TOTAL_RECEIPT_COUNT_DESC = "2";
			/// <summary>最終ログイン日時降順</summary>
			public const string LAST_LOGIN_DATE_DESC = "3";
			/// <summary>ランダム</summary>
			public const string RANDOM = "4";
		}

		/// <summary>メールアタックイベント(登録後24時間以内の男性)ソート</summary>
		public class MailAttackNewSort {
			/// <summary>会員登録日時降順</summary>
			public const string REGIST_DATE_DESC = "1";
			/// <summary>メール受信回数昇順</summary>
			public const string TOTAL_RX_MAIL_COUNT_ASC = "2";
			/// <summary>最終ログイン日時降順</summary>
			public const string LAST_LOGIN_DATE_DESC = "3";
		}

		/// <summary>ログイン中の会員(メール送信済み)ソート</summary>
		public class ManLoginedMailSend {
			/// <summary>最終アクション日時降順</summary>
			public const string LAST_ACTION_DATE_DESC = "1";
			/// <summary>メール送信日時昇順</summary>
			public const string LAST_RX_MAIL_DATE_ASC = "2";
			/// <summary>メール送信日時昇順</summary>
			public const string LAST_RX_MAIL_DATE_DESC = "3";
		}

		/// <summary>メールアタック(ログイン・新人)設定データ更新結果</summary>
		public class MailAttackMainteResult {
			/// <summary>正常終了</summary>
			public const string RESULT_OK = "0";
			/// <summary>期間が他と重複</summary>
			public const string RESULT_NG_DATE = "1";
		}
		
		/// <summary>メール返信ボーナスポイント付与結果</summary>
		public class AddMailResBonusPoint {
			/// <summary>正常終了</summary>
			public const string RESULT_OK = "0";
			/// <summary>期間外</summary>
			public const string RESULT_TERM_END = "1";
			/// <summary>付与済</summary>
			public const string RESULT_ADDED = "2";
			/// <summary>添付をまだ送信していない</summary>
			public const string RESULT_NOT_SEND_MAIL = "3";
			/// <summary>男性からの期限内のメールが存在しない</summary>
			public const string RESULT_NO_MAN_MAIL = "4";
			/// <summary>最大付与数を超えた</summary>
			public const string RESULT_ADDED_OVER = "5";
			/// <summary>その他エラー</summary>
			public const string RESULT_NG = "9";
		}

		/// <summary>メール返信ボーナスアタックリストのソート</summary>
		public class MailResBonusAttackSort {
			/// <summary>最終メール受信日時降順</summary>
			public const string LAST_RX_MAIL_DATE_DESC = "1";
			/// <summary>ログイン日時降順</summary>
			public const string LAST_LOGIN_DATE_DESC = "2";
		}
		
		/// <summary>出演者新着いいね通知 コンテンツ種別</summary>
		public class CastLikeNotice {
			/// <summary>プロフィール画像</summary>
			public const string CONTENTS_PROFILE_PIC = "1";
			/// <summary>プロフィール動画</summary>
			public const string CONTENTS_PROFILE_MOVIE = "2";
			/// <summary>つぶやき</summary>
			public const string CONTENTS_CAST_DIARY = "3";
			/// <summary>ブログ記事</summary>
			public const string CONTENTS_BLOG_ARTICLE = "4";
		}
	}
}