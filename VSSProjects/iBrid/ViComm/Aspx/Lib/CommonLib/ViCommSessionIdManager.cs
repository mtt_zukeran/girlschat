using System;
using System.Collections.Generic;
using System.Text;
using System.Web.SessionState;

namespace ViComm {
	public class ViCommSessionIdManager : SessionIDManager {
		public override string CreateSessionID(System.Web.HttpContext context) {
			return SessionIdGenerator.Generate();	
		}

		public override bool Validate(string id) {
			return !string.IsNullOrEmpty(id);
		}


		internal class SessionIdGenerator {
			private readonly static object _oSyncRoot = 1;
			private volatile static uint _iSessionIdSuffix = (uint)0;

			[System.Runtime.InteropServices.DllImport("kernel32.dll")]
			extern static short QueryPerformanceCounter(out long x);

			/// <summary>
			/// SessionID文字列を生成する。
			/// </summary>
			/// <remarks>
			/// マシン名＋プロセスID＋高分解能カウンター値＋補助カウンタ
			/// 
			/// マシン名：負荷分散構成考慮
			/// プロセスID：Webガーデン考慮
			/// 高分解能カウンター：.NET のDateTimeは精度がいまいちなので、高分解能カウンターを使用
			/// 補助カウンタ：上記３つで、重複する場合の考慮
			/// </remarks>
			/// <returns>SessionID文字列</returns>
			public static string Generate() {
				lock (_oSyncRoot) {
					long lCounter;
					QueryPerformanceCounter(out lCounter);
					StringBuilder oSessionIdBuilder = new StringBuilder();
					oSessionIdBuilder.Append(Environment.MachineName);

					// プロセスID
					oSessionIdBuilder.AppendFormat(Convert2RadixString((ulong)System.Diagnostics.Process.GetCurrentProcess().Id).PadLeft(6,'0'));

					// 高分解能パフォーマンスカウンタのカウンタ値(DateTime.Nowは精度が微妙)
					oSessionIdBuilder.AppendFormat(Convert2RadixString((ulong)lCounter).PadLeft(12,'0'));

					string sSessionIdBase = oSessionIdBuilder.ToString();

					// 補助カウンター
					_iSessionIdSuffix = (_iSessionIdSuffix + 1) % 100000;

					return string.Format("{0}{1:X4}",sSessionIdBase,Convert2RadixString(_iSessionIdSuffix).PadLeft(4,'0'));
				}
			}

			/// <summary>
			/// 36進数文字列に変換
			/// </summary>
			/// <param name="pValue">変換対象の数値</param>
			/// <returns>36進数文字列</returns>
			public static string Convert2RadixString(ulong pValue) {

				if (pValue == 0) {
					return "0";
				}

				StringBuilder oStringBuilder = new StringBuilder(41);
				ulong lCurrentValue = pValue;

				ulong lDigit;
				do {
					lDigit = lCurrentValue % (ulong)36;
					lCurrentValue = lCurrentValue / (ulong)36;

					oStringBuilder.Insert(0,GetNumberFromDigit((int)lDigit));
				}
				while (lCurrentValue != 0);

				return oStringBuilder.ToString();
			}

			private static char GetNumberFromDigit(int pDigit) {
				return (pDigit < 10) ? (char)('0' + pDigit) : (char)('a' + pDigit - 10);
			}

		}
	}
}
