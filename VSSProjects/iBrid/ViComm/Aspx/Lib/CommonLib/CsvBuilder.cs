using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Web.UI.WebControls;

namespace ViComm {
	
	/// <summary>
	/// CSV文字列を生成する機能を提供するHelperクラス
	/// </summary>
	/// <remarks>
	/// ※TSVにも対応
	/// </remarks>
	public class CsvBuilder {
		/*** delegate ***/
		protected delegate string AppendValueFunc(string pValue);
		
		/*** static field & const ***/
		private const string DEFAULT_SEPARAATOR = ",";
		private readonly static Regex brRegex = new Regex(@"<br[\s]*[/]{0,1}>", RegexOptions.IgnoreCase | RegexOptions.Compiled);

		/*** field ***/
		private string separator = null;
		private StringBuilder csvBuilder = new StringBuilder();

		public CsvBuilder():this(DEFAULT_SEPARAATOR) { }
		public CsvBuilder(string pSeparator) { 
			this.separator = pSeparator;
		}


		#region □■□ (+)public methods □■□ =======================================================================
		/// <summary>
		/// ヘッダーを追加する
		/// </summary>
		/// <param name="pValues">ヘッダーの項目リスト</param>
		public void AppendHeader(IList<string> pValues) {
			if( pValues == null ) throw new ArgumentNullException("pValues");
			
			AppendValueFunc oFunc = delegate(string pValue) { 
				// <br/>タグを除去する
				return brRegex.Replace(pValue, string.Empty); 
			};
			this.AppendLine(pValues,oFunc);
		}
		
		#region □ AppendData オーバーロード □ -------------------------------
		/// <summary>
		/// データ行を追加する。
		/// </summary>
		/// <param name="pDt">データを格納するDataTable</param>
		/// <param name="pFieldNames">出力対象となるDataTable内の列名リスト</param>
		public void AppendData(DataTable pDt, IList<string> pFieldNames) {
			if (pDt == null) throw new ArgumentNullException("pDt");
			if (pFieldNames == null) throw new ArgumentNullException("pFieldNames");

			foreach (DataRow oDr in pDt.Rows) {
				this.AppendData(oDr,pFieldNames);	
			}
		}
		/// <summary>
		/// データ行を追加する。
		/// </summary>
		/// <param name="pDr">データを格納するDataRow</param>
		/// <param name="pFieldNames">出力対象となるDataRow内の列名リスト</param>
		public void AppendData(DataRow pDr, IList<string> pFieldNames) {
			if (pDr == null) throw new ArgumentNullException("pDr");
			if (pFieldNames == null) throw new ArgumentNullException("pFieldNames");

			/*
			 * リストに詰め替える処理が無駄だが、さほどパフォーマンスへの影響は少ないので、
			 * 保守性を優先しリスト化し共通の行出力処理(AppendLine)を利用するようにしている。
			 */
			IList<string> oValues = new List<string>();
			for (int i = 0; i < pFieldNames.Count; i++) {
				oValues.Add(pDr[pFieldNames[i]].ToString());
			}
			
			this.AppendData(oValues);
		}
		/// <summary>
		/// データ行を追加する。
		/// </summary>
		/// <param name="pValues">値のリスト</param>
		public void AppendData(IList<string> pValues){
			if (pValues == null) throw new ArgumentNullException("pValues");
			this.AppendLine(pValues,null);
		}
		#endregion ------------------------------------------------------------
		
		public override string ToString() {
			return csvBuilder.ToString();
		}
		
		#endregion ====================================================================================================


		#region □■□ (-) private methods □■□ =====================================================================

		private void AppendLine(IList<string> pValues, AppendValueFunc pAppendValueFunc) {
			if (pValues == null) throw new ArgumentNullException("pValues");
			
			// 拡張処理が指定されてない場合は値をそのまま使うようにする
			if(pAppendValueFunc == null){pAppendValueFunc = delegate(string pValue){return pValue;};}
			
			for (int i = 0; i < pValues.Count; i++) {
				if (i != 0) csvBuilder.Append(separator);
				csvBuilder.Append(pAppendValueFunc(pValues[i]));
			}
			csvBuilder.AppendLine();
		}

		#endregion ====================================================================================================

	}
	
	/// <summary>
	/// GridViewの列情報を元にCSV文字列を生成する機能を提供するクラス
	/// </summary>
	public class GridViewCsvBuilder:CsvBuilder{
		GridView grid;
		DataTable dataTable;
		
		IList<string> headerValues = new List<string>();
		IList<string> dataFieldNames = new List<string>();

		public GridViewCsvBuilder(GridView pGrid,DataTable pDataTable) {
			if (pGrid == null) throw new ArgumentNullException("pGrid");
			if (pDataTable == null) throw new ArgumentNullException("pDataTable");
			
			this.grid = pGrid;
			this.dataTable = pDataTable;
		}
		
		public void AppendMapping(int pGridViewColumnIndex,string pDataFieldName){
			if (pGridViewColumnIndex < 0) throw new ArgumentOutOfRangeException("pGridViewColumnIndex");
			if (pDataFieldName == null) throw new ArgumentNullException("pDataFieldName");
			
			DataControlField oGridColumn = grid.Columns[pGridViewColumnIndex];
			if(oGridColumn.Visible){
				headerValues.Add(oGridColumn.HeaderText);
				dataFieldNames.Add(pDataFieldName);
			}
		}

		public override string ToString() {
			base.AppendHeader(this.headerValues);
			base.AppendData(this.dataTable,this.dataFieldNames);
			return base.ToString();
		}
	}
	
	
}
