﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using System.Text;

/// <summary>
/// EventLogWriter の概要の説明です

/// </summary>
public class EventLogWriter {
	private enum LogLevel:int {
		Error = 0,
		Warn = 1,
		Info = 2,
		Debug = 3
	}

	private static readonly LogLevel _logLevel;
	private const string SOURCE = "ViCOMM";
	private const string LOG = "Application";

	public static bool EnabledDebug {
		get {
			return (_logLevel >= LogLevel.Debug);
		}
	}

	static EventLogWriter() {
		string sLogLebel = ConfigurationManager.AppSettings["logLevel"];
		_logLevel = LogLevel.Error;

		int iLogLebel = int.MinValue;
		if (int.TryParse(sLogLebel,out iLogLebel)) {
			switch (iLogLebel) {
				case 0:
					_logLevel = LogLevel.Error;
					break;
				case 1:
					_logLevel = LogLevel.Warn;
					break;
				case 2:
					_logLevel = LogLevel.Info;
					break;
				case 3:
					_logLevel = LogLevel.Debug;
					break;
			}
		}

	}

	public static void Error(Exception pEx) {
		string sRoot = ConfigurationManager.AppSettings["Root"].ToString().ToLower();
		string err = "";

		err = "ViCOMM User Error Caught in Application_Error event\n" +
			 "\nError Message:" + pEx.Message.ToString() +
			 "\nStack Trace:" + pEx.StackTrace.ToString();

		if (!EventLog.SourceExists(SOURCE)) {
			EventLog.CreateEventSource(SOURCE,LOG);
		}

		EventLog.WriteEntry(SOURCE,err,EventLogEntryType.Error);
	}

	public static void Debug(string sMessage,params object[] pParams) {
		if (string.IsNullOrEmpty(sMessage))
			return;

		if (_logLevel < LogLevel.Debug)
			return;

		if (!EventLog.SourceExists(SOURCE)) {
			EventLog.CreateEventSource(SOURCE,LOG);
		}

		EventLog.WriteEntry(SOURCE,string.Format(sMessage,pParams),EventLogEntryType.Information);

	}
	public static void Debug(object sMessage) {
		Debug(sMessage.ToString(),new object[] { });
	}
	public static void Force(string sMessage,params object[] pParams) {

		if (string.IsNullOrEmpty(sMessage))
			return;

		if (!EventLog.SourceExists(SOURCE)) {
			EventLog.CreateEventSource(SOURCE,LOG);
		}
		string sLog;
		if (pParams.Length > 0) {
			sLog = string.Format(sMessage,pParams);
		} else {
			sLog = sMessage;
		}
		
		int iFailSafe = 0;
		while (Encoding.GetEncoding(932).GetByteCount(sLog) > 32000 && iFailSafe <= 10) {
			sLog = sLog.Substring(0,sLog.Length - 10);
			iFailSafe++;
		};

		EventLog.WriteEntry(SOURCE,sLog,EventLogEntryType.Information);

	}
	public static void Force(object sMessage) {
		Force(sMessage.ToString(),new object[] { });
	}

}
