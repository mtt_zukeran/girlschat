/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 楽天あんしん支払いサービスのユーザー認証機能に関する機能を提供するクラス
--	Progaram ID		: UserAuth
--
--  Creation Date	: 2010.08.23
--  Creater			: K.Itoh
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Net;
using System.IO;

namespace ViComm.Rakuten {

	/// <summary>
	///  楽天あんしん支払いサービスのユーザー認証機能に関する機能を提供するクラス
	/// </summary>
	public class UserAuth {
		private const string DEFAULT_BASE_URL = "https://api.id.rakuten.co.jp/openid/auth";
		
		#region □■□ ユーザー認証 関連 □■□ =======================================================================
		/// <summary>
		/// 認証要求URLを生成する
		/// </summary>
		/// <remarks>
		/// 機能：「会員認証」
		/// </remarks>
		/// <param name="pWithNickName">ニックネーム情報を取得するかどうかを示す値。true:取得する</param>
		/// <param name="pReturnTo">戻り先URL</param>
		/// <returns>認証要求URL</returns>
		public string GenerateUserAuthRequestUrl(bool pWithNickName,string pReturnTo){
			if (string.IsNullOrEmpty(pReturnTo)) throw new ArgumentException("pReturnTo");
		
			// 基本となるURL
			string sBaseUrl = this.GetBaseUrl();
			
			UrlBuilder oUrlBuilder = new UrlBuilder(sBaseUrl);
			// 固定値
			oUrlBuilder.Parameters.Add("openid.ns", "http://specs.openid.net/auth/2.0");
			// 戻り先URL
			oUrlBuilder.Parameters.Add("openid.return_to", HttpUtility.UrlEncode(pReturnTo));
			// 固定値
			oUrlBuilder.Parameters.Add("openid.claimed_id", "http://specs.openid.net/auth/2.0/identifier_select");
			// 固定値
			oUrlBuilder.Parameters.Add("openid.identity", "http://specs.openid.net/auth/2.0/identifier_select");
			// 固定値
			oUrlBuilder.Parameters.Add("openid.mode", "checkid_setup");
			
			if(pWithNickName){
				// 固定値
				oUrlBuilder.Parameters.Add("openid.ns.ax", "http://openid.net/srv/ax/1.0");
				// 固定値
				oUrlBuilder.Parameters.Add("openid.ax.mode", "fetch_request");
				// 固定値
				oUrlBuilder.Parameters.Add("openid.ax.type.nickname", "http://schema.openid.net/namePerson/friendly");
				// 固定値
				oUrlBuilder.Parameters.Add("openid.ax.required", "nickname");
			}
			
			return oUrlBuilder.ToString();
		}
		
		#endregion ====================================================================================================

		#region □■□ ユーザー認証結果 関連 □■□ ===================================================================
		
		/// <summary>
		/// ユーザー認証結果のRequestからOpenID情報を抽出する。
		/// </summary>
		/// <param name="pHttpRequest">ユーザー認証結果のRequest</param>
		/// <returns>OpenID情報</returns>
		public UserAuthResultEntity ParseUserAuthResult(HttpRequest pHttpRequest){
			if (pHttpRequest == null) throw new ArgumentNullException("pHttpRequest");

			string sIdeneity = pHttpRequest.Params["openid.identity"];
			string sNickName = pHttpRequest.Params["openid.ax.value.nickname"];	
			
			return new UserAuthResultEntity(sIdeneity,sNickName);
		}
		/// <summary>
		/// ユーザー認証結果確認要求を楽天に送信し認証の妥当性確認を行う
		/// </summary>
		/// <param name="pCheckAuthenticaionUrl">ユーザー認証結果リクエストURL</param>
		/// <returns>結果。true:成功、false：失敗</returns>
		public bool CheckAuthenticaion(string pCheckAuthenticaionUrl) {
			bool bIsOk = false;

			EventLogWriter.Debug("[START]楽天認証結果問い合わせ{0}{1}", Environment.NewLine, pCheckAuthenticaionUrl);

			WebRequest oWebRequest = HttpWebRequest.Create(pCheckAuthenticaionUrl);
			oWebRequest.Timeout = 10000; // 10s 設定値にするまでも無いので固定で定義

			WebResponse oWebResponse = oWebRequest.GetResponse();
			using(StreamReader oSr = new StreamReader(oWebResponse.GetResponseStream())){
				
				string sFirstLine = oSr.ReadLine();

				EventLogWriter.Debug("[ END ]楽天認証結果問い合わせ{0}{1}", Environment.NewLine, sFirstLine);

				bIsOk = (sFirstLine.Equals("is_valid:true"));
				
				oSr.Close();
			}
			
			return bIsOk;
		}
		
		/// <summary>
		/// ユーザー認証結果リクエストURLを生成する
		/// </summary>
		/// <param name="pHttpRequest">ユーザー認証結果のRequest</param>
		/// <returns>ユーザー認証結果リクエストURL</returns>
		public string GenerateCheckAuthenticaionUrl(HttpRequest pRequest){
			// 基本となるURL
			string sBaseUrl = this.GetBaseUrl();

			UrlBuilder oUrlBuilder = new UrlBuilder(sBaseUrl);

			// リクエストから引き継ぐパラメータの値引継ぎ処理			
			IList<string> oTransferParamNameList = new List<string>();
			oTransferParamNameList.Add("openid.ns");
			oTransferParamNameList.Add("openid.op_endpoint");
			oTransferParamNameList.Add("openid.claimed_id");
			oTransferParamNameList.Add("openid.response_nonce");
			oTransferParamNameList.Add("openid.identity");
			oTransferParamNameList.Add("openid.return_to");
			oTransferParamNameList.Add("openid.assoc_handle");
			oTransferParamNameList.Add("openid.signed");
			oTransferParamNameList.Add("openid.sig");
			oTransferParamNameList.Add("openid.ns.ax");
			oTransferParamNameList.Add("openid.ax.mode");
			oTransferParamNameList.Add("openid.ax.type.nickname");
			oTransferParamNameList.Add("openid.ax.value.nickname");

			foreach (string sTransferParamName in oTransferParamNameList) {
				string sRequestValue = pRequest.Params[sTransferParamName];
				if (!string.IsNullOrEmpty(sRequestValue)) {
					oUrlBuilder.Parameters.Add(sTransferParamName, HttpUtility.UrlEncode(sRequestValue));
				}
			}
			// 固定値
			oUrlBuilder.Parameters.Add("openid.mode", "check_authentication");
			
			return oUrlBuilder.ToString();
		}

		#endregion ====================================================================================================

		private string GetBaseUrl(){
			string sConfigBaseUrl = ConfigurationManager.AppSettings["Rakuten.UserAuth.Url"];
			return (!string.IsNullOrEmpty(sConfigBaseUrl))?sConfigBaseUrl:DEFAULT_BASE_URL;
		}
	}
}
