using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace ViComm.Rakuten {
	public class RakutenUtil {
		private const char ITEM_ID_SEPARATOR = '-';
		
		public static string Convert2ItemId(string pSiteCd,string pSettleType,int pSalesAmt){
			return string.Format("{1}{0}{2}{0}{3}", ITEM_ID_SEPARATOR, pSiteCd, pSettleType, pSalesAmt);
		}
		
		public static void Convert2PackKey(string pItemId,out string pSiteCd, out string pSettleType, out int pSalesAmt){
			string[] oItemIdParts = pItemId.Split(new char[] { ITEM_ID_SEPARATOR });
			if(oItemIdParts.Length!=3){
				throw new ArgumentException(string.Format("ItemIdの書式が正しくありません。itemId:{0}",pItemId));
			}
			
			pSiteCd = oItemIdParts[0].Trim();
			pSettleType = oItemIdParts[1].Trim();
			pSalesAmt = int.Parse(oItemIdParts[2].Trim());
		}
		
		public static string SanitizeXml(string pValue){
			pValue = pValue.Replace("&", "&amp;");
			pValue = pValue.Replace("<", "&lt;");
			pValue = pValue.Replace(">", "&gt;");
			pValue = pValue.Replace("'", "&apos;");
			pValue = pValue.Replace("\"", "&quot;");
			return pValue;
		}

		#region □■□ 設定関連 □■□ ================================================================================

		public static bool IsTMode {
			get{
				string sConfIsTMode = RakutenUtil.GetAppSettings("Rakuten.Checkout.IsTMode", false) ?? string.Empty;
				return sConfIsTMode.Equals("1");
			}
			
		}
		/// <summary>
		/// 指定されたキーに該当する値を設定ファイルから読み込む
		/// </summary>
		/// <param name="pKey">設定キー</param>
		/// <param name="pNeedRequiredCheck">必須チェックを行うかどうかを示す値。</param>
		/// <returns>設定キーに該当する設定値</returns>
		public static string GetAppSettings(string pKey, bool pNeedRequiredCheck) {
			string sValue = ConfigurationManager.AppSettings[pKey];
			if (pNeedRequiredCheck && string.IsNullOrEmpty(sValue)) {
				string sErrorMessage = string.Format("{0}が設定されていません。", pKey);
				throw new ConfigurationErrorsException(sErrorMessage);
			}
			return sValue;
		}
		#endregion ====================================================================================================

		
	}
}
