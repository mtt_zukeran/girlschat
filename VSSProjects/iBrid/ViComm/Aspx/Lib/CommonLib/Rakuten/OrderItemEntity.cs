using System;
using System.Collections.Generic;
using System.Text;

namespace ViComm.Rakuten {
	/// <summary>商品を表現するEntityクラス</summary>
	[Serializable]
	public class OrderItemEntity {
		
		private string itemId;
		private string itemName;
		private int itemNumbers;
		private decimal itemFee;
		private string itemSubId;

		/// <summary>商品IDを取得または設定する</summary>
		public string ItemId {
			get { return this.itemId; }
			set { this.itemId = value; }
		}
		/// <summary>商品名を取得または設定する</summary>
		public string ItemName {
			get { return this.itemName; }
			set { this.itemName = value; }
		}
		/// <summary>個数を取得または設定する</summary>
		public int ItemNumbers {
			get { return this.itemNumbers; }
			set { this.itemNumbers = value; }
		}
		/// <summary>商品単価(税込)を取得または設定する</summary>
		public decimal ItemFee {
			get { return this.itemFee; }
			set { this.itemFee = value; }
		}
		/// <summary>商品サブIDを取得または設定する</summary>
		public string ItemSubId {
			get { return this.itemSubId; }
			set { this.itemSubId = value; }
		}

	}
}
