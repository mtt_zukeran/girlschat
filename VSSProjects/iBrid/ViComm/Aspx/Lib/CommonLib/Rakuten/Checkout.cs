using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Security;
using System.Security.Cryptography;

namespace ViComm.Rakuten {

	/// <summary>
	/// 楽天あんしん支払いサービスの動的商品情報送信処理に関する機能を提供するクラス
	/// </summary>
	public class Checkout {
		private readonly string authMethod = "1"; //SHA1固定
		private readonly string isTMode;

		private static readonly string ItemXmlTemplate;
		private static readonly string OrderItemsInfoXmlTemplate;
		private static readonly Encoding ENC = Encoding.UTF8;
		
		/// <summary>タイプイニシャライザ</summary>
		static Checkout() {
			// XMLテンプレートの生成
			ItemXmlTemplate = GenerateItemXmlTemplate();
			OrderItemsInfoXmlTemplate = GenerateOrderItemsInfoXmlTemplate();
		}

		/// <summary>コンストラクタ</summary>
		public Checkout() {
			// ==============================
			//  設定値を読み込む
			// ==============================
			string sConfIsTMode = RakutenUtil.GetAppSettings("Rakuten.Checkout.IsTMode", false) ?? string.Empty;
			
			this.isTMode = sConfIsTMode;
		}


		#region □■□ 動的商品情報送信用のHTMLフォームアイテム生成 □■□ ====================================================

		/// <summary>
		/// 動的商品情報送信用のHTMLフォームアイテムを生成する
		/// </summary>
		/// <param name="pServiceId">サービスID</param>
		/// <param name="pAccessKey">アクセスキー</param>
		/// <param name="pCartId">カートID</param>
		/// <param name="pItem">商品</param>
		/// <param name="pOrderCompleteUrl">決済終了時戻りURL</param>
		/// <param name="pOrderFailedUrl">決済失敗時戻りURL</param>
		/// <returns>動的商品情報送信用のHTMLフォーム(文字列)</returns>
		public string GenerateCheckoutHtmlFormItem(string pServiceId, string pAccessKey, string pCartId, OrderItemEntity pItem, string pOrderCompleteUrl, string pOrderFailedUrl) {
			IList<OrderItemEntity> oItemList = new List<OrderItemEntity>();
			oItemList.Add(pItem);
			return this.GenerateCheckoutHtmlFormItem(pServiceId, pAccessKey, pCartId, oItemList, pOrderCompleteUrl, pOrderFailedUrl);
		}
		/// <summary>
		/// 動的商品情報送信用のHTMLフォームアイテムを生成する
		/// </summary>
		/// <param name="pServiceId">サービスID</param>
		/// <param name="pAccessKey">アクセスキー</param>
		/// <param name="pItemList">商品リスト</param>
		/// <param name="pOrderCompleteUrl">購入商品情報をBase64エンコードした文字列</param>
		/// <param name="pOrderFailedUrl">デジタル署名(文字列)</param>
		/// <returns>動的商品情報送信用のHTMLフォーム(文字列)</returns>
		public string GenerateCheckoutHtmlFormItem(string pServiceId, string pAccessKey, string pCartId, IList<OrderItemEntity> pItemList, string pOrderCompleteUrl, string pOrderFailedUrl) {
			string sOrderItemsInfoXml = this.GenerateOrderItemsInfoXml(pCartId, pServiceId, pItemList, pOrderCompleteUrl, pOrderFailedUrl);
			string sCheckoutString = this.GenerateCheckoutString(sOrderItemsInfoXml);
			string sSignature = this.GenerateSignatureString(pAccessKey,sOrderItemsInfoXml);
			return GenerateCheckoutHtmlFormItem(sCheckoutString, sSignature);
			
		}
		/// <summary>
		/// 動的商品情報送信用のHTMLフォームアイテムを生成する
		/// </summary>
		/// <param name="pCheckoutString">商品リスト</param>
		/// <param name="pSignatureString">デジタル署名(文字列)</param>
		/// <returns>動的商品情報送信用のHTMLフォーム(文字列)</returns>
		public string GenerateCheckoutHtmlFormItem( string pCheckoutString, string pSignatureString) {
			StringBuilder oCheckoutHtmlFormBuilder = new StringBuilder();
			oCheckoutHtmlFormBuilder.AppendFormat("  <input type='hidden' name='checkout' value='{0}' />   ", pCheckoutString).AppendLine();
			oCheckoutHtmlFormBuilder.AppendFormat("  <input type='hidden' name='sig'      value='{0}' />   ", pSignatureString).AppendLine();

			EventLogWriter.Debug("動的商品情報送信用のHTMLフォームアイテム{0}{1}", Environment.NewLine, oCheckoutHtmlFormBuilder);
			return oCheckoutHtmlFormBuilder.ToString();
		}
		
		#endregion ====================================================================================================

	
		#region □■□ XML関連 □■□ =================================================================================

		public string GenerateOrderItemsInfoXml(string pCartId, string pServiceId, IList<OrderItemEntity> pItemList, string pOrderCompleteUrl, string pOrderFailedUrl) {
			decimal dOrderTotalFee = CalculateOrderTotalFee(pItemList);
			return this.GenerateOrderItemsInfoXml(pCartId, pServiceId,pItemList, pOrderCompleteUrl, pOrderFailedUrl, dOrderTotalFee);
		}

		public string GenerateOrderItemsInfoXml(string pCartId, string pServiceId, IList<OrderItemEntity> pItemList, string pOrderCompleteUrl, string pOrderFailedUrl, decimal pOrderTotalFee) {
		
			string sOrderItemsInfoXml = (string)OrderItemsInfoXmlTemplate.Clone();

			sOrderItemsInfoXml = sOrderItemsInfoXml.Replace(":serviceId", RakutenUtil.SanitizeXml(pServiceId));
			sOrderItemsInfoXml = sOrderItemsInfoXml.Replace(":itemsInfo", this.GenerateItemsXml(pItemList));
			sOrderItemsInfoXml = sOrderItemsInfoXml.Replace(":orderCartId", RakutenUtil.SanitizeXml(pCartId));
			sOrderItemsInfoXml = sOrderItemsInfoXml.Replace(":orderCompleteUrl", RakutenUtil.SanitizeXml(pOrderCompleteUrl));
			sOrderItemsInfoXml = sOrderItemsInfoXml.Replace(":orderFailedUrl", RakutenUtil.SanitizeXml(pOrderFailedUrl));
			sOrderItemsInfoXml = sOrderItemsInfoXml.Replace(":orderTotalFee", RakutenUtil.SanitizeXml(pOrderTotalFee.ToString()));
			sOrderItemsInfoXml = sOrderItemsInfoXml.Replace(":authMethod", RakutenUtil.SanitizeXml(this.authMethod));
			sOrderItemsInfoXml = sOrderItemsInfoXml.Replace(":isTMode", RakutenUtil.SanitizeXml(this.isTMode));

			return sOrderItemsInfoXml;
		}
		
		public string GenerateItemsXml(IList<OrderItemEntity> pItemList){
			StringBuilder xmlBuilder = new StringBuilder();
			
			foreach(OrderItemEntity oItem in pItemList){
				string sItemXml = (string)ItemXmlTemplate.Clone();
				sItemXml= sItemXml.Replace(":itemId", RakutenUtil.SanitizeXml(oItem.ItemId));
				sItemXml = sItemXml.Replace(":itemName", RakutenUtil.SanitizeXml(oItem.ItemName));
				sItemXml = sItemXml.Replace(":itemSubId", RakutenUtil.SanitizeXml(Hash(oItem.ItemSubId)));
				sItemXml = sItemXml.Replace(":itemNumbers", RakutenUtil.SanitizeXml(oItem.ItemNumbers.ToString()));
				sItemXml = sItemXml.Replace(":itemFee", RakutenUtil.SanitizeXml(oItem.ItemFee.ToString()));
				xmlBuilder.Append(sItemXml);		
			}
			
			return xmlBuilder.ToString();
		}
		
		private static string GenerateItemXmlTemplate() {
			StringBuilder oXmlTemplateBuilder = new StringBuilder();
			oXmlTemplateBuilder.Append("<item>").AppendLine();
			oXmlTemplateBuilder.Append("  <itemId>:itemId</itemId>").AppendLine();
			oXmlTemplateBuilder.Append("  <itemName>:itemName</itemName>").AppendLine();
			oXmlTemplateBuilder.Append("  <itemSubId>:itemSubId</itemSubId>").AppendLine();
			oXmlTemplateBuilder.Append("  <itemNumbers>:itemNumbers</itemNumbers>").AppendLine();
			oXmlTemplateBuilder.Append("  <itemFee>:itemFee</itemFee>").AppendLine();
			oXmlTemplateBuilder.Append("</item>").AppendLine();
			return oXmlTemplateBuilder.ToString();
		}

		private static string GenerateOrderItemsInfoXmlTemplate() {

			StringBuilder oXmlTemplateBuilder = new StringBuilder();
			oXmlTemplateBuilder.Append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").AppendLine();
			oXmlTemplateBuilder.Append("<orderItemsInfo>").AppendLine();
			oXmlTemplateBuilder.Append("  <serviceId>:serviceId</serviceId>").AppendLine();
			oXmlTemplateBuilder.Append("  <itemsInfo>").AppendLine();
			oXmlTemplateBuilder.Append(":itemsInfo").AppendLine();
			oXmlTemplateBuilder.Append("  </itemsInfo>").AppendLine();
			oXmlTemplateBuilder.Append("  <orderCartId>:orderCartId</orderCartId>").AppendLine();
			oXmlTemplateBuilder.Append("  <orderCompleteUrl>:orderCompleteUrl</orderCompleteUrl>").AppendLine();
			oXmlTemplateBuilder.Append("  <orderFailedUrl>:orderFailedUrl</orderFailedUrl>").AppendLine();
			oXmlTemplateBuilder.Append("  <orderTotalFee>:orderTotalFee</orderTotalFee>").AppendLine();
			oXmlTemplateBuilder.Append("  <authMethod>:authMethod</authMethod>").AppendLine();
			oXmlTemplateBuilder.Append("  <isTMode>:isTMode</isTMode>").AppendLine();
			oXmlTemplateBuilder.Append("</orderItemsInfo>").AppendLine();

			return oXmlTemplateBuilder.ToString();
		}
		

		#endregion ====================================================================================================



		
		/// <summary>
		/// 購入商品情報をBase64エンコードした文字列を生成する
		/// </summary>
		/// <param name="pOrderItemsInfoXml">購入商品情報(XML文字列)</param>
		/// <returns>購入商品情報をBase64エンコードした文字列</returns>
		public string GenerateCheckoutString(string pOrderItemsInfoXml) {
			return Convert.ToBase64String(ENC.GetBytes(pOrderItemsInfoXml));
		}

		/// <summary>
		/// デジタル署名(文字列)を生成する
		/// </summary>
		/// <param name="pAccessKey">アクセスキー</param>
		/// <param name="pOrderItemsInfoXml">購入商品情報(XML文字列)</param>
		/// <returns>デジタル署名(文字列)</returns>
		public string GenerateSignatureString(string pAccessKey,string pOrderItemsInfoXml) {
			int iBlockSize = 64;
			HashAlgorithm oHashAlgorithm = new SHA1CryptoServiceProvider();

			byte[] oBinKey = null;
			if (pAccessKey.Length > iBlockSize) {
				oBinKey = oHashAlgorithm.ComputeHash(ENC.GetBytes(pAccessKey));
			} else {
				oBinKey = ENC.GetBytes(pAccessKey);
			}
			oBinKey = PadRightByteArray(oBinKey,iBlockSize,0x00);
			byte[] oBinInnerPad = PadRightByteArray(new byte[0], iBlockSize, 0x36);
			byte[] oBinOuterPad = PadRightByteArray(new byte[0], iBlockSize, 0x5c);
			byte[] oBinData = ENC.GetBytes(pOrderItemsInfoXml);
		
			byte[] oBinInnerHash = oHashAlgorithm.ComputeHash(ConcatByteArray(XorByteArray(oBinInnerPad, oBinKey), oBinData));
			byte[] oBinSig = oHashAlgorithm.ComputeHash(ConcatByteArray(XorByteArray(oBinOuterPad, oBinKey), oBinInnerHash));
			
			return BitConverter.ToString(oBinSig).Replace("-", string.Empty).ToLower();
		}
		
		private string Hash(string pValue){
			HashAlgorithm oHashAlgorithm = new SHA1CryptoServiceProvider();
			return BitConverter.ToString(oHashAlgorithm.ComputeHash(ENC.GetBytes(pValue))).Replace("-", string.Empty).ToLower();
		}
		
		private byte[] PadRightByteArray(byte[] pArray,int totalWidth,byte pValue){
			if (pArray.Length > totalWidth) return pArray;
			
			byte[] oResult = new byte[totalWidth];
			Array.Copy(pArray,0,oResult,0,pArray.Length);
			for(int i=pArray.Length;i<oResult.Length;i++){
				oResult[i] = pValue;
			}
			return oResult;
		}
		
		/// <summary>
		/// バイト配列を結合する
		/// </summary>
		/// <param name="pArray1">バイト配列１</param>
		/// <param name="pArray2">バイト配列２</param>
		/// <returns>バイト配列１、２を結合したバイト配列</returns>
		private byte[] ConcatByteArray(byte[] pArray1,byte[] pArray2){
			byte[] oResult = new byte[pArray1.Length+pArray2.Length];
			Array.Copy(pArray1, 0, oResult, 0, pArray1.Length);
			Array.Copy(pArray2, 0, oResult, pArray1.Length, pArray2.Length);
			return oResult;
		}
		/// <summary>
		/// バイト配列の各要素の排他的論理和を算出する
		/// </summary>
		/// <param name="pValue1">バイト配列１</param>
		/// <param name="pValue2">バイト配列２</param>
		/// <returns>バイト配列の各要素の排他的論理和</returns>
		private byte[] XorByteArray(byte[] pValue1, byte[] pValue2){
		    if(pValue1.Length != pValue2.Length)throw new ArgumentException();
			
		    byte[] result = new byte[pValue1.Length];
		    for(int i=0;i<pValue1.Length;i++){
				result[i] = (byte)(pValue1[i] ^ (byte)pValue2[i]);
		    }
		    return result;
		}
		

		/// <summary>
		/// 注文商品の商品合計金額を算出する。
		/// </summary>
		/// <param name="pItemList">対象の注文商品情報</param>
		/// <returns>商品合計金額</returns>
		private decimal CalculateOrderTotalFee(IList<OrderItemEntity> pItemList) {
			// 商品合計金額の算出
			decimal dOrderTotalFee = decimal.Zero;
			foreach (OrderItemEntity oItem in pItemList) {
				dOrderTotalFee += oItem.ItemFee * oItem.ItemNumbers;
			}
			return dOrderTotalFee;
		}

	
	}
}
