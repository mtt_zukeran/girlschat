using System;
using System.Collections.Generic;
using System.Text;

namespace ViComm.Rakuten {
	/// <summary>
	/// OpenID情報を表現するEntityクラス
	/// </summary>
	public class UserAuthResultEntity {
		private string openId;
		private string nickName;

		public string OpenId{
			get{ return this.openId;}
		}
		
		public string NickName{
			get{ return this.nickName;}
		}

		public UserAuthResultEntity(string pOpenId,string pNickName) {
			this.openId = pOpenId;
			this.nickName = pNickName;
		}

		public override string ToString() {
			StringBuilder oSb = new StringBuilder();
			oSb.AppendFormat("OpenId:{0}",this.openId).AppendLine();
			oSb.AppendFormat("NickName:{0}", this.nickName).AppendLine();
			return oSb.ToString();
		}
	}
}
