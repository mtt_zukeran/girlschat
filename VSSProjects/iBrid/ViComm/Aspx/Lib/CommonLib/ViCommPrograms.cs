﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Web;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Oracle.DataAccess.Client;
using System.Security.Cryptography;

namespace ViComm {

	public class ViCommPrograms {


		public static IDictionary<string,string> ConvertToDictionary(NameValueCollection oNameValueCollection) {
			IDictionary<string,string> oDict = new Dictionary<string,string>();

			foreach (string sKey in oNameValueCollection.AllKeys) {
				if (string.IsNullOrEmpty(sKey))
					continue;
				oDict.Add(sKey,oNameValueCollection[sKey]);
			}

			return oDict;
		}

		public static void GetCalendarText(string pYearMonth,ref string[] pDate,ref int[] pDayOfWeek) {
			pDate = new string[42];
			pDayOfWeek = new int[42];

			DateTime dDate = new DateTime();
			DateTime dFirstDate = DateTime.Parse(pYearMonth + "/01" + " 00:00:00");
			string sFirstWeekDay = dFirstDate.DayOfWeek.ToString();
			DateTime dLastDay = DateTime.Parse(pYearMonth + "/01" + " 00:00:00").AddMonths(1).AddSeconds(-1);
			string sLastWeekDay = dLastDay.DayOfWeek.ToString();

			int iLastMonthDay = GetLastMonthDay(sFirstWeekDay);
			for (int i = 0;i < iLastMonthDay;i++) {
				dDate = dFirstDate.AddDays(i - iLastMonthDay);
				pDate.SetValue(dDate.ToShortDateString(),i);
				pDayOfWeek.SetValue(dDate.DayOfWeek,i);
			}

			for (DateTime i = dFirstDate;i <= dLastDay;i = i.AddDays(1)) {
				pDate.SetValue(i.ToShortDateString(),iLastMonthDay);
				pDayOfWeek.SetValue(i.DayOfWeek,iLastMonthDay);
				iLastMonthDay++;
			}

			int iNextMonthDay = 42 - iLastMonthDay;
			for (int i = 1;i <= iNextMonthDay;i++) {
				dDate = dLastDay.AddDays(i);
				pDate.SetValue(dDate.ToShortDateString(),iLastMonthDay);
				pDayOfWeek.SetValue(dDate.DayOfWeek,iLastMonthDay);
				iLastMonthDay++;
			}
		}

		public static string[] GetCalendarLink(string pBaseUrl,string[] pDate,string pUrl,string pMode,string pSessionID,string pUserSeq) {
			string[] sLink = new string[42];
			for (int i = 0;i < 42;i++) {
				sLink.SetValue(GetNavigateUrl(pBaseUrl,pSessionID,pUrl + "?mode=datechange&reportday=" + pDate[i] + "&userseq=" + pUserSeq),i);
			}
			return sLink;
		}

		private static string GetNavigateUrl(string pBaseUrl,string pSessionId,string pUrl) {
			return pBaseUrl + "/(S(" + pSessionId + "))/" + pUrl;
		}

		private static int GetLastMonthDay(string pWeekDay) {
			int iLastMonthDay = 0;

			if (pWeekDay.Equals("Sunday")) {
				iLastMonthDay = 0;
			} else if (pWeekDay.Equals("Monday")) {
				iLastMonthDay = 1;
			} else if (pWeekDay.Equals("Tuesday")) {
				iLastMonthDay = 2;
			} else if (pWeekDay.Equals("Wednesday")) {
				iLastMonthDay = 3;
			} else if (pWeekDay.Equals("Thursday")) {
				iLastMonthDay = 4;
			} else if (pWeekDay.Equals("Friday")) {
				iLastMonthDay = 5;
			} else if (pWeekDay.Equals("Saturday")) {
				iLastMonthDay = 6;
			}
			return iLastMonthDay;
		}

		public static int Age(string pDate) {
			DateTime dtBirthDay;
			if (DateTime.TryParse(pDate,out dtBirthDay)) {
				DateTime d3 = new DateTime(dtBirthDay.Year,1,1);
				d3 = d3.AddDays(DateTime.Today.DayOfYear - 1);
				return DateTime.Today.Year - dtBirthDay.Year + (dtBirthDay <= d3 ? 0 : -1);
			} else {
				return 0;
			}
		}

		public static bool MashupCheckAge(string pDate) {
			DateTime dtBirthDay;
			DateTime dtCheckDate = DateTime.Now.AddYears(-18);

			if (DateTime.TryParse(pDate,out dtBirthDay)) {
				if (dtCheckDate.ToString("MM/dd").CompareTo("04/02") < 0) {
					dtCheckDate = dtCheckDate.AddYears(-1);
				}
				dtCheckDate = DateTime.Parse(string.Format("{0}/04/01",dtCheckDate.Year));

				if (dtBirthDay > dtCheckDate) {
					return false;
				} else {
					return true;
				}
			} else {
				return false;
			}
		}

		public static void CreatePagingCountSql(string pBaseQuery,out string pPagingCountQuery) {
			if (string.IsNullOrEmpty(pBaseQuery))
				throw new ArgumentException("pBaseQuery");

			StringBuilder objSqlBuilder = new StringBuilder();
			objSqlBuilder.Append("	SELECT				").AppendLine();
			objSqlBuilder.Append("		COUNT(*)		").AppendLine();// ここはCOUNT KEYとなる列を指定できるようにすると尚良い、いずれ対応


			objSqlBuilder.Append("	FROM						").AppendLine();
			objSqlBuilder.AppendFormat("	({0}) TMP_INNER		",pBaseQuery).AppendLine();


			pPagingCountQuery = objSqlBuilder.ToString();
		}
		public static OracleParameter[] CreatePagingSql(string pBaseQuery,string pSortExpression,int pStartRowIndex,int pMaximumRows,out string pPagingQuery) {
			if (string.IsNullOrEmpty(pSortExpression))
				throw new ArgumentException("pSortExpression");
			return CreatePagingSql(pBaseQuery + Environment.NewLine + pSortExpression,pStartRowIndex,pMaximumRows,out pPagingQuery);
		}
		public static OracleParameter[] CreatePagingSql(string pBaseQuery,int pStartRowIndex,int pMaximumRows,out string pPagingQuery) {
			if (string.IsNullOrEmpty(pBaseQuery))
				throw new ArgumentException("pBaseQuery");

			System.Text.StringBuilder oSqlBuilder = new System.Text.StringBuilder();
			oSqlBuilder.Append(" SELECT                                            ").AppendLine();
			oSqlBuilder.Append(" 	*                                              ").AppendLine();
			oSqlBuilder.Append(" FROM                                              ").AppendLine();
			oSqlBuilder.Append(" (                                                 ").AppendLine();
			oSqlBuilder.Append(" 	SELECT                                         ").AppendLine();
			oSqlBuilder.Append(" 		INNER.*			,                          ").AppendLine();
			oSqlBuilder.Append(" 		ROWNUM AS RNUM	,                          ").AppendLine();
			oSqlBuilder.Append(" 		ROWNUM AS RANK                             ").AppendLine();
			oSqlBuilder.Append(" 	FROM                                           ").AppendLine();
			oSqlBuilder.Append(" 	(                                              ").AppendLine();
			oSqlBuilder.AppendFormat(" 		{0}                                    ",pBaseQuery).AppendLine();
			oSqlBuilder.Append(" 	) INNER                                        ").AppendLine();
			oSqlBuilder.Append(" 	WHERE ROWNUM <= :LAST_ROW                      ").AppendLine();
			oSqlBuilder.Append(" )                                                 ").AppendLine();
			oSqlBuilder.Append(" WHERE                                             ").AppendLine();
			oSqlBuilder.Append(" 	RANK > :FIRST_ROW                              ").AppendLine();

			List<OracleParameter> oParamList = new List<OracleParameter>();
			oParamList.Add(new OracleParameter(":LAST_ROW",pStartRowIndex + pMaximumRows));
			oParamList.Add(new OracleParameter(":FIRST_ROW",pStartRowIndex));


			pPagingQuery = oSqlBuilder.ToString();
			return oParamList.ToArray();
		}

		/// <summary>
		/// 画像SEQから画像ファイル名を生成する
		/// </summary>
		/// <param name="pPicSeq">画像SEQ</param>
		/// <returns>画像ファイル名</returns>
		public static string GeneratePicFileNm(decimal pPicSeq) {
			return ViCommConst.PIC_HEADER + iBridCommLib.iBridUtil.addZero(pPicSeq.ToString(),ViCommConst.OBJECT_NM_LENGTH);
		}

		/// <summary>
		/// ｷｬｽﾄの着ﾎﾞｲｽﾃﾞｨﾚｸﾄﾘを取得する


		/// </summary>
		/// <param name="sWebPhisicalDir">着ﾎﾞｲｽﾃﾞｨﾚｸﾄﾘのﾙｰﾄﾊﾟｽ</param>
		/// <param name="pSiteCd">ｻｲﾄｺｰﾄﾞ</param>
		/// <param name="pLoginId">ｷｬｽﾄのﾛｸﾞｲﾝID</param>
		/// <returns>ｷｬｽﾄの着ﾎﾞｲｽﾃﾞｨﾚｸﾄﾘ</returns>
		public static string GetCastVoiceDir(string pWebPhisicalDir,string pSiteCd,string pLoginId) {
			// assert
			if (string.IsNullOrEmpty(pWebPhisicalDir))
				throw new ArgumentException("pWebPhisicalDir");
			if (string.IsNullOrEmpty(pSiteCd))
				throw new ArgumentException("pSiteCd");
			if (string.IsNullOrEmpty(pLoginId))
				throw new ArgumentException("pLoginId");

			using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,string.Empty,ViCommConst.FILE_UPLOAD_PASSWORD)) {

				if (!Directory.Exists(pWebPhisicalDir))
					throw new InvalidOperationException(string.Format("Web物理ディレクトリが存在しません。({0})",pWebPhisicalDir));

				string sPath = string.Empty;

				sPath = Path.Combine(pWebPhisicalDir,ViCommConst.VOICE_DIRECTRY.Replace(@"\",string.Empty));
				CheckAndGenerateDir(sPath);

				sPath = Path.Combine(sPath,pSiteCd);
				CheckAndGenerateDir(sPath);

				sPath = Path.Combine(sPath,ViCommConst.OPERATOR_DIRECTORY.Replace(@"\",string.Empty));
				CheckAndGenerateDir(sPath);

				sPath = Path.Combine(sPath,pLoginId);
				CheckAndGenerateDir(sPath);

				return sPath;
			}
		}
		/// <summary>
		/// ｷｬｽﾄの動画ﾃﾞｨﾚｸﾄﾘを取得する



		/// </summary>
		/// <param name="sWebPhisicalDir">着ﾎﾞｲｽﾃﾞｨﾚｸﾄﾘのﾙｰﾄﾊﾟｽ</param>
		/// <param name="pSiteCd">ｻｲﾄｺｰﾄﾞ</param>
		/// <param name="pLoginId">ｷｬｽﾄのﾛｸﾞｲﾝID</param>
		/// <returns>ｷｬｽﾄの着ﾎﾞｲｽﾃﾞｨﾚｸﾄﾘ</returns>
		public static string GetCastMovieDir(string pWebPhisicalDir,string pSiteCd,string pLoginId) {
			// assert
			if (string.IsNullOrEmpty(pWebPhisicalDir))
				throw new ArgumentException("pWebPhisicalDir");
			if (string.IsNullOrEmpty(pSiteCd))
				throw new ArgumentException("pSiteCd");
			if (string.IsNullOrEmpty(pLoginId))
				throw new ArgumentException("pLoginId");

			using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,string.Empty,ViCommConst.FILE_UPLOAD_PASSWORD)) {

				if (!Directory.Exists(pWebPhisicalDir))
					throw new InvalidOperationException(string.Format("Web物理ディレクトリが存在しません。({0})",pWebPhisicalDir));

				string sPath = string.Empty;

				sPath = Path.Combine(pWebPhisicalDir,ViCommConst.MOVIE_DIRECTRY.Replace(@"\",string.Empty));
				CheckAndGenerateDir(sPath);

				sPath = Path.Combine(sPath,pSiteCd);
				CheckAndGenerateDir(sPath);

				sPath = Path.Combine(sPath,ViCommConst.OPERATOR_DIRECTORY.Replace(@"\",string.Empty));
				CheckAndGenerateDir(sPath);

				sPath = Path.Combine(sPath,pLoginId);
				CheckAndGenerateDir(sPath);

				return sPath;
			}
		}
		/// <summary>
		/// ｷｬｽﾄのPicﾃﾞｨﾚｸﾄﾘを取得する

		/// </summary>
		/// <param name="sWebPhisicalDir">Picﾃﾞｨﾚｸﾄﾘのﾙｰﾄﾊﾟｽ</param>
		/// <param name="pSiteCd">ｻｲﾄｺｰﾄﾞ</param>
		/// <param name="pLoginId">ｷｬｽﾄのﾛｸﾞｲﾝID</param>
		/// <returns>ｷｬｽﾄの着ﾎﾞｲｽﾃﾞｨﾚｸﾄﾘ</returns>
		public static string GetCastPicDir(string pWebPhisicalDir,string pSiteCd,string pLoginId) {
			// assert
			if (string.IsNullOrEmpty(pWebPhisicalDir))
				throw new ArgumentException("pWebPhisicalDir");
			if (string.IsNullOrEmpty(pSiteCd))
				throw new ArgumentException("pSiteCd");
			if (string.IsNullOrEmpty(pLoginId))
				throw new ArgumentException("pLoginId");

			using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,string.Empty,ViCommConst.FILE_UPLOAD_PASSWORD)) {

				if (!Directory.Exists(pWebPhisicalDir))
					throw new InvalidOperationException(string.Format("Web物理ディレクトリが存在しません。({0})",pWebPhisicalDir));

				string sPath = string.Empty;

				sPath = Path.Combine(pWebPhisicalDir,ViCommConst.PIC_DIRECTRY.Replace(@"\",string.Empty));
				CheckAndGenerateDir(sPath);

				sPath = Path.Combine(sPath,pSiteCd);
				CheckAndGenerateDir(sPath);

				sPath = Path.Combine(sPath,ViCommConst.OPERATOR_DIRECTORY.Replace(@"\",string.Empty));
				CheckAndGenerateDir(sPath);

				sPath = Path.Combine(sPath,pLoginId);
				CheckAndGenerateDir(sPath);

				return sPath;
			}
		}
		public static string GetProductPhysicalDir(string pWebPhisicalDir,string pSiteCd,string pProductId) {
			// assert
			if (string.IsNullOrEmpty(pWebPhisicalDir))
				throw new ArgumentException("pWebPhisicalDir");
			if (string.IsNullOrEmpty(pSiteCd))
				throw new ArgumentException("pSiteCd");
			if (string.IsNullOrEmpty(pProductId))
				throw new ArgumentException("pProductId");

			string sPath = string.Empty;

			using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,string.Empty,ViCommConst.FILE_UPLOAD_PASSWORD)) {

				if (!Directory.Exists(pWebPhisicalDir))
					throw new InvalidOperationException(string.Format("Web物理ディレクトリが存在しません。({0})",pWebPhisicalDir));

				sPath = Path.Combine(pWebPhisicalDir,ViCommConst.PRODUCT_DIR_NAME);
				CheckAndGenerateDir(sPath);

				sPath = Path.Combine(sPath,pSiteCd);
				CheckAndGenerateDir(sPath);

				sPath = Path.Combine(sPath,pProductId);
				CheckAndGenerateDir(sPath);
			}

			return sPath;
		}
		/// <summary>
		/// 美人時計用のPicﾃﾞｨﾚｸﾄﾘを取得する

		/// </summary>
		/// <param name="sWebPhisicalDir">Picﾃﾞｨﾚｸﾄﾘのﾙｰﾄﾊﾟｽ</param>
		/// <param name="pSiteCd">ｻｲﾄｺｰﾄﾞ</param>
		/// <param name="pAssignedTime">美人時計の時間</param>
		/// <returns>美人時計用のPicﾃﾞｨﾚｸﾄﾘ</returns>
		public static string GetBeautyClockPicDir(string pWebPhisicalDir,string pSiteCd,string pAssignedTime) {
			// assert
			if (string.IsNullOrEmpty(pWebPhisicalDir))
				throw new ArgumentException("pWebPhisicalDir");
			if (string.IsNullOrEmpty(pSiteCd))
				throw new ArgumentException("pSiteCd");
			if (string.IsNullOrEmpty(pAssignedTime))
				throw new ArgumentException("pAssignedTime");

			using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,string.Empty,ViCommConst.FILE_UPLOAD_PASSWORD)) {

				if (!Directory.Exists(pWebPhisicalDir))
					throw new InvalidOperationException(string.Format("Web物理ディレクトリが存在しません。({0})",pWebPhisicalDir));

				string sPath = string.Empty;

				sPath = Path.Combine(pWebPhisicalDir,ViCommConst.EXTENSION_DIRECTORY.Replace(@"\",string.Empty));
				CheckAndGenerateDir(sPath);

				sPath = Path.Combine(sPath,ViCommConst.BEAUTY_CLOCK_DIRECTORY.Replace(@"\",string.Empty));
				CheckAndGenerateDir(sPath);

				sPath = Path.Combine(sPath,pSiteCd);
				CheckAndGenerateDir(sPath);

				sPath = Path.Combine(sPath,pAssignedTime);
				CheckAndGenerateDir(sPath);

				return sPath;
			}
		}

		/// <summary>
		/// FlashGame用のﾃﾞｨﾚｸﾄﾘを取得する


		/// </summary>
		/// <param name="sWebPhisicalDir">Picﾃﾞｨﾚｸﾄﾘのﾙｰﾄﾊﾟｽ</param>
		/// <param name="pSiteCd">ｻｲﾄｺｰﾄﾞ</param>
		/// <param name="pSexCd">性別ﾞ</param>
		/// <returns>FlashGame用のﾃﾞｨﾚｸﾄﾘ</returns>
		public static string GetFlashGameDir(string pWebPhisicalDir,string pSiteCd,string pSexCd) {
			// assert
			if (string.IsNullOrEmpty(pWebPhisicalDir))
				throw new ArgumentException("pWebPhisicalDir");
			if (string.IsNullOrEmpty(pSiteCd))
				throw new ArgumentException("pSiteCd");

			using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,string.Empty,ViCommConst.FILE_UPLOAD_PASSWORD)) {

				if (!Directory.Exists(pWebPhisicalDir))
					throw new InvalidOperationException(string.Format("Web物理ディレクトリが存在しません。({0})",pWebPhisicalDir));

				string sPath = string.Empty;

				sPath = Path.Combine(pWebPhisicalDir,ViCommConst.EXTENSION_DIRECTORY.Replace(@"\",string.Empty));
				CheckAndGenerateDir(sPath);

				sPath = Path.Combine(sPath,ViCommConst.FLASH_GAME_DIRECTORY.Replace(@"\",string.Empty));
				CheckAndGenerateDir(sPath);

				sPath = Path.Combine(sPath,pSiteCd);
				CheckAndGenerateDir(sPath);

				if (pSexCd.Equals(ViCommConst.MAN)) {
					sPath = Path.Combine(sPath,ViCommConst.MAN_DIRECTORY.Replace(@"\",string.Empty));
					CheckAndGenerateDir(sPath);

				} else {
					sPath = Path.Combine(sPath,ViCommConst.OPERATOR_DIRECTORY.Replace(@"\",string.Empty));
					CheckAndGenerateDir(sPath);
				}

				return sPath;
			}
		}

		/// <summary>
		/// 指定されたディレクトリが存在するかどうかチェックし、存在しない場合は作成する。

		/// ※ただし、指定されたディレクトリのルートが存在しない場合は例外をスローする。

		/// </summary>
		/// <param name="pDirectoryPath">対象のディレクトリパス</param>
		/// <return>チェックまたは作成したディレクトリパス</return>
		public static string CheckAndGenerateDir(string pDirectoryPath) {
			if (string.IsNullOrEmpty(pDirectoryPath))
				throw new ArgumentException("pDirectoryPath");

			if (!Directory.Exists(pDirectoryPath)) {
				Directory.CreateDirectory(pDirectoryPath);
			}
			return pDirectoryPath;
		}

		public static void DeleteFiles(string pDirPath,string pSearchPattern) {
			if (string.IsNullOrEmpty(pDirPath))
				throw new ArgumentException("pDirPath");
			if (string.IsNullOrEmpty(pSearchPattern))
				throw new ArgumentException("pSearchPattern");
			using (Impersonator.Impersonate(ViCommConst.FILE_UPLOAD_USERNAME,ViCommConst.FILE_UPLOAD_PASSWORD)) {
				foreach (string sFilePath in Directory.GetFiles(pDirPath,pSearchPattern)) {
					if (File.Exists(sFilePath))
						File.Delete(sFilePath);
				}
			}
		}

		public static string GetCurrentAspx() {
			string sFilePath = HttpContext.Current.Server.MapPath(HttpContext.Current.Request.Url.LocalPath);
			return new System.IO.FileInfo(sFilePath).Name;
		}

		public static string GetHashMd5(string pTargetStr) {
			string ps = "";
			MD5 md5 = MD5.Create();

			byte[] s = md5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(pTargetStr));

			for (int i = 0;i < s.Length;i++) {
				ps += s[i].ToString("x2");
			}

			return ps;
		}


		private static readonly Regex _regexBrTag = new Regex("<br[\\s]*[/]{0,1}>",RegexOptions.IgnoreCase | RegexOptions.Compiled);
		public static bool IsBrTag(string pValue) {
			return _regexBrTag.IsMatch(pValue);
		}
		public static string ReplaceBr(string pValue,string pReplaceValue) {
			return _regexBrTag.Replace(pValue,pReplaceValue);
		}

		private static readonly Regex _regexHtmlTag = new Regex("(<(?:\"[^\"]*\"|'[^']*'|[^'\">])*(?:>))",RegexOptions.IgnoreCase | RegexOptions.Compiled);
		public static string RemoveHtml(string pValue,bool pIgnoreBrTag) {
			return _regexHtmlTag.Replace(pValue,delegate(Match pMatch) {
				if (pIgnoreBrTag && IsBrTag(pMatch.Value)) {
					return pMatch.Value;
				} else {
					return string.Empty;
				}
			});
		}


		public static string GetAssignedNowTime() {
			return string.Format("{0}",(DateTime.Now.Hour * 60) + DateTime.Now.Minute);
		}

		public static string ConvertAssignedToTime(string pAssignedTime) {
			return string.Format("{0}:{1:D2}",(int.Parse(pAssignedTime) / 60),(int.Parse(pAssignedTime) % 60));
		}

		public static string ConvertTimeToAssigned(string pTime) {
			string[] sTmpTimeAry = pTime.Split(new char[] { ':' },2);
			return string.Format("{0}",(int.Parse(sTmpTimeAry[0]) * 60) + int.Parse(sTmpTimeAry[1]));
		}


		public static string DefHandleName(object pHandleNm) {
			if (pHandleNm.ToString().Equals(string.Empty)) {
				if (iBridCommLib.iBridUtil.GetStringValue(ConfigurationManager.AppSettings["defHandleName"]).Equals(string.Empty)) {
					return "NO NAME";
				} else {
					return iBridCommLib.iBridUtil.GetStringValue(ConfigurationManager.AppSettings["defHandleName"]);
				}
			} else {
				return pHandleNm.ToString();
			}
		}
	}

}
