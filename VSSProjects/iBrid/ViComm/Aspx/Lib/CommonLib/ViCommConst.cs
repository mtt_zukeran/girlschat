﻿namespace ViComm {

	public partial class ViCommConst {
		public const string WITHOUT = "-1";
		public const int WAIT_AUTH_MOVIE_INQUIRY = 1;
		public const int CAST_MOVIE_INQUIRY = 2;
		public const long GET_MOVIE_ERROR_VALUE = -1;

		/*----------------------------------*/
		/*　AD_CD							*/
		/*----------------------------------*/
		public const string AD_CD_RAKUTEN = "RAKUTEN";

		/*----------------------------------*/
		/*　Button ｱｸｼｮﾝ	 				*/
		/*----------------------------------*/
		public const string BUTTON_SUBMIT = "cmdSubmit";
		public const string BUTTON_SEARCH = "cmdSearch";
		public const string BUTTON_FINDUSER = "cmdFindUser";
		public const string BUTTON_EXTSEARCH = "cmdExtSearch";
		public const string BUTTON_NEXT_LINK = "cmdNext";
		public const string BUTTON_GOTO_LINK = "cmdGoto";
		public const string BUTTON_DELETE = "cmdDelete";
		public const string BUTTON_DEFAULT = "cmdDefault";
		public const string BUTTON_REGIST_CAST = "cmdRegistCast";
		public const string BUTTON_RESET_MAIL_NG = "cmdResetMailNg";
		public const string BUTTON_EDIT = "cmdEdit";

		public const string BUTTON_GOTO_PIC = "cmdGotoPic";
		public const string BUTTON_GOTO_MOVIE = "cmdGotoMovie";

		public const string BUTTON_ACTION_PURHASE = "purchase";
		public const string BUTTON_ACTION_DOWNLOAD = "download";

		public const string BUTTON_RETURN = "cmdReturn";
		public const string BUTTON_TX = "cmdTx";

		/*----------------------------------*/
		/*　Get ｱｸｼｮﾝ	 					*/
		/*----------------------------------*/
		public const string ACTION = "action";
		public const string ACTION_DELETE = "del";
		public const string ACTION_REDIRECT = "redirect";

		/*----------------------------------*/
		/*　プログラムルート				*/
		/*----------------------------------*/
		public const string PGM_ROOT_MAN = "/ViComm/man";
		public const string PGM_ROOT_WOMAN = "/ViComm/woman";
		public const string CAST_SITE_CD = "Z001";

		/*----------------------------------*/
		/* オンラインステータス				*/
		/*----------------------------------*/
		public const int USER_OFFLINE = 0;		// オフライン 
		public const int USER_LOGINED = 1;		// ログイン済み 
		public const int USER_WAITING = 2;		// 会話待機中 
		public const int USER_TALKING = 3;		// 会話中 
		public const int USER_DUMMY_TALKING = 9;// ダミー会話中 

		/*----------------------------------*/
		/* 長さ								*/
		/*----------------------------------*/
		public const int OBJECT_NM_LENGTH = 15;

		/*----------------------------------*/
		/* デフォルト						*/
		/*----------------------------------*/
		public const string DEFUALT_PUB_DAY = "2000/01/01";
		public const string DEFAUL_AD_GROUP_CD = "******";		// 広告グループ 
		public const string DEFAUL_USER_RANK = "*";				// ユーザーランク 
		public const int DEFAULT_ACT_CATEGORY_SEQ = -1;			// 演出カテゴリー 
		public const int DEFAULT_CAST_PIC_ATTR_TYPE_SEQ = -1;	// キャスト写真属性種別 
		public const int DEFAULT_CAST_PIC_ATTR_SEQ = -1;		// キャスト写真属性種別値 
		public const int DEFAULT_CAST_MOVIE_ATTR_TYPE_SEQ = -1;	// キャスト動画属性種別 
		public const int DEFAULT_CAST_MOVIE_ATTR_SEQ = -1;		// キャスト動画属性種別値 
		public const int DEFAULT_CAST_DIARY_ATTR_SEQ = -1;		// キャスト日記属性種別値 
		public const double DEFAULT_COOKIE_TIMEOUT = 60;		// クッキーのタイムアウト 

		/*----------------------------------*/
		/* ディレクトリ・ウェブパス			*/
		/*----------------------------------*/
		public const string PIC_DIRECTRY = "\\data";
		public const string MOVIE_DIRECTRY = "\\movie";
		public const string IMAGE_DIRECTRY = "\\image";
		public const string VOICE_DIRECTRY = "\\voice";
		public const string OPERATOR_DIRECTORY = "\\Operator";
		public const string MAN_DIRECTORY = "\\man";
		public const string EXTENSION_DIRECTORY = "\\Extension";
		public const string BEAUTY_CLOCK_DIRECTORY = "\\BeautyClock";
		public const string FLASH_GAME_DIRECTORY = "\\FlashGame";
		public const string SOCIAL_GAME_DIRECTORY = "\\SocialGame";
		public const string BOSS_IMAGE_DIRECTORY = "\\BossImage";
		public const string ITEM_IMAGE_DIRECTORY = "\\ItemImage";
		public const string IMAGE_WEB_PATH = "/image";
		public const string WEB_PIC_PATH = "/data";
		public const string WEB_MOVIE_PATH = "/movie";
		public const string WEB_VOICE_PATH = "/voice";
		public const string PRODUCT_DIR_NAME = "product";


		/*----------------------------------*/
		/* 写真・動画ファイル名関連			*/
		/*----------------------------------*/
		public const string PIC_HEADER = "";
		public const string PIC_FOODER = ".jpg";
		public const string PIC_FOODER_SMALL = "_s.jpg";
		public const string PIC_FOODER_ORIGINAL = "_o.jpg";
		public const string MOVIE_HEADER = "";
		public const string MOVIE_FOODER = ".3gp";
		public const string MOVIE_FOODER2 = ".3g2";
		public const string MOVIE_FOODER3 = ".mp4";

		/*----------------------------------*/
		/* 固定画像ファイル名　				*/
		/*----------------------------------*/
		public const string IMG_FILE_NM_CAST_NOPIC = "nopic.gif";
		public const string IMG_FILE_NM_CAST_NOPICSMALL = "nopic_s.gif";
		public const string IMG_FILE_NM_MAN_NOPIC = "photo_men1.gif";

		/*----------------------------------*/
		/* メール・電話宛先　　				*/
		/*----------------------------------*/
		public const string MOVIE_URL_HEADER = "mailto:m";
		public const string MOVIE_URL_HEADER2 = "m";
		public const string PHOTO_URL_HEADER = "mailto:p";
		public const string PHOTO_URL_HEADER2 = "p";
		public const string DIARY_URL_HEADER = "mailto:d";
		public const string DIARY_URL_HEADER2 = "d";
		public const string TWEET_URL_HEADER = "mailto:t";
		public const string TWEET_URL_HEADER2 = "t";
		public const string YAKYUKEN_URL_HEADER = "mailto:y";
		public const string YAKYUKEN_URL_HEADER2 = "y";
		public const string JYANKEN_URL_HEADER = "mailto:j";
		public const string JYANKEN_URL_HEADER2 = "j";
		public const string TEL_URL_HEADER = "tel:";

		/*----------------------------------*/
		/* 最大件数							*/
		/*----------------------------------*/
		/*
		public const int MAX_DETAIL_LIST_COUNT = 5;					// 明細表示件数
		public const int MAX_LIST_COUNT = 15;						// 一覧表示件数
		public const int MAX_CAST_DETAIL_LIST_COUNT = 5;			// 出演者・明細表示件数
		public const int MAX_CAST_LIST_COUNT = 15;					// 出演者・一覧表示件数
		*/
		public const int MAX_STAFF_INFO_LIST_COUNT = 2;				// スタッフ情報画面に最大表示件数 
		public const int MAX_PIC_MAINTE_LIST_COUNT = 2;				// オペレータ写真メンテ画面に最大表示件数 
		public const int MAX_PIC_DETAIL_COUNT = 1;					// ユーザ写真詳細画面に最大表示件数 
		public const int MAX_LIVE_CAMERA = 5;						// ライブ配信カメラ最大数 
		public const int MAX_ORG_MAIL_BLOCKS = 5;					// オリジナルメール最大ブロック数	 
		public const int MAX_MAN_INQUIRY_ITEMS = 4;					// 男性会員表示項目数(キャストPC) 
		public const int MAX_ADMIN_MAN_INQUIRY_ITEMS = 4;			// 男性会員表示項目数(管理) 
		public const int MAX_ADMIN_CAST_INQUIRY_ITEMS = 4;			// キャスト表示項目数(管理) 
		public const int MAX_AF_SETTLE_COMPANY = 20;				// アフリエート決済先

		/*----------------------------------*/
		/* コールステータス　　				*/
		/*----------------------------------*/
		public const string CALL_STATUS_OK = "00";
		public const string CALL_STATUS_BUSY = "01";
		public const string CALL_STATUS_NO_ANSWER = "02";
		public const string CALL_STATUS_NO_ASSING_NO = "03";
		public const string CALL_STATUS_NA_OUTGOING = "97";
		public const string CALL_STATUS_ERROR = "98";
		public const string CALL_STATUS_UNPROCESS = "99";

		/*----------------------------------*/
		/* 動画認証状況　　　　				*/
		/*----------------------------------*/
		public const string MOVIE_APPLY_WAIT = "0";		// 認証待ち 
		public const string MOVIE_APPLY_OK = "1";		// 公開 
		public const string MOVIE_APPLY_REMOVE = "2";	// 削除 
		public const string MOVIE_APPLY_NG = "3";		// 非公開 

		/*----------------------------------*/
		/* 待機種別　　　　　　				*/
		/*----------------------------------*/
		public const string WAIT_TYPE_VIDEO = "1";				// VIDEO
		public const string WAIT_TYPE_VOICE = "2";				// VOICE 
		public const string WAIT_TYPE_BOTH = "3";				// BOTH 

		/*----------------------------------*/
		/* 会話種別　　　　　　				*/
		/*----------------------------------*/
		public const string TALK_TYPE_WSHOT = "1";				// PRIVATE WSHOT
		public const string TALK_TYPE_PUBLIC = "2";				// PUBLIC WSHOT 

		/*----------------------------------*/
		/* 会話種別補足　　　　				*/
		/*----------------------------------*/
		public const int TALK_SUB_TYPE_ALL = 0;					// 全て 
		public const int TALK_SUB_TYPE_NORMAL = 1;				// 一般 
		public const int TALK_SUB_TYPE_INVITE = 2;				// 招待 
		public const int TALK_SUB_TYPE_MEETING = 3;				// 待ち合せ 

		/*----------------------------------*/
		/* メール受信区分					*/
		/*----------------------------------*/
		public const string MAIL_RX_ONLY_BOX = "1";				// メールBOX受信 
		public const string MAIL_RX_BOTH = "2";					// メールBOX受信＋携帯メール 
		public const string MAIL_RX_ONLY_FAVORIT = "3";			// メールBOX受信(全て)＋携帯メール(お気に入りのみ) 

		/*----------------------------------*/
		/* 登録状態							*/
		/*----------------------------------*/
		public const string REGIST_RX_EMAIL = "1";				// 仮登録ﾒｰﾙ受信
		public const string REGIST_COMPLITE = "2";				// 登録完了 
		public const string REGIST_COMPLITE_NO_REPORT = "3";	// 登録完了(成果未報告) 

		/*----------------------------------*/
		/* 検索条件　　	　　　　			*/
		/*----------------------------------*/
		public const uint INQUIRY_NONE_DISP_PROFILE = 0;						// NONE ONLINEと等値は障害のProfileの表示に障害が発生した場合、Online一覧を表示させるため　
		public const uint INQUIRY_ONLINE = 0;									// ONLINE
		public const uint INQUIRY_CAST_MOVIE = 1;								// Movieあり出演者 
		public const uint INQUIRY_CAST_MONITOR = 2;								// 会話ﾓﾆﾀｰ対象
		public const uint INQUIRY_FAVORIT = 3;									// お気に入り 
		public const uint INQUIRY_CAST_TALK_RANKING_WEEKLY = 4;					// ランキング・会話
		public const uint INQUIRY_CAST_TALK_RANKING_MONTHLY = 5;				// ランキング・会話
		public const uint INQUIRY_CAST_PICKUP = 6;								// PICKUP
		public const uint INQUIRY_TALK_MOVIE = 7;								// ダイジェスト 
		public const uint INQUIRY_TALK_MOVIE_PICKUP = 8;						// ダイジェスト(お勧め)
		public const uint INQUIRY_CONDITION = 9;								// 条件検索
		public const uint INQUIRY_CAST_WAITING = 10;							// 待機中
		public const uint INQUIRY_ROOM_MONITOR = 11;							// 部屋モニター
		public const uint INQUIRY_CAST_NEW_FACE = 12;							// 新人（全て） 
		public const uint INQUIRY_TALK_MOVIE_PART = 13;							// ダイジェスト(パート選択)
		public const uint INQUIRY_CAST_DIARY = 14;								// 日記 
		public const uint INQUIRY_TALK_HISTORY = 15;							// 会話履歴
		public const uint INQUIRY_MARKING = 16;									// 足跡
		public const uint INQUIRY_CAST_RECOMMEND = 17;							// ONLINE + OFFLINE DISPLAY対象者 
		public const uint INQUIRY_DIRECT = 18;									// ダイレクト 
		public const uint INQUIRY_RX_USER_MAIL_BOX = 19;						// メールBOX(ユーザーメール受信)
		public const uint INQUIRY_RX_INFO_MAIL_BOX = 20;						// メールBOX(お知らせメール受信)
		public const uint INQUIRY_TX_USER_MAIL_BOX = 21;						// メールBOX(ユーザーメール送信)
		public const uint INQUIRY_CAST_MAIL_RANKING_WEEKLY = 22;				// ランキング・メール
		public const uint INQUIRY_CAST_MAIL_RANKING_MONTHLY = 23;				// ランキング・メール
		public const uint INQUIRY_ADMIN_REPORT = 24;							// 管理者連絡
		public const uint INQUIRY_MAN_NEW = 25;									// 新規会員
		public const uint INQUIRY_REFUSE = 26;									// 拒否リスト 
		public const uint INQUIRY_CAST_MOVIE_ACCESS_RANKING_WEEKLY = 27;		// 動画プロフ被閲覧週間ランキング
		public const uint INQUIRY_CAST_MOVIE_ACCESS_RANKING_MONTHLY = 28;		// 動画プロフ被閲覧月間ランキング
		public const uint INQUIRY_CAST_DIARY_ACCESS_RANKING_WEEKLY = 29;		// 日記被閲覧週間ランキング
		public const uint INQUIRY_CAST_DIARY_ACCESS_RANKING_MONTHLY = 30;		// 日記被閲覧月間ランキング
		public const uint INQUIRY_CAST_FAVORIT_RANKING = 31;					// お気に入られ数ランキング
		public const uint INQUIRY_ONLINE_CAST_NEW_FACE = 32;					// 新人（オンラインのみ） 
		public const uint INQUIRY_CAST_TV_PHONE = 33;							// ＴＶ電話対象者 
		public const uint INQUIRY_LIKE_ME = 34;									// お気に入られ
		public const uint INQUIRY_CAST_LOGINED = 35;							// キャストログイン済み
		public const uint INQUIRY_ALL_TALK_HISTORY = 36;						// 全会話履歴
		public const uint INQUIRY_FORWARD = 37;									// 転送一覧
		public const uint INQUIRY_CAST_ONLINE_TV_PHONE = 38;					// ＴＶ電話対象者（オンラインのみ） 
		public const uint INQUIRY_TALK_REQUEST_HISTORY_ALL = 39;				// 会話リクエスト履歴一覧（全て） 
		public const uint INQUIRY_TALK_REQUEST_HISTORY_GROUP_MAN = 40;			// 会話リクエスト履歴一覧（男性グループ） 
		public const uint INQUIRY_TALK_REQUEST_HISTORY_SUCCESS = 41;			// 会話リクエスト履歴一覧（成功のみ） 
		public const uint INQUIRY_WAITING_MAN = 42;								// 待機男性一覧 
		public const uint INQUIRY_PERSONAL_SALE_MOVIE = 43;						// 個人毎販売動画一覧
		public const uint INQUIRY_SALE_MOVIE = 44;								// 販売動画一覧
		public const uint INQUIRY_PERSONAL_SALE_VOICE = 45;						// 個人毎販売着ﾎﾞｲｽ一覧
		public const uint INQUIRY_SALE_VOICE = 46;								// 販売着ﾎﾞｲｽ一覧
		public const uint INQUIRY_BBS_PIC = 47;									// 掲示板写真
		public const uint INQUIRY_BBS_MOVIE = 48;								// 掲示板動画
		public const uint INQUIRY_BBS_DOC = 49;									// 掲示板文章
		public const uint INQUIRY_GALLERY = 50;									// プロフィール写真一覧
		public const uint INQUIRY_MAIL_HISTORY = 51;							// メール履歴
		public const uint INQUIRY_BBS_OBJ = 52;									// 掲示板写真と動画
		public const uint INQUIRY_MAN_LOGINED_NEW = 53;							// ログイン中新規会員
		public const uint INQUIRY_RECOMMEND = 54;								// RECOMMEND
		public const uint INQUIRY_RANDOM_RECOMMEND = 55;						// ﾗﾝﾀﾞﾑRECOMMEND

		public const uint INQUIRY_ASP_AD = 57;									// ASP広告 
		public const uint INQUIRY_MAN_LOGINED = 58;								// LOGIN中(男性) 
		public const uint INQUIRY_GET_ASP_AD = 59;								// ASP広告取得 
		public const uint INQUIRY_CAST_TALK_POINT_RANKING = 60;					// 会話ポイントランキング
		public const uint INQUIRY_PICKUP_PIC = 61;								// ピックアップ(写真)
		public const uint INQUIRY_PICKUP_MOVIE = 62;							// ピックアップ(動画)
		public const uint INQUIRY_PICKUP_CHARACTER = 63;						// ピックアップ(ｷｬﾗｸﾀｰ)
		public const uint INQUIRY_MEMO = 64;									// メモ 
		public const uint INQUIRY_TX_BATCH_MAIL = 65;							// 一括未送信メール管理 
		public const uint INQUIRY_BBS_VIEW_HISTORY = 66;						// 掲示板写真・動画閲覧履歴 
 
		public const uint INQUIRY_MAN_ADD_POINT = 68;							// ポイント購入男性一覧  
		public const uint INQUIRY_REFUSE_ME = 74;								// 拒否られている 
		public const uint INQUIRY_CAST_TALK_POINT_RANKING_WEEKLY = 75;			// 会話ポイントランキング週間 
		public const uint INQUIRY_CAST_TALK_POINT_RANKING_MONTHLY = 76;			// 会話ポイントランキング月間 
		public const uint INQUIRY_CAST_DIARY_ACCESS_RANKING_WEEKLY2 = 77;		// 日記被閲覧週間ランキング(月～日) 
		public const uint INQUIRY_CAST_DIARY_ACCESS_RANKING_MONTHLY2 = 78;		// 日記被閲覧月間ランキング(先月) 
		public const uint INQUIRY_CAST_MARKING_RANKING_MONTHLY = 79;			// 足あと月間ランキング(女性) 
		public const uint INQUIRY_MAN_MARKING_RANKING_MONTHLY = 80;				// 足あと月間ランキング(男性) 
		public const uint INQUIRY_DIARY_LIKE = 81;								// 日記いいね男性一覧 

		// ここから商品関連の検索条件
		public const uint INQUIRY_PRODUCT = 1000;							// 商品関連の検索条件を表現する境界値 
		public const uint INQUIRY_PRODUCT_MOVIE = 1001;						// 商品動画 
		public const uint INQUIRY_PRODUCT_MOVIE_BUYING_HIST = 1002;			// 商品動画(購入履歴) 
		public const uint INQUIRY_PRODUCT_MOVIE_FAVORITE = 1003;			// 商品動画(お気に入り) 
		public const uint INQUIRY_PRODUCT_MOVIE_WEELKY_RANKING = 1004;		// 商品動画(週間ランキング) 
		public const uint INQUIRY_PRODUCT_MOVIE_MONTHLY_RANKING = 1005;		// 商品動画(月間ランキング) 
		public const uint INQUIRY_PRODUCT_PIC = 1101;						// 商品写真 
		public const uint INQUIRY_PRODUCT_PIC_BUYING_HIST = 1102;			// 商品写真(購入履歴) 
		public const uint INQUIRY_PRODUCT_PIC_FAVORITE = 1103;				// 商品写真(お気に入り) 
		public const uint INQUIRY_PRODUCT_PIC_WEELKY_RANKING = 1104;		// 商品写真(週間ランキング) 
		public const uint INQUIRY_PRODUCT_PIC_MONTHLY_RANKING = 1105;		// 商品写真(月間ランキング) 
		public const uint INQUIRY_PRODUCT_AUCTION = 1301;					// 商品ｵｰｸｼｮﾝ 
		public const uint INQUIRY_PRODUCT_AUCTION_BUYING_HIST = 1302;		// 商品ｵｰｸｼｮﾝ(購入履歴) 
		public const uint INQUIRY_PRODUCT_AUCTION_FAVORITE = 1303;			// 商品ｵｰｸｼｮﾝ(お気に入り) 
		public const uint INQUIRY_PRODUCT_AUCTION_ENTRY = 1311;				// 商品ｵｰｸｼｮﾝ(入札中) 
		public const uint INQUIRY_PRODUCT_AUCTION_WIN = 1312;				// 商品ｵｰｸｼｮﾝ(落札) 
		public const uint INQUIRY_PRODUCT_AUCTION_BID_LOG = 1320;			// 商品ｵｰｸｼｮﾝ(入札履歴) 

		public const uint INQUIRY_PRODUCT_THEME = 1401;						// 商品着せ替えﾂｰﾙ 
		public const uint INQUIRY_PRODUCT_THEME_BUYING_HIST = 1402;			// 商品着せ替えﾂｰﾙ(購入履歴) 
		public const uint INQUIRY_PRODUCT_THEME_FAVORITE = 1403;			// 商品着せ替えﾂｰﾙ(お気に入り) 

		public const uint INQUIRY_PRODUCT_END = 1999;						// 商品関連の検索条件を表現する境界値終端 

		// ここから拡張機能関連の検索条件
		public const uint INQUIRY_EXTENSION = 2000;							// 拡張機能関連の検索条件を表現する境界値始端 
		public const uint INQUIRY_EXTENSION_GAME = 2001;					// ｶﾞﾙﾁｬｹﾞｰﾑ 
		public const uint INQUIRY_EXTENSION_GAME_SCORE = 2002;				// ｶﾞﾙﾁｬｹﾞｰﾑ ｽｺｱ 
		public const uint INQUIRY_EXTENSION_BEAUTY_CLOCK = 2003;			// 美人時計 


		public const uint INQUIRY_EXTENSION_BLOG = 2010;					// ブログ 
		public const uint INQUIRY_EXTENSION_BLOG_OBJ = 2013;				// ブログ動画/写真 
		public const uint INQUIRY_EXTENSION_BLOG_ARTICLE = 2014;			// ブログ投稿 
		public const uint INQUIRY_EXTENSION_BLOG_ARTICLE_PICKUP = 2015;		// ブログ投稿ピックアップ 
		public const uint INQUIRY_RX_BLOG_MAIL_BOX = 2016;					// メールBOX(ブログメール受信) 
		public const uint INQUIRY_EXTENSION_CAST_MARKING_RANKING = 2017;	// 足あとランキング 
		public const uint INQUIRY_EXTENSION_LOVE_LIST = 2018;				// ラブリスト一覧 
		public const uint INQUIRY_EXTENSION_PETIT_LOVE_LIST = 2019;			// プチラブリスト一覧 
		public const uint INQUIRY_EXTENSION_MOTE_CON = 2020;				// モテコン一覧
		public const uint INQUIRY_EXTENSION_MOTE_KING = 2021;				// モテキング一覧
		public const uint INQUIRY_EXTENSION_BBS_THREAD = 2022;				// 掲示板一覧（スレッド形式） 
		public const uint INQUIRY_EXTENSION_RICHINO_USER = 2023;			// リッチーノ会員一覧
		public const uint INQUIRY_FAVORIT_AND_LIKE_ME_LIST = 2024;			// 好き好き同士 
		public const uint INQUIRY_EXTENSION_CAST_QUICK_RESPONSE = 2025;		// 10分以内に返事できる女の子 
		public const uint INQUIRY_EXTENSION_BINGO_ENTRY_WINNER = 2026;		// ビンゴ WINNER一覧 
		public const uint INQUIRY_EXTENSION_INTRODUCER_FRIEND = 2027;		// 友達紹介一覧
		public const uint INQUIRY_EXTENSION_END = 2499;						// 拡張機能関連の検索条件を表現する境界値終端 
		public const uint INQUIRY_EXTENSION_PWILD = 2500;					// 拡張機能関連(PWILD拡張)境界値始端 
		// ・予約範囲(2500～2999)
		public const uint INQUIRY_EXTENSION_PWILD_END = 2999;				// 拡張機能関連(PWILD拡張)境界値終端 


		/*----------------------------------*/
		/* キャストPC検索条件　　　			*/
		/*----------------------------------*/
		public class SearchManStatusOption {
			public const int SEL_MAN_LOGINED = 0x00001;				// LOGIN中
			public const int SEL_MAN_CONDITION = 0x00002;			// 条件検索
			public const int SEL_MAN_ALL = 0x00004;					// 全件
		}

		/*----------------------------------*/
		/* web要求							*/
		/*----------------------------------*/
		public const string REQUEST_TALK = "00";				// 会話要求(暫定) 
		public const string REQUEST_TALK_REGULAR = "01";		// 会話要求(Regular) 
		public const string REQUEST_LIVE_MONITOR = "02";		// ライブモニタ要求 
		public const string REQUEST_VIEW_TALK = "03";			// 会話モニタ要求 
		public const string REQUEST_DIALIN_RECORDING = "04";	// ダイアルイン録画 
		public const string REQUEST_DIALIN_PLAY = "05";			// ダイアルイン再生 
		public const string REQUEST_TALK_LIGHT = "06";			// 会話要求(Light) 
		public const string REQUEST_BROADCASTING = "07";		// 放送要求 
		public const string REQUEST_VIEW_BROADCASTING = "08";	// 放送ビュー要求 
		public const string REQUEST_DIALOUT_RECORDING = "09";	// ダイアルアウト録画 
		public const string REQUEST_DIALOUT_PLAY = "10";		// ダイアルアウト再生 
		public const string REQUEST_TALK_PREMIUM = "11";		// 会話要求(Premium) 
		public const string REQUEST_VIEW_SHARE_LIVE_TALK = "12";// 共有ライブトークモニター 
		public const string REQUEST_MONITOR_VOICE = "13";		// 音声会話モニタ 
		public const string REQUEST_RESERVED01 = "14";			// 音声会話モニタ(Premium and Reguler)・IVP整合性のため 
		public const string REQUEST_TALK_SMART_IVP = "17";			// クロスマイルアウト(RAD経由)
		public const string REQUEST_TALK_SMART_DIRECT = "18";		// クロスマイルアウト(DIRECT)
		public const string REQUEST_AD_MOTION_PLAY = "51";		// AD-MOTION再生サービス要求 
		public const string REQUEST_AD_MOTION_REC = "52";		// AD-MOTION録画サービス要求 
		public const string REQUEST_DIALIN_CONFERENCE = "54";	// ダイアルイン会議 
		public const string REQUEST_CONTACT_CALL_TYPE01 = "61";	// コンタクトセンターサービスタイプ01 
		public const string REQUEST_TEL_AUTH01 = "71";			// 電話番号認証 
		public const string REQUEST_TEL_AUTH_LIGHT = "73";		// 電話番号認証(LIGHT回線利用) 
		public const string REQUEST_MODERATOR = "99";			// 議長サービス要求 
		public const string REQUEST_VCS_MENU = "**";			// VCSサービス要求 

		/*----------------------------------*/
		/* 会話メニュー						*/
		/*----------------------------------*/
		public const string MENU_VIDEO_WTALK = "01";			// 3G Video(WShot)
		public const string MENU_VIDEO_PTALK = "02";			// 3G Video(Public)
		public const string MENU_VOICE_WTALK = "03";			// Voice(WShot)
		public const string MENU_VOICE_PTALK = "04";			// Voice(Public)

		/*----------------------------------*/
		/* VIEWメニュー						*/
		/*----------------------------------*/
		public const string MENU_VIEW_TALK = "01";				// View Talk
		public const string MENU_VIEW_ROOM = "02";				// View Room

		/*----------------------------------*/
		/* 性別								*/
		/*----------------------------------*/
		public const string MAN = "1";							// 男性 
		public const string WOMAN = "2";						// 女性 
		public const string OPERATOR = "3";						// オペ 

		/*----------------------------------*/
		/* 発着信区分						*/
		/*----------------------------------*/
		public const string INCOMMING_CALL = "1";				// 着信 
		public const string OUTGOING_CALL = "2";				// 発信 

		/*----------------------------------*/
		/* SP Result						*/
		/*----------------------------------*/
		public const string SP_RESULT_OK = "A";
		public const string SP_RESULT_NO_TEL = "B";
		public const string SP_RESULT_NO_WAITING = "C";
		public const string SP_RESULT_NG = "X";

		/*----------------------------------*/
		/* IVP RESULT						*/
		/*----------------------------------*/
		public const string IVP_RESULT_OK = "0";
		public const string IVP_RESULT_BUSY = "1";
		public const string IVP_RESULT_NO_LIVE = "2";
		public const string IVP_RESULT_NO_CONF = "3";
		public const string IVP_RESULT_OVER_SESSION = "4";
		public const string IVP_RESULT_NG_PARAM_LIGHT = "6";
		public const string IVP_RESULT_NO_ADMIN = "7";
		public const string IVP_RESULT_NG_PARAM = "8";
		public const string IVP_RESULT_NG_SITE = "9";

		/*----------------------------------*/
		/* ユーザー認証結果					*/
		/*----------------------------------*/
		public const int USER_INFO_OK = 0;
		public const int USER_INFO_UNAUTH_TEL = 1;
		public const int USER_INFO_NO_BAL = 2;
		public const int USER_INFO_NG = 3;
		public const int USER_INFO_ILLIGAL_ACCESS = 4;
		public const int USER_INFO_TIMEOUT = 9;
		public const int USER_INFO_INTERNAL_ERROR = -1;

		/*----------------------------------*/
		/* Carrier Type						*/
		/*----------------------------------*/
		public const string DOCOMO = "01";
		public const string SOFTBANK = "02";
		public const string KDDI = "03";
		public const string ACCELMAIL = "09";
		public const string ANDROID = "21";
		public const string IPHONE = "22";
		public const string CARRIER_OTHERS = "11";

		/*----------------------------------*/
		/* CSVダウンロードファイル名		*/
		/*----------------------------------*/
		public const string CSV_FILE_NM_DAILY_PERFORMANCE_LOG = "search_call_log.csv";
		public const string CSV_FILE_NM_CAST_TALK_LOG = "operatorSearchCallLog.csv";
		public const string CSV_FILE_NM_MANAGER_PERFORMANCE_LOG = "managerPerformanceLog.csv";
		public const string CSV_FILE_NM_AD_GROUP_DETAIL_PERFORMANCE_LOG = "adGroupDetailPerformance.csv";
		public const string CSV_FILE_NM_DOWNLOAD_MOVIE = "download_movie.csv";
		public const string CSV_FILE_NM_ACCESS_MOVIE = "access_movie.csv";
		public const string CSV_FILE_NM_CAST_SETTLEMENT = "cast_settlement.csv";
		public const string CSV_FILE_NM_SHARE_LIVE_SALES = "share_live_sales.csv";
		public const string CSV_FILE_NM_MAN_USED_POINT = "ManUsedPoint.csv";
		public const string CSV_FILE_NM_NOT_SETTLE_PERFORMANCE = "CastNotSettlePerformance.csv";
		public const string CSV_FILE_NM_PRODUCT_BUY_SUMMARY = "ProductBuySummary.csv";
		public const string CSV_FILE_NM_PAYMENT_HISTORY_SUMMARY = "PaymentHistorySummary.csv";
		public const string CSV_FILE_NM_MAN_TWEET = "ManTweet.csv";
		public const string CSV_FILE_NM_MAN_TWEET_COMMENT = "ManTweetComment.csv";
		public const string CSV_FILE_NM_PROFILE_PIC = "ProfilePic.csv";

		/*----------------------------------*/
		/* CALL_FROM						*/
		/*----------------------------------*/
		public const string CALL_FROM_MOVIE_INQUIRY = "1";
		public const string CALL_FROM_CAST_INQUIRY = "2";
		public const string CALL_FROM_CAST_MOVIE_INQUIRY = "3";
		public const string CALL_FROM_CAST_MOVIE_MAINTE = "4";
		public const string CALL_FROM_CAST_PRODUCTION_INQUIRY = "5";
		public const string CALL_FROM_SAMEPHONE_INQUIRY = "6";
		public const string CALL_FROM_UPLOAD_MOVIE = "7";
		public const string CALL_FROM_UPLOAD_PIC = "8";
		public const string CALL_FROM_WAIT_AUTH_MOVIE_INQUIRY = "9";
		public const string CALL_FROM_PROFILE_MENU = "9";
		public const string CALL_FROM_LIVE_MENU = "10";
		public const string CALL_FROM_SUB_CAST_INQUIRY = "11";
		public const string CALL_FROM_MOVIE_PART_LIST_NOPICKUP = "12";
		public const string CALL_FROM_MOVIE_PART_LIST_PICKUP = "13";

		/*----------------------------------*/
		/* 端末種別							*/
		/*----------------------------------*/
		public const string TERM_MOBILE = "1";			// 携帯電話
		public const string TERM_SIP = "2";				// SIP
		public const string TERM_H323 = "3";			// H.323
		public const string TERM_SKYPE = "4";			// SKYPE

		/*----------------------------------*/
		/* PREFIX							*/
		/*----------------------------------*/
		public const string PREFIX_VIDEO = "79";		// 3G Video
		public const string PREFIX_VOICE = "70";		// Voice Service

		/*----------------------------------*/
		/* DTMF動作							*/
		/*----------------------------------*/
		public const string DTMF_VIDEO_WTALK = "1";					// DTMF Action Video WShot Talk
		public const string DTMF_VIDEO_PTALK = "2";					// DTMF Action Video Public Talk
		public const string DTMF_VOICE_WTALK = "3";					// DTMF Action Voice WShot Talk
		public const string DTMF_VOICE_PTALK = "4";					// DTMF Action Voice Public Talk

		/*----------------------------------*/
		/* 文章グループ						*/
		/*----------------------------------*/
		public const string DOC_GRP_PREV_VER = "00";				// 通常文章(旧VER互換用) 
		public const string DOC_GRP_NORMAL = "11";					// 通常文章 
		public const string DOC_GRP_USER_VIEW = "12";				// ユーザーVIEW 
		public const string DOC_GRP_ERROR_MSG = "13";				// エラーメッセージ 
		public const string DOC_GRP_MAIL_TEMPLATE = "14";			// メールテンプレート 

		/*----------------------------------*/
		/* サイト補足						*/
		/*----------------------------------*/
		public const string SITE_DEFAULT_MENU = "M0";				// デフォルトメニューURL
		public const string SITE_MAN_TOP_PICKUP_ID = "10";			// (男性)NonUserTopに表示するﾋﾟｯｸｱｯﾌﾟ(Recommend)のIDのコード値
		public const string SITE_MAN_TOP_PICKUP_DROWS = "11";		// (男性)NonUserTopに表示するﾋﾟｯｸｱｯﾌﾟ(Recommend)の件数のコード値
		public const string SITE_WOMAN_TOP_MANNEW_DROWS = "12";		// (女性)UserTopに表示する新人男性一覧の件数のコード値

		/*----------------------------------*/
		/* 課金種別							*/
		/*----------------------------------*/
		public const string CHARGE_TALK_PUBLIC = "01";				// 会話・ビデオ(Public)
		public const string CHARGE_TALK_WSHOT = "02";				// 会話・ビデオ(WShot)
		public const string CHARGE_TALK_VOICE_WSHOT = "03";			// 会話・音声(WShot)
		public const string CHARGE_VIEW_LIVE = "04";				// ライブ視聴
		public const string CHARGE_VIEW_TALK = "05";				// 会話モニタ
		public const string CHARGE_VIEW_ONLINE = "06";				// 部屋モニタ
		public const string CHARGE_PLAY_MOVIE = "07";				// ロングムービー
		public const string CHARGE_RX_MAIL = "08";					// メール受信
		public const string CHARGE_MAIL_WITH_PIC = "09";			// メール添付画像閲覧
		public const string CHARGE_MAIL_WITH_MOVIE = "10";			// メール添付ムービー閲覧
		public const string CHARGE_TX_MAIL = "11";					// メール送信
		public const string CHARGE_TX_MAIL_WITH_PIC = "12";			// メール添付画像送信
		public const string CHARGE_TX_MAIL_WITH_MOVIE = "13";		// メール添付ムービー送信
		public const string CHARGE_BBS_MOVIE = "14";				// 掲示板ﾑｰﾋﾞｰ閲覧
		public const string CHARGE_BBS_PIC = "15";					// 掲示板画像閲覧
		public const string CHARGE_DOWNLOAD_PF_MOVIE = "16";		// プロフィールムービーダウンロード 
		public const string CHARGE_RX_MAIL_WITH_PIC = "17";			// メール添付画像受信
		public const string CHARGE_RX_MAIL_WITH_MOVIE = "18";		// メール添付ムービー受信
		public const string CHARGE_FRIEND_INTRO = "19";				// お友達紹介報酬
		public const string CHARGE_PLAY_ROOM_SHOW = "21";			// 部屋SHOW出演(キャスト支払用)
		public const string CHARGE_REC_PF_MOVIE = "22";				// プロフィールムービー録画(キャスト支払用)
		public const string CHARGE_GPF_TALK_VOICE = "23";			// 会話・音声(男性へ転送)
		public const string CHARGE_PLAY_PF_VOICE = "24";			// プロフィール再生(男性支払い用)
		public const string CHARGE_REC_PF_VOICE = "25";				// プロフィール録音(男性支払い用)
		public const string CHARGE_PRV_MESSAGE_RX = "26";			// プライベート伝言受信(キャスト支払い用)
		public const string CHARGE_PRV_PROFILE_RX = "27";			// プライベートプロフィール受信(キャスト)
		public const string CHARGE_WIRETAPPING = "28";				// 盗聴
		public const string CHARGE_PLAY_PV_MSG = "29";				// プライベート伝言再生
		public const string CHARGE_WRITE_BBS = "30";				// 掲示板書込
		public const string CHARGE_TALK_VOICE_PUBLIC = "31";		// 会話・音声(PUBLIC)
		public const string CHARGE_DOWNLOAD_MOVIE = "32";			// ダウンロードムービー
		public const string CHARGE_TIME_CALL = "33";				// 定時コール
		public const string CHARGE_DOWNLOAD_VOICE = "34";			// ダウンロード着ﾎﾞｲｽ  
		public const string CHARGE_SOCIAL_GAME = "35";				// ｿｰｼｬﾙｹﾞｰﾑ  
		public const string CHARGE_YAKYUKEN = "36";					// 野球拳 or FC会費
		public const string CHARGE_PRESENT_MAIL = "37";				// プレゼントメール

		// 商品関連
		public const string CHARGE_PROD_MOV = "61";					// 商品動画 
		public const string CHARGE_PROD_ADULT_MOV = "62";			// 商品動画(ｱﾀﾞﾙﾄ) 
		public const string CHARGE_PROD_PIC = "63";					// 商品写真 
		public const string CHARGE_PROD_ADULT_PIC = "64";			// 商品写真(ｱﾀﾞﾙﾄ) 
		public const string CHARGE_PROD_AUCTION = "65";				// 商品ｵｰｸｼｮﾝ 
		public const string CHARGE_PROD_ADULT_AUCTION = "66";		// 商品ｵｰｸｼｮﾝ(ｱﾀﾞﾙﾄ) 
		public const string CHARGE_PROD_THEME = "66";				// 商品着せ替えﾂｰﾙ 

		// CAST ACTION
		public const string CHARGE_CAST_ACTION = "50";				// キャストアクションによる課金の開始 
		public const string CHARGE_CAST_TALK_PUBLIC = "51";			// 男性待機・会話・ビデオ(Public)
		public const string CHARGE_CAST_TALK_WSHOT = "52";			// 男性待機・会話・ビデオ(WShot)
		public const string CHARGE_CAST_TALK_VOICE_WSHOT = "53";	// 男性待機・会話・音声(WShot)
		public const string CHARGE_CAST_TALK_VOICE_PUBLIC = "54";	// 男性待機・会話・音声(PUBLIC)

		/*----------------------------------*/
		/* ユーザーランクマスク				*/
		/*----------------------------------*/
		public const int MASK_RANK_A = 0x001;						// ユーザーランクA
		public const int MASK_RANK_B = 0x002;						// ユーザーランクB
		public const int MASK_RANK_C = 0x004;						// ユーザーランクC
		public const int MASK_RANK_D = 0x008;						// ユーザーランクD
		public const int MASK_RANK_E = 0x010;						// ユーザーランクE
		public const int MASK_RANK_F = 0x020;						// ユーザーランクF
		public const int MASK_RANK_G = 0x040;						// ユーザーランクG
		public const int MASK_RANK_H = 0x080;						// ユーザーランクH

		/*----------------------------------*/
		/* ユーザー状態マスク				*/
		/*----------------------------------*/
		public const int MASK_MAN_NORMAL = 0x001;					// 通常
		public const int MASK_MAN_TEL_NO_AUTH = 0x002;				// 電話番号未認証
		public const int MASK_MAN_NEW = 0x004;						// 新規 
		public const int MASK_MAN_NO_RECEIPT = 0x008;				// 未入金 
		public const int MASK_MAN_NO_USED = 0x010;					// 未利用
		public const int MASK_MAN_DELAY = 0x020;					// 遅延
		public const int MASK_MAN_RESIGNED = 0x040;					// 退会 
		public const int MASK_MAN_STOP = 0x080;						// 利用禁止
		public const int MASK_MAN_BLACK = 0x100;					// ブラック
		public const int MASK_MAN_HOLD = 0x200;						// 登録保留

		/*----------------------------------*/
		/* 女性ユーザー状態マスク			*/
		/*----------------------------------*/
		public const int MASK_WOMAN_AUTH_WAIT = 0x001;				// 認証待ち 
		public const int MASK_WOMAN_NORMAL = 0x002;					// 利用可能 
		public const int MASK_WOMAN_STOP = 0x004;					// 利用禁止 
		public const int MASK_WOMAN_RESIGNED = 0x008;				// 退会 
		public const int MASK_WOMAN_BAN = 0x010;					// 利用停止 
		public const int MASK_WOMAN_HOLD = 0x020;					// 保留 

		/*----------------------------------*/
		/* オンライン状態マスク				*/
		/*----------------------------------*/
		public const int MASK_OFFLINE = 0x01;						// オフライン
		public const int MASK_LOGINED = 0x02;						// ログイン済み中
		public const int MASK_WAITING = 0x04;						// 会話待機中
		public const int MASK_TALKING = 0x08;						// 会話中

		/*----------------------------------*/
		/* キャリア種別マスク				*/
		/*----------------------------------*/
		public const int MASK_DOCOMO = 0x01;						// ドコモ
		public const int MASK_SOFTBANK = 0x02;						// ソフトバンク
		public const int MASK_KDDI = 0x04;							// AU
		public const int MASK_ANDROID = 0x08;						// Android
		public const int MASK_IPHONE = 0x10;						// iPhone
		public const int MASK_OTHER = 0x20;							// その他

		/*----------------------------------*/
		/* ユーザー状態						*/
		/*----------------------------------*/
		public const string USER_MAN_NORMAL = "0";					// 通常
		public const string USER_MAN_TEL_NO_AUTH = "1";				// 電話番号未認証
		public const string USER_MAN_NEW = "2";						// 新規 
		public const string USER_MAN_NO_RECEIPT = "3";				// 未入金 
		public const string USER_MAN_NO_USED = "4";					// 未利用
		public const string USER_MAN_DELAY = "5";					// 遅延
		public const string USER_MAN_HOLD = "6";					// 登録保留
		public const string USER_MAN_RESIGNED = "7";				// 退会 
		public const string USER_MAN_STOP = "8";					// 利用禁止
		public const string USER_MAN_BLACK = "9";					// ブラック

		/*----------------------------------*/
		/* 女性会員ユーザ状態				*/
		/*----------------------------------*/
		public const string USER_WOMAN_AUTH_WAIT = "0";				// 認証待ち
		public const string USER_WOMAN_NORMAL = "1";				// 利用可能
		public const string USER_WOMAN_STOP = "2";					// 利用禁止
		public const string USER_WOMAN_HOLD = "6";					// 保留 
		public const string USER_WOMAN_RESIGNED = "7";				// 退会 
		public const string USER_WOMAN_BAN = "9";					// 利用停止

		/*----------------------------------*/
		/* 決済状況表示MASK 				*/
		/*----------------------------------*/
		public const int MSK_SETTLE_CASH = 0x0000001;				// 現金 
		public const int MSK_SETTLE_PREPAID = 0x0000002;			// プリペード 
		public const int MSK_SETTLE_CREDIT_PACK = 0x0000004;		// クレジットパック    
		public const int MSK_SETTLE_C_CHECK = 0x0000008;			// Ｃ－ＣＨＥＣＫ
		public const int MSK_SETTLE_BITCASH = 0x0000010;			// BITCASH
		public const int MSK_SETTLE_S_MONEY = 0x0000020;			// Ｓ－ＭＯＮＥＹ
		public const int MSK_SETTLE_CVSDL = 0x0000040;				// コンビニダウンロード 
		public const int MSK_SETTLE_POINT_AFFILIATE = 0x0000080;	// ポイントアフリエート 
		public const int MSK_SETTLE_ADULTID = 0x0000100;			// 大人ＩＤ
		public const int MSK_SETTLE_CVS_DIRECT = 0x0000200;			// コンビニダイレクト 
		public const int MSK_SETTLE_EDY = 0x0000400;				// EDY
		public const int MSK_SETTLE_AUTO_RECEIPT = 0x0000800;		// 自動入金






		public const int MSK_SETTLE_G_MONEY = 0x0001000;			// Ｇ－ＭＯＮＥＹ 
		public const int MSK_SETTLE_PAY_AFLTER_PACK = 0x0002000;	// 後払パック
		public const int MSK_SETTLE_RESERVE02 = 0x0004000;			// RESERVE02
		public const int MSK_SETTLE_RESERVE03 = 0x0008000;			// RESERVE03
		public const int MSK_SETTLE_CREDIT = 0x0010000;				// クレジット従量精算 
		public const int MSK_SETTLE_RESERVE04 = 0x0020000;			// RESERVE04
		public const int MSK_SETTLE_MOBILE_CASH = 0x0040000;		// 携帯送金 
		public const int MSK_SETTLE_CREDIT_AUTH = 0x0080000;		// クレジット０円決済 
		public const int MSK_SETTLE_RESERVE05 = 0x0100000;			// RESERVE05 
		public const int MSK_SETTLE_GIGA_POINT = 0x0200000;			// GIGA POINT 
		public const int MSK_SETTLE_RAKUTEN = 0x0400000;			// 楽天決済




		/*----------------------------------*/
		/* 決済状態							*/
		/*----------------------------------*/
		public const string SETTLE_STAT_OK = "0";					// 決済ＯＫ
		public const string SETTLE_STAT_SETTLE_NOW = "1";			// 決済処理中
		public const string SETTLE_STAT_COMM_ERROR = "2";			// 通信失敗 
		public const string SETTLE_STAT_CHARGE = "3";				// 従量制申請 
		public const string SETTLE_STAT_ID_INVALID = "9";			// 番号不正

		/*----------------------------------*/
		/* 会員登録種別						*/
		/*----------------------------------*/
		public const string REGIST_BEFORE = "01";
		public const string REGIST_AFTER = "02";
		public const string REGIST_CREDIT = "03";
		public const string REGIST_PREPAID = "11";
		public const string REGIST_TRIAL_PREPAID = "12";
		public const string REGIST_ONETIME_PREPAID = "13";
		public const string REGIST_COMM_CARD = "14";
		public const string REGIST_BITCASH = "15";
		public const string REGIST_WEB_USER = "90";
		public const string REGIST_WOMAN = "91";
		public const string REGIST_OPERATOR = "92";

		/*----------------------------------*/
		/* 登録元識別						*/
		/*----------------------------------*/
		public const string REGIST_FROM_AFFILIATER = "01";
		public const string REGIST_FROM_AD = "99";
		public const string REGIST_FROM_NONE = "NN";

		/*----------------------------------*/
		/* コード定義						*/
		/*----------------------------------*/
		public const string CODE_TYPE_MODIFY_PROFILE_PIC_TYPE = "29";// 無料画像変更種別 
		public const string CODE_TYPE_MAN_WAIT_TIME = "44";			// 待機時間(男性) 
		public const string CODE_TYPE_FIND_CALL_REQUEST = "45";		// 通話要求検索区分 
		public const string CODE_TYPE_VIOLATION_TYPE = "56";		// 違反報告種別 
		public const string CODE_TYPE_VIOLATION_TYPE_WOMAN = "38";	// 違反報告種別(女性が男性を違反報告する)
		public const string CODE_TYPE_WAIT_TYPE = "61";				// 待機区分 
		public const string CODE_TYPE_MAIL_RX_TYPE = "66";			// メール受信区分 
		public const string CODE_TYPE_AREA = "67";					// 都道府県 
		public const string CODE_TIME_PART = "68";					// 時間帯 
		public const string CODE_INVITE_TALK_SERVICE_POINT = "79";	// 会話招待サービスポイント 
		public const string CODE_TYPE_CVSDL = "85";					// コンビニDL 
		public const string CODE_TYPE_WAIT_TIME = "86";				// 待機時間(女性) 
		public const string CODE_TYPE_PRODUCT_PIC_TYPE = "21";		// 商品写真種別
		public const string CODE_TYPE_PRODUCT_MOVIE_TYPE = "22";	// 商品動画種別
		public const string CODE_TYPE_PRODUCT_TYPE = "25";			// 商品種別
		public const string CODE_TYPE_TALK_END_MAIL_RX_TYPE = "19";	// 通話切断理由メール受信区分 
		public const string CODE_TYPE_INFO_MAIL_RX_TYPE = "15";		// お知らせメール受信区分 
		public const string CODE_TYPE_BUY_POINT_MAIL_RX_TYPE = "23";// キャスト向け男性会員ポイント購入メール受信区分 
		public const string CODE_TYPE_BLOG_MAIL_RX_TYPE = "11";		// ブログ更新メール受信区分 
		public const string CODE_TYPE_DOC_NM = "56";				// HTML文書タイプ名称
		public const string CODE_TYPE_AUCTION_STATUS = "02";		// オークション状態 

		public const string CODE_TYPE_USER_DEFINE_FLAG_MAN = "A0";	// ﾕｰｻﾞｰ定義ﾌﾗｸﾞ(男性)
		public const string CODE_TYPE_USER_DEFINE_FLAG_WOMAN = "A1";// ﾕｰｻﾞｰ定義ﾌﾗｸﾞ(女性) 

		public const string CODE_TYPE_AD_MENU = "A7";				// 広告掲載メニュー

		/*----------------------------------*/
		/* ユーザー登録結果					*/
		/*----------------------------------*/
		public const int REG_USER_RST_NOT_FOUND_INFO = 1;			// 仮登録情報なし 
		public const int REG_USER_RST_UNT_EXIST = 2;				// 固体識別既登録
		public const int REG_USER_RST_TEL_EXIST = 3;				// 認証済み電話番号既登録
		public const int REG_USER_RST_HANDLE_NM_EXIST = 4;			// ﾊﾝﾄﾞﾙ名既登録
		public const int REG_USER_RST_SUB_SCR_NO_IS_NULL = 5;		// iMode-ID NULL


		/*----------------------------------*/
		/* パートナーサイト登録結果			*/
		/*----------------------------------*/
		public const int RESULT_USER_REGIST = 0;					// 登録ＯＫ
		public const int RESULT_USER_EXIST = 1;						// TEL番号/固体識別番号重複 
		public const int RESULT_USER_REGISTING = 8;					// 登録中 
		public const int RESULT_ADCD_ERROR = 9;						// 登録エラー

		/*----------------------------------*/
		/* ログイン方法						*/
		/*----------------------------------*/
		public const string LOGIN_BY_UTN = "1";						// 固体識別
		public const string LOGIN_BY_TEL = "2";						// 電話番号
		public const string LOGIN_BY_LOGINID = "3";					// ログインID
		public const string LOGIN_BY_IMODE_ID = "4";				// i-mode ID
		public const string LOGIN_BY_TWITTER_ID = "5";				// Twitter ID
		public const string LOGIN_BY_SNS_ACCOUNT_ID = "6";			// SNS ID

		/*----------------------------------*/
		/* 退会復帰方法						*/
		/*----------------------------------*/
		public const string RESIGNED_RETURN_BY_TEL = "1";			// 電話番号
		public const string RESIGNED_RETURN_BY_LOGINID = "2";		// ログインID

		/*----------------------------------*/
		/* VIEW用マスク						*/
		/*----------------------------------*/
		public const int VIEW_MASK_HTML_DOC_COLOR = 64;
		public const int VIEW_MASK_ACT_CATEGORY = 4;
		public const int VIEW_MASK_USER_RANK = 2;
		public const int VIEW_MASK_AD_GROUP = 1;

		/*----------------------------------*/
		/* テーブルINDEX					*/
		/*----------------------------------*/
#if (PREVIOUS)
#endif
		public const int DATASET_CAST = 1;							// キャスト検索結果 
		public const int DATASET_MAN = 2;							// 男性会員検索結果 
		public const int DATASET_ADMIN_REPORT = 3;					// 管理者連絡検索結果 
		public const int DATASET_MAIL = 4;							// メール検索結果 
		public const int DATASET_MAIL_RESERVED = 5;					// メール検索結果(本来は送信・受信を分けるべき) 
		public const int DATASET_TX_BATCH_MAIL = 6;					// 一括送信メール検索結果 
		public const int DATASET_PARTNER_MOVIE = 7;					// 相手が録画した動画検索結果 
		public const int DATASET_PARTNER_PIC = 8;					// 相手が撮影した写真検索結果 
		public const int DATASET_PARTNER_DIARY = 9;					// 相手が書いた日記検索結果 
		public const int DATASET_PARTNER_BBS_DOC = 10;				// 相手が書いた掲示板文章検索結果 
		public const int DATASET_PARTNER_VOICE = 11;				// 相手が録音したキャスト着ボイス 
		public const int DATASET_SELF_MOVIE = 12;					// 自分が録画した動画検索結果 
		public const int DATASET_SELF_PIC = 13;						// 自分が撮影した写真検索結果 
		public const int DATASET_SELF_DIARY = 14;					// 自分が書いた日記検索結果 
		public const int DATASET_SELF_BBS_DOC = 15;					// 自分が書いた掲示板文章検索結果 
		public const int DATASET_RANKING = 16;						// ランキング検索結果 
		public const int DATASET_TALK_HISTORY = 17;					// 会話履歴検索結果 
		public const int DATASET_CAST_BONUS = 18;					// 女性会員ボーナス検索結果 
		public const int DATASET_CAST_PAY = 19;						// 女性会員報酬検索結果 
		public const int DATASET_CAST_REGISTED_SITE_LIST = 20;		// 女性会員登録サイト一覧検索結果 
		public const int DATASET_REPUTATION = 21;					// 評判メッセージ検索結果 
		public const int DATASET_REPUTATION_RESERVED = 22;			// 評判メッセージ(本来は送信・受信を分けるべき) 
		public const int DATASET_ATTR_TYPE = 23;					// 属性値検索結果 
		public const int DATASET_TALK_MOVIE = 24;					// 会話動画検索結果 
		public const int DATASET_PARTNER_BBS_OBJ = 25;				// 相手が投稿した掲示板全ての検索結果
		public const int DATASET_SELF_BBS_OBJ = 26;					// 自分が投稿した掲示板全ての検索結果
		public const int DATASET_ASP_AD = 27;						// ASP広告検索結果 
		public const int DATASET_GET_ASP_AD = 28;					// ASP広告自動作成結果 
		public const int DATASET_PRODUCT_MOVIE = 29;				// 商品動画検索結果 
		public const int DATASET_PRODUCT_PIC = 30;					// 商品写真検索結果 
		public const int DATASET_BBS_VIEW_HISTORY = 31;				// 掲示板画像・動画閲覧履歴検索結果 
		public const int DATASET_PRODUCT_AUCTION = 32;				// 商品ｵｰｸｼｮﾝ検索結果 
		public const int DATASET_PRODUCT_AUCTION_BID_LOG = 33;		// 商品ｵｰｸｼｮﾝ入札履歴検索結果
		public const int DATASET_PRODUCT_THEME = 34;				// 商品着せ替えﾂｰﾙ検索結果

		public const int DATASET_GIFT_CODE = 38;					// ギフト券検索結果		
		public const int DATASET_CAST_LIKE_NOTICE = 39;				// 出演者新着いいね通知

		// 拡張機能 
		public const int DATASET_EX_GAME = 71;						// ｶﾞﾙﾁｬｹﾞｰﾑ 
		public const int DATASET_EX_GAME_SCORE = 72;				// ｶﾞﾙﾁｬｹﾞｰﾑ ｽｺｱ	
		public const int DATASET_EX_BEAUTY_CLOCK = 73;				// 美人時計検索結果 
		public const int DATASET_EX_PARTNER_BLOG_OBJ = 74;			// 相手が投稿したﾌﾞﾛｸﾞ添付ﾌｧｲﾙ 検索結果 
		public const int DATASET_EX_BLOG = 75;						// ﾌﾞﾛｸﾞ 検索結果
		public const int DATASET_EX_BLOG_OBJ = 76;					// 自分が投稿したﾌﾞﾛｸﾞ添付ﾌｧｲﾙ 検索結果 
		public const int DATASET_EX_BLOG_ARTICLE = 77;				// 自分が投稿したﾌﾞﾛｸﾞ投稿 検索結果 
		public const int DATASET_EX_BBS_THREAD = 78;				// スレッド形式BBS 検索結果 
		public const int DATASET_EX_WANTED_TARGET = 79;				// この娘を探せ･ﾀｰｹﾞｯﾄ 検索結果
		public const int DATASET_EX_BBS_RES = 80;					// スレッド形式BBS レス検索結果 
		public const int DATASET_EX_WANTED_SCHEDULE = 81;			// この娘を探せ･ｽｹｼﾞｭｰﾙ 検索結果
		public const int DATASET_EX_BINGO_ENTRY = 82;				// ﾋﾞﾝｺﾞｴﾝﾄﾘー 検索結果
		public const int DATASET_EX_INTRODUCER_FRIEND = 83;			// 友達紹介 検索結果
		// 90～99 PWILD拡張予約 

		/*----------------------------------*/
		/* メール種別						*/
		/*----------------------------------*/
		public const string MAIL_TP_APPROACH_TO_CAST = "01";		// アプローチメール(会員->キャスト)
		public const string MAIL_TP_APPROACH_TO_USER = "02";		// アプローチメール(キャスト->会員)
		public const string MAIL_TP_RETURN_TO_USER = "03";			// 返信メール(キャスト->会員)
		public const string MAIL_TP_RETURN_TO_CAST = "04";			// 返信メール(会員->キャスト)
		public const string MAIL_TP_INFO = "05";					// お知らせメール
		public const string MAIL_TP_REGIST = "06";					// 登録用URL付きメール(キャスト指定なし)
		public const string MAIL_TP_REGIST_WITH_CAST = "07";		// 登録用URL付きメール(キャスト指定あり)
		public const string MAIL_TP_NOTIFY_LOGIN = "08";			// ログイン通知
		public const string MAIL_TP_INVITE_TALK = "09";				// 会話招待メール
		public const string MAIL_TP_DM = "10";						// 一括送信メール
		public const string MAIL_TP_REGIST_COMPLITED = "11";		// 登録完了メール
		public const string MAIL_TP_EXIST_USER = "12";				// 既登録メール
		public const string MAIL_TP_MODIFY_COMPLITED = "13";		// メールアドレス変更完了メール
		public const string MAIL_TP_NOTIFY_ID_PW = "14";			// ＩＤ・ＰＷ通知メール
		public const string MAIL_TP_REG_COMP_WITH_CAST = "15";		// 登録完了メール(キャスト指定あり)
		public const string MAIL_TP_NOTIFY_LOGOUT = "16";			// ログアウト通知(Thank youメール)
		public const string MAIL_TP_CAST_MODIFY_COMPLITED = "17";	// キャストメールアドレス変更完了 
		public const string MAIL_TP_PAYMENT_GUIDE = "18";			// 入金案内
		public const string MAIL_TP_CAST_MAIL_MAGAZINE = "19";		// 管理者->キャスト向けメルマガ
		public const string MAIL_TP_APPROACH_ADMIN_TO_USER = "20";	// アプローチメール(キャストダミー(管理者)->会員)
		public const string MAIL_TP_APPLY_WOMAN_REGIST = "21";		// キャスト登録申請受付メール
		public const string MAIL_TP_WOMAN_REGIST_COMPITEd = "22";	// キャスト登録完了メール
		public const string MAIL_TP_URGE = "23";					// 督促メール
		public const string MAIL_TP_CAST_PAYOUT = "24";				// キャスト精算完了メール
		public const string MAIL_TP_REGISTED_CHECK = "25";			// 登録確認メール
		public const string MAIL_TP_UNAUTH_CAST_INVITE = "27";		// 未認証キャスト督促メール
		public const string MAIL_TP_MODIFY_BANK_ACCOUNT = "28";		// 口座変更通知メール
		public const string MAIL_TP_INVALID_ATTACHED_TYPE = "29";	// 添付種別相違メール
		public const string MAIL_TP_EIXST_ATTACHED = "30";			// 添付メール既受信
		public const string MAIL_TP_COMPLITE_ATTACHED = "31";		// 添付メール受信完了 
		public const string MAIL_TP_MEETING_TV = "32";				// 待ち合せメールTV
		public const string MAIL_TP_MEETING_VOICE = "33";			// 待ち合せメール音声 
		public const string MAIL_TP_BUY_POINT_NOFITY = "37";		// キャスト向け男性会員ポイント購入メール
		public const string MAIL_TP_BLOG_POST_NOFITY = "38";		// ﾌﾞﾛｸﾞ更新通知ﾒｰﾙ 
		public const string MAIL_TP_APPROACH_ADMIN_TO_CAST = "39";	// アプローチメール(会員ダミー(管理者)->キャスト)
		public const string MAIL_TP_TALK_END_REASON = "40";			// 会話終了理由ﾒｰﾙ




		public const string MAIL_TP_AGENT_REGIST = "44";			// エージェントID申請

		public const string MAIL_TP_ADMIN_TO_RESIGNED = "45";		// 退会者向けメール

		public const string MAIL_TP_REGIST_AUTO = "52";				// 登録用URL付きメール(動的アドレス)
		public const string MAIL_TP_WANTED_BOUNTY_ACQUIRED = "55";	// この娘を探せ 月間報酬付与メール
		public const string MAIL_TP_NOTIFY_TWEET_COMMENT = "56";	// つぶやきコメント通知メール
		
		public const string MAIL_TP_RELEASE_MAIL_ADDR = "62";		// メールアドレスNG解除メール
		public const string MAIL_TP_RELEASE_ADDR_CAST = "63";		// 出演者メールアドレスNG解除メール

		public const string MAIL_TP_CAST_PROF_PIC_NP = "68";		// 出演者プロフ画像非公開通知メール
		public const string MAIL_TP_CAST_PROF_MOVIE_NP = "69";		// 出演者無料動画非公開通知メール
		public const string MAIL_TP_CAST_BBS_PIC_NP = "70";			// 出演者掲示板画像非公開通知メール
		public const string MAIL_TP_CAST_BBS_MOVIE_NP = "71";		// 出演者掲示板動画非公開通知メール
		public const string MAIL_TP_CAST_STOCK_PIC_NP = "72";		// 出演者ストック画像非公開通知メール
		public const string MAIL_TP_CAST_STOCK_MOVIE_NP = "73";		// 出演者ストック動画非公開通知メール
		public const string MAIL_TP_CAST_GAME_PIC_NP = "74";		// 出演者ゲームお宝画像削除通知メール
		public const string MAIL_TP_CAST_GAME_MOVIE_NP = "75";		// 出演者ゲーム限定動画非公開通知メール
		public const string MAIL_TP_ACCEPT_WITHDRAWAL = "77";		// 退会申請者向けメール
		
		public const string MAIL_TP_CLEANING_ADDR = "99";			// アドレスクリーニングメール 

		/*----------------------------------*/
		/* メールBOX種別					*/
		/*----------------------------------*/
		public const string MAIL_BOX_USER = "1";					// ユーザーメール
		public const string MAIL_BOX_INFO = "2";					// お知らせメール
		public const string MAIL_BOX_BLOG = "3";					// ブログメール

		/*----------------------------------*/
		/* POST動作							*/
		/*----------------------------------*/
		public const int POST_ACT_UTN = 1;							// UTN設定 
		public const int POST_ACT_MAIL = 2;							// MAIL TO
		public const int POST_ACT_GUID = 3;							// i-Mode ID
		public const int POST_ACT_BOTH_ID = 4;						// UTN設定 & i-Mode ID

		/*----------------------------------*/
		/* 録画コーダー　	　				*/
		/*----------------------------------*/
		public const string REC_H263 = "0";
		public const string REC_H264 = "1";

		/*----------------------------------*/
		/* 送受信区分		　				*/
		/*----------------------------------*/
		public const string TX = "1";								// 送信
		public const string RX = "2";								// 受信
		public const string TXRX = "3";								// 送受信

		/*----------------------------------*/
		/* 関係				　				*/
		/*----------------------------------*/
		public const string REL_TALK = "3";							// 会話履歴あり
		public const string REL_FAVOTIR = "2";						// お気に入りに追加されている
		public const string REL_MARKING = "1";						// 足あとをつけられた

		/*----------------------------------*/
		/* 決済会社			　				*/
		/*----------------------------------*/
		public const string SETTLE_CORP_BIGSUN = "01";						// 株式会社BIGSUN
		public const string SETTLE_CORP_GIS = "02";							// グレートインフォメーション株式会社
		public const string SETTLE_CORP_DIGITAL_CHECK = "03";				// 株式会社デジタルチェック
		public const string SETTLE_CORP_BITCASH = "04";						// ビットキャッシュ株式会社L
		public const string SETTLE_CORP_SEASCAPE = "05";					// シースケイプ有限会社
		public const string SETTLE_CORP_SITE01 = "06";						// 株式会社サイト運営会社1
		public const string SETTLE_CORP_TELECOM = "07";						// テレコムクレジット株式会社
		public const string SETTLE_CORP_OFFICIAL_C = "08";					// オフィシャルチェック事務局
		public const string SETTLE_CORP_MEGA_DEALER = "09";					// 株式会社メガディーラー
		public const string SETTLE_CORP_ZERO = "10";						// 株式会社ゼロ
		public const string SETTLE_CORP_DIGICA = "11";						// 株式会社デジカジャパン
		public const string SETTLE_CORP_GACHIRI_SEL_ONE = "12";				// 株式会社ガッチリ(単一広告指定)
		public const string SETTLE_CORP_PAYPAL = "13";						// PayPal
		public const string SETTLE_CORP_GIGAFLOPS = "14";					// ギガフロップス株式会社
		public const string SETTLE_CORP_RAIO = "15";						// 株式会社ライオ
		public const string SETTLE_CORP_RAKUTEN = "16";						// 楽天
		public const string SETTLE_CORP_GACHIRI_SEL_LIST = "17";			// 株式会社ガッチリ(複数広告選択)
		public const string SETTLE_CORP_HARPYAN = "18";						// 株式会社ハーピアン
		public const string SETTLE_CORP_FUTURE_AFFILIATE = "19";			// 株式会社フューチャーアフリエイト 
		public const string SETTLE_CORP_INSIGHT = "20";						// 株式会社インサイト 
		public const string SETTLE_CORP_AKINAJISTA = "21";					// アキナジスタ株式会社 
		public const string SETTLE_CORP_SOPHIA = "26";						// ｿﾌｨｱ総合研究所株式会社 
		public const string SETTLE_CORP_EWALLET = "28";						// 株式会社イーウォレット 
		public const string SETTLE_CORP_IPS = "38";							// 株式会社インターネットペイメントサービス 
		public const string SETTLE_CORP_SYMPHONY = "41";					// 株式会社シンフォニー	
		public const string SETTLE_CORP_SITE02 = "44";						// 株式会社サイト運営会社2
		public const string SETTLE_CORP_ACT_SYSTEM = "47";					// 有限会社アクトシステム	
		public const string SETTLE_CORP_SOFTBANK_PAYMENT_SERVICE = "53";	// ソフトバンク・ペイメント・サービス株式会社
		

		/*----------------------------------*/
		/* 決済方法 						*/
		/*----------------------------------*/
		public const string SETTLE_CASH = "1";						// 現金 
		public const string SETTLE_PREPAID = "2";					// プリペード 
		public const string SETTLE_CREDIT_PACK = "3";				// クレジットパック
		public const string SETTLE_C_CHECK = "4";					// Ｃ－ＣＨＥＣＫ
		public const string SETTLE_BITCASH = "5";					// BITCASH
		public const string SETTLE_S_MONEY = "6";					// S-MONEY
		public const string SETTLE_CVSDL = "7";						// コンビニダウンロード 
		public const string SETTLE_CREDIT = "9";					// クレジット従量精算 
		public const string SETTLE_POINT_AFFILIATE = "A";			// ポイントアフリ
		public const string SETTLE_CVS_DIRECT = "B";				// コンビニダイレクト 
		public const string SETTLE_ADULTID = "C";					// 大人ＩＤ
		public const string SETTLE_EDY = "D";						// ＥＤＹ
		public const string SETTLE_ECO_CHIP = "E";					// エコチップ(S-Money)
		public const string SETTLE_G_MONEY = "G";					// G-MONEY
		public const string SETTLE_PAYPAL = "K";					// PayPal
		public const string SETTLE_MOBILE_CASH = "L";				// 携帯送金
		public const string SETTLE_GIGA_POINT = "M";				// GIGA POINT
		public const string SETTLE_CREDIT_AUTH = "N";				// クレジット0円決済 
		public const string SETTLE_AUTO_RECEIPT = "O";				// 自動入金
		public const string SETTLE_RAKUTEN = "P";					// 楽天あんしん支払い
		public const string SETTLE_SPS = "S";						// SoftBank Payment Service		
		public const string SETTLE_PAYMENT_AFTER_PACK = "Z";		// 後払PACK 

		/*----------------------------------*/
		/* 管理権限			　				*/
		/*----------------------------------*/
		public const string RIGHT_SYS_OWNER = "9";					// システム責任者 
		public const string RIGHT_SITE_OWNER = "8";					// サイト責任者 
		public const string RIGHT_SITE_MANAGER = "7";				// サイト管理者  
		public const string RIGHT_STAFF = "6";						// 本社スタッフ 
		public const string RIGHT_LOCAL_STAFF = "3";				// 地方スタッフ 
		public const string RIGHT_PRODUCTION = "2";					// プロダクション 
		public const string RIGHT_AD_MANAGE = "1";					// 広告管理 

		/*----------------------------------*/
		/* ｽﾏｰﾄﾌｫﾝｱﾌﾘｴｲﾄ認証送信方式		*/
		/*----------------------------------*/
		public const int AUTH_AFFILIATE_TRASN_NG = -1;				// 該当なし 
		public const int AUTH_AFFILIATE_TRASN_HTTP = 0;				// HTTPリクエスト 
		public const int AUTH_AFFILIATE_TRASN_IMAGE_TAG = 1;		// IMAGEタグ 
		public const int AUTH_AFFILIATE_TRASN_JS_TAG = 2;			// JavaScriptタグ
		public const int AUTH_AFFILIATE_TRASN_NONE = 9;				// ｽﾏｰﾄﾌｫﾝは認証しない 

		/*----------------------------------*/
		/* モニターフラグ					*/
		/*----------------------------------*/
		public const string MONITOR_TALK = "1";						// 会話モニター
		public const string MONITOR_ROOM = "2";						// 部屋モニター
		public const string MONITOR_VOICE = "3";					// 音声モニター

		/*----------------------------------*/
		/* キャスト属性数					*/
		/*----------------------------------*/
		public const int MAX_ATTR_COUNT = 20;
		public const int MAX_PLAY_COUNT = 40;

		/*----------------------------------*/
		/* キャストキャラクター数			*/
		/*----------------------------------*/
		public const int MAX_CHARACTER_COUNT = 20;

		/*----------------------------------*/
		/* キャストランク					*/
		/*----------------------------------*/
		public const string CAST_RANK_A = "A";							// キャストランクA
		public const string CAST_RANK_B = "B";							// キャストランクB
		public const string CAST_RANK_C = "C";							// キャストランクC
		public const string CAST_RANK_D = "D";							// キャストランクD
		public const string CAST_RANK_E = "E";							// キャストランクE

		/*----------------------------------*/
		/* ランキング種別					*/
		/*----------------------------------*/
		public const string RANKING_TALK_WEEKLY = "1";				// 会話ランキング週間 
		public const string RANKING_TALK_MONTHLY = "2";				// 会話ランキング月間
		public const string RANKING_MAIL_WEEKLY = "3";				// メール受信ランキング週間 
		public const string RANKING_MAIL_MONTHLY = "4";				// メール受信ランキング月間
		public const string RANKING_MOVIE_ACCESS_WEEKLY = "5";		// 動画プロフ被閲覧ランキング週間 
		public const string RANKING_MOVIE_ACCESS_MONTHLY = "6";		// 動画プロフ被閲覧ランキング月間
		public const string RANKING_DIARY_ACCESS_WEEKLY = "7";		// 日記被閲覧ランキング週間 
		public const string RANKING_DIARY_ACCESS_MONTHLY = "8";		// 日記被閲覧ランキング月間
		public const string RANKING_FAVORIT = "9";					// お気に入られ数ランキング
		public const string RANKING_POINT = "A";					// 獲得ﾎﾟｲﾝﾄﾗﾝｷﾝｸﾞ 
		public const string RANKING_TALK_POINT = "B";				// 獲得会話ﾎﾟｲﾝﾄﾗﾝｷﾝｸﾞ日別 
		public const string RANKING_EAGER_BEABER = "C";				// お仕事ﾗﾝｷﾝｸﾞ 
		public const string RANKING_MARKING = "D";					// 足あとランキング
		public const string RANKING_TALK_POINT_WEEKLY = "E";		// 獲得会話ﾎﾟｲﾝﾄﾗﾝｷﾝｸﾞ週間 
		public const string RANKING_TALK_POINT_MONTHLY = "F";		// 獲得会話ﾎﾟｲﾝﾄﾗﾝｷﾝｸﾞ月間 
		public const string RANKING_DIARY_ACCESS_WEEKLY2 = "G";		// 日記被閲覧ランキング週間(月～日)
		public const string RANKING_DIARY_ACCESS_MONTHLY2 = "H";	// 日記被閲覧ランキング月間(先月)
		public const string RANKING_MARKING_MONTHLY = "I";			// 足あとランキング(月間)

		/*----------------------------------*/
		/* 男性ランキング種別				*/
		/*----------------------------------*/
		public const string MAN_RANKING_MARKING_MONTHLY = "1";		// 足あとランキング(月間)

		/*----------------------------------*/
		/* SPエラーステータス				*/
		/*----------------------------------*/
		public const string SP_ERR_STATUS_HAVE_CHILD_RECORD = "E903";	// 子レコードが存在します。 
		public const string SP_ERR_STATUS_ALREADY_UPDATED = "W002";		// 他ユーザーによって内容が更新されました。 
		//public const string SP_ERR_STATUS_ALREADY_REGISTED = "E901";	// 既に登録されています。 
		//public const string SP_ERR_STATUS_NOT_NULL = "E902";			// Not NULL Error
		//public const string SP_ERR_STATUS_HAVE_PARENT_RECORD = "E904";// 親レコードが存在しません。 
		//public const string SP_ERR_STATUS_EXCLUDED = "W001";			// データが排他的に使用されています。(Resource Busy)
		//public const string SP_ERR_STATUS_NOT_DEFINED = "E999";		// エラーコード未定義

		/*----------------------------------*/
		/* 添付種別							*/
		/*----------------------------------*/
		public const int ATTACHED_PROFILE = 1;						// プロフィール用
		public const int ATTACHED_BBS = 2;							// 掲示板用
		public const int ATTACHED_MAIL = 3;							// メール用
		public const int ATTACHED_MOVIE = 4;						// 動画用
		public const int ATTACHED_VOICE = 5;						// 着ボイス用
		public const int ATTACHED_HIDE = 6;							// 非表示
		public const int ATTACHED_PRODUCT = 7;						// 商品		(※添付される類ではないが、OBJ_USED_HISTORYに入れるためここで定義)
		public const int ATTACHED_BLOG = 8;							// ブログ用
		public const int ATTACHED_SOCIAL_GAME = 9;					// ソーシャルゲーム用
		public const int ATTACHED_YAKYUKEN = 10;

		/*----------------------------------*/
		/* 添付方式							*/
		/*----------------------------------*/
		public const string ATTACHED_STOCK = "1";					// ストックより取得 
		public const string ATTACHED_MAILER = "2";					// 携帯より送信 

		/*----------------------------------*/
		/* メール添付種別					*/
		/*----------------------------------*/
		public const int ATTACH_NOTHING_INDEX = 0;					// なし(DB上は0ではなくNULL)
		public const int ATTACH_PIC_INDEX = 1;						// 画像 
		public const int ATTACH_MOVIE_INDEX = 2;					// 動画 
		public const int ATTACH_MAN_USE_ALL_PIC_INDEX = 3;			// 画像(男性がﾒｰﾗｰ・ｽﾄｯｸ両方使う場合のrdoAttache)ﾒｰﾗｰ 
		public const int ATTACH_MAN_USE_ALL_MOVIE_INDEX = 4;		// 動画(男性がﾒｰﾗｰ・ｽﾄｯｸ両方使う場合のrdoAttache)ﾒｰﾗｰ  

		/*----------------------------------*/
		/* 画面ID							*/
		/*----------------------------------*/
		public const string ERR_FRAME_DOWNLOAD_PF_MOVIE = "305";			// PF-MOVIEﾀﾞｳﾝﾛｰﾄﾞｴﾗｰ 
		public const string ERR_FRAME_ATTACHED_PIC = "306";					// 添付画像エラー 
		public const string ERR_FRAME_ATTACHED_MOVIE = "307";				// 添付動画エラー 
		public const string ERR_FRAME_BBS_PIC = "308";						// 画像掲示板エラー 
		public const string ERR_FRAME_BBS_MOVIE = "309";					// 動画掲示板エラー 
		public const string ERR_FRAME_BROWSER = "311";						// 未対応ブラウザエラー 
		public const string ERR_FRAME_USER_BLACK = "316";					// ログイン時ユーザーブラック(停止)
		public const string ERR_FRAME_USER_STOP = "317";					// ログイン時ユーザーブラック(禁止)
		public const string ERR_FRAME_SET_TERM_ID_MAN = "318";				// 簡単ログイン設定固体識別取得エラー（男）(エラーページに遷移しない修正:対象外)
		public const string ERR_FRAME_CREDIT_CHARGE_GG_FLAG = "319";		// クレジット従量制申請・GG_FLAG_ON	 (エラーページに遷移しない修正:対象外)
		public const string ERR_FRAME_USER_RESIGNED = "320";				// ログイン時ユーザー退会者 
		public const string ERR_FRAME_SET_TERM_ID_WOMAN = "321";			// 簡単ログイン設定固体識別取得エラー（女）(エラーページに遷移しない修正:対象外) 
		public const string ERR_REFUSE_MAN = "322";							// プロフ閲覧時拒否リスト登録（男） 
		public const string ERR_REFUSE_WOMAN = "323";						// プロフ閲覧時拒否リスト登録（女） 
		public const string ERR_FRAME_DOWNLOAD_MOVIE = "324";				// 動画ﾀﾞｳﾝﾛｰﾄﾞエラー 
		public const string ERR_FRAME_DOWNLOAD_VOICE = "325";				// 着ﾎﾞｲｽﾀﾞｳﾝﾛｰﾄﾞエラー 
		public const string ERR_ACCESS_BANNED_CAST = "327";					// 通常ｷｬｽﾄ以外にアクセスした場合（退会や禁止） 
		public const string ERR_ACCESS_BANNED_MAN = "328";					// 通常男性会員以外にアクセスした場合（退会や禁止） 
		public const string ERR_FRAME_PURCHASE_PRODUCT = "329";				// 商品購入エラー
		public const string ERR_FRAME_PURCHASE_PRODUCT_MOVIE = "330";		// 商品動画購入エラー(ﾎﾟｲﾝﾄ不足)
		public const string ERR_FRAME_PURCHASE_PRODUCT_PIC = "331";			// 商品写真購入エラー(ﾎﾟｲﾝﾄ不足)
		public const string ERR_FRAME_DUPLICATE_GUID = "332";				// TEL/LOGIN-IDログイン時GUID重複 
		public const string ERR_FRAME_DUPLICATE_TEL = "333";				// 男性会員電話番号重複(初回Call時に発覚) 
		public const string ERR_FRAME_NO_TEL = "334";						// 男性会員・待機時電話番号未設定 
		public const string ERR_FRAME_OVER_BBS_RES_SEARCH_LIMIT = "335";	// しゃべり場レス検索上限超過エラー 
		public const string ERR_FRAME_OVER_SEARCH_LIMIT_WOMAN = "326";		// メール検索上限超過エラー （女）
		public const string ERR_FRAME_OVER_SEARCH_LIMIT_MAN = "336";		// メール検索上限超過エラー （男）

		public const string SCR_BITCASH_OK = "850";							// BITCASH OK 
		public const string SCR_BITCASH_NG = "851";							// BITCASH NG 
		public const string SCR_SETUP_MAIL_COMPLITE = "852";				// メール受信設定完了 
		public const string SCR_DELETED_CAST_MAIL = "853";					// キャストメール削除完了 
		public const string SCR_DELETED_INFO_MAIL = "854";					// お知らせメール削除完了 
		public const string SCR_TX_MAIL_COMPLITE_MAN = "855";				// 男性会員メール送信完了 
		public const string SCR_TX_REMAIL_COMPLITE = "856";					// 返信メール送信完了 
		public const string SCR_MODIFY_MAN_PROFILE_COMPLITE = "857";		// 男性プロフィール変更完了 
		public const string SCR_USE_FREEDIAL_COMPLITE = "858";				// フリーダイアル利用設定完了 
		public const string SCR_NON_USE_FREEDIAL_COMPLITE = "859";			// フリーダイアル解除設定完了 
		public const string SCR_TX_MAIL_COMPLITE_CAST = "860";				// キャストメール送信完了  
		public const string SCR_DELETED_MAN_MAIL = "861";					// ユーザーメール削除完了 
		public const string SCR_TX_REMAIL_COMPLITE_CAST = "862";			// キャスト返信メール送信完了 
		public const string SCR_SETUP_MAIL_COMPLITE_CAST = "863";			// キャストメール受信設定完了 
		public const string SCR_DELETED_CAST_DIARY = "864";					// キャスト日記削除完了 
		public const string SCR_WRITE_DIARY_COMPLITE_CAST = "865";			// キャスト日記書込完了 
		public const string SCR_DELETED_CAST_PROFILE_MOVIE = "866";			// キャストプロフィール動画削除完了 
		public const string SCR_TALK_INVITE_MAIL_INVALID = "867";			// 会話招待メール無効 
		public const string SCR_MODIFY_PROFILE_PIC_COMPLITE = "868";		// キャスト画像プロフィール変更完了 
		public const string SCR_MODIFY_GALLERY_PIC_COMPLITE = "869";		// キャスト画像ギャラリー変更完了 
		public const string SCR_DELETED_CAST_PIC = "870";					// キャスト画像削除完了 
		public const string SCR_DELETED_CAST_MAIL_PIC = "871";				// キャストメール添付画像削除完了 
		public const string SCR_DELETED_CAST_MAIL_MOVIE = "872";			// キャストメール添付動画削除完了 
		public const string SCR_DELETED_USER_MAN_MAIL_PIC = "873";			// 男性会員メール添付画像削除完了 
		public const string SCR_DELETED_USER_MAN_MAIL_MOVIE = "874";		// 男性会員メール添付動画削除完了 
		public const string SCR_DELETED_CAST_BBS_PIC = "876";				// キャスト写メ掲示板用画像削除完了 
		public const string SCR_DELETED_CAST_BBS_MOVIE = "878";				// キャストムービー掲示板用動画削除完了 
		public const string SCR_UPDATED_CAST_BBS_PIC = "879";				// キャスト写メ掲示板用画像文章修正完了 
		public const string SCR_UPDATED_CAST_BBS_MOVIE = "880";				// キャストムービー掲示板用動画文章修正完了 
		public const string SCR_CAST_WAITING_START = "881";					// キャスト待機開始 
		public const string SCR_CAST_WAITING_END = "882";					// キャスト待機終了 
		public const string SCR_SERVICE_POINT_MAIL_INVALID = "883";			// サービスポイントメール無効 
		public const string SCR_CREDIT_QUICK_OK = "884";					// クレジットQUICK OK
		public const string SCR_CREDIT_QUICK_NG = "885";					// クレジットQUICK NG
		public const string SCR_PREPAID_TRANS_OK = "886";					// プリペード振替 OK
		public const string SCR_PREPAID_TRANS_NG = "887";					// プリペード振替 NG
		public const string SCR_DELETED_MAN_TX_MAIL = "888";				// 男性会員送信メール削除完了 
		public const string SCR_MODIFY_WOMAN_PROFILE_COMPLITE = "889";		// 女性プロフィール変更完了 
		public const string SCR_REGIST_TEMP_WOMAN_COMPLITE = "890";			// 女性仮登録完了 
		public const string SCR_PAY_AFTER_OK = "891";						// 後払パック購入完了 
		public const string SCR_MODIFY_BANK_COMPLITE = "892";				// 銀行口座変更完了 
		public const string SCR_SET_TERM_ID_COMPLITE_MAN = "893";			// 簡単ログイン設定完了（男性） 
		public const string SCR_TX_BATCH_MAIL_COMPLETE_CAST = "894";		// キャスト->会員メール一括送信完了 
		//NonUse
		//		public const string SCR_EXIST_DELETE_TALK_HISTORY = "895";			// 会話履歴削除確認 
		public const string SCR_DELETED_TALK_HISTORY = "896";				// 会話履歴削除完了 
		public const string SCR_SET_TERM_ID_COMPLITE_WOMAN = "897";			// 簡単ログイン設定完了（女性） 
		public const string SCR_MAN_WAITING_START = "898";					// 男性会員待機開始 
		public const string SCR_MAN_WAITING_END = "899";					// 男性会員待機終了  
		public const string SCR_AGE_AUTH = "900";							// 年齢認証
		public const string SCR_TX_REPORT_MAIL_COMPLETED = "950";			// 違反報告メール送信完了 
		public const string SCR_DELETED_CAST_TX_MAIL = "951";				// キャスト送信メール削除完了 
		public const string SCR_DELETE_USER_BBS_COMPLETE = "952";			// ユーザー掲示板削除完了 
		public const string SCR_DELETE_CAST_BBS_COMPLETE = "953";			// キャスト掲示板削除完了 
		public const string SCR_WRITE_USER_BBS_COMPLETE = "954";			// ユーザー掲示板書き込み完了 
		public const string SCR_WRITE_CAST_BBS_COMPLETE = "955";			// キャスト掲示板書き込み完了 
		public const string SCR_DELETED_CAST_INFO_MAIL = "956";				// キャストお知らせメール削除 
		public const string SCR_REGIST_MAN_COMPLITE = "957";				// 男性登録完了/折ﾒｰﾙ登録時 
		public const string SCR_REGIST_MAN_BY_CAST_COMPLITE = "957";		// 男性登録完了(キャスト指定あり)/折ﾒｰﾙ登録時 
		public const string SCR_WRITE_MESSAGE_COMPLITE = "958";				// ﾒｯｾｰｼﾞ書き込み完了(男性) 
		public const string SCR_DELETED_MAN_MESSAGE = "959";				// ﾒｯｾｰｼﾞ削除（男性） 
		public const string SCR_WOMAN_USE_FREEDIAL_COMPLITE = "960";		// フリーダイアル利用設定完了(女性)  
		public const string SCR_WOMAN_NON_USE_FREEDIAL_COMPLITE = "961";	// フリーダイアル解除設定完了(女性)  
		public const string SCR_WOMAN_WRITE_MESSAGE_COMPLITE = "962";		// ﾒｯｾｰｼﾞ書き込み完了(女性) 
		public const string SCR_DELETED_WOMAN_MESSAGE = "963";				// ﾒｯｾｰｼﾞ削除（女性） 
		public const string SCR_RELEASE_TERM_ID_COMPLITE_WOMAN = "964";		// 簡単ログイン設定解除
		public const string SCR_TX_REPORT_MAIL_COMPLETED_WOMAN = "965";		// 違反報告メール送信完了(女性)
		public const string SCR_DELETED_TALK_HISTORY_ALL = "966";			// 会話履歴削除完了(GROUP)
		public const string SCR_MODIFY_PIC_CLOSED_COMPLITE = "967";			// キャスト画像非表示変更完了 
		public const string SCR_MAN_RETURN_RESIGNED_COMPLITE = "968";		// 男性退会復帰完了 
		public const string SCR_WOMAN_RETURN_RESIGNED_COMPLITE = "969";		// 女性退会復帰完了 
		public const string SCR_WOMAN_PIC_RETURN_MAILER_COMPLITE = "970";	// 添付付きメール送信完了(女性、写真、返信) 
		public const string SCR_WOMAN_MOVIE_RETURN_MAILER_COMPLITE = "971";	// 添付付きメール送信完了(女性、動画、返信) 
		public const string SCR_WOMAN_PIC_BBS_MAILER_COMPLITE = "972";		// 画像BBSへの書込み完了(先にﾃｷｽﾄ・本文入力ﾀｲﾌﾟ) 
		public const string SCR_WOMAN_MOVIE_BBS_MAILER_COMPLITE = "973";	// 動画BBSへの書込み完了(先にﾃｷｽﾄ・本文入力ﾀｲﾌﾟ) 
		public const string SCR_WOMAN_PIC_PROFILE_MAILER_COMPLITE = "974";	// 無料アルバムへの書込み完了(先にﾃｷｽﾄ・本文入力ﾀｲﾌﾟ) 
		public const string SCR_PAYMENT_APPLICATION_COMPLITE = "975";		// 精算申請完了 
		public const string SCR_PAYMENT_CVS_DIRECT_COMPLITE = "976";		// コンビニダイレクト受付完了 
		public const string SCR_MAN_PIC_RETURN_MAILER_COMPLITE = "977";		// 添付付きメール送信完了(男性、写真、返信) 
		public const string SCR_MAN_MOVIE_RETURN_MAILER_COMPLITE = "978";	// 添付付きメール送信完了(男性、動画、返信) 
		public const string SCR_MAN_SECESSION_COMPLITE = "979";				// 男性退会完了 
		public const string SCR_WOMAN_SECESSION_COMPLITE = "980";			// 女性退会完了 
		public const string SCR_MAN_REMINDER_OK = "981";					// 男性リマインダー完了 
		public const string SCR_WOMAN_REMINDER_OK = "982";					// 女性リマインダー完了 
		public const string SCR_MAN_MODIFY_TEL_COMPLETE = "983";			// 男性電話番号変更完了 
		public const string SCR_WOMAN_MODIFY_TEL_COMPLETE = "984";			// 女性電話番号変更完了 
		public const string SCR_WOMAN_MOVIE_PROFILE_MAILER_COMPLITE = "985";// 無料動画への書込み完了(先にﾃｷｽﾄ・本文入力ﾀｲﾌﾟ) 
		public const string SCR_MODIFY_TALK_HIS_MEMO_COMPLATE = "986";		// 会話履歴メモ変更完了 
		public const string SCR_CAST_MODIFY_MEMO = "987";					// メモ変更完了(女性) 
		public const string SCR_CAST_DELETE_MEMO = "988";					// メモ削除完了(女性) 
		public const string SCR_MAN_MODIFY_MEMO = "989";					// メモ変更完了(男性) 
		public const string SCR_MAN_DELETE_MEMO = "990";					// メモ削除完了(男性) 
		public const string SCR_MAN_CHARACTER_CREATE_COMPLITE = "991";		// 男性ｷｬﾗｸﾀｰ作成完了 
		public const string SCR_NO_BALANCE_WAITING_MAN = "992";				// 所持ポイントが足りないので待機できません。 
		public const string SCR_MAN_BATCH_MAIL_DEL = "993";					// 男性会員メール一括削除完了 
		public const string SCR_CAST_BATCH_MAIL_DEL = "994";				// 女性会員メール一括削除完了 
		public const string SCR_EDY_SETTLE_OK = "995";						// EDY受付完了 
		public const string SCR_EDY_SETTLE_NG = "996";						// EDY受付失敗  
		public const string SCR_WOMAN_RETURN_TO_ORIGINAL_MENU = "9097";		// 女性会員・男性メニュー利用不可
		public const string SCR_REFUSED_CAST = "9098";						// 女性会員を拒否済みのため電話・メール不可(男性)
		public const string SCR_REFUSED_MAN = "9099";						// 男性会員を拒否済みのため電話・メール不可(女性)
		public const string SCR_CANCEL_BATCH_MAIL_COMPLITE = "9100";		// 女性会員・一括未送信メールのキャンセル完了 
		public const string SCR_MAN_RETURN_TO_ORIGINAL_MENU = "9101";		// 男性会員・女性メニュー利用不可 
		public const string SCR_MAN_USE_WHITE_PLAN = "9102";				// 男性会員・ホワイトプラン利用設定完了 
		public const string SCR_MAN_SETTING_FAVORIT_LOGIN_MAIL_RX = "9103";	// 男性会員・お気に入り女性ログインメール受信拒否設定完了 
		public const string SCR_WOMAN_BLOG_REGISTER_NG = "9104";			// 女性会員・ブログ登録不可 
		public const string SCR_WOMAN_BLOG_UPDATE_COMPLETE = "9105";		// 女性会員・ブログ更新完了 
		public const string SCR_MAN_RESET_MAIL_NG_COMPLETE = "9106";		// 男性会員・メールNG解除完了 
		public const string SCR_WOMAN_RESET_MAIL_NG_COMPLETE = "9107";		// 女性会員・メールNG解除完了 
		public const string SCR_INVITE_MAIL_LIMIT_OVER = "9108";			// 女性会員・会話招待メール送信上限数超過 
		public const string SCR_GET_WAITING_POINT_SUCCESS = "9109";			// 女性会員・待機ポイント・追加完了 
		public const string SCR_GET_WAITING_POINT_ADDED = "9110";			// 女性会員・待機ポイント・追加済み 
		public const string SCR_GET_WAITING_POINT_NO_WAIT = "9111";			// 女性会員・待機ポイント・待機していない 
		public const string SCR_GET_WAITING_POINT_BLACK_LIST = "9112";		// 女性会員・待機ポイント・ブラックリスト 
		public const string SCR_GET_WAITING_POINT_SYS_ERROR = "9113";		// 女性会員・待機ポイント・その他の理由で追加せず 	
		public const string SCR_BBS_EX_CREATE_THREAD_COMPLETE = "9114";		// しゃべり場スレッド作成完了 
		public const string SCR_BBS_EX_RES_COMPLETE = "9115";				// しゃべり場レス完了 
		public const string SCR_BBS_EX_RETURN_RES_COMPLETE = "9116";		// しゃべり場返信レス完了 
		public const string SCR_BBS_EX_MODIFY_THREAD_COMPLETE = "9117";		// しゃべり場スレッド編集完了 
		public const string SCR_BBS_EX_DELETE_THREAD_COMPLETE = "9118";		// しゃべり場スレッド削除完了 
		public const string SCR_BLOG_PREFER_PUBLISH = "9119";				// ブログ：公開申請完了 
		public const string SCR_POINT_LACK_MAIL = "9120";					// メール書込みポイント不足
		public const string SCR_POINT_LACK_MOVIE = "9121";					// 動画閲覧ポイント不足
		public const string SCR_BBS_EX_BLACK = "9122";						// しゃべり場利用禁止
		public const string SCR_DELETED_BLOG_MAIL = "9123";					// ﾌﾞﾛｸﾞ通知削除
		public const string SCR_CANT_USE_RICHINO = "9124";					// 女性会員リッチーノ閲覧不可 
		public const string SCR_OVER_VIEW_RICHINO_OBJ = "9125";				// リッチーノ特典閲覧回数オーバー 
		public const string SCR_MAN_SETUP_RX_MAIL_LIGHT_COMPLETE = "9126";	// 男性 メール受信簡易設定 完了  
		public const string SCR_WOMAN_SETUP_RX_MAIL_LIGHT_COMPLETE = "9127";// 女性 メール受信簡易設定 完了  
		public const string SCR_WOMAN_COLOMOBA_REQUEST_NG = "9128";			// 女性 コロモバ登録リクエストNG
		public const string SCR_MAN_ACCEPT_WITHDRAWAL_COMPLETE = "9129";	// 男性 退会受付完了 
		public const string SCR_WOMAN_ACCEPT_WITHDRAWAL_COMPLETE = "9130";	// 女性 退会受付完了 
		public const string SCR_QUICK_RESPONSE_BLACK = "9131";				// 女性 クイックレスポンスルームブラック 
		public const string SCR_QUICK_RESPONSE_OUT = "9132";				// 女性 クイックレスポンスルーム退室 
		public const string SCR_AUCTION_BID_ERR = "9133";					// 男性 ｵｰｸｼｮﾝ入札 
		public const string SCR_AUCTION_BID_OK = "9134";					// 男性 ｵｰｸｼｮﾝ入札成功 
		public const string SCR_WOMAN_PIC_TX_MAILER_COMPLITE = "9135";		// 添付付きメール送信完了(女性、写真、送信) 
		public const string SCR_WOMAN_MOVIE_TX_MAILER_COMPLITE = "9136";	// 添付付きメール送信完了(女性、動画、送信) 
		public const string SCR_MAN_PIC_TX_MAILER_COMPLITE = "9137";		// 添付付きメール送信完了(男性、写真、送信) 
		public const string SCR_MAN_MOVIE_TX_MAILER_COMPLITE = "9138";		// 添付付きメール送信完了(男性、動画、送信) 
		public const string SCR_MAN_BINGO_ENTRY_RESULT = "9139";			// 男性 ﾋﾞﾝｺﾞｴﾝﾄﾘｰ完了 
		public const string SCR_WOMAN_BINGO_ENTRY_RESULT = "9140";			// 女性 ﾋﾞﾝｺﾞｴﾝﾄﾘｰ完了 
		public const string SCR_MAN_BINGO_APPLICATION = "9141";				// 男性 ﾋﾞﾝｺﾞﾎﾞﾀﾝ押下結果 
		public const string SCR_WOMAN_BINGO_APPLICATION = "9142";			// 女性 ﾋﾞﾝｺﾞﾎﾞﾀﾝ押下結果 
		public const string SCR_WOMAN_LIVE_CHAT_NO_APPLY = "9143";			// 女性 ﾗｲﾌﾞﾁｬｯﾄ認証がされていない 
		public const string SCR_WRITE_DIARY_COMPLITE_CAST_PIC = "9144";		// キャスト日記書込完了(画像添付) 
		public const string SCR_TX_MAIL_JEALOUSY_REDIRECT = "9145";			// メール送信時相手から見てオフライン 
		public const string SCR_MAN_SMART_PHONE_EASYLOGIN_NG = "9146";		// 男性 ｽﾏｰﾄﾌｫﾝで簡単ﾛｸﾞｲﾝできない場合、ｴﾗｰﾍﾟｰｼﾞを表示する
		public const string SCR_WOMAN_SMART_PHONE_EASYLOGIN_NG = "9147";	// 女性 ｽﾏｰﾄﾌｫﾝで簡単ﾛｸﾞｲﾝできない場合、ｴﾗｰﾍﾟｰｼﾞを表示する
		public const string SCR_UPDATED_CAST_PLANNING_PIC = "9148";			// キャスト写メ企画画像文章修正完了 
		public const string SCR_UPDATED_CAST_PLANNING_MOVIE = "9149";		// キャスト写メ企画画像文章修正完了 
		public const string SCR_MAN_USE_CROSMILE_COMPLITE = "9150";			// 男性 CROSMILE利用設定完了 
		public const string SCR_MAN_NON_USE_CROSMILE_COMPLITE = "9151";		// 男性 CROSMILE解除設定完了 
		public const string SCR_WOMAN_USE_CROSMILE_COMPLITE = "9152";		// 女性 CROSMILE利用設定完了 
		public const string SCR_WOMAN_NON_USE_CROSMILE_COMPLITE = "9153";	// 女性 CROSMILE解除設定完了  
		public const string SCR_WOMAN_WAIT_BY_TALKING = "9154";				// 女性 通話中に待機動作を実行した 
		public const string SCR_MAN_REGIST_CROSMILE_COMPLITE = "9155";		// 男性 ｸﾛｽﾏｲﾙ登録完了 
		public const string SCR_WOMAN_REGIST_CROSMILE_COMPLITE = "9156";	// 女性 ｸﾛｽﾏｲﾙ登録完了 
		public const string SCR_MAN_MODIFY_PROFILE_PIC_COMPLITE = "9157";	// 男性 プロフィール画像修正完了 
		public const string SCR_DELETED_CAST_DIARY_SWITCH = "9158";			// キャスト日記削除完了 
		public const string SCR_WRITE_DIARY_COMPLITE_CAST_SWITCH = "9159";	// キャスト日記書込完了 
		public const string SCR_WRITE_DIARY_COMPLITE_CAST_PIC_SWITCH = "9160";// キャスト日記書込完了(画像添付) 


		/*----------------------------------*/
		/* HTML_DOC MAX_COUNT				*/
		/*----------------------------------*/
		public const int ADMIN_REPORT_HTML_DOC_MAX = 5;					// 管理者連絡HTML
		public const int MODIFY_HISTORY_HTML_DOC_MAX = 5;				// 変更履歴HTML

		/*----------------------------------*/
		/* ユーザーランク					*/
		/*----------------------------------*/
		public const string USER_RANK_A = "A";							// ユーザーランクA
		public const string USER_RANK_B = "B";							// ユーザーランクB
		public const string USER_RANK_C = "C";							// ユーザーランクC
		public const string USER_RANK_D = "D";							// ユーザーランクD
		public const string USER_RANK_E = "E";							// ユーザーランクE
		public const string USER_RANK_F = "F";							// ユーザーランクF
		public const string USER_RANK_G = "G";							// ユーザーランクG
		public const string USER_RANK_H = "H";							// ユーザーランクH

		/*----------------------------------*/
		/*	TOPﾍﾟｰｼﾞｷｬｽﾄ抽出条件			*/
		/*----------------------------------*/
		public const int CAST_TOP_PICKUP = 1;
		public const int CAST_TOP_TALK_MOVIE_PICKUP = 2;

		/*----------------------------------*/
		/*	メインキャラクター番号			*/
		/*----------------------------------*/
		public const string MAIN_CHAR_NO = "00";

		/*----------------------------------*/
		/*	フラグ(ON/OFF)			        */
		/*----------------------------------*/
		public const int FLAG_WITHOUT = -1;                             // 未設定 
		public const int FLAG_ON = 1;									// 有効
		public const int FLAG_OFF = 0;									// 無効
		public const string FLAG_WITHOUT_STR = "-1";                    // 未設定(文字列)
		public const string FLAG_ON_STR = "1";							// 有効(文字列)
		public const string FLAG_OFF_STR = "0";							// 無効(文字列)

		/*----------------------------------*/
		/*	デコメサーバー種別		        */
		/*----------------------------------*/
		public const string DECO_SRV_COCOSPACE = "1";					// ココスペース
		public const string DECO_SRV_ACCEL = "2";						// アクセルメール
		public const string DECO_SRV_OLD = "9";							// メディア

		/*----------------------------------*/
		/*	督促種別				        */
		/*----------------------------------*/
		public const string URGE_CALL = "1";							// 電話
		public const string URGE_MAIL = "2";							// メール
		public const string URGE_CALL_MAIL = "3";						// 両方

		/*----------------------------------*/
		/* 運営会社MASK		 				*/
		/*----------------------------------*/
		public const int MSK_COMPANY_MASHUP = 0x0000001;				// マッシュアップ 
		public const int MSK_COMPANY_MFREE = 0x0000002;					// エムフリー 
		public const int MSK_COMPANY_HARPYAN = 0x0000004;				// ハーピアン 
		public const int MSK_COMPANY_ARCHE = 0x0000008;					// アルケ 
		public const int MSK_COMPANY_PWILD = 0x0000010;					// ピーワイルド 
		public const int MSK_COMPANY_TOPGEAR = 0x0000020;				// トップギア 
		public const int MSK_COMPANY_SRONLINE = 0x0000040;				// エスアールオンライン 
		public const int MSK_COMPANY_IMPACT = 0x0000080;				// インパクト




		/*----------------------------------*/
		/* VCS ﾒﾆｭｰID		 				*/
		/*----------------------------------*/
		public const string VCS_MENU_TELLINK = "1";						// 電話番号認証
		public const string VCS_MENU_RELEASE_PF = "12";					// ＰＦ転送解除
		public const string VCS_MENU_DIRECT_TALK = "23";				// 指名ツーショット
		public const string VCS_MENU_DIRECT_REC_PF = "24";				// 個別ＰＦ録音
		public const string VCS_MENU_DIRECT_PLAY_PF = "25";				// 個別ＰＦ再生
		public const string VCS_MENU_WIRETAPPING = "26";				// 盗聴
		public const string VCS_MENU_GPF_TALK = "27";					// 逆PF会話
		public const string VCS_MENU_PLAY_PV_MSG = "28";				// プライベートメッセージ再生

		/*----------------------------------*/
		/*　IVP接続種別						*/
		/*----------------------------------*/
		public const string IVP_CONNECT_TYPE_VIDEO = "1";				// VIDEO
		public const string IVP_CONNECT_TYPE_VOICE = "2";				// VOICE

		/*----------------------------------*/
		/* 共有ﾗｲﾌﾞﾄｰｸ処理要求				*/
		/*----------------------------------*/
		public const string SHARE_LIVE_KEY_CREATE = "1";				// 共有ﾗｲﾌﾞﾄｰｸｷｰ発行 
		public const string SHARE_LIVE_KEY_TO_ACCEPT_NO = "2";			// 共有ﾗｲﾌﾞﾄｰｸｷｰ->ACCEPT番号変換

		/*----------------------------------*/
		/* 録音種別							*/
		/*----------------------------------*/
		public const string REC_TYPE_MESSAGE = "1";						// 伝言
		public const string REC_TYPE_PROFILE = "2";						// ﾌﾟﾛﾌｨｰﾙ 
		public const string REC_TYPE_MAN_WAITING = "3";					// 男性待機 

		/*----------------------------------*/
		/* FILE UPLOAD時ユーザー情報		*/
		/*----------------------------------*/
		public const string FILE_UPLOAD_USERNAME = "vicomm";
		public const string FILE_UPLOAD_PASSWORD = "vicomm";

		/*----------------------------------*/
		/* 次へ・前へのリンク種別			*/
		/*----------------------------------*/
		public const string ORDER_PREVIOUS = "prev";
		public const string ORDER_NEXT = "next";

		/*----------------------------------*/
		/* セッション格納用文字列			*/
		/*----------------------------------*/
		public const string SESSION_USER_SEQ_LIST = "SESSION_USER_SEQ_LIST";		// メール一括送信の対象ユーザー一覧

		/*----------------------------------*/
		/* エラーステータス					*/
		/*----------------------------------*/
		//CONFIRM
		public const string CONFIRM_STATUS_DELETE_TX_MAIL_LOG = "C107";				// 送信メール履歴削除確認 
		public const string CONFIRM_STATUS_DELETE_RX_MAIL_LOG = "C108";				// 受信メール履歴削除確認 
		public const string CONFIRM_STATUS_DELETE_INFO_MAIL_LOG = "C109";			// お知らせメール履歴削除確認 
		public const string CONFIRM_STATUS_DELETE_TXRX_MAIL_LOG = "C110";			// 送受信メール履歴削除確認 
		//INFOMATION
		public const string INFO_STATUS_DELETE_TX_MAIL_LOG = "I106";				// 送信メール履歴削除完了 
		public const string INFO_STATUS_DELETE_RX_MAIL_LOG = "I107";				// 受信メール履歴削除完了 
		public const string INFO_STATUS_DELETE_INFO_MAIL_LOG = "I108";				// お知らせメール履歴削除完了 
		public const string INFO_STATUS_ISSUE_PERSONAL_AD = "I109";					// 個人別広告ｺｰﾄﾞ発行完了 
		//ERROR
		public const string ERR_STATUS_MODIFY_COMMENT_COMPLATE = "I501";			// お気に入り・拒否メモ更新完了 
		public const string ERR_STATUS_CAST_TO_MAN_MAIL_LIMIT_OVER = "E501";		// 女性から男性へﾒｰﾙ一括送信時送信上限オーバー 
		public const string ERR_STATUS_MAN_NOT_FOUND = "E502";						// 男性会員検索未検出 
		public const string ERR_STATUS_CAST_TO_MAN_MAIL_NOT_MARKING = "E503";		// 足あとがついていない男性会員にメール送信 
		public const string ERR_STATUS_TITLE = "E504";								// タイトル未入力 
		public const string ERR_STATUS_DOC = "E505";								// 本文未入力 
		public const string ERR_STATUS_NG_WORD = "E506";							// NGワードあり 
		public const string ERR_STATUS_PREPAID_NO_BALANCE = "E507";					// プリペード残高なし 
		public const string ERR_STATUS_PREPAID_LOGIN = "E508";						// プリペードID・PWエラー 
		public const string ERR_STATUS_PREPAID_NG_TERM_ID = "E509";					// プリペード認証時固体識別取得不可 
		public const string ERR_STATUS_SAMPLE_PREPAID_NG = "E510";					// お試しプリペード利用不可 
		public const string ERR_STATUS_BLACK_USER = "E511";							// ブラックユーザーのため登録不可 
		public const string ERR_STATUS_TEL_NO = "E512";								// 電話番号未入力 
		public const string ERR_STATUS_TEL_NO_NG = "E513";							// 電話番号入力値不正 
		public const string ERR_STATUS_TEL_NO_ALREADY_EXIST = "E514";				// 電話番号登録済み
		public const string ERR_STATUS_HANDLE_NM = "E515";							// ハンドルネーム未入力 
		public const string ERR_STATUS_BIRTHDAY = "E516";							// 生年月日未入力 
		public const string ERR_STATUS_BIRTHDAY_YYYY_NG = "E517";					// 生年月日入力値不正(年) 
		public const string ERR_STATUS_UNDER_18 = "E518";							// 18歳未満 (プロフ登録時) 
		public const string ERR_STATUS_BIRTHDAY_NG = "E519";						// 生年月日入力値不正 
		public const string ERR_STATUS_REGIST_LOGIN = "E523";						// 新規登録時ログインエラー
		public const string ERR_STATUS_INVALID_ID_PW = "E524";						// ID・PASSWORD不正
		public const string ERR_STATUS_NO_BALANCE = "E525";							// 残高なし 
		public const string ERR_STATUS_PASSWORD = "E526";							// PASSWORD未入力 
		public const string ERR_STATUS_PASSWORD_NG = "E527";						// PASSWORD入力値不正
		public const string ERR_STATUS_MODIFIED_TEL_NO_ALREADY_EXIST = "E528";		// 電話番号登録済み(プロフ変更時)
		//		public const string ERR_STATUS_FAMILY_NM = "E529";							// 氏名未入力 
		public const string ERR_STATUS_TERM_ID_ALREADY_EXIST = "E530";				// 固体識別登録済み
		public const string ERR_STATUS_REGIST_NOT_DEFINED = "E531";					// 登録エラー(未定義)
		public const string ERR_STATUS_PAY_AFTER_UNAUTH_TEL = "E532";				// 後払パック購入時未認証電話番号
		public const string ERR_STATUS_PAY_AFTER_RECEIPT_ZERO = "E533";				// 後払パック購入時０回入金者購入不可
		public const string ERR_STATUS_PAY_AFTER_OVER_LIMIT = "E534";				// 後払パック購入時限度額超過
		public const string ERR_STATUS_BANK_NM = "E535";							// 銀行名未入力 
		public const string ERR_STATUS_BANK_OFFICE_NM = "E536";						// 支店名未入力 
		public const string ERR_STATUS_BANK_OFFICE_KANA_NM = "E559";				// 支店名カナ未入力 
		public const string ERR_STATUS_BANK_ACCOUNT_TYPE = "E537";					// 口座種類未入力 
		public const string ERR_STATUS_BANK_ACCOUNT_NO = "E538";					// 口座番号未入力 
		public const string ERR_STATUS_BANK_ACCOUNT_HOLDER_NM = "E539";				// 口座名義未入力 
		public const string ERR_STATUS_NO_TERM_ID = "E540";							// 固体識別取得不可
		public const string ERR_STATUS_TERM_ID_ALREADY_EXIST_BY_SET = "E541";		// 固体識別登録済み(再取得時)
		public const string ERR_STATUS_UNAUTH_TEL = "E542";							// 未認証電話番号(男性会員ログイン時)
		public const string ERR_STATUS_BLACK_USER_LOGIN = "E543";					// ブラックユーザー(男性会員ログイン時)
		public const string ERR_STATUS_RESIGNED_USER = "E544";						// 退会ユーザー(男性会員ログイン時)
		public const string ERR_STATUS_CAST_LOGIN_AUTH_WAIT = "E549";				// 認証待ち(キャストログイン時) 
		public const string ERR_STATUS_CAST_LOGIN_STOP = "E550";					// 利用禁止(キャストログイン時) 
		public const string ERR_STATUS_CAST_LOGIN_RESIGNED = "E551";				// 退会(キャストログイン時) 
		public const string ERR_STATUS_CAST_LOGIN_BAN = "E552";						// 利用停止(キャストログイン時) 
		public const string ERR_STATUS_EXIST_CREDIT_DEAL = "E554";					// ０円決済時クレジット決済あり 
		public const string ERR_STATUS_EXIST_SOME_TEL = "E555";						// キャスト電話番号認証複数該当あり 
		public const string ERR_STATUS_HAVE_TO_CHECK_RULE = "E556";					// 利用規約確認  
		public const string ERR_STATUS_HAVE_TO_CHECK_MAIL = "E557";					// 特定電子ﾒｰﾙ受取確認 
		public const string ERR_STATUS_NOT_EXIST_REGIST_APPLY = "E558";				// 登録申請なし 
		public const string ERR_STATUS_MESSAGE_OVER_LEN = "E563";					// ﾒｯｾｰｼﾞ本文が長すぎます。 
		public const string ERR_STATUS_NM_KANA = "E567";							// 氏名カナ未入力 
		public const string ERR_STATUS_NOT_AGREE = "E568";							// 規約に同意していない 
		public const string ERR_STATUS_END_LIVE = "E569";							// ライブ終了 
		public const string ERR_STATUS_END_MONIOR_TALK = "E570";					// モニター対象会話終了 
		public const string ERR_STATUS_BUSY = "E571";								// 相手話中
		public const string ERR_STATUS_END_ONLINE_MONIOR = "E572";					// 部屋モニター対象終了 
		public const string ERR_STATUS_OVER_ONLINE_MONIOR = "E573";					// 部屋モニタ数超過
		public const string ERR_STATUS_IVP_REQ_NO_TEL = "E574";						// IVP要求時電話番号未設定 
		public const string ERR_STATUS_MAN_BUSY = "E576";							// 男性待機呼出時(話中) 
		public const string ERR_STATUS_MAN_NO_BALANCE = "E577";						// 男性待機呼出時(残高なし)  
		public const string ERR_STATUS_BANK_OFFICE_NO = "E578";						// 支店番号未入力または数字３桁以外 
		public const string ERR_NOTHING_MOVIE = "E579";								// 動画不在エラー 
		public const string ERR_NOTHING_MOVIE_FOR_SOFTBANK = "E580";				// Softbank向け動画不在エラー 
		public const string ERR_STATUS_INVALID_RESIGNED = "E581";					// 退会復帰時、退会者が見つかりませんでした 
		public const string ERR_STATUS_NO_RESIGNED_USER = "E582";					// 退会復帰時、ステータスが退会以外 
		public const string ERR_STATUS_RESIGNED_RETURN_EXIST_SOME_TEL = "E583";		// 退会復帰時、電話番号複数該当あり 
		public const string ERR_STATUS_RESIGNED_RETURN_UNAUTH_TEL = "E584";			// 退会復帰時、電話番号未認証 
		public const string ERR_STATUS_OBJ_ATTR = "E585";							// 属性未選択 
		public const string ERR_STATUS_SETTLE_SERVER = "E586";						// 決済サーバー通信エラー 
		public const string ERR_STATUS_FAMILY_NM = "E587";							// 漢字氏名(姓)未入力 
		public const string ERR_STATUS_GIVEN_NM = "E588";							// 漢字氏名(名)未入力 
		public const string ERR_STATUS_ADDR = "E589";								// 漢字住所1未入力 
		public const string ERR_NOTHING_VOICE = "E590";								// 着ボイス不在 
		public const string ERR_STATUS_MAIL_ADDR = "E591";							// メールアドレス未入力 
		public const string ERR_STATUS_NOT_EXISTS_MAIL_ADDR = "E592";				// メールアドレス不在 
		public const string ERR_STATUS_HANDLE_NM_DUPLI = "E593";					// ﾊﾝﾄﾞﾙ名重複 
		public const string ERR_STATUS_INPUT_REQ_FILED = "E594";					// {0}が未入力です 
		public const string ERR_STATUS_VALIDATE_BANK_ACCOUNT_NO = "E595";			// 口座番号は数字で入力してください 
		public const string ERR_STATUS_VALIDATE_OFFICE_KANA_NM = "E596";			// 支店名カナはカタカナで入力してください 
		public const string ERR_STATUS_VALIDATE_BANK_ACCOUNT_HOLDER_NM = "E597";	// 口座名義は全角カタカナで姓名はスペースで区切ってください 
		public const string ERR_STATUS_DISABLED_TX_MAIL = "E598";					// 通話中、またはﾀﾞﾐｰ通話中にメールを送信することは出来ません。 
		public const string ERR_STATUS_REVIEWS = "E599";							// 評価が選択されていません 
		public const string ERR_STATUS_CAST_WRITE_BBS_LIMIT = "E600";				// 1日に書込可能な回数を超えています。 
		public const string ERR_STATUS_CAST_WRITE_BBS_INTERVAL_SEC = "E601";		// 前回書込からの期間が短すぎます。 
		public const string ERR_STATUS_MAN_WRITE_BBS_LIMIT = "E602";				// 1日に書き込み可能な回数を超えています。 
		public const string ERR_STATUS_MAN_WRITE_BBS_INTERVAL_SEC = "E603";			// 前回書込からの期間が短すぎます。 
		public const string ERR_STATUS_DISABLED_TX_MAIL_JEALOUSY = "E604";			// やきもち防止機能によって相手にオフラインに見られています。 
		public const string ERR_STATUS_SELECT_REQ_FILED = "E605";					// {0}が未選択です 
		public const string ERR_STATUS_NOT_FOUND_PERSONAL_AD_CD = "E606";			// 口コミ紹介コードが見つかりません 
		public const string ERR_STATUS_PERSONAL_AD_USED = "E607";					// 個人別広告ｺｰﾄﾞ利用中 
		public const string ERR_STATUS_PERSONAL_INVALID = "E608";					// 個人別広告ｺｰﾄﾞ入力形式不正  
		public const string ERR_STATUS_NOT_SELECT_SITE_CD = "E609";					// 登録ｻｲﾄを選んでください。  
		public const string ERR_STATUS_OVER_LETTER_COUNT = "E610";					// {0}文字以内で入力して下さい。 
		public const string ERR_STATUS_IVP_REQ_NO_WAITING = "E611";					// IVP要求時ｷｬｽﾄが待機状態ではない 
		public const string ERR_STATUS_VIOLATION_NOT_SELECTED = "E612";				// 通報理由未選択  
		public const string ERR_STATUS_UNDER_18_MODIFY = "E613";					// 18歳未満(プロフ変更時) 
		public const string ERR_STATUS_LENGTH_OVER_FAV_MEMO = "E614";				// お気に入りメモ文字数オーバー  
		public const string ERR_STATUS_LENGTH_OVER_REF_MEMO = "E615";				// 拒否メモ文字数オーバー 
		public const string ERR_STATUS_OVER_BYTE_COUNT = "E616";					// {0}が長すぎます 
		public const string ERR_STATUS_SENT_MAIL = "E617";							// このメールは送信済みです 
		public const string ERR_STATUS_NM_KANJI = "E618";							// 氏名漢字を入力して下さい。 
		public const string ERR_STATUS_LENGTH_OVER_TITLE = "E619";					// 件名の文字が多すぎます 
		public const string ERR_STATUS_VALIDATE_KANA_FIELD = "E620";				// {0}はカタカナで入力してください。 
		public const string ERR_STATUS_VALIDATE_BANK_CD = "E621";					// 金融機関コードは半角数字で入力してください。 
		public const string ERR_STATUS_VALIDATE_BANK_OFFICE_CD = "E622";			// 支店コードは半角数字で入力してください。 
		public const string ERR_STATUS_BANK_CD = "E623";							// 金融機関コード未入力 
		public const string ERR_STATUS_BANK_OFFICE_CD = "E624";						// 支店コード未入力 
		public const string ERR_STATUS_DISABLED_TX_MAIL_OFFLINE = "E625";			// 通話中、またはﾀﾞﾐｰ通話中にメールを送信することは出来ません。 
		public const string ERR_STATUS_JEALOUSY = "E626";							// やきもち設定で片方だけｵﾌﾗｲﾝにすることは出来ません。 
		public const string ERR_STATUS_UNDER_23 = "E627";							// 23歳未満 (女性会員登録時) 
		public const string ERR_STATUS_POSTER_DEL_NA = "E628";						// 投稿者削除不可 
		public const string ERR_STATUS_VALIDATE_RANGE_BANK_ACCOUNT_NO = "E629";		// 口座番号は7桁で入力してください 
		public const string ERR_STATUS_NO_BANK_DATA = "E630";						// 銀行情報に不備があります  
		public const string ERR_STATUS_BANK_ACCOUNT_NO_LENGTH_NOT_7 = "E635";		// 口座番号は7桁です。不足している場合は頭に0をつけて下さい。 

		// 商品用
		public const string ERR_STATUS_BID_AMT_EMPTY = "P001";						// 入札金額未入力 
		// TELMIN用(MINKURU)
		public const string ERR_STATUS_MINKURU_HANDLE_NM_EMPTY = "MI01";			// ハンドルネームを入力してください。 
		public const string ERR_STATUS_MINKURU_HANDLE_NM_LENGTH = "MI02";			// ハンドルネームは2文字～10文字の間で入力してください。 
		public const string ERR_STATUS_MINKURU_AREA_EMPTY = "MI03";					// 地域を選択してください。 
		public const string ERR_STATUS_MINKURU_AGE_CHECK = "MI04";					// 18歳未満の方は登録できません。 
		public const string ERR_STATUS_MINKURU_AGREEMENT = "MI05";					// 利用規約に同意していません。 

		// EX01～ PWILD用 
		public const string ERR_STATUS_BLOG_TITLE_EMPTY = "EX01";					// ﾌﾞﾛｸﾞﾀｲﾄﾙ未入力 
		public const string ERR_STATUS_BLOG_DOC_EMPTY = "EX02";						// ﾌﾞﾛｸﾞ紹介文未入力 
		public const string ERR_STATUS_BLOG_TITLE_LENGTH = "EX03";					// ﾌﾞﾛｸﾞﾀｲﾄﾙ桁超過 
		public const string ERR_STATUS_BLOG_DOC_LENGTH = "EX04";					// ﾌﾞﾛｸﾞ紹介文桁超過 
		public const string ERR_STATUS_BLOG_ARTICLE_TITLE_EMPTY = "EX05";			// ﾌﾞﾛｸﾞ投稿ﾀｲﾄﾙ未入力 
		public const string ERR_STATUS_BLOG_ARTICLE_BODY_EMPTY = "EX06";			// ﾌﾞﾛｸﾞ投稿本文未入力 
		public const string ERR_STATUS_BLOG_ARTICLE_TITLE_LENGTH = "EX07";			// ﾌﾞﾛｸﾞ投稿ﾀｲﾄﾙ桁超過 
		public const string ERR_STATUS_BLOG_ARTICLE_BODY_LENGTH_SHORT = "EX08";		// ﾌﾞﾛｸﾞ投稿本文桁不足 
		public const string ERR_STATUS_BLOG_OBJ_ALREADY_USED = "EX09";				// 使用済ブログオブジェクトの再利用不可 
		public const string ERR_STATUS_BLOG_ARTICLE_TAG_NOT_FOUND = "EX10";			// ブログ投稿、タグ(＃＃＃[num]＃＃＃)未使用 
		//BBS関係20～29
		public const string ERR_STATUS_BBS_THREAD_TITLE_EMPTY = "EX20";				// BBSｽﾚｯﾄﾞﾀｲﾄﾙ未入力 
		public const string ERR_STATUS_BBS_THREAD_TITLE_LENGTH = "EX21";			// BBSｽﾚｯﾄﾞﾀｲﾄﾙ桁超過 
		public const string ERR_STATUS_BBS_THREAD_TITLE_NG = "EX22";				// BBSｽﾚｯﾄﾞﾀｲﾄﾙNGﾜｰﾄﾞあり 
		public const string ERR_STATUS_BBS_THREAD_DOC_EMPTY = "EX23";				// BBSｽﾚｯﾄﾞ本文未入力 
		public const string ERR_STATUS_BBS_THREAD_DOC_LENGTH = "EX24";				// BBSｽﾚｯﾄﾞ本文桁超過 
		public const string ERR_STATUS_BBS_THREAD_DOC_NG = "EX25";					// BBSｽﾚｯﾄﾞ本文NGﾜｰﾄﾞあり 
		public const string ERR_STATUS_BBS_RES_EMPTY = "EX26";						// BBSﾚｽ本文未入力 
		public const string ERR_STATUS_BBS_RES_LENGTH = "EX27";						// BBSﾚｽ桁超過 
		public const string ERR_STATUS_BBS_RES_NG = "EX28";							// BBSﾚｽNGﾜｰﾄﾞあり 
		public const string ERR_STATUS_BBS_SEARCH_KEY_EMPTY = "EX29";				// BBS検索ﾜｰﾄﾞ未入力 
		//その他30～ 
		public const string ERR_STATUS_MAIL_SAME_SEND = "EX30";						// 同一相手・内容2分以内送信不可 
		public const string ERR_WITHDRAWAL_REASON_DOC_EMPTY = "EX31";				// 退会理由未入力 
		public const string INFO_STATUS_QUICK_RES_TITLE = "EX32";					// クイックレスポンスタイトル

		/*----------------------------------*/
		/* 番号種別							*/
		/*----------------------------------*/
		public const string NO_TYPE_TERMINAL_UNIQUE_ID = "1";
		public const string NO_TYPE_TEL = "2";

		/*----------------------------------*/
		/* NGﾜｰﾄﾞ表記区分					*/
		/*----------------------------------*/
		public const string NG_WORD_MARK_TYPE_STRING = "1";							// 正規表現
		public const string NG_WORD_MARK_TYPE_REGULAR = "2";						// 通常文章

		/*----------------------------------*/
		/* ｷｬｽﾄ成果報酬発生ﾀｲﾐﾝｸﾞ			*/
		/*----------------------------------*/
		public const string AFFILIATE_REPORT_CERTIFIED = "1";						// 認証完了時
		public const string AFFILIATE_REPORT_SETUP_PROFILE = "2";					// ﾌﾟﾛﾌｨｰﾙ登録完了時

		/*----------------------------------*/
		/* 男性会員成果報酬発生ﾀｲﾐﾝｸﾞ		*/
		/*----------------------------------*/
		public const string MAN_AFFILIATE_REPORT_REGISTED = "1";					//登録完了時 
		public const string MAN_AFFILIATE_REPORT_LOGIN = "2";						//初回ﾛｸﾞｲﾝ時 
		public const string MAN_AFFILIATE_REPORT_BUY_POINT = "3";					//初回ﾎﾟｲﾝﾄ購入時 
		public const string MAN_AFFILIATE_REPORT_MAIL_OK = "4";						//登録完了ﾒｰﾙ到達時(ｴﾗｰﾘﾀｰﾝﾒｰﾙなし) 
		public const string MAN_AFFILIATE_REPORT_PROFILE = "5";						//ﾌﾟﾛﾌｨｰﾙ登録完了時 

		/*----------------------------------*/
		/* 変更利用区分MASK					*/
		/*----------------------------------*/
		public const int MASK_MODIFY_REASON_REGIST = 0x001;							// 登録 
		public const int MASK_MODIFY_REASON_MODIFY = 0x002;							// 変更 
		public const int MASK_MODIFY_REASON_DELETE = 0x004;							// 削除 
		public const int MASK_MODIFY_REASON_RECEIPT = 0x008;						// 入金 
		public const int MASK_MODIFY_REASON_TRANS_POINT = 0x010;					// ポイント振替 
		public const int MASK_MODIFY_REASON_CANCEL_RECEIPT = 0x020;					// 入金取消 

		/*----------------------------------*/
		/* 変更利用区分						*/
		/*----------------------------------*/
		public const string MODIFY_REASON_REGIST_INFO = "01";						//登録 
		public const string MODIFY_REASON_MODIFY_INFO = "02";						//訂正 
		public const string MODIFY_REASON_DELETE_INFO = "03";						//削除 
		public const string MODIFY_REASON_STATUS_CHANGE = "04";						//状態変更 
		public const string MODIFY_REASON_RECEIPT = "05";							//入金 
		public const string MODIFY_REASON_CHANGE_REGIST_TYPE = "06";				//クレジット->後払振替 
		public const string MODIFY_REASON_TRANS_POINT_PREPAID = "07";				//ポイント振替(プリペード)
		public const string MODIFY_REASON_TRANS_POINT_CREDIT_PACK = "08";			//ポイント振替(クレジット)
		public const string MODIFY_REASON_TRANS_POINT_C_CHECK = "09";				//ポイント振替(C-CHECK)
		public const string MODIFY_REASON_TRANS_POINT_BITCASH = "10";				//ポイント振替(BITCASH)
		public const string MODIFY_REASON_TRANS_POINT_S_MONEY = "11";				//ポイント振替(S-MONEY)
		public const string MODIFY_REASON_CANCEL_RECEIPT = "12";					//入金取消 
		public const string MODIFY_REASON_AUTO_RECEIPT = "13";						//自動入金 
		public const string MODIFY_REASON_CHANGE_REGIST_CREDIT = "14";				//後払->クレジット振替 
		public const string MODIFY_REASON_TRANS_POINT_CVSDL = "15";					//ポイント振替(CVSDL)
		public const string MODIFY_REASON_TRANS_POINT_A_CHECK = "16";				//ポイント振替(A-CHECK)
		public const string MODIFY_REASON_TRANS_POINT_SSAD = "17";					//ポイント振替(SSAD)
		public const string MODIFY_REASON_TRANS_POINT_OFFICIAL_CHECK = "18";		//ポイント振替(OC)
		public const string MODIFY_REASON_TRANS_POINT_MCHA = "19";					//ポイント振替(MCHA)
		public const string MODIFY_REASON_TRANS_POINT_POINT_GATE = "20";			//ポイント振替(POINT GATE)
		public const string MODIFY_REASON_TRANS_POINT_G_MONEY = "21";				//ポイント振替(G-MONEY)
		public const string MODIFY_REASON_TRANS_POINT_PAYMENT_AFTER = "22";			//ポイント振替(後払パック)
		public const string MODIFY_REASON_TRANS_POINT_POICHA = "23";				//ポイント振替(PoiCha)
		public const string MODIFY_REASON_TRANS_POINT_SAKAZUKI = "24";				//ポイント振替(SAKAZUKI)
		public const string MODIFY_REASON_TRANS_POINT_CREDIT = "25";				//ポイント振替(クレジット従量)
		public const string MODIFY_REASON_TRANS_POINT_SAKAZUKI_LIST = "26";			//ポイント振替(SAKAZUKI_LIST)
		public const string MODIFY_REASON_TRANS_POINT_MOBILE_CASH = "27";			//携帯送金
		public const string MODIFY_REASON_TRANS_POINT_CREDIT_AUTH = "28";			//ポイント振替(０円決済)
		public const string MODIFY_REASON_TRANS_POINT_PBK = "29";					//ポイント振替(ﾎﾟｲﾝﾄｱﾌﾘ・ﾗｲｵ社)
		public const string MODIFY_REASON_TRANS_POINT_GIGA_POINT = "30";			//ポイント振替(GIGA-POINT)
		public const string MODIFY_REASON_TRANS_POINT_FRIEND_POINT = "31";			//友達紹介ポイント追加
		public const string MODIFY_REASON_TRANS_POINT_RAKUTEN = "32";				//ポイント振替(楽天あんしん決済)
		public const string MODIFY_REASON_MODIFY_MAIL_ADDR = "36";					//メールアドレス変更
		public const string MODIFY_REASON_MODIFY_TEL = "37";						//電話番号変更
		public const string MODIFY_REASON_MODIFY_LOGIN_PASSWORD = "39";				//パスワード変更

		/*----------------------------------*/
		/* 入力種別							*/
		/*----------------------------------*/
		public const string INPUT_TYPE_LIST = "0";									//リスト 
		public const string INPUT_TYPE_TEXT = "1";									//テキストボックス 
		public const string INPUT_TYPE_RADIO = "2";									//ラジオボタン 

		/*----------------------------------*/
		/* 発信結果							*/
		/*----------------------------------*/
		public const string CALL_RESULT_NONE = "00";								//通話要求がありました 
		public const string CALL_RESULT_NONE_NO_CALL = "01";						//通話ページまで来ましたが、TEL発信せず別ページへ行ってしまいました 
		public const string CALL_RESULT_REQUESTER_DISCONN = "11";					//男性の任意切断によって通話が終了しました 
		public const string CALL_RESULT_PARTNER_DISCONN = "12";						//貴女の任意切断によって通話が終了しました 
		public const string CALL_RESULT_POINT_DISC = "13";							//男性のﾎﾟｲﾝﾄがなくなった為、通話が終了しました 
		public const string CALL_RESULT_NO_ANSWER = "21";							//貴女が電話に出なかった為、接続できませんでした 
		public const string CALL_RESULT_BUSY = "22";								//貴女の携帯電話が通話中だった為、接続できませんでした 
		public const string CALL_RESULT_NO_MACH = "23";                             //男性がTV電話か、音声通話の種別を間違えた為、接続できませんでした 
		public const string CALL_RESULT_NO_NUMBER = "24";                           //登録されている電話番号に誤りがある為、接続できませんでした 
		public const string CALL_RESULT_REFUSE = "97";								//貴女の携帯電話が通話中だった為、接続できませんでした （着信拒否） 
		public const string CALL_RESULT_FAIL_OTHERS = "98";                         //その他の原因によって通話が終了しました 
		public const string CALL_RESULT_FAIL_SYS_ERROR = "99";                      //申し訳ございませんが、システムエラーの為、接続できませんでした 

		/*----------------------------------*/
		/* 簡易発信結果(男性で利用)			*/
		/*----------------------------------*/
		public const string CALL_RESULT_EASY_MAN_DISCONN = "1";						//男性側の任意切断 
		public const string CALL_RESULT_EASY_CAST_DISCONN = "2";					//女性側の任意切断 
		public const string CALL_RESULT_EASY_POINT_DISCONN = "3";					//ポイント切れ 
		public const string CALL_RESULT_EASY_SYS_DISCONN = "4";						//システムによる切断 

		/*----------------------------------*/
		/* 発信結果検索種類					*/
		/*----------------------------------*/
		public const string FIND_CALL_RESULT_SUCCESS = "0";							// 通話成立分のみ(発信と着信) 
		public const string FIND_CALL_RESULT_GROUP_MAN = "1";						// 男性会員別に(発信と着信) 
		public const string FIND_CALL_RESULT_ALL = "2";								// 全ての履歴を(発信と着信) 
		public const string FIND_CALL_RESULT_SUCCESS_INCOMMING = "3";				// 通話成立分のみ(着信) 
		public const string FIND_CALL_RESULT_GROUP_MAN_INCOMMING = "4";				// 男性会員別に(着信) 
		public const string FIND_CALL_RESULT_ALL_INCOMMING = "5";					// 全ての履歴を(着信) 
		public const string FIND_CALL_RESULT_SUCCESS_OUTGOING = "6";				// 通話成立分のみ(発信) 
		public const string FIND_CALL_RESULT_GROUP_MAN_OUTGOING = "7";				// 男性会員別に(発信) 
		public const string FIND_CALL_RESULT_ALL_OUTGOING = "8";					// 全ての履歴を(発信) 

		/*----------------------------------*/
		/* 発呼状態							*/
		/*----------------------------------*/
		public const string CALL_RPT_OUTGOING = "1";								// 呼出中
		public const string CALL_RPT_CONNECT = "2";									// 接続完了

		public const string CALL_RPT_DISCONNECT = "3";								// 切断
		public const string CALL_RPT_CHANGE_TO_TALK = "4";							// 会話移行通知
		public const string CALL_RPT_CHANGE_TO_BROADCAST = "5"; 					// 放送移行通知
		
		/*----------------------------------*/
		/* 販売動画関連      				*/
		/*----------------------------------*/
		public const string TARGET_USE_TYPE_DOWNLOAD = "d";							// ﾀﾞｳﾝﾛｰﾄﾞ 
		public const string TARGET_USE_TYPE_STREAMING = "s";						// ｽﾄﾘｰﾐﾝｸﾞ  
		public const string PREFIX_SAMPLE_MOVIE = "sample_";						// サンプル動画 

		/*----------------------------------*/
		/* キャリア別ファイルSuffix			*/
		/*----------------------------------*/
		public const string FILE_SUFFIX_DOCOMO = "d";		// docomo
		public const string FILE_SUFFIX_AU = "a";			// au
		public const string FILE_SUFFIX_SOFTBANK = "s";		// softbank
		/*----------------------------------*/
		/* 音声ファイル拡張子				*/
		/*----------------------------------*/
		public const string FILE_EXTENSION_DOCOMO = "3gp";		// docomo
		public const string FILE_EXTENSION_AU = "mmf";			// au
		public const string FILE_EXTENSION_SOFTBANK = "mmf";	// softbank

		/*----------------------------------*/
		/* ソートタイプ						*/
		/*----------------------------------*/
		public const string SORT_NO = "0";											//何もしない 
		public const string SORT_ASC = "1";											//昇順ソート追加 
		public const string SORT_DESC = "2";										//降順ソート追加 

		/*----------------------------------*/
		/* ｷｬﾗｸﾀｰ利用不可区分				*/
		/*----------------------------------*/
		public const int NA_CHAR_NONE = 0; 						// NONE
		public const int NA_CHAR_ADMIN = 1;						// ﾕｰｻﾞｰ状態により自動設定または管理者指定 
		public const int NA_CHAR_PF_NOT_APPROVED = 2;			// ﾌﾟﾛﾌｨｰﾙ未認証 
		public const int NA_CHAR_IMPORT = 3;					// ｲﾝﾎﾟｰﾄﾃﾞｰﾀ未ﾛｸﾞｲﾝ 
		public const int NA_CHAR_OUTSIDE_REGIST = 4;			// 外部I/F登録  
		public const int NA_CHAR_INVISIBLE = 5;					// 女性から見えなくなる





		/*----------------------------------*/
		/* ﾃﾞﾊﾞｲｽ種別						*/
		/*----------------------------------*/
		public const string DEVICE_3G = "1"; 					// 3G
		public const string DEVICE_SMART_PHONE = "2"; 			// ANDROID + IPHONE
		public const string DEVICE_ANDROID = "3"; 				// ANDROID
		public const string DEVICE_IPHONE = "4"; 				// IPHONE


		/*----------------------------------*/
		/* 退会受付状態						*/
		/*----------------------------------*/
		public const string WITHDRAWAL_STATUS_CHOICE = "1"; 	// 選択のみ 
		public const string WITHDRAWAL_STATUS_ACCEPT = "2"; 	// 受付 
		public const string WITHDRAWAL_STATUS_COMPLIATE = "3"; 	// 退会完了 


		/*----------------------------------*/
		/* 個別機能設定      				*/
		/*----------------------------------*/
		/*------------------------------------------------------------------*/
		/*追加した場合は、9_RECORDのT_RELEASE_PROGRAMとPKG_MANAGE_COMPANYと	*/
		/*9_RECORD/各会社FOLDER/T_MANAGE_COMPANYも修正すること				*/
		/*------------------------------------------------------------------*/
		public const ulong RELEASE_CAST_IP_PIC_AUTH = 0x0000000000000001;				// キャスト認証時身分証写真要求 
		public const ulong RELEASE_SUPPORT_STAFF_ID = 0x0000000000000002;				// キャスト従業員コード対応  従業員コードの保有及び表示・出力 
		public const ulong RELEASE_VALIDATE_ACCOUNT_INFO = 0x0000000000000004;			// キャスト支払銀行口座変更時認証処理 
		public const ulong RELEASE_CAST_CSV_ATTR_OUTPUT = 0x0000000000000008;			// キャストCSVに属性等も出力 
		public const ulong RELEASE_SUPPORT_BANK_OFFICE_NO = 0x0000000000000010;			// キャスト支払銀行支店番号対応 
		public const ulong RELEASE_SALE_MOVIE = 0x0000000000000020;						// 動画販売 
		public const ulong RELEASE_SALE_VOICE = 0x0000000000000040;						// 着ボイス販売 
		public const ulong RELEASE_REGIST_CONSTELLATION = 0x0000000000000080;			// 属性に星座を自動登録する
		public const ulong RELEASE_TIME_CALL = 0x0000000000000100;						// 定時ｺｰﾙｻｰﾋﾞｽ 
		public const ulong RELEASE_MULTI_PICKUP = 0x0000000000000200;					// ピックアップリスト複数作成 
		public const ulong RELEASE_ATTR_INQUIRY = 0x0000000000000400;					// 属性による会員検索 
		public const ulong RELEASE_CAST_OPERATION_HANDLE_NM = 0x0000000000000800;		// 出演者別稼動集計でハンドル名を表示する
		public const ulong RELEASE_PERFORMANCE_FREE_DETAIL = 0x0000000000001000;		// 稼動集計でフリーダイヤル細分化をする 
		public const ulong RELEASE_KICK_BACK_INTRO_SYSTEM = 0x0000000000002000;			// 新お友達紹介（入金、獲得ポイントのキックバック、異性紹介） 
		public const ulong RELEASE_UNIQUE_ACCESS_SUMMARY = 0x0000000000004000;			// ｱｸｾｽ集計のﾕﾆｰｸ数集計 
		public const ulong RELEASE_GALLERY_PIC_INPUT_TITLE_AND_DOC = 0x0000000000008000;// 無料画像のタイトルと本文の入力 
		public const ulong RELEASE_CONVERT_IMAGE = 0x0000000000010000;					// 画像変換機能 
		public const ulong RELEASE_NEW_VARIABLE = 0x0000000000020000;					// 新変数でのモバイル画面表示対応 
		public const ulong RELEASE_RICHINO = 0x0000000000040000;						// リッチーノランク対応 
		public const ulong RELEASE_NEW_CHAR_UNAUTH = 0x0000000000080000;				// キャストによるキャラ設定をキャラ未認証とする
		public const ulong RELEASE_PROFILE_MOVIE_INPUT_DOC = 0x0000000000100000;		// プロフ動画の本文の入力 
		public const ulong RELEASE_CAST_USE_DUMMY_BIRTHDAY = 0x0000000000200000;		// キャストが営業用誕生日を使用する ※年齢を属性入力しない 
		public const ulong RELEASE_CERTIFY_MAN_PROFILE_PIC = 0x0000000000400000;		// 男性プロフィール写真認証 
		public const ulong RELEASE_CAST_BBS_OBJ_FIND = 0x0000000000800000;				// 女性自身がアップロードした掲示板の画像や動画を属性で検索できるようにする 
		public const ulong RELEASE_FAVORIT_ME_SEARCH = 0x0000000001000000;				// お気に入られ数で出演者検索できるようにする 
		public const ulong RELEASE_CAST_PIC_GALLERY_ATTR = 0x0000000002000000;			// 女性無料画像にてカテゴリを利用する
		public const ulong RELEASE_BLUR_MODIFY_REQUEST = 0x0000000004000000;			// 女性無料画像アップロード時に、モザイク処理依頼ができる 
		public const ulong RELEASE_MAN_MAIL_USE_MAILER = 0x0000000008000000;			// 添付メール送信時に、メールにて直接添付する 
		public const ulong RELEASE_DISABLE_MODIFY_HANDLE_AND_PASS = 0x0000000010000000;	// プロフィール設定で、ハンドルネームとパスワードの変更不可 
		public const ulong RELEASE_UPDATE_SESSION = 0x0000000020000000;					// セッション情報の更新をDisplayDoc表示時に毎回行う 
		public const ulong RELEASE_DISABLE_BBS_MODIFY_LIST = 0x0000000040000000;		// 掲示板の変更・削除というリストボックスを非表示にする 
		public const ulong RELEASE_BANK_TYPE_RADIO = 0x0000000080000000;				// 口座種別をラジオボタンで入力させる 
		public const ulong RELEASE_TX_MAIL_DISABLE_TEMPLATE = 0x0000000100000000;		// メール送信時のテンプレート選択リストを非表示にする 
		public const ulong RELEASE_CHECK_HANDLE_NM_DUPLI = 0x0000000200000000;			// ﾊﾝﾄﾞﾙ名の重複ﾁｪｯｸを行う 
		public const ulong RELEASE_NOT_REDIRECT_REGIST_CAST = 0x0000000400000000;		// キャスト指名ありの登録申請へリダイレクトしない 
		public const ulong RELEASE_BULK_UPDATE_MAN = 0x0000000800000000;				// 男性会員の属性一括更新 
		public const ulong RELEASE_CVS_DIRECT_FORM_OMIT = 0x0000001000000000;			// コンビニダイレクト決済にて、フォームの入力を省略する 
		public const ulong RELEASE_CAST_TEL_INPUT_AFTER = 0x0000002000000000;			// キャスト登録の電話番号入力をプロフ入力画面で行う 
		public const ulong RELEASE_OBJ_ATTR_TYPE_HIDE_ALL = 0x0000004000000000;			// 属性の検索条件から「全て」を消去する 
		public const ulong RELEASE_PORTAL_SITE = 0x0000008000000000;					// ポータルサイトが存在する 
		public const ulong RELEASE_SELECT_HTML_TEXT_VISIBLE = 0x0000010000000000;		// 管理からのメール送信時のメール種別選択を消去する 
		public const ulong RELEASE_REGIST_TEMP_AD = 0x0000020000000000;					// 未登録の広告コードを自動登録する 
		public const ulong RELEASE_MAN_REG_PROF_RESETTING = 0x0000040000000000;			// 男性会員登録時にプロフィール再設定フラグを立てる 
		public const ulong RELEASE_BODY_TEXT_VISIBLE = 0x0000080000000000;				// BODYタグのTEXT属性を利用しない 
		public const ulong RELEASE_RETURN_MAIL_USE_QUOTE = 0x0000100000000000;			// 返信メールにて引用を利用する 
		public const ulong RELEASE_PRODUCT_BUY = 0x0000200000000000;					// 商品購入機能を利用する 
		public const ulong RELEASE_SETTLEMENT_LOG = 0x0000400000000000;					// 現金、携帯送金の決済履歴を作成する  
		public const ulong RELEASE_ADMIN_VIEW_REQEST_HISTORY = 0x0000800000000000;		// 管理画面・男女詳細画面の会話履歴を、通話要求履歴に変更する 
		public const ulong RELEASE_BIND_SELECT_SITE = 0x0001000000000000;				// サイト選択画面を必ず表示する 
		public const ulong RELEASE_PERMIT_NO_BODY_REPORT = 0x0002000000000000;			// 本文なしの通報を許可する
		public const ulong RELEASE_TX_MAIL_BATCH_DISABLE_TEMPLATE = 0x0004000000000000;	// 一括ﾒｰﾙ送信では動画/画像の添付機能を利用しない。 
		public const ulong RELEASE_BATCH_MAIL_SEND_MAIL = 0x0008000000000000;			// 一括メール送信完了時に、送信完了メールを送信する 
		public const ulong RELEASE_ENABLED_WHITE_PLAN = 0x0020000000000000;				// ﾎﾜｲﾄﾌﾟﾗﾝの利用を有効にする 
		public const ulong RELEASE_ASYNC_TX_ADMIN_MAIL = 0x0040000000000000;			// お知らせメール/アプローチメールを非同期で送信する 
		public const ulong RELEASE_NOT_AUTO_UPDATE_GUID = 0x0080000000000000;			// GUIDをログイン時に自動で補完更新しない  
		public const ulong RELEASE_CHECK_TMGT_DUPLI = 0x0100000000000000;				// 電話番号、メールアドレス、GUID、固体識別 の重複チェックを行う 
		public const ulong RELEASE_BATCH_MAIL_LIMIT_NO_CHECK = 0x0400000000000000;		// 一括メール送信上限数の上限チェックをしない 
		public const ulong RELEASE_PROFILE_PIC_AUTH = 0x0800000000000000;				// 常にプロフ画像の認証を行う
		public const ulong RELEASE_BULK_UPDATE_CAST = 0x1000000000000000;				// 女性会員の属性一括更新 
		public const ulong RELEASE_STOCK_MAIL_OBJ_AUTH = 0x2000000000000000;			// メール添付ファイルのストック時に認証を行う
		public const ulong RELEASE_SEND_MAIL_OBJ_AUTH = 0x4000000000000000;				// メール添付ファイル付のメールを送信する際に認証を行う
		/*----------------------------------*/
		/* 個別機能設定2      				*/
		/*----------------------------------*/
		public const ulong RELEASE_MAN_MAIL_USE_ALL = 0x0000000000000001;				// 添付メール送信時に、ストックとメール添付形式の両方を選択できる
		public const ulong RELEASE_CAST_INPUT_BANK_CD = 0x0000000000000002;				// 銀行コードを入力させる 
		public const ulong RELEASE_SEND_MAIL_NG_WORD_MASK = 0x0000000000000004;			// NGﾜｰﾄﾞがﾒｰﾙ文章にあっても送信させる。但しNGﾜｰﾄﾞをｱｽﾀﾘｽｸで隠す 
		public const ulong RELEASE_USE_DIARY_ATTR = 0x0000000000000008;			        // 日記書き込み時に属性を入力させる
		public const ulong RELEASE_DISABLE_DIARY_DAY = 0x0000000000000010;		    	// 日記書き込み時に日付を選択させない 
		public const ulong RELEASE_DISP_NEW_DIARY_ONLY = 0x0000000000000020;			// 日記一覧表示時に、最新の1件のみ表示させる 
		public const ulong RELEASE_COMBI_EMAIL_NG_WITH_RX = 0x0000000000000040;			// メールアドレスNGと受信設定を連動させる 
		public const ulong RELEASE_DISABLED_DELETE_DIARY = 0x0000000000000080;			// 日記を物理削除しない 
		public const ulong RELEASE_RX_MAIL_WAIT_WITH_TALK = 0x0000000000000100;			// 通話したことのある女性がTEL待ちしたときに通知する(PWILDはデフォルトで有効) 
		public const ulong RELEASE_ENABLED_JEALOUSY_DIARY = 0x0000000000000200;			// 日記のやきもち防止機能を有効にする 
		public const ulong RELEASE_INQUIRY_SELECT_COLUMNS = 0x0000000000000400;			// 男女検索の一覧項目の表示可否を選択できるようにする 
		public const ulong RELEASE_TX_JEALOUSY_REDIRECT = 0x0000000000000800;			// ﾒｰﾙ送信時にやきもち設定でｵﾌﾗｲﾝだった場合ﾘﾀﾞｲﾚｸﾄさせる 
		public const ulong RELEASE_OVERWRITE_PRE_REGIST = 0x0000000000001000;			// 仮登録出演者が再度仮登録した際に新しい情報で仮登録する
		public const ulong RELEASE_ENABLE_EASYLOGIN_ERROR = 0x0000000000002000;			// ｽﾏｰﾄﾌｫﾝで簡単ﾛｸﾞｲﾝできない場合、ｴﾗｰﾍﾟｰｼﾞを表示する
		public const ulong RELEASE_NO_DISP_REFUSE_LIKE_ME = 0x0000000000004000;			// 拒否ﾕｰｻﾞからお気に入り登録された際、被お気に入りﾘｽﾄに表示しない

		public const ulong RELEASE_ENABLED_JEALOUSY_PROFILE_PIC = 0x0000000000008000;	// ﾌﾟﾛﾌ画像のやきもち防止機能を有効にする
		public const ulong RELEASE_JEALOUSY_OFFLINE_ANY = 0x0000000000010000;			// やきもち防止機能のﾛｸﾞｲﾝ、待機中のどちらかにｵﾌﾗｲﾝが設定された際、両方をｵﾌﾗｲﾝ設定とする
		public const ulong RELEASE_INQUIRY_MULTI_AD_CD = 0x0000000000020000;			// 男女検索時に、広告コードを複数入力可能にする
		public const ulong RELEASE_DISABLE_PINEDIT = 0x0000000000040000;				// pinEditを使用しない

		public const ulong RELEASE_SEEK_CONDITION_NEGATIVE = 0x0000000000080000;		// 男女検索時に、否定の検索条件を有効にする
		public const ulong RELEASE_ERR_WAIT_BY_TALKING = 0x0000000000100000;			// 通話中に待機延長、待機終了を設定した場合にエラーとする
		public const ulong RELEASE_NON_TITLE_BY_USER_MAIL = 0x0000000000200000;			// 会員間メールにてタイトルを利用しない

		public const ulong RELEASE_NO_DISP_REFUSE_OBJS = 0x0000000000400000;			// 拒否ﾕｰｻﾞには掲示板、画像、動画、日記が表示されなくなる。

		public const ulong RELEASE_KICK_BACK_MAN_WOMAN = 0x0000000000800000;			// キックバックは男性から女性のみ有効にする
		public const ulong RELEASE_ENABLE_EXPANDED_CHARGE = 0x0000000001000000;			// 拡張課金設定を有効にする
		public const ulong RELEASE_DIARY_SWITCH_SCREEN = 0x0000000002000000;			// 日記の表示をｽｸﾘｰﾝID毎に分ける

		public const ulong RELEASE_NOT_REDIRECT_CAST_PC = 0x0000000008000000;			// ｷｬｽﾄｻｲﾄはPCﾘﾀﾞｲﾚｸﾄしない
		public const ulong RELEASE_SP_TEL_NOT_ABSOLUTE = 0x0000000020000000;			// スマートフォンでの会員登録時に、電話番号の入力をサイトの設定に合わせる
		public const ulong RELEASE_SP_LOGIN_RETURN_MAIL_CHAT = 0x0000000080000000;		// ｽﾏｰﾄﾌｫﾝでﾛｸﾞｲﾝ後のﾍﾟｰｼﾞがﾒｰﾙ返信ﾍﾟｰｼﾞに指定されている場合はﾁｬｯﾄﾒｰﾙ表示に遷移させる
		public const ulong RELEASE_DISPLAY_RETURN_MAIL_POINT_LACK = 0x0000000100000000;	// ポイント不足時にもメール返信ページを表示させる
		public const ulong RELEASE_DISABLE_PERSONAL_ATTR_COMBO_BOX = 0x0000000200000000;// 個人別画像・動画一覧のｶﾃｺﾞﾘ検索を表示しない
		public const ulong RELEASE_DISABLE_ATTR_COMBO_BOX_CNT = 0x0000000400000000;		// 画像・動画ｶﾃｺﾞﾘ検索の件数を表示しない
		public const ulong RELEASE_ENABLE_TWILIO = 0x0000000800000000;					// 音声通話はTwilioを利用する
		public const ulong RELEASE_CERTIFY_NOTIFICATION_APP = 0x0000001000000000;		// メール通知アプリでの認証を行う
		public const ulong RELEASE_MAIL_LOTTERY_MOVIE = 0x0000002000000000;				// メールdeガチャのアニメーション表示を行う
		public const ulong RELEASE_MAIL_LOTTERY_MOVIE_IPHONE = 0x0000004000000000;		// iPhoneでメールdeガチャのアニメーション表示を行う
		public const ulong RELEASE_BBS_SEARCH_MULTIPLE_ENTRIES = 0x0000008000000000;	// しゃべり場で検索キーワードを複数入力させる
		public const ulong RELEASE_USER_TEL_REDIRECT = 0x0000010000000000;				// 会員の通話発信時に電話番号登録に飛ばす
		public const ulong RELEASE_DISABLE_EMPTY_TEL_TWILIO = 0x0000020000000000;		// 電話番号未登録者はTwilioを利用しない
		public const ulong RELEASE_CERTIFY_APP_ANDROID = 0x0000040000000000;			// Android端末でメール通知アプリでの認証を行う
		public const ulong RELEASE_CERTIFY_APP_IPHONE = 0x0000080000000000;				// iPhoneでメール通知アプリでの認証を行う
		public const ulong RELEASE_VOICEAPP = 0x0000100000000000;						// 音声通話アプリ
		public const ulong RELEASE_MAN_REJECT_ICLOUD_MAIL = 0x0000200000000000;			// iCloudメールでの会員登録を拒否
		public const ulong RELEASE_VOICEAPP_ONLY_DIAL = 0x0000400000000000;				// 音声通話アプリを発信専用にする
		public const ulong RELEASE_ACCESS_MAIL_COUNT = 0x0000800000000000;				// メールからのアクセス数の集計をおこなう
		public const ulong RELEASE_MAIL_DOMAIN_RANDOM = 0x0001000000000000;				// メール文章ドメインのランダム切替
		public const ulong RELEASE_PAYMENT_CREDIT_VOICE = 0x0002000000000000;			// CREDIX音声ガイダンスによる決済
		public const ulong RELEASE_RETURN_PATH_RANDOM = 0x0004000000000000;				// Return-Pathのランダム切替
		public const ulong RELEASE_FROM_ADDRESS_RANDOM = 0x0008000000000000;			// Fromアドレスのランダム切替
		public const ulong RELEASE_PICKUP_MV_SEARCH_MAIL_TX_DATE = 0x0010000000000000;	// 無料動画ピックアップで最終メール送信日時での絞り込みを行う
		public const ulong RELEASE_PROF_ACCESS_COUNT = 0x0020000000000000;				// プロフアクセス解析を行う
		public const ulong RELEASE_TX_MAIL_FROM_PROFILE = 0x0040000000000000;			// プロフィールページからメール送信を行う
		public const ulong RELEASE_TX_MAIL_FROM_PROFILE_CAST = 0x0080000000000000;		// 出演者が会員プロフィールページからメール送信を行う
		public const ulong RELEASE_INTRO_ACCESS_COUNT = 0x0100000000000000;				// 出演者の異性紹介URLからのアクセスを集計する
		public const ulong RELEASE_CAST_OBJ_NON_PUBLIC_INFO = 0x0200000000000000;		// 出演者の投稿した画像・動画を非公開に設定した際に通知メール送信ページに遷移させる
		public const ulong RELEASE_MAIL_APP_ACCESS_BONUS = 0x0400000000000000;			// メール通知アプリの通知からアクセスした場合にボーナスポイントを付与する
		public const ulong RELEASE_BATCH_MAIL_RECENT_ACTIVE = 0x0800000000000000;		// 一括メールの送信対象を直近のアクティブな会員にする
		public const ulong RELEASE_MAIL_RES_BONUS = 0x1000000000000000;					// メール返信ボーナス
		public const ulong RELEASE_CAST_LIKE_NOTICE = 0x2000000000000000;				// 出演者新着いいね通知

		/*----------------------------------*/
		/* お気に入り検索条件  				*/
		/*----------------------------------*/
		public const string FIND_ALL = "0";											// 全て
		public const string FIND_FAVORITE_ONLY = "1";								// お気に入りの会員のみ
		public const string FIND_NOT_FAVORITE_ONLY = "2";							// お気に入り以外の会員のみ

		/*----------------------------------*/
		/* メール一括削除タイプ 			*/
		/*----------------------------------*/
		public const string DELETE_MAIL_TX = "1";									// 送信メール 
		public const string DELETE_MAIL_RX = "2";									// 受信メール 
		public const string DELETE_MAIL_INFO = "3";									// お知らせメール  
		public const string DELETE_MAIL_TXRX = "4";									// 送受信メール削除

		/*----------------------------------*/
		/* 会話通知マスク					*/
		/*----------------------------------*/
		public const int NOTIFY_TALK_TV = 1;										// TV電話
		public const int NOTIFY_TALK_VOICE = 2;										// 音声電話
		public const int NOTIFY_TALK_BOTH = 4;										// 両方  

		/*----------------------------------*/
		/* 待機状態検索条件					*/
		/*----------------------------------*/
		public class SeekOnlineStatus {
			public const int IGNORE = 0;											// 無視 
			public const int WAITING = 1;											// 待機中のみ 
			public const int LOGINED_AND_WAITING = 2;								// ﾛｸﾞｲﾝ+待機中
			//			public const int WAITING_FIND_USER_INQUIRY = 3;							// 待機中のみ(FinUser利用時のWAITINGは"3"が指定されている)
			public const int LOGINED = 4;
		}

		/*----------------------------------*/
		/* ﾌﾟﾛｸﾞﾗﾑﾙｰﾄ						*/
		/*----------------------------------*/
		public class ProgramRoot {
			public const string Man = "/ViComm/man";
			public const string Woman = "/ViComm/woman";
		}
		/*----------------------------------*/
		/* ﾌﾟﾛｸﾞﾗﾑ							*/
		/*----------------------------------*/
		public class Program {
			public const string LoginUser = "LoginUser.aspx";
		}
		/*----------------------------------*/
		/* ﾌﾟﾛｸﾞﾗﾑ ｱｸｼｮﾝ					*/
		/*----------------------------------*/
		public class ProgramAction {
			public const string LOAD = "Load";
			public const string LOGIN = "Login";
		}

		/*----------------------------------*/
		/* OpenIDﾀｲﾌﾟ						*/
		/*----------------------------------*/
		/// <summary>
		/// OpenIDﾀｲﾌﾟを定義する定数クラス
		/// </summary>
		public class OpenIdTypes {
			/// <summary>楽天ID</summary>
			public const string RAKUTEN = "01";
		}
		/*----------------------------------*/
		/* 動画アスペクト比タイプ			*/
		/*----------------------------------*/
		/// <summary>
		/// 動画アスペクト比タイプを定義する定数クラス
		/// </summary>
		public class MovieAspectType {
			/// <summary>未指定</summary>
			public const string NONE = "0";
			/// <summary>ノーマル(4:3)</summary>
			public const string NORMAL = "1";
			/// <summary>ワイド(16:9)</summary>
			public const string WIDE = "2";
		}

		// ﾒｰﾙ検索の上限(既定値)
		public const int DEFAULT_MAIL_SEARCH_LIMIT = 100;

		/*----------------------------------*/
		/* 掲示板 ｵﾌﾞｼﾞｪｸﾄﾀｲﾌﾟ				*/
		/*----------------------------------*/
		public class BbsObjType {
			public const string BBSDOC = "1";				// BBS書込み 
			public const string PIC = "2";					// BBS写真 
			public const string MOVIE = "3";				// BBS動画 
		}

		/*----------------------------------*/
		/* 画像の回転						*/
		/*----------------------------------*/
		public class ImageRotateType {
			public const string ROTATE_RIGHT_90 = "1";
			public const string ROTATE_LEFT_90 = "2";
			public const string ROTATE_180 = "3";
		}

		/*----------------------------------*/
		/* 写真の縮尺方式						*/
		/*----------------------------------*/
		public class ScaleType {
			public const string ASPECT_RATIO = "1";		// 縦横比 
			public const string SCALE_RATIO = "2";		// 縮尺比 
			public const string SQUARE = "3";			// 正方形 
		}

		/*----------------------------------*/
		/* 使用性別		 					*/
		/*----------------------------------*/
		public class UsableSexCd {
			public const string MAN = "1";					// 男性用 
			public const string WOMAN = "2";				// 女性用 
			public const string COMMON = "3";				// 共通 
		}

		/// <summary>
		/// 一括処理ｽﾃｰﾀｽ
		/// </summary>
		public class BulkProcStatus {
			public const string PROCESSING = "00";				// 処理中 
			public const string SUCCESS = "20";					// 完了 
			public const string SUCCESS_WITH_ERROR = "21";		// 完了(エラーあり)	 
			public const string ERROR = "99";					// エラー 
		}

		/*----------------------------------*/
		/* 口座種別		 					*/
		/*----------------------------------*/
		public class BankAccountType {
			public const string NORMAL_ACCOUNT = "1";		// 普通 
			public const string CHECKING_ACCOUNT = "2";		// 当座 
			public const string SAVINGS_ACCOUNTS = "3";		// 貯蓄 
		}

		/// <summary>
		/// メール送信種別 
		/// </summary>
		public class MailSendType {
			public const int NONE = -1;
			/// <summary>デコメ</summary>
			public const int DECO_MAIL = 1;
			/// <summary>テキストメール</summary>
			public const int TEXT = 2;
		}
		/// <summary>
		/// メール送信サーバー種別 
		/// </summary>
		public class MailServerType {
			public const int NONE = -1;
			/// <summary>デコメ</summary>
			public const int Deco = 1;
			/// <summary>リレイ</summary>
			public const int Relay = 2;
		}

		/// <summary>
		/// ﾋﾟｯｸｱｯﾌﾟ区分 
		/// </summary>
		public class PicupTypes {
			/// <summary>未分類</summary>
			public const string UNCLASSIFIED = "00";
			/// <summary>ｷｬｽﾄｷｬﾗｸﾀｰ</summary>
			public const string CAST_CHARACTER = "01";
			/// <summary>ﾌﾟﾛﾌ画像</summary>
			public const string PROFILE_PIC = "11";
			/// <summary>ﾌﾟﾛﾌ動画</summary>
			public const string PROFILE_MOVIE = "12";
			/// <summary>掲示板画像</summary>
			public const string BBS_PIC = "21";
			/// <summary>掲示板動画</summary>
			public const string BBS_MOVIE = "22";
			/// <summary>野球拳画像</summary>
			public const string YAKYUKEN_PIC = "31";
			/// <summary>ｹﾞｰﾑお宝画像</summary>
			public const string SOCIAL_GAME_PIC = "41";
			/// <summary>ｹﾞｰﾑお宝動画</summary>
			public const string SOCIAL_GAME_MOVIE = "42";
			/// <summary>ﾒｰﾙｽﾄｯｸ画像</summary>
			public const string MAIL_STOCK_PIC = "51";
			/// <summary>ﾒｰﾙｽﾄｯｸ動画</summary>
			public const string MAIL_STOCK_MOVIE = "52";
			/// <summary>類似画像検索</summary>
			public const string RESEMBLED_PIC = "61";
		}

		/// <summary>
		/// ソート区分 
		/// </summary>
		public class SortType {
			/// <summary>新着順</summary>
			public const string NEW = "1";
			/// <summary>最終ﾛｸﾞｲﾝ日時</summary>
			public const string LAST_LOGIN_DATE = "2";
			/// <summary>待機中優先</summary>
			public const string ONLINE_STATUS = "3";
			/// <summary>ﾋﾟｯｸｱｯﾌﾟ公開開始日</summary>
			public const string PICKUP_START_PUB_DAY = "4";
			/// <summary>ログイン中</summary>
			public const string LOGINED = "5";
			/// <summary>待機中</summary>
			public const string WAITING = "6";
			/// <summary>閲覧回数</summary>
			public const string READING_COUNT = "7";
			/// <summary>ランダム</summary>
			public const string RANDOM = "8";
		}

		/// <summary>
		/// 友達紹介区分 
		/// </summary>
		public class FriendIntroType {
			/// <summary>紹介</summary>
			public const string INTRODUCER = "1";
			/// <summary>被紹介</summary>
			public const string UNDER_INTRODUCER = "2";
			/// <summary>ｷｯｸﾊﾞｯｸ</summary>
			public const string KICK_BACK = "3";
		}

		/// <summary>
		/// 商品種別
		/// </summary>
		public class ProductType {
			/// <summary>動画</summary>
			public const string NORMAL_MOVIE = "01";
			/// <summary>写真</summary>
			public const string NORMAL_PIC = "02";
			/// <summary>ｵｰｸｼｮﾝ</summary>
			public const string NORMAL_AUCTION = "03";
			/// <summary>着せ替えﾂｰﾙ</summary>
			public const string THEME = "04";
			/// <summary>ｱﾀﾞﾙﾄ動画</summary>
			public const string ADULT_MOVIE = "11";
			/// <summary>ｱﾀﾞﾙﾄ写真</summary>
			public const string ADULT_PIC = "12";
			/// <summary>ｱﾀﾞﾙﾄｵｰｸｼｮﾝ</summary>
			public const string ADULT_AUCTION = "13";

			public static bool IsMovie(string pProductType) {
				if (string.IsNullOrEmpty(pProductType))
					return false;

				bool bIsMovie = false;
				bIsMovie = bIsMovie || pProductType.Equals(NORMAL_MOVIE);
				bIsMovie = bIsMovie || pProductType.Equals(ADULT_MOVIE);
				return bIsMovie;
			}
			public static bool IsPic(string pProductType) {
				if (string.IsNullOrEmpty(pProductType))
					return false;

				bool bIsPic = false;
				bIsPic = bIsPic || pProductType.Equals(NORMAL_PIC);
				bIsPic = bIsPic || pProductType.Equals(ADULT_PIC);
				return bIsPic;
			}
			public static bool IsAuction(string pProductType) {
				if (string.IsNullOrEmpty(pProductType))
					return false;

				bool bIsAuction = false;
				bIsAuction = bIsAuction || pProductType.Equals(NORMAL_AUCTION);
				bIsAuction = bIsAuction || pProductType.Equals(ADULT_AUCTION);
				return bIsAuction;
			}
			public static bool IsTheme(string pProductType) {
				if (string.IsNullOrEmpty(pProductType))
					return false;

				bool bIsTheme = false;
				bIsTheme = bIsTheme || pProductType.Equals(THEME);
				return bIsTheme;
			}
			public static bool IsAdult(string pProductType) {
				if (string.IsNullOrEmpty(pProductType))
					return false;

				bool bIsAdult = false;
				bIsAdult = bIsAdult || pProductType.Equals(ADULT_MOVIE);
				bIsAdult = bIsAdult || pProductType.Equals(ADULT_PIC);
				bIsAdult = bIsAdult || pProductType.Equals(ADULT_AUCTION);
				return bIsAdult;
			}
		}

		/// <summary>
		/// 商品動画種別
		/// </summary>		
		public class ProductMovieType {
			public const string CONTENTS = "01";
			public const string SAMPLE = "11";
		}
		/// <summary>
		/// 商品写真種別
		/// </summary>		
		public class ProductPicType {
			public const string CONTENTS = "01";
			public const string SAMPLE = "11";
			public const string PACKAGE_A = "21";
			public const string PACKAGE_B = "31";
		}
		/// <summary>
		/// 写真ｻｲｽﾞ種別
		/// </summary>
		[System.Flags]
		public enum PicSizeType:int {
			WITHOUT = -1,
			ORIGINAL = 0,
			LARGE = 1,
			SMALL = 2,
			//			BLUR = 4,
			LARGE_BLUR = LARGE | 4,
			SMALL_BLUR = SMALL | 4
		}
		public const int MAX_OF_RANDOM_NUM_4_CAST = 5;

		/// <summary>
		/// 精算種別
		/// </summary>
		public class PaymentType {
			public const string SELF_PAYMENT = "1";	//手動精算 
			public const string AUTO_PAYMENT = "2";	//月末自動精算 
		}

		/// <summary>メールテンプレート種別</summary>
		public class MailTemplateType {
			/// <summary>通常メール</summary>
			public const string NORMAL = "1001";
		}

		/// <summary>送信メールレイアウト</summary>
		public class TxMailLayOutType {
			/// <summary>通常メール＋待ち合わせメール</summary>
			public const string NORMAL_AND_MEETING = "";
			/// <summary>通常メールのみ</summary>
			public const string NORMAL_MAIL = "1";
			/// <summary>待ち合わせメールのみ</summary>
			public const string MEETING_MAIL = "2";
		}

		public class SeekConditionType {
			public const string CastCharacter = "01";
			public const string UserManCharacter = "02";
		}
		/// <summary>
		/// 商品ランキング区分 
		/// </summary>
		public class ProductRankingType {
			public const string Weekly = "1";
			public const string Monthly = "2";
		}

		/// <summary>
		/// 出演者ブラックリスト区分 
		/// </summary>
		public class CastCharacterBlackType {
			/// <summary>待機ポイント</summary>
			public const string WaitingBonusPoint = "1";
			/// <summary>しゃべり場</summary>
			public const string TalkBbs = "2";
			/// <summary>写真＋動画掲示板</summary>
			public const string BbsObj = "3";
			/// <summary>文章掲示板</summary>
			public const string BbsDoc = "4";
			/// <summary>友達紹介</summary>
			public const string Friends = "5";
			/// <summary>10分以内の返信</summary>
			public const string QuickResponse = "6";
		}

		/// <summary>
		/// ﾕｰｻﾞｰ定義ﾎﾟｲﾝﾄ追加結果
		/// </summary>
		public class UserDefPointResult {
			public const string OK = "0";				//ポイント正常追加 
			public const string NOT_ADD_POINT = "1";	//ポイント未追加 
			public const string NA_FLAG = "2";			//NA_FLAGが1
			public const string TIME_OUT = "3";			//時間外 
			public const string ADDED = "4";			//追加済 
			public const string NOT_FOUND = "9";		//T_USER_DEF_POINTにレコード存在しない 

			public const string NOT_WAITING = "5";		//待機してない 
			public const string BLACKLIST = "6";		//ブラックリスト 
			public const string NOT_USER = "7";			//キャスト存在しない 
		}

		/// <summary>
		/// ブログファイル種別
		/// </summary>
		public class BlogFileType {
			public const string PIC = "1";
			public const string MOVIE = "2";
		}
		/// <summary>
		/// ブログ投稿ｽステータス
		/// </summary>
		public class BlogArticleStatus {
			public const string DRAFT = "1";
			public const string PRIVATE = "2";
			public const string PUBLIC = "3";
		}
		/// <summary>
		/// ブログ投稿結果
		/// </summary>
		public class ModifyBlogArticleResult {
			public const string RESULT_OBJ_ALREADY_USED = "1";
		}

		/// <summary>
		/// ランキング拡張区分 
		/// </summary>
		public class ExRanking {
			/// <summary>足あと</summary>
			public const string EX_RANKING_MARKING = "1";
			/// <summary>モテキング</summary>
			public const string EX_RANKING_FAVORIT_MAN = "2";
			/// <summary>モテコン</summary>
			public const string EX_RANKING_FAVORIT_WOMAN = "3";
		}

		/// <summary>
		/// 美人時計ステータス
		/// </summary>		
		public class BeautyClock {
			public const int MIN_ASSIGNED_TIME = 0;		//0:00分 
			public const int MAX_ASSIGNED_TIME = 1439;	//23:59分 
		}

		/// <summary>
		/// スレッド掲示板ソート種別 
		/// </summary>
		public class BbsThreadSortType {
			public const string NEW_RES = "1";				//レス新着順 
			public const string RES_COUNT = "2";			//レス数順 
			public const string THREAD_ACCESS_COUNT = "3";	//スレッドアクセス数順 
		}

		public class BbsResSortType {
			public const string RES_WRITE_DATE_DESC = "1";	//レス書込み日時降順 
			public const string RES_WRITE_DATE_ASC = "2";	//レス書込み日時昇順 
		}

		/// <summary>
		/// スレッド掲示板管理者フラグ検索条件 
		/// </summary>
		public class BbsThreadSearchAdminType {
			public const string ALL = "1";					//全て 
			public const string ADMIN_FLAG_IS_TRUE = "2";	//管理者スレッド以外 
			public const string ADMIN_FLAG_IS_FALSE = "3";	//管理者スレッドのみ 		
		}

		/// <summary>
		/// この娘を探せステータス 
		/// </summary>	
		public const int MAX_WANTED_HINT = 3;

		///	<summary>
		///	リッチーノステータス 
		/// </summary>
		public const int FREE_BBS_VIEW_NOT_LOGIN_DAYS = 31;		//無料で見れる掲示板、表示条件 

		public class BbsThreadType {
			public const string NORMAL = "0";				//一般 
			public const string RICHINO = "1";				//リッチーノ 
			public const string BLOG = "2";					//ブログ 
		}

		/*----------------------------------*/
		/* ボーナスポイント種別				*/
		/*----------------------------------*/
		public const string BONUS_TYPE_ADMIN = "0";									// 管理者付与 
		public const string BONUS_TYPE_FAVORIT = "1";								// お気入られﾎﾞｰﾅｽ 
		public const string BONUS_TYPE_BLOG = "2";									// ﾌﾞﾛｸﾞﾎﾞｰﾅｽ 
		public const string BONUS_TYPE_OMIKUJI = "3";								// おみくじﾎﾞｰﾅｽ
		public const string BONUS_TYPE_FANCLUB = "4";								// ﾌｧﾝｸﾗﾌﾞ会費
		public const string BONUS_TYPE_REFUSE = "9";								// 拒否によるﾏｲﾅｽ 

		/*----------------------------------*/
		/* ｲﾝﾎﾟｰﾄﾕｰｻﾞ初回ﾛｸﾞｲﾝ時ｱｸｼｮﾝ		*/
		/*----------------------------------*/
		public class ImpFirstLoginActType {
			public const string USER_TOP = "0";				//ﾕｰｻﾞｰﾄｯﾌﾟ 
			public const string MODIFY_HANDLE_NM = "1";		//ﾊﾝﾄﾞﾙﾈｰﾑ編集 
			public const string MODIFY_PROFILE = "2";		//ﾌﾟﾛﾌｨｰﾙ編集 
		}

		/*----------------------------------*/
		/* NA_FLAG							*/
		/*----------------------------------*/
		public class NaFlag {
			public const int OK = 0;						//OK 
			public const int ADMIN = 1;						//管理(自動で変更される)  
			public const int NO_AUTH = 2;					//未認証 
			public const int IMPORT = 3;					//ｲﾝﾎﾟｰﾄ 
			public const int NO_CERTIFIED = 4;				//未認証(ｷｬｽﾄｷｬﾗｸﾀｰ認証用)
		}

		/*----------------------------------*/
		/* ﾕｰｻﾞｰ定義ﾎﾟｲﾝﾄ					*/
		/*----------------------------------*/
		public class UserDefPointId {
			public const string PROFILE_FILL_DOC = "PROFILE_FILL_DOC";			//プロフィール設定完了ポイント 
			public const string PROFILE_PIC = "PROFILE_PIC";					//プロフィール写真添付ポイント(顔写真以外)  
			public const string PROFILE_PIC_FACE = "PROFILE_PIC_FACE";			//プロフィール顔写真添付ポイント 
			public const string WITHDRAWAL_300 = "WITHDRAWAL_300";			// 退会時付与ポイント（300円分）
			public const string WITHDRAWAL_500 = "WITHDRAWAL_500";			// 退会時付与ポイント（500円分）
			public const string WITHDRAWAL_1000 = "WITHDRAWAL_1000";		// 退会時付与ポイント（1000円分）
		}

		/// <summary>同一メールチェックステータス</summary>
		public class SendMailStatus {
			/// <summary>正常</summary>
			public const string NORMAL = "0";
			/// <summary>同一送信有</summary>
			public const string SAME = "9";
		}

		public class QueryParameters {
			public const string PARAM_RELOAD_SELF = "_reloadself";
		}

		/*----------------------------------*/
		/* 10分以内に返事のできる女の子		*/
		/*----------------------------------*/
		public class QuickResponse {
			public const string IN = "1";			//入室
			public const string OUT = "2";			//退室
			public const string EXTENSION = "3";	//延長
		}

		/// <summary>
		/// オークションステータス 
		/// </summary>
		public class AuctionStatus {
			/// <summary>通常</summary>
			public const string Normal = "10";
			/// <summary>落札済</summary>
			public const string WinBid = "50";
			/// <summary>落札辞退</summary>
			public const string DeclinationBid = "70";
			/// <summary>終了</summary>
			public const string End = "80";
			/// <summary>終了(入札無)</summary>
			public const string NonbidEnd = "81";
		}
		/// <summary>
		/// ｵｰｸｼｮﾝ入札ｽﾃｰﾀｽ 
		/// </summary>
		public class AuctionBidStatus {
			/// <summary>通常</summary>
			public const string OK = "00";
			/// <summary>ｵｰｸｼｮﾝ期間前</summary>
			public const string BEFORE_PERIOD = "11";
			/// <summary>ｵｰｸｼｮﾝ期間後</summary>
			public const string AFTER_PERIOD = "12";
			/// <summary>ﾎﾟｲﾝﾄ不足</summary>
			public const string POINT_SHORTAGE = "21";
			/// <summary>入札金額不足</summary>
			public const string BID_AMT_SHORTAGE = "32";
		}

		/// <summary>
		/// 精算ﾀｲﾐﾝｸﾞ種別 
		/// </summary>
		public class PaymentTimingType {
			public const int AutoMonthAndRequest = 0;	//自動精算＋精算申請 
			public const int AutoMonthOnly = 1;			//自動精算のみ
			public const int RequestOnly = 2;			//精算申請のみ
		}

		/// <summary>
		/// ﾋﾞﾝｺﾞｶｰﾄﾞ取得結果 
		/// </summary>
		public class GetBingoCardResult {
			///<summary>正常発行</summary>
			public const string OK = "0";
			///<summary>カード未取得</summary>	
			public const string NOT_HAVE_CARD = "1";
			///<summary>開催されていません</summary>
			public const string NOT_TERM = "2";
		}

		/// <summary>
		/// ﾋﾞﾝｺﾞ状態 
		/// </summary>
		public class BingoStatus {
			///<summary>ビンゴ未達成</summary>
			public const string NOT_BINGO = "0";
			///<summary>ビンゴ達成</summary>
			public const string BINGO = "1";
			///<summary>ビンゴ達成 申請拒否期間</summary>
			public const string BINGO_DENIED = "2";
			///<summary>ビンゴ達成 申請済み</summary>
			public const string BINGO_APPLIED = "3";
			///<summary>ビンゴ達成 申請完了</summary>
			public const string BINGO_COMPLETE = "4";
		}

		/// <summary>
		/// ｻｲﾄ利用状態 
		/// </summary>
		public class SiteUseStatus {
			///<summary>ﾗｲﾌﾞﾁｬｯﾄのみ利用</summary>
			public const string LIVE_CHAT_ONLY = "0";
			///<summary>ｹﾞｰﾑのみ利用</summary>
			public const string GAME_ONLY = "1";
			///<summary>ｹﾞｰﾑとﾗｲﾌﾞﾁｬｯﾄ両方利用</summary>
			public const string LIVE_CHAT_AND_GAME = "2";
		}

		/// <summary>
		/// 女性会員属性一括更新種別
		/// </summary>
		public class BulkUpdateCastType {
			public const int USER_STATUS = 1;
			public const int AD_CD = 2;
			public const int MAIL_ADDR_FLAG = 4;
			public const int DELETE = 8;
			public const int DELETE_CHAR = 16;
		}

		/// <summary>
		/// 認証要請メール種別
		/// </summary>
		public class SendMailUnauthType {
			public const string MAN_PROFILE_PIC = "1";
			public const string CAST_PROFILE_PIC = "2";
			public const string MAN_STOCK_MAIL_PIC = "3";
			public const string MAN_STOCK_MAIL_MOVIE = "4";
			public const string MAN_MAILER_MAIL_PIC = "5";
			public const string MAN_MAILER_MAIL_MOVIE = "6";
			public const string CAST_STOCK_MAIL_PIC = "7";
			public const string CAST_STOCK_MAIL_MOVIE = "8";
			public const string CAST_MAILER_MAIL_PIC = "9";
			public const string CAST_MAILER_MAIL_MOVIE = "10";
			public const string CAST_PROFILE_MOVIE = "11";
			public const string CAST_BBS_PIC = "12";
			public const string CAST_BBS_MOVIE = "13";
		}

		/// <summary>
		/// プロフィール画像種別
		/// </summary>
		public class ProfilePicType {
			/// <summary>プロフィール</summary>
			public const string PROFILE = "1";
			/// <summary>ギャラリー</summary>
			public const string GALLERY = "2";
			/// <summary>非表示</summary>
			public const string HIDE = "3";
			/// <summary>削除</summary>
			public const string DELETE = "4";
		}

		/// <summary>
		/// 日記オブジェクトタイプ 
		/// </summary>
		public class DiaryObjType {
			public const string DOC = "1";					// 書込み(添付無し) 
			public const string PIC = "2";					// 写真 
		}

		///<summary>
		/// 還元ポイントタイプ 
		/// </summary>
		/// 
		public class PointReductionType {
			public const string InputValue = "0";           //入力値 
			public const string AddRate = "1";           //加算率 
		}

		///<summary>
		/// 変更者区分 
		/// </summary>
		public class ModifyUserCd {
			public const string Admin = "0";				//管理者 
			public const string User = "1";					//ユーザー 
		}


		public class CastPicAttrTypeDef {
			// 企画画像アイテム№ 
			public const string PLAN_PIC_ITEM_NO = "99";
		}
		public class CastMovieAttrTypeDef {
			// 企画動画アイテム№ 
			public const string PLAN_MOVIE_ITEM_NO = "99";
		}

		public class UseCrosmile {
			public const string NOT_USE = "0";
			public const string USE_UAC_ONLY = "1";
			public const string USE_UAS_ONLY = "2";
			public const string USE_UAC_AND_UAS = "3";
		}

		public class ProfilePicNonPublishFlag {
			/// <summary>なし</summary>
			public const string NONE = "0";
			/// <summary>プロフ以外</summary>
			public const string EXCEPT_PROFILE = "1";
			/// <summary>全て</summary>
			public const string ALL = "2";
		}

		/*----------------------------------*/
		/* LIGHTチャネル接続種別			*/
		/*----------------------------------*/
		public const string LIGHT_CH_IVP = "1";			// IVP接続

		public const string LIGHT_CH_VCS = "2";			// VCS接続

        public class Browsers{
			public const string IPHONE_OLD = "iphonebrowser";
        }

		///<summary>
		/// メールサーバー 
		/// </summary>
		public class MailServer {
			public const string BEAM = "1";
			public const string MULLER3 = "2";
		}

		/*----------------------------------*/
		/* メールアドレス状態MASK			*/
		/*----------------------------------*/
		public const int MSK_MAIL_ADDR_OK = 0x001;					// 正常 
		public const int MSK_MAIL_ADDR_FILTERING_ERROR = 0x002;		// フィルタリングエラー 
		public const int MSK_MAIL_ADDR_NG = 0x004;					// 不通エラー

		/*----------------------------------*/
		/* メールアドレス状態				*/
		/*----------------------------------*/
		public const int MAIL_ADDR_OK = 0;					// 正常 
		public const int MAIL_ADDR_FILTERING_ERROR = 1;		// フィルタリングエラー 
		public const int MAIL_ADDR_NG = 2;					// 不通エラー
	}
}

