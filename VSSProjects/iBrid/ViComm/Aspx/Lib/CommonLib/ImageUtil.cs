using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Configuration;

using iBridCommLib;

namespace ViComm {
	public class ImageUtil {

		public static RotateFlipType Convert2RotateFlipType(string p) {

			switch (p) {
				case ViCommConst.ImageRotateType.ROTATE_RIGHT_90:
					return RotateFlipType.Rotate90FlipNone;
				case ViCommConst.ImageRotateType.ROTATE_LEFT_90:
					return RotateFlipType.Rotate270FlipNone;
				case ViCommConst.ImageRotateType.ROTATE_180:
					return RotateFlipType.Rotate180FlipNone;
				default:
					return RotateFlipType.RotateNoneFlipNone;

			}
		}

		public static void RotateFilp(string pCastPicDir,string pPicSeq,RotateFlipType pRotateFlipType) {
			if (pRotateFlipType.Equals(RotateFlipType.RotateNoneFlipNone))
				return;

			string sPicFileNm = iBridUtil.addZero(pPicSeq,ViCommConst.OBJECT_NM_LENGTH);

			using(localhost.Service oSvc = new ViComm.localhost.Service())
			using (Impersonator.Impersonate(ViCommConst.FILE_UPLOAD_USERNAME,ViCommConst.FILE_UPLOAD_PASSWORD)) {

				string sMailParseDir = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["MailParseDir"]);
				string sUrl = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["localhost.Service"]);
				if(!string.IsNullOrEmpty(sUrl)){
					oSvc.Url = sUrl;
				}

				
				string[] oFilePathArray = Directory.GetFiles(pCastPicDir,string.Format("{0}*.jpg",sPicFileNm));
				foreach (string sFilePath in oFilePathArray) {
					using (Image oImage = new Bitmap(sFilePath)) {
						oImage.RotateFlip(pRotateFlipType);
						oImage.Save(sFilePath,ImageFormat.Jpeg);
					}
					
					FileInfo oFile = new FileInfo(sFilePath);
					oSvc.SetCopyRight(sMailParseDir, oFile.Directory.FullName, oFile.Name, string.Empty);
				}
			}
		}
	}
}
