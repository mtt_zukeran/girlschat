﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

// 
//namespace ViComm {

	/// <summary>
	/// 男性、女性、ｷｬｽﾄなどの表示名に関する補助的な機能を提供するUtilityクラスです
	/// </summary>
	public class DisplayWordUtil {

		/// <summary>
		/// Validatorのエラーメッセージに含まれる男性、女性(キャスト、出演者)の表示名置き換えを行う
		/// </summary>
		/// <param name="pValidatorCollection">対象のValidatorCollection</param>
		public static void ReplaceValidatorErrorMessage(ValidatorCollection pValidatorCollection) {
			foreach (BaseValidator oValidator in pValidatorCollection) {
				oValidator.ErrorMessage = Replace(oValidator.ErrorMessage);
			}
		}
		/// <summary>
		/// GridViewの列ヘッダーに含まれる男性、女性(キャスト、出演者)の表示名置き換えを行う
		/// </summary>
		/// <param name="pGrid"></param>
		public static void ReplaceGridColumnHeader(GridView pGrid) {
			foreach(DataControlField oField in pGrid.Columns){
				oField.HeaderText = Replace(oField.HeaderText);	
			}
		}
		/// <summary>
		/// 男性、女性(キャスト、出演者)の表示名置き換えを行う
		/// </summary>
		/// <param name="pValue"></param>
		/// <returns></returns>
		public static string Replace(string pValue) {
			pValue = ReplaceMan(pValue);
			pValue = ReplaceWoman(pValue);
			return pValue;
		}
		
		/// <summary>
		/// アプリケーション構成ファイルに男性の表示名のエイリアスが設定されている場合、男性の表示名を置き換えます。
		/// </summary>
		/// <param name="pValue">対象の文字列</param>
		/// <returns>置き換え後の文字列</returns>
		public static string ReplaceMan(string pValue) {
			
			IList<string> oReplaceTargetWordList = new List<string>();
			oReplaceTargetWordList.Add("男性会員");
			oReplaceTargetWordList.Add("男性");

			return ReplaceImpl(pValue, oReplaceTargetWordList, "WordMan");
		}
		/// <summary>
		/// アプリケーション構成ファイルに女性(ｷｬｽﾄ)の表示名のエイリアスが設定されている場合、女性(ｷｬｽﾄ)の表示名を置き換えます。
		/// </summary>
		/// <param name="pValue">対象の文字列</param>
		/// <returns>置き換え後の文字列</returns>
		public static string ReplaceWoman(string pValue) {

			IList<string> oReplaceTargetWordList = new List<string>();
			oReplaceTargetWordList.Add("女性会員");
			oReplaceTargetWordList.Add("女性");
			oReplaceTargetWordList.Add("ｷｬｽﾄ");
			oReplaceTargetWordList.Add("キャスト");
			oReplaceTargetWordList.Add("出演者");
			oReplaceTargetWordList.Add("オペレーター");

			return ReplaceImpl(pValue, oReplaceTargetWordList, "WordCast");
		}
		public static string ReplaceOkList(string pValue){
			return ReplaceImpl(pValue, "WordOkList");
		}
		public static string ReplaceMessageDeleteButton(string pValue) {
			return ReplaceImpl(pValue, "WordMessageDeleteButton");
		}
		public static string ReplaceSetupRxMailCastMailLabel(string pValue) {
			return ReplaceImpl(pValue, "WordSetupRxMailCastMailLabel");
		}
		public static string ReplaceWordCommentList(string pValue) {
			return ReplaceImpl(pValue, "WordCommentList");
		}
		public static string ReplaceLogin(string pValue) {
			IList<string> oReplaceTargetWordList = new List<string>();
			oReplaceTargetWordList.Add("ログイン");
			oReplaceTargetWordList.Add("ﾛｸﾞｲﾝ");

			return ReplaceImpl(pValue,oReplaceTargetWordList,"WordLogin");
		}
		public static string ReplaceInfo(string pValue) {
			IList<string> oReplaceTargetWordList = new List<string>();
			oReplaceTargetWordList.Add("画像");
			oReplaceTargetWordList.Add("動画");

			return ReplaceImpl(pValue,oReplaceTargetWordList,"WordInfo");
		}
		public static string ReplaceWordFindUserCommentHeader(string pValue) {
			return ReplaceImpl(pValue,"WordFindUserCommentHeader");
		}
		public static string ReplaceWordFindUserCommentFooter(string pValue) {
			return ReplaceImpl(pValue,"WordFindUserCommentFooter").Replace("br","<br />");
		}


		private static string ReplaceImpl(string pValue, string pDisplayValueKey) {
			return ReplaceImpl(pValue,new List<string>(), pDisplayValueKey);
		}
		private static string ReplaceImpl(string pValue, IList<string> oReplaceTargetWordList, string pDisplayValueKey){

			string sDisplayValue = System.Configuration.ConfigurationManager.AppSettings[pDisplayValueKey];
		
			if (!string.IsNullOrEmpty(sDisplayValue)) {
				if( oReplaceTargetWordList.Count==0 ) {
					pValue = sDisplayValue;
				}else{
					foreach(string sReplaceTargetWord in oReplaceTargetWordList) {
						pValue = pValue.Replace(sReplaceTargetWord, sDisplayValue);
					}
				}
			}
			return pValue;
		}
			
	}
//}