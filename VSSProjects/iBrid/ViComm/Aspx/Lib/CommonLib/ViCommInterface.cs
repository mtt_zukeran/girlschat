using System;
using System.IO;
using System.Web;
using System.Net;
using System.Configuration;
using System.Text;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Threading;
using iBridCommLib;

namespace ViComm {

	public class ViCommThreadInterface {
		private string url;
		private string postData;
		private bool logging;
		private int sleepTime;

		public ViCommThreadInterface(string pUrl) {
			url = pUrl;
		}

		public ViCommThreadInterface(string pUrl,string pPostData,bool pLogging,int pSleepTime) {
			url = pUrl;
			postData = pPostData;
			logging = pLogging;
			sleepTime = pSleepTime;
		}

		public void TransToParent() {
			Thread thread = new Thread(ThreadTransToParent);
			thread.Start();
		}
		private void ThreadTransToParent() {
			ViCommInterface.PrvTransToParent(url);
		}

		public void PostToParent() {
			Thread thread = new Thread(ThreadPostToParent);
			thread.Start();
		}

		private void ThreadPostToParent() {
			Thread.Sleep(sleepTime);
			ViCommInterface.PrvPostToParent(url,postData,logging);
		}

	}

	public class ViCommInterface {

		public static int GetWebUserInfo(string pUrl,string pUserId,out int pBalPoint,out string pTel,out string pPassword,out string pCondition,out string pRetCd) {

			int iRet = ViCommConst.USER_INFO_NG;
			pBalPoint = -1;
			pTel = "";
			pPassword = "";
			pCondition = "";
			pRetCd = "";

			Encoding enc = Encoding.GetEncoding("Shift_JIS");
			string url = string.Format("{0}?request=11&userid={1}",pUrl,pUserId);

			try {
				WebRequest req = WebRequest.Create(url);
				req.Timeout = 20000;
				WebResponse res = req.GetResponse();

				Stream st = res.GetResponseStream();
				using (StreamReader sr = new StreamReader(st,enc)) {
					NameValueCollection objCol = HttpUtility.ParseQueryString(sr.ReadToEnd());
					sr.Close();
					st.Close();
					string sUserID;
					if (objCol["userid"] != null) {
						sUserID = objCol["userid"].ToString();
					} else {
						sUserID = "";
					}

					if (objCol["result"].ToString().Equals("0") && (!sUserID.Equals(""))) {
						pBalPoint = int.Parse(objCol["point"].ToString()) * 10;

						if (objCol["limit"] != null) {
							pBalPoint += int.Parse(objCol["limit"]) * 10;
						}

						pTel = iBridUtil.GetStringValue(objCol["phone"]);
						pPassword = iBridUtil.GetStringValue(objCol["userpasswd"]);
						pCondition = iBridUtil.GetStringValue(objCol["condition"]);
						pRetCd = iBridUtil.GetStringValue(objCol["retcd"]);
						iRet = ViCommConst.USER_INFO_OK;

						if (pTel.Equals("")) {
							iRet = ViCommConst.USER_INFO_UNAUTH_TEL;
						} else if (pBalPoint <= 0) {
							iRet = ViCommConst.USER_INFO_NO_BAL;
						}
					}
				}
			} catch (Exception e) {
				WriteIFError(e,"GetWebUserInfo",url);
			}
			return iRet;
		}

		public static bool TransUsedInfo(string pUrl,string pSiteCd,string pUserId,int pUsedPoint,string pChargeType) {

			bool bRet = false;

			Encoding enc = Encoding.GetEncoding("Shift_JIS");
			string url = string.Format("{0}?request=03&loc={1}&pgm={2}&userid={3}&vcsid={3}&timepoint={4}&usedpoint={5}&del=0&charge={6}",pUrl,"01",pSiteCd,pUserId,pUsedPoint,pUsedPoint,pChargeType);

			try {
				WebRequest req = WebRequest.Create(url);
				req.Timeout = 20000;
				WebResponse res = req.GetResponse();
				Stream st = res.GetResponseStream();
				using (StreamReader sr = new StreamReader(st,enc)) {
					NameValueCollection objCol = HttpUtility.ParseQueryString(sr.ReadToEnd());
					sr.Close();
					st.Close();
					if (objCol["result"].ToString().EndsWith("0")) {
						bRet = true;
					}
				}
			} catch (Exception e) {
				WriteIFError(e,"TransUsedInfo",url);
			}
			return bRet;
		}

		public static string TransIVPControl(string pUrl) {
			try {
				WebRequest req = WebRequest.Create(pUrl);
				req.Timeout = 20000;
				WebResponse res = req.GetResponse();

				Stream st = res.GetResponseStream();
				using (StreamReader sr = new StreamReader(st)) {
					NameValueCollection objCol = HttpUtility.ParseQueryString(sr.ReadToEnd());
					sr.Close();
					st.Close();
					return iBridUtil.GetStringValue(objCol["result"]);
				}
			} catch (Exception e) {
				WriteIFError(e,"TransIVPControl",pUrl);
				return "-1";
			}
		}

		public static string RegistSIPTerminal(string pUrl,ref string pSipId,ref string pSipPassword) {
			try {
				WebRequest req = WebRequest.Create(pUrl);
				req.Timeout = 20000;
				WebResponse res = req.GetResponse();

				Stream st = res.GetResponseStream();
				using (StreamReader sr = new StreamReader(st)) {
					NameValueCollection objCol = HttpUtility.ParseQueryString(sr.ReadToEnd());
					sr.Close();
					st.Close();
					pSipId = iBridUtil.GetStringValue(objCol["sipid"]);
					pSipPassword = iBridUtil.GetStringValue(objCol["sippw"]);
					return iBridUtil.GetStringValue(objCol["result"]);
				}
			} catch (Exception e) {
				WriteIFError(e,"RegistSIPTerminal",pUrl);
				return "-1";
			}
		}

		public static string ShareLiveTalkKey(string pUrl,out string pKey) {
			try {
				WebRequest req = WebRequest.Create(pUrl);
				req.Timeout = 20000;
				WebResponse res = req.GetResponse();

				Stream st = res.GetResponseStream();
				using (StreamReader sr = new StreamReader(st)) {
					NameValueCollection objCol = HttpUtility.ParseQueryString(sr.ReadToEnd());
					sr.Close();
					st.Close();
					pKey = iBridUtil.GetStringValue(objCol["key"]);
					return iBridUtil.GetStringValue(objCol["result"]);
				}
			} catch (Exception e) {
				WriteIFError(e,"ShareLiveTalkKey",pUrl);
				pKey = "";
				return "-1";
			}
		}


		public static bool TransIVPUserRequest(string pUrl,ref string pAccectSeq,ref string pDialNo,ref string pVidedoPrefix,ref string pAudioPrefix,ref string pResult) {
			bool bRet;

			try {
				WebRequest req = WebRequest.Create(pUrl);
				req.Timeout = 20000;
				WebResponse res = req.GetResponse();
				Stream st = res.GetResponseStream();

				using (StreamReader sr = new StreamReader(st)) {
					NameValueCollection objCol = HttpUtility.ParseQueryString(sr.ReadToEnd());
					sr.Close();
					st.Close();
					pResult = objCol["result"].ToString();
					if (pResult.EndsWith("0")) {
						bRet = true;
						pAccectSeq = iBridUtil.GetStringValue(objCol["accept"]);
						pDialNo = iBridUtil.GetStringValue(objCol["dial"]);
						pVidedoPrefix = iBridUtil.GetStringValue(objCol["video"]);
						pAudioPrefix = iBridUtil.GetStringValue(objCol["audio"]);
					} else {
						bRet = false;
					}
				}
				return bRet;
			} catch (Exception e) {
				WriteIFError(e,"TransIVPUserRequest",pUrl);
				return false;
			}
		}

		public static string TransToParent(string pUrl,bool pThreading) {
			try {
				if (pThreading == false) {
					return PrvTransToParent(pUrl);
				} else {
					ViCommThreadInterface threadClass = new ViCommThreadInterface(pUrl);
					threadClass.TransToParent();
					return "0";
				}
			} catch (Exception e) {
				WriteIFError(e,"TransToParent",pUrl);
				return "-1";
			}
		}

		public static string PostToParent(string pUrl,string pPostData,bool pThreading,bool bLogging,int pSleep) {
			try {
				if (pThreading == false) {
					return PrvPostToParent(pUrl,pPostData,bLogging);
				} else {
					ViCommThreadInterface threadClass = new ViCommThreadInterface(pUrl,pPostData,bLogging,pSleep);
					threadClass.PostToParent();
					return "0";
				}
			} catch (Exception e) {
				WriteIFError(e,"PostToParent",pUrl);
				return "-1";
			}
		}

		public static string PrvTransToParent(string pUrl) {
			try {
				WriteIFLog("TransToParent",pUrl);
				WebRequest req = WebRequest.Create(pUrl);
				req.Timeout = 30000;
				WebResponse res = req.GetResponse();

				Stream st = res.GetResponseStream();
				using (StreamReader sr = new StreamReader(st)) {
					string sResponse = sr.ReadToEnd();
					NameValueCollection objCol = HttpUtility.ParseQueryString(sResponse);
					sr.Close();
					st.Close();
					WriteIFLog("TransToParent",pUrl + " Response=" + sResponse);
					return iBridUtil.GetStringValue(objCol["result"]);
				}
			} catch (Exception e) {
				WriteIFError(e,"TransToParent",pUrl);
				return "-1";
			}
		}

		public static string PrvPostToParent(string pUrl,string pPostData,bool pLogging) {
			try {
				if (pLogging) {
					WriteIFLog("PostToParent",pUrl + pPostData);
				}
				Encoding enc = Encoding.GetEncoding("Shift_JIS");

				if (pUrl.Contains(iBridUtil.GetStringValue(ConfigurationManager.AppSettings["MailAppBatchPath"]))) {
					enc = Encoding.GetEncoding("UTF-8");
				}

				byte[] postDataBytes = enc.GetBytes(pPostData);

				WebClient wc = new WebClient();
				wc.Headers.Add("Content-Type","application/x-www-form-urlencoded");
				byte[] resData = wc.UploadData(pUrl,postDataBytes);
				wc.Dispose();
				string resText = enc.GetString(resData);
				if (pLogging) {
					WriteIFLog("PostToParent"," Response=" + resText);
				}
				return resText;
			} catch (Exception e) {
				WriteIFError(e,"PostToParent",pUrl);
				return "-1";
			}
		}

		public static void WriteIFError(Exception e,string pFunctionNm,string pQuery) {
			string sRoot = ConfigurationManager.AppSettings["Root"].ToString().ToLower();
			string sSource = "ViCOMM";
			string sLog = "Application";

			string err = "ViComm Interface Error Caught in Application_Error event\n" +
					  "\nFuntion " + pFunctionNm +
					  "\nQuery " + pQuery +
					  "\nError Message:" + e.Message.ToString() +
					  "\nStack Trace:" + e.StackTrace.ToString();

			if (!EventLog.SourceExists(sSource)) {
				EventLog.CreateEventSource(sSource,sLog);
			}

			EventLog.WriteEntry(sSource,err,EventLogEntryType.Error);
		}

		public static void WriteIFLog(string pFunctionNm,string pLog) {
			string sRoot = ConfigurationManager.AppSettings["Root"].ToString().ToLower();
			string sSource = "ViCOMM";
			string sLog = "Application";

			string err = "ViComm Interface log\n" +
					  "\nFuntion " + pFunctionNm +
					  "\nLog " + pLog;

			if (!EventLog.SourceExists(sSource)) {
				EventLog.CreateEventSource(sSource,sLog);
			}

			EventLog.WriteEntry(sSource,err,EventLogEntryType.Information);
		}
	}
}
