﻿using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;
using System;

namespace ViComm {

	public class ViCommDual:DbSession {


		public ViCommDual() {
		}

		public string GetSidSeq() {
			return GetSeq("SELECT SEQ_SETTLE_SID.NEXTVAL SEQ FROM DUAL");
		}

		private string GetSeq(string pSql) {
			DataSet ds;
			DataRow dr;
			int iSeq = 0;
			conn = DbConnect(pSql);
			using (cmd = CreateSelectCommand(pSql,conn))
			using (ds = new DataSet()) {
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"DUAL");
					if (ds.Tables["DUAL"].Rows.Count != 0) {
						dr = ds.Tables["DUAL"].Rows[0];
						iSeq = int.Parse(dr["SEQ"].ToString());
					}
				}
			}
			conn.Close();

			return DateTime.Now.ToString("yyMMdd") + string.Format("{0:D5}",iSeq);
		}
	}
}
