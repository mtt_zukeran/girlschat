/*************************************************************************/
-- ﾋﾟｯｸｱｯﾌﾟ           テーブル定義
/*************************************************************************/
CREATE TABLE T_PICKUP (
    SITE_CD             VARCHAR2(4)         NOT NULL,
    PICKUP_ID           VARCHAR2(2)         NOT NULL,
    PICKUP_TITLE        VARCHAR2(128)       NULL,
    PICKUP_HEADER_DOC   VARCHAR2(4000)       NULL,
    PICKUP_COLOR        VARCHAR2(7)         NULL,
    PICKUP_MASK         NUMBER(15,0)        DEFAULT 0 NOT NULL,
    REVISION_NO         NUMBER(15,0)        DEFAULT 0 NOT NULL,
    UPDATE_DATE         DATE                NULL,
    CONSTRAINT pk_T_PICKUP          PRIMARY KEY (SITE_CD,
                                                 PICKUP_ID)
)
;

CREATE INDEX ix_RL00208 ON T_PICKUP (
    SITE_CD                       ASC
)
;

/*************************************************************************/
-- ﾋﾟｯｸｱｯﾌﾟｷｬﾗｸﾀｰ           テーブル定義
/*************************************************************************/
CREATE TABLE T_PICKUP_CHARACTER (
    SITE_CD             VARCHAR2(4)         NOT NULL,
    USER_SEQ            NUMBER(15,0)        NOT NULL,
    USER_CHAR_NO        VARCHAR2(2)         NOT NULL,
    PICKUP_ID           VARCHAR2(2)         NOT NULL,
    COMMENT_PICKUP      VARCHAR2(4000)      NULL,
	PICKUP_FLAG				NUMBER(1)			DEFAULT 0 NOT NULL,
    REVISION_NO         NUMBER(15,0)        DEFAULT 0 NOT NULL,
    UPDATE_DATE         DATE                NULL,
    CONSTRAINT pk_T_PICKUP_CHARACTER  PRIMARY KEY (SITE_CD,
                                                   USER_SEQ,
                                                   USER_CHAR_NO,
                                                   PICKUP_ID)
)
;

CREATE INDEX ix_RL00209 ON T_PICKUP_CHARACTER (
    SITE_CD                       ASC,
    PICKUP_ID                     ASC
)
;

CREATE INDEX ix_RL00210 ON T_PICKUP_CHARACTER (
    SITE_CD                       ASC,
    USER_SEQ                      ASC,
    USER_CHAR_NO                  ASC
)
;


ALTER TABLE T_PICKUP ADD (
    CONSTRAINT f_RL00208 FOREIGN KEY (SITE_CD)
       REFERENCES T_SITE
)
;

ALTER TABLE T_PICKUP_CHARACTER ADD (
    CONSTRAINT f_RL00209 FOREIGN KEY (SITE_CD,
                                      PICKUP_ID)
       REFERENCES T_PICKUP ON DELETE CASCADE
)
;

ALTER TABLE T_PICKUP_CHARACTER ADD (
    CONSTRAINT f_RL00210 FOREIGN KEY (SITE_CD,
                                      USER_SEQ,
                                      USER_CHAR_NO)
       REFERENCES T_CAST_CHARACTER ON DELETE CASCADE
)
;


ALTER TABLE T_CAST_CHARACTER ADD(PICKUP_MASK NUMBER(15) DEFAULT 0 NOT NULL);

@ K:VSSProjects\iBrid\ViComm\Oracle\9_RECORD\COMMON\T_PICKUP.SQL


DECLARE
	CURSOR CS_CHARACTER IS SELECT SITE_CD,USER_SEQ,USER_CHAR_NO,COMMENT_PICKUP,PICKUP_FLAG
		FROM
			T_CAST_CHARACTER
		FOR UPDATE;
BEGIN
	FOR REC_CHARACTER IN CS_CHARACTER LOOP
		IF (REC_CHARACTER.COMMENT_PICKUP IS NOT NULL) THEN
			INSERT INTO T_PICKUP_CHARACTER(
				SITE_CD        ,
				USER_SEQ       ,
				USER_CHAR_NO   ,
				PICKUP_ID      ,
				COMMENT_PICKUP ,
				PICKUP_FLAG        
			)VALUES(
				REC_CHARACTER.SITE_CD		,
				REC_CHARACTER.USER_SEQ		,
				REC_CHARACTER.USER_CHAR_NO	,
				'01'						,
				REC_CHARACTER.COMMENT_PICKUP,
				REC_CHARACTER.PICKUP_FLAG
			);

			IF REC_CHARACTER.PICKUP_FLAG != 0 THEN
				UPDATE T_CAST_CHARACTER SET PICKUP_MASK = 1
					WHERE
						CURRENT OF CS_CHARACTER;
			END IF;
		END IF;
	END LOOP;

	COMMIT;

END LOOP;
/
