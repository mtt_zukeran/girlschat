/*************************************************************************/
-- 広告ｺｰﾄﾞ別登録日集計           テーブル定義
/*************************************************************************/
CREATE TABLE T_REGIST_AD (
    SITE_CD             VARCHAR2(4)         NOT NULL,
    AD_CD               VARCHAR2(32)        NOT NULL,
    REGIST_DAY          VARCHAR2(10)        NOT NULL,
    ACCESS_COUNT        NUMBER(7,0)         DEFAULT 0 NOT NULL,
    REGIST_COUNT        NUMBER(7,0)         DEFAULT 0 NOT NULL,
    CONSTRAINT pk_T_REGIST_AD       PRIMARY KEY (SITE_CD,
                                                 AD_CD,
                                                 REGIST_DAY)
)
;

CREATE INDEX ix_RL00154 ON T_REGIST_AD (
    SITE_CD                       ASC
)
;

CREATE INDEX ix_RL00155 ON T_REGIST_AD (
    AD_CD                         ASC
)
;

/*************************************************************************/
-- 広告ｺｰﾄﾞ別ｱｸｾｽ売上集計           テーブル定義
/*************************************************************************/
CREATE TABLE T_DAILY_AD_ACCESS_SALES (
    SITE_CD             VARCHAR2(4)         NOT NULL,
    AD_CD               VARCHAR2(32)        NOT NULL,
    REGIST_DAY          VARCHAR2(10)        NOT NULL,
    REPORT_DAY          VARCHAR2(10)        NOT NULL,
    LOGIN_COUNT         NUMBER(7,0)         DEFAULT 0 NOT NULL,
    TOTAL_USED_POINT    NUMBER(7,0)         DEFAULT 0 NOT NULL,
    CASH_SALES_COUNT    NUMBER(5,0)         DEFAULT 0 NOT NULL,
    CASH_SALES_AMT      NUMBER(8,0)         DEFAULT 0 NOT NULL,
    CREDIT_PACK_SALES_COUNT NUMBER(5,0)         DEFAULT 0 NOT NULL,
    CREDIT_PACK_SALES_AMT NUMBER(8,0)         DEFAULT 0 NOT NULL,
    CCHECK_SALES_COUNT  NUMBER(5,0)         DEFAULT 0 NOT NULL,
    CCHECK_SALES_AMT    NUMBER(8,0)         DEFAULT 0 NOT NULL,
    SMONEY_SALES_COUNT  NUMBER(5,0)         DEFAULT 0 NOT NULL,
    SMONEY_SALES_AMT    NUMBER(8,0)         DEFAULT 0 NOT NULL,
    BITCASH_SALES_COUNT NUMBER(5,0)         DEFAULT 0 NOT NULL,
    BITCASH_SALES_AMT   NUMBER(8,0)         DEFAULT 0 NOT NULL,
    CONVINI_SALES_COUNT NUMBER(5,0)         DEFAULT 0 NOT NULL,
    CONVINI_SALES_AMT   NUMBER(8,0)         DEFAULT 0 NOT NULL,
    ACHECK_SALES_COUNT  NUMBER(5,0)         DEFAULT 0 NOT NULL,
    ACHECK_SALES_AMT    NUMBER(8,0)         DEFAULT 0 NOT NULL,
    SSAD_SALES_COUNT    NUMBER(5,0)         DEFAULT 0 NOT NULL,
    SSAD_SALES_AMT      NUMBER(8,0)         DEFAULT 0 NOT NULL,
    ADULTID_SALES_COUNT NUMBER(5,0)         DEFAULT 0 NOT NULL,
    ADULTID_SALES_AMT   NUMBER(8,0)         DEFAULT 0 NOT NULL,
    OC_SALES_COUNT      NUMBER(5,0)         DEFAULT 0 NOT NULL,
    OC_SALES_AMT        NUMBER(8,0)         DEFAULT 0 NOT NULL,
    CONSTRAINT pk_T_DAILY_AD_ACCESS_SALES  PRIMARY KEY (SITE_CD,
                                                        AD_CD,
                                                        REGIST_DAY,
                                                        REPORT_DAY)
)
;

CREATE INDEX ix_RL00156 ON T_DAILY_AD_ACCESS_SALES (
    SITE_CD                       ASC,
    AD_CD                         ASC,
    REGIST_DAY                    ASC
)
;

ALTER TABLE T_DAILY_AD_ACCESS_SALES ADD (
    CONSTRAINT f_RL00156 FOREIGN KEY (SITE_CD,
                                      AD_CD,
                                      REGIST_DAY)
       REFERENCES T_REGIST_AD
)
;

ALTER TABLE T_REGIST_AD ADD (
    CONSTRAINT f_RL00154 FOREIGN KEY (SITE_CD)
       REFERENCES T_SITE
)
;

ALTER TABLE T_REGIST_AD ADD (
    CONSTRAINT f_RL00155 FOREIGN KEY (AD_CD)
       REFERENCES T_AD
)
;

UPDATE T_SITE SET PRIORITY = 3 WHERE SITE_CD = 'Z001';

UPDATE T_SITE SET PRIORITY = 2 WHERE SITE_CD = 'A002';


/*************************************************************************/
-- 管理者連絡           
/*************************************************************************/

--HTML_DOC1~5追加。
ALTER TABLE T_ADMIN_REPORT ADD (
    HTML_DOC1           VARCHAR2(3000)      NULL,
    HTML_DOC2           VARCHAR2(3000)      NULL,
    HTML_DOC3           VARCHAR2(3000)      NULL,
    HTML_DOC4           VARCHAR2(3000)      NULL,
    HTML_DOC5           VARCHAR2(3000)      NULL
)
;

--既存データをHTML_DOC1,2にコピー
UPDATE T_ADMIN_REPORT SET HTML_DOC1 = SUBSTRB(HTML_DOC,1,3000), HTML_DOC2 = SUBSTRB(HTML_DOC,3001,3000);

--もう使用しないHTML_DOCを削除
ALTER TABLE T_ADMIN_REPORT DROP COLUMN HTML_DOC;

commit;