SELECT T1.*,
	GET_MAN_PHOTO_IMG_PATH		(T1.SITE_CD,T1.LOGIN_ID,T1.PIC_SEQ,0)				AS PHOTO_IMG_PATH				,
	GET_MAN_SMALL_PHOTO_IMG_PATH(T1.SITE_CD,T1.LOGIN_ID,T1.PIC_SEQ,0)				AS SMALL_PHOTO_IMG_PATH			,
	SUBSTRB(T1.MOBILE_TERMINAL_NM,1,40)												AS MOBILE_TERMINAL_SHORT_NM		,
-- Join Field
{3}
--
	T_RECORDING.RECORDING_DATE														AS MAN_PF_RECORDING_DATE		,
	T_RECORDING.FOWARD_TERM_SCH_DATE												AS MAN_PF_FOWARD_TERM_SCH_DATE	,
	T_RECORDING.WAITING_TYPE														AS MAN_PF_WAITING_TYPE			,
--
	T_CODE_DTL1.CODE_NM																AS MOBILE_CARRIER_NM			,
	T_CODE_DTL2.CODE_NM																AS USER_STATUS_NM				,
	T_CODE_DTL3.CODE_NM																AS CAST_MAIL_RX_TYPE_NM			,
	T_CODE_DTL4.CODE_NM																AS INFO_MAIL_RX_TYPE_NM			,
	T_CODE_DTL5.CODE_NM																AS ONLINE_STATUS_NM				,
--
	NVL(T_COMMUNICATION.TOTAL_TALK_COUNT,0)											AS PARTNER_TOTAL_TALK_COUNT		,
	NVL(T_COMMUNICATION.TOTAL_RX_MAIL_COUNT,0)										AS PARTNER_TOTAL_RX_MAIL_COUNT	,
	NVL(T_COMMUNICATION.TOTAL_TX_MAIL_COUNT,0)										AS PARTNER_TOTAL_TX_MAIL_COUNT	,
	T_COMMUNICATION.LAST_RX_MAIL_DATE												AS PARTNER_LAST_RX_MAIL_DATE	,
--
	T_MEMO.REVIEWS																	AS MEMO_REVIEWS					,
	T_MEMO.MEMO_DOC																	AS MEMO_DOC						,
	T_MEMO.UPDATE_DATE																AS MEMO_UPDATE_DATE				,
--
	CASE
		WHEN (T1.TERMINAL_UNIQUE_ID IS NOT NULL) THEN
			'1'
		ELSE
			'0'
	END																				AS HAVE_TERM_UNIQUE_ID_FLAG	,
	CASE
		WHEN T1.CHARACTER_ONLINE_STATUS = 0 THEN
			T_SITE_MANAGEMENT.OFFLINE_GUIDANCE
		WHEN T1.CHARACTER_ONLINE_STATUS = 1 THEN
			T_SITE_MANAGEMENT.LOGINED_GUIDANCE
		WHEN T1.CHARACTER_ONLINE_STATUS = 2 THEN
			T_SITE_MANAGEMENT.WAITING_GUIDANCE
		WHEN (T1.CHARACTER_ONLINE_STATUS = 3) THEN
			T_SITE_MANAGEMENT.TALKING_WSHOT_GUIDANCE
	END																				AS MOBILE_ONLINE_STATUS_NM	,
	CASE
--
		WHEN (T_RECORDING.WAITING_TYPE = 1) THEN
			T_SITE_MANAGEMENT.CONNECT_TYPE_VIDEO_GUIDANCE
		WHEN (T_RECORDING.WAITING_TYPE = 2) THEN
			T_SITE_MANAGEMENT.CONNECT_TYPE_VOICE_GUIDANCE
		WHEN (T_RECORDING.WAITING_TYPE = 3) THEN
			T_SITE_MANAGEMENT.CONNECT_TYPE_BOTH_GUIDANCE
	END																				AS MAN_PF_WAITING_TYPE_NM	,
--
	CASE
		WHEN (T1.REGIST_DATE >= TO_CHAR(SYSDATE - T_SITE_MANAGEMENT.MAN_NEW_FACE_DAYS,'YYYY/MM/DD')) THEN
		1
	ELSE
		0
	END																				AS NEW_MAN_FLAG				,
--
	(SELECT NVL(COUNT(USER_SEQ),0) AS CNT FROM T_FAVORIT
		WHERE
			SITE_CD					= T1.SITE_CD			AND
			USER_SEQ				= T1.USER_SEQ			AND
			USER_CHAR_NO			= T1.USER_CHAR_NO		AND	
			PARTNER_USER_SEQ		= :PARTNER_USER_SEQ1	AND
			PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO1 ) 						AS LIKE_ME_FLAG,
--
	(SELECT NVL(COUNT(*),0) AS CNT FROM T_FAVORIT
		WHERE
			SITE_CD					= T1.SITE_CD				AND
			USER_SEQ				= :PARTNER_USER_SEQ2		AND
			USER_CHAR_NO			= :PARTNER_USER_CHAR_NO2	AND	
			PARTNER_USER_SEQ		= T1.USER_SEQ				AND
			PARTNER_USER_CHAR_NO	= T1.USER_CHAR_NO ) 							AS FAVORIT_FLAG ,
--
	(SELECT NVL(COUNT(*),0) AS CNT FROM T_REFUSE
		WHERE
			SITE_CD					= T1.SITE_CD				AND
			USER_SEQ				= :PARTNER_USER_SEQ3		AND
			USER_CHAR_NO			= :PARTNER_USER_CHAR_NO3	AND
			PARTNER_USER_SEQ		= T1.USER_SEQ				AND
			PARTNER_USER_CHAR_NO	= T1.USER_CHAR_NO	)							AS REFUSE_FLAG
FROM
(
-- Inner Sql
{0}
) 	T1,
	T_COMMUNICATION			,
	T_SITE_MANAGEMENT		,
	T_RECORDING				,
	T_MEMO					,
-- Jobin Table
{1}
	T_CODE_DTL	T_CODE_DTL1	,
	T_CODE_DTL	T_CODE_DTL2	,
	T_CODE_DTL	T_CODE_DTL3	,
	T_CODE_DTL	T_CODE_DTL4	,
	T_CODE_DTL	T_CODE_DTL5
WHERE
--
	T1.SITE_CD				= T_SITE_MANAGEMENT.SITE_CD					AND
--
	T1.SITE_CD				= T_COMMUNICATION.SITE_CD				(+)	AND
	T1.USER_SEQ				= T_COMMUNICATION.USER_SEQ				(+)	AND
	T1.USER_CHAR_NO			= T_COMMUNICATION.USER_CHAR_NO			(+) AND
	:PARTNER_USER_SEQ4		= T_COMMUNICATION.PARTNER_USER_SEQ		(+)	AND
	:PARTNER_USER_CHAR_NO4	= T_COMMUNICATION.PARTNER_USER_CHAR_NO	(+) AND
--
	T1.SITE_CD				= T_MEMO.SITE_CD						(+)	AND
	:PARTNER_USER_SEQ5		= T_MEMO.USER_SEQ						(+)	AND
	:PARTNER_USER_CHAR_NO5	= T_MEMO.USER_CHAR_NO					(+) AND
	T1.USER_SEQ				= T_MEMO.PARTNER_USER_SEQ				(+)	AND
	T1.USER_CHAR_NO			= T_MEMO.PARTNER_USER_CHAR_NO			(+) AND
--
	T1.REC_SEQ				= T_RECORDING.REC_SEQ					(+) AND
-- Jobin Condition
{2}
	:CODE_TYPE1					= T_CODE_DTL1.CODE_TYPE		(+)	AND
	T1.MOBILE_CARRIER_CD		= T_CODE_DTL1.CODE			(+)	AND
	:CODE_TYPE2					= T_CODE_DTL2.CODE_TYPE		(+)	AND
	T1.USER_STATUS				= T_CODE_DTL2.CODE			(+)	AND
	:CODE_TYPE3					= T_CODE_DTL5.CODE_TYPE		(+)	AND
	T1.CHARACTER_ONLINE_STATUS	= T_CODE_DTL5.CODE			(+) AND
	:CODE_TYPE4					= T_CODE_DTL3.CODE_TYPE		(+)	AND
	T1.CAST_MAIL_RX_TYPE		= T_CODE_DTL3.CODE			(+)	AND
	:CODE_TYPE5					= T_CODE_DTL4.CODE_TYPE		(+)	AND
	T1.INFO_MAIL_RX_TYPE		= T_CODE_DTL4.CODE			(+)
ORDER BY T1.RNUM
