
CREATE INDEX ix_POSSESSION_MAN_TREASURE01 ON T_POSSESSION_MAN_TREASURE (
    SITE_CD                       ASC,
    USER_SEQ                      ASC,
    USER_CHAR_NO                  ASC,
    CAST_GAME_PIC_SEQ             ASC
)
;

CREATE INDEX ix_POSSESSION_CAST_TREASURE01 ON T_POSSESSION_CAST_TREASURE (
    SITE_CD                       ASC,
    USER_SEQ                      ASC,
    USER_CHAR_NO                  ASC,
    CAST_TREASURE_SEQ             ASC
)
;


DROP TABLE T_GET_MAN_TREASURE_LOG;

CREATE TABLE T_GET_MAN_TREASURE_LOG (
    SITE_CD             VARCHAR2(4)         NOT NULL,
    USER_SEQ            NUMBER(15,0)        NOT NULL,
    USER_CHAR_NO        VARCHAR2(2)         NOT NULL,
    GET_TREASURE_LOG_SEQ NUMBER(15,0)        NOT NULL,
    CAST_USER_SEQ       NUMBER(15,0)        NULL,
    CAST_CHAR_NO        VARCHAR2(2)         NULL,
    CAST_GAME_PIC_SEQ   NUMBER(15,0)        NULL,
    GET_TREASURE_COMPLETE_FLAG NUMBER(1,0)         DEFAULT 0 NOT NULL,
    UPDATE_DATE         DATE                NULL,
    CONSTRAINT pk_T_GET_MAN_TREASURE_LOG  PRIMARY KEY (SITE_CD,
                                                       USER_SEQ,
                                                       USER_CHAR_NO,
                                                       GET_TREASURE_LOG_SEQ)
)
;

CREATE INDEX ix_RL00445 ON T_GET_MAN_TREASURE_LOG (
    SITE_CD                       ASC,
    CAST_USER_SEQ                 ASC,
    CAST_CHAR_NO                  ASC,
    CAST_GAME_PIC_SEQ             ASC
)
;

CREATE INDEX ix_RL00446 ON T_GET_MAN_TREASURE_LOG (
    SITE_CD                       ASC,
    USER_SEQ                      ASC,
    USER_CHAR_NO                  ASC
)
;


ALTER TABLE T_GET_MAN_TREASURE_LOG ADD (
    CONSTRAINT f_RL00445 FOREIGN KEY (SITE_CD,
                                      CAST_USER_SEQ,
                                      CAST_CHAR_NO,
                                      CAST_GAME_PIC_SEQ)
       REFERENCES T_MAN_TREASURE
)
;

ALTER TABLE T_GET_MAN_TREASURE_LOG ADD (
    CONSTRAINT f_RL00446 FOREIGN KEY (SITE_CD,
                                      USER_SEQ,
                                      USER_CHAR_NO)
       REFERENCES T_GAME_CHARACTER ON DELETE CASCADE
)
;

ALTER TABLE T_DATE_PLAN RENAME COLUMN WAITING_HOURS TO WAITING_MIN;
ALTER TABLE T_PART_TIME_JOB_PLAN RENAME COLUMN WAITING_HOURS TO WAITING_MIN;



/*************************************************************************/
-- 男性用お宝規定回数ｺﾝﾌﾟﾘｰﾄ時獲得ｱｲﾃﾑ           テーブル定義
/*************************************************************************/
CREATE TABLE T_MAN_COMP_COUNT_GET_ITEM (
    SITE_CD             VARCHAR2(4)         NOT NULL,
    DISPLAY_MONTH       VARCHAR2(7)         NOT NULL,
    COMP_COUNT_GET_ITEM_SEQ NUMBER(15,0)        NOT NULL,
    COMPLETE_COUNT      NUMBER(2,0)         DEFAULT 0 NOT NULL,
    GAME_ITEM_SEQ       NUMBER(15,0)        NULL,
    ITEM_COUNT          NUMBER(10,0)        DEFAULT 0 NOT NULL,
    CONSTRAINT pk_T_MAN_COMP_COUNT_GET_ITEM  PRIMARY KEY (SITE_CD,
                                                          DISPLAY_MONTH,
                                                          COMP_COUNT_GET_ITEM_SEQ)
)
;

CREATE INDEX ix_RL00424 ON T_MAN_COMP_COUNT_GET_ITEM (
    SITE_CD                       ASC,
    GAME_ITEM_SEQ                 ASC
)
;

ALTER TABLE T_MAN_COMP_COUNT_GET_ITEM ADD (
    CONSTRAINT f_RL00424 FOREIGN KEY (SITE_CD,
                                      GAME_ITEM_SEQ)
       REFERENCES T_GAME_ITEM
)
;

/*************************************************************************/
-- 男性用お宝規定回数ｺﾝﾌﾟﾘｰﾄ時獲得ｱｲﾃﾑ履歴           テーブル定義
/*************************************************************************/
CREATE TABLE T_MAN_COMP_COUNT_GET_ITEM_LOG (
    SITE_CD             VARCHAR2(4)         NOT NULL,
    USER_SEQ            NUMBER(15,0)        NOT NULL,
    USER_CHAR_NO        VARCHAR2(2)         NOT NULL,
    DISPLAY_MONTH       VARCHAR2(7)         NOT NULL,
    GET_DATE            DATE                NULL,
    COMP_COUNT_GET_ITEM_SEQ NUMBER(15,0)        NOT NULL,
    CONSTRAINT pk_T_MAN_COMP_COUNT_GET_ITEM_L  PRIMARY KEY (SITE_CD,
                                                            USER_SEQ,
                                                            USER_CHAR_NO,
                                                            DISPLAY_MONTH,
                                                            COMP_COUNT_GET_ITEM_SEQ)
)
;

CREATE INDEX ix_RL00449 ON T_MAN_COMP_COUNT_GET_ITEM_LOG (
    SITE_CD                       ASC,
    DISPLAY_MONTH                 ASC,
    COMP_COUNT_GET_ITEM_SEQ       ASC
)
;

CREATE INDEX ix_RL00450 ON T_MAN_COMP_COUNT_GET_ITEM_LOG (
    SITE_CD                       ASC,
    USER_SEQ                      ASC,
    USER_CHAR_NO                  ASC
)
;

ALTER TABLE T_MAN_COMP_COUNT_GET_ITEM_LOG ADD (
    CONSTRAINT f_RL00449 FOREIGN KEY (SITE_CD,
                                      DISPLAY_MONTH,
                                      COMP_COUNT_GET_ITEM_SEQ)
       REFERENCES T_MAN_COMP_COUNT_GET_ITEM
)
;

ALTER TABLE T_MAN_COMP_COUNT_GET_ITEM_LOG ADD (
    CONSTRAINT f_RL00450 FOREIGN KEY (SITE_CD,
                                      USER_SEQ,
                                      USER_CHAR_NO)
       REFERENCES T_GAME_CHARACTER
)
;

CREATE SEQUENCE SEQ_CAST_TREASURE
	START WITH 1000
	INCREMENT BY 1
	MINVALUE 1000
	MAXVALUE 999999999999999
	NOCACHE
	ORDER
	CYCLE
;

CREATE SEQUENCE SEQ_SALE_SCHEDULE
	START WITH 1000
	INCREMENT BY 1
	MINVALUE 1000
	MAXVALUE 999999999999999
	NOCACHE
	ORDER
	CYCLE
;


CREATE INDEX ix_COMP_MAN_TREASURE_ITEM01 ON T_COMP_MAN_TREASURE_GET_ITEM (
    SITE_CD                       ASC,
    DISPLAY_DAY                   ASC
)
;


ALTER TABLE T_GAME_LOGIN_BONUS_MONTHLY MODIFY  GAME_LOGIN_BONUS_MONTH VARCHAR2(7);


ALTER TABLE T_GAME_COMMUNICATION ADD(
    PARTY_ATTACK_WIN_COUNT NUMBER(8,0)         DEFAULT 0 NOT NULL,
    PARTY_ATTACK_LOSS_COUNT NUMBER(8,0)         DEFAULT 0 NOT NULL,
    PARTY_LAST_ATTACK_DATE DATE                NULL,
    PARTY_DEFENCE_WIN_COUNT NUMBER(8,0)         DEFAULT 0 NOT NULL,
    PARTY_DEFENCE_LOSS_COUNT NUMBER(8,0)         DEFAULT 0 NOT NULL,
    PARTY_LAST_DEFENCE_DATE DATE                NULL
);



/*************************************************************************/
-- ﾊﾟｰﾃｨﾊﾞﾄﾙﾒﾝﾊﾞ           テーブル定義
/*************************************************************************/
CREATE TABLE T_PARTY_BATTLE_MEMBER (
    SITE_CD             VARCHAR2(4)         NOT NULL,
    USER_SEQ            NUMBER(15,0)        NOT NULL,
    USER_CHAR_NO        VARCHAR2(2)         NOT NULL,
    PARTNER_GAME_CHARACTER_TYPE VARCHAR2(1)         NOT NULL,
    PARTNER_USER_SEQ    NUMBER(15,0)        NULL,
    PARTNER_USER_CHAR_NO VARCHAR2(2)         NULL,
    CONSTRAINT pk_T_PARTY_BATTLE_MEMBER  PRIMARY KEY (SITE_CD,
                                                      USER_SEQ,
                                                      USER_CHAR_NO,
                                                      PARTNER_GAME_CHARACTER_TYPE)
)
;

CREATE INDEX ix_RL00451 ON T_PARTY_BATTLE_MEMBER (
    SITE_CD                       ASC,
    USER_SEQ                      ASC,
    USER_CHAR_NO                  ASC
)
;

CREATE INDEX ix_RL00452 ON T_PARTY_BATTLE_MEMBER (
    SITE_CD                       ASC,
    PARTNER_USER_SEQ              ASC,
    PARTNER_USER_CHAR_NO          ASC
)
;

ALTER TABLE T_PARTY_BATTLE_MEMBER ADD (
    CONSTRAINT f_RL00451 FOREIGN KEY (SITE_CD,
                                      USER_SEQ,
                                      USER_CHAR_NO)
       REFERENCES T_GAME_CHARACTER
)
;

ALTER TABLE T_PARTY_BATTLE_MEMBER ADD (
    CONSTRAINT f_RL00452 FOREIGN KEY (SITE_CD,
                                      PARTNER_USER_SEQ,
                                      PARTNER_USER_CHAR_NO)
       REFERENCES T_GAME_CHARACTER
)
;

ALTER TABLE T_SOCIAL_GAME ADD (
    DEF_FORCE_DECREASE_LATE_MIN NUMBER(3,0)         DEFAULT 0 NOT NULL,
    DEF_FORCE_DECREASE_LATE_MAX NUMBER(3,0)         DEFAULT 0 NOT NULL,
    POLICE_COME_LATE    NUMBER(4,0)         DEFAULT 0 NOT NULL,
    POLICE_COME_LATE_LV_DIFFERENCE NUMBER(4,0)         DEFAULT 0 NOT NULL,
    POLICE_LV_DIFFERENCE NUMBER(4,0)         DEFAULT 0 NOT NULL
);


ALTER TABLE T_MAN_COMP_COUNT_GET_ITEM ADD (
    REVISION_NO         NUMBER(15,0)        DEFAULT 0 NOT NULL,
    UPDATE_DATE         DATE                NULL
)
;

ALTER TABLE T_COMP_MAN_TREASURE_GET_ITEM ADD (
    REVISION_NO         NUMBER(15,0)        DEFAULT 0 NOT NULL,
    UPDATE_DATE         DATE                NULL
)
;
ALTER TABLE T_GAME_ITEM_SALE ADD(
	NOTICE_DISP_FLAG NUMBER(1,0) DEFAULT 0 NOT NULL
);

CREATE SEQUENCE SEQ_COMP_COUNT_GET_ITEM
	START WITH 10000
	INCREMENT BY 1
	MINVALUE 10000
	MAXVALUE 999999999999999
	NOCACHE
	ORDER
	CYCLE
;

CREATE SEQUENCE SEQ_GAME_LOGIN_BONUS
	START WITH 20000
	INCREMENT BY 1
	MINVALUE 20000
	MAXVALUE 999999999999999
	NOCACHE
	ORDER
	CYCLE
;

ALTER TABLE T_GAME_COMMUNICATION ADD(
    LAST_SUPPORT_DATE   DATE                NULL,
    LAST_PARTNER_SUPPORT_DATE DATE                NULL,
    LAST_HUG_DATE       DATE                NULL,
    LAST_PARTNER_HUG_DATE DATE                NULL,
    LAST_PRESENT_DATE   DATE                NULL,
    LAST_PARTNER_PRESENT_DATE DATE                NULL
);

ALTER TABLE T_SOCIAL_GAME ADD(
    GET_TREASURE_FRIENDLY_POINT NUMBER(3,0)         DEFAULT 0 NOT NULL,
    HUG_FRIENDLY_POINT  NUMBER(3,0)         DEFAULT 0 NOT NULL,
    HUG_FRIENDLY_POINT_BY_FELLOW NUMBER(3,0)         DEFAULT 0 NOT NULL,
    HUG_COOPERATION_POINT NUMBER(3,0)         DEFAULT 0 NOT NULL,
    HUGGED_ADD_EXP_BY_FELLOW NUMBER(3,0)         DEFAULT 0 NOT NULL,
    FORCE_RECOVERY_COOP_POINT NUMBER(3,0)         DEFAULT 0 NOT NULL
);

DROP TABLE T_FRIENDLY_POINT;

/*************************************************************************/
-- 親密ﾎﾟｲﾝﾄ           テーブル定義
/*************************************************************************/
CREATE TABLE T_FRIENDLY_POINT (
    SITE_CD             VARCHAR2(4)         NOT NULL,
    USER_SEQ            NUMBER(15,0)        NOT NULL,
    USER_CHAR_NO        VARCHAR2(2)         NOT NULL,
    PARTNER_USER_SEQ    NUMBER(15,0)        NOT NULL,
    PARTNER_USER_CHAR_NO VARCHAR2(2)         NOT NULL,
    FRIENDLY_POINT_SEQ  NUMBER(15,0)        NOT NULL,
    FRIENDLY_POINT      NUMBER(6,0)         DEFAULT 0 NOT NULL,
    ENDRESS_SAVE_FLAG   NUMBER(1,0)         DEFAULT 0 NOT NULL,
    UPDATE_DATE         DATE                NULL,
    CONSTRAINT pk_T_FRIENDLY_POINT  PRIMARY KEY (SITE_CD,
                                                 USER_SEQ,
                                                 USER_CHAR_NO,
                                                 PARTNER_USER_SEQ,
                                                 PARTNER_USER_CHAR_NO,
                                                 FRIENDLY_POINT_SEQ)
)
;

CREATE INDEX ix_RL00409 ON T_FRIENDLY_POINT (
    SITE_CD                       ASC,
    USER_SEQ                      ASC,
    USER_CHAR_NO                  ASC
)
;

CREATE INDEX ix_RL00410 ON T_FRIENDLY_POINT (
    SITE_CD                       ASC,
    PARTNER_USER_SEQ              ASC,
    PARTNER_USER_CHAR_NO          ASC
)
;

CREATE INDEX ix_FRIENDLY_POINT01 ON T_FRIENDLY_POINT (
    UPDATE_DATE                   ASC,
    ENDRESS_SAVE_FLAG             ASC
)
;

ALTER TABLE T_FRIENDLY_POINT ADD (
    CONSTRAINT f_RL00409 FOREIGN KEY (SITE_CD,
                                      USER_SEQ,
                                      USER_CHAR_NO)
       REFERENCES T_GAME_CHARACTER ON DELETE CASCADE
)
;

ALTER TABLE T_FRIENDLY_POINT ADD (
    CONSTRAINT f_RL00410 FOREIGN KEY (SITE_CD,
                                      PARTNER_USER_SEQ,
                                      PARTNER_USER_CHAR_NO)
       REFERENCES T_GAME_CHARACTER ON DELETE CASCADE
)
;

ALTER TABLE T_SOCIAL_GAME ADD(
FRIENDLY_POINT_DEL_DAYS NUMBER(2,0)         DEFAULT 0 NOT NULL
);

UPDATE T_SOCIAL_GAME SET
FRIENDLY_POINT_DEL_DAYS = 7;

ALTER TABLE T_LEVEL_UP_EXP RENAME COLUMN LIMIT_FRIENDS_COUNT TO FELLOW_COUNT_LIMIT;
ALTER TABLE T_GAME_FELLOW RENAME COLUMN FRIEND_APPLICATION_STATUS TO FELLOW_APPLICATION_STATUS;


ALTER TABLE T_SOCIAL_GAME RENAME COLUMN DEF_FORCE_DECREASE_LATE_MIN TO DEF_FORCE_DECREASE_RATE_MIN;
ALTER TABLE T_SOCIAL_GAME RENAME COLUMN DEF_FORCE_DECREASE_LATE_MAX TO DEF_FORCE_DECREASE_RATE_MAX;
ALTER TABLE T_SOCIAL_GAME RENAME COLUMN POLICE_COME_LATE TO POLICE_COME_RATE;
ALTER TABLE T_SOCIAL_GAME RENAME COLUMN POLICE_COME_LATE_LV_DIFFERENCE TO POLICE_COME_RATE_LV_DIFFERENCE;

CREATE SEQUENCE SEQ_FRIENDLY_POINT
	START WITH 20000
	INCREMENT BY 1
	MINVALUE 20000
	MAXVALUE 999999999999999
	NOCACHE
	ORDER
	CYCLE
;

ALTER TABLE T_SOCIAL_GAME ADD(
    SAME_USER_ATTACK_LIMIT_COUNT NUMBER(2,0)         DEFAULT 0 NOT NULL,
    PT_BATTLE_ATTACK_LIMIT_COUNT NUMBER(2,0)         DEFAULT 0 NOT NULL,
    PT_BATTLE_ATTACK_INTERVAL_MIN NUMBER(3,0)         DEFAULT 0 NOT NULL,
    DATE_LIMIT_COUNT    NUMBER(2,0)         DEFAULT 0 NOT NULL,
    PART_TIME_JOB_LIMIT_COUNT NUMBER(2,0)         DEFAULT 0 NOT NULL
);

ALTER TABLE T_SOCIAL_GAME ADD(
    BATTLE_WIN_EXP      NUMBER(3,0)         DEFAULT 0 NOT NULL,
    BATTLE_LOSS_EXP     NUMBER(3,0)         DEFAULT 0 NOT NULL,
    PARTY_BATTLE_WIN_EXP NUMBER(3,0)         DEFAULT 0 NOT NULL,
    PARTY_BATTLE_LOSS_EXP NUMBER(3,0)         DEFAULT 0 NOT NULL
);

ALTER TABLE T_POSSESSION_MAN_TREASURE ADD(
    USED_DOUBLE_TRAP_COUNT NUMBER(3,0)         DEFAULT 0 NOT NULL,
    USED_DOUBLE_TRAP_FLAG NUMBER(1,0)         DEFAULT 0 NOT NULL
);

ALTER TABLE T_POSSESSION_CAST_TREASURE ADD(
    USED_DOUBLE_TRAP_COUNT NUMBER(3,0)         DEFAULT 0 NOT NULL,
    USED_DOUBLE_TRAP_FLAG NUMBER(1,0)         DEFAULT 0 NOT NULL
);

ALTER TABLE T_SOCIAL_GAME ADD(
    ADD_FELLOW_FORCE             NUMBER(3,0)         DEFAULT 0 NOT NULL,
    REMOVE_FELLOW_FORCE          NUMBER(3,0)         DEFAULT 0 NOT NULL,
    REMOVE_FELLOW_PARTNER_FORCE  NUMBER(3,0)         DEFAULT 0 NOT NULL
);

DROP TABLE T_GAME_LOGIN_BONUS_MONTH_LOG;
DROP TABLE T_GAME_LOGIN_BONUS_DAY_LOG;
DROP TABLE T_GAME_LOGIN_BONUS_MONTHLY;
DROP TABLE T_GAME_LOGIN_BONUS_DAY;


/*************************************************************************/
-- 月別ｹﾞｰﾑﾛｸﾞｲﾝﾎﾞｰﾅｽ           テーブル定義
/*************************************************************************/
CREATE TABLE T_GAME_LOGIN_BONUS_MONTHLY (
    SITE_CD             VARCHAR2(4)         NOT NULL,
    GAME_LOGIN_BONUS_MONTH VARCHAR2(7)         NOT NULL,
    GAME_LOGIN_BONUS_SEQ NUMBER(15,0)        NOT NULL,
    SEX_CD              VARCHAR2(1)         NULL,
    GAME_ITEM_SEQ       NUMBER(15,0)        NULL,
    ITEM_COUNT          NUMBER(10,0)        DEFAULT 0 NOT NULL,
    NEED_LOGIN_COUNT    NUMBER(10,0)        DEFAULT 0 NOT NULL,
    REVISION_NO         NUMBER(15,0)        DEFAULT 0 NOT NULL,
    UPDATE_DATE         DATE                NULL,
    CONSTRAINT pk_T_GAME_LOGIN_BONUS_MONTHLY  PRIMARY KEY (SITE_CD,
                                                           GAME_LOGIN_BONUS_MONTH,
                                                           GAME_LOGIN_BONUS_SEQ)
)
;

CREATE INDEX ix_RL00355 ON T_GAME_LOGIN_BONUS_MONTHLY (
    SITE_CD                       ASC,
    GAME_ITEM_SEQ                 ASC
)
;

CREATE INDEX ix_RL00398 ON T_GAME_LOGIN_BONUS_MONTHLY (
    SITE_CD                       ASC
)
;

/*************************************************************************/
-- 日別ｹﾞｰﾑﾛｸﾞｲﾝﾎﾞｰﾅｽ           テーブル定義
/*************************************************************************/
CREATE TABLE T_GAME_LOGIN_BONUS_DAY (
    SITE_CD             VARCHAR2(4)         NOT NULL,
    GAME_LOGIN_BONUS_SEQ NUMBER(15,0)        NOT NULL,
    GAME_LOGIN_BONUS_START_DATE DATE                NULL,
    GAME_LOGIN_BONUS_END_DATE DATE                NULL,
    SEX_CD              VARCHAR2(1)         NULL,
    GAME_ITEM_SEQ       NUMBER(15,0)        NULL,
    ITEM_COUNT          NUMBER(10,0)        DEFAULT 0 NOT NULL,
    GAME_POINT          NUMBER(10,0)        DEFAULT 0 NOT NULL,
    REVISION_NO         NUMBER(15,0)        DEFAULT 0 NOT NULL,
    UPDATE_DATE         DATE                NULL,
    CONSTRAINT pk_T_GAME_LOGIN_BONUS_DAY  PRIMARY KEY (SITE_CD,
                                                       GAME_LOGIN_BONUS_SEQ)
)
;

CREATE INDEX ix_RL00347 ON T_GAME_LOGIN_BONUS_DAY (
    SITE_CD                       ASC
)
;

CREATE INDEX ix_RL00352 ON T_GAME_LOGIN_BONUS_DAY (
    SITE_CD                       ASC,
    GAME_ITEM_SEQ                 ASC
)
;

ALTER TABLE T_GAME_LOGIN_BONUS_MONTHLY ADD (
    CONSTRAINT f_RL00355 FOREIGN KEY (SITE_CD,
                                      GAME_ITEM_SEQ)
       REFERENCES T_GAME_ITEM
)
;

ALTER TABLE T_GAME_LOGIN_BONUS_MONTHLY ADD (
    CONSTRAINT f_RL00398 FOREIGN KEY (SITE_CD)
       REFERENCES T_SITE
)
;

ALTER TABLE T_GAME_LOGIN_BONUS_DAY ADD (
    CONSTRAINT f_RL00347 FOREIGN KEY (SITE_CD)
       REFERENCES T_SITE
)
;

ALTER TABLE T_GAME_LOGIN_BONUS_DAY ADD (
    CONSTRAINT f_RL00352 FOREIGN KEY (SITE_CD,
                                      GAME_ITEM_SEQ)
       REFERENCES T_GAME_ITEM
)
;


/*************************************************************************/
-- 月別ｹﾞｰﾑﾛｸﾞｲﾝﾎﾞｰﾅｽ獲得履歴           テーブル定義
/*************************************************************************/
CREATE TABLE T_GAME_LOGIN_BONUS_MONTH_LOG (
    SITE_CD             VARCHAR2(4)         NOT NULL,
    USER_SEQ            NUMBER(15,0)        NOT NULL,
    USER_CHAR_NO        VARCHAR2(2)         NOT NULL,
    GET_DATE            DATE                NULL,
    GAME_LOGIN_BONUS_SEQ NUMBER(15,0)        NOT NULL,
    GAME_LOGIN_BONUS_MONTH VARCHAR2(7)         NOT NULL,
    CONSTRAINT pk_T_GAME_LOGIN_BONUS_MONTH_LO  PRIMARY KEY (SITE_CD,
                                                            USER_SEQ,
                                                            USER_CHAR_NO,
                                                            GAME_LOGIN_BONUS_SEQ,
                                                            GAME_LOGIN_BONUS_MONTH)
)
;

CREATE INDEX ix_RL00413 ON T_GAME_LOGIN_BONUS_MONTH_LOG (
    SITE_CD                       ASC,
    USER_SEQ                      ASC,
    USER_CHAR_NO                  ASC
)
;

CREATE INDEX ix_RL00414 ON T_GAME_LOGIN_BONUS_MONTH_LOG (
    SITE_CD                       ASC,
    GAME_LOGIN_BONUS_SEQ          ASC,
    GAME_LOGIN_BONUS_MONTH        ASC
)
;

/*************************************************************************/
-- 日別ｹﾞｰﾑﾛｸﾞｲﾝﾎﾞｰﾅｽ獲得履歴           テーブル定義
/*************************************************************************/
CREATE TABLE T_GAME_LOGIN_BONUS_DAY_LOG (
    SITE_CD             VARCHAR2(4)         NOT NULL,
    USER_SEQ            NUMBER(15,0)        NOT NULL,
    USER_CHAR_NO        VARCHAR2(2)         NOT NULL,
    GET_DATE            DATE                NULL,
    REPORT_DAY          VARCHAR2(10)        NOT NULL,
    CONSTRAINT pk_T_GAME_LOGIN_BONUS_DAY_LOG  PRIMARY KEY (SITE_CD,
                                                           USER_SEQ,
                                                           USER_CHAR_NO,
                                                           REPORT_DAY)
)
;

CREATE INDEX ix_RL00374 ON T_GAME_LOGIN_BONUS_DAY_LOG (
    SITE_CD                       ASC,
    USER_SEQ                      ASC,
    USER_CHAR_NO                  ASC
)
;

ALTER TABLE T_GAME_LOGIN_BONUS_MONTH_LOG ADD (
    CONSTRAINT f_RL00413 FOREIGN KEY (SITE_CD,
                                      USER_SEQ,
                                      USER_CHAR_NO)
       REFERENCES T_GAME_CHARACTER ON DELETE CASCADE
)
;

ALTER TABLE T_GAME_LOGIN_BONUS_MONTH_LOG ADD (
    CONSTRAINT f_RL00414 FOREIGN KEY (SITE_CD,
                                      GAME_LOGIN_BONUS_MONTH,
                                      GAME_LOGIN_BONUS_SEQ)
       REFERENCES T_GAME_LOGIN_BONUS_MONTHLY ON DELETE CASCADE
)
;

ALTER TABLE T_GAME_LOGIN_BONUS_DAY_LOG ADD (
    CONSTRAINT f_RL00374 FOREIGN KEY (SITE_CD,
                                      USER_SEQ,
                                      USER_CHAR_NO)
       REFERENCES T_GAME_CHARACTER ON DELETE CASCADE
)
;

ALTER TABLE T_BATTLE_LOG RENAME COLUMN SUPPORT_REQUEST_KILL_FLAG TO SUPPORT_REQUEST_END_FLAG;


ALTER TABLE T_POSSESSION_MAN_TREASURE DROP COLUMN USED_DOUBLE_TRAP_FLAG;
ALTER TABLE T_POSSESSION_CAST_TREASURE DROP COLUMN USED_DOUBLE_TRAP_FLAG;

ALTER TABLE T_DAILY_PART_TIME_JOB_LOG ADD (
    PART_TIME_JOB_SEQ       NUMBER(15,0)        NULL
)
;

CREATE SEQUENCE SEQ_BATTLE_LOG
	START WITH 20000
	INCREMENT BY 1
	MINVALUE 20000
	MAXVALUE 999999999999999
	NOCACHE
	ORDER
	CYCLE
;

ALTER TABLE T_DAILY_DATE_LOG ADD (
    DATE_PLAN_SEQ       NUMBER(15,0)        NULL
)
;

CREATE SEQUENCE SEQ_DATE_LOG
	START WITH 4000
	INCREMENT BY 1
	MINVALUE 4000
	MAXVALUE 999999999999999
	NOCACHE
	ORDER
	CYCLE
;

ALTER TABLE T_GAME_CHARACTER ADD(
    TOTAL_PARTY_ATTACK_WIN_COUNT NUMBER(8,0)         DEFAULT 0 NOT NULL,
    TOTAL_PARTY_ATTACK_LOSS_COUNT NUMBER(8,0)         DEFAULT 0 NOT NULL,
    TOTAL_PARTY_DEFENCE_WIN_COUNT NUMBER(8,0)         DEFAULT 0 NOT NULL,
    TOTAL_PARTY_DEFENCE_LOSS_COUNT NUMBER(8,0)         DEFAULT 0 NOT NULL
);

DROP TABLE T_PARTY_BATTLE_MEMBER;

ALTER TABLE T_GAME_FELLOW ADD(
    PARTY_MEMBER_FLAG   NUMBER(1,0)         DEFAULT 0 NOT NULL
);


/*************************************************************************/
-- 進行履歴           テーブル定義
/*************************************************************************/
CREATE TABLE T_PROGRESS_LOG (
    SITE_CD             VARCHAR2(4)         NOT NULL,
    USER_SEQ            NUMBER(15,0)        NOT NULL,
    USER_CHAR_NO        VARCHAR2(2)         NOT NULL,
    SEQ_PROGRESS_LOG    NUMBER(15,0)        NOT NULL,
    PROGRESS_LOG_TYPE   VARCHAR2(1)         NULL,
    STAGE_SEQ           NUMBER(15,0)        NULL,
    TOWN_SEQ            NUMBER(15,0)        NOT NULL,
    CREATE_DATE         DATE                NULL,
    CONSTRAINT pk_T_PROGRESS_LOG  PRIMARY KEY (SITE_CD,
                                               USER_SEQ,
                                               USER_CHAR_NO,
                                               SEQ_PROGRESS_LOG)
)
;

CREATE INDEX ix_RL00451 ON T_PROGRESS_LOG (
    SITE_CD                       ASC,
    STAGE_SEQ                     ASC
)
;

CREATE INDEX ix_RL00452 ON T_PROGRESS_LOG (
    SITE_CD                       ASC,
    STAGE_SEQ                     ASC,
    TOWN_SEQ                      ASC
)
;

CREATE INDEX ix_RL00453 ON T_PROGRESS_LOG (
    SITE_CD                       ASC,
    USER_SEQ                      ASC,
    USER_CHAR_NO                  ASC
)
;

ALTER TABLE T_PROGRESS_LOG ADD (
    CONSTRAINT f_RL00451 FOREIGN KEY (SITE_CD,
                                      STAGE_SEQ)
       REFERENCES T_STAGE
)
;

ALTER TABLE T_PROGRESS_LOG ADD (
    CONSTRAINT f_RL00452 FOREIGN KEY (SITE_CD,
                                      STAGE_SEQ,
                                      TOWN_SEQ)
       REFERENCES T_TOWN
)
;

ALTER TABLE T_PROGRESS_LOG ADD (
    CONSTRAINT f_RL00453 FOREIGN KEY (SITE_CD,
                                      USER_SEQ,
                                      USER_CHAR_NO)
       REFERENCES T_GAME_CHARACTER ON DELETE CASCADE
)
;

CREATE INDEX ix_RL00415 ON T_DAILY_PART_TIME_JOB_LOG (
    SITE_CD                       ASC,
    PART_TIME_JOB_SEQ             ASC
)
;

ALTER TABLE T_DAILY_PART_TIME_JOB_LOG ADD (
    CONSTRAINT f_RL00415 FOREIGN KEY (SITE_CD,
                                      PART_TIME_JOB_SEQ)
       REFERENCES T_PART_TIME_JOB_PLAN
)
;

CREATE INDEX ix_RL00454 ON T_DAILY_DATE_LOG (
    SITE_CD                       ASC,
    DATE_PLAN_SEQ                 ASC
)
;

ALTER TABLE T_DAILY_DATE_LOG ADD (
    CONSTRAINT f_RL00454 FOREIGN KEY (SITE_CD,
                                      DATE_PLAN_SEQ)
       REFERENCES T_DATE_PLAN
)
;

CREATE SEQUENCE SEQ_PROGRESS_LOG
	START WITH 20000
	INCREMENT BY 1
	MINVALUE 20000
	MAXVALUE 999999999999999
	NOCACHE
	ORDER
	CYCLE
;

ALTER TABLE T_SOCIAL_GAME ADD(
BATTLE_DECREASE_ENDURANCE NUMBER(3,0)         DEFAULT 0 NOT NULL
);

ALTER TABLE T_DAILY_DATE_LOG MODIFY DATE_COUNT NUMBER(8,0);

ALTER TABLE T_GAME_COMMUNICATION ADD(
    DATE_COUNT NUMBER(8,0)        DEFAULT 0 NOT NULL,
    LAST_DATE_DATE DATE
);

