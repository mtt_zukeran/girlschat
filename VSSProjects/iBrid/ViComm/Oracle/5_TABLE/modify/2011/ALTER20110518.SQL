DELETE T_ERROR WHERE ERROR_CD LIKE 'CR%';
INSERT INTO T_ERROR(SITE_CD,ERROR_CD,ERROR_DTL,ERROR_ABBR,UPDATE_DATE) VALUES('A001','CR00','通話要求がありました。'												,'通話要求のみ'										,SYSDATE);
INSERT INTO T_ERROR(SITE_CD,ERROR_CD,ERROR_DTL,ERROR_ABBR,UPDATE_DATE) VALUES('A001','CR01','通話ページまで来ましたが、TEL発信せず別ページへ行ってしまいました。'	,'TEL発信なし'										,SYSDATE);
INSERT INTO T_ERROR(SITE_CD,ERROR_CD,ERROR_DTL,ERROR_ABBR,UPDATE_DATE) VALUES('A001','CR11','{0}の任意切断によって通話が終了しました。'								,'{0}の切断により終了'								,SYSDATE);
INSERT INTO T_ERROR(SITE_CD,ERROR_CD,ERROR_DTL,ERROR_ABBR,UPDATE_DATE) VALUES('A001','CR12','{0}の任意切断によって通話が終了しました。'								,'{0}の切断により終了'								,SYSDATE);
INSERT INTO T_ERROR(SITE_CD,ERROR_CD,ERROR_DTL,ERROR_ABBR,UPDATE_DATE) VALUES('A001','CR13','男性会員のﾎﾟｲﾝﾄがなくなった為、通話が終了しました。'					,'0ﾎﾟｲﾝﾄにより終了'									,SYSDATE);
INSERT INTO T_ERROR(SITE_CD,ERROR_CD,ERROR_DTL,ERROR_ABBR,UPDATE_DATE) VALUES('A001','CR21','{0}が電話に出なかった為、接続できませんでした。'						,'{0}が電話に出ない'								,SYSDATE);
INSERT INTO T_ERROR(SITE_CD,ERROR_CD,ERROR_DTL,ERROR_ABBR,UPDATE_DATE) VALUES('A001','CR22','{0}の携帯電話が通話中だった為、接続できませんでした。'					,'{0}が通話中'										,SYSDATE);
INSERT INTO T_ERROR(SITE_CD,ERROR_CD,ERROR_DTL,ERROR_ABBR,UPDATE_DATE) VALUES('A001','CR23','{0}がTV電話か、音声通話の種別を間違えた為、接続できませんでした。'		,'{0}が発信種別違い'								,SYSDATE);
INSERT INTO T_ERROR(SITE_CD,ERROR_CD,ERROR_DTL,ERROR_ABBR,UPDATE_DATE) VALUES('A001','CR24','登録されている電話番号に誤りがある為、接続できませんでした。'			,'電話番号未登録'									,SYSDATE);
INSERT INTO T_ERROR(SITE_CD,ERROR_CD,ERROR_DTL,ERROR_ABBR,UPDATE_DATE) VALUES('A001','CR97','{0}の携帯電話が通話中だった為、接続できませんでした。'					,'着信拒否により通話不可'							,SYSDATE);
INSERT INTO T_ERROR(SITE_CD,ERROR_CD,ERROR_DTL,ERROR_ABBR,UPDATE_DATE) VALUES('A001','CR98','その他の原因によって通話が開始できませんでした。'						,'その他の理由により切断'							,SYSDATE);
INSERT INTO T_ERROR(SITE_CD,ERROR_CD,ERROR_DTL,ERROR_ABBR,UPDATE_DATE) VALUES('A001','CR99','申し訳ございませんが、システムエラーの為、接続できませんでした。'		,'システムエラー'									,SYSDATE);


UPDATE T_MAIL_TYPE SET MAIL_TYPE_NM = '仮登録受付メール(キャスト指定なし)' WHERE MAIL_TYPE ='06';
UPDATE T_MAIL_TYPE SET MAIL_TYPE_NM = '仮登録受付メール(キャスト指定あり)' WHERE MAIL_TYPE ='07';
COMMIT;

/*************************************************************************/
-- 男性ﾕｰｻﾞｰｷｬﾗｸﾀｰﾌﾞﾗｯｸ           テーブル定義
/*************************************************************************/
CREATE TABLE T_USER_MAN_CHARACTER_BLACK (
    SITE_CD             VARCHAR2(4)         NOT NULL,
    USER_SEQ            NUMBER(15,0)        NOT NULL,
    USER_CHAR_NO        VARCHAR2(2)         NOT NULL,
    BLACK_TYPE          VARCHAR2(1)         NOT NULL,
    REGIST_DATE         DATE                NULL,
    REMARKS             VARCHAR2(3000)      NULL,
    DEL_FLAG            NUMBER(1,0)         DEFAULT 0 NOT NULL,
    UPDATE_DATE         DATE                NULL,
    REVISION_NO         NUMBER(15,0)        DEFAULT 0 NOT NULL,
    CONSTRAINT pk_T_USER_MAN_CHARACTER_BLACK  PRIMARY KEY (SITE_CD,
                                                           USER_SEQ,
                                                           USER_CHAR_NO,
                                                           BLACK_TYPE)
)
;

CREATE INDEX ix_RL00319 ON T_USER_MAN_CHARACTER_BLACK (
    SITE_CD                       ASC,
    USER_SEQ                      ASC,
    USER_CHAR_NO                  ASC
)
;

/*************************************************************************/
-- 外部キー定義
/*************************************************************************/
ALTER TABLE T_USER_MAN_CHARACTER_BLACK ADD (
    CONSTRAINT f_RL00319 FOREIGN KEY (SITE_CD,
                                      USER_SEQ,
                                      USER_CHAR_NO)
       REFERENCES T_USER_MAN_CHARACTER ON DELETE CASCADE
)
;




ALTER TABLE T_SITE_MANAGEMENT DROP COLUMN CAST_WAIT_START_MAIL_NO;

ALTER TABLE T_SITE_MANAGEMENT_EX ADD(
    TP_MAN_MAIL_MOVIE   VARCHAR2(6)         NULL,
    TP_MAN_MAIL_PIC     VARCHAR2(6)         NULL,
    TP_MAN_PF_PIC       VARCHAR2(6)         NULL,
    TP_MAN_TEL_FAIL_VOICE_TV VARCHAR2(6)         NULL,
    TP_MAN_TEL_FAIL_TV_VOICE VARCHAR2(6)         NULL,
    TP_MAN_WAITING_CAST_WITH_TALK VARCHAR2(6)         NULL,
    TP_MAN_TALK_BUSY    VARCHAR2(6)         NULL,
    TP_CAST_MAIL_MOVIE   VARCHAR2(6)         NULL,
    TP_CAST_MAIL_PIC     VARCHAR2(6)         NULL,
    TP_CAST_BBS_MOVIE   VARCHAR2(6)         NULL,
    TP_CAST_BBS_PIC     VARCHAR2(6)         NULL,
    TP_CAST_PF_MOVIE    VARCHAR2(6)         NULL,
    TP_CAST_PF_PIC      VARCHAR2(6)         NULL,
    TP_CAST_TEL_FAIL_VOICE_TV VARCHAR2(6)         NULL,
    TP_CAST_TEL_FAIL_TV_VOICE VARCHAR2(6)         NULL,
    TP_CAST_TALK_BUSY   VARCHAR2(6)         NULL
);


CREATE INDEX ix_WANTED_TARGET_A1_1 ON T_WANTED_TARGET (
    SITE_CD                       ASC,
    USER_SEQ                      ASC,
    USER_CHAR_NO                  ASC
)
;





ALTER TABLE T_CAST_CHARACTER_EX ADD(
    SITE_CD             VARCHAR2(4)         NOT NULL,
    USER_SEQ            NUMBER(15,0)        NOT NULL,
    USER_CHAR_NO        VARCHAR2(2)         NOT NULL,
    ENABLED_BLOG_FLAG   NUMBER(1,0)         DEFAULT 0 NOT NULL,
    ENABLED_RICHINO_FLAG NUMBER(1,0)         DEFAULT 0 NOT NULL,
    BUY_POINT_MAIL_RX_TYPE VARCHAR2(1)         NULL,
    WAITING_POINT_LAST_ADD_DAY VARCHAR2(10)        NULL,
    WAITING_POINT_LAST_RELEASE_DAY VARCHAR2(10)        NULL,
    FACE_PIC_BOUNS_STATUS NUMBER(1,0)         DEFAULT 0 NOT NULL,
    UPDATE_DATE         DATE                NULL,
    REVISION_NO         NUMBER(15,0)        DEFAULT 0 NOT NULL,
    NO_UPDATE_RICHINO_VIEW_FLAG NUMBER(1,0)         NULL,
    CONSTRAINT pk_T_CAST_CHARACTER_EX  PRIMARY KEY (SITE_CD,
                                                    USER_SEQ,
                                                    USER_CHAR_NO)
)
;


ALTER TABLE T_USER_MAN_CHARACTER_EX ADD(
    WAIT_MAIL_RX_FLAG   NUMBER(1,0)         DEFAULT 0 NOT NULL
);
