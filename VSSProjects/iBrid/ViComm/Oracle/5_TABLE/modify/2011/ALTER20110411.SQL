/*************************************************************************/
-- ｷｬｽﾄｷｬﾗｸﾀｰﾌﾞﾗｯｸ           テーブル定義
/*************************************************************************/
CREATE TABLE T_CAST_CHARACTER_BLACK (
    SITE_CD             VARCHAR2(4)         NOT NULL,
    USER_CHAR_NO        VARCHAR2(2)         NOT NULL,
    USER_SEQ            NUMBER(15,0)        NOT NULL,
    BLACK_TYPE          VARCHAR2(1)         NOT NULL,
    REGIST_DATE         DATE                NULL,
    REMARKS             VARCHAR2(3000)      NULL,
    DEL_FLAG            NUMBER(1,0)         DEFAULT 0 NOT NULL,
    UPDATE_DATE         DATE                NULL,
    REVISION_NO         NUMBER(15,0)        DEFAULT 0 NOT NULL,
    CONSTRAINT pk_T_CAST_CHARACTER_BLACK  PRIMARY KEY (SITE_CD,
                                                       USER_SEQ,
                                                       USER_CHAR_NO,
                                                       BLACK_TYPE)
)
;

CREATE INDEX ix_RL00309 ON T_CAST_CHARACTER_BLACK (
    SITE_CD                       ASC,
    USER_SEQ                      ASC,
    USER_CHAR_NO                  ASC
)
;

ALTER TABLE T_CAST_CHARACTER_BLACK ADD (
    CONSTRAINT f_RL00309 FOREIGN KEY (SITE_CD,
                                      USER_SEQ,
                                      USER_CHAR_NO)
       REFERENCES T_CAST_CHARACTER ON DELETE CASCADE
)
;






CREATE INDEX ix_GAME_SCORE_A3_04 ON T_GAME_SCORE (
    GAME_SCORE_SEQ                ASC
)
;

CREATE INDEX ix_GAME_SCORE_A3_03 ON T_GAME_SCORE (
    SITE_CD                       ASC,
    GAME_TYPE                     ASC,
    SEX_CD                        ASC,
    USER_SEQ                      ASC,
    USER_CHAR_NO                  ASC,
    GAME_SCORE                    ASC
)
;

CREATE INDEX ix_GAME_SCORE_A3_02 ON T_GAME_SCORE (
    SITE_CD                       ASC,
    GAME_TYPE                     ASC,
    SEX_CD                        ASC,
    GAME_SCORE_SEQ                ASC
)
;

CREATE INDEX ix_GAME_SCORE_A3_01 ON T_GAME_SCORE (
    SITE_CD                       ASC,
    GAME_TYPE                     ASC,
    SEX_CD                        ASC,
    GAME_SCORE                    ASC
)
;

CREATE SEQUENCE SEQ_GAME_PLAY_LOG
       START WITH 100000
       INCREMENT BY 1
       MINVALUE 100000
       MAXVALUE 999999999999999
       NOCACHE
       ORDER
       CYCLE
;

DROP TABLE T_GAME_PLAY_WEEKLY_SUMMARY;

CREATE TABLE T_GAME_PLAY_SUMMARY (
    SITE_CD             VARCHAR2(4)         NOT NULL,
    GAME_TYPE           VARCHAR2(2)         NOT NULL,
    SEX_CD              VARCHAR2(1)         NOT NULL,
    REPORT_DAY          VARCHAR2(10)        NOT NULL,
    GAME_PLAY_COUNT     NUMBER(15,0)        DEFAULT 0 NOT NULL,
    CONSTRAINT pk_T_GAME_PLAY_SUMMARY  PRIMARY KEY (SITE_CD,
                                                    GAME_TYPE,
                                                    SEX_CD,
                                                    REPORT_DAY)
)
;

CREATE INDEX ix_RL00299 ON T_GAME_PLAY_SUMMARY (
    SITE_CD                       ASC,
    GAME_TYPE                     ASC,
    SEX_CD                        ASC
)
;

ALTER TABLE T_GAME_PLAY_SUMMARY ADD (
    CONSTRAINT f_RL00299 FOREIGN KEY (SITE_CD,
                                      GAME_TYPE,
                                      SEX_CD)
       REFERENCES T_GAME
)
;

ALTER TABLE T_SITE ADD (
    CAST_REGIST_REPORT_TIMING VARCHAR2(1)         NULL,
    MAN_REGIST_REPORT_TIMING VARCHAR2(1)         NULL
);

ALTER TABLE T_CAST_CHARACTER_EX ADD(
    WAITING_POINT_LAST_ADD_DAY VARCHAR2(10)        NULL,
    WAITING_POINT_LAST_RELEASE_DAY VARCHAR2(10)        NULL
);


CREATE INDEX ix_USER_DEF_POINT_HISTORY_3_01 ON T_USER_DEF_POINT_HISTORY (
    SITE_CD                       ASC,
    USER_SEQ                      ASC,
    ADD_POINT_ID                  ASC,
    ADD_POINT_DATE                ASC
)
;



DROP INDEX ix_RL00280;
ALTER TABLE T_BLOG ADD CONSTRAINTS uk_RL00280 UNIQUE (SITE_CD,USER_SEQ,USER_CHAR_NO);
CREATE UNIQUE INDEX ix_BLOG_A2_01 			ON T_BLOG 			(BLOG_SEQ ASC);
CREATE UNIQUE INDEX ix_BLOG_OBJ_A2_01 		ON T_BLOG_OBJS 		(OBJ_SEQ ASC);
CREATE UNIQUE INDEX ix_BLOG_ARTICLE_A2_01 	ON T_BLOG_ARTICLE 	(BLOG_ARTICLE_SEQ ASC);

CREATE INDEX ix_BLOG_OBJS_A2_02 ON T_BLOG_OBJS (
    SITE_CD                       ASC,
    USER_SEQ                      ASC,
    USER_CHAR_NO                  ASC,
    BLOG_FILE_TYPE                ASC
)
;



CREATE SEQUENCE SEQ_RANKING_CTL
       START WITH 200 
       INCREMENT BY 1
       MINVALUE 200
       MAXVALUE 999999999999999
       NOCACHE
       ORDER
       CYCLE
;




CREATE INDEX ix_CAST_CHARACTER_EX_A0_02 ON T_CAST_CHARACTER_EX (
    USER_SEQ                      ASC
)
;

CREATE INDEX ix_CAST_CHARACTER_EX_A0_01 ON T_CAST_CHARACTER_EX (
    SITE_CD                       ASC,
    USER_SEQ                      ASC
)
;


ALTER TABLE T_MARKING ADD(RANKING_CTL_SEQ NUMBER(15));


DROP TABLE T_MARKING_RANKING;
DROP TABLE T_MARKING_RANKING_CTL;


/*************************************************************************/
-- ﾗﾝｷﾝｸﾞ集計拡張           テーブル定義
/*************************************************************************/
CREATE TABLE T_RANKING_EX (
    SITE_CD             VARCHAR2(4)         NOT NULL,
    SUMMARY_TYPE        VARCHAR2(1)         NOT NULL,
    USER_SEQ            NUMBER(15,0)        NOT NULL,
    USER_CHAR_NO        VARCHAR2(2)         NOT NULL,
    RANKING_CTL_SEQ     NUMBER(15,0)        NOT NULL,
    LAST_LOGIN_DATE     DATE                NULL,
    SUMMARY_COUNT       NUMBER(6,0)         DEFAULT 0 NOT NULL,
    CONSTRAINT pk_T_RANKING_EX  PRIMARY KEY (SITE_CD,
                                             SUMMARY_TYPE,
                                             USER_SEQ,
                                             USER_CHAR_NO,
                                             RANKING_CTL_SEQ)
)
;

CREATE INDEX ix_RL00311 ON T_RANKING_EX (
    SITE_CD                       ASC,
    SUMMARY_TYPE                  ASC,
    RANKING_CTL_SEQ               ASC
)
;

CREATE INDEX ix_RANKING_EX02 ON T_RANKING_EX (
    SITE_CD                       ASC,
    USER_SEQ                      ASC,
    USER_CHAR_NO                  ASC
)
;

CREATE INDEX ix_RANKING_EX01 ON T_RANKING_EX (
    SITE_CD                       ASC,
    SUMMARY_COUNT                 ASC
)
;
/*************************************************************************/
-- ﾗﾝｷﾝｸﾞ集計設定           テーブル定義
/*************************************************************************/
CREATE TABLE T_RANKING_CTL (
    SITE_CD             VARCHAR2(4)         NOT NULL,
    SUMMARY_TYPE        VARCHAR2(1)         NOT NULL,
    RANKING_CTL_SEQ     NUMBER(15,0)        NOT NULL,
    SUMMARY_START_DATE  DATE                NULL,
    SUMMARY_END_DATE    DATE                NULL,
    REMARKS             VARCHAR2(3000)      NULL,
    UPDATE_DATE         DATE                NULL,
    REVISION_NO         NUMBER(15,0)        DEFAULT 0 NOT NULL,
    CONSTRAINT pk_T_RANKING_CTL  PRIMARY KEY (SITE_CD,
                                              SUMMARY_TYPE,
                                              RANKING_CTL_SEQ)
)
;

CREATE INDEX ix_RL00310 ON T_RANKING_CTL (
    SITE_CD                       ASC
)
;

CREATE INDEX ix_RANKING_CTL02 ON T_RANKING_CTL (
    RANKING_CTL_SEQ               ASC
)
;

CREATE INDEX ix_RANKING_CTL01 ON T_RANKING_CTL (
    SITE_CD                       ASC,
    SUMMARY_START_DATE            DESC,
    SUMMARY_END_DATE              DESC
)
;

ALTER TABLE T_RANKING_EX ADD (
    CONSTRAINT f_RL00311 FOREIGN KEY (SITE_CD,
                                      SUMMARY_TYPE,
                                      RANKING_CTL_SEQ)
       REFERENCES T_RANKING_CTL
)
;


ALTER TABLE T_RANKING_CTL ADD (
    CONSTRAINT f_RL00310 FOREIGN KEY (SITE_CD)
       REFERENCES T_SITE
)
;

ALTER TABLE T_FAVORIT ADD(
    LAST_VIEW_PROFILE_DATE DATE                NULL
);

DROP PACKAGE BODY PKG_MARKING_RANKING;
DROP PACKAGE PKG_MARKING_RANKING;
DROP PACKAGE BODY PKG_MARKING_RANKING_CTL;
DROP PACKAGE PKG_MARKING_RANKING_CTL;


CREATE INDEX ix_RANKING_EX03 ON T_RANKING_EX (
    SITE_CD                       ASC,
    SUMMARY_COUNT                 ASC,
    LAST_LOGIN_DATE               ASC
)
;

CREATE INDEX ix_FAVORIT_3_06 ON T_FAVORIT (
    SITE_CD                       ASC,
    LAST_VIEW_PROFILE_DATE        ASC
)
;

ALTER TABLE T_SITE_MANAGEMENT_EX ADD(LOVE_LIST_PROFILE_DAYS NUMBER(2) DEFAULT 0 NOT NULL);

DROP VIEW VW_MARKING_RANKING01;
DROP PROCEDURE MARKING_RANKING_CTL_GET;
DROP PROCEDURE MARKING_RANKING_CTL_MAINTE;


DROP INDEX ix_USER_3_07;
CREATE INDEX ix_USER_3_07 ON T_USER (
    IMODE_ID                      ASC,
    NOT_USED_EASY_LOGIN           ASC
)
;




/*************************************************************************/
-- 掲示板ｽﾚｯﾄﾞ           テーブル定義
/*************************************************************************/
CREATE TABLE T_BBS_THREAD (
    SITE_CD             VARCHAR2(4)         NOT NULL,
    USER_SEQ            NUMBER(15,0)        NOT NULL,
    USER_CHAR_NO        VARCHAR2(2)         NOT NULL,
    BBS_THREAD_SEQ      NUMBER(15,0)        NOT NULL,
    BBS_THREAD_HANDLE_NM VARCHAR2(60)        NULL,
    BBS_THREAD_TITLE    VARCHAR2(4000)      NULL,
    BBS_THREAD_DOC      VARCHAR2(4000)      NULL,
    ADMIN_THREAD_FLAG   NUMBER(1,0)         DEFAULT 0 NOT NULL,
    LAST_RES_WRITE_DATE DATE                NULL,
    LAST_THREAD_SUB_SEQ NUMBER(6,0)         DEFAULT 0 NOT NULL,
    DEL_FLAG            NUMBER(1,0)         DEFAULT 0 NOT NULL,
    CREATE_DATE         DATE                NULL,
    UPDATE_DATE         DATE                NULL,
    REVISION_NO         NUMBER(15,0)        DEFAULT 0 NOT NULL,
    CONSTRAINT pk_T_BBS_THREAD  PRIMARY KEY (SITE_CD,
                                             USER_SEQ,
                                             USER_CHAR_NO,
                                             BBS_THREAD_SEQ)
)
;

CREATE INDEX ix_RL00312 ON T_BBS_THREAD (
    SITE_CD                       ASC,
    USER_SEQ                      ASC,
    USER_CHAR_NO                  ASC
)
;

CREATE INDEX ix_BBS_THREAD02 ON T_BBS_THREAD (
    DEL_FLAG                      ASC
)
;

CREATE INDEX ix_BBS_THREAD01 ON T_BBS_THREAD (
    SITE_CD                       ASC,
    LAST_RES_WRITE_DATE           ASC
)
;

/*************************************************************************/
-- 掲示板ｽﾚｯﾄﾞｱｸｾｽ集計           テーブル定義
/*************************************************************************/
CREATE TABLE T_BBS_THREAD_ACCESS (
    SITE_CD             VARCHAR2(4)         NOT NULL,
    USER_SEQ            NUMBER(15,0)        NOT NULL,
    USER_CHAR_NO        VARCHAR2(2)         NOT NULL,
    BBS_THREAD_SEQ      NUMBER(15,0)        NOT NULL,
    REPORT_DAY          VARCHAR2(10)        NOT NULL,
    ACCESS_COUNT        NUMBER(7,0)         DEFAULT 0 NOT NULL,
    CONSTRAINT pk_T_BBS_THREAD_ACCESS  PRIMARY KEY (SITE_CD,
                                                    USER_SEQ,
                                                    USER_CHAR_NO,
                                                    BBS_THREAD_SEQ,
                                                    REPORT_DAY)
)
;

CREATE INDEX ix_RL00314 ON T_BBS_THREAD_ACCESS (
    SITE_CD                       ASC,
    USER_SEQ                      ASC,
    USER_CHAR_NO                  ASC,
    BBS_THREAD_SEQ                ASC
)
;

CREATE INDEX ix_BBS_THREAD01 ON T_BBS_THREAD_ACCESS (
    SITE_CD                       ASC,
    BBS_THREAD_SEQ                ASC,
    REPORT_DAY                    ASC
)
;

/*************************************************************************/
-- 掲示板ﾚｽ           テーブル定義
/*************************************************************************/
CREATE TABLE T_BBS_RES (
    SITE_CD             VARCHAR2(4)         NOT NULL,
    USER_SEQ            NUMBER(15,0)        NOT NULL,
    USER_CHAR_NO        VARCHAR2(2)         NOT NULL,
    BBS_THREAD_SEQ      NUMBER(15,0)        NOT NULL,
    BBS_THREAD_SUB_SEQ  NUMBER(6,0)         NOT NULL,
    BBS_RES_HANDLE_NM   VARCHAR2(60)        NULL,
    BBS_RES_USER_SEQ    NUMBER(15,0)        NULL,
    BBS_RES_USER_CHAR_NO VARCHAR2(2)         NULL,
    BBS_RES_DOC         VARCHAR2(4000)      NULL,
    POST_BBS_THREAD_SEQ NUMBER(15,0)        NULL,
    POST_THREAD_SUB_SEQ NUMBER(6,0)         NULL,
    DEL_FLAG            NUMBER(1,0)         DEFAULT 0 NOT NULL,
    CREATE_DATE         DATE                NULL,
    UPDATE_DATE         DATE                NULL,
    CONSTRAINT pk_T_BBS_RES         PRIMARY KEY (SITE_CD,
                                                 USER_SEQ,
                                                 USER_CHAR_NO,
                                                 BBS_THREAD_SEQ,
                                                 BBS_THREAD_SUB_SEQ)
)
;

CREATE INDEX ix_RL00313 ON T_BBS_RES (
    SITE_CD                       ASC,
    USER_SEQ                      ASC,
    USER_CHAR_NO                  ASC,
    BBS_THREAD_SEQ                ASC
)
;

CREATE INDEX ix_RL00315 ON T_BBS_RES (
    SITE_CD                       ASC,
    BBS_RES_USER_SEQ              ASC,
    BBS_RES_USER_CHAR_NO          ASC
)
;

CREATE INDEX ix_BBS_RES02 ON T_BBS_RES (
    DEL_FLAG                      ASC
)
;

CREATE INDEX ix_BBS_RES01 ON T_BBS_RES (
    SITE_CD                       ASC,
    BBS_THREAD_SEQ                ASC,
    BBS_THREAD_SUB_SEQ            ASC
)
;

ALTER TABLE T_BBS_THREAD ADD (
    CONSTRAINT f_RL00312 FOREIGN KEY (SITE_CD,
                                      USER_SEQ,
                                      USER_CHAR_NO)
       REFERENCES T_CAST_CHARACTER ON DELETE CASCADE
)
;

ALTER TABLE T_BBS_THREAD_ACCESS ADD (
    CONSTRAINT f_RL00314 FOREIGN KEY (SITE_CD,
                                      USER_SEQ,
                                      USER_CHAR_NO,
                                      BBS_THREAD_SEQ)
       REFERENCES T_BBS_THREAD ON DELETE CASCADE
)
;

ALTER TABLE T_BBS_RES ADD (
    CONSTRAINT f_RL00313 FOREIGN KEY (SITE_CD,
                                      USER_SEQ,
                                      USER_CHAR_NO,
                                      BBS_THREAD_SEQ)
       REFERENCES T_BBS_THREAD ON DELETE CASCADE
)
;

ALTER TABLE T_BBS_RES ADD (
    CONSTRAINT f_RL00315 FOREIGN KEY (SITE_CD,
                                      BBS_RES_USER_SEQ,
                                      BBS_RES_USER_CHAR_NO)
       REFERENCES T_CAST_CHARACTER ON DELETE CASCADE
)
;
ALTER TABLE T_BBS_THREAD ADD (
    BBS_THREAD_DOC1     VARCHAR2(4000)      NULL,
    BBS_THREAD_DOC2     VARCHAR2(4000)      NULL,
    BBS_THREAD_DOC3     VARCHAR2(4000)      NULL,
    BBS_THREAD_DOC4     VARCHAR2(4000)      NULL
);
ALTER TABLE T_BBS_RES ADD(
    BBS_RES_DOC1        VARCHAR2(4000)      NULL,
    BBS_RES_DOC2        VARCHAR2(4000)      NULL,
    BBS_RES_DOC3        VARCHAR2(4000)      NULL,
    BBS_RES_DOC4        VARCHAR2(4000)      NULL
);

ALTER TABLE T_BBS_THREAD DROP COLUMN BBS_THREAD_DOC;
ALTER TABLE T_BBS_RES DROP COLUMN BBS_RES_DOC;


ALTER TABLE T_BBS_THREAD ADD(
    TODAY_ACCESS_COUNT  NUMBER(7,0)         DEFAULT 0 NOT NULL
);

ALTER TABLE T_BBS_RES ADD(
    ANONYMOUS_FLAG      NUMBER(1,0)         DEFAULT 0 NOT NULL
);

CREATE SEQUENCE SEQ_BBS_THREAD
		START WITH 1 
		INCREMENT BY 1
		MINVALUE 1
		MAXVALUE 999999999999999
		NOCACHE
		ORDER
		CYCLE
;
