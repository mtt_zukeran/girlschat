/*************************************************************************/
-- ﾒｰﾙ不達履歴           テーブル定義
/*************************************************************************/
CREATE TABLE T_NG_MAIL_HISTORY (
    USER_SEQ            NUMBER(15,0)        NOT NULL,
    NG_MAIL_SEQ         NUMBER(15,0)        NOT NULL,
    NG_MAIL_DATE        DATE                NULL,
    EMAIL_ADDR          VARCHAR2(120)       NULL,
    REQUEST_TX_MAIL_SUBSEQ NUMBER(6,0)         NULL,
    REQUEST_TX_MAIL_SEQ NUMBER(15,0)        NULL,
    CONSTRAINT pk_T_NG_MAIL_HISTORY  PRIMARY KEY (USER_SEQ,
                                                  NG_MAIL_SEQ)
)
;

CREATE INDEX ix_RL00329 ON T_NG_MAIL_HISTORY (
    REQUEST_TX_MAIL_SUBSEQ        ASC,
    REQUEST_TX_MAIL_SEQ           ASC
)
;

ALTER TABLE T_NG_MAIL_HISTORY DROP CONSTRAINT f_RL00329;

ALTER TABLE T_NG_MAIL_HISTORY ADD (
    CONSTRAINT f_RL00329 FOREIGN KEY (REQUEST_TX_MAIL_SEQ,
                                      REQUEST_TX_MAIL_SUBSEQ)
       REFERENCES T_REQ_TX_MAIL_DTL ON DELETE CASCADE
)
;


CREATE SEQUENCE SEQ_NG_MAIL
       START WITH 1 
       INCREMENT BY 1
       MINVALUE 1
       MAXVALUE 999999999999999
       NOCACHE
       ORDER
       CYCLE
;

UPDATE T_ERROR SET ERROR_DTL = '発信先番号が存在しない為、接続できませんでした' ,ERROR_ABBR = '発信先番号が存在しない' WHERE ERROR_CD = 'CR24';

COMMIT;

ALTER TABLE T_LOGIN_USER ADD (ADMIN_WAIT_FLAG NUMBER(1) DEFAULT 0 NOT NULL);
