CREATE TABLE T_WAIT_SCHEDULE (
	SITE_CD					VARCHAR2(4)	NOT NULL,
	USER_SEQ				NUMBER(15,0)	NOT NULL,
	USER_CHAR_NO			VARCHAR2(2)	NOT NULL,
	WAIT_SCHEDULE_SEQ		NUMBER(15,0)	NOT NULL,
	WAIT_START_TIME			DATE			NOT NULL,
	UPDATE_DATE				DATE			NOT NULL,
	REVISION_NO				NUMBER(15,0)	DEFAULT 0 NOT NULL,
	CONSTRAINT pk_T_WAIT_SCHEDULE PRIMARY KEY(SITE_CD,USER_SEQ,USER_CHAR_NO,WAIT_SCHEDULE_SEQ)
);

CREATE SEQUENCE SEQ_WAIT_SCHEDULE
	START WITH 1000
	INCREMENT BY 1
	MINVALUE 1000
	MAXVALUE 999999999999999
	NOCACHE
	ORDER
	CYCLE
;

ALTER TABLE T_SITE_MANAGEMENT_EX ADD(
	TX_MAIL_WAIT_START_PREV_MIN	NUMBER(30,0)	DEFAULT 60 NOT NULL
);