ALTER TABLE T_AD ADD(
	AD_MENU					VARCHAR2(2)	DEFAULT '00' NOT NULL	,
	SEX_CD					VARCHAR2(1)	DEFAULT '-' NOT NULL	,
	AD_START_DATE			DATE			NULL					,
	AD_END_DATE				DATE			NULL					,
	PUBLISH_START_DATE		DATE			NULL					,
	PUBLISH_END_DATE		DATE			NULL					,
	AD_COST					NUMBER(10,0)	DEFAULT 0 NOT NULL
);


CREATE TABLE T_AD_DATA_USER_MONTHLY (
	SITE_CD						VARCHAR2(4)	NOT NULL,
	AD_CD						VARCHAR2(32)	NOT NULL,
	AD_COST						NUMBER(10,0)	DEFAULT 0 NOT NULL,
	ACCESS_COUNT				NUMBER(10,0)	DEFAULT 0 NOT NULL,
	REGIST_COUNT				NUMBER(5,0)	DEFAULT 0 NOT NULL,
	REGIST_COUNT_AD				NUMBER(5,0)	DEFAULT 0 NOT NULL,
	LOGIN_COUNT					NUMBER(10,0)	DEFAULT 0 NOT NULL,
	CHARGE_POINT_USER_COUNT		NUMBER(5,0)	DEFAULT 0 NOT NULL,
	START_RECEIPT_USER_COUNT	NUMBER(5,0)	DEFAULT 0 NOT NULL,
	ENT_MONTH_RECEIPT_COUNT		NUMBER(5,0)	DEFAULT 0 NOT NULL,
	ENT_MONTH_RECEIPT_AMT		NUMBER(10,0)	DEFAULT 0 NOT NULL,
	REPEAT_RECEIPT_COUNT		NUMBER(5,0)	DEFAULT 0 NOT NULL,
	REPEAT_RECEIPT_AMT			NUMBER(10,0)	DEFAULT 0 NOT NULL,
	AGGR_ACCESS_COUNT			NUMBER(10,0)	DEFAULT 0 NOT NULL,
	AGGR_REGIST_COUNT			NUMBER(10,0)	DEFAULT 0 NOT NULL,
	AGGR_REGIST_COUNT_AD		NUMBER(10,0)	DEFAULT 0 NOT NULL,
	AGGR_CHARGE_POINT_USER		NUMBER(10,0)	DEFAULT 0 NOT NULL,
	AGGR_REPEAT_RECEIPT_USER	NUMBER(10,0)	DEFAULT 0 NOT NULL,
	AGGR_RECEIPT_USER_COUNT		NUMBER(10,0)	DEFAULT 0 NOT NULL,
	AGGR_RECEIPT_AMT			NUMBER(10,0)	DEFAULT 0 NOT NULL,
	AGGR_AD_COST				NUMBER(10,0)	DEFAULT 0 NOT NULL,
	REPORT_MONTH				VARCHAR2(7)	NOT NULL,
	CONSTRAINT pk_T_AD_DATA_USER_MONTHLY PRIMARY KEY (SITE_CD,AD_CD,REPORT_MONTH)
);


CREATE TABLE T_AD_DATA_USER_DAILY (
	SITE_CD						VARCHAR2(4)	NOT NULL,
	AD_CD						VARCHAR2(32)	NOT NULL,
	AD_COST						NUMBER(10,0)	DEFAULT 0 NOT NULL,
	ACCESS_COUNT				NUMBER(10,0)	DEFAULT 0 NOT NULL,
	REGIST_COUNT				NUMBER(5,0)	DEFAULT 0 NOT NULL,
	REGIST_COUNT_AD				NUMBER(5,0)	DEFAULT 0 NOT NULL,
	LOGIN_COUNT					NUMBER(10,0)	DEFAULT 0 NOT NULL,
	CHARGE_POINT_USER_COUNT		NUMBER(5,0)	DEFAULT 0 NOT NULL,
	START_RECEIPT_USER_COUNT	NUMBER(5,0)	DEFAULT 0 NOT NULL,
	ENT_MONTH_RECEIPT_COUNT		NUMBER(5,0)	DEFAULT 0 NOT NULL,
	ENT_MONTH_RECEIPT_AMT		NUMBER(10,0)	DEFAULT 0 NOT NULL,
	REPEAT_RECEIPT_COUNT		NUMBER(5,0)	DEFAULT 0 NOT NULL,
	REPEAT_RECEIPT_AMT			NUMBER(10,0)	DEFAULT 0 NOT NULL,
	AGGR_RECEIPT_USER_COUNT		NUMBER(10,0)	DEFAULT 0 NOT NULL,
	AGGR_RECEIPT_AMT			NUMBER(10,0)	DEFAULT 0 NOT NULL,
	AGGR_AD_COST				NUMBER(10,0)	DEFAULT 0 NOT NULL,
	REPORT_DAY					VARCHAR2(10)	NOT NULL,
	CONSTRAINT pk_T_AD_DATA_USER_DAILY PRIMARY KEY (SITE_CD,AD_CD,REPORT_DAY)
);


CREATE TABLE T_SALES_BY_ENT_MONTHLY (
	SITE_CD			VARCHAR2(4)	NOT NULL,
	RECEIPT_COUNT	NUMBER(10,0)	DEFAULT 0 NOT NULL,
	RECEIPT_AMT		NUMBER(10,0)	DEFAULT 0 NOT NULL,
	ENT_MONTH		VARCHAR2(7)	NOT NULL,
	REPORT_MONTH	VARCHAR2(7)	NOT NULL,
	CONSTRAINT pk_T_SALES_BY_ENT_MONTHLY PRIMARY KEY (SITE_CD,ENT_MONTH,REPORT_MONTH)
);


CREATE TABLE T_SALES_BY_ENT_DAILY (
	SITE_CD			VARCHAR2(4)	NOT NULL,
	RECEIPT_COUNT	NUMBER(10,0)	DEFAULT 0 NOT NULL,
	RECEIPT_AMT		NUMBER(10,0)	DEFAULT 0 NOT NULL,
	ENT_MONTH		VARCHAR2(7)	NOT NULL,
	REPORT_DAY		VARCHAR2(10)	NOT NULL,
	CONSTRAINT pk_T_SALES_BY_ENT_DAILY PRIMARY KEY (SITE_CD,ENT_MONTH,REPORT_DAY)
);


CREATE TABLE T_AD_DATA_CAST_MONTHLY (
	SITE_CD						VARCHAR2(4)	NOT NULL,
	AD_CD						VARCHAR2(32)	NOT NULL,
	AD_COST						NUMBER(10,0)	DEFAULT 0 NOT NULL,
	ACCESS_COUNT				NUMBER(10,0)	DEFAULT 0 NOT NULL,
	REGIST_COUNT				NUMBER(5,0)	DEFAULT 0 NOT NULL,
	REGIST_COUNT_AD				NUMBER(5,0)	DEFAULT 0 NOT NULL,
	LOGIN_COUNT					NUMBER(10,0)	DEFAULT 0 NOT NULL,
	GET_POINT_CAST_COUNT		NUMBER(5,0)	DEFAULT 0 NOT NULL,
	START_PAYMENT_CAST_COUNT	NUMBER(5,0)	DEFAULT 0 NOT NULL,
	ENT_MONTH_PAYMENT_COUNT		NUMBER(5,0)	DEFAULT 0 NOT NULL,
	ENT_MONTH_PAYMENT_AMT		NUMBER(10,0)	DEFAULT 0 NOT NULL,
	REPEAT_PAYMENT_COUNT		NUMBER(5,0)	DEFAULT 0 NOT NULL,
	REPEAT_PAYMENT_AMT			NUMBER(10,0)	DEFAULT 0 NOT NULL,
	AGGR_ACCESS_COUNT			NUMBER(10,0)	DEFAULT 0 NOT NULL,
	AGGR_REGIST_COUNT			NUMBER(10,0)	DEFAULT 0 NOT NULL,
	AGGR_REGIST_COUNT_AD		NUMBER(10,0)	DEFAULT 0 NOT NULL,
	AGGR_GET_POINT_CAST			NUMBER(10,0)	DEFAULT 0 NOT NULL,
	AGGR_REPEAT_PAYMENT_CAST	NUMBER(10,0)	DEFAULT 0 NOT NULL,
	AGGR_PAYMENT_CAST_COUNT		NUMBER(10,0)	DEFAULT 0 NOT NULL,
	AGGR_PAYMENT_AMT			NUMBER(10,0)	DEFAULT 0 NOT NULL,
	AGGR_AD_COST				NUMBER(10,0)	DEFAULT 0 NOT NULL,
	REPORT_MONTH				VARCHAR2(7)	NOT NULL,
	CONSTRAINT pk_T_AD_DATA_CAST_MONTHLY PRIMARY KEY (SITE_CD,AD_CD,REPORT_MONTH)
);


CREATE TABLE T_AD_DATA_CAST_DAILY (
	SITE_CD						VARCHAR2(4)	NOT NULL,
	AD_CD						VARCHAR2(32)	NOT NULL,
	AD_COST						NUMBER(10,0)	DEFAULT 0 NOT NULL,
	ACCESS_COUNT				NUMBER(10,0)	DEFAULT 0 NOT NULL,
	REGIST_COUNT				NUMBER(5,0)	DEFAULT 0 NOT NULL,
	REGIST_COUNT_AD				NUMBER(5,0)	DEFAULT 0 NOT NULL,
	LOGIN_COUNT					NUMBER(10,0)	DEFAULT 0 NOT NULL,
	GET_POINT_CAST_COUNT		NUMBER(5,0)	DEFAULT 0 NOT NULL,
	START_PAYMENT_CAST_COUNT	NUMBER(5,0)	DEFAULT 0 NOT NULL,
	ENT_MONTH_PAYMENT_COUNT		NUMBER(5,0)	DEFAULT 0 NOT NULL,
	ENT_MONTH_PAYMENT_AMT		NUMBER(10,0)	DEFAULT 0 NOT NULL,
	REPEAT_PAYMENT_COUNT		NUMBER(5,0)	DEFAULT 0 NOT NULL,
	REPEAT_PAYMENT_AMT			NUMBER(10,0)	DEFAULT 0 NOT NULL,
	AGGR_PAYMENT_CAST_COUNT		NUMBER(10,0)	DEFAULT 0 NOT NULL,
	AGGR_PAYMENT_AMT			NUMBER(10,0)	DEFAULT 0 NOT NULL,
	AGGR_AD_COST				NUMBER(10,0)	DEFAULT 0 NOT NULL,
	REPORT_DAY					VARCHAR2(10)	NOT NULL,
	CONSTRAINT pk_T_AD_DATA_CAST_DAILY PRIMARY KEY (SITE_CD,AD_CD,REPORT_DAY)
);


CREATE TABLE T_PAYMENT_BY_ENT_MONTHLY (
	SITE_CD			VARCHAR2(4)	NOT NULL,
	PAYMENT_COUNT	NUMBER(10,0)	DEFAULT 0 NOT NULL,
	PAYMENT_AMT		NUMBER(10,0)	DEFAULT 0 NOT NULL,
	ENT_MONTH		VARCHAR2(7)	NOT NULL,
	REPORT_MONTH	VARCHAR2(7)	NOT NULL,
	CONSTRAINT pk_T_PAYMENT_BY_ENT_MONTHLY PRIMARY KEY (SITE_CD,ENT_MONTH,REPORT_MONTH)
);


CREATE TABLE T_PAYMENT_BY_ENT_DAILY (
	SITE_CD			VARCHAR2(4)	NOT NULL,
	PAYMENT_COUNT	NUMBER(10,0)	DEFAULT 0 NOT NULL,
	PAYMENT_AMT		NUMBER(10,0)	DEFAULT 0 NOT NULL,
	ENT_MONTH		VARCHAR2(7)	NOT NULL,
	REPORT_DAY		VARCHAR2(10)	NOT NULL,
	CONSTRAINT pk_T_PAYMENT_BY_ENT_DAILY PRIMARY KEY (SITE_CD,ENT_MONTH,REPORT_DAY)
);