ALTER TABLE T_SITE_MANAGEMENT ADD (
    PF_TALK_NOTIFY_MASK NUMBER(1,0)         DEFAULT 0 NOT NULL
);

DROP TABLE T_CAST_CHARGE_TEMP;

/*************************************************************************/
-- ｷｬｽﾄ課金設定一時保存           テーブル定義
/*************************************************************************/
CREATE TABLE T_CAST_CHARGE_TEMP (
    SITE_CD             VARCHAR2(4)         NOT NULL,
    CHARGE_TYPE         VARCHAR2(2)         NOT NULL,
    ACT_CATEGORY_SEQ    NUMBER(15,0)        NOT NULL,
    CAST_CHARGE_TEMP_CD VARCHAR2(2)         NOT NULL,
    CHARGE_POINT_NORMAL NUMBER(5,0)         DEFAULT 0 NOT NULL,
    CHARGE_POINT_NO_RECEIPT NUMBER(5,0)         DEFAULT 0 NOT NULL,
    CHARGE_POINT_NORMAL2 NUMBER(5,0)         DEFAULT 0 NOT NULL,
    CHARGE_POINT_NO_RECEIPT2 NUMBER(5,0)         DEFAULT 0 NOT NULL,
    CHARGE_POINT_NORMAL3 NUMBER(5,0)         DEFAULT 0 NOT NULL,
    CHARGE_POINT_NO_RECEIPT3 NUMBER(5,0)         DEFAULT 0 NOT NULL,
    CHARGE_POINT_NORMAL4 NUMBER(5,0)         DEFAULT 0 NOT NULL,
    CHARGE_POINT_NO_RECEIPT4 NUMBER(5,0)         DEFAULT 0 NOT NULL,
    CHARGE_POINT_NORMAL5 NUMBER(5,0)         DEFAULT 0 NOT NULL,
    CHARGE_POINT_NO_RECEIPT5 NUMBER(5,0)         DEFAULT 0 NOT NULL,
    CHARGE_UNIT_SEC     NUMBER(3,0)         DEFAULT 0 NOT NULL,
    FREE_DIAL_FEE       NUMBER(5,0)         DEFAULT 0 NOT NULL,
    REVISION_NO         NUMBER(15,0)        DEFAULT 0 NOT NULL,
    UPDATE_DATE         DATE                NULL,
    CONSTRAINT pk_T_CAST_CHARGE_TEMP  PRIMARY KEY (SITE_CD,
                                                   CHARGE_TYPE,
                                                   ACT_CATEGORY_SEQ,
                                                   CAST_CHARGE_TEMP_CD)
)
;

CREATE INDEX ix_RL00470 ON T_CAST_CHARGE_TEMP (
    CHARGE_TYPE                   ASC
)
;

CREATE INDEX ix_RL00471 ON T_CAST_CHARGE_TEMP (
    SITE_CD                       ASC,
    ACT_CATEGORY_SEQ              ASC
)
;

CREATE INDEX ix_RL00479 ON T_CAST_CHARGE_TEMP (
    SITE_CD                       ASC,
    ACT_CATEGORY_SEQ              ASC,
    CAST_CHARGE_TEMP_CD           ASC
)
;

DROP TABLE T_CAST_CHARGE_TEMP_BIND;

/*************************************************************************/
-- ｷｬｽﾄ課金設定一時保存適用条件           テーブル定義
/*************************************************************************/
CREATE TABLE T_CAST_CHARGE_TEMP_CONDITION (
    SITE_CD             VARCHAR2(4)         NOT NULL,
    CAST_CHARGE_TEMP_CD VARCHAR2(2)         NOT NULL,
    CAST_CHARGE_TEMP_CD_NM VARCHAR2(256)       NULL,
    APPLICATION_DATE    DATE                NULL,
    NA_FLAG             NUMBER(1,0)         DEFAULT 0 NOT NULL,
    UPDATE_DATE         DATE                NULL,
    REVISION_NO         NUMBER(15,0)        DEFAULT 0 NOT NULL,
    CONSTRAINT pk_T_CAST_CHARGE_TEMP_CONDITIO  PRIMARY KEY (SITE_CD,
                                                            CAST_CHARGE_TEMP_CD)
)
;

CREATE INDEX ix_RL00477 ON T_CAST_CHARGE_TEMP_CONDITION (
    SITE_CD                       ASC
)
;

/*************************************************************************/
-- 外部キー定義
/*************************************************************************/
ALTER TABLE T_CAST_CHARGE_TEMP ADD (
    CONSTRAINT f_RL00470 FOREIGN KEY (CHARGE_TYPE)
       REFERENCES T_CHARGE_TYPE
)
;

ALTER TABLE T_CAST_CHARGE_TEMP ADD (
    CONSTRAINT f_RL00471 FOREIGN KEY (SITE_CD,
                                      ACT_CATEGORY_SEQ)
       REFERENCES T_ACT_CATEGORY
)
;

ALTER TABLE T_CAST_CHARGE_TEMP ADD (
    CONSTRAINT f_RL00479 FOREIGN KEY (SITE_CD,
                                      CAST_CHARGE_TEMP_CD)
       REFERENCES T_CAST_CHARGE_TEMP_CONDITION ON DELETE CASCADE
)
;

ALTER TABLE T_CAST_CHARGE_TEMP_CONDITION ADD (
    CONSTRAINT f_RL00477 FOREIGN KEY (SITE_CD)
       REFERENCES T_SITE
)
;

/*************************************************************************/
-- 会員課金追加設定           テーブル定義
/*************************************************************************/
CREATE TABLE T_USER_CHARGE_ADD (
    SITE_CD             VARCHAR2(4)         NOT NULL,
    USER_RANK           VARCHAR2(1)         NOT NULL,
    CHARGE_TYPE         VARCHAR2(2)         NOT NULL,
    ACT_CATEGORY_SEQ    NUMBER(15,0)        NOT NULL,
    CAST_RANK           VARCHAR2(1)         NOT NULL,
    CHARGE_POINT_NORMAL NUMBER(5,0)         DEFAULT 0 NOT NULL,
    CHARGE_POINT_NEW    NUMBER(5,0)         DEFAULT 0 NOT NULL,
    CHARGE_POINT_NO_RECEIPT NUMBER(5,0)         DEFAULT 0 NOT NULL,
    CHARGE_POINT_NO_USE NUMBER(5,0)         DEFAULT 0 NOT NULL,
    CHARGE_POINT_FREE_DIAL NUMBER(5,0)         DEFAULT 0 NOT NULL,
    CHARGE_POINT_WHITE_PLAN NUMBER(5,0)         DEFAULT 0 NOT NULL,
    CHARGE_UNIT_SEC     NUMBER(3,0)         DEFAULT 0 NOT NULL,
    UPDATE_DATE         DATE                NULL,
    REVISION_NO         NUMBER(15,0)        DEFAULT 0 NOT NULL,
    USER_CHARGE_ADD_CD  VARCHAR2(2)         NOT NULL,
    CONSTRAINT pk_T_USER_CHARGE_ADD  PRIMARY KEY (SITE_CD,
                                                  USER_RANK,
                                                  CHARGE_TYPE,
                                                  ACT_CATEGORY_SEQ,
                                                  CAST_RANK,
                                                  USER_CHARGE_ADD_CD)
)
;

CREATE INDEX ix_RL00472 ON T_USER_CHARGE_ADD (
    SITE_CD                       ASC,
    USER_RANK                     ASC
)
;

CREATE INDEX ix_RL00473 ON T_USER_CHARGE_ADD (
    SITE_CD                       ASC,
    ACT_CATEGORY_SEQ              ASC
)
;

CREATE INDEX ix_RL00474 ON T_USER_CHARGE_ADD (
    CHARGE_TYPE                   ASC
)
;

CREATE INDEX ix_RL00476 ON T_USER_CHARGE_ADD (
    SITE_CD                       ASC,
    USER_CHARGE_ADD_CD            ASC
)
;

/*************************************************************************/
-- 会員課金追加設定適応条件           テーブル定義
/*************************************************************************/
CREATE TABLE T_USER_CHARGE_ADD_CONDITION (
    SITE_CD             VARCHAR2(4)         NOT NULL,
    USER_CHARGE_ADD_CD  VARCHAR2(2)         NOT NULL,
    USER_CHARGE_ADD_CD_NM VARCHAR2(256)       NULL,
    LAST_RECEIPT_DATE_START DATE                NULL,
    LAST_RECEIPT_DATE_END DATE                NULL,
    LAST_RECEIPT_USE_FLAG NUMBER(1,0)         DEFAULT 0 NOT NULL,
    RECEIPT_SUM_START_DATE DATE                NULL,
    RECEIPT_SUM_SPECIFIED_MIN NUMBER(8,0)         DEFAULT 0 NOT NULL,
    RECEIPT_SUM_SPECIFIED_MAX NUMBER(8,0)         DEFAULT 0 NOT NULL,
    AF_CONTAIN_SPECIFIED_FLAG NUMBER(1,0)         DEFAULT 0 NOT NULL,
    RECEIPT_SUM_SPECIFIED_USE_FLAG NUMBER(1,0)         DEFAULT 0 NOT NULL,
    RECEIPT_SUM_MIN     NUMBER(8,0)         DEFAULT 0 NOT NULL,
    RECEIPT_SUM_MAX     NUMBER(8,0)         DEFAULT 0 NOT NULL,
    AF_CONTAIN_FLAG     NUMBER(1,0)         DEFAULT 0 NOT NULL,
    RECEIPT_SUM_USE_FLAG NUMBER(1,0)         DEFAULT 0 NOT NULL,
    NA_FLAG             NUMBER(1,0)         DEFAULT 0 NOT NULL,
    APPLICATION_DATE    DATE                NULL,
    UPDATE_DATE         DATE                NULL,
    REVISION_NO         NUMBER(15,0)        DEFAULT 0 NOT NULL,
    CONSTRAINT pk_T_USER_CHARGE_ADD_CONDITION  PRIMARY KEY (SITE_CD,
                                                            USER_CHARGE_ADD_CD)
)
;

CREATE INDEX ix_RL00475 ON T_USER_CHARGE_ADD_CONDITION (
    SITE_CD                       ASC
)
;

/*************************************************************************/
-- 外部キー定義
/*************************************************************************/
ALTER TABLE T_USER_CHARGE_ADD ADD (
    CONSTRAINT f_RL00472 FOREIGN KEY (SITE_CD,
                                      USER_RANK)
       REFERENCES T_USER_RANK
)
;

ALTER TABLE T_USER_CHARGE_ADD ADD (
    CONSTRAINT f_RL00473 FOREIGN KEY (SITE_CD,
                                      ACT_CATEGORY_SEQ)
       REFERENCES T_ACT_CATEGORY
)
;

ALTER TABLE T_USER_CHARGE_ADD ADD (
    CONSTRAINT f_RL00474 FOREIGN KEY (CHARGE_TYPE)
       REFERENCES T_CHARGE_TYPE
)
;

ALTER TABLE T_USER_CHARGE_ADD DROP CONSTRAINT f_RL00476;

ALTER TABLE T_USER_CHARGE_ADD ADD (
    CONSTRAINT f_RL00476 FOREIGN KEY (SITE_CD,
                                      USER_CHARGE_ADD_CD)
       REFERENCES T_USER_CHARGE_ADD_CONDITION ON DELETE CASCADE
)
;

ALTER TABLE T_USER_CHARGE_ADD_CONDITION ADD (
    CONSTRAINT f_RL00475 FOREIGN KEY (SITE_CD)
       REFERENCES T_SITE
)
;



ALTER TABLE T_USER ADD(
    CROSMILE_LAST_USED_VERSION VARCHAR2(12) NULL
);

ALTER TABLE T_USER ADD(
    TALK_SMART_DIRECT_NOT_MONITOR NUMBER(1,0)         DEFAULT 0 NOT NULL
);

ALTER TABLE T_CAST_CHARGE ADD(
    CAST_USE_TALK_APP_ADD_POINT NUMBER(5,0)         DEFAULT 0 NOT NULL,
    MAN_USE_TALK_APP_ADD_POINT NUMBER(5,0)         DEFAULT 0 NOT NULL
);

ALTER TABLE T_CAST_CHARGE_TEMP ADD(
    CAST_USE_TALK_APP_ADD_POINT NUMBER(5,0)         DEFAULT 0 NOT NULL,
    MAN_USE_TALK_APP_ADD_POINT NUMBER(5,0)         DEFAULT 0 NOT NULL
);

ALTER TABLE T_USER_CHARGE_ADD ADD(
    CHARGE_CAST_TALK_APP_POINT NUMBER(5,0)         DEFAULT 0 NOT NULL,
    CHARGE_MAN_TALK_APP_POINT NUMBER(5,0)         DEFAULT 0 NOT NULL
);

ALTER TABLE T_USER_CHARGE ADD(
    CHARGE_CAST_TALK_APP_POINT NUMBER(5,0)         DEFAULT 0 NOT NULL,
    CHARGE_MAN_TALK_APP_POINT NUMBER(5,0)         DEFAULT 0 NOT NULL
);


ALTER TABLE T_USER_HISTORY MODIFY(
	MOBILE_TERMINAL_NM VARCHAR2(512)
);

ALTER TABLE T_SYS ADD(
    XSM_VOICE_PHONE_LINE_FLAG NUMBER(1,0)         DEFAULT 0 NOT NULL
);
ALTER TABLE T_SYS DROP COLUMN VOICE_TALK_USE_CROSMILE_FLAG;

