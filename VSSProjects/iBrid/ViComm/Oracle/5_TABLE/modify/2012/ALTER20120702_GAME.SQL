/*************************************************************************/
--   DB_A5 for ORACLE9
--                                   作成者 ataba
--                                   作成日付 2012/07/02  時間 09:28
/*************************************************************************/

/*************************************************************************/
-- 抽選設定非ｺﾝﾌﾟ           テーブル定義
/*************************************************************************/
CREATE TABLE T_LOTTERY_NOT_COMP (
    SITE_CD                     VARCHAR2(4)         NOT NULL,
    LOTTERY_SEQ	                NUMBER(15,0)        NOT NULL,
    SEX_CD                      VARCHAR2(1)         NULL,
    FIRST_BUY_FREE_START_DATE 	DATE          		NULL,
    FIRST_BUY_FREE_END_DATE 	DATE            	NULL,
    PUBLISH_START_DATE  	    DATE                NULL,
    PUBLISH_END_DATE    		DATE                NULL,
    REVISION_NO         		NUMBER(15,0)        DEFAULT 0 NOT NULL,
    UPDATE_DATE         		DATE               	NULL,
    PRICE          				NUMBER(10,0)        NULL,
    PRICE_1          			NUMBER(10,0)        NULL,
    PRICE_2    					NUMBER(10,0)        NULL,
    PRICE_3    					NUMBER(10,0)        NULL,
    LOTTERY_NM                	VARCHAR2(512)		NULL,
    LOTTERY_COUNT_1         	NUMBER(10,0)      	NULL,
    LOTTERY_COUNT_2         	NUMBER(10,0)      	NULL,
    LOTTERY_COUNT_3         	NUMBER(10,0)      	NULL,
    LOTTERY_SET_FLG_1			NUMBER(1,0)         DEFAULT 0 NOT NULL,
    LOTTERY_SET_FLG_2			NUMBER(1,0)         DEFAULT 0 NOT NULL,
    LOTTERY_SET_FLG_3			NUMBER(1,0)         DEFAULT 0 NOT NULL,
    PREMIUM_COUNT_1             NUMBER(10,0)      	NULL,
    PREMIUM_COUNT_2             NUMBER(10,0)      	NULL,
    PREMIUM_COUNT_3             NUMBER(10,0)      	NULL,
    CONSTRAINT pk_T_LOTTERY_NOT_COMP       PRIMARY KEY (SITE_CD,
                                                 		LOTTERY_SEQ)
)
;
/*************************************************************************/
-- 抽選獲得アイテム非ｺﾝﾌﾟ           テーブル定義
/*************************************************************************/
CREATE TABLE T_LOTTERY_GET_ITEM_NOT_COMP (
	SITE_CD         	VARCHAR2(4)         	NOT NULL,
	LOTTERY_SEQ       	NUMBER(15,0)        	NOT NULL,
    GAME_ITEM_SEQ       NUMBER(15,0)        	NOT NULL,
    RATE                NUMBER(3,0)         	DEFAULT 0 NOT NULL,
    PM_RATE        		NUMBER(3,0)         	DEFAULT 0 NOT NULL,
    PM_ITEM_FLAG   		NUMBER(1,0)         	DEFAULT 0 NOT NULL,
	PICKUP_FLAG			NUMBER(1,0)         	DEFAULT 0 NOT NULL,
    REVISION_NO         NUMBER(15,0)        	DEFAULT 0 NOT NULL,
    UPDATE_DATE         DATE                	NULL,
    CONSTRAINT pk_T_LOTTERY_GET_ITEM_NOT_COMP  	PRIMARY KEY (SITE_CD,
                                               				 LOTTERY_SEQ,
                                                   			 GAME_ITEM_SEQ)
)
;
/*************************************************************************/
-- ガチャ獲得履歴           テーブル定義
/*************************************************************************/
CREATE TABLE T_SET_LOTTERY_NOT_COMP_LOG (
	SITE_CD							VARCHAR2(4)		NOT NULL,
	USER_SEQ						NUMBER(15,0)	NOT NULL,
	USER_CHAR_NO					VARCHAR2(2)		NOT NULL,
	LOTTERY_SEQ						NUMBER(15,0)	NOT NULL,
	SET_LOTTERY_ITEM_GET_LOG_SEQ	NUMBER(15,0)	NOT NULL,
	LOTTERY_SET_FLAG				NUMBER(1,0)     DEFAULT 0 NOT NULL,
	CREATE_DATE						DATE			NULL,
 	CONSTRAINT pk_T_SET_LOTTERY_NOT_COMP_LOG PRIMARY KEY (SITE_CD,
													   	  USER_SEQ,
													      USER_CHAR_NO,
													      LOTTERY_SEQ,
													      SET_LOTTERY_ITEM_GET_LOG_SEQ)
)
;
/*************************************************************************/
-- 抽選ｱｲﾃﾑ獲得履歴非ｺﾝﾌﾟ           テーブル定義
/*************************************************************************/
CREATE TABLE T_LOTTERY_ITEM_GET_LOG_NCP (
	SITE_CD                    		VARCHAR2(4)  	NOT NULL,
    USER_SEQ 						NUMBER(15,0)    NOT NULL,
   	USER_CHAR_NO        			VARCHAR2(2)     NOT NULL,
	LOTTERY_SEQ 					NUMBER(15,0)    NOT NULL,
    LOTTERY_ITEM_GET_LOG_SEQ 		NUMBER(15,0)    NOT NULL,
   	SET_LOTTERY_ITEM_GET_LOG_SEQ 	NUMBER(15,0)    NOT NULL,
	GAME_ITEM_SEQ       			NUMBER(15,0)    NULL,
	GAME_ITEM_COUNT      			NUMBER(15,0)    DEFAULT 0 NOT NULL,
    CONSTRAINT pk_T_LOTTERY_ITEM_GET_LOG_NCP  PRIMARY KEY (SITE_CD,
                                                           USER_SEQ,
                                                           USER_CHAR_NO,
                                                           LOTTERY_SEQ,
                                                           LOTTERY_ITEM_GET_LOG_SEQ)
)
;