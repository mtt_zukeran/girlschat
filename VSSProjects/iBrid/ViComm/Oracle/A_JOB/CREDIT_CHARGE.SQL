/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: バックグランド
--  Title			: バッチ制御
--	Progaram ID		: CREDIT_CHARGE
--	Compile Turn	: 0
--  Creation Date	: 08.10.21
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE CREDIT_CHARGE
IS
	CURSOR CS_SITE IS SELECT SITE_CD FROM T_SITE
		WHERE
			USED_CHARGE_SETTLE_FLAG  != 0;

	BUF_STATUS VARCHAR2(10);

	PGM_NM 	CONSTANT VARCHAR(255)	:= 'CREDIT_CHARGE';

BEGIN

	APP_COMM.TRACE(APP_COMM.TRACE_INFO,PGM_NM || ' START');

	FOR REC_SITE IN CS_SITE LOOP
		EXEC_CREDIT_MEASURED_RATE(REC_SITE.SITE_CD,BUF_STATUS);
		COMMIT;
	END LOOP;

	APP_COMM.TRACE(APP_COMM.TRACE_INFO,PGM_NM || ' END');

EXCEPTION
	WHEN OTHERS THEN

		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);

		BUF_STATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END CREDIT_CHARGE;
/
SHOW ERROR;
