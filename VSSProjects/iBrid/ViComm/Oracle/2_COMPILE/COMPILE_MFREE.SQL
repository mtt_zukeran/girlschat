spool c:\VICOMM-MFREE.txt

CONNECT VICOMM_MFREE/VICOMM_MFREE@VICOMM_MFREE

@K:\VSSProjects\iBrid\ViComm\Oracle\2_COMPILE\COMPILE_VICOMM.SQL

/*----------------------------------------------*/
/* Setup Records								*/
/*----------------------------------------------*/
@K:\VssProjects\iBrid\ViComm\Oracle\9_RECORD\MFREE\T_CODE_DTL;
@K:\VssProjects\iBrid\ViComm\Oracle\9_RECORD\MFREE\T_OK_PLAY_LIST;
@K:\VssProjects\iBrid\ViComm\Oracle\9_RECORD\MFREE\T_MAIL_TYPE;
@K:\VssProjects\iBrid\ViComm\Oracle\9_RECORD\MFREE\T_MAN_ATTR;
/*----------------------------------------------*/
/* Setup Programs								*/
/*----------------------------------------------*/
@K:\VSSProjects\iBrid\ViComm\Oracle\9_RECORD\COMMON\T_PROGRAM.SQL
/*----------------------------------------------*/
/* Setup Error									*/
/*----------------------------------------------*/
@K:\VSSProjects\iBrid\ViComm\Oracle\9_RECORD\COMMON\T_ERROR;
@K:\VSSProjects\iBrid\ViComm\Oracle\9_RECORD\COMMON\T_ERROR_Z001;
@K:\VSSProjects\iBrid\ViComm\Oracle\9_RECORD\COMMON\COPY_ERROR.SQL;

/*----------------------------------------------*/
/* 機能設定										*/
/*----------------------------------------------*/
@K:\VssProjects\iBrid\ViComm\Oracle\9_RECORD\MFREE\T_MANAGE_COMPANY;

/*----------------------------------------------*/
/* メニュー設定									*/
/*----------------------------------------------*/
DELETE T_SITEMAP;
@K:VSSProjects\iBrid\ViComm\Oracle\9_RECORD\COMMON\T_SITE_MAP_SYS_OWNER;
@K:VSSProjects\iBrid\ViComm\Oracle\9_RECORD\COMMON\T_SITE_MAP_SITE_OWNER;

DELETE T_SITEMAP WHERE MENU_TITLE='MAQIAアフィリエイト管理';
DELETE T_SITEMAP WHERE MENU_TITLE='商品管理';
DELETE T_SITEMAP WHERE MENU_TITLE='期間別商品購入集計';
DELETE T_SITEMAP WHERE MENU_TITLE='商品別購入履歴';

INSERT INTO T_SITEMAP(MANAGE_COMPANY_CD,ADMIN_TYPE,HIERARCHY,MENU_TITLE)VALUES('A1','8',1,'男性メッセージ管理'				);
INSERT INTO T_SITEMAP(MANAGE_COMPANY_CD,ADMIN_TYPE,HIERARCHY,MENU_TITLE)VALUES('A1','8',1,'出演者メッセージ管理'			);


/*----------------------------------------------*/
/* メールオブジェクト設定						*/
/*----------------------------------------------*/
UPDATE
	T_SITE
SET
	STAMP_FILE_NM	= NULL	,
	PIC_SIZE_LARGE	= 200	,
	PIC_SIZE_SMALL	= 80	,
	SCALE_TYPE		= '1'
WHERE
	SITE_CD = 'A001';

/*----------------------------------------------*/
/* その他										*/
/*----------------------------------------------*/
UPDATE T_SITE SET TX_LO_MAILER_IP = NULL,TX_HI_MAILER_IP = NULL;
commit;






-- 注）sysで接続しています。
CONNECT SYS/IBDB@VICOMM_MFREE AS SYSDBA
EXEC UTL_RECOMP.RECOMP_SERIAL(schema => 'VICOMM_MFREE');
SPOOL OFF;
exit



