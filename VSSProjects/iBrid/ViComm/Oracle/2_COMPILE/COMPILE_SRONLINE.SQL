spool c:\VICOMM-SRONLINE.txt

CONNECT VICOMM_SRONLINE/VICOMM_SRONLINE@VICOMM_SRONLINE

@K:\VSSProjects\ibrid\ViComm\Oracle\2_COMPILE\COMPILE_VICOMM.SQL

/*----------------------------------------------*/
/* Setup Records								*/
/*----------------------------------------------*/
@K:\VssProjects\ibrid\ViComm\Oracle\9_RECORD\SRONLINE\T_CODE_DTL;
/*----------------------------------------------*/
/* Setup Programs								*/
/*----------------------------------------------*/
@K:\VSSProjects\ibrid\ViComm\Oracle\9_RECORD\COMMON\T_PROGRAM.SQL
@K:\VSSProjects\ibrid\ViComm\Oracle\9_RECORD\SRONLINE\T_PROGRAM.SQL
/*----------------------------------------------*/
/* Setup Error									*/
/*----------------------------------------------*/
@K:\VSSProjects\ibrid\ViComm\Oracle\9_RECORD\COMMON\T_ERROR;
@K:\VSSProjects\iBrid\ViComm\Oracle\9_RECORD\COMMON\T_ERROR_Z001;
@K:\VSSProjects\iBrid\ViComm\Oracle\9_RECORD\COMMON\COPY_ERROR.SQL;

/*----------------------------------------------*/
/* Trigger 										*/
/*----------------------------------------------*/
@K:\VssProjects\iBrid\ViComm\Oracle\7_TRIGGER\TRG_CAST_PIC_ATTR_TYPE
@K:\VssProjects\iBrid\ViComm\Oracle\7_TRIGGER\TRG_CAST_PIC_ATTR_TYPE_VALUE

/*----------------------------------------------*/
/* 機能設定										*/
/*----------------------------------------------*/
@K:\VssProjects\iBrid\ViComm\Oracle\9_RECORD\SRONLINE\T_MANAGE_COMPANY;

/*----------------------------------------------*/
/* メニュー設定									*/
/*----------------------------------------------*/
DELETE T_SITEMAP;
COMMIT;

@K:VSSProjects\iBrid\ViComm\Oracle\9_RECORD\COMMON\T_SITE_MAP_SYS_OWNER.SQL;
@K:VSSProjects\iBrid\ViComm\Oracle\9_RECORD\COMMON\T_SITE_MAP_SITE_OWNER.SQL;
@K:\VSSProjects\iBrid\ViComm\Oracle\9_RECORD\SRONLINE\T_SITE_MAP_SITE_OWNER.SQL

DELETE T_SITEMAP WHERE MENU_TITLE='MAQIAアフィリエイト管理';
DELETE T_SITEMAP WHERE MENU_TITLE='商品管理';
DELETE T_SITEMAP WHERE MENU_TITLE='出演者動画属性区分';
DELETE T_SITEMAP WHERE MENU_TITLE='出演者動画属性値';
DELETE T_SITEMAP WHERE MENU_TITLE='期間別商品購入集計';
DELETE T_SITEMAP WHERE MENU_TITLE='商品別購入履歴';

DELETE T_SITEMAP WHERE ADMIN_TYPE='1' AND MENU_TITLE ='広告グループ別詳細稼動集計（男性会員）';
DELETE T_SITEMAP WHERE ADMIN_TYPE='1' AND MENU_TITLE ='広告グループ別詳細稼動集計（女性会員）';

COMMIT;

/*----------------------------------------------*/
/* メールオブジェクト設定						*/
/*----------------------------------------------*/
UPDATE
	T_SITE
SET
	STAMP_FILE_NM	= 'kikon_stamp.png'	,
	PIC_SIZE_LARGE	= 220				,
	PIC_SIZE_SMALL	= 96				,
	SCALE_TYPE		= '3'
WHERE
	SITE_CD = 'A001';

/*----------------------------------------------*/
/* その他										*/
/*----------------------------------------------*/
UPDATE T_SITE SET TX_LO_MAILER_IP = NULL,TX_HI_MAILER_IP = NULL;

UPDATE T_PROGRAM SET ENABLE_OVERRIDE=1 WHERE PROGRAM_ROOT='/ViComm/man' AND PROGRAM_ID='WriteUserBbs.aspx' ;

UPDATE T_PROGRAM SET ENABLE_OVERRIDE=1 WHERE PROGRAM_ROOT='/ViComm/man' 	AND PROGRAM_ID='FindUser.aspx' ;
UPDATE T_PROGRAM SET ENABLE_OVERRIDE=1 WHERE PROGRAM_ROOT='/ViComm/man' 	AND PROGRAM_ID='RegistUserRequestByTermId.aspx' ;
UPDATE T_PROGRAM SET ENABLE_OVERRIDE=1 WHERE PROGRAM_ROOT='/ViComm/man' 	AND PROGRAM_ID='RegistUserRequestByTermIdCast.aspx' ;
UPDATE T_PROGRAM SET ENABLE_OVERRIDE=1 WHERE PROGRAM_ROOT='/ViComm/man' 	AND PROGRAM_ID='StartWaiting.aspx' ;
UPDATE T_PROGRAM SET ENABLE_OVERRIDE=1 WHERE PROGRAM_ROOT='/ViComm/man' 	AND PROGRAM_ID='ModifyUserProfile.aspx' ;

UPDATE T_PROGRAM SET ENABLE_OVERRIDE=1 WHERE PROGRAM_ROOT='/ViComm/woman' 	AND PROGRAM_ID='FindUser.aspx' ;
UPDATE T_PROGRAM SET ENABLE_OVERRIDE=1 WHERE PROGRAM_ROOT='/ViComm/woman' 	AND PROGRAM_ID='ModifyUserProfile.aspx' ;
UPDATE T_PROGRAM SET ENABLE_OVERRIDE=1 WHERE PROGRAM_ROOT='/ViComm/woman' 	AND PROGRAM_ID='RegistUserRequestByTermId.aspx' ;
UPDATE T_PROGRAM SET ENABLE_OVERRIDE=1 WHERE PROGRAM_ROOT='/ViComm/woman' 	AND PROGRAM_ID='StartWaiting.aspx' ;
UPDATE T_PROGRAM SET ENABLE_OVERRIDE=1 WHERE PROGRAM_ROOT='/ViComm/woman' 	AND PROGRAM_ID='WriteCastBbs.aspx' ;
UPDATE T_PROGRAM SET ENABLE_OVERRIDE=1 WHERE PROGRAM_ROOT='/ViComm/woman' 	AND PROGRAM_ID='SendProfilePic.aspx' ;
UPDATE T_PROGRAM SET ENABLE_OVERRIDE=1 WHERE PROGRAM_ROOT='/ViComm/woman' 	AND PROGRAM_ID='FindBatchMailUser.aspx' ;

/*
UPDATE T_PROGRAM SET ENABLE_OVERRIDE=1 WHERE PROGRAM_ROOT='/ViComm/woman' 	AND PROGRAM_ID='FindBatchMailUser.aspx' ;
UPDATE T_PROGRAM SET ENABLE_OVERRIDE=1 WHERE PROGRAM_ROOT='/ViComm/woman' 	AND PROGRAM_ID='TxMail.aspx' ;
UPDATE T_PROGRAM SET ENABLE_OVERRIDE=1 WHERE PROGRAM_ROOT='/ViComm/woman' 	AND PROGRAM_ID='ReturnMail.aspx' ;
UPDATE T_PROGRAM SET ENABLE_OVERRIDE=1 WHERE PROGRAM_ROOT='/ViComm/woman' 	AND PROGRAM_ID='FindUser.aspx' ;
UPDATE T_PROGRAM SET ENABLE_OVERRIDE=1 WHERE PROGRAM_ROOT='/ViComm/woman' 	AND PROGRAM_ID='TxInviteMail.aspx' ;
*/
commit;



-- 注）sysで接続しています。
CONNECT SYS/IBDB@VICOMM_SRONLINE AS SYSDBA
EXEC UTL_RECOMP.RECOMP_SERIAL(schema => 'VICOMM_SRONLINE');
SPOOL OFF;
exit

