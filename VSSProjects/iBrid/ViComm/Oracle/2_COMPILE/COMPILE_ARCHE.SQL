spool c:\VICOMM-ARCHE.txt

CONNECT VICOMM_ARCHE/VICOMM_ARCHE@VICOMM_ARCHE

@K:\VSSProjects\ibrid\ViComm\Oracle\2_COMPILE\COMPILE_VICOMM.SQL

/*----------------------------------------------*/
/* Setup Records								*/
/*----------------------------------------------*/
@K:\VssProjects\ibrid\ViComm\Oracle\9_RECORD\ARCHE\T_CODE_DTL;
/*----------------------------------------------*/
/* Setup Programs								*/
/*----------------------------------------------*/
@K:\VSSProjects\ibrid\ViComm\Oracle\9_RECORD\COMMON\T_PROGRAM.SQL
@K:\VSSProjects\ibrid\ViComm\Oracle\9_RECORD\ARCHE\T_PROGRAM.SQL
/*----------------------------------------------*/
/* Setup Error									*/
/*----------------------------------------------*/
@K:\VSSProjects\ibrid\ViComm\Oracle\9_RECORD\COMMON\T_ERROR;
@K:\VSSProjects\iBrid\ViComm\Oracle\9_RECORD\COMMON\T_ERROR_Z001;
@K:\VSSProjects\iBrid\ViComm\Oracle\9_RECORD\COMMON\COPY_ERROR.SQL;

/*----------------------------------------------*/
/* Trigger 										*/
/*----------------------------------------------*/
@K:\VssProjects\iBrid\ViComm\Oracle\7_TRIGGER\TRG_CAST_PIC_ATTR_TYPE
@K:\VssProjects\iBrid\ViComm\Oracle\7_TRIGGER\TRG_CAST_PIC_ATTR_TYPE_VALUE

/*----------------------------------------------*/
/* 機能設定										*/
/*----------------------------------------------*/
@K:\VssProjects\iBrid\ViComm\Oracle\9_RECORD\ARCHE\T_MANAGE_COMPANY;

/*----------------------------------------------*/
/* メニュー設定									*/
/*----------------------------------------------*/
DELETE T_SITEMAP;
COMMIT;

@K:VSSProjects\iBrid\ViComm\Oracle\9_RECORD\COMMON\T_SITE_MAP_SYS_OWNER.SQL;
@K:VSSProjects\iBrid\ViComm\Oracle\9_RECORD\COMMON\T_SITE_MAP_SITE_OWNER.SQL;

DELETE T_SITEMAP WHERE ADMIN_TYPE='1' AND MENU_TITLE ='広告グループ別詳細稼動集計（男性会員）';
DELETE T_SITEMAP WHERE ADMIN_TYPE='1' AND MENU_TITLE ='広告グループ別詳細稼動集計（女性会員）';
DELETE T_SITEMAP WHERE MENU_TITLE='MAQIAアフィリエイト管理';
DELETE T_SITEMAP WHERE MENU_TITLE='商品管理';
DELETE T_SITEMAP WHERE MENU_TITLE='期間別商品購入集計';
DELETE T_SITEMAP WHERE MENU_TITLE='商品別購入履歴';
INSERT INTO T_SITEMAP(MANAGE_COMPANY_CD,ADMIN_TYPE,HIERARCHY,MENU_TITLE)VALUES('A1','9',2,'ＮＧメールアドレス解除'				);

COMMIT;


/*----------------------------------------------*/
/* メールオブジェクト設定						*/
/*----------------------------------------------*/
UPDATE
	T_SITE
SET
	STAMP_FILE_NM	= NULL	,
	PIC_SIZE_LARGE	= 200	,
	PIC_SIZE_SMALL	= 80	,
	SCALE_TYPE		= '1'
WHERE
	SITE_CD = 'A001';

/*----------------------------------------------*/
/* その他										*/
/*----------------------------------------------*/
--ﾒｰﾙ作成時にﾃｰﾌﾞﾙﾀｸﾞなどを利用しない
UPDATE T_SITE SET MAIL_DOC_NOT_USE_WEB_FACE_FLAG = 1;
--
UPDATE T_SITE SET TX_LO_MAILER_IP = NULL,TX_HI_MAILER_IP = NULL;

UPDATE T_PROGRAM SET ENABLE_OVERRIDE=1 WHERE PROGRAM_ROOT='/ViComm/man' 	AND PROGRAM_ID='ModifyUserProfile.aspx';
UPDATE T_PROGRAM SET ENABLE_OVERRIDE=1 WHERE PROGRAM_ROOT='/ViComm/man' 	AND PROGRAM_ID='ModifyUserHandleNm.aspx';
UPDATE T_PROGRAM SET ENABLE_OVERRIDE=1 WHERE PROGRAM_ROOT='/ViComm/man' 	AND PROGRAM_ID='ReturnMail.aspx';
UPDATE T_PROGRAM SET ENABLE_OVERRIDE=1 WHERE PROGRAM_ROOT='/ViComm/man' 	AND PROGRAM_ID='TxMail.aspx';
UPDATE T_PROGRAM SET ENABLE_OVERRIDE=1 WHERE PROGRAM_ROOT='/ViComm/man' 	AND PROGRAM_ID='SetupRxMail.aspx';
UPDATE T_PROGRAM SET ENABLE_OVERRIDE=1 WHERE PROGRAM_ROOT='/ViComm/man' 	AND PROGRAM_ID='RegistUserRequestByTermId.aspx';
UPDATE T_PROGRAM SET ENABLE_OVERRIDE=1 WHERE PROGRAM_ROOT='/ViComm/man' 	AND PROGRAM_ID='RegistUserRequestByTermIdCast.aspx';
UPDATE T_PROGRAM SET ENABLE_OVERRIDE=1 WHERE PROGRAM_ROOT='/ViComm/woman' 	AND PROGRAM_ID='StartWaitting.aspx';
UPDATE T_PROGRAM SET ENABLE_OVERRIDE=1 WHERE PROGRAM_ROOT='/ViComm/woman' 	AND PROGRAM_ID='ModifyUserProfile.aspx';
UPDATE T_PROGRAM SET ENABLE_OVERRIDE=1 WHERE PROGRAM_ROOT='/ViComm/woman' 	AND PROGRAM_ID='ModifyUserHandleNm.aspx';
UPDATE T_PROGRAM SET ENABLE_OVERRIDE=1 WHERE PROGRAM_ROOT='/ViComm/woman' 	AND PROGRAM_ID='ReturnMail.aspx';
UPDATE T_PROGRAM SET ENABLE_OVERRIDE=1 WHERE PROGRAM_ROOT='/ViComm/woman' 	AND PROGRAM_ID='TxMail.aspx';
UPDATE T_PROGRAM SET ENABLE_OVERRIDE=1 WHERE PROGRAM_ROOT='/ViComm/woman' 	AND PROGRAM_ID='ListRequestHistory.aspx';
UPDATE T_PROGRAM SET ENABLE_OVERRIDE=1 WHERE PROGRAM_ROOT='/ViComm/woman'	AND PROGRAM_ID='SetupRxMail.aspx';


commit;

/*----------------------------------------------*/
/* Job											*/
/*----------------------------------------------*/
--@K:\VssProjects\iBrid\ViComm\Oracle\A_JOB\JOB_ARCHE












-- 注）sysで接続しています。
CONNECT SYS/IBDB@VICOMM_ARCHE AS SYSDBA
EXEC UTL_RECOMP.RECOMP_SERIAL(schema => 'VICOMM_ARCHE');
SPOOL OFF;
exit
