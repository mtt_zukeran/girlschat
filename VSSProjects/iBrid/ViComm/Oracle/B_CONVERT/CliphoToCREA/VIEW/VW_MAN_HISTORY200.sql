/*************************************************************************/
--	System			: ViComm
--  Title			: お気に入り、拒否情報 View00
--	Progaram ID		: VW_MAN_HISTORY200
--  Creation Date	: 10.06.09
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
CREATE OR REPLACE VIEW VW_MAN_HISTORY200(
	USERID			,
	AUID			,
	PSEND			,
	PRECEIVE		,
	PFLG			,
	MODIFYDATE		,
	REGISTDATE		,
	MSEND			,
	MRECEIVE		,
	RUNFLG			,
	USER_SEQ		,
	SEX_CD			,
	USER_CHAR_NO	,
	CAST_USER_SEQ	,
	CAST_SEX_CD		,
	CAST_USER_CHAR_NO
)AS SELECT
	MAN_HISTORY2.USERID		,
	MAN_HISTORY2.AUID		,
	MAN_HISTORY2.PSEND		,
	MAN_HISTORY2.PRECEIVE	,
	MAN_HISTORY2.PFLG		,
	MAN_HISTORY2.MODIFYDATE	,
	MAN_HISTORY2.REGISTDATE	,
	MAN_HISTORY2.MSEND		,
	MAN_HISTORY2.MRECEIVE	,
	MAN_HISTORY2.RUNFLG		,
	T1.USER_SEQ				,
	T1.SEX_CD				,
	T1.USER_CHAR_NO			,
	T2.USER_SEQ				,
	T2.SEX_CD				,
	T2.USER_CHAR_NO
FROM
	MAN_HISTORY2		,
	T_EX_USER_SEQ	 T1	,
	T_EX_USER_SEQ	 T2
WHERE
	MAN_HISTORY2.USERID	= T1.USERID	(+)	AND
	'1'					= T1.SEX_CD	(+)	AND
	MAN_HISTORY2.AUID 	= T2.USERID	(+)	AND
	'3'					= T2.SEX_CD	(+)
;
