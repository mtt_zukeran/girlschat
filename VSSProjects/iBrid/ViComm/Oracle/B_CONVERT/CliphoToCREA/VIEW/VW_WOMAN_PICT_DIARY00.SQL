/*************************************************************************/
--	System			: ViComm
--  Title			: ���L View00
--	Progaram ID		: VW_WOMAN_PICT_DIARY00
--  Creation Date	: 10.06.09
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
CREATE OR REPLACE VIEW VW_WOMAN_PICT_DIARY00(
	USERID					,
	KID						,
	BBSID					,
	READNUM					,
	K_OPT1					,
	K_OPT2					,
	PICNAME					,
	TITLE					,
	BODY					,
	WRITEDATE				,
	V_BBS					,
	WRITEFLAG				,
	MOVIETYPE				,
	SIZE#					,
	IMGRANK					,
	DISP_FACE				,
	IS_DELETED				,
	USER_SEQ				,
	SEX_CD					,
	USER_CHAR_NO
)AS SELECT
	WOMAN_PICT_DIARY.USERID						,
	WOMAN_PICT_DIARY.KID						,
	WOMAN_PICT_DIARY.BBSID						,
	WOMAN_PICT_DIARY.READNUM					,
	WOMAN_PICT_DIARY.K_OPT1						,
	WOMAN_PICT_DIARY.K_OPT2						,
	WOMAN_PICT_DIARY.PICNAME					,
	WOMAN_PICT_DIARY.TITLE						,
	WOMAN_PICT_DIARY.BODY						,
	WOMAN_PICT_DIARY.WRITEDATE					,
	WOMAN_PICT_DIARY.V_BBS						,
	WOMAN_PICT_DIARY.WRITEFLAG					,
	WOMAN_PICT_DIARY.MOVIETYPE					,
	WOMAN_PICT_DIARY.SIZE#						,
	WOMAN_PICT_DIARY.IMGRANK					,
	WOMAN_PICT_DIARY.DISP_FACE					,
	WOMAN_PICT_DIARY.IS_DELETED					,
	T_EX_USER_SEQ.USER_SEQ						,
	T_EX_USER_SEQ.SEX_CD						,
	T_EX_USER_SEQ.USER_CHAR_NO
FROM
	WOMAN_PICT_DIARY	,
	T_EX_USER_SEQ
WHERE
	WOMAN_PICT_DIARY.USERID	= T_EX_USER_SEQ.USERID						(+)	AND
	'3'						= T_EX_USER_SEQ.SEX_CD						(+)
;
