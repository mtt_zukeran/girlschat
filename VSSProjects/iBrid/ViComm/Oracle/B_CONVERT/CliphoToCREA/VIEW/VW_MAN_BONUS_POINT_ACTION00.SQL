/*************************************************************************/
--	System			: ViComm
--  Title			: bonus���� View00
--	Progaram ID		: VW_MAN_BONUS_POINT_ACTION00
--  Creation Date	: 10.06.09
--	Update Date		: 
--  Author			: i-Brid
/*************************************************************************/
CREATE OR REPLACE VIEW VW_MAN_BONUS_POINT_ACTION00 (
	USERID			,
	CONF_ID			,
	BUYID			,
	F_KEY			,
	POINT			,
	WRITEDATE		,
	USER_SEQ		,
	SEX_CD			,
	USER_CHAR_NO
)AS SELECT
	MAN_BONUS_POINT_ACTION.USERID			,
	MAN_BONUS_POINT_ACTION.CONF_ID			,
	MAN_BONUS_POINT_ACTION.BUYID			,
	MAN_BONUS_POINT_ACTION.F_KEY			,
	MAN_BONUS_POINT_ACTION.POINT			,
	MAN_BONUS_POINT_ACTION.WRITEDATE		,
	T_EX_USER_SEQ.USER_SEQ					,
	T_EX_USER_SEQ.SEX_CD					,
	T_EX_USER_SEQ.USER_CHAR_NO
FROM
	MAN_BONUS_POINT_ACTION,
	T_EX_USER_SEQ
WHERE
	MAN_BONUS_POINT_ACTION.USERID	= T_EX_USER_SEQ.USERID	(+)	AND
	'1'								= T_EX_USER_SEQ.SEX_CD	(+)
;
