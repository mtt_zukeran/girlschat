/*************************************************************************/
--	System			: ViComm
--  Title			: 通話、およびメール送受信履歴 View00
--	Progaram ID		: VW_MAN_HISTORY100
--  Creation Date	: 10.06.09
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
CREATE OR REPLACE VIEW VW_MAN_HISTORY100(
	USERID			,
	AUID			,
	MODIFYDATE		,
	PKIND			,
	COUNT			,
	DISABLED		,
	USER_SEQ		,
	SEX_CD			,
	USER_CHAR_NO	,
	CAST_USER_SEQ	,
	CAST_SEX_CD		,
	CAST_USER_CHAR_NO
)AS SELECT
	MAN_HISTORY1.USERID		,
	MAN_HISTORY1.AUID		,
	MAN_HISTORY1.MODIFYDATE	,
	MAN_HISTORY1.PKIND		,
	MAN_HISTORY1.COUNT		,
	MAN_HISTORY1.DISABLED	,
	T1.USER_SEQ				,
	T1.SEX_CD				,
	T1.USER_CHAR_NO			,
	T2.USER_SEQ				,
	T2.SEX_CD				,
	T2.USER_CHAR_NO
FROM
	MAN_HISTORY1		,
	T_EX_USER_SEQ	 T1	,
	T_EX_USER_SEQ	 T2
WHERE
	MAN_HISTORY1.USERID		= T1.USERID	(+)	AND
	'1'						= T1.SEX_CD (+)	AND
	MAN_HISTORY1.AUID 		= T2.USERID	(+)	AND
	'3'						= T2.SEX_CD (+)
;
