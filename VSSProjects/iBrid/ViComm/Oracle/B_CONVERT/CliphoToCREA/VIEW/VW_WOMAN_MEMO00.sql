/*************************************************************************/
--	System			: ViComm
--  Title			: �� View00
--	Progaram ID		: VW_WOMAN_MEMO00
--  Creation Date	: 10.06.09
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
CREATE OR REPLACE VIEW VW_WOMAN_MEMO00(
	MID				,
	USERID			,
	AUID			,
	BODY			,
	WRITEDATE		,
	USER_SEQ		,
	SEX_CD			,
	USER_CHAR_NO	,
	CAST_USER_SEQ	,
	CAST_SEX_CD		,
	CAST_USER_CHAR_NO
)AS SELECT
	WOMAN_MEMO.MID			,
	WOMAN_MEMO.USERID		,
	WOMAN_MEMO.AUID			,
	WOMAN_MEMO.BODY			,
	WOMAN_MEMO.WRITEDATE	,
	T1.USER_SEQ				,
	T1.SEX_CD				,
	T1.USER_CHAR_NO			,
	T2.USER_SEQ				,
	T2.SEX_CD				,
	T2.USER_CHAR_NO
FROM
	WOMAN_MEMO			,
	T_EX_USER_SEQ	 T1	,
	T_EX_USER_SEQ	 T2
WHERE
	WOMAN_MEMO.AUID		= T1.USERID	(+)	AND
	'1'					= T1.SEX_CD	(+)	AND
	WOMAN_MEMO.USERID 	= T2.USERID	(+)	AND
	'3'					= T2.SEX_CD	(+)
;
