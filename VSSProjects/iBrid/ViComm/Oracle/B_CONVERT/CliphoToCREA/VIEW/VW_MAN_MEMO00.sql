/*************************************************************************/
--	System			: ViComm
--  Title			: �� View00
--	Progaram ID		: VW_MAN_MEMO00
--  Creation Date	: 10.06.09
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
CREATE OR REPLACE VIEW VW_MAN_MEMO00(
	MID				,
	USERID			,
	AUID			,
	BODY			,
	WRITEDATE		,
	USER_SEQ		,
	SEX_CD			,
	USER_CHAR_NO	,
	CAST_USER_SEQ	,
	CAST_SEX_CD		,
	CAST_USER_CHAR_NO
)AS SELECT
	MAN_MEMO.MID			,
	MAN_MEMO.USERID			,
	MAN_MEMO.AUID			,
	MAN_MEMO.BODY			,
	MAN_MEMO.WRITEDATE		,
	T1.USER_SEQ				,
	T1.SEX_CD				,
	T1.USER_CHAR_NO			,
	T2.USER_SEQ				,
	T2.SEX_CD				,
	T2.USER_CHAR_NO
FROM
	MAN_MEMO			,
	T_EX_USER_SEQ	 T1	,
	T_EX_USER_SEQ	 T2
WHERE
	MAN_MEMO.USERID	= T1.USERID	(+)	AND
	'1'				= T1.SEX_CD	(+)	AND
	MAN_MEMO.AUID 	= T2.USERID	(+)	AND
	'3'				= T2.SEX_CD	(+)
;
