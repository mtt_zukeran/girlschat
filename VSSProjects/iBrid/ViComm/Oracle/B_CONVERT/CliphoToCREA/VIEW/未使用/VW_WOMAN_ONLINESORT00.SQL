/*************************************************************************/
--	System			: ViComm
--  Title			: View00
--	Progaram ID		: VW_WOMAN_ONLINESORT00
--  Creation Date	: 10.06.09
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
CREATE OR REPLACE VIEW VW_WOMAN_ONLINESORT00(
	ONLINESORTID	,
	USERID			,
	KIND			,
	SORTNUM			,
	WAITINGKIND		,
	USER_SEQ		,
	SEX_CD			,
	USER_CHAR_NO
)AS SELECT
	WOMAN_ONLINESORT.ONLINESORTID	,
	WOMAN_ONLINESORT.USERID			,
	WOMAN_ONLINESORT.KIND			,
	WOMAN_ONLINESORT.SORTNUM		,
	WOMAN_ONLINESORT.WAITINGKIND	,
	T_EX_USER_SEQ.USER_SEQ			,
	T_EX_USER_SEQ.SEX_CD			,
	T_EX_USER_SEQ.USER_CHAR_NO
FROM
	WOMAN_ONLINESORT	,
	T_EX_USER_SEQ
WHERE
	WOMAN_ONLINESORT.USERID	= T_EX_USER_SEQ.USERID	(+)	AND
	'3'						= T_EX_USER_SEQ.SEX_CD	(+)
;
