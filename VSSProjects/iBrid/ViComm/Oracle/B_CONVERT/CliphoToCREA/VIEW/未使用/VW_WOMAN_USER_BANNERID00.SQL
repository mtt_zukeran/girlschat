/*************************************************************************/
--	System			: ViComm
--  Title			: View00
--	Progaram ID		: VW_WOMAN_USER_BANNERID00
--  Creation Date	: 10.06.09
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
CREATE OR REPLACE VIEW VW_WOMAN_USER_BANNERID00(
	USERID		,
	BANNERID	,
	WRITEDATE	,
	USER_SEQ	,
	SEX_CD		,
	USER_CHAR_NO
)AS SELECT
	WOMAN_USER_BANNERID.USERID		,
	WOMAN_USER_BANNERID.BANNERID	,
	WOMAN_USER_BANNERID.WRITEDATE	,
	T_EX_USER_SEQ.USER_SEQ			,
	T_EX_USER_SEQ.SEX_CD			,
	T_EX_USER_SEQ.USER_CHAR_NO
FROM
	WOMAN_USER_BANNERID	,
	T_EX_USER_SEQ
WHERE
	WOMAN_USER_BANNERID.USERID	= T_EX_USER_SEQ.USERID	(+)	AND
	'3'							= T_EX_USER_SEQ.SEX_CD	(+)
;
