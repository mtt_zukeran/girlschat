/*************************************************************************/
--  System                      : ViComm
--  Title                       : 男性会員のログView01
--  Progaram ID                 : VW_MLOG_LOGYYYYMM01
--  Creation Date               : 10.06.11
--  Update Date                 : 
--  Author                      : i-Brid(K.Itoh)
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE VIEW VW_MLOG_LOGYYYYMM01(
    LOGID           ,
    USERID          ,
    AUSERID         ,
    NOWPOINT        ,
    USEDPOINT       ,
    ACTION          ,
    ACTIONGROUP     ,
    ACTIONDETAIL    ,
    IP              ,
    LOGDATE         ,
    MAN_USER_SEQ    ,
    MAN_SEX_CD      ,
	MAN_USER_CHAR_NO,
    CAST_USER_SEQ   ,
    CAST_SEX_CD     ,
	CAST_USER_CHAR_NO
)
AS
  SELECT 
    MLOG_LOGYYYYMM.LOGID 		,
    MLOG_LOGYYYYMM.USERID 		,
    MLOG_LOGYYYYMM.AUSERID 		,
    MLOG_LOGYYYYMM.NOWPOINT 	,
    MLOG_LOGYYYYMM.USEDPOINT 	,
    MLOG_LOGYYYYMM.ACTION 		,
    MLOG_LOGYYYYMM.ACTIONGROUP 	,
    MLOG_LOGYYYYMM.ACTIONDETAIL ,
    MLOG_LOGYYYYMM.IP 			,
    MLOG_LOGYYYYMM.LOGDATE 		,
    T1.USER_SEQ 				,
    T1.SEX_CD 					,
	T1.USER_CHAR_NO				,
    T2.USER_SEQ 				,
    T2.SEX_CD					,
	T2.USER_CHAR_NO
  FROM 
    MLOG_LOGYYYYMM      ,
    T_EX_USER_SEQ T1    ,
    T_EX_USER_SEQ T2    
  WHERE
        MLOG_LOGYYYYMM.USERID   = T1.USERID (+)
	AND	'1'						= T1.SEX_CD (+)
    AND MLOG_LOGYYYYMM.AUSERID  = T2.USERID (+)
	AND '3'						= T2.SEX_CD (+)
;
