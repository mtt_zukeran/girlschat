/*************************************************************************/
--	System			: ViComm
--  Title			: View00
--	Progaram ID		: VW_MAINTABLE_BLACKJACK00
--  Creation Date	: 10.06.09
--	Update Date		: 
--  Author			: i-Brid
/*************************************************************************/
CREATE OR REPLACE VIEW VW_MAINTABLE_BLACKJACK00 (
	USERID		,
	SEX			,
	BETPOINT	,
	BETTIME		,
	GAMEFLAG	,
	USER_SEQ	,
	SEX_CD		,
	USER_CHAR_NO
)AS SELECT
	BLACKJACK.USERID		,
	BLACKJACK.SEX			,
	BLACKJACK.BETPOINT		,
	BLACKJACK.BETTIME		,
	BLACKJACK.GAMEFLAG		,
	T_EX_USER_SEQ.USER_SEQ	,
	T_EX_USER_SEQ.SEX_CD	,
	T_EX_USER_SEQ.USER_CHAR_NO
FROM
	BLACKJACK		,
	T_EX_USER_SEQ
WHERE
	BLACKJACK.USERID = T_EX_USER_SEQ.USERID	(+)
;
