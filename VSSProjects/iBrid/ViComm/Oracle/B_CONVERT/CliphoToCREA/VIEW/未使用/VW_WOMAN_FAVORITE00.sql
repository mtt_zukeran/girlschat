/*************************************************************************/
--	System			: ViComm
--  Title			: お気に入り View00
--	Progaram ID		: VW_WOMAN_FAVORITE00
--  Creation Date	: 10.06.09
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
CREATE OR REPLACE VIEW VW_WOMAN_FAVORITE00(
	AUID			,
	PSEND			,
	PRECEIVE		,
	PFLG			,
	MODIFYDATE		,
	REGISTDATE		,
	USERID			,
	USER_SEQ		,
	SEX_CD			,
	USER_CHAR_NO	,
	AUSER_SEQ		,
	ASEX_CD			,
	AUSER_CHAR_NO
)AS SELECT
	WOMAN_FAVORITE.AUID			,
	WOMAN_FAVORITE.PSEND		,
	WOMAN_FAVORITE.PRECEIVE		,
	WOMAN_FAVORITE.PFLG			,
	WOMAN_FAVORITE.MODIFYDATE	,
	WOMAN_FAVORITE.REGISTDATE	,
	WOMAN_FAVORITE.USERID		,
	T1.USER_SEQ					,
	T1.SEX_CD					,
	T1.USER_CHAR_NO				,
	T2.USER_SEQ					,
	T2.SEX_CD					,
	T2.USER_CHAR_NO
FROM
	WOMAN_FAVORITE		,
	T_EX_USER_SEQ	 T1	,
	T_EX_USER_SEQ	 T2
WHERE
	WOMAN_FAVORITE.USERID	= T1.USERID	(+)	AND
	'3'						= T1.SEX_CD	(+)	AND
	WOMAN_FAVORITE.AUID 	= T2.USERID	(+)	AND
	'1'						= T2.SEX_CD	(+)
;
