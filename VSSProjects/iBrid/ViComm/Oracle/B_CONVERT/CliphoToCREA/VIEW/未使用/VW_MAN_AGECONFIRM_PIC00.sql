/*************************************************************************/
--	System			: ViComm
--  Title			: �N��F�؉摜��� View00
--	Progaram ID		: VW_MAN_AGECONFIRM_PIC00
--  Creation Date	: 10.06.09
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
CREATE OR REPLACE VIEW VW_MAN_AGECONFIRM_PIC00(
	AGE_PICID		,
	USERID			,
	CHECK_STATUS	,
	WRITEDATE		,
	FILE_SIZE		,
	USER_SEQ        ,
	SEX_CD			,
	USER_CHAR_NO
)AS SELECT
	MAN_AGECONFIRM_PIC.AGE_PICID		,
	MAN_AGECONFIRM_PIC.USERID			,
	MAN_AGECONFIRM_PIC.CHECK_STATUS		,
	MAN_AGECONFIRM_PIC.WRITEDATE		,
	MAN_AGECONFIRM_PIC.FILE_SIZE		,
	T_EX_USER_SEQ.USER_SEQ				,
	T_EX_USER_SEQ.SEX_CD				,
	T_EX_USER_SEQ.USER_CHAR_NO
FROM
	MAN_AGECONFIRM_PIC	,
	T_EX_USER_SEQ
WHERE
	MAN_AGECONFIRM_PIC.USERID 	= T_EX_USER_SEQ.USERID	(+)	AND
	'1'							= T_EX_USER_SEQ.SEX_CD	(+)
;

