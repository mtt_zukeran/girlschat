/*************************************************************************/
--	System			: ViComm
--  Title			: View00
--	Progaram ID		: VW_WOMAN_GOMI200
--  Creation Date	: 10.06.09
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
CREATE OR REPLACE VIEW VW_WOMAN_GOMI200(
	USERID		,
	CHANGEDATE	,
	STATUS		,
	USER_SEQ	,
	SEX_CD		,
	USER_CHAR_NO
)AS SELECT
	WOMAN_GOMI2.USERID		,
	WOMAN_GOMI2.CHANGEDATE	,
	WOMAN_GOMI2.STATUS		,
	T_EX_USER_SEQ.USER_SEQ	,
	T_EX_USER_SEQ.SEX_CD	,
	T_EX_USER_SEQ.USER_CHAR_NO
FROM
	WOMAN_GOMI2		,
	T_EX_USER_SEQ
WHERE
	WOMAN_GOMI2.USERID	= T_EX_USER_SEQ.USERID	(+)	AND
	'3'					= T_EX_USER_SEQ.SEX_CD	(+)
;
