/*************************************************************************/
--	System			: ViComm
--  Title			: View00
--	Progaram ID		: VW_MAN_ROOM00
--  Creation Date	: 10.06.09
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
CREATE OR REPLACE VIEW VW_MAN_ROOM00(
	RO_UID		,
	RO_NAME		,
	USER_SEQ	,
	SEX_CD		,
	USER_CHAR_NO
)AS SELECT
	MAN_ROOM.RO_UID			,
	MAN_ROOM.RO_NAME		,
	T_EX_USER_SEQ.USER_SEQ	,
	T_EX_USER_SEQ.SEX_CD	,
	T_EX_USER_SEQ.USER_CHAR_NO
FROM
	MAN_ROOM		,
	T_EX_USER_SEQ
WHERE
	MAN_ROOM.RO_UID 	= T_EX_USER_SEQ.USERID	(+)	AND
	'1'					= T_EX_USER_SEQ.SEX_CD	(+)
;
