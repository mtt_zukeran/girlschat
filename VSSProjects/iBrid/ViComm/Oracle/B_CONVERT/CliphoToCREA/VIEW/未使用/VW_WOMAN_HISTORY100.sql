/*************************************************************************/
--	System			: ViComm
--  Title			: 通話、およびメール送受信履歴 View00
--	Progaram ID		: VW_WOMAN_HISTORY100
--  Creation Date	: 10.06.09
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
CREATE OR REPLACE VIEW VW_WOMAN_HISTORY100(
	USERID			,
	AUID			,
	MODIFYDATE		,
	PKIND			,
	COUNT			,
	DISABLED		,
	CAST_USER_SEQ	,
	CAST_SEX_CD		,
	CAST_USER_CHAR_NO,
	USER_SEQ		,
	SEX_CD			,
	USER_CHAR_NO
)AS SELECT
	WOMAN_HISTORY1.USERID		,
	WOMAN_HISTORY1.AUID			,
	WOMAN_HISTORY1.MODIFYDATE	,
	WOMAN_HISTORY1.PKIND		,
	WOMAN_HISTORY1.COUNT		,
	WOMAN_HISTORY1.DISABLED		,
	T1.USER_SEQ					,
	T1.SEX_CD					,
	T1.USER_CHAR_NO				,
	T2.USER_SEQ					,
	T2.SEX_CD					,
	T2.USER_CHAR_NO
FROM
	WOMAN_HISTORY1		,
	T_EX_USER_SEQ	 T1	,
	T_EX_USER_SEQ	 T2
WHERE
	WOMAN_HISTORY1.USERID	= T1.USERID	(+) AND
	'3'						= T1.SEX_CD	(+) AND
	WOMAN_HISTORY1.AUID 	= T2.USERID	(+) AND
	'1'						= T2.SEX_CD	(+) 
;
