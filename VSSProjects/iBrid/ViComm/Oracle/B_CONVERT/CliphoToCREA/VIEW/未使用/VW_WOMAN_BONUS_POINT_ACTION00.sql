/*************************************************************************/
--	System			: ViComm
--  Title			: 特殊追加ポイント View00
--	Progaram ID		: VW_WOMAN_BONUS_POINT_ACTION00
--  Creation Date	: 10.06.09
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
CREATE OR REPLACE VIEW VW_WOMAN_BONUS_POINT_ACTION00(
	USERID		,
	CONF_ID		,
	BUYID		,
	F_KEY		,
	POINT		,
	WRITEDATE	,
	USER_SEQ	,
	SEX_CD		,
	USER_CHAR_NO
)AS SELECT
	WOMAN_BONUS_POINT_ACTION.USERID		,
	WOMAN_BONUS_POINT_ACTION.CONF_ID	,
	WOMAN_BONUS_POINT_ACTION.BUYID		,
	WOMAN_BONUS_POINT_ACTION.F_KEY		,
	WOMAN_BONUS_POINT_ACTION.POINT		,
	WOMAN_BONUS_POINT_ACTION.WRITEDATE	,
	T_EX_USER_SEQ.USER_SEQ				,
	T_EX_USER_SEQ.SEX_CD				,
	T_EX_USER_SEQ.USER_CHAR_NO
FROM
	WOMAN_BONUS_POINT_ACTION	,
	T_EX_USER_SEQ
WHERE
	WOMAN_BONUS_POINT_ACTION.USERID = T_EX_USER_SEQ.USERID	(+)	AND
	'3'								= T_EX_USER_SEQ.SEX_CD	(+)
;
