/*************************************************************************/
--	System			: ViComm
--  Title			: View00
--	Progaram ID		: VW_MAN_SPAMMAIL_BLOCK00
--  Creation Date	: 10.06.09
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
CREATE OR REPLACE VIEW VW_MAN_SPAMMAIL_BLOCK00(
	HIST_ID		,
	USERID		,
	AUID		,
	STATUS		,
	MODIFYDATE	,
	USER_SEQ	,
	SEX_CD		,
	USER_CHAR_NO,
	AUSER_SEQ	,
	ASEX_CD		,
	AUSER_CHAR_NO
)AS SELECT
	MAN_SPAMMAIL_BLOCK.HIST_ID		,
	MAN_SPAMMAIL_BLOCK.USERID		,
	MAN_SPAMMAIL_BLOCK.AUID			,
	MAN_SPAMMAIL_BLOCK.STATUS		,
	MAN_SPAMMAIL_BLOCK.MODIFYDATE	,
	T1.USER_SEQ						,
	T1.SEX_CD						,
	T1.USER_CHAR_NO					,
	T2.USER_SEQ						,
	T2.SEX_CD						,
	T2.USER_CHAR_NO
FROM
	MAN_SPAMMAIL_BLOCK	,
	T_EX_USER_SEQ	 T1	,
	T_EX_USER_SEQ	 T2
WHERE
	MAN_SPAMMAIL_BLOCK.USERID 	= T1.USERID	(+)	AND
	'1'							= T1.SEX_CD	(+)	AND
	MAN_SPAMMAIL_BLOCK.AUID 	= T2.USERID	(+)	AND
	'3'							= T2.SEX_CD	(+)
;
