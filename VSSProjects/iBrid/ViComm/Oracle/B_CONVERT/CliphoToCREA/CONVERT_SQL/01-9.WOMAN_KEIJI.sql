conn VICOMM_TEMP/VICOMM_TEMP@VICOMM_CREA

TIMING START

DECLARE
    
    CURSOR CS_WOMAN_KEIJI IS
		SELECT
			*
		FROM
			CLIPHO.VW_WOMAN_KEIJI00
		WHERE
			SEX_CD		= VICOMM_CONST.CAST
		ORDER BY
			WRITEDATE;
    
	CURSOR CS_CAST(prmUSER_SEQ	T_USER.USER_SEQ%TYPE) IS
		SELECT
			LOGIN_ID
		FROM
			T_USER
		WHERE
			USER_SEQ	= prmUSER_SEQ;

    REC_WOMAN_KEIJI             CLIPHO.VW_WOMAN_KEIJI00%ROWTYPE;
    REC_CAST					CS_CAST%ROWTYPE;

    BUF_SITE_CD                 VARCHAR2(4);
    BUF_BAT_DTL                 VARCHAR2(4000);
    BUF_WEB_DIR    			    T_SITE.WEB_PHISICAL_DIR%TYPE;
    BUF_CLOSED_FLAG             NUMBER(1);
    BUF_PIC_SEQ                 T_CAST_PIC.PIC_SEQ%TYPE;
    BUF_PIC_ATTR_TYPE_SEQ       T_CAST_PIC.CAST_PIC_ATTR_TYPE_SEQ%TYPE;
    BUF_PIC_ATTR_SEQ            T_CAST_PIC.CAST_PIC_ATTR_SEQ%TYPE;
    BUF_MOVIE_SEQ               T_CAST_MOVIE.MOVIE_SEQ%TYPE;
    BUF_MOVIE_ATTR_TYPE_SEQ     T_CAST_MOVIE.CAST_MOVIE_ATTR_TYPE_SEQ%TYPE;
    BUF_MOVIE_ATTR_SEQ          T_CAST_MOVIE.CAST_MOVIE_ATTR_SEQ%TYPE;
    BUF_PIC_SRC_DIR             VARCHAR2(4000);
    BUF_MOVIE_SRC_DIR           VARCHAR2(4000);

    fh                          UTL_FILE.FILE_TYPE;
    bat_pic                     UTL_FILE.FILE_TYPE;
    bat_movie                   UTL_FILE.FILE_TYPE;
	download_pic				UTL_FILE.FILE_TYPE;
	download_movie				UTL_FILE.FILE_TYPE;

	BUF_PIC_BAT_COUNT			NUMBER(5);
    
BEGIN
    -- ｻｲﾄｺｰﾄﾞ取得
    BUF_SITE_CD         := GET_SITE_CD;
    BUF_MOVIE_SRC_DIR   := 'E:\i-Brid\Site\ViComm-CREA\FtpClient\wbbsmovie\';
	BUF_WEB_DIR			:= GET_WEB_PHISICAL_DIR;
    
    fh              := UTL_FILE.FOPEN('D:\ERROR'    ,'T_CAST_BBS_' || TO_CHAR(SYSDATE,'YYYYMMDDHH24MISS') || '.TXT'         ,'W', 32767);
    bat_pic         := UTL_FILE.FOPEN('D:\BAT'      ,'T_CAST_BBS_PIC_' || TO_CHAR(SYSDATE,'YYYYMMDDHH24MISS') || '.BAT'     ,'W', 32767);
    bat_movie       := UTL_FILE.FOPEN('D:\BAT'      ,'T_CAST_BBS_MOVIE_' || TO_CHAR(SYSDATE,'YYYYMMDDHH24MISS') || '.BAT'   ,'W', 32767);
	download_pic	:= UTL_FILE.FOPEN('D:\BAT'		,'FTP_T_CAST_BBS_PIC_' || TO_CHAR(SYSDATE,'YYYYMMDDHH24MISS') || '.TXT'	,'W', 32767);
	download_movie	:= UTL_FILE.FOPEN('D:\BAT'		,'FTP_T_CAST_BBS_MOVIE_' || TO_CHAR(SYSDATE,'YYYYMMDDHH24MISS') || '.TXT'	,'W', 32767);

	BUF_PIC_BAT_COUNT	:= 0;

    FOR REC_WOMAN_KEIJI IN CS_WOMAN_KEIJI LOOP

        BUF_PIC_ATTR_TYPE_SEQ       := VICOMM_CONST.DEFAULT_PIC_ATTR_TYPE_SEQ;
        BUF_PIC_ATTR_SEQ            := VICOMM_CONST.DEFAULT_PIC_ATTR_SEQ;
        BUF_MOVIE_ATTR_TYPE_SEQ     := VICOMM_CONST.DEFAULT_MOVIE_ATTR_TYPE_SEQ;
        BUF_MOVIE_ATTR_SEQ          := VICOMM_CONST.DEFAULT_MOVIE_ATTR_SEQ;
        
        BEGIN
            
            -- 未認証ﾌﾗｸﾞの値を算出
            IF (REC_WOMAN_KEIJI.V_BBS='1') THEN
                BUF_CLOSED_FLAG := 0;
            ELSE
                BUF_CLOSED_FLAG := 1;
            END IF;
            
            IF    (REC_WOMAN_KEIJI.BBSID = 2  ) THEN
				IF (BUF_CLOSED_FLAG = 0) THEN
	                /*********************************/
	                -- T_CAST_BBS(ｷｬｽﾄ掲示板) 
	                /*********************************/
	                INSERT INTO T_CAST_BBS(
	                    SITE_CD                     ,
	                    USER_SEQ                    ,
	                    USER_CHAR_NO                ,
	                    BBS_SEQ                     ,
	                    BBS_TITLE                   ,
	                    BBS_DOC                     ,
	                    CREATE_DATE                 ,
	                    READING_COUNT               
	                )VALUES(
	                    BUF_SITE_CD                 	,
	                    REC_WOMAN_KEIJI.USER_SEQ    	,
	                    REC_WOMAN_KEIJI.USER_CHAR_NO	,
	                    VICOMM_CREA.SEQ_OBJ.NEXTVAL		,
	                    REC_WOMAN_KEIJI.TITLE       	,
	                    REC_WOMAN_KEIJI.BODY        	,
	                    REC_WOMAN_KEIJI.WRITEDATE   	,
	                    REC_WOMAN_KEIJI.READNUM
	                );
				ELSE
	                INSERT INTO T_CAST_BBS(
	                    SITE_CD                     ,
	                    USER_SEQ                    ,
	                    USER_CHAR_NO                ,
	                    BBS_SEQ                     ,
	                    BBS_TITLE                   ,
	                    BBS_DOC                     ,
	                    CREATE_DATE                 ,
	                    READING_COUNT    			, 
						DEL_FLAG					,
						ADMIN_DEL_FLAG
	                )VALUES(
	                    BUF_SITE_CD                 	,
	                    REC_WOMAN_KEIJI.USER_SEQ    	,
	                    REC_WOMAN_KEIJI.USER_CHAR_NO	,
	                    VICOMM_CREA.SEQ_OBJ.NEXTVAL		,
	                    REC_WOMAN_KEIJI.TITLE       	,
	                    REC_WOMAN_KEIJI.BODY        	,
	                    REC_WOMAN_KEIJI.WRITEDATE   	,
	                    REC_WOMAN_KEIJI.READNUM			,
						1								,
						1
					);
				END IF;
            ELSIF (REC_WOMAN_KEIJI.BBSID = 200) THEN
                /*********************************/
                -- T_CAST_PIC(ｷｬｽﾄ写真) 
                /*********************************/
                OPEN CS_CAST(REC_WOMAN_KEIJI.USER_SEQ);
				FETCH CS_CAST INTO REC_CAST;
				IF CS_CAST%FOUND THEN

	                SELECT VICOMM_CREA.SEQ_OBJ.NEXTVAL INTO BUF_PIC_SEQ FROM DUAL;
	                
	                IF (REC_WOMAN_KEIJI.CAST_PIC_ATTR_TYPE_SEQ IS NOT NULL) THEN
	                    BUF_PIC_ATTR_TYPE_SEQ := REC_WOMAN_KEIJI.CAST_PIC_ATTR_TYPE_SEQ;
	                END IF;
	                IF (REC_WOMAN_KEIJI.CAST_PIC_ATTR_SEQ IS NOT NULL) THEN
	                    BUF_PIC_ATTR_SEQ := REC_WOMAN_KEIJI.CAST_PIC_ATTR_SEQ;
	                END IF;
	                
	                INSERT INTO T_CAST_PIC(
	                    SITE_CD                     ,
	                    USER_SEQ                    ,
	                    USER_CHAR_NO                ,
	                    PIC_SEQ                     ,
	                    PROFILE_PIC_FLAG            ,
	                    UPLOAD_DATE                 ,
	                    UPDATE_DATE                 ,
	                    REVISION_NO                 ,
	                    PIC_TYPE                    ,
	                    UNAUTH_FLAG                 ,
	                    PIC_TITLE                   ,
	                    PIC_DOC                     ,
	                    CAST_PIC_ATTR_TYPE_SEQ      ,
	                    CAST_PIC_ATTR_SEQ           ,
	                    READING_COUNT               ,
						CLOSED_FLAG					,
						CLIPHO_ID
	                )VALUES(
	                    BUF_SITE_CD                 ,
	                    REC_WOMAN_KEIJI.USER_SEQ    ,
	                    REC_WOMAN_KEIJI.USER_CHAR_NO,
	                    BUF_PIC_SEQ                 ,
	                    0                           ,
	                    REC_WOMAN_KEIJI.WRITEDATE   ,
	                    REC_WOMAN_KEIJI.WRITEDATE   ,
	                    VICOMM_CREA.SEQ_REVISION_NO.NEXTVAL,
	                    VICOMM_CONST.ATTACHED_BBS   ,
	                    0				             ,
	                    REC_WOMAN_KEIJI.TITLE       ,
	                    REC_WOMAN_KEIJI.BODY        ,
	                    BUF_PIC_ATTR_TYPE_SEQ       ,
	                    BUF_PIC_ATTR_SEQ            ,
	                    REC_WOMAN_KEIJI.READNUM     ,
						BUF_CLOSED_FLAG				,
						REC_WOMAN_KEIJI.KID
	                );
	                
	                -- 画像ファイルのコピー用にバッチファイルを出力しておく
					UTL_FILE.PUT_LINE(download_pic,'wshaimg/buf/' || REC_WOMAN_KEIJI.PICNAME);

					BUF_BAT_DTL := 	'CALL E:\i-Brid\MailTools\CREA\MediaConvertImage.bat A001 "E:\i-Brid\Site\ViComm-CREA\FtpClient\wshaimg\buf\" "E:\i-Brid\Site\ViComm-CREA\DATA\A001\OPERATOR\' ||
									REC_CAST.LOGIN_ID || '" "' || LPAD(BUF_PIC_SEQ,15,'0') || '" "' || REC_WOMAN_KEIJI.PICNAME || '"';
	                UTL_FILE.PUT_LINE(bat_pic,BUF_BAT_DTL);

					BUF_PIC_BAT_COUNT := BUF_PIC_BAT_COUNT + 1;

					IF (BUF_PIC_BAT_COUNT > 5000) THEN
						BUF_PIC_BAT_COUNT := 0;
						UTL_FILE.FCLOSE(bat_pic);
						DBMS_LOCK.SLEEP(1);
						bat_pic         := UTL_FILE.FOPEN('D:\BAT'      ,'T_CAST_BBS_PIC_' || TO_CHAR(SYSDATE,'YYYYMMDDHH24MISS') || '.BAT'     ,'W', 32767);
						
					END IF;
				END IF;
				CLOSE CS_CAST;

            ELSIF (REC_WOMAN_KEIJI.BBSID = 500) THEN
                /*********************************/
                -- T_CAST_MOVIE(ｷｬｽﾄ動画) 
                /*********************************/
                OPEN CS_CAST(REC_WOMAN_KEIJI.USER_SEQ);
				FETCH CS_CAST INTO REC_CAST;
				IF CS_CAST%FOUND THEN
                
	                IF (REC_WOMAN_KEIJI.CAST_MOVIE_ATTR_TYPE_SEQ IS NOT NULL) THEN
	                    BUF_MOVIE_ATTR_TYPE_SEQ := REC_WOMAN_KEIJI.CAST_MOVIE_ATTR_TYPE_SEQ;
	                END IF;
	                IF (REC_WOMAN_KEIJI.CAST_MOVIE_ATTR_SEQ IS NOT NULL) THEN
	                    BUF_MOVIE_ATTR_SEQ := REC_WOMAN_KEIJI.CAST_MOVIE_ATTR_SEQ;
	                END IF;

					--AMCは移行しないでOK
					IF (REC_WOMAN_KEIJI.MOVIETYPE = '3gp') THEN
	                
		                SELECT VICOMM_CREA.SEQ_OBJ.NEXTVAL INTO BUF_MOVIE_SEQ FROM DUAL;
		                
		                INSERT INTO T_CAST_MOVIE(
		                    SITE_CD                         ,
		                    USER_SEQ                        ,
		                    USER_CHAR_NO                    ,
		                    MOVIE_SEQ                       ,
		                    MOVIE_TITLE                     ,
		                    UNAUTH_FLAG                     ,
		                    UPLOAD_DATE                     ,
		                    UPDATE_DATE                     ,
		                    REVISION_NO                     ,
		                    MOVIE_TYPE                      ,
		                    MOVIE_DOC                       ,
		                    CAST_MOVIE_ATTR_TYPE_SEQ        ,
		                    CAST_MOVIE_ATTR_SEQ             ,
		                    READING_COUNT                   ,
							CLOSED_FLAG						,
							CLIPHO_ID
		                )VALUES(
		                    BUF_SITE_CD                     	,
		                    REC_WOMAN_KEIJI.USER_SEQ        	,
		                    REC_WOMAN_KEIJI.USER_CHAR_NO    	,
		                    BUF_MOVIE_SEQ                   	,
		                    REC_WOMAN_KEIJI.TITLE           	,
		                    0				                	,
		                    REC_WOMAN_KEIJI.WRITEDATE       	,
		                    REC_WOMAN_KEIJI.WRITEDATE       	,
		                    VICOMM_CREA.SEQ_REVISION_NO.NEXTVAL,
		                    VICOMM_CONST.ATTACHED_BBS       	,
		                    REC_WOMAN_KEIJI.BODY            	,
		                    BUF_MOVIE_ATTR_TYPE_SEQ         	,
		                    BUF_MOVIE_ATTR_SEQ              	,
		                    REC_WOMAN_KEIJI.READNUM         	,
							BUF_CLOSED_FLAG						,
							REC_WOMAN_KEIJI.KID
		                );
		                
		                -- 動画ファイルのコピー用にバッチファイルを出力しておく
						UTL_FILE.PUT_LINE(download_movie,'wbbsmovie/3gp/' || REC_WOMAN_KEIJI.PICNAME || '.3gp');
						UTL_FILE.PUT_LINE(download_movie,'wbbsmovie/3g2/' || REC_WOMAN_KEIJI.PICNAME || '.3g2');

		                BUF_BAT_DTL := 'copy ' || BUF_MOVIE_SRC_DIR || '3gp\' || REC_WOMAN_KEIJI.PICNAME || '.3gp ' || BUF_WEB_DIR || '\MOVIE\' || BUF_SITE_CD || '\Operator\' || REC_CAST.LOGIN_ID || '\' || LPAD(BUF_MOVIE_SEQ,15,'0') || '.3gp';
		                UTL_FILE.PUT_LINE(bat_movie,BUF_BAT_DTL);
		                BUF_BAT_DTL := 'copy ' || BUF_MOVIE_SRC_DIR || '3g2\' || REC_WOMAN_KEIJI.PICNAME || '.3g2 ' || BUF_WEB_DIR || '\MOVIE\' || BUF_SITE_CD || '\Operator\' || REC_CAST.LOGIN_ID || '\' || LPAD(BUF_MOVIE_SEQ,15,'0') || '.3g2';
		                UTL_FILE.PUT_LINE(bat_movie,BUF_BAT_DTL);
					END IF;
				END IF;
				CLOSE CS_CAST;

            ELSE
                UTL_FILE.PUT_LINE(fh,REC_WOMAN_KEIJI.KID || ',' || -99999 || ',' || 'BBSIDが不正です。');
            END IF;

        EXCEPTION
            WHEN OTHERS THEN
                UTL_FILE.PUT_LINE(fh,REC_WOMAN_KEIJI.KID || ',' || SQLCODE);
				IF CS_CAST%ISOPEN THEN
					CLOSE CS_CAST;
				END IF;
        END;
    END LOOP;

    UTL_FILE.FCLOSE(fh);
    UTL_FILE.FCLOSE(bat_pic);
    UTL_FILE.FCLOSE(bat_movie);
    UTL_FILE.FCLOSE(download_pic);
    UTL_FILE.FCLOSE(download_movie);

EXCEPTION
    WHEN OTHERS THEN
        UTL_FILE.PUT_LINE(fh,'UNKNOWN' || ',' || SQLCODE);

		IF CS_CAST%ISOPEN THEN
			CLOSE CS_CAST;
		END IF;

        UTL_FILE.FCLOSE(fh);
        UTL_FILE.FCLOSE(bat_pic);
        UTL_FILE.FCLOSE(bat_movie);
	    UTL_FILE.FCLOSE(download_pic);
	    UTL_FILE.FCLOSE(download_movie);
END;
/
COMMIT;
/
TIMING STOP

