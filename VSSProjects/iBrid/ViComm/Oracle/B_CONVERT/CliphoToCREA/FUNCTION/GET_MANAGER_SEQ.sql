/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: システム管理
--  Title			: マネージャーSEQ取得
--	Progaram ID		: GET_MANAGER_SEQ
--	Compile Turn	: 0
--  Creation Date	: 10.06.11
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
CREATE OR REPLACE FUNCTION GET_MANAGER_SEQ
	RETURN	NUMBER
IS
BEGIN

	RETURN 1;

EXCEPTION
	WHEN OTHERS THEN

		RETURN	1;

END GET_MANAGER_SEQ;
/
SHOW ERROR;
