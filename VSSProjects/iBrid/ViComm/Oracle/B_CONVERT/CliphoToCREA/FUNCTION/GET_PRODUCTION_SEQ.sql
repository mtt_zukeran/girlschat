/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: システム管理
--  Title			: プロダクションSEQ取得
--	Progaram ID		: GET_PRODUCTION_SEQ
--	Compile Turn	: 0
--  Creation Date	: 10.06.11
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
CREATE OR REPLACE FUNCTION GET_PRODUCTION_SEQ
	RETURN	NUMBER
IS
BEGIN

	RETURN 100;

EXCEPTION
	WHEN OTHERS THEN

		RETURN	100;

END GET_PRODUCTION_SEQ;
/
SHOW ERROR;
