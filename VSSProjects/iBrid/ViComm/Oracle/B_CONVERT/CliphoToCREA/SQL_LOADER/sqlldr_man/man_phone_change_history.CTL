OPTIONS(LOAD=-1,ERRORS=-1,ROWS=1000000,BINDSIZE=1024000,DIRECT=Y)
LOAD DATA
INFILE 'D:\cnv\man_phone_change_history.csv.new'
BADFILE 'D:\cnv\man_phone_change_history.bad'
TRUNCATE
INTO TABLE man_phone_change_history
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(
userid       ,
cid          ,
former_phone CHAR(4000) "REPLACE( REPLACE(:former_phone,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
php          CHAR(4000) "REPLACE( REPLACE(:php,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
modify_date  DATE(19) "YYYY-MM-DD HH24:MI:SS" NULLIF modify_date="0000-00-00 00:00:00"  
)

