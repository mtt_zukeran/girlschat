OPTIONS(LOAD=-1,ERRORS=-1,ROWS=1000000,BINDSIZE=1024000,DIRECT=Y)
LOAD DATA
INFILE 'D:\cnv\woman_mail_picmanage.csv.new'
BADFILE 'D:\cnv\woman_mail_picmanage.bad'
TRUNCATE
INTO TABLE woman_mail_picmanage
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(
id        ,
userid    ,
auid      ,
dpicid    ,
writedate  DATE(19) "YYYY-MM-DD HH24:MI:SS" NULLIF writedate="0000-00-00 00:00:00"  , 
mes_id    
)

