OPTIONS(LOAD=-1,ERRORS=-1,ROWS=1000000,BINDSIZE=1024000,DIRECT=Y)
LOAD DATA
INFILE 'D:\cnv\woman_cookie_manage.csv.new'
BADFILE 'D:\cnv\woman_cookie_manage.bad'
TRUNCATE
INTO TABLE woman_cookie_manage
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(
id        ,
userid    ,
ckey       CHAR(4000) "REPLACE( REPLACE(:ckey,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
writedate  DATE(19) "YYYY-MM-DD HH24:MI:SS" NULLIF writedate="0000-00-00 00:00:00" 
)

