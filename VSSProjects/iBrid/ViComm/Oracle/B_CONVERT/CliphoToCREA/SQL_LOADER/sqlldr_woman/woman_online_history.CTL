OPTIONS(LOAD=-1,ERRORS=-1,ROWS=1000000,BINDSIZE=1024000,DIRECT=Y)
LOAD DATA
INFILE 'D:\cnv\woman_online_history.csv.new'
BADFILE 'D:\cnv\woman_online_history.bad'
TRUNCATE
INTO TABLE woman_online_history
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(
userid   , 
histid   , 
status   , 
writedate DATE(19) "YYYY-MM-DD HH24:MI:SS" NULLIF writedate="0000-00-00 00:00:00"  , 
modifydate DATE(19) "YYYY-MM-DD HH24:MI:SS" NULLIF modifydate="0000-00-00 00:00:00" 
)

