OPTIONS(LOAD=-1,ERRORS=-1,ROWS=1000000,BINDSIZE=1024000,DIRECT=Y)
LOAD DATA
INFILE 'D:\cnv\woman_temp_regist.csv.new'
BADFILE 'D:\cnv\woman_temp_regist.bad'
TRUNCATE
INTO TABLE woman_temp_regist
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(
id        ,
utn        CHAR(4000) "REPLACE( REPLACE(:utn,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
loginpass  CHAR(4000) "REPLACE( REPLACE(:loginpass,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
mail       CHAR(4000) "REPLACE( REPLACE(:mail,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
writedate  DATE(19) "YYYY-MM-DD HH24:MI:SS" NULLIF writedate="0000-00-00 00:00:00"  , 
age       ,
name       CHAR(4000) "REPLACE( REPLACE(:name,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
banner     CHAR(4000) "REPLACE( REPLACE(:banner,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
comment#   CHAR(4000) "REPLACE( REPLACE(:comment#,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))" 
)

