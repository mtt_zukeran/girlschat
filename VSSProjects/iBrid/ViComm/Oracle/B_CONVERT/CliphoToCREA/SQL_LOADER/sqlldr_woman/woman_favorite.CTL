OPTIONS(LOAD=-1,ERRORS=-1,ROWS=1000000,BINDSIZE=1024000,DIRECT=Y)
LOAD DATA
INFILE 'D:\cnv\woman_favorite.csv.new'
BADFILE 'D:\cnv\woman_favorite.bad'
TRUNCATE
INTO TABLE woman_favorite
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(
auid   , 
psend   , 
preceive   , 
pflg   , 
modifydate DATE(19) "YYYY-MM-DD HH24:MI:SS" NULLIF modifydate="0000-00-00 00:00:00"  , 
registdate DATE(19) "YYYY-MM-DD HH24:MI:SS" NULLIF registdate="0000-00-00 00:00:00"  , 
userid  
)

