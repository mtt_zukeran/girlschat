OPTIONS(LOAD=-1,ERRORS=-1,ROWS=1000000,BINDSIZE=20971520,readsize=20971520)
LOAD DATA
INFILE 'D:\cnv\mpvlog_log201101.csv.new'
BADFILE 'D:\cnv\mpvlog_log201101.bad'
APPEND
INTO TABLE mpvlog_logyyyymm
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(
logid   , 
bannercode  CHAR(4000) "REPLACE( REPLACE(:bannercode,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
logdate  DATE(19) "YYYY-MM-DD HH24:MI:SS" NULLIF logdate="0000-00-00 00:00:00"  ,
landingurl  CHAR(4000) "REPLACE( REPLACE(:landingurl,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
machine   CHAR(4000) "REPLACE( REPLACE(:machine,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))" 
)

