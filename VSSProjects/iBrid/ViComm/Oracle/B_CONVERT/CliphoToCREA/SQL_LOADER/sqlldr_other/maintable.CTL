OPTIONS(LOAD=-1,ERRORS=-1,ROWS=1000000,BINDSIZE=1024000,DIRECT=Y)
LOAD DATA
INFILE 'D:\cnv\maintable.csv.new'
BADFILE 'D:\cnv\maintable.bad'
TRUNCATE
INTO TABLE maintable
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(
userid   , 
usedpoint   , 
point   , 
limitpoint   , 
modifydate DATE(19) "YYYY-MM-DD HH24:MI:SS" NULLIF modifydate="0000-00-00 00:00:00"  , 
session_no   , 
sex CHAR(4000) "REPLACE( REPLACE(:sex,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
kind CHAR(4000) "REPLACE( REPLACE(:kind,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
talkflg   , 
logoutdate DATE(19) "YYYY-MM-DD HH24:MI:SS" NULLIF logoutdate="0000-00-00 00:00:00"  , 
loginkind   , 
logout   , 
waitingkind   , 
waitingkindforsort   , 
sortopt   , 
mail_unread_num   , 
unread_chk_date DATE(19) "YYYY-MM-DD HH24:MI:SS" NULLIF unread_chk_date="0000-00-00 00:00:00" 
)

