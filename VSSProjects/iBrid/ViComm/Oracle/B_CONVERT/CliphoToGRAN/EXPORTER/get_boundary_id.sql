SET LINESIZE 9999
SELECT 
--woman
	(SELECT MAX(g_picid)	FROM woman_gallery_pic			)AS woman_gallery_pic		,
	(SELECT MAX(infoid) 	FROM woman_information		    )AS woman_information		,
	(SELECT MAX(kid) 		FROM woman_keiji				)AS woman_keiji				,
	(SELECT MAX(mes_id) 	FROM woman_mailbox 			    )AS woman_mailbox 			,
	(SELECT MAX(mid) 		FROM woman_ownermail_history 	)AS woman_ownermail_history ,
	(SELECT MAX(mdetailid) 	FROM woman_ownermail_detail 	)AS woman_ownermail_detail 	,
	(SELECT MAX(g_picid) 	FROM woman_profmovie			)AS woman_profmovie			,
--man                                                                                
	(SELECT MAX(buyid)		FROM man_ad_action 				)AS man_ad_action 			,
	(SELECT MAX(buyid)		FROM man_hanbai 				)AS man_hanbai 				,
	(SELECT MAX(infoid)		FROM man_information 			)AS man_information 		,
	(SELECT MAX(kid)		FROM man_keiji 					)AS man_keiji 				,
	(SELECT MAX(mes_id)		FROM man_mailbox 				)AS man_mailbox 			,
	(SELECT MAX(mid)		FROM man_ownermail_history 		)AS man_ownermail_history 	,
	(SELECT MAX(mdetailid)	FROM man_ownermail_detail 		)AS man_ownermail_detail 	,
--mlog
	(SELECT MAX(logid) 	FROM mlog_logYYYYMM			WHERE MONTH=(SELECT MAX(MONTH) 		FROM mlog_logYYYYMM			)	)AS mlog_logYYYYMM,
-- tvphonelog
	(SELECT MAX(cid_seq)	FROM tvphonelog_logYYYYMM 	WHERE MONTH=(SELECT MAX(MONTH) 		FROM tvphonelog_logYYYYMM	)	)AS tvphonelog_logYYYYMM,
--wlog
	(SELECT MAX(logid) 	FROM wlog_logYYYYMM 		WHERE MONTH=(SELECT MAX(MONTH) 		FROM wlog_logYYYYMM			)	)AS wlog_logYYYYMM
FROM
	DUAL;
