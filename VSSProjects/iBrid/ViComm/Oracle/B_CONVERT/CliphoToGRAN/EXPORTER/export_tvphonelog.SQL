use tvphonelog;

/***********  Diff Export **************/	
/*
set @cid_seq		= 0;
set @cid_seq 	= (SELECT IFNULL(MIN(t.cid_seq),@cid_seq) FROM (SELECT cid_seq FROM log201110 WHERE cid_seq<=@cid_seq ORDER BY cid_seq DESC LIMIT 5000) t);
SELECT CONCAT('boundary tvphonelog_cid_seq:',@cid_seq);
SELECT
	uid,
	REPLACE(REPLACE(uphp,"\n","<BR>"),"\r","<BR>"),
	auid,
	REPLACE(REPLACE(aphp,"\n","<BR>"),"\r","<BR>"),
	REPLACE(REPLACE(kind,"\n","<BR>"),"\r","<BR>"),
	connecttime,
	postdate,
	call_startdate,
	call_enddate,
	usedpoint_time,
	callingkind,
	waitingkind,
	m_remainpoint,
	agent_kind,
	REPLACE(REPLACE(machine,"\n","<BR>"),"\r","<BR>"),
	status,
	end_status,
	REPLACE(REPLACE(cid,"\n","<BR>"),"\r","<BR>"),
	cid_seq,
	use_freedial
	FROM log201110 
	WHERE cid_seq>@cid_seq
	INTO OUTFILE "/home/ibrid/tvphonelog_log201110.csv" FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '"'; 
*/

/***********  Full Export **************/		


SELECT 
	logid,
	uid,
	REPLACE(REPLACE(cid,"\n","<BR>"),"\r","<BR>"),
	REPLACE(REPLACE(data,"\n","<BR>"),"\r","<BR>"),
	request_sec,
	result_sec,
	check_flg,
	exec_flg,
	writedate,
	execdate,
	auid,
	auid_sec
	FROM sunall_sts6log 
	INTO OUTFILE "/home/ibrid/sunall_sts6log.csv" FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '"'; 

SELECT 
	uid,
	REPLACE(REPLACE(uphp,"\n","<BR>"),"\r","<BR>"),
	auid,
	REPLACE(REPLACE(aphp,"\n","<BR>"),"\r","<BR>"),
	REPLACE(REPLACE(kind,"\n","<BR>"),"\r","<BR>"),
	connecttime,
	postdate,
	call_startdate,
	call_enddate,
	usedpoint_time,
	callingkind,
	waitingkind,
	m_remainpoint,
	agent_kind,
	REPLACE(REPLACE(machine,"\n","<BR>"),"\r","<BR>"),
	status,
	end_status,
	REPLACE(REPLACE(cid,"\n","<BR>"),"\r","<BR>"),
	cid_seq,
	use_whiteplan
FROM log_confirm 
	INTO OUTFILE "/home/ibrid/log_confirm.csv" FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '"'; 


SELECT 
	uid,
	REPLACE(REPLACE(uphp,"\n","<BR>"),"\r","<BR>"),
	auid,
	REPLACE(REPLACE(aphp,"\n","<BR>"),"\r","<BR>"),
	REPLACE(REPLACE(kind,"\n","<BR>"),"\r","<BR>"),
	connecttime,
	postdate,
	call_startdate,
	call_enddate,
	usedpoint_time,
	callingkind,
	waitingkind,
	m_remainpoint,
	agent_kind,
	REPLACE(REPLACE(machine,"\n","<BR>"),"\r","<BR>"),
	status,
	end_status,
	REPLACE(REPLACE(cid,"\n","<BR>"),"\r","<BR>"),
	cid_seq,
	use_freedial
FROM log200806 
	INTO OUTFILE "/home/ibrid/tvphonelog_log200806.csv" FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '"'; 

SELECT 
	uid,
	REPLACE(REPLACE(uphp,"\n","<BR>"),"\r","<BR>"),
	auid,
	REPLACE(REPLACE(aphp,"\n","<BR>"),"\r","<BR>"),
	REPLACE(REPLACE(kind,"\n","<BR>"),"\r","<BR>"),
	connecttime,
	postdate,
	call_startdate,
	call_enddate,
	usedpoint_time,
	callingkind,
	waitingkind,
	m_remainpoint,
	agent_kind,
	REPLACE(REPLACE(machine,"\n","<BR>"),"\r","<BR>"),
	status,
	end_status,
	REPLACE(REPLACE(cid,"\n","<BR>"),"\r","<BR>"),
	cid_seq,
	use_freedial
FROM log201004 
	INTO OUTFILE "/home/ibrid/tvphonelog_log201004.csv" FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '"'; 

SELECT 
	uid,
	REPLACE(REPLACE(uphp,"\n","<BR>"),"\r","<BR>"),
	auid,
	REPLACE(REPLACE(aphp,"\n","<BR>"),"\r","<BR>"),
	REPLACE(REPLACE(kind,"\n","<BR>"),"\r","<BR>"),
	connecttime,
	postdate,
	call_startdate,
	call_enddate,
	usedpoint_time,
	callingkind,
	waitingkind,
	m_remainpoint,
	agent_kind,
	REPLACE(REPLACE(machine,"\n","<BR>"),"\r","<BR>"),
	status,
	end_status,
	REPLACE(REPLACE(cid,"\n","<BR>"),"\r","<BR>"),
	cid_seq,
	use_freedial
FROM log201005 
	INTO OUTFILE "/home/ibrid/tvphonelog_log201005.csv" FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '"'; 

SELECT 
	uid,
	REPLACE(REPLACE(uphp,"\n","<BR>"),"\r","<BR>"),
	auid,
	REPLACE(REPLACE(aphp,"\n","<BR>"),"\r","<BR>"),
	REPLACE(REPLACE(kind,"\n","<BR>"),"\r","<BR>"),
	connecttime,
	postdate,
	call_startdate,
	call_enddate,
	usedpoint_time,
	callingkind,
	waitingkind,
	m_remainpoint,
	agent_kind,
	REPLACE(REPLACE(machine,"\n","<BR>"),"\r","<BR>"),
	status,
	end_status,
	REPLACE(REPLACE(cid,"\n","<BR>"),"\r","<BR>"),
	cid_seq,
	use_freedial
FROM log201006 
	INTO OUTFILE "/home/ibrid/tvphonelog_log201006.csv" FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '"'; 

SELECT 
	uid,
	REPLACE(REPLACE(uphp,"\n","<BR>"),"\r","<BR>"),
	auid,
	REPLACE(REPLACE(aphp,"\n","<BR>"),"\r","<BR>"),
	REPLACE(REPLACE(kind,"\n","<BR>"),"\r","<BR>"),
	connecttime,
	postdate,
	call_startdate,
	call_enddate,
	usedpoint_time,
	callingkind,
	waitingkind,
	m_remainpoint,
	agent_kind,
	REPLACE(REPLACE(machine,"\n","<BR>"),"\r","<BR>"),
	status,
	end_status,
	REPLACE(REPLACE(cid,"\n","<BR>"),"\r","<BR>"),
	cid_seq,
	use_freedial
FROM log201007 
	INTO OUTFILE "/home/ibrid/tvphonelog_log201007.csv" FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '"'; 

SELECT 
	uid,
	REPLACE(REPLACE(uphp,"\n","<BR>"),"\r","<BR>"),
	auid,
	REPLACE(REPLACE(aphp,"\n","<BR>"),"\r","<BR>"),
	REPLACE(REPLACE(kind,"\n","<BR>"),"\r","<BR>"),
	connecttime,
	postdate,
	call_startdate,
	call_enddate,
	usedpoint_time,
	callingkind,
	waitingkind,
	m_remainpoint,
	agent_kind,
	REPLACE(REPLACE(machine,"\n","<BR>"),"\r","<BR>"),
	status,
	end_status,
	REPLACE(REPLACE(cid,"\n","<BR>"),"\r","<BR>"),
	cid_seq,
	use_freedial
FROM log201008 
	INTO OUTFILE "/home/ibrid/tvphonelog_log201008.csv" FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '"'; 

SELECT 
	uid,
	REPLACE(REPLACE(uphp,"\n","<BR>"),"\r","<BR>"),
	auid,
	REPLACE(REPLACE(aphp,"\n","<BR>"),"\r","<BR>"),
	REPLACE(REPLACE(kind,"\n","<BR>"),"\r","<BR>"),
	connecttime,
	postdate,
	call_startdate,
	call_enddate,
	usedpoint_time,
	callingkind,
	waitingkind,
	m_remainpoint,
	agent_kind,
	REPLACE(REPLACE(machine,"\n","<BR>"),"\r","<BR>"),
	status,
	end_status,
	REPLACE(REPLACE(cid,"\n","<BR>"),"\r","<BR>"),
	cid_seq,
	use_freedial
FROM log201009 
	INTO OUTFILE "/home/ibrid/tvphonelog_log201009.csv" FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '"'; 

SELECT 
	uid,
	REPLACE(REPLACE(uphp,"\n","<BR>"),"\r","<BR>"),
	auid,
	REPLACE(REPLACE(aphp,"\n","<BR>"),"\r","<BR>"),
	REPLACE(REPLACE(kind,"\n","<BR>"),"\r","<BR>"),
	connecttime,
	postdate,
	call_startdate,
	call_enddate,
	usedpoint_time,
	callingkind,
	waitingkind,
	m_remainpoint,
	agent_kind,
	REPLACE(REPLACE(machine,"\n","<BR>"),"\r","<BR>"),
	status,
	end_status,
	REPLACE(REPLACE(cid,"\n","<BR>"),"\r","<BR>"),
	cid_seq,
	use_freedial
FROM log201010 
	INTO OUTFILE "/home/ibrid/tvphonelog_log201010.csv" FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '"'; 

SELECT 
	uid,
	REPLACE(REPLACE(uphp,"\n","<BR>"),"\r","<BR>"),
	auid,
	REPLACE(REPLACE(aphp,"\n","<BR>"),"\r","<BR>"),
	REPLACE(REPLACE(kind,"\n","<BR>"),"\r","<BR>"),
	connecttime,
	postdate,
	call_startdate,
	call_enddate,
	usedpoint_time,
	callingkind,
	waitingkind,
	m_remainpoint,
	agent_kind,
	REPLACE(REPLACE(machine,"\n","<BR>"),"\r","<BR>"),
	status,
	end_status,
	REPLACE(REPLACE(cid,"\n","<BR>"),"\r","<BR>"),
	cid_seq,
	use_freedial
FROM log201011 
	INTO OUTFILE "/home/ibrid/tvphonelog_log201011.csv" FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '"'; 

SELECT 
	uid,
	REPLACE(REPLACE(uphp,"\n","<BR>"),"\r","<BR>"),
	auid,
	REPLACE(REPLACE(aphp,"\n","<BR>"),"\r","<BR>"),
	REPLACE(REPLACE(kind,"\n","<BR>"),"\r","<BR>"),
	connecttime,
	postdate,
	call_startdate,
	call_enddate,
	usedpoint_time,
	callingkind,
	waitingkind,
	m_remainpoint,
	agent_kind,
	REPLACE(REPLACE(machine,"\n","<BR>"),"\r","<BR>"),
	status,
	end_status,
	REPLACE(REPLACE(cid,"\n","<BR>"),"\r","<BR>"),
	cid_seq,
	use_freedial
FROM log201012 
	INTO OUTFILE "/home/ibrid/tvphonelog_log201012.csv" FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '"'; 

SELECT 
	uid,
	REPLACE(REPLACE(uphp,"\n","<BR>"),"\r","<BR>"),
	auid,
	REPLACE(REPLACE(aphp,"\n","<BR>"),"\r","<BR>"),
	REPLACE(REPLACE(kind,"\n","<BR>"),"\r","<BR>"),
	connecttime,
	postdate,
	call_startdate,
	call_enddate,
	usedpoint_time,
	callingkind,
	waitingkind,
	m_remainpoint,
	agent_kind,
	REPLACE(REPLACE(machine,"\n","<BR>"),"\r","<BR>"),
	status,
	end_status,
	REPLACE(REPLACE(cid,"\n","<BR>"),"\r","<BR>"),
	cid_seq,
	use_freedial
FROM log201101 
	INTO OUTFILE "/home/ibrid/tvphonelog_log201101.csv" FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '"'; 

SELECT 
	uid,
	REPLACE(REPLACE(uphp,"\n","<BR>"),"\r","<BR>"),
	auid,
	REPLACE(REPLACE(aphp,"\n","<BR>"),"\r","<BR>"),
	REPLACE(REPLACE(kind,"\n","<BR>"),"\r","<BR>"),
	connecttime,
	postdate,
	call_startdate,
	call_enddate,
	usedpoint_time,
	callingkind,
	waitingkind,
	m_remainpoint,
	agent_kind,
	REPLACE(REPLACE(machine,"\n","<BR>"),"\r","<BR>"),
	status,
	end_status,
	REPLACE(REPLACE(cid,"\n","<BR>"),"\r","<BR>"),
	cid_seq,
	use_freedial
FROM log201102 
	INTO OUTFILE "/home/ibrid/tvphonelog_log201102.csv" FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '"'; 

SELECT 
	uid,
	REPLACE(REPLACE(uphp,"\n","<BR>"),"\r","<BR>"),
	auid,
	REPLACE(REPLACE(aphp,"\n","<BR>"),"\r","<BR>"),
	REPLACE(REPLACE(kind,"\n","<BR>"),"\r","<BR>"),
	connecttime,
	postdate,
	call_startdate,
	call_enddate,
	usedpoint_time,
	callingkind,
	waitingkind,
	m_remainpoint,
	agent_kind,
	REPLACE(REPLACE(machine,"\n","<BR>"),"\r","<BR>"),
	status,
	end_status,
	REPLACE(REPLACE(cid,"\n","<BR>"),"\r","<BR>"),
	cid_seq,
	use_freedial
FROM log201103 
	INTO OUTFILE "/home/ibrid/tvphonelog_log201103.csv" FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '"'; 

SELECT 
	uid,
	REPLACE(REPLACE(uphp,"\n","<BR>"),"\r","<BR>"),
	auid,
	REPLACE(REPLACE(aphp,"\n","<BR>"),"\r","<BR>"),
	REPLACE(REPLACE(kind,"\n","<BR>"),"\r","<BR>"),
	connecttime,
	postdate,
	call_startdate,
	call_enddate,
	usedpoint_time,
	callingkind,
	waitingkind,
	m_remainpoint,
	agent_kind,
	REPLACE(REPLACE(machine,"\n","<BR>"),"\r","<BR>"),
	status,
	end_status,
	REPLACE(REPLACE(cid,"\n","<BR>"),"\r","<BR>"),
	cid_seq,
	use_freedial
FROM log201104 
	INTO OUTFILE "/home/ibrid/tvphonelog_log201104.csv" FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '"'; 

SELECT 
	uid,
	REPLACE(REPLACE(uphp,"\n","<BR>"),"\r","<BR>"),
	auid,
	REPLACE(REPLACE(aphp,"\n","<BR>"),"\r","<BR>"),
	REPLACE(REPLACE(kind,"\n","<BR>"),"\r","<BR>"),
	connecttime,
	postdate,
	call_startdate,
	call_enddate,
	usedpoint_time,
	callingkind,
	waitingkind,
	m_remainpoint,
	agent_kind,
	REPLACE(REPLACE(machine,"\n","<BR>"),"\r","<BR>"),
	status,
	end_status,
	REPLACE(REPLACE(cid,"\n","<BR>"),"\r","<BR>"),
	cid_seq,
	use_freedial
FROM log201105 
	INTO OUTFILE "/home/ibrid/tvphonelog_log201105.csv" FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '"'; 

SELECT 
	uid,
	REPLACE(REPLACE(uphp,"\n","<BR>"),"\r","<BR>"),
	auid,
	REPLACE(REPLACE(aphp,"\n","<BR>"),"\r","<BR>"),
	REPLACE(REPLACE(kind,"\n","<BR>"),"\r","<BR>"),
	connecttime,
	postdate,
	call_startdate,
	call_enddate,
	usedpoint_time,
	callingkind,
	waitingkind,
	m_remainpoint,
	agent_kind,
	REPLACE(REPLACE(machine,"\n","<BR>"),"\r","<BR>"),
	status,
	end_status,
	REPLACE(REPLACE(cid,"\n","<BR>"),"\r","<BR>"),
	cid_seq,
	use_freedial
FROM log201106 
	INTO OUTFILE "/home/ibrid/tvphonelog_log201106.csv" FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '"'; 

SELECT 
	uid,
	REPLACE(REPLACE(uphp,"\n","<BR>"),"\r","<BR>"),
	auid,
	REPLACE(REPLACE(aphp,"\n","<BR>"),"\r","<BR>"),
	REPLACE(REPLACE(kind,"\n","<BR>"),"\r","<BR>"),
	connecttime,
	postdate,
	call_startdate,
	call_enddate,
	usedpoint_time,
	callingkind,
	waitingkind,
	m_remainpoint,
	agent_kind,
	REPLACE(REPLACE(machine,"\n","<BR>"),"\r","<BR>"),
	status,
	end_status,
	REPLACE(REPLACE(cid,"\n","<BR>"),"\r","<BR>"),
	cid_seq,
	use_freedial
FROM log201107 
	INTO OUTFILE "/home/ibrid/tvphonelog_log201107.csv" FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '"'; 

SELECT 
	uid,
	REPLACE(REPLACE(uphp,"\n","<BR>"),"\r","<BR>"),
	auid,
	REPLACE(REPLACE(aphp,"\n","<BR>"),"\r","<BR>"),
	REPLACE(REPLACE(kind,"\n","<BR>"),"\r","<BR>"),
	connecttime,
	postdate,
	call_startdate,
	call_enddate,
	usedpoint_time,
	callingkind,
	waitingkind,
	m_remainpoint,
	agent_kind,
	REPLACE(REPLACE(machine,"\n","<BR>"),"\r","<BR>"),
	status,
	end_status,
	REPLACE(REPLACE(cid,"\n","<BR>"),"\r","<BR>"),
	cid_seq,
	use_freedial
FROM log201108 
	INTO OUTFILE "/home/ibrid/tvphonelog_log201108.csv" FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '"'; 

SELECT 
	uid,
	REPLACE(REPLACE(uphp,"\n","<BR>"),"\r","<BR>"),
	auid,
	REPLACE(REPLACE(aphp,"\n","<BR>"),"\r","<BR>"),
	REPLACE(REPLACE(kind,"\n","<BR>"),"\r","<BR>"),
	connecttime,
	postdate,
	call_startdate,
	call_enddate,
	usedpoint_time,
	callingkind,
	waitingkind,
	m_remainpoint,
	agent_kind,
	REPLACE(REPLACE(machine,"\n","<BR>"),"\r","<BR>"),
	status,
	end_status,
	REPLACE(REPLACE(cid,"\n","<BR>"),"\r","<BR>"),
	cid_seq,
	use_freedial
FROM log201109 
	INTO OUTFILE "/home/ibrid/tvphonelog_log201109.csv" FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '"'; 

SELECT 
	uid,
	REPLACE(REPLACE(uphp,"\n","<BR>"),"\r","<BR>"),
	auid,
	REPLACE(REPLACE(aphp,"\n","<BR>"),"\r","<BR>"),
	REPLACE(REPLACE(kind,"\n","<BR>"),"\r","<BR>"),
	connecttime,
	postdate,
	call_startdate,
	call_enddate,
	usedpoint_time,
	callingkind,
	waitingkind,
	m_remainpoint,
	agent_kind,
	REPLACE(REPLACE(machine,"\n","<BR>"),"\r","<BR>"),
	status,
	end_status,
	REPLACE(REPLACE(cid,"\n","<BR>"),"\r","<BR>"),
	cid_seq,
	use_freedial
FROM log201110 
	INTO OUTFILE "/home/ibrid/tvphonelog_log201110.csv" FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '"'; 

SELECT 
	uid,
	REPLACE(REPLACE(uphp,"\n","<BR>"),"\r","<BR>"),
	auid,
	REPLACE(REPLACE(aphp,"\n","<BR>"),"\r","<BR>"),
	REPLACE(REPLACE(kind,"\n","<BR>"),"\r","<BR>"),
	connecttime,
	postdate,
	call_startdate,
	call_enddate,
	usedpoint_time,
	callingkind,
	waitingkind,
	m_remainpoint,
	agent_kind,
	REPLACE(REPLACE(machine,"\n","<BR>"),"\r","<BR>"),
	status,
	end_status,
	REPLACE(REPLACE(cid,"\n","<BR>"),"\r","<BR>"),
	cid_seq,
	use_freedial
FROM log201111 
	INTO OUTFILE "/home/ibrid/tvphonelog_log201111.csv" FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '"'; 

/**/

