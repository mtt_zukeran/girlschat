use maintable;

/***********  Full Export ************************************************/	

SELECT domainid,REPLACE(REPLACE(domain,"\n","<BR>"),"\r","<BR>")
	FROM blackdomain 
	INTO OUTFILE "/home/ibrid/blackdomain.csv" FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '"'; 

SELECT uid,REPLACE(REPLACE(sex,"\n","<BR>"),"\r","<BR>"),betpoint,bettime,gameflag
	FROM blackjack 
	INTO OUTFILE "/home/ibrid/blackjack.csv" FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '"'; 

SELECT roomid,REPLACE(REPLACE(roomname,"\n","<BR>"),"\r","<BR>"),roomstatus,ownerid,partnerid,REPLACE(REPLACE(ownerhandle,"\n","<BR>"),"\r","<BR>"),REPLACE(REPLACE(partnerhandle,"\n","<BR>"),"\r","<BR>"),REPLACE(REPLACE(sex,"\n","<BR>"),"\r","<BR>"),chattime,chatnum,getaway
	FROM chat 
	INTO OUTFILE "/home/ibrid/chat.csv" FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '"'; 

SELECT did,REPLACE(REPLACE(word,"\n","<BR>"),"\r","<BR>"),cflag
	FROM dictionary 
	INTO OUTFILE "/home/ibrid/dictionary.csv" FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '"'; 

SELECT uid,REPLACE(REPLACE(php,"\n","<BR>"),"\r","<BR>"),REPLACE(REPLACE(name,"\n","<BR>"),"\r","<BR>"),REPLACE(REPLACE(mail,"\n","<BR>"),"\r","<BR>"),REPLACE(REPLACE(banner,"\n","<BR>"),"\r","<BR>"),modifydate,pass,REPLACE(REPLACE(sex,"\n","<BR>"),"\r","<BR>")
	FROM joincheck 
	INTO OUTFILE "/home/ibrid/joincheck.csv" FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '"'; 

SELECT uid,usedpoint,point,limitpoint,modifydate,session,REPLACE(REPLACE(sex,"\n","<BR>"),"\r","<BR>"),REPLACE(REPLACE(kind,"\n","<BR>"),"\r","<BR>"),talkflg,logoutdate,loginkind,logout,waitingkind,waitingkindforsort,sortopt,mail_unread_num,ownermail_num,blogmail_unread_num,is_yakimo_free
	FROM maintable 
	INTO OUTFILE "/home/ibrid/maintable.csv" FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '"'; 

