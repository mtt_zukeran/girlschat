OPTIONS(LOAD=-1,ERRORS=-1,ROWS=1000000,BINDSIZE=1024000,DIRECT=Y)
LOAD DATA
INFILE 'D:\cnv\man_friend_mileage_access.csv.new'
BADFILE 'D:\cnv\man_friend_mileage_access.bad'
TRUNCATE
INTO TABLE man_friend_mileage_access
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(
access_id   , 
userid   , 
mailto_genid CHAR(4000) "REPLACE( REPLACE(:mailto_genid,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
machine CHAR(4000) "REPLACE( REPLACE(:machine,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
writedate DATE(19) "YYYY-MM-DD HH24:MI:SS" NULLIF writedate="0000-00-00 00:00:00" 
)

