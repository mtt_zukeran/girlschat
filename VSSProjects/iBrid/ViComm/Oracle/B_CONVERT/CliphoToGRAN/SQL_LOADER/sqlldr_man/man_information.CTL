OPTIONS(LOAD=-1,ERRORS=-1,ROWS=1000000,BINDSIZE=1024000,DIRECT=Y)
LOAD DATA
INFILE 'D:\cnv\man_information.csv.new'
BADFILE 'D:\cnv\man_information.bad'
TRUNCATE
INTO TABLE man_information
FIELDS TERMINATED BY ','
TRAILING NULLCOLS
(
infoid   , 
title CHAR(4000) "REPLACE( REPLACE(:title,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
body1 CHAR(4000) "REPLACE( REPLACE(:body1,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
body2 CHAR(4000) "REPLACE( REPLACE(:body2,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
body3 CHAR(4000) "REPLACE( REPLACE(:body3,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
body4 CHAR(4000) "REPLACE( REPLACE(:body4,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
body5 CHAR(4000) "REPLACE( REPLACE(:body5,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
writedate DATE(19) "YYYY-MM-DD HH24:MI:SS" NULLIF writedate="0000-00-00 00:00:00"  , 
v_bbs   
)

