OPTIONS(LOAD=-1,ERRORS=-1,ROWS=1000000,BINDSIZE=1024000,DIRECT=Y)
LOAD DATA
INFILE 'D:\cnv\man_draft_box.csv.new'
BADFILE 'D:\cnv\man_draft_box.bad'
TRUNCATE
INTO TABLE man_draft_box
FIELDS TERMINATED BY ','
TRAILING NULLCOLS
(
id   , 
userid   , 
type CHAR(4000) "REPLACE( REPLACE(:type,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
f_key   , 
f_key2   , 
opt   , 
subject CHAR(4000) "REPLACE( REPLACE(:subject,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
body1 CHAR(4000) "REPLACE( REPLACE(:body1,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
body2 CHAR(4000) "REPLACE( REPLACE(:body2,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
body3 CHAR(4000) "REPLACE( REPLACE(:body3,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
body4 CHAR(4000) "REPLACE( REPLACE(:body4,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
writedate DATE(19) "YYYY-MM-DD HH24:MI:SS" NULLIF writedate="0000-00-00 00:00:00"  , 
completed  
)

