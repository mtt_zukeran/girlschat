OPTIONS(LOAD=-1,ERRORS=-1,ROWS=1000000,BINDSIZE=1024000,DIRECT=Y)
LOAD DATA
INFILE 'D:\cnv\man_user4.csv.new'
BADFILE 'D:\cnv\man_user4.bad'
TRUNCATE
INTO TABLE man_user4
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(
userid   , 
yencnt   , 
dolcnt   , 
ccnt   , 
joinip CHAR(4000) "REPLACE( REPLACE(:joinip,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
name CHAR(4000) "REPLACE( REPLACE(:name,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
banner CHAR(4000) "REPLACE( REPLACE(:banner,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
mailorig CHAR(4000) "REPLACE( REPLACE(:mailorig,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
machine CHAR(4000) "REPLACE( REPLACE(:machine,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
ecnt   , 
bcnt   , 
gcnt   , 
lastpaydate DATE(19) "YYYY-MM-DD" NULLIF lastpaydate="0000-00-00"  , 
scnt   , 
user_bannerid CHAR(4000) "REPLACE( REPLACE(:user_bannerid,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
pcnt   , 
ascode CHAR(4000) "REPLACE( REPLACE(:ascode,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
affili_status   , 
affili_sessid CHAR(4000) "REPLACE( REPLACE(:affili_sessid,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
wcnt   , 
agechk_status   , 
zcnt  , 
hcnt   , 
last_autocomeback_date DATE(19) "YYYY-MM-DD HH24:MI:SS" NULLIF last_autocomeback_date="0000-00-00 00:00:00"  , 
user_agent CHAR(4000) "REPLACE( REPLACE(:user_agent,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
vcnt   , 
ncnt   , 
icnt   
)
