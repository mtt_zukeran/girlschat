OPTIONS(LOAD=-1,ERRORS=-1,ROWS=1000000,BINDSIZE=1024000,DIRECT=Y)
LOAD DATA
INFILE 'D:\cnv\man_mail_movmanage.csv.new'
BADFILE 'D:\cnv\man_mail_movmanage.bad'
TRUNCATE
INTO TABLE man_mail_movmanage
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(
id        ,
userid    ,
auid      ,
dmovid    ,
writedate DATE(19) "YYYY-MM-DD HH24:MI:SS" NULLIF writedate="0000-00-00 00:00:00"  , 
mes_id    
)

