OPTIONS(LOAD=-1,ERRORS=-1,ROWS=1000000,BINDSIZE=1024000,DIRECT=Y)
LOAD DATA
INFILE 'D:\cnv\man_ownermail_detail.csv.new'
BADFILE 'D:\cnv\man_ownermail_detail.bad'
TRUNCATE
INTO TABLE man_ownermail_detail
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(
mdetailid   , 
mid   , 
userid   , 
mail_to CHAR(4000) "REPLACE( REPLACE(:mail_to,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
handle CHAR(4000) "REPLACE( REPLACE(:handle,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
point   , 
senddate DATE(19) "YYYY-MM-DD HH24:MI:SS" NULLIF senddate="0000-00-00 00:00:00"  , 
writedate DATE(19) "YYYY-MM-DD HH24:MI:SS" NULLIF writedate="0000-00-00 00:00:00"  , 
mdetail_status   , 
error_msg CHAR(4000) "REPLACE( REPLACE(:error_msg,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))" 
)

