OPTIONS(LOAD=-1,ERRORS=-1,ROWS=1000000,BINDSIZE=1024000,DIRECT=Y)
LOAD DATA
INFILE 'D:\cnv\man_user4_multi.csv.new'
BADFILE 'D:\cnv\man_user4_multi.bad'
TRUNCATE
INTO TABLE man_user4_multi
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(
userid   , 
allcnt   , 
allcnt2  
)

