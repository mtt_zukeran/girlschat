OPTIONS(LOAD=-1,ERRORS=-1,ROWS=1000000,BINDSIZE=1024000,DIRECT=Y)
LOAD DATA
INFILE 'D:\cnv\woman_manysendmsg_hist.csv.new'
BADFILE 'D:\cnv\woman_manysendmsg_hist.bad'
TRUNCATE
INTO TABLE woman_manysendmsg_hist
FIELDS TERMINATED BY ','
TRAILING NULLCOLS
(
histid   , 
userid   , 
subject CHAR(4000) "REPLACE( REPLACE(:subject,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
body1 CHAR(4000) "REPLACE( REPLACE(:body1,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
body2 CHAR(4000) "REPLACE( REPLACE(:body2,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
body3 CHAR(4000) "REPLACE( REPLACE(:body3,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
body4 CHAR(4000) "REPLACE( REPLACE(:body4,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
sendusercnt   , 
writedate DATE(19) "YYYY-MM-DD HH24:MI:SS" NULLIF writedate="0000-00-00 00:00:00"  , 
file_opt CHAR(4000) "REPLACE( REPLACE(:file_opt,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
filename CHAR(4000) "REPLACE( REPLACE(:filename,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
movietype CHAR(4000) "REPLACE( REPLACE(:movietype,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
file_size   , 
is_sent   , 
is_working  
)

