OPTIONS(LOAD=-1,ERRORS=-1,ROWS=1000000,BINDSIZE=1024000,DIRECT=Y)
LOAD DATA
INFILE 'D:\cnv\woman_joinmail.csv.new'
BADFILE 'D:\cnv\woman_joinmail.bad'
TRUNCATE
INTO TABLE woman_joinmail
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(
userid   , 
normaldate DATE(19) "YYYY-MM-DD HH24:MI:SS" NULLIF normaldate="0000-00-00 00:00:00" 
)

