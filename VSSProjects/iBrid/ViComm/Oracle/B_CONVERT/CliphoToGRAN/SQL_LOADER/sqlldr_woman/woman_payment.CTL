OPTIONS(LOAD=-1,ERRORS=-1,ROWS=1000000,BINDSIZE=1024000,DIRECT=Y)
LOAD DATA
INFILE 'D:\cnv\woman_payment.csv.new'
BADFILE 'D:\cnv\woman_payment.bad'
TRUNCATE
INTO TABLE woman_payment
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(
payid   , 
userid   , 
chargename CHAR(4000) "REPLACE( REPLACE(:chargename,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
point   , 
paypoint   , 
payrate   , 
paylimit   , 
payip CHAR(4000) "REPLACE( REPLACE(:payip,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
paystart DATE(19) "YYYY-MM-DD HH24:MI:SS" NULLIF paystart="0000-00-00 00:00:00"  , 
payend DATE(19) "YYYY-MM-DD HH24:MI:SS" NULLIF payend="0000-00-00 00:00:00"  , 
paydate DATE(19) "YYYY-MM-DD HH24:MI:SS" NULLIF paydate="0000-00-00 00:00:00"  , 
registid  
)

