OPTIONS(LOAD=-1,ERRORS=-1,ROWS=1000000,BINDSIZE=1024000,DIRECT=Y)
LOAD DATA
INFILE 'D:\cnv\woman_diary_title.csv.new'
BADFILE 'D:\cnv\woman_diary_title.bad'
TRUNCATE
INTO TABLE woman_diary_title
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(
diaryid     ,
userid      ,
diary_title  CHAR(4000) "REPLACE( REPLACE(:diary_title,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
writedate    DATE(19) "YYYY-MM-DD HH24:MI:SS" NULLIF writedate="0000-00-00 00:00:00" 
)

