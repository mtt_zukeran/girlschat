OPTIONS(LOAD=-1,ERRORS=-1,ROWS=1000000,BINDSIZE=1024000,DIRECT=Y)
LOAD DATA
INFILE 'D:\cnv\sunall_sts6log.csv.new'
BADFILE 'D:\cnv\sunall_sts6log.bad'
TRUNCATE
INTO TABLE sunall_sts6log
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(
logid ,
userid ,
cid CHAR(4000) "REPLACE( REPLACE(:cid,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
data CHAR(4000) "REPLACE( REPLACE(:data,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
request_sec ,
result_sec ,
check_flg ,
exec_flg ,
writedate  DATE(19) "YYYY-MM-DD HH24:MI:SS" NULLIF writedate="0000-00-00 00:00:00"  , 
execdate  DATE(19) "YYYY-MM-DD HH24:MI:SS" NULLIF execdate="0000-00-00 00:00:00"  , 
auid ,
auid_sec
)

