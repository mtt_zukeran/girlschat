OPTIONS(LOAD=-1,ERRORS=-1,ROWS=1000000,BINDSIZE=1024000)
LOAD DATA
INFILE 'D:\cnv\tvphonelog_log201102.csv.new'
BADFILE 'D:\cnv\tvphonelog_log201102.bad'
APPEND
INTO TABLE tvphonelog_logyyyymm
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(
userid   , 
uphp CHAR(4000) "REPLACE( REPLACE(:uphp,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
auid   , 
aphp CHAR(4000) "REPLACE( REPLACE(:aphp,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
kind CHAR(4000) "REPLACE( REPLACE(:kind,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
connecttime   , 
postdate DATE(19) "YYYY-MM-DD HH24:MI:SS" NULLIF postdate="0000-00-00 00:00:00"  , 
call_startdate DATE(19) "YYYY-MM-DD HH24:MI:SS" NULLIF call_startdate="0000-00-00 00:00:00"  , 
call_enddate DATE(19) "YYYY-MM-DD HH24:MI:SS" NULLIF call_enddate="0000-00-00 00:00:00"  , 
usedpoint_time   , 
callingkind   , 
waitingkind   , 
m_remainpoint   , 
agent_kind   , 
machine CHAR(4000) "REPLACE( REPLACE(:machine,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
status   , 
end_status   , 
cid CHAR(4000) "REPLACE( REPLACE(:cid,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
cid_seq   , 
use_freedial ,
month constant '201102'
)

