OPTIONS(LOAD=-1,ERRORS=-1,ROWS=1000000,BINDSIZE=20971520,readsize=20971520)
LOAD DATA
INFILE 'D:\cnv\wlog_log201111.csv.new'
BADFILE 'D:\cnv\wlog_log201111.bad'
APPEND
INTO TABLE wlog_logyyyymm
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(
logid   , 
userid   , 
auid   , 
nowpoint   , 
usedpoint   , 
action   , 
actiongroup   , 
actiondetail   , 
ip CHAR(4000) "REPLACE( REPLACE(:ip,'\\\"','\"\"'), '<BR>',chr(13) || chr(10))"  , 
logdate DATE(19) "YYYY-MM-DD HH24:MI:SS" NULLIF logdate="0000-00-00 00:00:00"  ,
month constant '201111'
)

