OPTIONS(LOAD=-1,ERRORS=-1,ROWS=1000000,BINDSIZE=20971520,readsize=20971520)
LOAD DATA
INFILE 'D:\cnv\diary_pvlog_log201105.csv.new'
BADFILE 'D:\cnv\diary_pvlog_log201105.bad'
APPEND
INTO TABLE diary_pvlog_logyyyymm
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(
logid   , 
did   ,
userid   , 
auid   , 
writedate DATE(19) "YYYY-MM-DD HH24:MI:SS" NULLIF writedate="0000-00-00 00:00:00"
)

