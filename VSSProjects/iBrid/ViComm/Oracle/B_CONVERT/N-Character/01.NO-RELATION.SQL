CONN VICOMM_TEMP/VICOMM_TEMP@VICOMM;
INSERT INTO T_ERROR				SELECT * FROM VICOMM_IBRID.T_ERROR;
INSERT INTO T_TEXT				SELECT * FROM VICOMM_IBRID.T_TEXT;
INSERT INTO T_CODE				SELECT * FROM VICOMM_IBRID.T_CODE;
INSERT INTO T_CODE_DTL			SELECT * FROM VICOMM_IBRID.T_CODE_DTL;
INSERT INTO T_MAINTE_LOG		SELECT * FROM VICOMM_IBRID.T_MAINTE_LOG;
INSERT INTO T_PROGRAM_TROUBLE	SELECT * FROM VICOMM_IBRID.T_PROGRAM_TROUBLE;
INSERT INTO T_MISCELLANEOUS		SELECT * FROM VICOMM_IBRID.T_MISCELLANEOUS;
INSERT INTO T_HOLIDAY			SELECT * FROM VICOMM_IBRID.T_HOLIDAY;
INSERT INTO T_WEB_FACE			SELECT * FROM VICOMM_IBRID.T_WEB_FACE;
INSERT INTO T_MAIL_TYPE			SELECT * FROM VICOMM_IBRID.T_MAIL_TYPE;

INSERT INTO T_AFFILIATER(
	AFFILIATER_CD          ,
	AFFILIATER_NM          ,
	AS_INFO_PREFIX         ,
	TRACKING_URL           ,
	TRACKING_HTTP_FLAG     ,
	ADD_ON_INFO            ,
	AUTH_RESULT_TRANS_TYPE ,
	UPDATE_DATE            ,
	REVISION_NO
)SELECT
	AFFILIATER_CD          ,
	AFFILIATER_NM          ,
	AS_INFO_PREFIX         ,
	TRACKING_URL           ,
	TRACKING_HTTP_FLAG     ,
	ADD_ON_INFO            ,
	NVL(AUTH_RESULT_TRANS_TYPE,0),
	UPDATE_DATE            ,
	REVISION_NO
FROM
 VICOMM_IBRID.T_AFFILIATER;


INSERT INTO T_AD				SELECT * FROM VICOMM_IBRID.T_AD;
INSERT INTO T_TERMINAL			SELECT * FROM VICOMM_IBRID.T_TERMINAL;
INSERT INTO T_CARRIER_DOMAIN	SELECT * FROM VICOMM_IBRID.T_CARRIER_DOMAIN;

INSERT INTO T_SYS(
	SIP_IVP_LOC_CD         ,
	SIP_IVP_SITE_CD        ,
	SIP_REGIST_URL         ,
	SIP_DOMAIN             ,
	SIP_DOMAIN_LOCAL       ,
	IVP_REQUEST_URL        ,
	IVP_ADMIN_URL          ,
	IVP_STARTUP_CAMERA_URL ,
	LIVE_KEY               ,
	MAIL_IF_URL            ,
	INSTALL_DIR            ,
	ONLINE_MONITOR_SITE_CD ,
	UPDATE_DATE            ,
	REVISION_NO            
)SELECT
	SIP_IVP_LOC_CD         ,
	SIP_IVP_SITE_CD        ,
	SIP_REGIST_URL         ,
	SIP_DOMAIN             ,
	SIP_DOMAIN_LOCAL       ,
	IVP_REQUEST_URL        ,
	IVP_ADMIN_URL          ,
	IVP_STARTUP_CAMERA_URL ,
	LIVE_KEY               ,
	MAIL_IF_URL            ,
	INSTALL_DIR            ,
	'' 					   ,
	UPDATE_DATE            ,
	REVISION_NO            
FROM VICOMM_IBRID.T_SYS;


INSERT INTO T_OTHER_WEB_SITE(
	OTHER_WEB_SYS_ID       ,
	SITE_NM                ,
	TOP_PAGE_URL           ,
	USER_INFO_URL          ,
	LOGIN_URL              ,
	MY_PAGE_URL            ,
	VICOMM_LOGIN_URL       ,
	UPDATE_DATE            ,
	REVISION_NO            
)SELECT
	OTHER_WEB_SYS_ID       ,
	SITE_NM                ,
	TOP_PAGE_URL           ,
	USER_INFO_URL          ,
	LOGIN_URL              ,
	MY_PAGE_URL            ,
	VICOMM_LOGIN_URL       ,
	UPDATE_DATE            ,
	REVISION_NO            
FROM VICOMM_IBRID.T_OTHER_WEB_SITE;


CONN VICOMM_TEMP/VICOMM_TEMP@VICOMM;

INSERT INTO T_SITE(
	SITE_CD                         ,
	SITE_NM                         ,
	SITE_MARK                       ,
	URL                             ,
	SITE_TYPE                       ,
	BASE_DIR                        ,
	WEB_PHISICAL_DIR                ,
	HOST_NM                         ,
	SUB_HOST_NM                     ,
--	JOB_OFFER_SITE_HOST_NM          ,
--	JOB_OFFER_SITE_TYPE             ,
--	JOB_OFFER_START_DOC             ,
	LOCAL_IP_ADDR                   ,
	LOCAL_PORT                      ,
	MAIL_HOST                       ,
	TX_LO_MAILER_IP                 ,
	TX_HI_MAILER_IP                 ,
	MAILER_FTP_ID                   ,
	MAILER_FTP_PW                   ,
	CALL_LOCK_TIMEOUT_SEC           ,
	CALL_TIMEOUT_SEC                ,
	NEXT_BUSINESS_TIME              ,
	PRIORITY                        ,
	WSHOT_TALK_DIALOUT_NO           ,
	WSHOT_TALK_START_GUIDANCE       ,
	PUB_TALK_DIALOUT_NO             ,
	PUB_TALK_START_GUIDANCE         ,
	IVP_LOC_CD                      ,
	IVP_SITE_CD                     ,
	IVP_OBJECT_STORE_FOLDER         ,
	IVP_REQUEST_URL                 ,
	IVP_STARTUP_CAMERA_URL          ,
	SUPPORT_PREMIUM_TALK_FLAG       ,
	ENABLE_PRIVATE_TALK_MENU_FLAG   ,
	SUPPORT_KDDI_TV_TEL_FLAG        ,
	SUPPORT_CHG_MONITOR_TO_TALK     ,
	POINT_PRICE                     ,
	MAIL_DEL_DAYS                   ,
	TRAN_DEL_DAYS                   ,
	TALK_HISTORY_DEL_DAYS           ,
	DIARY_DEL_DAYS                  ,
	SUMMARY_REPORT_DEL_DAYS         ,
	RECEIPT_DEL_DAYS                ,
	USE_OTHER_SYS_INFO_FLAG         ,
	OTHER_WEB_SYS_ID                ,
	PC_REDIRECT_URL                 ,
	PAGE_TITLE                      ,
	PAGE_KEYWORD                    ,
	PAGE_DESCRIPTION                ,
	CAST_MULTI_CTL_SITE_FLAG        ,
	MULTI_CHAR_FLAG                 ,
	CHAR_NO_ITEM_NM                 ,
	NA_FLAG                         ,
	SITE_OPEN_DATE                  ,
	UPDATE_DATE                     ,
	REVISION_NO                     
)SELECT
	SITE_CD                         ,
	SITE_NM                         ,
	SITE_MARK                       ,
	URL                             ,
	SITE_TYPE                       ,
	BASE_DIR                        ,
	WEB_PHISICAL_DIR                ,
	HOST_NM                         ,
	SUB_HOST_NM                     ,
--	JOB_OFFER_SITE_HOST_NM          ,
--	JOB_OFFER_SITE_TYPE             ,
--	JOB_OFFER_START_DOC             ,
	LOCAL_IP_ADDR                   ,
	LOCAL_PORT                      ,
	MAIL_HOST                       ,
	TX_LO_MAILER_IP                 ,
	TX_HI_MAILER_IP                 ,
	MAILER_FTP_ID                   ,
	MAILER_FTP_PW                   ,
	CALL_LOCK_TIMEOUT_SEC           ,
	CALL_TIMEOUT_SEC                ,
	NEXT_BUSINESS_TIME              ,
	PRIORITY                        ,
	WSHOT_TALK_DIALOUT_NO           ,
	WSHOT_TALK_START_GUIDANCE       ,
	PUB_TALK_DIALOUT_NO             ,
	PUB_TALK_START_GUIDANCE         ,
	IVP_LOC_CD                      ,
	IVP_SITE_CD                     ,
	IVP_OBJECT_STORE_FOLDER         ,
	IVP_REQUEST_URL                 ,
	IVP_STARTUP_CAMERA_URL          ,
	SUPPORT_PREMIUM_TALK_FLAG       ,
	ENABLE_PRIVATE_TALK_MENU_FLAG   ,
	SUPPORT_KDDI_TV_TEL_FLAG        ,
	SUPPORT_CHG_MONITOR_TO_TALK     ,
	POINT_PRICE                     ,
	MAIL_DEL_DAYS                   ,
	TRAN_DEL_DAYS                   ,
	TALK_HISTORY_DEL_DAYS           ,
	DIARY_DEL_DAYS                  ,
	SUMMARY_REPORT_DEL_DAYS         ,
	RECEIPT_DEL_DAYS                ,
	USE_OTHER_SYS_INFO_FLAG         ,
	OTHER_WEB_SYS_ID                ,
	PC_REDIRECT_URL                 ,
	PAGE_TITLE                      ,
	PAGE_KEYWORD                    ,
	PAGE_DESCRIPTION                ,
	CAST_MULTI_CTL_SITE_FLAG        ,
	0				                ,
	''				                ,
	NA_FLAG                         ,
	SITE_OPEN_DATE                  ,
	UPDATE_DATE                     ,
	REVISION_NO                     
FROM VICOMM_IBRID.T_SITE;


INSERT INTO T_PRODUCTION		SELECT * FROM VICOMM_IBRID.T_PRODUCTION;

INSERT INTO T_MANAGER(
	PRODUCTION_SEQ ,
	MANAGER_SEQ    ,
	MANAGER_NM     ,
	LOGIN_ID       ,
	LOGIN_PASSWORD ,
	TEL            ,
	EMAIL_ADDR     ,
	REGIST_DATE    ,
	UPDATE_DATE    ,
	REVISION_NO    
)SELECT
	PRODUCTION_SEQ ,
	MANAGER_SEQ    ,
	MANAGER_NM     ,
	LOGIN_ID       ,
	LOGIN_PASSWORD ,
	TEL            ,
	EMAIL_ADDR     ,
	REGIST_DATE    ,
	UPDATE_DATE    ,
	REVISION_NO    
FROM VICOMM_IBRID.T_MANAGER;


INSERT INTO T_ADMIN(
	ADMIN_ID         ,
	ADMIN_PASSWORD   ,
	ADMIN_TYPE       ,
	ADMIN_NM         ,
	SITE_CD          ,
	PRODUCTION_SEQ   ,
	MANAGER_SEQ      ,
	AD_CD            ,
	REVISION_NO      ,
	UPDATE_DATE      
)SELECT
	ADMIN_ID         ,
	ADMIN_PASSWORD   ,
	ADMIN_TYPE       ,
	ADMIN_NM         ,
	SITE_CD          ,
	PRODUCTION_SEQ   ,
	MANAGER_SEQ      ,
	AD_CD            ,
	REVISION_NO      ,
	UPDATE_DATE            
FROM VICOMM_IBRID.T_ADMIN;
	

INSERT INTO T_MANAGE_COMPANY(
 MANAGE_COMPANY_CD        ,
 MANAGE_COMPANY_NM        ,
 TEL                      ,
 MANAGE_PERSON_NM         ,
 EMAIL_ADDR               ,
 SETTLE_COMPANY_MASK      ,
 CAST_MINIMUM_PAYMENT     ,
 AUTO_GET_TEL_INFO_FLAG   ,
 AD_MANAGE_HOST           ,
 UPDATE_DATE              ,
 REVISION_NO              
)SELECT
 MANAGE_COMPANY_CD        ,
 MANAGE_COMPANY_NM        ,
 TEL                      ,
 MANAGE_PERSON_NM         ,
 EMAIL_ADDR               ,
 SETTLE_COMPANY_MASK      ,
 CAST_MINIMUM_PAYMENT     ,
 AUTO_GET_TEL_INFO_FLAG   ,
 AD_MANAGE_HOST           ,
 UPDATE_DATE              ,
 REVISION_NO              
FROM VICOMM_IBRID.T_MANAGE_COMPANY;

INSERT INTO T_SITE_MANAGEMENT(
	SITE_CD                                ,
	REGIST_SERVICE_POINT                   ,
	INVITE_MAIL_AVA_HOUR                   ,
	RE_INVITE_MAIL_NA_HOUR                 ,
	TX_MAIL_REF_MARKING                    ,
	APROACH_MAIL_INTERVAL_HOUR             ,
	LOGIN_MAIL_INTERVAL_HOUR               ,
	LOGOUT_MAIL_INTERVAL_HOUR              ,
	LOGOUT_MAIL_VALID_TALK_SEC             ,
	NEW_FACE_DAYS                          ,
	NEW_FACE_MARK                          ,
	DISPLAY_MOVIE_UPLOAD_DAYS              ,
	RANKING_CREATE_COUNT                   ,
	FIND_MAN_ELAPASED_DAYS                 ,
	CM_MAIL_RX_LIMIT                       ,
	SUPPORT_EMAIL_ADDR                     ,
	INFO_EMAIL_ADDR                        ,
	SUPPORT_TEL                            ,
	PREPAID_EXPIRE_DAYS                    ,
	CHARGE_START_POINT                     ,
	RECEIPT_SIGHT                          ,
	WEB_FACE_SEQ                           ,
	FOOTER_HTML_DOC                        ,
	MAIL_CAST_ATTR_TYPE_SEQ1               ,
	MAIL_CAST_ATTR_TYPE_SEQ2               ,
	INQ_MAN_TYPE_SEQ1                      ,
	INQ_MAN_TYPE_SEQ2                      ,
	INQ_MAN_TYPE_SEQ3                      ,
	INQ_MAN_TYPE_SEQ4                      ,
	INVITE_TALK_CAST_CHARGE_FLAG           ,
	CAST_MINIMUM_PAYMENT                   ,
	NO_USE_RETURN_MAIL_FLAG                ,
	BBS_DUP_CHARGE_FLAG                    ,
	USE_ATTACHED_MAIL_FLAG                 ,
	REGIST_HANDLE_NM_INPUT_FLAG            ,
	REGIST_BIRTHDAY_INPUT_FLAG             ,
	MAN_INTRO_POINT                        ,
	CAST_INTRO_POINT                       ,
	TOP_PAGE_SEEK_MODE                     ,
	OFFLINE_GUIDANCE                       ,
	WAITING_GUIDANCE                       ,
	CALLING_GUIDANCE                       ,
	TALKING_WSHOT_GUIDANCE                 ,
	TALKING_PARTY_GUIDANCE                 ,
	PROFILE_PAGEING_FLAG                   ,
	PROFILE_PREVIUOS_GUIDANCE              ,
	PROFILE_NEXT_GUIDANCE                  ,
	CAST_CAN_SELECT_MONITOR_FLAG           ,
	MAIL_EMBED_DOMAIN                      ,
	UPDATE_DATE                            ,
	REVISION_NO                            
)SELECT
	SITE_CD                                ,
	REGIST_SERVICE_POINT                   ,
	INVITE_MAIL_AVA_HOUR                   ,
	RE_INVITE_MAIL_NA_HOUR                 ,
	TX_MAIL_REF_MARKING                    ,
	APROACH_MAIL_INTERVAL_HOUR             ,
	LOGIN_MAIL_INTERVAL_HOUR               ,
	LOGOUT_MAIL_INTERVAL_HOUR              ,
	LOGOUT_MAIL_VALID_TALK_SEC             ,
	NEW_FACE_DAYS                          ,
	NEW_FACE_MARK                          ,
	DISPLAY_MOVIE_UPLOAD_DAYS              ,
	RANKING_CREATE_COUNT                   ,
	FIND_MAN_ELAPASED_DAYS                 ,
	CM_MAIL_RX_LIMIT                       ,
	SUPPORT_EMAIL_ADDR                     ,
	INFO_EMAIL_ADDR                        ,
	SUPPORT_TEL                            ,
	PREPAID_EXPIRE_DAYS                    ,
	CHARGE_START_POINT                     ,
	RECEIPT_SIGHT                          ,
	WEB_FACE_SEQ                           ,
	FOOTER_HTML_DOC                        ,
	MAIL_CAST_ATTR_TYPE_SEQ1               ,
	MAIL_CAST_ATTR_TYPE_SEQ2               ,
	INQ_MAN_TYPE_SEQ1                      ,
	INQ_MAN_TYPE_SEQ2                      ,
	INQ_MAN_TYPE_SEQ3                      ,
	INQ_MAN_TYPE_SEQ4                      ,
	INVITE_TALK_CAST_CHARGE_FLAG           ,
	CAST_MINIMUM_PAYMENT                   ,
	NO_USE_RETURN_MAIL_FLAG                ,
	BBS_DUP_CHARGE_FLAG                    ,
	USE_ATTACHED_MAIL_FLAG                 ,
	REGIST_HANDLE_NM_INPUT_FLAG            ,
	REGIST_BIRTHDAY_INPUT_FLAG             ,
	MAN_INTRO_POINT                        ,
	CAST_INTRO_POINT                       ,
	TOP_PAGE_SEEK_MODE                     ,
	OFFLINE_GUIDANCE                       ,
	WAITING_GUIDANCE                       ,
	CALLING_GUIDANCE                       ,
	TALKING_WSHOT_GUIDANCE                 ,
	TALKING_PARTY_GUIDANCE                 ,
	PROFILE_PAGEING_FLAG                   ,
	PROFILE_PREVIUOS_GUIDANCE              ,
	PROFILE_NEXT_GUIDANCE                  ,
	CAST_CAN_SELECT_MONITOR_FLAG           ,
	MAIL_EMBED_DOMAIN                      ,
	UPDATE_DATE                            ,
	REVISION_NO                            
FROM VICOMM_IBRID.T_SITE_MANAGEMENT;

INSERT INTO T_SITE_HTML_MANAGE	SELECT * FROM VICOMM_IBRID.T_SITE_HTML_MANAGE;
INSERT INTO T_SITE_HTML_DOC(
HTML_DOC_SEQ           ,
HTML_DOC_SUB_SEQ       ,
HTML_DOC_TITLE         ,
HTML_DOC1              ,
HTML_DOC2              ,
HTML_DOC3              ,
HTML_DOC4              ,
HTML_DOC5              ,
HTML_DOC6              ,
HTML_DOC7              ,
HTML_DOC8              ,
HTML_DOC9              ,
HTML_DOC10             ,
NON_DISP_IN_MAIL_BOX   ,
NON_DISP_IN_DECOMAIL   ,
UPDATE_DATE            ,
REVISION_NO            
)SELECT
HTML_DOC_SEQ           ,
HTML_DOC_SUB_SEQ       ,
HTML_DOC_TITLE         ,
HTML_DOC1              ,
HTML_DOC2              ,
HTML_DOC3              ,
HTML_DOC4              ,
HTML_DOC5              ,
HTML_DOC6              ,
HTML_DOC7              ,
HTML_DOC8              ,
HTML_DOC9              ,
HTML_DOC10             ,
NON_DISP_IN_MAIL_BOX   ,
NON_DISP_IN_DECOMAIL   ,
UPDATE_DATE            ,
REVISION_NO            
FROM VICOMM_IBRID.T_SITE_HTML_DOC;

COMMIT;

INSERT INTO T_PROGRAM(
PROGRAM_ROOT   ,
PROGRAM_ID     ,
PROGRAM_NM     ,
REMARKS        ,
UPDATE_DATE    ,
REVISION_NO    
)SELECT
PROGRAM_ROOT   ,
PROGRAM_ID     ,
PROGRAM_NM     ,
REMARKS        ,
UPDATE_DATE    ,
REVISION_NO    
FROM VICOMM_IBRID.T_PROGRAM;


INSERT INTO T_CHARGE_TYPE		SELECT * FROM VICOMM_IBRID.T_CHARGE_TYPE;
INSERT INTO T_LIVE_CAMERA		SELECT * FROM VICOMM_IBRID.T_LIVE_CAMERA;

INSERT INTO T_ADMIN_REPORT		SELECT * FROM VICOMM_IBRID.T_ADMIN_REPORT;

INSERT INTO T_BUSINESS_REPORT	SELECT * FROM VICOMM_IBRID.T_BUSINESS_REPORT;


INSERT INTO T_ACT_CATEGORY		SELECT * FROM VICOMM_IBRID.T_ACT_CATEGORY;
INSERT INTO T_SITE_AFFILIATER	SELECT * FROM VICOMM_IBRID.T_SITE_AFFILIATER;
INSERT INTO T_SITE_AD_POINT		SELECT * FROM VICOMM_IBRID.T_SITE_AD_POINT;
INSERT INTO T_AD_GROUP			SELECT * FROM VICOMM_IBRID.T_AD_GROUP;
INSERT INTO T_SITE_AD_GROUP		SELECT * FROM VICOMM_IBRID.T_SITE_AD_GROUP;
INSERT INTO T_MAIL_TEMPLATE(
	SITE_CD                        ,
	MAIL_TEMPLATE_NO               ,
	HTML_DOC_SEQ                   ,
	MAIL_TEMPLATE_TYPE             ,
	TEMPLATE_NM                    ,
	MAIL_TITLE                     ,
	CAST_CREATE_ORG_SUB_SEQ        ,
	WEB_FACE_SEQ                   ,
	MAIL_ATTACHED_OBJ_TYPE         ,
	REVISION_NO                    ,
	UPDATE_DATE                    
)SELECT
	SITE_CD                        ,
	MAIL_TEMPLATE_NO               ,
	HTML_DOC_SEQ                   ,
	MAIL_TEMPLATE_TYPE             ,
	TEMPLATE_NM                    ,
	MAIL_TITLE                     ,
	CAST_CREATE_ORG_SUB_SEQ        ,
	WEB_FACE_SEQ                   ,
	MAIL_ATTACHED_OBJ_TYPE         ,
	REVISION_NO                    ,
	UPDATE_DATE                    
FROM VICOMM_IBRID.T_MAIL_TEMPLATE;

INSERT INTO T_USER_CHARGE(
SITE_CD                ,
USER_RANK              ,
CHARGE_TYPE            ,
ACT_CATEGORY_SEQ       ,
CHARGE_POINT_NORMAL    ,
CHARGE_POINT_NEW       ,
CHARGE_POINT_NO_RECEIPT,
CHARGE_POINT_NO_USE    ,
CHARGE_POINT_FREE_DIAL ,
CHARGE_UNIT_SEC        ,
UPDATE_DATE            ,
REVISION_NO            
)SELECT
SITE_CD                ,
USER_RANK              ,
CHARGE_TYPE            ,
ACT_CATEGORY_SEQ       ,
CHARGE_POINT_NORMAL    ,
CHARGE_POINT_NEW       ,
CHARGE_POINT_NO_RECEIPT,
CHARGE_POINT_NO_USE    ,
CHARGE_POINT_FREE_DIAL ,
CHARGE_UNIT_SEC        ,
UPDATE_DATE            ,
REVISION_NO            
FROM VICOMM_IBRID.T_USER_CHARGE;


INSERT INTO T_RECEIPT_SERVICE_POINT(
	SITE_CD                ,
	USER_RANK              ,
	SETTLE_TYPE            ,
	RECEIPT_AMT            ,
	ADD_RATE0              ,
	ADD_RATE1              ,
	ADD_RATE2              ,
	ADD_RATE               ,
	PAYMENT_DELAY_FLAG     ,
	REVISION_NO            ,
	UPDATE_DATE            
)SELECT
	SITE_CD                ,
	USER_RANK              ,
	SETTLE_TYPE            ,
	RECEIPT_AMT            ,
	ADD_RATE0              ,
	ADD_RATE1              ,
	ADD_RATE2              ,
	ADD_RATE               ,
	PAYMENT_DELAY_FLAG     ,
	REVISION_NO            ,
	UPDATE_DATE            
FROM VICOMM_IBRID.T_RECEIPT_SERVICE_POINT;


COMMIT;

INSERT INTO T_CAST_CHARGE		SELECT * FROM VICOMM_IBRID.T_CAST_CHARGE;


INSERT INTO T_VIEW_KEY(
    SITE_CD        ,
	PROGRAM_ROOT   ,
	PROGRAM_ID     ,
	VIEW_KEY_MASK  ,
	UPDATE_DATE    ,
	REVISION_NO    
)SELECT
    SITE_CD        ,
	PROGRAM_ROOT   ,
	PROGRAM_ID     ,
	VIEW_KEY_MASK  ,
	UPDATE_DATE    ,
	REVISION_NO    
FROM VICOMM_IBRID.T_VIEW_KEY;


INSERT INTO T_USER_VIEW(
	SITE_CD                ,
	PROGRAM_ROOT           ,
	PROGRAM_ID             ,
	AD_GROUP_CD            ,
	USER_RANK              ,
	ACT_CATEGORY_SEQ       ,
	HTML_DOC_SEQ           ,
	VIEW_KEY_MASK          ,
	UPDATE_DATE            ,
	REVISION_NO            
)SELECT
	SITE_CD                ,
	PROGRAM_ROOT           ,
	PROGRAM_ID             ,
	AD_GROUP_CD            ,
	USER_RANK              ,
	ACT_CATEGORY_SEQ       ,
	HTML_DOC_SEQ           ,
	VIEW_KEY_MASK          ,
	UPDATE_DATE            ,
	REVISION_NO            
FROM VICOMM_IBRID.T_USER_VIEW;

INSERT INTO T_NG_WORD			SELECT * FROM VICOMM_IBRID.T_NG_WORD;

INSERT INTO T_ACCESS_AD			SELECT * FROM VICOMM_IBRID.T_ACCESS_AD;
INSERT INTO T_DAILY_AD_SALES	SELECT * FROM VICOMM_IBRID.T_DAILY_AD_SALES;
INSERT INTO T_REGIST_AD			SELECT * FROM VICOMM_IBRID.T_REGIST_AD;

INSERT INTO T_DAILY_AD_ACCESS_SALES	SELECT * FROM VICOMM_IBRID.T_DAILY_AD_ACCESS_SALES;

INSERT INTO T_OK_PLAY_LIST(
	SITE_CD        ,
	OK_PLAY        ,
	OK_PLAY_NM     ,
	ITEM_NO        ,
	REVISION_NO    ,
	UPDATE_DATE    
)SELECT 
	SITE_CD        ,
	OK_PLAY        ,
	OK_PLAY_NM     ,
	ITEM_NO        ,
	REVISION_NO    ,
	UPDATE_DATE    
FROM VICOMM_IBRID.T_OK_PLAY_LIST;


INSERT INTO T_PACK				SELECT * FROM VICOMM_IBRID.T_PACK;
INSERT INTO T_SETTLE_COMPANY	SELECT * FROM VICOMM_IBRID.T_SETTLE_COMPANY;
INSERT INTO T_SITE_SETTLE		SELECT * FROM VICOMM_IBRID.T_SITE_SETTLE;

INSERT INTO T_DAILY_SALES		SELECT * FROM VICOMM_IBRID.T_DAILY_SALES;
INSERT INTO T_SITE_SUPPLEMENT	SELECT * FROM VICOMM_IBRID.T_SITE_SUPPLEMENT;
INSERT INTO T_ACCESS_TOP_PAGE	SELECT * FROM VICOMM_IBRID.T_ACCESS_TOP_PAGE;
INSERT INTO T_DAILY_PERFORMANCE	SELECT * FROM VICOMM_IBRID.T_DAILY_PERFORMANCE;

INSERT INTO T_CAST_ATTR_TYPE	SELECT * FROM VICOMM_IBRID.T_CAST_ATTR_TYPE;
INSERT INTO T_MAN_ATTR_TYPE		SELECT * FROM VICOMM_IBRID.T_MAN_ATTR_TYPE;
INSERT INTO T_CAST_ATTR_TYPE_VALUE	SELECT * FROM VICOMM_IBRID.T_CAST_ATTR_TYPE_VALUE;
INSERT INTO T_MAN_ATTR_TYPE_VALUE	SELECT * FROM VICOMM_IBRID.T_MAN_ATTR_TYPE_VALUE;
COMMIT;

UPDATE VICOMM_IBRID.T_USER SET LOGIN_SEQ = NULL;
COMMIT;

INSERT INTO T_USER				SELECT * FROM VICOMM_IBRID.T_USER;
INSERT INTO T_CAST(
	USER_SEQ                       ,
	CAST_NM                        ,
	CAST_KANA_NM                   ,
	CONTACT_TEL                    ,
	BIRTHDAY                       ,
	MONITOR_ENABLE_FLAG            ,
	ONLINE_LIVE_ENABLE_FLAG        ,
	WORK_STYLE_TYPE                ,
	PRODUCTION_SEQ                 ,
	MANAGER_SEQ                    ,
	TOTAL_PAYMENT_AMT              ,
	REVISION_NO                    
)SELECT
	USER_SEQ                       ,
	CAST_NM                        ,
	CAST_KANA_NM                   ,
	CONTACT_TEL                    ,
	BIRTHDAY                       ,
	MONITOR_ENABLE_FLAG            ,
	ONLINE_LIVE_ENABLE_FLAG        ,
	WORK_STYLE_TYPE                ,
	PRODUCTION_SEQ                 ,
	MANAGER_SEQ                    ,
	TOTAL_PAYMENT_AMT              ,
	REVISION_NO                    
FROM VICOMM_IBRID.T_CAST;


INSERT INTO T_USER_MAN			SELECT * FROM VICOMM_IBRID.T_USER_MAN;
INSERT INTO T_RECEIPT			SELECT * FROM VICOMM_IBRID.T_RECEIPT;
INSERT INTO T_POINT_ADD_HISTORY	SELECT * FROM VICOMM_IBRID.T_POINT_ADD_HISTORY;
INSERT INTO T_OTHER_WEB_USER	SELECT * FROM VICOMM_IBRID.T_OTHER_WEB_USER;
INSERT INTO T_OBJ_USED_HISTORY	SELECT * FROM VICOMM_IBRID.T_OBJ_USED_HISTORY;
COMMIT;

INSERT /*+ APPEND */ INTO T_USER_HISTORY(
	SITE_CD                        ,
	USER_SEQ                       ,
	USER_HIS_SEQ                   ,
	MODIFY_BEFORE_AFTER_CD         ,
	CREATE_DATE                    ,
	MODIFY_REASON_CD               ,
	SEX_CD                         ,
	USE_TERMINAL_TYPE              ,
	URI                            ,
	TERMINAL_PASSWORD              ,
	TEL                            ,
	EMAIL_ADDR                     ,
	LOGIN_ID                       ,
	LOGIN_PASSWORD                 ,
	USER_STATUS                    ,
	MOBILE_CARRIER_CD              ,
	MOBILE_TERMINAL_NM             ,
	TERMINAL_UNIQUE_ID             ,
	TEL_ATTESTED_FLAG              ,
	NON_EXIST_MAIL_ADDR_FLAG       ,
	BAL_POINT                      ,
	SERVICE_POINT                  ,
	SERVICE_POINT_EFFECTIVE_DATE   ,
	LIMIT_POINT                    ,
	LAST_LOGIN_DATE                ,
	FIRST_POINT_USED_DATE          ,
	LAST_POINT_USED_DATE           ,
	FIRST_RECEIPT_DAY              ,
	LAST_RECEIPT_DAY               ,
	RECEIPT_LIMIT_DAY              ,
	BILL_AMT                       ,
	TOTAL_RECEIPT_AMT              ,
	TOTAL_RECEIPT_COUNT            ,
	EXIST_CREDIT_DEAL_FLAG         ,
	BLACK_DAY                      ,
	BLACK_COUNT                    ,
	BLACK_TIME                     ,
	LAST_BLACK_DAY                 ,
	LAST_BLACK_TIME                ,
	CAST_MAIL_RX_TYPE              ,
	INFO_MAIL_RX_TYPE              ,
	LOGIN_MAIL_RX_FLAG1            ,
	LOGIN_MAIL_RX_FLAG2            ,
	AD_CD                          ,
	PROFILE_OK_FLAG                ,
	URGE_LEVEL                     ,
	MAX_URGE_COUNT                 ,
	URGE_COUNT_TODAY               ,
	URGE_RESTART_DAY               ,
	URGE_END_DAY                   ,
	URGE_EXEC_LAST_DATE            ,
	URGE_EXEC_LAST_STATUS          ,
	URGE_EXEC_COUNT                ,
	URGE_CONNECT_COUNT
)SELECT
	SITE_CD                        ,
	USER_SEQ                       ,
	USER_HIS_SEQ                   ,
	MODIFY_BEFORE_AFTER_CD         ,
	CREATE_DATE                    ,
	MODIFY_REASON_CD               ,
	SEX_CD                         ,
	USE_TERMINAL_TYPE              ,
	URI                            ,
	TERMINAL_PASSWORD              ,
	TEL                            ,
	EMAIL_ADDR                     ,
	LOGIN_ID                       ,
	LOGIN_PASSWORD                 ,
	USER_STATUS                    ,
	MOBILE_CARRIER_CD              ,
	MOBILE_TERMINAL_NM             ,
	TERMINAL_UNIQUE_ID             ,
	TEL_ATTESTED_FLAG              ,
	NON_EXIST_MAIL_ADDR_FLAG       ,
	BAL_POINT                      ,
	SERVICE_POINT                  ,
	SERVICE_POINT_EFFECTIVE_DATE   ,
	LIMIT_POINT                    ,
	LAST_LOGIN_DATE                ,
	FIRST_POINT_USED_DATE          ,
	LAST_POINT_USED_DATE           ,
	FIRST_RECEIPT_DAY              ,
	LAST_RECEIPT_DAY               ,
	RECEIPT_LIMIT_DAY              ,
	BILL_AMT                       ,
	TOTAL_RECEIPT_AMT              ,
	TOTAL_RECEIPT_COUNT            ,
	EXIST_CREDIT_DEAL_FLAG         ,
	BLACK_DAY                      ,
	BLACK_COUNT                    ,
	BLACK_TIME                     ,
	LAST_BLACK_DAY                 ,
	LAST_BLACK_TIME                ,
	CAST_MAIL_RX_TYPE              ,
	INFO_MAIL_RX_TYPE              ,
	LOGIN_MAIL_RX_FLAG1            ,
	LOGIN_MAIL_RX_FLAG2            ,
	AD_CD                          ,
	PROFILE_OK_FLAG                ,
	COLLECTION_LEVEL               ,
	MAX_COLLECTION_COUNT           ,
	COLLECTION_COUNT_TODAY         ,
	RE_COLLECTION_DAY              ,
	COLLECTION_END_DAY             ,
	COLLECTION_CALL_LAST_DATE      ,
	COLLECTION_CALL_LAST_STAT      ,
	COLLECTION_CALL_EXEC_COUNT     ,
	COLLECTION_CONNECT_COUNT
FROM
	VICOMM_IBRID.T_USER_HISTORY;
COMMIT;



INSERT INTO T_SETTLE_LOG(
	SETTLE_SEQ             ,
	SITE_CD                ,
	SETTLE_COMPANY_CD      ,
	SETTLE_TYPE            ,
	USER_SEQ               ,
	SID                    ,
	SETTLE_STATUS          ,
	SETTLE_POINT           ,
	SETTLE_AMT             ,
	SETTLE_ID              ,
	CREATE_DATE            ,
	TRANS_DATE             ,
	ANSWER_DATE            ,
	SETTLE_COMPLITE_FLAG
)SELECT
	SETTLE_SEQ             ,
	SITE_CD                ,
	SETTLE_COMPANY_CD      ,
	SETTLE_TYPE            ,
	USER_SEQ               ,
	SID                    ,
	SETTLE_STATUS          ,
	SETTLE_POINT           ,
	SETTLE_AMT             ,
	SETTLE_ID              ,
	CREATE_DATE            ,
	TRANS_DATE             ,
	ANSWER_DATE            ,
	SETTLE_COMPLITE_FLAG
FROM VICOMM_IBRID.T_SETTLE_LOG;
COMMIT;

INSERT INTO T_LOGIN_USER		SELECT * FROM VICOMM_IBRID.T_LOGIN_USER;
INSERT INTO T_BONUS_LOG			SELECT * FROM VICOMM_IBRID.T_BONUS_LOG;
