/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: �b�������@�`��������
--  Title			: �v�d�a�I�y���[�^�ғ��󋵏���
--	Progaram ID		: WEB_CLEAR_OPE_PERFORMANCE
--	Compile Turn	: 0
--  Creation Date	: 10.03.31
--	Update Date		:
--  Author			: i-Brid(R.Suzuki)
/*************************************************************************/
-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE WEB_CLEAR_OPE_PERFORMANCE(
	pLOGIN_ID		IN	VARCHAR2,
	pSTATUS			OUT	VARCHAR2
)IS
	-- CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'WEB_CLEAR_OPE_PERFORMANCE';

	CURSOR CS_CAST IS
		SELECT
			SITE_CD		,
			USER_SEQ	,
			USER_CHAR_NO
		FROM
			VW_CAST_CHARACTER01
		WHERE
			LOGIN_ID	= pLOGIN_ID;

	REC_TIME_OPERATION	PKG_TIME_OPERATION.CS01%ROWTYPE;

BEGIN
	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS			:= APP_COMM.STATUS_NORMAL;

	FOR REC_CAST IN CS_CAST LOOP
		OPEN PKG_TIME_OPERATION.CS01(REC_CAST.USER_SEQ,REC_CAST.USER_CHAR_NO);
		FETCH PKG_TIME_OPERATION.CS01 INTO REC_TIME_OPERATION;
		IF PKG_TIME_OPERATION.CS01%FOUND THEN
			UPDATE T_TIME_OPERATION
				SET
					PAYMENT_FLAG = 1,
					PAYMENT_DATE = SYSDATE
				WHERE
					SITE_CD			= REC_CAST.SITE_CD		AND
					USER_SEQ		= REC_CAST.USER_SEQ		AND
					USER_CHAR_NO	= REC_CAST.USER_CHAR_NO	AND
					PAYMENT_FLAG	= 0;
		END IF;
		CLOSE PKG_TIME_OPERATION.CS01;
	END LOOP;

	COMMIT;

EXCEPTION

	WHEN OTHERS THEN

			LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);

		PKG_TIME_OPERATION.CLOSE_ALL;

		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END WEB_CLEAR_OPE_PERFORMANCE;
/
SHOW ERROR;

