/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: オペレータ管理
--  Title			: 会員つぶやき購読登録
--	Progaram ID		: REGIST_MAN_TWEET_READ
--	Compile Turn	: 0
--  Creation Date	: 13.03.21
--	Update Date		:
--  Author			: K.Miyazato
/*************************************************************************/
CREATE OR REPLACE PROCEDURE REGIST_MAN_TWEET_READ(
	pSITE_CD		IN 	VARCHAR2,
	pMAN_USER_SEQ	IN 	VARCHAR2,
	pCAST_USER_SEQ	IN 	VARCHAR2,
	pCAST_CHAR_NO	IN 	VARCHAR2,
	pRESULT			OUT	VARCHAR2,
	pSTATUS			OUT	VARCHAR2
)IS
	--	CONSTANT
	PGM_NM			CONSTANT VARCHAR(255)	:= 'REGIST_MAN_TWEET_READ';
	RESULT_OK		CONSTANT VARCHAR2(1)	:= '0';	--正常
	RESULT_NG		CONSTANT VARCHAR2(1)	:= '9';	--エラー

BEGIN
	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS	:= APP_COMM.STATUS_NORMAL;
	pRESULT := RESULT_NG;

	IF (pMAN_USER_SEQ IS NULL OR pCAST_USER_SEQ IS NULL OR pCAST_CHAR_NO IS NULL) THEN
		RETURN;
	END IF;

	BEGIN
		INSERT INTO T_MAN_TWEET_READ(
			SITE_CD			,
			MAN_USER_SEQ	,
			CAST_USER_SEQ	,
			CAST_CHAR_NO	,
			CREATE_DATE
		)VALUES(
			pSITE_CD		,
			pMAN_USER_SEQ	,
			pCAST_USER_SEQ	,
			pCAST_CHAR_NO	,
			SYSDATE
		);
	EXCEPTION
		WHEN DUP_VAL_ON_INDEX THEN
			RETURN;
	END;

	pRESULT	:= RESULT_OK;

	COMMIT;

EXCEPTION
	WHEN OTHERS THEN
		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);
		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END REGIST_MAN_TWEET_READ;
/
SHOW ERROR;

