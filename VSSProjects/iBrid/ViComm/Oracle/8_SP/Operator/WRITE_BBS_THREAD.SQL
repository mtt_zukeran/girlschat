/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: オペレータ管理
--  Title			: 掲示板スレッド書込み
--	Progaram ID		: WRITE_BBS_THREAD
--	Compile Turn	: 0
--  Creation Date	: 11.04.08
--	Update Date		:
--  Author			: i-Brid(R.Suzuki)
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE WRITE_BBS_THREAD(
	pSITE_CD				IN	VARCHAR2	,
	pUSER_SEQ				IN	VARCHAR2	,
	pUSER_CHAR_NO			IN	VARCHAR2	,
	pBBS_THREAD_SEQ			IN	VARCHAR2	,
	pBBS_THREAD_HANDLE_NM	IN	VARCHAR2	,
	pBBS_THREAD_TITLE		IN	VARCHAR2	,
	pBBS_THREAD_DOC			IN	VARCHAR2	,
	pDEL_FLAG				IN	NUMBER		,
	pNEW_BBS_THREAD_SEQ		OUT	VARCHAR2	,
	pSTATUS					OUT	VARCHAR2
)IS
	-- CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'WRITE_BBS_THREAD';

	REC_BBS_THREAD	PKG_BBS_THREAD.CS02%ROWTYPE;
BEGIN
	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS					:= APP_COMM.STATUS_NORMAL;
	pNEW_BBS_THREAD_SEQ		:= pBBS_THREAD_SEQ;

	OPEN PKG_BBS_THREAD.CS02(pSITE_CD,pUSER_SEQ,pUSER_CHAR_NO,pBBS_THREAD_SEQ);
	FETCH PKG_BBS_THREAD.CS02 INTO REC_BBS_THREAD;
	IF PKG_BBS_THREAD.CS02%FOUND THEN

		IF (pDEL_FLAG = 0) THEN
			--更新は本文のみ行う
			UPDATE T_BBS_THREAD SET
					BBS_THREAD_DOC1		= SUBSTRB(pBBS_THREAD_DOC,1,4000)		,
					BBS_THREAD_DOC2		= SUBSTRB(pBBS_THREAD_DOC,4001,4000)	,
					BBS_THREAD_DOC3		= SUBSTRB(pBBS_THREAD_DOC,8001,4000)	,
					BBS_THREAD_DOC4		= SUBSTRB(pBBS_THREAD_DOC,12001,4000)	,
					UPDATE_DATE			= SYSDATE								,
					REVISION_NO			= SEQ_REVISION_NO.NEXTVAL
				WHERE
					CURRENT OF PKG_BBS_THREAD.CS02;
		ELSE
			UPDATE T_BBS_THREAD SET
					DEL_FLAG			= 1			,
					UPDATE_DATE			= SYSDATE	,
					REVISION_NO			= SEQ_REVISION_NO.NEXTVAL
				WHERE
					CURRENT OF PKG_BBS_THREAD.CS02;
		END IF;
	ELSE
		SELECT SEQ_BBS_THREAD.NEXTVAL INTO pNEW_BBS_THREAD_SEQ FROM DUAL;

		INSERT INTO T_BBS_THREAD (
			SITE_CD				,
			USER_SEQ			,
			USER_CHAR_NO		,
			BBS_THREAD_SEQ		,
			BBS_THREAD_TITLE	,
			BBS_THREAD_HANDLE_NM,
			BBS_THREAD_DOC1		,
			BBS_THREAD_DOC2		,
			BBS_THREAD_DOC3		,
			BBS_THREAD_DOC4		,
			LAST_RES_WRITE_DATE	,
			CREATE_DATE			,
			UPDATE_DATE			,
			REVISION_NO
		)VALUES(
			pSITE_CD							,
			pUSER_SEQ							,
			pUSER_CHAR_NO						,
			pNEW_BBS_THREAD_SEQ					,
			pBBS_THREAD_TITLE					,
			pBBS_THREAD_HANDLE_NM				,
			SUBSTRB(pBBS_THREAD_DOC,1,4000)		,
			SUBSTRB(pBBS_THREAD_DOC,4001,4000)	,
			SUBSTRB(pBBS_THREAD_DOC,8001,4000)	,
			SUBSTRB(pBBS_THREAD_DOC,12001,4000)	,
			SYSDATE								,
			SYSDATE								,
			SYSDATE								,
			SEQ_REVISION_NO.NEXTVAL
		);

	END IF;
	CLOSE PKG_BBS_THREAD.CS02;

	COMMIT;

EXCEPTION
	WHEN OTHERS THEN

			LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);

		PKG_BBS_THREAD.CLOSE_ALL;

		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END WRITE_BBS_THREAD;
/
SHOW ERROR;
