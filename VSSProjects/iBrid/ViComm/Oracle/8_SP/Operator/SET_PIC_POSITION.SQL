/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: オペレータ管理
--  Title			: 写真表示順序更新
--	Progaram ID		: SET_PIC_POSITION
--	Compile Turn	: 0
--  Creation Date	: 08.10.21
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--  2009.11.30 Adding Parameter
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE SET_PIC_POSITION(
	pSITE_CD		IN	VARCHAR2	,
	pUSER_SEQ		IN	VARCHAR2	,
	pUSER_CHAR_NO	IN	VARCHAR2	,
	pPIC_SEQ		IN	VARCHAR2	,
	pPIC_TYPE		IN 	NUMBER		,
	pMOVE_POS		IN	NUMBER		,
	pSTATUS			OUT	VARCHAR2
)IS
	-- CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'SET_PIC_POSITION';

	CURSOR CS_CAST_PIC IS
		SELECT PIC_SEQ,ROWID FROM T_CAST_PIC
			WHERE
				SITE_CD			= pSITE_CD		AND
				USER_SEQ		= pUSER_SEQ		AND
				USER_CHAR_NO	= pUSER_CHAR_NO	AND
				PIC_TYPE		= pPIC_TYPE
			ORDER BY
				SITE_CD				ASC,
				USER_SEQ			ASC,
				USER_CHAR_NO		ASC,
				PIC_TYPE			ASC,
				DISPLAY_POSITION	ASC,
				PIC_SEQ				DESC;

	ARY_SEQ		APP_COMM.TBL_VARCHAR2;
	ARY_ROWID	APP_COMM.TBL_VARCHAR2;

	BUF_SEQ		T_CAST_PIC.PIC_SEQ%TYPE;
	BUF_ROWID	ROWID;

	BUF_CNT			NUMBER(3);
	BUF_IDX			NUMBER(3);
	BUF_CNT_MAX		NUMBER(3);
	BUF_OK			BOOLEAN;

BEGIN

	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS			:= APP_COMM.STATUS_NORMAL;

	BUF_CNT 	:= 0;
	BUF_IDX 	:= 0;
	BUF_CNT_MAX	:= 100;

	FOR REC_PIC IN CS_CAST_PIC LOOP
		BUF_CNT := 	BUF_CNT + 1;
		ARY_SEQ	(BUF_CNT)	:= REC_PIC.PIC_SEQ;
		ARY_ROWID(BUF_CNT)	:= REC_PIC.ROWID;

		IF pPIC_SEQ = REC_PIC.PIC_SEQ THEN
			BUF_IDX := BUF_CNT;
		END IF;

	END LOOP;

	IF (BUF_IDX = 0) THEN
		RETURN;
	END IF;

	BUF_OK := FALSE;

	IF pMOVE_POS = 1 THEN
		IF ((BUF_IDX + 1) <= BUF_CNT) THEN
			BUF_OK := TRUE;
		END IF;

	ELSIF pMOVE_POS = -1 THEN
		IF ((BUF_IDX - 1) > 0) THEN
			BUF_OK := TRUE;
		END IF;
	END IF;

	-- CHANGE POSITION
	IF (BUF_OK) THEN
		BUF_SEQ		:= ARY_SEQ	(BUF_IDX + pMOVE_POS);
		BUF_ROWID	:= ARY_ROWID(BUF_IDX + pMOVE_POS);

		ARY_SEQ		(BUF_IDX + pMOVE_POS)	:= ARY_SEQ	(BUF_IDX);
		ARY_ROWID	(BUF_IDX + pMOVE_POS)	:= ARY_ROWID(BUF_IDX);

		ARY_SEQ		(BUF_IDX)	:= BUF_SEQ;
		ARY_ROWID	(BUF_IDX)	:= BUF_ROWID;

		FOR i in 1..BUF_CNT LOOP
			IF (i < BUF_CNT_MAX) THEN
				UPDATE T_CAST_PIC SET DISPLAY_POSITION = i
					WHERE
						ROWID = ARY_ROWID(i);
			END IF;
		END LOOP;

		COMMIT;

	END IF;

EXCEPTION
	WHEN OTHERS THEN

			LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);

		IF CS_CAST_PIC%ISOPEN THEN
			CLOSE CS_CAST_PIC;
		END IF;

		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END SET_PIC_POSITION;
/
SHOW ERROR;
