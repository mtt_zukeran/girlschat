/*************************************************************************/
--	System			: ViComm
--	Sub System Name	: オペレータ管理
--	Title			: 掲示板レス書込み
--	Progaram ID		: WRITE_BBS_RES
--	Compile Turn	: 0
--	Creation Date	: 11.04.12
--	Update Date		:
--	Author			: i-Brid
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--	Date        Updater    Update Explain
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE WRITE_BBS_RES(
	pSITE_CD				IN	VARCHAR2	,
	pBBS_THREAD_SEQ			IN	VARCHAR2	,
	pBBS_RES_USER_SEQ		IN	VARCHAR2	,
	pBBS_RES_USER_CHAR_NO	IN	VARCHAR2	,
	pBBS_RES_HANDLE_NM		IN	VARCHAR2	,
	pBBS_RES_DOC			IN	VARCHAR2	,
	pPOST_BBS_THREAD_SEQ	IN	VARCHAR2	,
	pPOST_THREAD_SUB_SEQ	IN	VARCHAR2	,
	pANONYMOUS_FLAG			IN	NUMBER		,
	pNEW_BBS_THREAD_SUB_SEQ	OUT	NUMBER		,
	pSTATUS					OUT	VARCHAR2
)IS
	-- CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'WRITE_BBS_RES';

	REC_BBS_THREAD			PKG_BBS_THREAD.CS05%ROWTYPE;
BEGIN
	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS					:= APP_COMM.STATUS_NORMAL;
	pNEW_BBS_THREAD_SUB_SEQ	:= NULL;

	OPEN PKG_BBS_THREAD.CS05(pSITE_CD,pBBS_THREAD_SEQ);
	FETCH PKG_BBS_THREAD.CS05 INTO REC_BBS_THREAD;

	IF PKG_BBS_THREAD.CS05%FOUND THEN

		pNEW_BBS_THREAD_SUB_SEQ	:= REC_BBS_THREAD.LAST_THREAD_SUB_SEQ + 1;

		INSERT INTO T_BBS_RES (
			SITE_CD									,
			USER_SEQ								,
			USER_CHAR_NO							,
			BBS_THREAD_SEQ							,
			BBS_THREAD_SUB_SEQ						,
			BBS_RES_HANDLE_NM						,
			BBS_RES_USER_SEQ						,
			BBS_RES_USER_CHAR_NO					,
			BBS_RES_DOC1							,
			BBS_RES_DOC2							,
			BBS_RES_DOC3							,
			BBS_RES_DOC4							,
			POST_BBS_THREAD_SEQ						,
			POST_THREAD_SUB_SEQ						,
			ANONYMOUS_FLAG							,
			DEL_FLAG								,
			CREATE_DATE								,
			UPDATE_DATE
		)VALUES(
			pSITE_CD								,
			REC_BBS_THREAD.USER_SEQ					,
			REC_BBS_THREAD.USER_CHAR_NO				,
			pBBS_THREAD_SEQ							,
			pNEW_BBS_THREAD_SUB_SEQ					,
			pBBS_RES_HANDLE_NM						,
			pBBS_RES_USER_SEQ						,
			pBBS_RES_USER_CHAR_NO					,
			SUBSTRB(pBBS_RES_DOC,	1,		4000)	,
			SUBSTRB(pBBS_RES_DOC,	4001,	4000)	,
			SUBSTRB(pBBS_RES_DOC,	8001,	4000)	,
			SUBSTRB(pBBS_RES_DOC,	12001,	4000)	,
			pPOST_BBS_THREAD_SEQ					,
			pPOST_THREAD_SUB_SEQ					,
			pANONYMOUS_FLAG							,
			0										,
			SYSDATE									,
			SYSDATE
		);

		UPDATE T_BBS_THREAD SET
			LAST_RES_WRITE_DATE		= SYSDATE					,
			LAST_THREAD_SUB_SEQ		= pNEW_BBS_THREAD_SUB_SEQ	,
			UPDATE_DATE				= SYSDATE					,
			REVISION_NO				= SEQ_REVISION_NO.NEXTVAL
		WHERE
			CURRENT OF PKG_BBS_THREAD.CS05;

	END IF;
	CLOSE PKG_BBS_THREAD.CS05;

	COMMIT;

EXCEPTION
	WHEN OTHERS THEN

			LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);

		PKG_BBS_THREAD.CLOSE_ALL;

		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END WRITE_BBS_RES;
/
SHOW ERROR;
