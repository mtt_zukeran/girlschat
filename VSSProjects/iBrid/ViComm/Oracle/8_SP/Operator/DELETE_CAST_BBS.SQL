/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: オペレータ管理
--  Title			: キャスト掲示板書込削除
--	Progaram ID		: DELETE_CAST_BBS
--	Compile Turn	: 0
--  Creation Date	: 10.04.22
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE DELETE_CAST_BBS(
	pSITE_CD		IN	VARCHAR2,
	pUSER_SEQ		IN	VARCHAR2,
	pUSER_CHAR_NO	IN	VARCHAR2,
	pBBS_SEQ		IN	VARCHAR2,
	pSTATUS			OUT	VARCHAR2
)IS
	-- CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'DELETE_CAST_BBS';

	CURSOR CS_MAX_BBS_SEQ IS
		SELECT
			MAX(BBS_SEQ) AS BBS_SEQ
		FROM
			T_CAST_BBS
		WHERE
			SITE_CD			= pSITE_CD		AND
			USER_SEQ		= pUSER_SEQ		AND
			USER_CHAR_NO	= pUSER_CHAR_NO AND
			DEL_FLAG		= 0;

	REC_MAX_BBS_SEQ		CS_MAX_BBS_SEQ%ROWTYPE;

	REC_CAST_BBS		PKG_CAST_BBS.CS02%ROWTYPE;
	REC_CAST_CHARACTER	PKG_CAST_CHARACTER.CS02%ROWTYPE;
BEGIN
	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS	:= APP_COMM.STATUS_NORMAL;
	
	/*------------------------------*/
	/* Open Cursor					*/
	/*------------------------------*/
	OPEN PKG_CAST_BBS.CS02(pSITE_CD,pUSER_SEQ,pUSER_CHAR_NO,pBBS_SEQ);
	
	/*------------------------------*/
	/* Update Record				*/
	/*------------------------------*/
	FETCH PKG_CAST_BBS.CS02 INTO REC_CAST_BBS;
	IF PKG_CAST_BBS.CS02%FOUND THEN
	
		UPDATE T_CAST_BBS SET
			DEL_FLAG = 1
		WHERE
			CURRENT OF PKG_CAST_BBS.CS02;

		OPEN CS_MAX_BBS_SEQ;
		FETCH CS_MAX_BBS_SEQ INTO REC_MAX_BBS_SEQ;
		IF CS_MAX_BBS_SEQ%FOUND THEN
			OPEN PKG_CAST_CHARACTER.CS02(pSITE_CD,pUSER_SEQ,pUSER_CHAR_NO);
			FETCH PKG_CAST_CHARACTER.CS02 INTO REC_CAST_CHARACTER;
			IF PKG_CAST_CHARACTER.CS02%FOUND THEN
				UPDATE T_CAST_CHARACTER
					SET
						BBS_SEQ	= REC_MAX_BBS_SEQ.BBS_SEQ
					WHERE
						CURRENT OF PKG_CAST_CHARACTER.CS02;
			END IF;
			CLOSE PKG_CAST_CHARACTER.CS02;
		END IF;
		CLOSE CS_MAX_BBS_SEQ;

	END IF;
	
	CLOSE PKG_CAST_BBS.CS02;
	
	COMMIT;
	
EXCEPTION
	WHEN OTHERS THEN

			LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);

		PKG_CAST_BBS.CLOSE_ALL;
		PKG_CAST_CHARACTER.CLOSE_ALL;

		IF CS_MAX_BBS_SEQ%ISOPEN THEN
			CLOSE CS_MAX_BBS_SEQ;
		END IF;

		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END DELETE_CAST_BBS;
/
SHOW ERROR;
