/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: 男性会員
--  Title			: ログインメール受信拒否フラグ更新
--	Progaram ID		: LOGIN_MAIL_NOT_RX_MAINTE
--	Compile Turn	: 0
--  Creation Date	: 11.03.02
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE LOGIN_MAIL_NOT_RX_MAINTE(
	pSITE_CD				IN	VARCHAR2,
	pUSER_SEQ				IN	VARCHAR2,
	pUSER_CHAR_NO			IN	VARCHAR2,
	pPARTNER_USER_SEQ		IN	VARCHAR2,
	pPARTNER_USER_CHAR_NO	IN	VARCHAR2,
	pLOGIN_MAIL_NOT_RX_FLAG	IN	NUMBER	,
	pSTATUS					OUT	VARCHAR2
)IS
	-- CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'LOGIN_MAIL_NOT_RX_MAINTE';

	REC_FAVORIT				PKG_FAVORIT.CS02%ROWTYPE;
	
BEGIN

	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS			:= APP_COMM.STATUS_NORMAL;

	OPEN PKG_FAVORIT.CS02(pSITE_CD, pUSER_SEQ, pUSER_CHAR_NO, pPARTNER_USER_SEQ, pPARTNER_USER_CHAR_NO);
	FETCH PKG_FAVORIT.CS02 INTO REC_FAVORIT;
	
	IF PKG_FAVORIT.CS02%FOUND THEN
		UPDATE T_FAVORIT SET
				LOGIN_MAIL_NOT_RX_FLAG	= pLOGIN_MAIL_NOT_RX_FLAG
			WHERE
				CURRENT OF PKG_FAVORIT.CS02;
	END IF;
	
	CLOSE PKG_FAVORIT.CS02;
	
	COMMIT;

EXCEPTION

	WHEN OTHERS THEN

		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);
		
		PKG_FAVORIT.CLOSE_ALL;
		
		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END LOGIN_MAIL_NOT_RX_MAINTE;
/

SHOW ERROR;

