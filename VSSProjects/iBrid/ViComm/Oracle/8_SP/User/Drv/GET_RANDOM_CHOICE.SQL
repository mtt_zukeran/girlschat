SET SERVEROUTPUT ON;

DECLARE
	pPARTNER_USER_SEQ		APP_COMM.TBL_VARCHAR2;
	pPARTNER_USER_CHAR_NO	APP_COMM.TBL_VARCHAR2;
	pRECORD_COUNT	NUMBER(3);
	pSTATUS			VARCHAR2(10);
BEGIN
	GET_RANDOM_CHOICE(
		'A001'	,
		1000	,	-- USER SEQ
		'01'	,	-- CHAR
		'1'		,	-- SEX CD
		'01'	,	-- RANDOM_COHICE_TYPE
		1		,	-- NEED COUNT
		pPARTNER_USER_SEQ		,
		pPARTNER_USER_CHAR_NO	,
		pRECORD_COUNT			,
		pSTATUS
	);

	DBMS_OUTPUT.PUT_LINE(pSTATUS);
	FOR i IN 1..pRECORD_COUNT LOOP
		DBMS_OUTPUT.PUT_LINE(pPARTNER_USER_SEQ(i) || '-' || pPARTNER_USER_CHAR_NO(i));
	END LOOP;
END;
/
