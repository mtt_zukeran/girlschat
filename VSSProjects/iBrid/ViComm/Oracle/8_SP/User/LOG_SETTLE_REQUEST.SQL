/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: �V�X�e���Ǘ�
--  Title			: ���ϗv��
--	Progaram ID		: LOG_SETTLE_REQUEST
--	Compile Turn	: 0
--  Creation Date	: 08.10.21
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE LOG_SETTLE_REQUEST(
	pSITE_CD				IN	VARCHAR2,
	pUSER_SEQ				IN	VARCHAR2,
	pSETTLE_COMPANY_CD		IN	VARCHAR2,
	pSETTLE_TYPE			IN	VARCHAR2,
	pSETTLE_STATUS			IN	VARCHAR2,
	pSETTLE_AMT				IN	NUMBER	,
	pSETTLE_POINT			IN	NUMBER	,
	pSETTLE_ID				IN	VARCHAR2,
	pASP_AD_CD				IN	VARCHAR2,
	pSETTLE_SEQ				OUT	VARCHAR2,
	pSID					OUT	VARCHAR2,
	pSTATUS					OUT	VARCHAR2,
	pSETTLE_UNIQUE_VALUE	IN	VARCHAR2 DEFAULT NULL
)IS
	-- CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'LOG_SETTLE_REQUEST';

	BUF_SID_SEQ	NUMBER(5);

BEGIN
	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS			:= APP_COMM.STATUS_NORMAL;

	SELECT SEQ_SETTLE.NEXTVAL INTO pSETTLE_SEQ FROM DUAL;

	SELECT SEQ_SETTLE_SID.NEXTVAL INTO BUF_SID_SEQ FROM DUAL;
	pSID := pSITE_CD || TO_CHAR(SYSDATE,'YYMMDD') || TO_CHAR(BUF_SID_SEQ,'FM00000');

	INSERT INTO T_SETTLE_LOG(
		SITE_CD				,
		SETTLE_SEQ			,
		SETTLE_TYPE			,
		CREATE_DATE			,
		TRANS_DATE			,
		SETTLE_STATUS		,
		USER_SEQ			,
		SETTLE_AMT			,
		SETTLE_POINT		,
		SETTLE_ID			,
		SID					,
		SETTLE_COMPANY_CD	,
		ASP_AD_CD			,
		SETTLE_UNIQUE_VALUE
	)VALUES(
		pSITE_CD			,
		pSETTLE_SEQ			,
		pSETTLE_TYPE		,
		SYSDATE				,
		SYSDATE				,
		pSETTLE_STATUS		,
		pUSER_SEQ			,
		pSETTLE_AMT			,
		pSETTLE_POINT		,
		pSETTLE_ID			,
		pSID				,
		pSETTLE_COMPANY_CD	,
		pASP_AD_CD			,
		pSETTLE_UNIQUE_VALUE
	);

	COMMIT;

EXCEPTION

	WHEN OTHERS THEN

		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);

		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END LOG_SETTLE_REQUEST;
/
SHOW ERROR;

