SET DEFINE OFF;
/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: 男性会員
--  Title			: 男性ユーザー掲示板書き込み
--	Progaram ID		: WRITE_USER_BBS
--	Compile Turn	: 0
--  Creation Date	: 10.04.20
--	Update Date		: 10.04.20
--  Author			: i-Brid
/*************************************************************************/
-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE WRITE_USER_BBS(
	pSITE_CD			IN	VARCHAR2	,
	pUSER_SEQ			IN	VARCHAR2	,
	pUSER_CHAR_NO		IN	VARCHAR2	,
	pBBS_TITLE			IN	VARCHAR2	,
	pBBS_DOC			IN	VARCHAR2	,
	pSTATUS				OUT	VARCHAR2
)IS
	--	CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'WRITE_USER_BBS';

	REC_MAN_CHARACTER	PKG_USER_MAN_CHARACTER.CS05%ROWTYPE;
	BUF_BBS_SEQ			T_USER_MAN_BBS.BBS_SEQ%TYPE;
BEGIN
	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/	
	pSTATUS	:= APP_COMM.STATUS_NORMAL;

	OPEN PKG_USER_MAN_CHARACTER.CS05(pSITE_CD,pUSER_SEQ,VICOMM_CONST.MAIN_CHAR);
	FETCH PKG_USER_MAN_CHARACTER.CS05 INTO REC_MAN_CHARACTER;
	IF PKG_USER_MAN_CHARACTER.CS05%FOUND THEN

		UPDATE T_USER_MAN_CHARACTER SET
				LAST_ACTION_DATE = SYSDATE
			WHERE
				CURRENT OF PKG_USER_MAN_CHARACTER.CS05;
	ELSE
		APP_COMM.TRACE(APP_COMM.TRACE_INFO,'WRITE_USER_BBS USER NOT FOUND SEQ=' || pUSER_SEQ);
	END IF;
	CLOSE PKG_USER_MAN_CHARACTER.CS05;
	
	SELECT SEQ_OBJ.NEXTVAL INTO BUF_BBS_SEQ FROM DUAL;
	
	
	INSERT INTO T_USER_MAN_BBS(
		SITE_CD				,
		USER_SEQ			,
		USER_CHAR_NO		,
		BBS_SEQ				,
		BBS_TITLE			,
		BBS_DOC				,
		CREATE_DATE
	)VALUES(
		pSITE_CD			,
		pUSER_SEQ			,
		pUSER_CHAR_NO		,
		BUF_BBS_SEQ			,
		pBBS_TITLE			,
		pBBS_DOC			,
		SYSDATE
	);
	
	COMMIT;
	
EXCEPTION
	WHEN OTHERS THEN

		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);

		PKG_USER_MAN_CHARACTER.CLOSE_ALL;

		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END WRITE_USER_BBS;
/
SHOW ERROR;
