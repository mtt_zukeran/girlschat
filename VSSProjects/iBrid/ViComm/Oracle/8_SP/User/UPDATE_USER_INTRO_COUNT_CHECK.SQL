/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: User
--  Title			: 会員紹介人数確認日時更新
--	Progaram ID		: UPDATE_USER_INTRO_COUNT_CHECK
--	Compile Turn	: 0
--  Creation Date	: 15.12.22
--	Update Date		:
--  Author			: M&TT Y.Ikemiya
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  2009.11.30 Adding Parameter
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE UPDATE_USER_INTRO_COUNT_CHECK(
	pSITE_CD		IN	VARCHAR2	,
	pUSER_SEQ		IN	VARCHAR2	,
	pUSER_CHAR_NO	IN	VARCHAR2	,
	pSTATUS			OUT	VARCHAR2
)IS
	-- CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'UPDATE_USER_INTRO_COUNT_CHECK';
	
	REC_INTRO_CHECK			PKG_INTRO_CHECK.CS01%ROWTYPE;

BEGIN
	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS			:= APP_COMM.STATUS_NORMAL;

	OPEN PKG_INTRO_CHECK.CS01(pSITE_CD,pUSER_SEQ,pUSER_CHAR_NO);
	FETCH PKG_INTRO_CHECK.CS01 INTO REC_INTRO_CHECK;
	IF PKG_INTRO_CHECK.CS01%FOUND THEN
		UPDATE T_INTRO_CHECK SET
			USER_INTRO_COUNT_CHECK_DATE = SYSDATE
		WHERE
			CURRENT OF PKG_INTRO_CHECK.CS01;
	ELSE
		INSERT INTO T_INTRO_CHECK(
			SITE_CD							,
			USER_SEQ						,
			USER_CHAR_NO					,
			USER_INTRO_COUNT_CHECK_DATE
		) VALUES (
			pSITE_CD						,
			pUSER_SEQ						,
			pUSER_CHAR_NO					,
			SYSDATE
		);
	END IF;
	CLOSE PKG_INTRO_CHECK.CS01;

	COMMIT;


EXCEPTION
	WHEN OTHERS THEN

		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);
		
		PKG_INTRO_CHECK.CLOSE_ALL;

		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END UPDATE_USER_INTRO_COUNT_CHECK;
/
SHOW ERROR;
