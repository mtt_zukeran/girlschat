/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: �������
--  Title			: 
--	Progaram ID		: BLOG_MAINTE
--	Compile Turn	: 0
--  Creation Date	: 11.03.09
--	Update Date		:
--  Author			: K.Itoh
/*************************************************************************/
-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
/*-----------------------------------------------------------------------*/

CREATE OR REPLACE PROCEDURE BLOG_MAINTE(
	pSITE_CD		IN		VARCHAR2				,
	pUSER_SEQ		IN		VARCHAR2				,
	pUSER_CHAR_NO	IN		VARCHAR2				,
	pBLOG_SEQ		IN OUT	NUMBER					,
--			
	pBLOG_TITLE    	IN		VARCHAR2				,
	pBLOG_DOC      	IN		VARCHAR2				,
--	
	pDEL_FLAG		IN		NUMBER		DEFAULT 0 	,
	pSTATUS			OUT		VARCHAR2	
)IS
--	CONST
	PGM_NM 					CONSTANT VARCHAR(255)	:= 'BLOG_MAINTE';
--	REC	
	REC_BLOG				PKG_BLOG.CS02	%ROWTYPE;
	
BEGIN
	pSTATUS	:= APP_COMM.STATUS_NORMAL;

	OPEN PKG_BLOG.CS02(pSITE_CD,pUSER_SEQ,pUSER_CHAR_NO,pBLOG_SEQ);
	FETCH PKG_BLOG.CS02 INTO REC_BLOG;
	IF PKG_BLOG.CS02%FOUND THEN
		IF ( pDEL_FLAG = 1 ) THEN
			/*
			 * DELETE
			 */
			DELETE FROM T_BLOG
			WHERE
				CURRENT OF PKG_BLOG.CS02;
		ELSE
			/*
			 * UPDATE
			 */		
			UPDATE T_BLOG SET
				BLOG_TITLE		= pBLOG_TITLE				,
				BLOG_DOC   		= pBLOG_DOC                 ,
				UPDATE_DATE		= SYSDATE                   ,
				REVISION_NO		= SEQ_REVISION_NO.NEXTVAL 	
			WHERE
				CURRENT OF PKG_BLOG.CS02;
		END IF;
		
	ELSE
	
		/*
		 * INSERT
		 */		
		SELECT SEQ_BLOG.NEXTVAL INTO pBLOG_SEQ FROM DUAL;
		INSERT INTO T_BLOG(
			SITE_CD					,
			USER_SEQ        		,
			USER_CHAR_NO    		,
			BLOG_SEQ        		,
			BLOG_TITLE      		,
			BLOG_DOC        		,
			REGIST_DATE     		,
			UPDATE_DATE     		,
			REVISION_NO     
		) VALUES(
			pSITE_CD				,
			pUSER_SEQ       		,
			pUSER_CHAR_NO   		,
			pBLOG_SEQ		   		,
			pBLOG_TITLE     		,
			pBLOG_DOC       		,
			SYSDATE			   		,
			SYSDATE		    		,
			SEQ_REVISION_NO.NEXTVAL    
		);
	
	END IF;
	CLOSE PKG_BLOG.CS02;
	
	COMMIT;
		
EXCEPTION
	WHEN OTHERS THEN
		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);
		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);
		
		PKG_BLOG.CLOSE_ALL;

END BLOG_MAINTE;
/
SHOW ERROR;
