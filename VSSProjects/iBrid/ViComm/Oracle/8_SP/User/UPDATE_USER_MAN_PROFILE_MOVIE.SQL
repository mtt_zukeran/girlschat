/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: オペレータ管理
--  Title			: 動画更新
--	Progaram ID		: UPDATE_USER_MAN_PROFILE_MOVIE
--	Compile Turn	: 0
--  Creation Date	: 09.08.19
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  XXXX/XX/XX  XXXXXXXX
--  2009.11.30 Adding Parameter
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE UPDATE_USER_MAN_PROFILE_MOVIE(
	pSITE_CD				IN	VARCHAR2,
	pUSER_SEQ				IN	VARCHAR2,
	pUSER_CHAR_NO			IN	VARCHAR2,
	pMOVIE_SEQ				IN	VARCHAR2,
	pMOVIE_TITLE			IN	VARCHAR2,
	pDEL_FLAG				IN	NUMBER	,
	pMOVIE_TYPE				IN	NUMBER	,
	pSTATUS					OUT	VARCHAR2
)IS
	--	CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'UPDATE_USER_MAN_PROFILE_MOVIE';

	REC_USER_MAN_MOVIE	PKG_USER_MAN_MOVIE.CS02%ROWTYPE;

BEGIN
	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS	:= APP_COMM.STATUS_NORMAL;

	/*------------------------------*/
	/* Open Cursor					*/
	/*------------------------------*/
	OPEN PKG_USER_MAN_MOVIE.CS02(pSITE_CD,pUSER_SEQ,pUSER_CHAR_NO,pMOVIE_SEQ);

	/*------------------------------*/
	/* Update Record				*/
	/*------------------------------*/
	FETCH PKG_USER_MAN_MOVIE.CS02 INTO REC_USER_MAN_MOVIE;
	IF PKG_USER_MAN_MOVIE.CS02%FOUND THEN
		IF (pDEL_FLAG != 0) THEN

			DELETE T_USER_MAN_MOVIE
				WHERE
					CURRENT OF PKG_USER_MAN_MOVIE.CS02;
		ELSE
			UPDATE T_USER_MAN_MOVIE SET 
				MOVIE_TITLE		= pMOVIE_TITLE			,
				MOVIE_TYPE		= pMOVIE_TYPE
				WHERE
					CURRENT OF PKG_USER_MAN_MOVIE.CS02;
		END IF;

	END IF;

	CLOSE PKG_USER_MAN_MOVIE.CS02;

	COMMIT;

EXCEPTION
	WHEN OTHERS THEN

		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);

		PKG_USER_MAN_MOVIE.CLOSE_ALL;
	
		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END UPDATE_USER_MAN_PROFILE_MOVIE;
/
SHOW ERROR;
