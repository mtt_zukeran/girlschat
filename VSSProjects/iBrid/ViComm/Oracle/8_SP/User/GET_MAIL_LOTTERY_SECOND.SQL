/*************************************************************************/
--	System			: ViComm
--	Sub System Name	: 会員
--	Title			: ﾒｰﾙdeｶﾞﾁｬ
--	Progaram ID		: GET_MAIL_LOTTERY_SECOND
--	Compile Turn	: 0
--	Creation Date	: 15.01.05
--	Update Date		: 
--	Author			: M&TT Y.Ikemiya
/*************************************************************************/
-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE GET_MAIL_LOTTERY_SECOND(
	pSITE_CD					IN	VARCHAR2	,
	pUSER_SEQ					IN	VARCHAR2	,
	pUSER_CHAR_NO				IN	VARCHAR2	,
	pSEX_CD						IN	VARCHAR2	,
	pCHECK_ONLY_FLAG			IN	NUMBER		,
	pMAIL_LOTTERY_RATE_SEQ		OUT	VARCHAR2	,
	pMAIL_LOTTERY_REWARD_POINT	OUT	VARCHAR2	,
	pRESULT						OUT	VARCHAR2	,
	pSTATUS						OUT	VARCHAR2
)IS
	--	CONSTANT
	PGM_NM						CONSTANT VARCHAR(255)	:= 'GET_MAIL_LOTTERY_SECOND';
	RESULT_OK					CONSTANT VARCHAR2(1)	:= '0';	--完了
	RESULT_NOT_PUBLISH			CONSTANT VARCHAR2(1)	:= '1';	--期間外
	RESULT_NO_TICKET			CONSTANT VARCHAR2(1)	:= '2';	--ﾁｹｯﾄ不足
	RESULT_NG					CONSTANT VARCHAR2(1)	:= '9';	--その他

	--	CURSOR
	CURSOR CS_MAIL_LOTTERY IS
		SELECT
			MAIL_LOTTERY_SEQ			,
			NEED_SECOND_LOTTERY_COUNT	,
			START_DATE					,
			END_DATE
		FROM
			T_MAIL_LOTTERY
		WHERE
			SITE_CD		=  pSITE_CD		AND
			SEX_CD		=  pSEX_CD		AND
			SYSDATE BETWEEN START_DATE AND END_DATE;

	CURSOR CS_MAIL_LOTTERY_DENOMINATOR(
		prmMAIL_LOTTERY_SEQ		T_MAIL_LOTTERY_RATE.MAIL_LOTTERY_SEQ%TYPE
	) IS
	SELECT
		SUM(RATE) AS DENOMINATOR
	FROM
		T_MAIL_LOTTERY_RATE
	WHERE
		SITE_CD						= pSITE_CD					AND
		MAIL_LOTTERY_SEQ			= prmMAIL_LOTTERY_SEQ		AND
		SECOND_LOTTERY_FLAG			= 1;
	
	CURSOR CS_MAIL_LOTTERY_RATE(
		prmMAIL_LOTTERY_SEQ		T_MAIL_LOTTERY_RATE.MAIL_LOTTERY_SEQ%TYPE
	) IS
		SELECT
			MAIL_LOTTERY_RATE_SEQ	,
			REWARD_POINT			,
			RATE
		FROM
			T_MAIL_LOTTERY_RATE
		WHERE
			SITE_CD					= pSITE_CD					AND
			MAIL_LOTTERY_SEQ		= prmMAIL_LOTTERY_SEQ		AND
			SECOND_LOTTERY_FLAG		= 1;
	
	REC_MAIL_LOTTERY				CS_MAIL_LOTTERY				%ROWTYPE;
	REC_MAIL_LOTTERY_RATE			CS_MAIL_LOTTERY_RATE		%ROWTYPE;
	REC_MAIL_LOTTERY_DENOMINATOR	CS_MAIL_LOTTERY_DENOMINATOR	%ROWTYPE;
	
	REC_MAIL_LOTTERY_TICKET_COUNT	PKG_MAIL_LOTTERY_TICKET_COUNT.CS01%ROWTYPE;

	BUF_RANDOM_VALUE				NUMBER(5);
	BUF_DENOMINATOR					NUMBER(5);
	BUF_NUMERATOR					NUMBER(5);
	BUF_ADD_POINT_RESULT			VARCHAR2(1);
	BUF_ADD_POINT_STATUS			VARCHAR2(4);
	BUF_FOUND						BOOLEAN;
	
BEGIN
	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS						:= APP_COMM.STATUS_NORMAL;
	pRESULT						:= RESULT_OK;
	pMAIL_LOTTERY_RATE_SEQ		:= NULL;
	pMAIL_LOTTERY_REWARD_POINT	:= 0;

	BUF_RANDOM_VALUE			:= 0;
	BUF_DENOMINATOR				:= 0;
	BUF_NUMERATOR				:= 0;
	BUF_ADD_POINT_RESULT		:= '0';
	BUF_ADD_POINT_STATUS		:= APP_COMM.STATUS_NORMAL;

	OPEN CS_MAIL_LOTTERY;
	FETCH CS_MAIL_LOTTERY INTO REC_MAIL_LOTTERY;
	IF CS_MAIL_LOTTERY%NOTFOUND THEN
		pRESULT	:= RESULT_NOT_PUBLISH;
		CLOSE CS_MAIL_LOTTERY;
		RETURN;
	END IF;
	CLOSE CS_MAIL_LOTTERY;
	
	OPEN PKG_MAIL_LOTTERY_TICKET_COUNT.CS01(pSITE_CD,pUSER_SEQ,pUSER_CHAR_NO,REC_MAIL_LOTTERY.MAIL_LOTTERY_SEQ);
	FETCH PKG_MAIL_LOTTERY_TICKET_COUNT.CS01 INTO REC_MAIL_LOTTERY_TICKET_COUNT;
	IF PKG_MAIL_LOTTERY_TICKET_COUNT.CS01%FOUND THEN
		IF (REC_MAIL_LOTTERY_TICKET_COUNT.POSSESSION_COUNT >= REC_MAIL_LOTTERY.NEED_SECOND_LOTTERY_COUNT) THEN
			IF pCHECK_ONLY_FLAG = 1 THEN
				CLOSE PKG_MAIL_LOTTERY_TICKET_COUNT.CS01;
				ROLLBACK;
				RETURN;
			END IF;
			
			IF (pRESULT = RESULT_OK) THEN
				
				OPEN CS_MAIL_LOTTERY_DENOMINATOR(REC_MAIL_LOTTERY.MAIL_LOTTERY_SEQ);
				FETCH CS_MAIL_LOTTERY_DENOMINATOR INTO REC_MAIL_LOTTERY_DENOMINATOR;
				CLOSE CS_MAIL_LOTTERY_DENOMINATOR;
				
				IF REC_MAIL_LOTTERY_DENOMINATOR.DENOMINATOR = 0 THEN
					CLOSE PKG_MAIL_LOTTERY_TICKET_COUNT.CS01;
					pRESULT := RESULT_NG;
					ROLLBACK;
					RETURN;
				END IF;
				
				BUF_RANDOM_VALUE	:= GET_RANDOM_VALUE(1,REC_MAIL_LOTTERY_DENOMINATOR.DENOMINATOR);
				BUF_FOUND			:= FALSE;
				
				FOR REC_MAIL_LOTTERY_RATE IN CS_MAIL_LOTTERY_RATE(REC_MAIL_LOTTERY.MAIL_LOTTERY_SEQ) LOOP
				
					IF REC_MAIL_LOTTERY_RATE.RATE > 0 THEN
						BUF_NUMERATOR	:= BUF_NUMERATOR + REC_MAIL_LOTTERY_RATE.RATE;

						IF BUF_RANDOM_VALUE <= BUF_NUMERATOR THEN
							BUF_FOUND					:= TRUE;
							pMAIL_LOTTERY_RATE_SEQ		:= REC_MAIL_LOTTERY_RATE.MAIL_LOTTERY_RATE_SEQ;
							pMAIL_LOTTERY_REWARD_POINT			:= REC_MAIL_LOTTERY_RATE.REWARD_POINT;
							EXIT;
						END IF;
					END IF;
				END LOOP;
				
				IF (pMAIL_LOTTERY_REWARD_POINT > 0) THEN
					ADD_USER_DEF_POINT(
						pSITE_CD 		=> pSITE_CD,
						pUSER_SEQ 		=> pUSER_SEQ,
						pADD_POINT_ID 	=> PW_VICOMM_CONST.ADD_POINT_MAIL_LOTTERY_SECOND,
						pRESULT 		=> BUF_ADD_POINT_RESULT,
						pSTATUS 		=> BUF_ADD_POINT_STATUS,
						pADD_POINT		=> pMAIL_LOTTERY_REWARD_POINT
					);

					IF (BUF_ADD_POINT_RESULT != '0' OR BUF_ADD_POINT_STATUS != APP_COMM.STATUS_NORMAL) THEN
						pRESULT := RESULT_NG;
						CLOSE PKG_MAIL_LOTTERY_TICKET_COUNT.CS01;
						ROLLBACK;
						RETURN;
					END IF;
					
					IF (REC_MAIL_LOTTERY_TICKET_COUNT.POSSESSION_COUNT > REC_MAIL_LOTTERY.NEED_SECOND_LOTTERY_COUNT) THEN
						UPDATE T_MAIL_LOTTERY_TICKET_COUNT SET
							POSSESSION_COUNT	= POSSESSION_COUNT - REC_MAIL_LOTTERY.NEED_SECOND_LOTTERY_COUNT,
							UPDATE_DATE			= SYSDATE
						WHERE
							CURRENT OF PKG_MAIL_LOTTERY_TICKET_COUNT.CS01;
					ELSE
						DELETE FROM T_MAIL_LOTTERY_TICKET_COUNT
						WHERE
							CURRENT OF PKG_MAIL_LOTTERY_TICKET_COUNT.CS01;
					END IF;
				ELSE
					CLOSE PKG_MAIL_LOTTERY_TICKET_COUNT.CS01;
					pRESULT := RESULT_NG;
					ROLLBACK;
					RETURN;
				END IF;
			END IF;
		ELSE
			pRESULT := RESULT_NO_TICKET;
		END IF;
	ELSE
		pRESULT := RESULT_NO_TICKET;
	END IF;
	CLOSE PKG_MAIL_LOTTERY_TICKET_COUNT.CS01;
	
	COMMIT;

EXCEPTION
	WHEN OTHERS THEN

		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);
		
		PKG_MAIL_LOTTERY_TICKET_COUNT.CLOSE_ALL;

		IF CS_MAIL_LOTTERY%ISOPEN THEN
			CLOSE CS_MAIL_LOTTERY;
		END IF;

		IF CS_MAIL_LOTTERY_DENOMINATOR%ISOPEN THEN
			CLOSE CS_MAIL_LOTTERY_DENOMINATOR;
		END IF;

		IF CS_MAIL_LOTTERY_RATE%ISOPEN THEN
			CLOSE CS_MAIL_LOTTERY_RATE;
		END IF;

		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END GET_MAIL_LOTTERY_SECOND;
/
SHOW ERROR;
