/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: メール管理
--  Title			: メール既読フラグ更新
--	Progaram ID		: UPDATE_MAIL_READ_FLAG
--	Compile Turn	: 0
--  Creation Date	: 08.10.21
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE UPDATE_MAIL_READ_FLAG(
	pMAIL_SEQ	IN	VARCHAR2,
	pSTATUS		OUT	VARCHAR2
)IS
	-- CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'UPDATE_MAIL_READ_FLAG';

	REC_DOC		PKG_MAIL_LOG.CS02%ROWTYPE;

BEGIN
	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS	:= APP_COMM.STATUS_NORMAL;

	/*------------------------------*/
	/* Open Cursor					*/
	/*------------------------------*/
	OPEN PKG_MAIL_LOG.CS02(pMAIL_SEQ);

	/*------------------------------*/
	/* Update Record				*/
	/*------------------------------*/
	FETCH PKG_MAIL_LOG.CS02 INTO REC_DOC;
	IF PKG_MAIL_LOG.CS02%FOUND THEN
		IF (REC_DOC.READ_FLAG = 0 AND REC_DOC.RX_SEX_CD = VICOMM_CONST.MAN) THEN
			PKG_COMMUNICATION.UPDATE_COMM_STATUS(
				pSITE_CD				=>	REC_DOC.RX_SITE_CD,
				pSEX_CD					=>	VICOMM_CONST.MAN,
				pUSER_SEQ				=>	REC_DOC.RX_USER_SEQ,
				pUSER_CHAR_NO			=>	REC_DOC.RX_USER_CHAR_NO,
				pPARTNER_USER_SEQ		=>	REC_DOC.TX_USER_SEQ,
				pPARTNER_USER_CHAR_NO	=>	REC_DOC.TX_USER_CHAR_NO,
				pCOMMUNICATION_TYPE		=>	PKG_COMMUNICATION.COMM_TYPE_MAIL_READ_BY_MAN
			);
		END IF;

		UPDATE T_MAIL_LOG SET
				READ_FLAG = 1,
				READ_DATE = NVL(REC_DOC.READ_DATE,SYSDATE)
			WHERE
				CURRENT OF PKG_MAIL_LOG.CS02;
	END IF;

	CLOSE PKG_MAIL_LOG.CS02;
	
	COMMIT;

EXCEPTION
	WHEN OTHERS THEN

	LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);

		PKG_MAIL_LOG.CLOSE_ALL;
	
		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END UPDATE_MAIL_READ_FLAG;
/
SHOW ERROR;
