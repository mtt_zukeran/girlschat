/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: システム管理
--  Title			: ユーザー課金更新
--	Progaram ID		: USER_CHARGE_IMPORT
--	Compile Turn	: 0
--  Creation Date	: 09.06.01
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE USER_CHARGE_IMPORT(
	pSITE_CD					IN	VARCHAR2	,
	pUSER_RANK					IN	VARCHAR2	,
	pCHARGE_TYPE				IN	VARCHAR2	,
	pCATEGORY_INDEX				IN	NUMBER		,
	pCAST_RANK					IN	VARCHAR2	,
	pCAMPAIN_CD					IN	VARCHAR2	,
	pCHARGE_POINT_NORMAL		IN	NUMBER		,
	pCHARGE_POINT_NEW			IN	NUMBER		,
	pCHARGE_POINT_NO_RECEIPT	IN	NUMBER		,
	pCHARGE_POINT_NO_USE		IN	NUMBER		,
	pCHARGE_POINT_FREE_DIAL		IN	NUMBER		,
	pCHARGE_POINT_WHITE_PLAN	IN	NUMBER		,
	pCHARGE_CAST_TALK_APP_POINT	IN	NUMBER		,
	pCHARGE_MAN_TALK_APP_POINT	IN	NUMBER		,
	pCHARGE_UNIT_SEC			IN	NUMBER		,
	pSTATUS						OUT	VARCHAR2
)IS
	-- CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'USER_CHARGE_IMPORT';

	CURSOR CS_CATEGORY IS SELECT ACT_CATEGORY_SEQ FROM T_ACT_CATEGORY
		WHERE
			SITE_CD = pSITE_CD AND PRIORITY = pCATEGORY_INDEX;

	REC_USER_CHARGE	PKG_USER_CHARGE.CS02%ROWTYPE;

	BUF_ACT_CATEGORY_SEQ	T_ACT_CATEGORY.ACT_CATEGORY_SEQ%TYPE;

BEGIN
	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS			:= APP_COMM.STATUS_NORMAL;

	OPEN CS_CATEGORY;
	FETCH CS_CATEGORY INTO BUF_ACT_CATEGORY_SEQ;
	IF CS_CATEGORY%NOTFOUND THEN
		APP_COMM.TRACE(APP_COMM.TRACE_INFO,PGM_NM || ' CATEGORY INDEX NOT FOUND '|| pSITE_CD ||'-'||pCATEGORY_INDEX);
		CLOSE CS_CATEGORY;
		RETURN;
	END IF;
	CLOSE CS_CATEGORY;

	OPEN PKG_USER_CHARGE.CS02(pSITE_CD,pUSER_RANK,pCHARGE_TYPE,BUF_ACT_CATEGORY_SEQ,pCAST_RANK,pCAMPAIN_CD);
	FETCH PKG_USER_CHARGE.CS02 INTO REC_USER_CHARGE;

	IF PKG_USER_CHARGE.CS02%FOUND THEN
		UPDATE T_USER_CHARGE SET
				CHARGE_POINT_NORMAL			= pCHARGE_POINT_NORMAL			,
				CHARGE_POINT_NEW			= pCHARGE_POINT_NEW				,
				CHARGE_POINT_NO_RECEIPT		= pCHARGE_POINT_NO_RECEIPT		,
				CHARGE_POINT_NO_USE			= pCHARGE_POINT_NO_USE			,
				CHARGE_POINT_FREE_DIAL		= pCHARGE_POINT_FREE_DIAL		,
				CHARGE_POINT_WHITE_PLAN		= pCHARGE_POINT_WHITE_PLAN		,
				CHARGE_CAST_TALK_APP_POINT	= pCHARGE_CAST_TALK_APP_POINT	,
				CHARGE_MAN_TALK_APP_POINT	= pCHARGE_MAN_TALK_APP_POINT	,
				CHARGE_UNIT_SEC				= pCHARGE_UNIT_SEC				,
				UPDATE_DATE					= SYSDATE						,
				REVISION_NO					= SEQ_REVISION_NO.NEXTVAL
			WHERE
				CURRENT OF PKG_USER_CHARGE.CS02;
	ELSE
		INSERT INTO T_USER_CHARGE(
			SITE_CD						,
			USER_RANK					,
			CHARGE_TYPE					,
			ACT_CATEGORY_SEQ			,
			CAST_RANK					,
			CAMPAIN_CD					,
			CHARGE_POINT_NORMAL			,
			CHARGE_POINT_NEW			,
			CHARGE_POINT_NO_RECEIPT		,
			CHARGE_POINT_NO_USE			,
			CHARGE_POINT_FREE_DIAL		,
			CHARGE_POINT_WHITE_PLAN		,
			CHARGE_CAST_TALK_APP_POINT	,
			CHARGE_MAN_TALK_APP_POINT	,
			CHARGE_UNIT_SEC				,
			UPDATE_DATE					,
			REVISION_NO
		)VALUES(
			pSITE_CD					,
			pUSER_RANK					,
			pCHARGE_TYPE				,
			BUF_ACT_CATEGORY_SEQ		,
			pCAST_RANK					,
			pCAMPAIN_CD					,
			pCHARGE_POINT_NORMAL		,
			pCHARGE_POINT_NEW			,
			pCHARGE_POINT_NO_RECEIPT	,
			pCHARGE_POINT_NO_USE		,
			pCHARGE_POINT_FREE_DIAL		,
			pCHARGE_POINT_WHITE_PLAN	,
			pCHARGE_CAST_TALK_APP_POINT	,
			pCHARGE_MAN_TALK_APP_POINT	,
			pCHARGE_UNIT_SEC			,
			SYSDATE						,
			SEQ_REVISION_NO.NEXTVAL

		);
	END IF;
	
	CLOSE PKG_USER_CHARGE.CS02;

	COMMIT;

EXCEPTION
	WHEN OTHERS THEN

		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);

		IF CS_CATEGORY%ISOPEN THEN
			CLOSE CS_CATEGORY;
		END IF;

		PKG_USER_CHARGE.CLOSE_ALL;

		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);
END USER_CHARGE_IMPORT;
/
SHOW ERROR;
