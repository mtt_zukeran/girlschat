/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: システム管理
--  Title			: 督促レベル取得
--	Progaram ID		: URGE_LEVEL_GET
--	Compile Turn	: 0
--  Creation Date	: 09.12.14
--	Update Date		:
--  Author			: i-Brid(T.Tokunaga)
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE URGE_LEVEL_GET(
	pSITE_CD			IN	VARCHAR2	,
	pURGE_LEVEL			IN	VARCHAR2	,
	pDELAY_DAYS			OUT	NUMBER		,
	pROWID				OUT	VARCHAR2	,
	pREVISION_NO		OUT	NUMBER		,
	pRECORD_COUNT		OUT	NUMBER		,
	pSTATUS				OUT	VARCHAR2
)IS
	-- CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'URGE_LEVEL_GET';

	REC_URGE_LEVEL		PKG_URGE_LEVEL.CS02%ROWTYPE;
BEGIN

	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS				:= APP_COMM.STATUS_NORMAL;
	pRECORD_COUNT 		:= 0;

	pDELAY_DAYS			:= NULL;
	pROWID				:= NULL;
	pREVISION_NO		:= 0;
	pRECORD_COUNT		:= 0;

	/*------------------------------*/
	/* Get Record					*/
	/*------------------------------*/
	OPEN PKG_URGE_LEVEL.CS02(pSITE_CD,pURGE_LEVEL);
	FETCH PKG_URGE_LEVEL.CS02 INTO REC_URGE_LEVEL;

	IF PKG_URGE_LEVEL.CS02%FOUND THEN
		pDELAY_DAYS			:= REC_URGE_LEVEL.DELAY_DAYS		;
		pROWID				:= REC_URGE_LEVEL.ROWID				;
		pREVISION_NO		:= REC_URGE_LEVEL.REVISION_NO		;
		pRECORD_COUNT		:= 1								;
	END IF;

	CLOSE PKG_URGE_LEVEL.CS02;
	COMMIT;

EXCEPTION
	WHEN OTHERS THEN

		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);
		PKG_URGE_LEVEL.CLOSE_ALL;
		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END URGE_LEVEL_GET;
/
SHOW ERROR;