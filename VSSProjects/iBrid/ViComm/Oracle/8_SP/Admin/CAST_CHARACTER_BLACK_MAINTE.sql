/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: システム管理
--  Title			: 出演者ブラックリスト設定更新
--	Progaram ID		: CAST_CHARACTER_BLACK_MAINTE
--	Compile Turn	: 0
--  Creation Date	: 11.04.04
--	Update Date		:
--  Author			: i-Brid(R.Suzuki)
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE CAST_CHARACTER_BLACK_MAINTE(
	pSITE_CD			IN	VARCHAR2	,
	pUSER_SEQ			IN	VARCHAR2	,
	pUSER_CHAR_NO		IN	VARCHAR2	,
	pBLACK_TYPE			IN	VARCHAR2	,
	pREMARKS			IN	VARCHAR2	,
	pROWID				IN	VARCHAR2	,
	pREVISION_NO		IN	NUMBER		,
	pDEL_FLAG			IN	NUMBER		,
	pSTATUS				OUT	VARCHAR2
)IS
	--	CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'CAST_CHARACTER_BLACK_MAINTE';

	BUF_RESULT						VARCHAR2(1);
	REC_CAST_CHARACTER_BLACK		PKG_CAST_CHARACTER_BLACK.CS03%ROWTYPE;
BEGIN
	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS	:= APP_COMM.STATUS_NORMAL;

	/*------------------------------*/
	/* Open Cursor					*/
	/*------------------------------*/
	OPEN PKG_CAST_CHARACTER_BLACK.CS03(pROWID);

	/*------------------------------*/
	/* Update Record				*/
	/*------------------------------*/
	FETCH PKG_CAST_CHARACTER_BLACK.CS03 INTO REC_CAST_CHARACTER_BLACK;

	IF PKG_CAST_CHARACTER_BLACK.CS03%FOUND THEN

		IF (APP_COMM.CHECK_REVISION_NO(pREVISION_NO,REC_CAST_CHARACTER_BLACK.REVISION_NO) = TRUE) THEN

			UPDATE T_CAST_CHARACTER_BLACK SET
					REMARKS			= pREMARKS					,
					DEL_FLAG		= pDEL_FLAG					,
					UPDATE_DATE		= SYSDATE					,
					REVISION_NO		= SEQ_REVISION_NO.NEXTVAL
				WHERE
					CURRENT OF PKG_CAST_CHARACTER_BLACK.CS03;

		ELSE
			RAISE APP_COMM.UPDATE_BY_ANYONE;
		END IF;
	ELSE
		INSERT INTO T_CAST_CHARACTER_BLACK(
			SITE_CD				,
			USER_SEQ			,
			USER_CHAR_NO		,
			BLACK_TYPE			,
			REMARKS				,
			REGIST_DATE			,
			UPDATE_DATE			,
			DEL_FLAG			,
			REVISION_NO
		)VALUES(
			pSITE_CD			,
			pUSER_SEQ			,
			pUSER_CHAR_NO		,
			pBLACK_TYPE			,
			pREMARKS			,
			SYSDATE				,
			SYSDATE				,
			pDEL_FLAG			,
			SEQ_REVISION_NO.NEXTVAL
		);
	END IF;

	CLOSE PKG_CAST_CHARACTER_BLACK.CS03;

	IF (pBLACK_TYPE = VICOMM_CONST.CAST_BLACK_TYPE_QUICK AND pDEL_FLAG = 0) THEN
$IF $$PW_EXT $THEN
		QUICK_RESPONSE_ROOM(pSITE_CD,pUSER_SEQ,pUSER_CHAR_NO,'2',BUF_RESULT,pSTATUS,1);
$ELSE
		NULL;
$END
	END IF;
	COMMIT;

EXCEPTION
	WHEN OTHERS THEN

		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);

		PKG_CAST_CHARACTER_BLACK.CLOSE_ALL;

		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END CAST_CHARACTER_BLACK_MAINTE;
/
SHOW ERROR;
