/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: システム管理
--  Title			: 出演者属性種別更新
--	Progaram ID		: CAST_DIARY_ATTR_VALUE_MAINTE
--	Compile Turn	: 0
--  Creation Date	: 09.06.01
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE CAST_DIARY_ATTR_VALUE_MAINTE(
	pSITE_CD				IN	VARCHAR2	,
	pCAST_DIARY_ATTR_SEQ	IN	VARCHAR2	,
	pCAST_DIARY_ATTR_NM		IN	VARCHAR2	,
	pPRIORITY				IN	NUMBER		,
	pROWID					IN	VARCHAR2	,
	pREVISION_NO			IN	NUMBER		,
	pDEL_FLAG				IN	NUMBER		,
	pSTATUS					OUT	VARCHAR2
)IS
	--	CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'CAST_DIARY_ATTR_VALUE_MAINTE';

	REC_ATTR_TYPE	PKG_CAST_DIARY_ATTR_VALUE.CS03%ROWTYPE;
BEGIN
	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS	:= APP_COMM.STATUS_NORMAL;

	/*------------------------------*/
	/* Open Cursor					*/
	/*------------------------------*/
	OPEN PKG_CAST_DIARY_ATTR_VALUE.CS03(pROWID);

	/*------------------------------*/
	/* Update Record				*/
	/*------------------------------*/
	FETCH PKG_CAST_DIARY_ATTR_VALUE.CS03 INTO REC_ATTR_TYPE;

	IF PKG_CAST_DIARY_ATTR_VALUE.CS03%FOUND THEN

		IF (APP_COMM.CHECK_REVISION_NO(pREVISION_NO,REC_ATTR_TYPE.REVISION_NO) = TRUE) THEN
			IF pDEL_FLAG = 0 THEN

				UPDATE T_CAST_DIARY_ATTR_VALUE SET
						CAST_DIARY_ATTR_NM		= pCAST_DIARY_ATTR_NM		,
						PRIORITY				= pPRIORITY					,
						UPDATE_DATE				= SYSDATE					,
						REVISION_NO				= SEQ_REVISION_NO.NEXTVAL
					WHERE
						CURRENT OF PKG_CAST_DIARY_ATTR_VALUE.CS03;
			ELSE
				DELETE T_CAST_DIARY_ATTR_VALUE
					WHERE
						CURRENT OF PKG_CAST_DIARY_ATTR_VALUE.CS03;
			END IF;
		ELSE
			RAISE APP_COMM.UPDATE_BY_ANYONE;
		END IF;
	ELSE
		IF pDEL_FLAG = 0 THEN

			IF (pCAST_DIARY_ATTR_SEQ != 0) THEN
				LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,'T_CAST_DIARY_ATTR_VALUE NOT FOUND SEQ=' || pCAST_DIARY_ATTR_SEQ);
				RAISE PROGRAM_ERROR;
			END IF;

			INSERT INTO T_CAST_DIARY_ATTR_VALUE (
				SITE_CD							,
				CAST_DIARY_ATTR_SEQ				,
				CAST_DIARY_ATTR_NM				,
				PRIORITY						,
				UPDATE_DATE						,
				REVISION_NO
			)VALUES(
				pSITE_CD						,
				SEQ_DIARY_ATTR_VALUE.NEXTVAL	,
				pCAST_DIARY_ATTR_NM				,
				pPRIORITY						,
				SYSDATE							,
				SEQ_REVISION_NO.NEXTVAL
			);
		END IF;

	END IF;

	CLOSE PKG_CAST_DIARY_ATTR_VALUE.CS03;

	COMMIT;

EXCEPTION
	WHEN OTHERS THEN

		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);

		PKG_CAST_DIARY_ATTR_VALUE.CLOSE_ALL;

		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END CAST_DIARY_ATTR_VALUE_MAINTE;
/
SHOW ERROR;
