/*************************************************************************/
--	System			: ViComm
--	Sub System Name	: 
--	Title			: 掲示板レス　削除フラグ更新
--	Progaram ID		: BBS_RES_DEL_FLAG_UPDATE
--	Compile Turn	: 0
--	Creation Date	: 11.04.11
--	Update Date		:
--	Author			: i-Brid
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXX    XXXXXXXXXXX
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE BBS_RES_DEL_FLAG_UPDATE(
	pSITE_CD					IN	VARCHAR2				,
	pUSER_SEQ					IN	NUMBER					,
	pUSER_CHAR_NO				IN	VARCHAR2				,
	pBBS_THREAD_SEQ				IN	NUMBER					,
	pBBS_THREAD_SUB_SEQ			IN	NUMBER					,
	pDEL_FLAG					IN	NUMBER	DEFAULT 0		,
	pSTATUS						OUT	VARCHAR2
)IS
	--	CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'BBS_RES_DEL_FLAG_UPDATE';

	REC_BBS_RES		PKG_BBS_RES.CS02	%ROWTYPE;
BEGIN
	pSTATUS	:= APP_COMM.STATUS_NORMAL;
	
	OPEN PKG_BBS_RES.CS02(pSITE_CD,pUSER_SEQ,pUSER_CHAR_NO,pBBS_THREAD_SEQ,pBBS_THREAD_SUB_SEQ);
	FETCH PKG_BBS_RES.CS02 INTO REC_BBS_RES;

	IF PKG_BBS_RES.CS02%FOUND THEN
		IF ( REC_BBS_RES.DEL_FLAG <> pDEL_FLAG ) THEN
			/*
			 * UPDATE
			 */
			UPDATE T_BBS_RES SET
					DEL_FLAG		= pDEL_FLAG		,
					UPDATE_DATE		= SYSDATE
				WHERE
					CURRENT OF PKG_BBS_RES.CS02;
		END IF;

	END IF;

	CLOSE PKG_BBS_RES.CS02;
		
	COMMIT;

EXCEPTION
	WHEN OTHERS THEN

		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);
		PKG_BBS_RES.CLOSE_ALL;
		
		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END BBS_RES_DEL_FLAG_UPDATE;
/
SHOW ERROR;
