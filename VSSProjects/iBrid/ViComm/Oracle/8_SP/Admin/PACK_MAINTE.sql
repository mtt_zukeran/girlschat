/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: システム管理
--  Title			: パック設定更新
--	Progaram ID		: PACK_MAINTE
--	Compile Turn	: 0
--  Creation Date	: 09.12.14
--	Update Date		:
--  Author			: i-Brid(T.Tokunaga)
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  2010/12/06  ttakahashi FIRST_TIME_FLAGを追加
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE PACK_MAINTE(
	pSITE_CD			IN	VARCHAR2	,
	pSETTLE_TYPE		IN	VARCHAR2	,
	pSALES_AMT			IN	NUMBER		,
	pREMARKS			IN	VARCHAR2	,
	pCOMMODITIES_CD		IN	VARCHAR2	,
	pFIRST_TIME_FLAG	IN	NUMBER		,
	pROWID				IN	VARCHAR2	,
	pREVISION_NO		IN	NUMBER		,
	pDEL_FLAG			IN	NUMBER		,
	pSTATUS				OUT	VARCHAR2
)IS
	--	CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'PACK_MAINTE';

	REC_PACK		PKG_PACK.CS03%ROWTYPE;
BEGIN
	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS	:= APP_COMM.STATUS_NORMAL;

	/*------------------------------*/
	/* Open Cursor					*/
	/*------------------------------*/
	OPEN PKG_PACK.CS03(pROWID);

	/*------------------------------*/
	/* Update Record				*/
	/*------------------------------*/
	FETCH PKG_PACK.CS03 INTO REC_PACK;

	IF PKG_PACK.CS03%FOUND THEN

		IF (APP_COMM.CHECK_REVISION_NO(pREVISION_NO,REC_PACK.REVISION_NO) = TRUE) THEN
			IF pDEL_FLAG = 0 THEN

				UPDATE T_PACK SET
						SETTLE_TYPE		= pSETTLE_TYPE				,
						SALES_AMT		= pSALES_AMT				,
						REMARKS			= pREMARKS					,
						COMMODITIES_CD	= pCOMMODITIES_CD			,
						FIRST_TIME_FLAG	= pFIRST_TIME_FLAG			,
						UPDATE_DATE		= SYSDATE					,
						REVISION_NO		= SEQ_REVISION_NO.NEXTVAL
					WHERE
						CURRENT OF PKG_PACK.CS03;
			ELSE
				DELETE T_PACK
					WHERE
						CURRENT OF PKG_PACK.CS03;
			END IF;
		ELSE
			RAISE APP_COMM.UPDATE_BY_ANYONE;
		END IF;
	ELSE
		INSERT INTO T_PACK(
			SITE_CD				,
			SETTLE_TYPE			,
			SALES_AMT			,
			REMARKS				,
			COMMODITIES_CD		,
			FIRST_TIME_FLAG		,
			UPDATE_DATE			,
			REVISION_NO
		)VALUES(
			pSITE_CD			,
			pSETTLE_TYPE		,
			pSALES_AMT			,
			pREMARKS			,
			pCOMMODITIES_CD		,
			pFIRST_TIME_FLAG	,
			SYSDATE				,
			SEQ_REVISION_NO.NEXTVAL
		);
	END IF;

	CLOSE PKG_PACK.CS03;

	COMMIT;

EXCEPTION
	WHEN OTHERS THEN

		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);

		PKG_PACK.CLOSE_ALL;

		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END PACK_MAINTE;
/
SHOW ERROR;
