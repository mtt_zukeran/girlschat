/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: システム管理
--  Title			: 督促スケジュール取得
--	Progaram ID		: URGE_SCHEDULE_GET
--	Compile Turn	: 0
--  Creation Date	: 09.12.14
--	Update Date		:
--  Author			: i-Brid(T.Tokunaga)
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE URGE_SCHEDULE_GET(
	pSITE_CD					IN	VARCHAR2				,
	pURGE_LEVEL					IN	NUMBER					,
	pURGE_RECORD_COUNT			IN	NUMBER					,
--
	pURGE_COUNT_PER_DAY			OUT	APP_COMM.TBL_VARCHAR2	,
	pURGE_START_TIME			OUT	APP_COMM.TBL_VARCHAR2	,
	pURGE_END_TIME				OUT	APP_COMM.TBL_VARCHAR2	,
	pURGE_TYPE					OUT	APP_COMM.TBL_VARCHAR2	,
	pURGE_LAST_EXEC_DATE		OUT	APP_COMM.TBL_VARCHAR2	,
	pURGE_MAIL_TEMPLATE_NO		OUT	APP_COMM.TBL_VARCHAR2	,
	pURGE_ROWID					OUT	APP_COMM.TBL_VARCHAR2	,
	pURGE_REVISION_NO			OUT	APP_COMM.TBL_VARCHAR2	,
	pSTATUS						OUT	VARCHAR2				
)IS
	-- CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'URGE_SCHEDULE_GET';

	REC_URGE_SCHEDULE		PKG_URGE_SCHEDULE.CS02%ROWTYPE;
	BUF_INDEX				NUMBER(1);
BEGIN
	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS	:= APP_COMM.STATUS_NORMAL;

	/*------------------------------*/
	/* Get Record					*/
	/*------------------------------*/
	OPEN PKG_URGE_SCHEDULE.CS02(pSITE_CD,pURGE_LEVEL);
	FETCH PKG_URGE_SCHEDULE.CS02 INTO REC_URGE_SCHEDULE;

	FOR i IN 1..pURGE_RECORD_COUNT LOOP

		pURGE_COUNT_PER_DAY		(i)	:= '0'		;
		pURGE_START_TIME		(i)	:= '00:00'	;
		pURGE_END_TIME			(i)	:= '00:00'	;
		pURGE_TYPE				(i)	:= '1'		;
		pURGE_LAST_EXEC_DATE	(i) := NULL		;
		pURGE_MAIL_TEMPLATE_NO	(i) := NULL		;
		pURGE_ROWID				(i)	:= NULL		;
		pURGE_REVISION_NO		(i)	:= NULL		;

		IF PKG_URGE_SCHEDULE.CS02%FOUND THEN
			IF(i = REC_URGE_SCHEDULE.URGE_COUNT_PER_DAY) THEN	
				pURGE_COUNT_PER_DAY		(i)	:= REC_URGE_SCHEDULE.URGE_COUNT_PER_DAY		;
				pURGE_START_TIME	 	(i)	:= REC_URGE_SCHEDULE.URGE_START_TIME		;
				pURGE_END_TIME 			(i)	:= REC_URGE_SCHEDULE.URGE_END_TIME			;
				pURGE_TYPE 				(i)	:= REC_URGE_SCHEDULE.URGE_TYPE				;
				pURGE_LAST_EXEC_DATE	(i)	:= TO_CHAR(REC_URGE_SCHEDULE.URGE_LAST_EXEC_DATE,'YYYY/MM/DD HH24:MI');
				pURGE_MAIL_TEMPLATE_NO	(i)	:= REC_URGE_SCHEDULE.URGE_MAIL_TEMPLATE_NO	;
				pURGE_ROWID				(i)	:= REC_URGE_SCHEDULE.ROWID					;
				pURGE_REVISION_NO		(i)	:= REC_URGE_SCHEDULE.REVISION_NO			;
				FETCH PKG_URGE_SCHEDULE.CS02 INTO REC_URGE_SCHEDULE;
			END IF;
		END IF;
	END LOOP;

	CLOSE PKG_URGE_SCHEDULE.CS02;

	COMMIT;

EXCEPTION
	WHEN OTHERS THEN

		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);

		PKG_URGE_SCHEDULE.CLOSE_ALL;

		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END URGE_SCHEDULE_GET;
/
SHOW ERROR;
