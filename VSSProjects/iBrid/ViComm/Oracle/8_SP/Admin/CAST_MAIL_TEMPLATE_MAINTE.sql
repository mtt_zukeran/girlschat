/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: システム管理
--  Title			: キャストメールテンプレート更新
--	Progaram ID		: CAST_MAIL_TEMPLATE_MAINTE
--	Compile Turn	: 0
--  Creation Date	: 09.06.01
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--  2009.11.30 Adding Parameter
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE CAST_MAIL_TEMPLATE_MAINTE(
	pSITE_CD			IN	VARCHAR2	,
	pUSER_SEQ			IN	VARCHAR2	,
	pUSER_CHAR_NO		IN	VARCHAR2	,
	pMAIL_TEMPLATE_NO	IN	VARCHAR2	,
	pHTML_DOC_SUB_SEQ	IN	VARCHAR2	,
	pMAIL_TITLE			IN	VARCHAR2	,
	pHTML_DOC_TITLE		IN	VARCHAR2	,
	pHTML_DOC			IN	APP_COMM.TBL_LONG_VARCHAR2,
	pHTML_DOC_COUNT		IN	NUMBER		,
	pROWID_VIEW			IN	VARCHAR2	,
	pREVISION_NO_VIEW	IN	NUMBER		,
	pROWID_MANAGE		IN	VARCHAR2	,
	pREVISION_NO_MANAGE	IN	NUMBER		,
	pROWID_DOC			IN	VARCHAR2	,
	pREVISION_NO_DOC	IN	NUMBER		,
	pDEL_FLAG			IN	NUMBER		,
	pHTML_DOC_SEQ		OUT	VARCHAR2	,
	pSTATUS				OUT	VARCHAR2
)IS
	--	CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'CAST_MAIL_TEMPLATE_MAINTE';

	REC_TEMPLATE	PKG_CAST_MAIL_TEMPLATE.CS03%ROWTYPE;

	BUF_CNT		NUMBER(3);

BEGIN
	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS			:= APP_COMM.STATUS_NORMAL;
	pHTML_DOC_SEQ	:= NULL;

	SITE_HTML_DOC_MAINTE(
		pSITE_CD							,
		pHTML_DOC_SUB_SEQ					,
		VICOMM_CONST.DOC_GRP_MAIL_TEMPLATE	,
		NULL								,
		VICOMM_CONST.DEFUALT_PUB_DAY		,
		pHTML_DOC_TITLE						,
		pHTML_DOC							,
		pHTML_DOC_COUNT						,
		NULL								,	-- PAGE_TITLE            
		NULL								,	-- PAGE_KEYWORD          
		NULL								,	-- PAGE_DESCRIPTION      
		0									,	-- TABLE_INDEX
		NULL								,	-- WEB_FACE_SEQ
		VICOMM_CONST.WOMAN					,
		NULL								,	-- END PUB_DAY
		NULL								,   -- NON DISP IN MAIL_BOX
		NULL								,   -- NON DISP IN DECOMAIL
		NULL								,	-- USER AGENT
		NULL								,	-- CSS FILE NM
		NULL								,	--            2
		NULL								,	--            3
		NULL								,	--            4
		NULL								,	--            5
		NULL								,	-- JAVASCRIPT FILE NM 1
		NULL								,	--                    2
		NULL								,	--                    3
		NULL								,	--                    4
		NULL								,	--                    5
		NULL								,	-- CANONICAL
		1									,	-- PROTECT_IMAGE_FLAG
		1									,	-- EMOJI_TOOL_FLAG
		0									,	-- NOINDEX_FLAG
		0									,	-- NOFOLLOW_FLAG
		0									,	-- NOARCHIVE_FLAG
		0									,	-- NA_FLAG
		pROWID_MANAGE						,
		pREVISION_NO_MANAGE					,
		pROWID_DOC							,
		pREVISION_NO_DOC					,
		pDEL_FLAG							,
		pHTML_DOC_SEQ						,
		pSTATUS
	);

	IF (pSTATUS != APP_COMM.STATUS_NORMAL) THEN
		RAISE PROGRAM_ERROR;
	END IF;

	IF (pDEL_FLAG = 0) THEN
		OPEN PKG_CAST_MAIL_TEMPLATE.CS03(pROWID_VIEW);
	
		FETCH PKG_CAST_MAIL_TEMPLATE.CS03 INTO REC_TEMPLATE;
		IF PKG_CAST_MAIL_TEMPLATE.CS03%FOUND THEN
			IF (APP_COMM.CHECK_REVISION_NO(pREVISION_NO_VIEW,REC_TEMPLATE.REVISION_NO) = TRUE) THEN
				UPDATE T_CAST_MAIL_TEMPLATE SET
						MAIL_TITLE			= pMAIL_TITLE			,
						HTML_DOC_SEQ		= pHTML_DOC_SEQ			,
						MAIL_TEMPLATE_NO 	= pMAIL_TEMPLATE_NO		,
						UPDATE_DATE			= SYSDATE				,
						REVISION_NO			= SEQ_REVISION_NO.NEXTVAL
					WHERE
						CURRENT OF PKG_CAST_MAIL_TEMPLATE.CS03;
			ELSE
				RAISE APP_COMM.UPDATE_BY_ANYONE;
			END IF;
		ELSE
			INSERT INTO T_CAST_MAIL_TEMPLATE(
				SITE_CD					,
				USER_SEQ				,
				USER_CHAR_NO			,
				MAIL_TEMPLATE_NO		,
				MAIL_TITLE				,
				HTML_DOC_SEQ			,
				UPDATE_DATE				,
				REVISION_NO
			)VALUES(
				pSITE_CD				,
				pUSER_SEQ				,
				pUSER_CHAR_NO			,
				pMAIL_TEMPLATE_NO		,
				pMAIL_TITLE				,
				pHTML_DOC_SEQ			,
				SYSDATE					,
				SEQ_REVISION_NO.NEXTVAL
			);
		END IF;
	
		CLOSE PKG_CAST_MAIL_TEMPLATE.CS03;
	
	END IF;

	COMMIT;

EXCEPTION
	WHEN OTHERS THEN

		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);

		PKG_CAST_MAIL_TEMPLATE.CLOSE_ALL;

		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END CAST_MAIL_TEMPLATE_MAINTE;
/
SHOW ERROR;
