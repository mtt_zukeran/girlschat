/*************************************************************************/
--	System			: ViComm
--	Sub System Name	: システム管理
--	Title			: デートプラン更新
--	Progaram ID		: DATE_PLAN_MAINTE
--	Compile Turn	: 0
--	Creation Date	: 11.07.19
--	Update Date		:
--	Author			: iBrid
/*************************************************************************/
-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
/*-----------------------------------------------------------------------*/

CREATE OR REPLACE PROCEDURE DATE_PLAN_MAINTE(
	pSITE_CD						IN		VARCHAR2	,
	pDATE_PLAN_SEQ					IN OUT	VARCHAR2	,
	pDATE_PLAN_NM					IN		VARCHAR2	,
	pCHARGE_FLAG					IN		NUMBER		,
	pPRICE							IN		NUMBER		,
	pPAYMENT						IN		NUMBER		,
	pFRIENDLY_POINT					IN		NUMBER		,
	pWAITING_MIN					IN		NUMBER		,
	pSTAGE_SEQ						IN		NUMBER		,
	pREVISION_NO					IN		NUMBER		,
--
	pDELETE_FLAG					IN		NUMBER		DEFAULT 0	,
	pSTATUS							OUT		VARCHAR2
)IS
--	CONST
	PGM_NM		CONSTANT VARCHAR(255)	:= 'DATE_PLAN_MAINTE';

--	REC
	REC_DATE_PLAN	PKG_DATE_PLAN.CS02%ROWTYPE;
BEGIN
	pSTATUS	:= APP_COMM.STATUS_NORMAL;

	OPEN PKG_DATE_PLAN.CS02(pSITE_CD,pDATE_PLAN_SEQ);
	FETCH PKG_DATE_PLAN.CS02 INTO REC_DATE_PLAN;
	IF (PKG_DATE_PLAN.CS02%FOUND) THEN
		IF (APP_COMM.CHECK_REVISION_NO(pREVISION_NO,REC_DATE_PLAN.REVISION_NO)) THEN
			IF (pDELETE_FLAG = 1) THEN
				/*
				 * DELETE
				 */
				DELETE FROM T_DATE_PLAN
				WHERE
					CURRENT OF PKG_DATE_PLAN.CS02;

			ELSE
				/*
				 * UPDATE
				 */
				UPDATE T_DATE_PLAN SET
					DATE_PLAN_NM				= pDATE_PLAN_NM					,
					CHARGE_FLAG					= pCHARGE_FLAG					,
					PRICE						= pPRICE						,
					PAYMENT						= pPAYMENT						,
					FRIENDLY_POINT				= pFRIENDLY_POINT				,
					WAITING_MIN					= pWAITING_MIN					,
					STAGE_SEQ					= pSTAGE_SEQ					,
					REVISION_NO					= SEQ_REVISION_NO.NEXTVAL		,
					UPDATE_DATE					= SYSDATE
				WHERE
					CURRENT OF PKG_DATE_PLAN.CS02;

			END IF;
		ELSE
			-- 楽観ロックエラー
			RAISE APP_COMM.UPDATE_BY_ANYONE;
		END IF;

	ELSE
		/*
		 * INSERT
		 */
		SELECT SEQ_DATE_PLAN.NEXTVAL INTO pDATE_PLAN_SEQ FROM DUAL;
		INSERT INTO T_DATE_PLAN(
			SITE_CD						,
			DATE_PLAN_SEQ				,
			DATE_PLAN_NM				,
			CHARGE_FLAG					,
			PRICE						,
			PAYMENT						,
			FRIENDLY_POINT				,
			WAITING_MIN					,
			STAGE_SEQ					,
			REVISION_NO					,
			UPDATE_DATE
		) VALUES(
			pSITE_CD					,
			pDATE_PLAN_SEQ				,
			pDATE_PLAN_NM				,
			pCHARGE_FLAG				,
			pPRICE						,
			pPAYMENT					,
			pFRIENDLY_POINT				,
			pWAITING_MIN				,
			pSTAGE_SEQ					,
			SEQ_REVISION_NO.NEXTVAL		,
			SYSDATE
		);

	END IF;
	CLOSE PKG_DATE_PLAN.CS02;

	COMMIT;

EXCEPTION
	WHEN OTHERS THEN
		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);
		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

		PKG_DATE_PLAN.CLOSE_ALL;

END DATE_PLAN_MAINTE;
/
SHOW ERROR;
