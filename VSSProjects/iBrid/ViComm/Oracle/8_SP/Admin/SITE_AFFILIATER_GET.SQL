/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: システム管理
--  Title			: サイト別アフリエター取得
--	Progaram ID		: SITE_AFFILIATER_GET
--	Compile Turn	: 0
--  Creation Date	: 09.06.01
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE SITE_AFFILIATER_GET(
	pSITE_CD				IN	VARCHAR2	,
	pAFFILIATER_CD			IN	VARCHAR2	,
	pTRACKING_ADDITION_INFO	OUT	VARCHAR2	,
	pAD_CD					OUT	VARCHAR2	,
	pAD_NM					OUT	VARCHAR2	,
	pROWID					OUT	VARCHAR2	,
	pREVISION_NO			OUT	NUMBER		,
	pRECORD_COUNT			OUT	NUMBER		,
	pSTATUS					OUT	VARCHAR2
)IS
	-- CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'SITE_AFFILIATER_GET';

	CURSOR CS_AD(prmAD_CD T_AD.AD_CD%TYPE) IS SELECT AD_NM FROM T_AD
		WHERE
			AD_CD = prmAD_CD;

	REC_SITE_AFFILIATER	PKG_SITE_AFFILIATER.CS02%ROWTYPE;
BEGIN
	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS			:= APP_COMM.STATUS_NORMAL;
	pRECORD_COUNT 	:= 0;
	pROWID			:= NULL;
	pREVISION_NO	:= 0;

	pAD_CD			:= NULL;
	pAD_NM			:= NULL;

	/*------------------------------*/
	/* Get Record					*/
	/*------------------------------*/
	OPEN PKG_SITE_AFFILIATER.CS02(pSITE_CD,pAFFILIATER_CD);
	FETCH PKG_SITE_AFFILIATER.CS02 INTO REC_SITE_AFFILIATER;

	IF PKG_SITE_AFFILIATER.CS02%FOUND THEN
		pAD_CD					:= REC_SITE_AFFILIATER.AD_CD		;
		pTRACKING_ADDITION_INFO	:= REC_SITE_AFFILIATER.TRACKING_ADDITION_INFO;
		pROWID					:= REC_SITE_AFFILIATER.ROWID		;
		pREVISION_NO			:= REC_SITE_AFFILIATER.REVISION_NO	;
		pRECORD_COUNT			:= 1;

		OPEN CS_AD(REC_SITE_AFFILIATER.AD_CD);
		FETCH CS_AD INTO pAD_NM;
		CLOSE CS_AD;

	END IF;

	CLOSE PKG_SITE_AFFILIATER.CS02;

	COMMIT;

EXCEPTION
	WHEN OTHERS THEN

		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);

		PKG_SITE_AFFILIATER.CLOSE_ALL;

		IF CS_AD%ISOPEN THEN
			CLOSE CS_AD;
		END IF;

		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END SITE_AFFILIATER_GET;
/
SHOW ERROR;
