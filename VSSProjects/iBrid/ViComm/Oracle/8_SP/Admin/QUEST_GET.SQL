/*************************************************************************/
--	System			: ViComm
--	Sub System Name	: システム管理
--	Title			: クエスト設定取得
--	Progaram ID		: QUEST_GET
--	Compile Turn	: 0
--	Creation Date	: 12.07.05
--	Update Date		:
--	Author			:M&TT Y.Ikemiya
/*************************************************************************/
-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
/*-----------------------------------------------------------------------*/

CREATE OR REPLACE PROCEDURE QUEST_GET(
	pSITE_CD						IN	VARCHAR2	,
	pQUEST_SEQ						IN	VARCHAR2	,
	pQUEST_NM						OUT	VARCHAR2	,
	pSEX_CD							OUT	VARCHAR2	,
	pGET_REWARD_END_DATE			OUT	VARCHAR2	,
	pPUBLISH_START_DATE				OUT	VARCHAR2	,
	pPUBLISH_END_DATE				OUT	VARCHAR2	,
	pPUBLISH_FLAG					OUT	VARCHAR2	,
	pREVISION_NO					OUT	NUMBER		,
--
	pSTATUS							OUT	VARCHAR2
)IS
--	CONST
	PGM_NM							CONSTANT VARCHAR(255)	:= 'QUEST_GET';

--	REC
	REC_QUEST						PKG_QUEST.CS02%ROWTYPE;
BEGIN
	-- Initialize
	pSTATUS							:= APP_COMM.STATUS_NORMAL;
	pQUEST_NM						:= NULL;
	pSEX_CD							:= NULL;
	pGET_REWARD_END_DATE			:= NULL;
	pPUBLISH_START_DATE				:= NULL;
	pPUBLISH_END_DATE				:= NULL;
	pPUBLISH_FLAG					:= NULL;
	pREVISION_NO					:= 0;

	OPEN PKG_QUEST.CS02(pSITE_CD,pQUEST_SEQ);
	FETCH PKG_QUEST.CS02 INTO REC_QUEST;
	IF PKG_QUEST.CS02%FOUND THEN
		pQUEST_NM						:= REC_QUEST.QUEST_NM						;
		pSEX_CD							:= REC_QUEST.SEX_CD							;
		pGET_REWARD_END_DATE			:= TO_CHAR(REC_QUEST.GET_REWARD_END_DATE		, 'YYYY/MM/DD HH24:MI:SS')	;
		pPUBLISH_START_DATE				:= TO_CHAR(REC_QUEST.PUBLISH_START_DATE		, 'YYYY/MM/DD HH24:MI:SS')	;
		pPUBLISH_END_DATE				:= TO_CHAR(REC_QUEST.PUBLISH_END_DATE			, 'YYYY/MM/DD HH24:MI:SS')	;
		pPUBLISH_FLAG					:= REC_QUEST.PUBLISH_FLAG					;
		pREVISION_NO					:= REC_QUEST.REVISION_NO					;
	END IF;
	CLOSE PKG_QUEST.CS02;
	
	COMMIT;

EXCEPTION
	WHEN OTHERS THEN
		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);
		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

		PKG_QUEST.CLOSE_ALL;

END QUEST_GET;
/
SHOW ERROR;
