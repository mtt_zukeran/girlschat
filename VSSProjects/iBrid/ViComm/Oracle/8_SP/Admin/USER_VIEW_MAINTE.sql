/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: システム管理
--  Title			: ユーザービュー更新
--	Progaram ID		: USER_VIEW_MAINTE
--	Compile Turn	: 0
--  Creation Date	: 09.06.01
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE USER_VIEW_MAINTE(
	pSITE_CD				IN	VARCHAR2	,
	pPROGRAM_ROOT			IN	VARCHAR2	,
	pPROGRAM_ID				IN	VARCHAR2	,
	pUSER_AGENT_TYPE		IN	VARCHAR2	,
	pAD_GROUP_CD			IN	VARCHAR2	,
	pUSER_RANK				IN	VARCHAR2	,
	pACT_CATEGORY_SEQ		IN	VARCHAR2	,
	pPAGE_TITLE				IN	VARCHAR2	,
	pPAGE_KEYWORD			IN	VARCHAR2	,
	pPAGE_DESCRIPTION		IN	VARCHAR2	,
	pTABLE_INDEX			IN	NUMBER		,
	pCSS_FILE_NM1			IN	VARCHAR2	,
	pCSS_FILE_NM2			IN	VARCHAR2	,
	pCSS_FILE_NM3			IN	VARCHAR2	,
	pCSS_FILE_NM4			IN	VARCHAR2	,
	pCSS_FILE_NM5			IN	VARCHAR2	,
	pJAVASCRIPT_FILE_NM1	IN	VARCHAR2	,
	pJAVASCRIPT_FILE_NM2	IN	VARCHAR2	,
	pJAVASCRIPT_FILE_NM3	IN	VARCHAR2	,
	pJAVASCRIPT_FILE_NM4	IN	VARCHAR2	,
	pJAVASCRIPT_FILE_NM5	IN	VARCHAR2	,
	pPROTECT_IMAGE_FLAG		IN	NUMBER		,
	pEMOJI_TOOL_FLAG		IN	NUMBER		,
	pNOINDEX_FLAG			IN	NUMBER		,
	pNOFOLLOW_FLAG			IN	NUMBER		,
	pNOARCHIVE_FLAG			IN	NUMBER		,
	pHTML_DOC_SUB_SEQ		IN	VARCHAR2	,
	pHTML_DOC_TITLE			IN	VARCHAR2	,
	pHTML_DOC				IN	APP_COMM.TBL_LONG_VARCHAR2,
	pHTML_DOC_COUNT			IN	NUMBER		,
	pROWID_VIEW				IN	VARCHAR2	,
	pREVISION_NO_VIEW		IN	NUMBER		,
	pROWID_MANAGE			IN	VARCHAR2	,
	pREVISION_NO_MANAGE		IN	NUMBER		,
	pROWID_DOC				IN	VARCHAR2	,
	pREVISION_NO_DOC		IN	NUMBER		,
	pDEL_FLAG				IN	NUMBER		,
	pHTML_DOC_SEQ			OUT	VARCHAR2	,
	pSTATUS					OUT	VARCHAR2
)IS
	-- CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'USER_VIEW_MAINTE';


	CURSOR CS_DTL_CNT IS SELECT COUNT(*) FROM T_SITE_HTML_DOC
		WHERE
			HTML_DOC_SEQ = pHTML_DOC_SEQ;

	REC_VIEW	PKG_USER_VIEW.CS03%ROWTYPE;

	BUF_VIEW_KEY_MASK T_VIEW_KEY.VIEW_KEY_MASK%TYPE;
	BUF_CNT		NUMBER(3);

BEGIN
	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS			:= APP_COMM.STATUS_NORMAL;
	pHTML_DOC_SEQ	:= NULL;

	SITE_HTML_DOC_MAINTE(
		pSITE_CD						,
		pHTML_DOC_SUB_SEQ				,
		VICOMM_CONST.DOC_GRP_USER_VIEW	,
		NULL							,
		VICOMM_CONST.DEFUALT_PUB_DAY	,
		pHTML_DOC_TITLE					,
		pHTML_DOC						,
		pHTML_DOC_COUNT					,
		pPAGE_TITLE						,
		pPAGE_KEYWORD					,
		pPAGE_DESCRIPTION				,
		pTABLE_INDEX					,
		NULL							, -- WEB_FACE_SEQ
		NULL							, -- SEX CD
		NULL							,
		NULL							, -- NON_DISP_IN_MAIL_BOX
		NULL							, -- NON_DISP_IN_DECOMAIL
		pUSER_AGENT_TYPE				,
		pCSS_FILE_NM1					,
		pCSS_FILE_NM2					,
		pCSS_FILE_NM3					,
		pCSS_FILE_NM4					,
		pCSS_FILE_NM5					,
		pJAVASCRIPT_FILE_NM1			, -- JAVASCRIPT FILE NM1
		pJAVASCRIPT_FILE_NM2			, -- JAVASCRIPT FILE NM2
		pJAVASCRIPT_FILE_NM3			, -- JAVASCRIPT FILE NM3
		pJAVASCRIPT_FILE_NM4			, -- JAVASCRIPT FILE NM4
		pJAVASCRIPT_FILE_NM5			, -- JAVASCRIPT FILE NM5
		NULL							, -- CANONICAL
		pPROTECT_IMAGE_FLAG				,
		pEMOJI_TOOL_FLAG				,
		pNOINDEX_FLAG					,
		pNOFOLLOW_FLAG					,
		pNOARCHIVE_FLAG					,
		0								, -- NA FLAG
		pROWID_MANAGE					,
		pREVISION_NO_MANAGE				,
		pROWID_DOC						,
		pREVISION_NO_DOC				,
		pDEL_FLAG						,
		pHTML_DOC_SEQ					,
		pSTATUS
	);
	IF (pSTATUS != APP_COMM.STATUS_NORMAL) THEN
		RAISE PROGRAM_ERROR;
	END IF;


	BUF_VIEW_KEY_MASK := 0;
	IF pAD_GROUP_CD != VICOMM_CONST.DEFAUL_AD_GROUP_CD THEN
		BUF_VIEW_KEY_MASK := BUF_VIEW_KEY_MASK + VICOMM_CONST.VIEW_MASK_AD_GROUP;
	END IF;
	
	IF pUSER_RANK != VICOMM_CONST.DEFAUL_USER_RANK THEN
		BUF_VIEW_KEY_MASK := BUF_VIEW_KEY_MASK + VICOMM_CONST.VIEW_MASK_USER_RANK;
	END IF;
	
	IF pACT_CATEGORY_SEQ != VICOMM_CONST.DEFAULT_ACT_CATEGORY_SEQ THEN
		BUF_VIEW_KEY_MASK := BUF_VIEW_KEY_MASK + VICOMM_CONST.VIEW_MASK_ACT_CATEGORY;
	END IF;
	

	IF (pDEL_FLAG = 0) THEN
		OPEN PKG_USER_VIEW.CS03(pROWID_VIEW);
	
		FETCH PKG_USER_VIEW.CS03 INTO REC_VIEW;
		IF PKG_USER_VIEW.CS03%FOUND THEN
			IF (APP_COMM.CHECK_REVISION_NO(pREVISION_NO_VIEW,REC_VIEW.REVISION_NO) = TRUE) THEN
				UPDATE T_USER_VIEW SET
						VIEW_KEY_MASK	= BUF_VIEW_KEY_MASK		,
						HTML_DOC_SEQ	= pHTML_DOC_SEQ			,
						UPDATE_DATE		= SYSDATE				,
						REVISION_NO		= SEQ_REVISION_NO.NEXTVAL
					WHERE
						CURRENT OF PKG_USER_VIEW.CS03;
			ELSE
				RAISE APP_COMM.UPDATE_BY_ANYONE;
			END IF;
		ELSE
			INSERT INTO T_USER_VIEW(
				SITE_CD				,
				PROGRAM_ROOT		,
				PROGRAM_ID			,
				USER_AGENT_TYPE		,
				AD_GROUP_CD			,
				USER_RANK			,
				ACT_CATEGORY_SEQ	,
				HTML_DOC_SEQ		,
				VIEW_KEY_MASK		,
				UPDATE_DATE			,
				REVISION_NO
			)VALUES(
				pSITE_CD			,
				pPROGRAM_ROOT		,
				pPROGRAM_ID			,
				pUSER_AGENT_TYPE	,
				pAD_GROUP_CD		,
				pUSER_RANK			,
				pACT_CATEGORY_SEQ	,
				pHTML_DOC_SEQ		,
				BUF_VIEW_KEY_MASK	,
				SYSDATE				,
				SEQ_REVISION_NO.NEXTVAL
			);
		END IF;
	
		CLOSE PKG_USER_VIEW.CS03;
	
	END IF;

	IF (pDEL_FLAG = 0) THEN
		BEGIN
			INSERT INTO T_VIEW_KEY(
				SITE_CD			,
				PROGRAM_ROOT	,
				PROGRAM_ID		,
				USER_AGENT_TYPE	,
				VIEW_KEY_MASK	,
				UPDATE_DATE
			)VALUES(
				pSITE_CD			,
				pPROGRAM_ROOT		,
				pPROGRAM_ID			,
				PUSER_AGENT_TYPE	,
				BUF_VIEW_KEY_MASK	,
				SYSDATE
			);
		EXCEPTION
			WHEN DUP_VAL_ON_INDEX THEN
				NULL;
		END;
	ELSE
		BUF_CNT := 0;
		OPEN CS_DTL_CNT;
		FETCH CS_DTL_CNT INTO BUF_CNT;
		CLOSE CS_DTL_CNT;

		BUF_CNT := NVL(BUF_CNT,0);

		IF (BUF_CNT = 0) THEN
			DELETE T_VIEW_KEY WHERE
				SITE_CD 		= pSITE_CD 			AND
				PROGRAM_ROOT	= pPROGRAM_ROOT		AND
				PROGRAM_ID		= pPROGRAM_ID		AND
				USER_AGENT_TYPE	= pUSER_AGENT_TYPE	AND
				VIEW_KEY_MASK	= BUF_VIEW_KEY_MASK;
		END IF;
	END IF;

	COMMIT;


EXCEPTION
	WHEN OTHERS THEN

		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);

		PKG_USER_VIEW.CLOSE_ALL;

		IF CS_DTL_CNT%ISOPEN THEN
			CLOSE CS_DTL_CNT;
		END IF;

		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);
END USER_VIEW_MAINTE;
/
SHOW ERROR;
