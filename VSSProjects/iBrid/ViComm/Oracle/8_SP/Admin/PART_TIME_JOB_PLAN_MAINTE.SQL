/*************************************************************************/
--	System			: ViComm
--	Sub System Name	: システム管理
--	Title			: バイトプラン更新
--	Progaram ID		: PART_TIME_JOB_PLAN_MAINTE
--	Compile Turn	: 0
--	Creation Date	: 11.07.19
--	Update Date		:
--	Author			: iBrid
/*************************************************************************/
-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
/*-----------------------------------------------------------------------*/

CREATE OR REPLACE PROCEDURE PART_TIME_JOB_PLAN_MAINTE(
	pSITE_CD						IN		VARCHAR2	,
	pPART_TIME_JOB_SEQ				IN OUT	VARCHAR2	,
	pPART_TIME_JOB_NM				IN		VARCHAR2	,
	pINCOME							IN		NUMBER		,
	pWAITING_MIN					IN		NUMBER		,
	pSTAGE_SEQ						IN		NUMBER		,
	pREVISION_NO					IN		NUMBER		,
--
	pDELETE_FLAG					IN		NUMBER		DEFAULT 0	,
	pSTATUS							OUT		VARCHAR2
)IS
--	CONST
	PGM_NM		CONSTANT VARCHAR(255)	:= 'PART_TIME_JOB_PLAN_MAINTE';

--	REC
	REC_PART_TIME_JOB_PLAN	PKG_PART_TIME_JOB_PLAN.CS02%ROWTYPE;
BEGIN
	pSTATUS	:= APP_COMM.STATUS_NORMAL;

	OPEN PKG_PART_TIME_JOB_PLAN.CS02(pSITE_CD,pPART_TIME_JOB_SEQ);
	FETCH PKG_PART_TIME_JOB_PLAN.CS02 INTO REC_PART_TIME_JOB_PLAN;
	IF (PKG_PART_TIME_JOB_PLAN.CS02%FOUND) THEN
		IF (APP_COMM.CHECK_REVISION_NO(pREVISION_NO,REC_PART_TIME_JOB_PLAN.REVISION_NO)) THEN
			IF (pDELETE_FLAG = 1) THEN
				/*
				 * DELETE
				 */
				DELETE FROM T_PART_TIME_JOB_PLAN
				WHERE
					CURRENT OF PKG_PART_TIME_JOB_PLAN.CS02;

			ELSE
				/*
				 * UPDATE
				 */
				UPDATE T_PART_TIME_JOB_PLAN SET
					PART_TIME_JOB_NM			= pPART_TIME_JOB_NM				,
					INCOME						= pINCOME						,
					WAITING_MIN					= pWAITING_MIN					,
					STAGE_SEQ					= pSTAGE_SEQ					,
					REVISION_NO					= SEQ_REVISION_NO.NEXTVAL		,
					UPDATE_DATE					= SYSDATE
				WHERE
					CURRENT OF PKG_PART_TIME_JOB_PLAN.CS02;

			END IF;
		ELSE
			-- 楽観ロックエラー
			RAISE APP_COMM.UPDATE_BY_ANYONE;
		END IF;

	ELSE
		/*
		 * INSERT
		 */
		SELECT SEQ_PART_TIME_JOB.NEXTVAL INTO pPART_TIME_JOB_SEQ FROM DUAL;
		INSERT INTO T_PART_TIME_JOB_PLAN(
			SITE_CD						,
			PART_TIME_JOB_SEQ			,
			PART_TIME_JOB_NM			,
			INCOME						,
			WAITING_MIN					,
			STAGE_SEQ					,
			REVISION_NO					,
			UPDATE_DATE
		) VALUES(
			pSITE_CD					,
			pPART_TIME_JOB_SEQ			,
			pPART_TIME_JOB_NM			,
			pINCOME						,
			pWAITING_MIN				,
			pSTAGE_SEQ					,
			SEQ_REVISION_NO.NEXTVAL		,
			SYSDATE
		);

	END IF;
	CLOSE PKG_PART_TIME_JOB_PLAN.CS02;

	COMMIT;

EXCEPTION
	WHEN OTHERS THEN
		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);
		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

		PKG_PART_TIME_JOB_PLAN.CLOSE_ALL;

END PART_TIME_JOB_PLAN_MAINTE;
/
SHOW ERROR;
