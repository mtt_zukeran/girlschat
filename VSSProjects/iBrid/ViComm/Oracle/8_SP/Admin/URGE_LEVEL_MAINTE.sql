/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: システム管理
--  Title			: 督促レベル更新
--	Progaram ID		: URGE_LEVEL_MAINTE
--	Compile Turn	: 0
--  Creation Date	: 09.12.14
--	Update Date		:
--  Author			: i-Brid(T.Tokunaga)
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE URGE_LEVEL_MAINTE(
	pSITE_CD			IN	VARCHAR2	,
	pURGE_LEVEL			IN	NUMBER		,
	pDELAY_DAYS			IN	NUMBER		,
	pROWID				IN	VARCHAR2	,
	pREVISION_NO		IN	NUMBER		,
	pDEL_FLAG			IN	NUMBER		,
	pSTATUS				OUT	VARCHAR2	
)IS
	-- CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'URGE_LEVEL_MAINTE';

	REC_URGE_LEVEL	PKG_URGE_LEVEL.CS03%ROWTYPE;
BEGIN
	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS	:= APP_COMM.STATUS_NORMAL;

	/*------------------------------*/
	/* Open Cursor					*/
	/*------------------------------*/
	OPEN PKG_URGE_LEVEL.CS03(pROWID);

	/*------------------------------*/
	/* Update Record				*/
	/*------------------------------*/
	FETCH PKG_URGE_LEVEL.CS03 INTO REC_URGE_LEVEL;

	IF PKG_URGE_LEVEL.CS03%FOUND THEN

		IF (APP_COMM.CHECK_REVISION_NO(pREVISION_NO,REC_URGE_LEVEL.REVISION_NO) = TRUE) THEN
			IF pDEL_FLAG = 0 THEN

				UPDATE T_URGE_LEVEL SET
						DELAY_DAYS			= pDELAY_DAYS				,
						UPDATE_DATE			= SYSDATE					,
						REVISION_NO			= SEQ_REVISION_NO.NEXTVAL
					WHERE
						CURRENT OF PKG_URGE_LEVEL.CS03;
			ELSE
				DELETE T_URGE_LEVEL
					WHERE
						CURRENT OF PKG_URGE_LEVEL.CS03;
				DELETE T_URGE_SCHEDULE
					WHERE
						SITE_CD				= pSITE_CD		AND
						URGE_LEVEL			= pURGE_LEVEL
				;
			END IF;
		ELSE
			RAISE APP_COMM.UPDATE_BY_ANYONE;
		END IF;
	ELSE
		IF pDEL_FLAG = 0 THEN

			INSERT INTO T_URGE_LEVEL (	
				SITE_CD					,
				URGE_LEVEL				,
				DELAY_DAYS				,
				UPDATE_DATE				,
				REVISION_NO
			)VALUES(
				pSITE_CD				,
				pURGE_LEVEL				,
				pDELAY_DAYS				,
				SYSDATE					,
				SEQ_REVISION_NO.NEXTVAL
			);
		END IF;

	END IF;

	CLOSE PKG_URGE_LEVEL.CS03;

	COMMIT;

EXCEPTION
	WHEN OTHERS THEN

		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);
		PKG_URGE_LEVEL.CLOSE_ALL;
		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END URGE_LEVEL_MAINTE;
/
SHOW ERROR;