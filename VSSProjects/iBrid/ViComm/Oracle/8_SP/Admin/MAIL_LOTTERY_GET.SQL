/*************************************************************************/
--	System			: ViComm
--	Sub System Name	: システム管理
--	Title			: メールdeガチャ設定取得
--	Progaram ID		: MAIL_LOTTERY_GET
--	Compile Turn	: 0
--	Creation Date	: 14.12.29
--	Update Date		:
--	Author			: M%TT Y.Ikemiya
/*************************************************************************/
-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
/*-----------------------------------------------------------------------*/

CREATE OR REPLACE PROCEDURE MAIL_LOTTERY_GET(
	pSITE_CD						IN	VARCHAR2	,
	pMAIL_LOTTERY_SEQ				IN	VARCHAR2	,
	pSTART_DATE						OUT	DATE		,
	pEND_DATE						OUT	DATE		,
	pSEX_CD							OUT	VARCHAR2	,
	pNEED_SECOND_LOTTERY_COUNT		OUT	VARCHAR2	,
	pREMARKS						OUT	VARCHAR2	,
	pREVISION_NO					OUT	NUMBER		,
	pSTATUS							OUT	VARCHAR2
)IS
--	CONST
	PGM_NM							CONSTANT VARCHAR(255)	:= 'MAIL_LOTTERY_GET';
--	CURSOR
	CURSOR CS_MAIL_LOTTERY IS
		SELECT
			*
		FROM
			T_MAIL_LOTTERY
		WHERE
			SITE_CD				= pSITE_CD				AND
			MAIL_LOTTERY_SEQ	= pMAIL_LOTTERY_SEQ
	;

	REC_MAIL_LOTTERY			CS_MAIL_LOTTERY%ROWTYPE;
BEGIN
	pSTATUS							:= APP_COMM.STATUS_NORMAL;
	
	pSTART_DATE					:= NULL;
	pEND_DATE					:= NULL;
	pSEX_CD						:= NULL;
	pNEED_SECOND_LOTTERY_COUNT	:= NULL;
	pREMARKS					:= NULL;
	pREVISION_NO				:= 0;

	OPEN CS_MAIL_LOTTERY;
	FETCH CS_MAIL_LOTTERY INTO REC_MAIL_LOTTERY;
	IF CS_MAIL_LOTTERY%FOUND THEN
		pSTART_DATE					:= REC_MAIL_LOTTERY.START_DATE;
		pEND_DATE					:= REC_MAIL_LOTTERY.END_DATE;
		pSEX_CD						:= REC_MAIL_LOTTERY.SEX_CD;
		pNEED_SECOND_LOTTERY_COUNT	:= REC_MAIL_LOTTERY.NEED_SECOND_LOTTERY_COUNT;
		pREMARKS					:= REC_MAIL_LOTTERY.REMARKS;
		pREVISION_NO				:= REC_MAIL_LOTTERY.REVISION_NO;
	END IF;
	CLOSE CS_MAIL_LOTTERY;
	
	COMMIT;

EXCEPTION
	WHEN OTHERS THEN
		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);
		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

		IF CS_MAIL_LOTTERY%ISOPEN THEN
			CLOSE CS_MAIL_LOTTERY;
		END IF;

END MAIL_LOTTERY_GET;
/
SHOW ERROR;
