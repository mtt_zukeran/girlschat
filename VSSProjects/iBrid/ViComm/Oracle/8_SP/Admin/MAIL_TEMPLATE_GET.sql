/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: システム管理
--  Title			: メールテンプレート取得
--	Progaram ID		: MAIL_TEMPLATE_GET
--	Compile Turn	: 0
--  Creation Date	: 09.06.01
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE MAIL_TEMPLATE_GET(
	pSITE_CD					IN	VARCHAR2	,
	pMAIL_TEMPLATE_NO			IN	VARCHAR2	,
	pHTML_DOC_SUB_SEQ			IN	VARCHAR2	,
	pMAIL_TEMPLATE_TYPE			OUT	VARCHAR2	,
	pTEMPLATE_NM				OUT	VARCHAR2	,
	pMAIL_TITLE					OUT	VARCHAR2	,
	pEMAIL_ADDR					OUT	VARCHAR2	,
	pWEB_FACE_SEQ				OUT	VARCHAR2	,
	pMOBILE_MAIL_FONT_SIZE		OUT	NUMBER		,
	pMOBILE_MAIL_BACK_COLOR		OUT	VARCHAR2	,
	pCAST_CREATE_ORG_FLAG		OUT	NUMBER		,
	pMAIL_ATTACHED_OBJ_TYPE		OUT	NUMBER		,
	pMAIL_ATTACHED_METHOD		OUT	VARCHAR2	,
	pTEXT_MAIL_FLAG				OUT	NUMBER		,
	pDEL_FLAG					OUT	NUMBER		,
	pHTML_DOC_SEQ				OUT	VARCHAR2	,
	pHTML_DOC_TITLE				OUT	VARCHAR2	,
	pHTML_DOC					OUT	APP_COMM.TBL_LONG_VARCHAR2,
	pNON_DISP_IN_MAIL_BOX		OUT	NUMBER		,
	pNON_DISP_IN_DECOMAIL		OUT	NUMBER		,
	pSEX_CD						OUT	VARCHAR2	,
	pFORCE_TX_MOBILE_MAIL_FLAG	OUT	NUMBER		,
	pMAIL_DOC_SHORTEN_FLAG		OUT	NUMBER		,
	pROWID_VIEW					OUT	VARCHAR2	,
	pREVISION_NO_VIEW			OUT	NUMBER		,
	pROWID_MANAGE				OUT	VARCHAR2	,
	pREVISION_NO_MANAGE			OUT	NUMBER		,
	pROWID_DOC					OUT	VARCHAR2	,
	pREVISION_NO_DOC			OUT	NUMBER		,
	pRECORD_COUNT				OUT	NUMBER		,
	pSTATUS						OUT	VARCHAR2
)IS
	--	CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'MAIL_TEMPLATE_GET';

	REC_TEMPLATE		PKG_MAIL_TEMPLATE.CS02%ROWTYPE;

	BUF_END_PUB_DAY				T_SITE_HTML_MANAGE.END_PUB_DAY			%TYPE;
	BUF_USER_AGENT_TYPE			T_SITE_HTML_MANAGE.USER_AGENT_TYPE		%TYPE;
	BUF_CSS_FILE_NM1			T_SITE_HTML_MANAGE.CSS_FILE_NM1			%TYPE;
	BUF_CSS_FILE_NM2			T_SITE_HTML_MANAGE.CSS_FILE_NM2			%TYPE;
	BUF_CSS_FILE_NM3			T_SITE_HTML_MANAGE.CSS_FILE_NM3			%TYPE;
	BUF_CSS_FILE_NM4			T_SITE_HTML_MANAGE.CSS_FILE_NM4			%TYPE;
	BUF_CSS_FILE_NM5			T_SITE_HTML_MANAGE.CSS_FILE_NM5			%TYPE;
	BUF_JAVASCRIPT_FILE_NM1		T_SITE_HTML_MANAGE.JAVASCRIPT_FILE_NM1	%TYPE;
	BUF_JAVASCRIPT_FILE_NM2		T_SITE_HTML_MANAGE.JAVASCRIPT_FILE_NM2	%TYPE;
	BUF_JAVASCRIPT_FILE_NM3		T_SITE_HTML_MANAGE.JAVASCRIPT_FILE_NM3	%TYPE;
	BUF_JAVASCRIPT_FILE_NM4		T_SITE_HTML_MANAGE.JAVASCRIPT_FILE_NM4	%TYPE;
	BUF_JAVASCRIPT_FILE_NM5		T_SITE_HTML_MANAGE.JAVASCRIPT_FILE_NM5	%TYPE;
	BUF_CANONICAL				T_SITE_HTML_MANAGE.CANONICAL			%TYPE;
	BUF_PROTECT_IMAGE_FLAG		T_SITE_HTML_MANAGE.PROTECT_IMAGE_FLAG	%TYPE;
	BUF_EMOJI_TOOL_FLAG			T_SITE_HTML_MANAGE.EMOJI_TOOL_FLAG		%TYPE;
	BUF_NOINDEX_FLAG			T_SITE_HTML_MANAGE.NOINDEX_FLAG			%TYPE;
	BUF_NOFOLLOW_FLAG			T_SITE_HTML_MANAGE.NOFOLLOW_FLAG		%TYPE;
	BUF_NOARCHIVE_FLAG			T_SITE_HTML_MANAGE.NOARCHIVE_FLAG		%TYPE;
	BUF_NA_FLAG					T_SITE_HTML_MANAGE.NA_FLAG				%TYPE;
	BUF_PAGE_TITLE				T_SITE_HTML_MANAGE.PAGE_TITLE			%TYPE;
	BUF_PAGE_KEYWORD			T_SITE_HTML_MANAGE.PAGE_KEYWORD			%TYPE;
	BUF_PAGE_DESCRIPTION		T_SITE_HTML_MANAGE.PAGE_DESCRIPTION		%TYPE;
	BUF_TABLE_INDEX				T_SITE_HTML_MANAGE.TABLE_INDEX			%TYPE;
	BUF_WEB_FACE_SEQ			T_SITE_HTML_MANAGE.WEB_FACE_SEQ			%TYPE;

	BUF_RECORD_COUNT	NUMBER(2);

BEGIN
	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS					:= APP_COMM.STATUS_NORMAL;
	pRECORD_COUNT			:= 0;
	pCAST_CREATE_ORG_FLAG	:= 0;
	pHTML_DOC_SEQ			:= NULL;

	/*------------------------------*/
	/* Get Record					*/
	/*------------------------------*/
	OPEN PKG_MAIL_TEMPLATE.CS02(pSITE_CD,pMAIL_TEMPLATE_NO);
	FETCH PKG_MAIL_TEMPLATE.CS02 INTO REC_TEMPLATE;

	IF PKG_MAIL_TEMPLATE.CS02%FOUND THEN
		pMAIL_TEMPLATE_TYPE			:= REC_TEMPLATE.MAIL_TEMPLATE_TYPE;
		pTEMPLATE_NM				:= REC_TEMPLATE.TEMPLATE_NM;
		pMAIL_TITLE					:= REC_TEMPLATE.MAIL_TITLE;
		pEMAIL_ADDR					:= REC_TEMPLATE.EMAIL_ADDR;
		pWEB_FACE_SEQ				:= REC_TEMPLATE.WEB_FACE_SEQ;
		pMOBILE_MAIL_FONT_SIZE		:= REC_TEMPLATE.MOBILE_MAIL_FONT_SIZE;
		pMOBILE_MAIL_BACK_COLOR		:= REC_TEMPLATE.MOBILE_MAIL_BACK_COLOR;
		pHTML_DOC_SEQ				:= REC_TEMPLATE.HTML_DOC_SEQ;
		pMAIL_ATTACHED_OBJ_TYPE		:= REC_TEMPLATE.MAIL_ATTACHED_OBJ_TYPE;
		pMAIL_ATTACHED_METHOD		:= REC_TEMPLATE.MAIL_ATTACHED_METHOD;
		pTEXT_MAIL_FLAG				:= REC_TEMPLATE.TEXT_MAIL_FLAG;
		pFORCE_TX_MOBILE_MAIL_FLAG	:= REC_TEMPLATE.FORCE_TX_MOBILE_MAIL_FLAG;
		pMAIL_DOC_SHORTEN_FLAG		:= REC_TEMPLATE.MAIL_DOC_SHORTEN_FLAG;
		pDEL_FLAG					:= REC_TEMPLATE.DEL_FLAG;
		pROWID_VIEW					:= REC_TEMPLATE.ROWID		;
		pREVISION_NO_VIEW			:= REC_TEMPLATE.REVISION_NO	;
		pRECORD_COUNT				:= 1;

		IF (pHTML_DOC_SUB_SEQ = REC_TEMPLATE.CAST_CREATE_ORG_SUB_SEQ) THEN
			pCAST_CREATE_ORG_FLAG := 1;
		END IF;

		SITE_HTML_DOC_GET(
			REC_TEMPLATE.HTML_DOC_SEQ,
			pHTML_DOC_SUB_SEQ		,
			BUF_PAGE_TITLE			,
			BUF_PAGE_KEYWORD		,
			BUF_PAGE_DESCRIPTION	,
			BUF_TABLE_INDEX			,
			BUF_WEB_FACE_SEQ		,
			pSEX_CD					,
			pHTML_DOC_TITLE			,
			pHTML_DOC				,
			BUF_END_PUB_DAY			,
			pNON_DISP_IN_MAIL_BOX	,
			pNON_DISP_IN_DECOMAIL	,
			BUF_USER_AGENT_TYPE		,
			BUF_CSS_FILE_NM1		,
			BUF_CSS_FILE_NM2		,
			BUF_CSS_FILE_NM3		,
			BUF_CSS_FILE_NM4		,
			BUF_CSS_FILE_NM5		,
			BUF_JAVASCRIPT_FILE_NM1	,
			BUF_JAVASCRIPT_FILE_NM2	,
			BUF_JAVASCRIPT_FILE_NM3	,
			BUF_JAVASCRIPT_FILE_NM4	,
			BUF_JAVASCRIPT_FILE_NM5	,
			BUF_CANONICAL			,
			BUF_PROTECT_IMAGE_FLAG	,
			BUF_EMOJI_TOOL_FLAG		,
			BUF_NOINDEX_FLAG		,
			BUF_NOFOLLOW_FLAG		,
			BUF_NOARCHIVE_FLAG		,
			BUF_NA_FLAG				,
			pROWID_MANAGE			,
			pREVISION_NO_MANAGE		,
			pROWID_DOC				,
			pREVISION_NO_DOC		,
			BUF_RECORD_COUNT		,
			pSTATUS
		);
	END IF;

	IF (pSTATUS != APP_COMM.STATUS_NORMAL) THEN
		RAISE PROGRAM_ERROR;
	END IF;

	CLOSE PKG_MAIL_TEMPLATE.CS02;

	COMMIT;

EXCEPTION
	WHEN OTHERS THEN

		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);

		PKG_MAIL_TEMPLATE.CLOSE_ALL;

		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END MAIL_TEMPLATE_GET;
/
SHOW ERROR;
