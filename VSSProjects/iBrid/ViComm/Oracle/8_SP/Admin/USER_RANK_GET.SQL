/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: システム管理
--  Title			: ユーザーランク取得
--	Progaram ID		: USER_RANK_GET
--	Compile Turn	: 0
--  Creation Date	: 09.06.01
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE USER_RANK_GET(
	pSITE_CD					IN	VARCHAR2	,
	pUSER_RANK					IN	VARCHAR2	,
	pUSER_RANK_NM				OUT	VARCHAR2	,
	pTOTAL_RECEIPT_AMT			OUT	NUMBER		,
	pLIMIT_POINT				OUT	NUMBER		,
	pWEB_FACE_SEQ				OUT	VARCHAR2	,
	pINTRO_POINT_RATE			OUT	NUMBER		,
	pUSER_RANK_KEEP_AMT			OUT NUMBER		,
	pPOINT_AFFILIATE_RANK_FLAG	OUT	NUMBER		,
	pBINGO_BALL_RATE			OUT	NUMBER		,
	pROWID						OUT	VARCHAR2	,
	pREVISION_NO				OUT	NUMBER		,
	pRECORD_COUNT				OUT	NUMBER		,
	pSTATUS						OUT	VARCHAR2
)IS
	-- CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'USER_RANK_GET';

	REC_USER_RANK		PKG_USER_RANK.CS02%ROWTYPE;
BEGIN
	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS			:= APP_COMM.STATUS_NORMAL;
	pRECORD_COUNT 	:= 0;

	pUSER_RANK_NM				:= NULL;
	pTOTAL_RECEIPT_AMT			:= NULL;
	pWEB_FACE_SEQ				:= NULL;
	pINTRO_POINT_RATE			:= NULL;
	pUSER_RANK_KEEP_AMT			:= NULL;
	pPOINT_AFFILIATE_RANK_FLAG	:= 0;
	pBINGO_BALL_RATE			:= 0;
	pROWID						:= NULL;
	pREVISION_NO				:= 0;
	pRECORD_COUNT				:= 0;

	/*------------------------------*/
	/* Get Record					*/
	/*------------------------------*/
	OPEN PKG_USER_RANK.CS02(pSITE_CD,pUSER_RANK);
	FETCH PKG_USER_RANK.CS02 INTO REC_USER_RANK;

	IF PKG_USER_RANK.CS02%FOUND THEN
		pUSER_RANK_NM				:= REC_USER_RANK.USER_RANK_NM		;
		pTOTAL_RECEIPT_AMT			:= REC_USER_RANK.TOTAL_RECEIPT_AMT	;
		pLIMIT_POINT				:= REC_USER_RANK.LIMIT_POINT		;
		pWEB_FACE_SEQ				:= REC_USER_RANK.WEB_FACE_SEQ		;
		pINTRO_POINT_RATE			:= REC_USER_RANK.INTRO_POINT_RATE	;
		pUSER_RANK_KEEP_AMT			:= REC_USER_RANK.USER_RANK_KEEP_AMT	;
		pPOINT_AFFILIATE_RANK_FLAG	:= REC_USER_RANK.POINT_AFFILIATE_RANK_FLAG;
		pBINGO_BALL_RATE			:= REC_USER_RANK.BINGO_BALL_RATE	;
		pROWID						:= REC_USER_RANK.ROWID				;
		pREVISION_NO				:= REC_USER_RANK.REVISION_NO		;
		pRECORD_COUNT				:= 1;
	END IF;

	CLOSE PKG_USER_RANK.CS02;

	COMMIT;

EXCEPTION
	WHEN OTHERS THEN

		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);

		PKG_USER_RANK.CLOSE_ALL;

		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END USER_RANK_GET;
/
SHOW ERROR;
