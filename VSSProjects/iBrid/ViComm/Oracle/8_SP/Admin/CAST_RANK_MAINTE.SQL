/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: システム管理
--  Title			: キャストランク更新
--	Progaram ID		: CAST_RANK_MAINTE
--	Compile Turn	: 0
--  Creation Date	: 10.08.05
--	Update Date		:
--  Author			: i-Brid(R.Suzuki)
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE CAST_RANK_MAINTE(
	pUSER_RANK			IN	VARCHAR2	,
	pUSER_RANK_NM		IN	VARCHAR2	,
	pINTRO_POINT_RATE	IN	NUMBER		,
	pPOINT_PRICE		IN	NUMBER		,
	pBINGO_BALL_RATE	IN	NUMBER		,
	pROWID				IN	VARCHAR2	,
	pREVISION_NO		IN	NUMBER		,
	pDEL_FLAG			IN	NUMBER		,
	pSTATUS				OUT	VARCHAR2
)IS
	--	CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'CAST_RANK_MAINTE';

	REC_CAST_RANK	PKG_CAST_RANK.CS03%ROWTYPE;
BEGIN
	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS	:= APP_COMM.STATUS_NORMAL;

	/*------------------------------*/
	/* Open Cursor					*/
	/*------------------------------*/
	OPEN PKG_CAST_RANK.CS03(pROWID);

	/*------------------------------*/
	/* Update Record				*/
	/*------------------------------*/
	FETCH PKG_CAST_RANK.CS03 INTO REC_CAST_RANK;

	IF PKG_CAST_RANK.CS03%FOUND THEN

		IF (APP_COMM.CHECK_REVISION_NO(pREVISION_NO,REC_CAST_RANK.REVISION_NO) = TRUE) THEN
			IF pDEL_FLAG = 0 THEN

				UPDATE T_CAST_RANK SET
						USER_RANK_NM		= pUSER_RANK_NM			,
						INTRO_POINT_RATE	= pINTRO_POINT_RATE		,
						POINT_PRICE			= pPOINT_PRICE			,
						BINGO_BALL_RATE		= pBINGO_BALL_RATE		,
						UPDATE_DATE			= SYSDATE				,
						REVISION_NO			= SEQ_REVISION_NO.NEXTVAL
					WHERE
						CURRENT OF PKG_CAST_RANK.CS03;
			ELSE
				DELETE T_CAST_RANK
					WHERE
						CURRENT OF PKG_CAST_RANK.CS03;
			END IF;
		ELSE
			RAISE APP_COMM.UPDATE_BY_ANYONE;
		END IF;
	ELSE
		IF pDEL_FLAG = 0 THEN

			INSERT INTO T_CAST_RANK (
				USER_RANK			,
				USER_RANK_NM		,
				INTRO_POINT_RATE	,
				POINT_PRICE			,
				BINGO_BALL_RATE		,
				UPDATE_DATE			,
				REVISION_NO
			)VALUES(
				pUSER_RANK				,
				pUSER_RANK_NM			,
				pINTRO_POINT_RATE		,
				pPOINT_PRICE			,
				pBINGO_BALL_RATE		,
				SYSDATE					,
				SEQ_REVISION_NO.NEXTVAL
			);
		END IF;

	END IF;

	CLOSE PKG_CAST_RANK.CS03;

	COMMIT;

EXCEPTION
	WHEN OTHERS THEN

		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);

		PKG_CAST_RANK.CLOSE_ALL;

		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END CAST_RANK_MAINTE;
/
SHOW ERROR;
