/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: システム管理
--  Title			: 男性お宝属性取得
--	Progaram ID		: MAN_TREASURE_ATTR_GET
--	Compile Turn	: 0
--  Creation Date	: 09.06.01
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE MAN_TREASURE_ATTR_GET(
	pCAST_GAME_PIC_ATTR_SEQ	IN		VARCHAR2	,
	pSITE_CD				OUT		VARCHAR2	,
	pCAST_GAME_PIC_ATTR_NM	OUT		VARCHAR2	,
	pPRIORITY				OUT		NUMBER		,
	pROWID					OUT		VARCHAR2	,
	pREVISION_NO			OUT		NUMBER		,
	pRECORD_COUNT			OUT		NUMBER		,
	pSTATUS					OUT		VARCHAR2
)IS
	--	CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'MAN_TREASURE_ATTR_GET';

	REC_ATTR_TYPE		PKG_MAN_TREASURE_ATTR.CS02%ROWTYPE;
BEGIN

	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS			:= APP_COMM.STATUS_NORMAL;
	pRECORD_COUNT 	:= 0;

	pCAST_GAME_PIC_ATTR_NM	:= NULL;
	pPRIORITY				:= 0;
	pROWID					:= NULL;
	pREVISION_NO			:= 0;
	pRECORD_COUNT			:= 0;

	/*------------------------------*/
	/* Get Record					*/
	/*------------------------------*/
	OPEN PKG_MAN_TREASURE_ATTR.CS02(pCAST_GAME_PIC_ATTR_SEQ);
	FETCH PKG_MAN_TREASURE_ATTR.CS02 INTO REC_ATTR_TYPE;

	IF PKG_MAN_TREASURE_ATTR.CS02%FOUND THEN
		pSITE_CD				:= REC_ATTR_TYPE.SITE_CD				;
		pCAST_GAME_PIC_ATTR_NM	:= REC_ATTR_TYPE.CAST_GAME_PIC_ATTR_NM	;
		pPRIORITY				:= REC_ATTR_TYPE.PRIORITY				;
		pROWID					:= REC_ATTR_TYPE.ROWID					;
		pREVISION_NO			:= REC_ATTR_TYPE.REVISION_NO			;
		pRECORD_COUNT			:= 1;
	END IF;

	CLOSE PKG_MAN_TREASURE_ATTR.CS02;

	COMMIT;
EXCEPTION
	WHEN OTHERS THEN

		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);

		PKG_MAN_TREASURE_ATTR.CLOSE_ALL;

		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END MAN_TREASURE_ATTR_GET;
/
SHOW ERROR;
