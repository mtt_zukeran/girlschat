/*************************************************************************/
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: �ްѷ�׸���擾
--	Progaram ID		: GAME_CHARACTER_GET
--	Compile Turn	: 0
--	Creation Date	: 11.08.15
--	Update Date		:
--	Author			: i-Brid
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE GAME_CHARACTER_GET(
	pSITE_CD					IN	VARCHAR2	,
	pUSER_SEQ					IN	VARCHAR2	,
	pUSER_CHAR_NO				IN	VARCHAR2	,
	pGAME_HANDLE_NM				OUT	VARCHAR2	,
	pUNASSIGNED_FORCE_COUNT		OUT	NUMBER		,
	pCOOPERATION_POINT			OUT	NUMBER		,
	pREVISION_NO				OUT	VARCHAR2	,
	pROW_COUNT					OUT	NUMBER		,
	pSTATUS						OUT	VARCHAR2
)IS
	--	CONSTANT
	PGM_NM	CONSTANT VARCHAR(255)	:= 'GAME_CHARACTER_GET';
	
	--REC
	REC_GAME_CHARACTER	PKG_GAME_CHARACTER.CS02%ROWTYPE;
BEGIN
	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS						:= APP_COMM.STATUS_NORMAL;

	pGAME_HANDLE_NM				:= NULL;
	pUNASSIGNED_FORCE_COUNT		:= 0;
	pCOOPERATION_POINT			:= 0;
	pREVISION_NO				:= 0;
	pROW_COUNT					:= 0;

	/*------------------------------*/
	/* Get Record					*/
	/*------------------------------*/
	OPEN PKG_GAME_CHARACTER.CS02(pSITE_CD,pUSER_SEQ,pUSER_CHAR_NO);
	FETCH PKG_GAME_CHARACTER.CS02 INTO REC_GAME_CHARACTER;

	IF PKG_GAME_CHARACTER.CS02%FOUND THEN
		pGAME_HANDLE_NM				:= REC_GAME_CHARACTER.GAME_HANDLE_NM			;
		pUNASSIGNED_FORCE_COUNT		:= REC_GAME_CHARACTER.UNASSIGNED_FORCE_COUNT	;
		pCOOPERATION_POINT			:= REC_GAME_CHARACTER.COOPERATION_POINT			;
		pREVISION_NO				:= REC_GAME_CHARACTER.REVISION_NO				;
		pROW_COUNT					:= 1											;
	END IF;

	CLOSE PKG_GAME_CHARACTER.CS02;

	COMMIT;

EXCEPTION
	WHEN OTHERS THEN

		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);

		PKG_GAME_CHARACTER.CLOSE_ALL;

		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END GAME_CHARACTER_GET;
/
SHOW ERROR;
