/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: システム管理
--  Title			: この娘を探せ 月間情報取得
--	Progaram ID		: WANTED_MONTHLY_DATA_GET
--	Compile Turn	: 0
--  Creation Date	: 13.06.13
--	Update Date		:
--  Author			: M&TT Y.Ikemiya
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE WANTED_MONTHLY_DATA_GET(
	pSITE_CD						IN	VARCHAR2	,
	pREPORT_MONTH					IN	VARCHAR2	,
	pUNIQUE_USER_COUNT				OUT	VARCHAR2	,
	pCAUGHT_COUNT					OUT	VARCHAR2	,
	pCOMPLETE_COUNT					OUT	VARCHAR2	,
	pCOMPLETE_USER_COUNT			OUT	VARCHAR2	,
	pACQUIRED_USER_COUNT			OUT	VARCHAR2	,
	pBOUNTY_POINT_PER_SHEET			OUT	VARCHAR2	,
	pSTATUS							OUT	VARCHAR2
)IS
	-- CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'WANTED_MONTHLY_DATA_GET';

	CURSOR CS_MANAGEMENT_EX IS
		SELECT
			WANTED_MONTHLY_BOUNTY_POINT
		FROM
			T_SITE_MANAGEMENT_EX
		WHERE
			T_SITE_MANAGEMENT_EX.SITE_CD	= pSITE_CD;
	
	CURSOR CS_WANTED_ENTRANT_USER_CNT IS
		SELECT
			COUNT(*) AS ENTRANT_USER_COUNT
		FROM
			(
				SELECT
					1
				FROM
					T_WANTED_ENTRANT
				WHERE
					SITE_CD			= pSITE_CD	AND
					EXECUTION_DAY	LIKE pREPORT_MONTH || '%'
				GROUP BY USER_SEQ,USER_CHAR_NO
			);
	
	CURSOR CS_WANTED_CAUGHT_COUNT IS
		SELECT
			COUNT(*) AS CAUGHT_COUNT
		FROM
			T_WANTED_ENTRANT
		WHERE
			SITE_CD			= pSITE_CD	AND
			CAUGHT_FLAG		= 1			AND
			EXECUTION_DAY	LIKE pREPORT_MONTH || '%';
	
	CURSOR CS_WANTED_COMP_SHEET_CNT IS
		SELECT
			SUM(COMPLETE_COUNT) AS TOTAL_COMP_CNT
		FROM
			T_WANTED_COMP_SHEET_CNT
		WHERE
			SITE_CD			= pSITE_CD	AND
			REPORT_MONTH	= REPLACE(pREPORT_MONTH,'/');
	
	CURSOR CS_WANTED_COMP_USER_CNT IS
		SELECT
			COUNT(*) AS COMP_USER_COUNT
		FROM
			T_WANTED_COMP_SHEET_CNT
		WHERE
			SITE_CD			= pSITE_CD	AND
			REPORT_MONTH	= REPLACE(pREPORT_MONTH,'/');
	
	CURSOR CS_WANTED_POINT_ACQUIRED_USER IS
		SELECT
			COUNT(*) AS POINT_ACQUIRED_USER_CNT
		FROM
			T_WANTED_COMP_SHEET_CNT
		WHERE
			SITE_CD					= pSITE_CD	AND
			REPORT_MONTH			= REPLACE(pREPORT_MONTH,'/') AND
			POINT_ACQUIRED_FLAG		= 1;

	REC_MANAGEMENT_EX				CS_MANAGEMENT_EX%ROWTYPE;
	REC_WANTED_ENTRANT_USER_CNT		CS_WANTED_ENTRANT_USER_CNT%ROWTYPE;
	REC_WANTED_CAUGHT_COUNT			CS_WANTED_CAUGHT_COUNT%ROWTYPE;
	REC_WANTED_COMP_SHEET_CNT		CS_WANTED_COMP_SHEET_CNT%ROWTYPE;
	REC_WANTED_COMP_USER_CNT		CS_WANTED_COMP_USER_CNT%ROWTYPE;
	REC_WANTED_POINT_ACQUIRED_USER	CS_WANTED_POINT_ACQUIRED_USER%ROWTYPE;
BEGIN

--Initialize
	pSTATUS	:= APP_COMM.STATUS_NORMAL;
	
	pUNIQUE_USER_COUNT			:= 0;
	pCAUGHT_COUNT				:= 0;
	pCOMPLETE_COUNT				:= 0;
	pCOMPLETE_USER_COUNT		:= 0;
	pACQUIRED_USER_COUNT		:= 0;
	pBOUNTY_POINT_PER_SHEET		:= 0;
	
	OPEN CS_MANAGEMENT_EX;
	FETCH CS_MANAGEMENT_EX INTO REC_MANAGEMENT_EX;
	CLOSE CS_MANAGEMENT_EX;
	
	OPEN CS_WANTED_ENTRANT_USER_CNT;
	FETCH CS_WANTED_ENTRANT_USER_CNT INTO REC_WANTED_ENTRANT_USER_CNT;
	CLOSE CS_WANTED_ENTRANT_USER_CNT;
	
	pUNIQUE_USER_COUNT := NVL(REC_WANTED_ENTRANT_USER_CNT.ENTRANT_USER_COUNT,0);
	
	OPEN CS_WANTED_CAUGHT_COUNT;
	FETCH CS_WANTED_CAUGHT_COUNT INTO REC_WANTED_CAUGHT_COUNT;
	CLOSE CS_WANTED_CAUGHT_COUNT;
	
	pCAUGHT_COUNT := REC_WANTED_CAUGHT_COUNT.CAUGHT_COUNT;
	
	OPEN CS_WANTED_COMP_SHEET_CNT;
	FETCH CS_WANTED_COMP_SHEET_CNT INTO REC_WANTED_COMP_SHEET_CNT;
	CLOSE CS_WANTED_COMP_SHEET_CNT;
	
	pCOMPLETE_COUNT := NVL(REC_WANTED_COMP_SHEET_CNT.TOTAL_COMP_CNT,0);
	
	OPEN CS_WANTED_COMP_USER_CNT;
	FETCH CS_WANTED_COMP_USER_CNT INTO REC_WANTED_COMP_USER_CNT;
	CLOSE CS_WANTED_COMP_USER_CNT;
	
	pCOMPLETE_USER_COUNT := REC_WANTED_COMP_USER_CNT.COMP_USER_COUNT;
	
	OPEN CS_WANTED_POINT_ACQUIRED_USER;
	FETCH CS_WANTED_POINT_ACQUIRED_USER INTO REC_WANTED_POINT_ACQUIRED_USER;
	CLOSE CS_WANTED_POINT_ACQUIRED_USER;
	
	pACQUIRED_USER_COUNT := REC_WANTED_POINT_ACQUIRED_USER.POINT_ACQUIRED_USER_CNT;
	
	IF (pCOMPLETE_COUNT != 0) THEN
		pBOUNTY_POINT_PER_SHEET := TRUNC(REC_MANAGEMENT_EX.WANTED_MONTHLY_BOUNTY_POINT / pCOMPLETE_COUNT);
	ELSE
		pBOUNTY_POINT_PER_SHEET := 0;
	END IF;
	
EXCEPTION
	WHEN OTHERS THEN

		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);

		IF CS_MANAGEMENT_EX%ISOPEN THEN
			CLOSE CS_MANAGEMENT_EX;
		END IF;

		IF CS_WANTED_ENTRANT_USER_CNT%ISOPEN THEN
			CLOSE CS_WANTED_ENTRANT_USER_CNT;
		END IF;

		IF CS_WANTED_CAUGHT_COUNT%ISOPEN THEN
			CLOSE CS_WANTED_CAUGHT_COUNT;
		END IF;

		IF CS_WANTED_COMP_SHEET_CNT%ISOPEN THEN
			CLOSE CS_WANTED_COMP_SHEET_CNT;
		END IF;

		IF CS_WANTED_COMP_USER_CNT%ISOPEN THEN
			CLOSE CS_WANTED_COMP_USER_CNT;
		END IF;

		IF CS_WANTED_POINT_ACQUIRED_USER%ISOPEN THEN
			CLOSE CS_WANTED_POINT_ACQUIRED_USER;
		END IF;
		
		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END WANTED_MONTHLY_DATA_GET;
/
SHOW ERROR;
