/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: システム管理
--  Title			: 商品ジャンルカテゴリ更新
--	Progaram ID		: PRODUCT_GENRE_CATEGORY_MAINTE
--	Compile Turn	: 0
--  Creation Date	: 10.12.16
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE PRODUCT_GENRE_CATEGORY_MAINTE(
	pSITE_CD					IN	VARCHAR2	,
	pPRODUCT_GENRE_CATEGORY_CD	IN	VARCHAR2	,
	pPRODUCT_GENRE_CATEGORY_NM	IN	VARCHAR2	,
	pPRODUCT_TYPE				IN	VARCHAR2	,
	pROWID						IN	VARCHAR2	,
	pREVISION_NO				IN	NUMBER		,
	pDEL_FLAG					IN	NUMBER		,
	pSTATUS						OUT	VARCHAR2
)IS
	--	CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'PRODUCT_GENRE_CATEGORY_MAINTE';

	REC_PRODUCT_GENRE_CATEGORY	PKG_PRODUCT_GENRE_CATEGORY.CS03%ROWTYPE;
BEGIN
	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS	:= APP_COMM.STATUS_NORMAL;

	/*------------------------------*/
	/* Open Cursor					*/
	/*------------------------------*/
	OPEN PKG_PRODUCT_GENRE_CATEGORY.CS03(pROWID);

	/*------------------------------*/
	/* Update Record				*/
	/*------------------------------*/
	FETCH PKG_PRODUCT_GENRE_CATEGORY.CS03 INTO REC_PRODUCT_GENRE_CATEGORY;

	IF PKG_PRODUCT_GENRE_CATEGORY.CS03%FOUND THEN

		IF (APP_COMM.CHECK_REVISION_NO(pREVISION_NO,REC_PRODUCT_GENRE_CATEGORY.REVISION_NO) = TRUE) THEN
			IF pDEL_FLAG = 0 THEN

				UPDATE T_PRODUCT_GENRE_CATEGORY SET
						PRODUCT_GENRE_CATEGORY_NM	=	pPRODUCT_GENRE_CATEGORY_NM	,
						UPDATE_DATE					=	SYSDATE						,
						REVISION_NO					=	SEQ_REVISION_NO.NEXTVAL
					WHERE
						CURRENT OF PKG_PRODUCT_GENRE_CATEGORY.CS03;
			ELSE
				DELETE T_PRODUCT_GENRE_CATEGORY
					WHERE
						CURRENT OF PKG_PRODUCT_GENRE_CATEGORY.CS03;
			END IF;
		ELSE
			RAISE APP_COMM.UPDATE_BY_ANYONE;
		END IF;
	ELSE
		IF pDEL_FLAG = 0 THEN

			INSERT INTO T_PRODUCT_GENRE_CATEGORY(
				SITE_CD							,
				PRODUCT_GENRE_CATEGORY_CD		,
				PRODUCT_GENRE_CATEGORY_NM		,
				PRODUCT_TYPE					,
				UPDATE_DATE						,
				REVISION_NO
			)VALUES(
				pSITE_CD						,
				pPRODUCT_GENRE_CATEGORY_CD		,
				pPRODUCT_GENRE_CATEGORY_NM		,
				pPRODUCT_TYPE					,
				SYSDATE							,
				SEQ_REVISION_NO.NEXTVAL
			);
		END IF;

	END IF;

	CLOSE PKG_PRODUCT_GENRE_CATEGORY.CS03;

	COMMIT;

EXCEPTION
	WHEN OTHERS THEN

		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);

		PKG_PRODUCT_GENRE_CATEGORY.CLOSE_ALL;

		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END PRODUCT_GENRE_CATEGORY_MAINTE;
/
SHOW ERROR;
