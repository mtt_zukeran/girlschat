/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: システム管理
--  Title			: サイト別広告グループ取得
--	Progaram ID		: SITE_AD_GROUP_GET
--	Compile Turn	: 0
--  Creation Date	: 09.06.01
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE SITE_AD_GROUP_GET(
	pSITE_CD			IN	VARCHAR2	,
	pAD_CD				IN	VARCHAR2	,
	pAD_GROUP_CD		OUT	VARCHAR2	,
	pAD_NM				OUT	VARCHAR2	,
	pROWID				OUT	VARCHAR2	,
	pREVISION_NO		OUT	NUMBER		,
	pRECORD_COUNT		OUT	NUMBER		,
	pSTATUS				OUT	VARCHAR2
)IS
	-- CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'SITE_AD_GROUP_GET';

	CURSOR CS_AD IS SELECT AD_NM FROM T_AD
		WHERE
			AD_CD = pAD_CD;

	REC_SITE_AD_GROUP	PKG_SITE_AD_GROUP.CS02%ROWTYPE;
BEGIN
	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS			:= APP_COMM.STATUS_NORMAL;
	pRECORD_COUNT 	:= 0;
	pAD_GROUP_CD	:= NULL;
	pROWID			:= NULL;
	pREVISION_NO	:= 0;
	pRECORD_COUNT	:= 0;

	/*------------------------------*/
	/* Get Record					*/
	/*------------------------------*/
	OPEN PKG_SITE_AD_GROUP.CS02(pSITE_CD,pAD_CD);
	FETCH PKG_SITE_AD_GROUP.CS02 INTO REC_SITE_AD_GROUP;

	IF PKG_SITE_AD_GROUP.CS02%FOUND THEN
		pAD_GROUP_CD	:= REC_SITE_AD_GROUP.AD_GROUP_CD	;
		pROWID			:= REC_SITE_AD_GROUP.ROWID			;
		pREVISION_NO	:= REC_SITE_AD_GROUP.REVISION_NO	;
		pRECORD_COUNT	:= 1;
	END IF;

	CLOSE PKG_SITE_AD_GROUP.CS02;

	OPEN CS_AD;
	FETCH CS_AD INTO pAD_NM;
	CLOSE CS_AD;

	COMMIT;

EXCEPTION
	WHEN OTHERS THEN

		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);

		PKG_SITE_AD_GROUP.CLOSE_ALL;

		IF CS_AD%ISOPEN THEN
			CLOSE CS_AD;
		END IF;

		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END SITE_AD_GROUP_GET;
/
SHOW ERROR;
