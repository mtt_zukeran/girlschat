/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: システム管理
--  Title			: パック設定取得
--	Progaram ID		: PACK_GET
--	Compile Turn	: 0
--  Creation Date	: 09.12.14
--	Update Date		:
--  Author			: i-Brid(T.Tokunaga)
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  2010/12/06  ttakahashi FIRST_TIME_FLAGを追加
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE PACK_GET(
	pSITE_CD			IN	VARCHAR2	,
	pSETTLE_TYPE		IN	VARCHAR2	,
	pSALES_AMT			IN	NUMBER		,
	pREMARKS			OUT	VARCHAR2	,
	pCOMMODITIES_CD		OUT	VARCHAR2	,
	pROWID				OUT	VARCHAR2	,
	pREVISION_NO		OUT	NUMBER		,
	pFIRST_TIME_FLAG	OUT	NUMBER		,
	pRECORD_COUNT		OUT	NUMBER		,
	pSTATUS				OUT	VARCHAR2	
)IS
	--	CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'PACK_GET';

	REC_PACK	PKG_PACK.CS02%ROWTYPE;
BEGIN
	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS	:= APP_COMM.STATUS_NORMAL;

	pREMARKS			:= NULL;
	pCOMMODITIES_CD		:= NULL;
	pREVISION_NO		:= 0;
	pRECORD_COUNT		:= 0;

	/*------------------------------*/
	/* Get Record					*/
	/*------------------------------*/
	OPEN PKG_PACK.CS02(pSITE_CD,pSETTLE_TYPE,pSALES_AMT);
	FETCH PKG_PACK.CS02 INTO REC_PACK;

	IF PKG_PACK.CS02%FOUND THEN
		pREMARKS			:= REC_PACK.REMARKS			;
		pCOMMODITIES_CD		:= REC_PACK.COMMODITIES_CD	;
		pROWID				:= REC_PACK.ROWID			;
		pREVISION_NO		:= REC_PACK.REVISION_NO		;
		pFIRST_TIME_FLAG	:= REC_PACK.FIRST_TIME_FLAG	;
		pRECORD_COUNT		:= 1;
	END IF;

	CLOSE PKG_PACK.CS02;
	COMMIT;

EXCEPTION
	WHEN OTHERS THEN

		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);
		PKG_PACK.CLOSE_ALL;
		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END PACK_GET;
/
SHOW ERROR;
