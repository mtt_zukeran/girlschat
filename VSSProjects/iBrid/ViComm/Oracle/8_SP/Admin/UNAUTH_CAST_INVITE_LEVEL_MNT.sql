/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: システム管理
--  Title			: 未認証キャスト勧誘レベル更新
--	Progaram ID		: UNAUTH_CAST_INVITE_LEVEL_MNT
--	Compile Turn	: 0
--  Creation Date	: 10.05.28
--	Update Date		:
--  Author			: i-Brid(M.Yanagawa)
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE UNAUTH_CAST_INVITE_LEVEL_MNT(
	pSITE_CD			IN	VARCHAR2	,
	pINVITE_LEVEL		IN	NUMBER		,
	pDELAY_DAYS			IN	NUMBER		,
	pROWID				IN	VARCHAR2	,
	pREVISION_NO		IN	NUMBER		,
	pDEL_FLAG			IN	NUMBER		,
	pSTATUS				OUT	VARCHAR2	
)IS
	-- CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'UNAUTH_CAST_INVITE_LEVEL_MNT';

	REC_UNAUTH_CAST_INVITE_LEVEL	PKG_UNAUTH_CAST_INVITE_LEVEL.CS03%ROWTYPE;
BEGIN
	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS	:= APP_COMM.STATUS_NORMAL;

	/*------------------------------*/
	/* Open Cursor					*/
	/*------------------------------*/
	OPEN PKG_UNAUTH_CAST_INVITE_LEVEL.CS03(pROWID);

	/*------------------------------*/
	/* Update Record				*/
	/*------------------------------*/
	FETCH PKG_UNAUTH_CAST_INVITE_LEVEL.CS03 INTO REC_UNAUTH_CAST_INVITE_LEVEL;

	IF PKG_UNAUTH_CAST_INVITE_LEVEL.CS03%FOUND THEN

		IF (APP_COMM.CHECK_REVISION_NO(pREVISION_NO,REC_UNAUTH_CAST_INVITE_LEVEL.REVISION_NO) = TRUE) THEN
			IF pDEL_FLAG = 0 THEN

				UPDATE T_UNAUTH_CAST_INVITE_LEVEL SET
						DELAY_DAYS			= pDELAY_DAYS				,
						UPDATE_DATE			= SYSDATE					,
						REVISION_NO			= SEQ_REVISION_NO.NEXTVAL
					WHERE
						CURRENT OF PKG_UNAUTH_CAST_INVITE_LEVEL.CS03;
			ELSE
				DELETE T_UNAUTH_CAST_INVITE_LEVEL
					WHERE
						CURRENT OF PKG_UNAUTH_CAST_INVITE_LEVEL.CS03;
			END IF;
		ELSE
			RAISE APP_COMM.UPDATE_BY_ANYONE;
		END IF;
	ELSE
		IF pDEL_FLAG = 0 THEN

			INSERT INTO T_UNAUTH_CAST_INVITE_LEVEL (	
				SITE_CD					,
				INVITE_LEVEL			,
				DELAY_DAYS				,
				UPDATE_DATE				,
				REVISION_NO
			)VALUES(
				pSITE_CD				,
				pINVITE_LEVEL			,
				pDELAY_DAYS				,
				SYSDATE					,
				SEQ_REVISION_NO.NEXTVAL
			);
		END IF;

	END IF;

	CLOSE PKG_UNAUTH_CAST_INVITE_LEVEL.CS03;

	COMMIT;

EXCEPTION
	WHEN OTHERS THEN

		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);
		PKG_UNAUTH_CAST_INVITE_LEVEL.CLOSE_ALL;
		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END UNAUTH_CAST_INVITE_LEVEL_MNT;
/
SHOW ERROR;
