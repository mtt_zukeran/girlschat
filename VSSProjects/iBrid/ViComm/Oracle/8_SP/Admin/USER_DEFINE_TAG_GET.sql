/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: システム管理
--  Title			: ユーザー定義タグ取得
--	Progaram ID		: USER_DEFINE_TAG_GET
--	Compile Turn	: 0
--  Creation Date	: 09.06.01
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE USER_DEFINE_TAG_GET(
	pSITE_CD			IN	VARCHAR2	,
	pVARIABLE_ID		IN	VARCHAR2	,
	pUSER_AGENT_TYPE	IN	VARCHAR2	,
	pVARIABLE_NM		OUT	VARCHAR2	,
	pCATEGORY_SEQ		OUT	NUMBER		,
	pHTML_DOC			OUT	APP_COMM.TBL_LONG_VARCHAR2,
	pROWID				OUT	VARCHAR2	,
	pREVISION_NO		OUT	NUMBER		,
	pRECORD_COUNT		OUT	NUMBER		,
	pSTATUS				OUT	VARCHAR2
)IS
	-- CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'USER_DEFINE_TAG_GET';

	REC_TAG		PKG_USER_DEFINE_TAG.CS02%ROWTYPE;
BEGIN
	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS			:= APP_COMM.STATUS_NORMAL;
	pRECORD_COUNT 	:= 0;

	pVARIABLE_NM	:= NULL;
	pCATEGORY_SEQ   := NULL;
	pRECORD_COUNT	:= 0;
	pROWID			:= NULL;
	pREVISION_NO	:= 0;

	FOR i IN 1..20 LOOP
		pHTML_DOC(i) :=  '';
	END LOOP;

	/*------------------------------*/
	/* Get Record					*/
	/*------------------------------*/
	OPEN PKG_USER_DEFINE_TAG.CS02(pSITE_CD,pVARIABLE_ID,pUSER_AGENT_TYPE);
	FETCH PKG_USER_DEFINE_TAG.CS02 INTO REC_TAG;
	IF PKG_USER_DEFINE_TAG.CS02%FOUND THEN
		pVARIABLE_NM	:= REC_TAG.VARIABLE_NM	;
		pCATEGORY_SEQ	:= REC_TAG.CATEGORY_SEQ	;
		pHTML_DOC(1)	:= REC_TAG.HTML_DOC1	;
		pHTML_DOC(2)	:= REC_TAG.HTML_DOC2	;
		pHTML_DOC(3)	:= REC_TAG.HTML_DOC3	;
		pHTML_DOC(4)	:= REC_TAG.HTML_DOC4	;
		pHTML_DOC(5)	:= REC_TAG.HTML_DOC5	;
		pHTML_DOC(6)	:= REC_TAG.HTML_DOC6	;
		pHTML_DOC(7)	:= REC_TAG.HTML_DOC7	;
		pHTML_DOC(8)	:= REC_TAG.HTML_DOC8	;
		pHTML_DOC(9)	:= REC_TAG.HTML_DOC9	;
		pHTML_DOC(10)	:= REC_TAG.HTML_DOC10	;
		pROWID			:= REC_TAG.ROWID		;
		pREVISION_NO	:= REC_TAG.REVISION_NO	;
		pRECORD_COUNT	:= 1;
	END IF;

	CLOSE PKG_USER_DEFINE_TAG.CS02;

	COMMIT;

EXCEPTION
	WHEN OTHERS THEN

		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);

		PKG_USER_DEFINE_TAG.CLOSE_ALL;

		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END USER_DEFINE_TAG_GET;
/
SHOW ERROR;
