/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: システム管理
--  Title			: 未認証キャスト勧誘レベル取得
--	Progaram ID		: UNAUTH_CAST_INVITE_LEVEL_GET
--	Compile Turn	: 0
--  Creation Date	: 10.05.28
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE UNAUTH_CAST_INVITE_LEVEL_GET(
	pSITE_CD			IN	VARCHAR2	,
	pINVITE_LEVEL		IN	NUMBER		,
	pDELAY_DAYS			OUT	NUMBER		,
	pROWID				OUT	VARCHAR2	,
	pREVISION_NO		OUT	NUMBER		,
	pRECORD_COUNT		OUT	NUMBER		,
	pSTATUS				OUT	VARCHAR2
)IS
	-- CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'UNAUTH_CAST_INVITE_LEVEL_GET';

	REC_UNAUTH_CAST_INVITE_LEVEL	PKG_UNAUTH_CAST_INVITE_LEVEL.CS02%ROWTYPE;
BEGIN

	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS				:= APP_COMM.STATUS_NORMAL;
	pRECORD_COUNT 		:= 0;

	pDELAY_DAYS			:= NULL;
	pROWID				:= NULL;
	pREVISION_NO		:= 0;
	pRECORD_COUNT		:= 0;

	/*------------------------------*/
	/* Get Record					*/
	/*------------------------------*/
	OPEN PKG_UNAUTH_CAST_INVITE_LEVEL.CS02(pSITE_CD,pINVITE_LEVEL);
	FETCH PKG_UNAUTH_CAST_INVITE_LEVEL.CS02 INTO REC_UNAUTH_CAST_INVITE_LEVEL;

	IF PKG_UNAUTH_CAST_INVITE_LEVEL.CS02%FOUND THEN
		pDELAY_DAYS			:= REC_UNAUTH_CAST_INVITE_LEVEL.DELAY_DAYS		;
		pROWID				:= REC_UNAUTH_CAST_INVITE_LEVEL.ROWID			;
		pREVISION_NO		:= REC_UNAUTH_CAST_INVITE_LEVEL.REVISION_NO		;
		pRECORD_COUNT		:= 1											;
	END IF;

	CLOSE PKG_UNAUTH_CAST_INVITE_LEVEL.CS02;
	COMMIT;

EXCEPTION
	WHEN OTHERS THEN
		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);
		PKG_UNAUTH_CAST_INVITE_LEVEL.CLOSE_ALL;
		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END UNAUTH_CAST_INVITE_LEVEL_GET;
/
SHOW ERROR;