/*************************************************************************/
--	System			: ViComm
--	Sub System Name	: システム管理
--	Title			: メール文章ドメイン設定取得
--	Progaram ID		: MAIL_BD_DOMAIN_SETTINGS_GET
--	Compile Turn	: 0
--	Creation Date	: 10.07.05
--	Update Date		:
--	Author			: Kazuaki.Itoh@i-Brid
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE MAIL_BD_DOMAIN_SETTINGS_GET(
	pMAIL_BODY_DOMAIN_SETTINGS_SEQ		IN	NUMBER		,
	pSITE_CD							OUT	VARCHAR2	,
	pREPLACE_DOMAIN						OUT	VARCHAR2	,
	pUSED_NOW_FLAG						OUT	NUMBER		,
	pUSED_RANDOM_FLAG					OUT	NUMBER		,
	pCARRIER_USED_MASK					OUT	NUMBER		,
	pROWID								OUT	VARCHAR2	,
	pREVISION_NO						OUT	NUMBER		,
	pRECORD_COUNT						OUT	NUMBER		,
	pSTATUS								OUT	VARCHAR2
)IS
	--	CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'MAIL_BD_DOMAIN_SETTINGS_GET';

	REC_MAIL_BODY_DOMAIN_SETTINGS		PKG_MAIL_BODY_DOMAIN_SETTINGS.CS02%ROWTYPE;
BEGIN
	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS			:= APP_COMM.STATUS_NORMAL;
	pRECORD_COUNT 	:= 0;

	pSITE_CD			:= NULL;
	pREPLACE_DOMAIN		:= NULL;
	pUSED_NOW_FLAG		:= 0;
	pUSED_RANDOM_FLAG	:= 0;
	pCARRIER_USED_MASK	:= 0;
	pROWID				:= NULL;
	pREVISION_NO		:= 0;
	pRECORD_COUNT		:= 0;

	/*------------------------------*/
	/* Get Record					*/
	/*------------------------------*/
	OPEN PKG_MAIL_BODY_DOMAIN_SETTINGS.CS02(pMAIL_BODY_DOMAIN_SETTINGS_SEQ);
	FETCH PKG_MAIL_BODY_DOMAIN_SETTINGS.CS02 INTO REC_MAIL_BODY_DOMAIN_SETTINGS;

	IF PKG_MAIL_BODY_DOMAIN_SETTINGS.CS02%FOUND THEN
		pSITE_CD			:= REC_MAIL_BODY_DOMAIN_SETTINGS.SITE_CD			;
		pREPLACE_DOMAIN		:= REC_MAIL_BODY_DOMAIN_SETTINGS.REPLACE_DOMAIN		;
		pUSED_NOW_FLAG		:= REC_MAIL_BODY_DOMAIN_SETTINGS.USED_NOW_FLAG		;
		pUSED_RANDOM_FLAG	:= REC_MAIL_BODY_DOMAIN_SETTINGS.USED_RANDOM_FLAG	;
		pCARRIER_USED_MASK	:= REC_MAIL_BODY_DOMAIN_SETTINGS.CARRIER_USED_MASK	;
		pROWID				:= REC_MAIL_BODY_DOMAIN_SETTINGS.ROWID				;
		pREVISION_NO		:= REC_MAIL_BODY_DOMAIN_SETTINGS.REVISION_NO		;
		pRECORD_COUNT		:= 1;
	END IF;

	CLOSE PKG_MAIL_BODY_DOMAIN_SETTINGS.CS02;
	COMMIT;

EXCEPTION
	WHEN OTHERS THEN
		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);
		PKG_MAIL_BODY_DOMAIN_SETTINGS.CLOSE_ALL;
		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END MAIL_BD_DOMAIN_SETTINGS_GET;
/
SHOW ERROR;
