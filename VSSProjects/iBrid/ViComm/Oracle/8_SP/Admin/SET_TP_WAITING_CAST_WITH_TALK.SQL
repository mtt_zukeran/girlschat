/*************************************************************************/
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 男性会員・待機開始通知(通話履歴あり)更新
--	Progaram ID		: SET_TP_WAITING_CAST_WITH_TALK
--	Compile Turn	: 0
--	Creation Date	: 11.09.08
--	Update Date		:
--	Author			: iBrid
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE SET_TP_WAITING_CAST_WITH_TALK(
	pSITE_CD				IN	VARCHAR2,
	pMAIL_TEMPLATE_NO		IN	VARCHAR2,
	pSTATUS					OUT	VARCHAR2
)IS
	-- CONSTANT
	PGM_NM	CONSTANT VARCHAR(255)	:= 'SET_TP_WAITING_CAST_WITH_TALK';

	REC_SITE_MANAGEMENT_EX	PKG_SITE_MANAGEMENT_EX.CS02%ROWTYPE;
BEGIN
	/*------------------------------*/
	/* INITILIZE					*/
	/*------------------------------*/
	pSTATUS	:= APP_COMM.STATUS_NORMAL;

	OPEN PKG_SITE_MANAGEMENT_EX.CS02(pSITE_CD);
	FETCH PKG_SITE_MANAGEMENT_EX.CS02 INTO REC_SITE_MANAGEMENT_EX;
	IF PKG_SITE_MANAGEMENT_EX.CS02%FOUND THEN
		UPDATE T_SITE_MANAGEMENT_EX SET
				TP_MAN_WAITING_CAST_WITH_TALK	= pMAIL_TEMPLATE_NO	,
				UPDATE_DATE						= SYSDATE			,
				REVISION_NO						= SEQ_REVISION_NO.NEXTVAL
			WHERE
				CURRENT OF PKG_SITE_MANAGEMENT_EX.CS02;
	ELSE
		INSERT INTO T_SITE_MANAGEMENT_EX (
			SITE_CD							,
			TP_MAN_WAITING_CAST_WITH_TALK	,
			WANTED_START_TIME				,
			WANTED_END_TIME					,
			WANTED_ANNOUNCE_TIME			,
			WANTED_BOUNTY_POINT				,
			BUY_POINT_MAIL_COMM_DAYS		,
			LOVE_LIST_PROFILE_DAYS			,
			FACE_PIC_BONUS_POINT			,
			CAST_REFUSE_POINT				,
			CAST_FAVORIT_POINT				,
			BBS_RES_SEARCH_LIMIT			,
			WANTED_APPLICANT_DEL_DAYS		,
			MAN_ADD_BINGO_JACKPOT_MIN		,
			MAN_ADD_BINGO_JACKPOT_MAX		,
			CAST_ADD_BINGO_JACKPOT_MIN		,
			CAST_ADD_BINGO_JACKPOT_MAX		,
			UPDATE_DATE						,
			REVISION_NO
		) VALUES (
			pSITE_CD						,
			pMAIL_TEMPLATE_NO				,
			0								,
			0								,
			0								,
			0								,
			0								,
			0								,
			0								,
			0								,
			0								,
			0								,
			0								,
			0								,
			0								,
			0								,
			0								,
			SYSDATE							,
			SEQ_REVISION_NO.NEXTVAL
		);
	END IF;
	CLOSE PKG_SITE_MANAGEMENT_EX.CS02;

	COMMIT;

EXCEPTION
	WHEN OTHERS THEN

		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);
		PKG_SITE_MANAGEMENT_EX.CLOSE_ALL;
		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END SET_TP_WAITING_CAST_WITH_TALK;
/
SHOW ERROR;
