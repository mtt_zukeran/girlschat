/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: システム管理
--  Title			: ユーザー定義タグ取得
--	Progaram ID		: USER_DEFINE_TAG_MAINTE
--	Compile Turn	: 0
--  Creation Date	: 09.06.01
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE USER_DEFINE_TAG_MAINTE(
	pSITE_CD			IN	VARCHAR2	,
	pVARIABLE_ID		IN	VARCHAR2	,
	pUSER_AGENT_TYPE	IN	VARCHAR2	,
	pVARIABLE_NM		IN	VARCHAR2	,
	pCATEGORY_SEQ		IN	NUMBER		,
	pHTML_DOC			IN	APP_COMM.TBL_LONG_VARCHAR2,
	pHTML_DOC_COUNT		IN	NUMBER		,
	pROWID				IN	VARCHAR2	,
	pREVISION_NO		IN	NUMBER		,
	pDEL_FLAG			IN	NUMBER		,
	pSTATUS				OUT	VARCHAR2
)IS
	-- CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'USER_DEFINE_TAG_MAINTE';

	REC_TAG	PKG_USER_DEFINE_TAG.CS03%ROWTYPE;

	BUF_HTML_DOC APP_COMM.TBL_LONG_VARCHAR2;
BEGIN

	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS	:= APP_COMM.STATUS_NORMAL;

	FOR i IN 1..pHTML_DOC_COUNT LOOP
		BUF_HTML_DOC(i) := pHTML_DOC(i);
	END LOOP;

	FOR i IN pHTML_DOC_COUNT+1..10 LOOP
		BUF_HTML_DOC(i) := NULL;
	END LOOP;

	OPEN PKG_USER_DEFINE_TAG.CS03(pROWID);
	FETCH PKG_USER_DEFINE_TAG.CS03 INTO REC_TAG;
	IF PKG_USER_DEFINE_TAG.CS03%FOUND THEN
		IF (APP_COMM.CHECK_REVISION_NO(pREVISION_NO,REC_TAG.REVISION_NO) = TRUE) THEN
			IF pDEL_FLAG = 0 THEN
				UPDATE T_USER_DEFINE_TAG SET
						VARIABLE_NM		= pVARIABLE_NM		,
						CATEGORY_SEQ    = pCATEGORY_SEQ		,
						HTML_DOC1		= BUF_HTML_DOC(1)	,
						HTML_DOC2		= BUF_HTML_DOC(2)	,
						HTML_DOC3		= BUF_HTML_DOC(3)	,
						HTML_DOC4		= BUF_HTML_DOC(4)	,
						HTML_DOC5		= BUF_HTML_DOC(5)	,
						HTML_DOC6		= BUF_HTML_DOC(6)	,
						HTML_DOC7		= BUF_HTML_DOC(7)	,
						HTML_DOC8		= BUF_HTML_DOC(8)	,
						HTML_DOC9		= BUF_HTML_DOC(9)	,
						HTML_DOC10		= BUF_HTML_DOC(10)	,
						UPDATE_DATE		= SYSDATE			,
						REVISION_NO		= SEQ_REVISION_NO.NEXTVAL
					WHERE
						CURRENT OF PKG_USER_DEFINE_TAG.CS03;
			ELSE
				DELETE T_USER_DEFINE_TAG
					WHERE
						CURRENT OF PKG_USER_DEFINE_TAG.CS03;
			END IF;
		ELSE
			RAISE APP_COMM.UPDATE_BY_ANYONE;
		END IF;
	ELSE
		IF (pDEL_FLAG = 0) THEN
			INSERT INTO T_USER_DEFINE_TAG(
				SITE_CD			,
				VARIABLE_ID		,
				USER_AGENT_TYPE	,
				VARIABLE_NM		,
				CATEGORY_SEQ	,
				HTML_DOC1		,
				HTML_DOC2		,
				HTML_DOC3		,
				HTML_DOC4		,
				HTML_DOC5		,
				HTML_DOC6		,
				HTML_DOC7		,
				HTML_DOC8		,
				HTML_DOC9		,
				HTML_DOC10		,
				UPDATE_DATE		,
				REVISION_NO
			)VALUES(
				pSITE_CD 				,
				pVARIABLE_ID			,
				pUSER_AGENT_TYPE		,
				pVARIABLE_NM			,
				pCATEGORY_SEQ			,
				BUF_HTML_DOC(1)			,
				BUF_HTML_DOC(2)			,
				BUF_HTML_DOC(3)			,
				BUF_HTML_DOC(4)			,
				BUF_HTML_DOC(5)			,
				BUF_HTML_DOC(6)			,
				BUF_HTML_DOC(7)			,
				BUF_HTML_DOC(8)			,
				BUF_HTML_DOC(9)			,
				BUF_HTML_DOC(10)		,
				SYSDATE					,
				SEQ_REVISION_NO.NEXTVAL
			);
		END IF;
	END IF;

	CLOSE PKG_USER_DEFINE_TAG.CS03;

	COMMIT;

EXCEPTION
	WHEN OTHERS THEN

		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);

		PKG_USER_DEFINE_TAG.CLOSE_ALL;

		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END USER_DEFINE_TAG_MAINTE;
/
SHOW ERROR;
