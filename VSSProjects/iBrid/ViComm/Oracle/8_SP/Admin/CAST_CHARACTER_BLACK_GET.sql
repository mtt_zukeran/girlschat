/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: システム管理
--  Title			: 出演者ブラックリスト取得
--	Progaram ID		: CAST_CHARACTER_BLACK_GET
--	Compile Turn	: 0
--  Creation Date	: 11.04.04
--	Update Date		:
--  Author			: i-Brid(R.Suzuki)
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE CAST_CHARACTER_BLACK_GET(
	pSITE_CD			IN	VARCHAR2	,
	pUSER_SEQ			IN	VARCHAR2	,
	pUSER_CHAR_NO		IN	VARCHAR2	,
	pBLACK_TYPE			IN	VARCHAR2	,
	pREMARKS			OUT	VARCHAR2	,
	pROWID				OUT	VARCHAR2	,
	pREVISION_NO		OUT	NUMBER		,
	pRECORD_COUNT		OUT	NUMBER		,
	pSTATUS				OUT	VARCHAR2	
)IS
	--	CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'CAST_CHARACTER_BLACK_GET';

	REC_CAST_CHARACTER_BLACK	PKG_CAST_CHARACTER_BLACK.CS02%ROWTYPE;
BEGIN
	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS	:= APP_COMM.STATUS_NORMAL;

	pREMARKS			:= NULL;
	pREVISION_NO		:= 0;
	pRECORD_COUNT		:= 0;

	/*------------------------------*/
	/* Get Record					*/
	/*------------------------------*/
	OPEN PKG_CAST_CHARACTER_BLACK.CS02(pSITE_CD,pUSER_SEQ,pUSER_CHAR_NO,pBLACK_TYPE);
	FETCH PKG_CAST_CHARACTER_BLACK.CS02 INTO REC_CAST_CHARACTER_BLACK;

	IF PKG_CAST_CHARACTER_BLACK.CS02%FOUND THEN
		pREMARKS			:= REC_CAST_CHARACTER_BLACK.REMARKS			;
		pROWID				:= REC_CAST_CHARACTER_BLACK.ROWID			;
		pREVISION_NO		:= REC_CAST_CHARACTER_BLACK.REVISION_NO		;
		pRECORD_COUNT		:= 1;
	END IF;

	CLOSE PKG_CAST_CHARACTER_BLACK.CS02;
	COMMIT;

EXCEPTION
	WHEN OTHERS THEN

		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);
		PKG_CAST_CHARACTER_BLACK.CLOSE_ALL;
		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END CAST_CHARACTER_BLACK_GET;
/
SHOW ERROR;
