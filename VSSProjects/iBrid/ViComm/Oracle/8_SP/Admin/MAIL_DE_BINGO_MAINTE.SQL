/*************************************************************************/
--	System			: ViComm
--	Sub System Name	: システム管理
--	Title			: メールdeビンゴ開催設定更新
--	Progaram ID		: MAIL_DE_BINGO_MAINTE
--	Compile Turn	: 0
--	Creation Date	: 11.07.06
--	Update Date		:
--	Author			: i-Brid
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE MAIL_DE_BINGO_MAINTE(
	pSITE_CD					IN	VARCHAR2	,
	pBINGO_TERM_SEQ				IN	VARCHAR2	,
	pSEX_CD						IN	VARCHAR2	,
	pBINGO_START_DATE			IN	DATE		,
	pBINGO_END_DATE				IN	DATE		,
	pJACKPOT					IN	NUMBER		,
	pREMARKS					IN	VARCHAR2	,
	pBINGO_APPLICATION_DATE		IN	DATE		,
	pBINGO_BALL_FLAG			IN	NUMBER		,
	pROWID						IN	VARCHAR2	,
	pREVISION_NO				IN	NUMBER		,
	pDEL_FLAG					IN	NUMBER		,
	pSTATUS						OUT	VARCHAR2
)IS
	--	CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'MAIL_DE_BINGO_MAINTE';

	REC_MAIL_DE_BINGO		PKG_MAIL_DE_BINGO.CS03%ROWTYPE;
BEGIN
	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS	:= APP_COMM.STATUS_NORMAL;

	/*------------------------------*/
	/* Open Cursor					*/
	/*------------------------------*/
	OPEN PKG_MAIL_DE_BINGO.CS03(pROWID);

	/*------------------------------*/
	/* Update Record				*/
	/*------------------------------*/
	FETCH PKG_MAIL_DE_BINGO.CS03 INTO REC_MAIL_DE_BINGO;

	IF PKG_MAIL_DE_BINGO.CS03%FOUND THEN

		IF (APP_COMM.CHECK_REVISION_NO(pREVISION_NO,REC_MAIL_DE_BINGO.REVISION_NO) = TRUE) THEN

			IF (pDEL_FLAG = 0) THEN
				UPDATE T_MAIL_DE_BINGO SET
						SEX_CD					= pSEX_CD					,
						BINGO_START_DATE		= pBINGO_START_DATE			,
						BINGO_END_DATE			= pBINGO_END_DATE			,
						JACKPOT					= pJACKPOT					,
						REMARKS					= pREMARKS					,
						BINGO_APPLICATION_DATE	= pBINGO_APPLICATION_DATE	,
						BINGO_BALL_FLAG			= pBINGO_BALL_FLAG			,
						UPDATE_DATE				= SYSDATE					,
						REVISION_NO				= SEQ_REVISION_NO.NEXTVAL
					WHERE
						CURRENT OF PKG_MAIL_DE_BINGO.CS03;
			ELSE
				DELETE T_MAIL_DE_BINGO
					WHERE
						CURRENT OF PKG_MAIL_DE_BINGO.CS03;
			END IF;
		ELSE
			RAISE APP_COMM.UPDATE_BY_ANYONE;
		END IF;
	ELSE
		INSERT INTO T_MAIL_DE_BINGO(
			SITE_CD					,
			BINGO_TERM_SEQ			,
			SEX_CD					,
			BINGO_START_DATE		,
			BINGO_END_DATE			,
			JACKPOT					,
			REMARKS					,
			BINGO_APPLICATION_DATE	,
			BINGO_BALL_FLAG			,
			UPDATE_DATE				,
			REVISION_NO
		)VALUES(
			pSITE_CD					,
			SEQ_BINGO_TERM.NEXTVAL		,
			pSEX_CD						,
			pBINGO_START_DATE			,
			pBINGO_END_DATE				,
			pJACKPOT					,
			pREMARKS					,
			pBINGO_APPLICATION_DATE		,
			pBINGO_BALL_FLAG			,
			SYSDATE						,
			SEQ_REVISION_NO.NEXTVAL
		);
	END IF;

	CLOSE PKG_MAIL_DE_BINGO.CS03;

	COMMIT;

EXCEPTION
	WHEN OTHERS THEN

		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);

		PKG_MAIL_DE_BINGO.CLOSE_ALL;

		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END MAIL_DE_BINGO_MAINTE;
/
SHOW ERROR;
