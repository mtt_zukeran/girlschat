/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: システム管理
--  Title			: つぶやきコメントランキング表示順位設定更新
--	Progaram ID		: TWEET_COMMENT_DISP_RANK_MAINTE
--	Compile Turn	: 0
--  Creation Date	: 17.04.10
--	Update Date		:
--  Author			: K.Yogi
/*************************************************************************/
create or replace
PROCEDURE TWEET_COMMENT_DISP_RANK_MAINTE(
	pDISP_RANK_SEQ		IN	NUMBER,
	pSEX_CD		IN	VARCHAR2,
	pDISP_RANK_NO_NOW		IN	APP_COMM.TBL_NUMBER,
  pDISP_RANK_NO_PAST		IN	APP_COMM.TBL_NUMBER,
  pDISP_RANK_NO_MANAGE		IN	APP_COMM.TBL_NUMBER,
	pDELETE_FLAG	IN	NUMBER,
--	pREVISION_NO	IN	NUMBER,
	pSTATUS			OUT	VARCHAR2
)IS
  PGM_NM 	CONSTANT VARCHAR(255)	:= 'TWEET_COMMENT_DISP_RANK_MAINTE';
  nDispRankSeq NUMBER;
  DISP_RANK_TYPE_MANAGE NUMBER := 0; --ランキング表示順位(管理画面)
  DISP_RANK_TYPE_NOW NUMBER := 1;    --ランキング表示順位(現在ランキング)
  DISP_RANK_TYPE_PAST NUMBER := 2;   --ランキング表示順位(過去ランキング)

BEGIN
  pSTATUS	:= APP_COMM.STATUS_NORMAL;
  nDispRankSeq := pDISP_RANK_SEQ;
  
  --レコード数が変わっているかもしれないので、更新の場合も全削除後にもう一度登録する
  DELETE FROM 
    T_TWEET_COMMENT_DISP_RANK	
  WHERE 
    DISP_RANK_SEQ = nDispRankSeq 
    AND SEX_CD =pSEX_CD;
  
  IF (pDELETE_FLAG = 0) THEN
  
    IF nDispRankSeq IS NULL OR (nDispRankSeq < 1) THEN
      nDispRankSeq := SEQ_TWEET_COMMENT_DISP_RANK.NEXTVAL;
    END IF;
    
    --ランキング表示順位(管理画面ランキング)
    FOR i IN 1..pDISP_RANK_NO_MANAGE.COUNT LOOP
      INSERT INTO T_TWEET_COMMENT_DISP_RANK 
      (
        DISP_RANK_SEQ,
        DISP_RANK_TYPE,
        SEX_CD,
        DISP_RANK_NO
      )VALUES(
        nDispRankSeq,
        DISP_RANK_TYPE_MANAGE,
        PSEX_CD,
        pDISP_RANK_NO_MANAGE(i)
      );
    END LOOP;
    
    --ランキング表示順位(現在ランキング)
    FOR i IN 1..pDISP_RANK_NO_NOW.COUNT LOOP
      INSERT INTO T_TWEET_COMMENT_DISP_RANK 
      (
        DISP_RANK_SEQ,
        DISP_RANK_TYPE,
        SEX_CD,
        DISP_RANK_NO
      )VALUES(
        nDispRankSeq,
        DISP_RANK_TYPE_NOW,
        PSEX_CD,
        pDISP_RANK_NO_NOW(i)
      );
    END LOOP;
    
    --ランキング表示順位(過去ランキング)
    FOR i IN 1..pDISP_RANK_NO_PAST.COUNT LOOP
      INSERT INTO T_TWEET_COMMENT_DISP_RANK 
      (
        DISP_RANK_SEQ,
        DISP_RANK_TYPE,
        SEX_CD,
        DISP_RANK_NO
      )VALUES(
        nDispRankSeq,
        DISP_RANK_TYPE_PAST,
        PSEX_CD,
        pDISP_RANK_NO_PAST(i)
      );
    END LOOP;
    
  END IF;
  
  COMMIT;
  
EXCEPTION
	WHEN OTHERS THEN
		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);
		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

--		PKG_VOTE_RANK.CLOSE_ALL;
END TWEET_COMMENT_DISP_RANK_MAINTE;
/
SHOW ERROR;