/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: システム管理
--  Title			: 男性用お宝チェック完了（チェック済みフラグをたてる）
--	Progaram ID		: MAN_TREASURE_CHECKED
--	Compile Turn	: 0
--  Creation Date	: 16.09.30
--	Update Date		:
--  Author			: M&TT Zukeran
/*************************************************************************/
-- [ Update History ]
/*-----------------------------------------------------------------------*/
--  Date        Updater    Update Explain
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE MAN_TREASURE_CHECKED(
	pSITE_CD				IN	VARCHAR2	,
	pUSER_SEQ				IN	VARCHAR2	,
	pUSER_CHAR_NO			IN	VARCHAR2	,
	pCAST_GAME_PIC_SEQ		IN	VARCHAR2	,
	pSTATUS					OUT	VARCHAR2
)IS
	--	CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'MAN_TREASURE_CHECKED';

	CURSOR CS_MAN_TREASURE IS
		SELECT ROWID,T_MAN_TREASURE.* FROM T_MAN_TREASURE
		WHERE
			SITE_CD				= pSITE_CD			AND
			USER_SEQ			= pUSER_SEQ			AND
			USER_CHAR_NO		= pUSER_CHAR_NO		AND
			CAST_GAME_PIC_SEQ	= pCAST_GAME_PIC_SEQ
		FOR UPDATE WAIT 5;

	REC_MAN_TREASURE		CS_MAN_TREASURE%ROWTYPE;
BEGIN
	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS					:= APP_COMM.STATUS_NORMAL;

	/*------------------------------*/
	/* Get Record					*/
	/*------------------------------*/
	OPEN CS_MAN_TREASURE();
	FETCH CS_MAN_TREASURE INTO REC_MAN_TREASURE;
	IF CS_MAN_TREASURE%FOUND THEN
		UPDATE T_MAN_TREASURE SET
			CHECKED_FLAG = 1
		WHERE
			CURRENT OF CS_MAN_TREASURE;
	END IF;
	CLOSE CS_MAN_TREASURE;

	COMMIT;
EXCEPTION
	WHEN OTHERS THEN

		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);

		IF CS_MAN_TREASURE%ISOPEN THEN
			CLOSE CS_MAN_TREASURE;
		END IF;

		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END MAN_TREASURE_CHECKED;
/
SHOW ERROR;
