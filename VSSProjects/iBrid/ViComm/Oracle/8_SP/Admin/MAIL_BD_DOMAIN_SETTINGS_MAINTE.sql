/*************************************************************************/
--	System			: ViComm
--	Sub System Name	: システム管理
--	Title			: メール文章ドメイン設定更新
--	Progaram ID		: MAIL_BD_DOMAIN_SETTINGS_MAINTE
--	Compile Turn	: 0
--	Creation Date	: 10.07.05
--	Update Date		:
--	Author			: Kazuaki.Itoh@i-Brid
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE MAIL_BD_DOMAIN_SETTINGS_MAINTE(
	pSITE_CD						IN	VARCHAR2	,
	pMAIL_BODY_DOMAIN_SETTINGS_SEQ	IN	NUMBER		,
	pREPLACE_DOMAIN					IN	VARCHAR2	,
	pUSED_NOW_FLAG					IN	NUMBER		,
	pUSED_RANDOM_FLAG				IN	NUMBER		,
	pCARRIER_USED_MASK				IN	NUMBER		,
	pROWID							IN	VARCHAR2	,
	pREVISION_NO					IN	NUMBER		,
	pDEL_FLAG						IN	NUMBER		,
	pSTATUS							OUT	VARCHAR2
)IS
	--	CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'MAIL_BD_DOMAIN_SETTINGS_MAINTE';

	CURSOR CS_MAIL_BODY_DOMAIN_SETTINGS IS 
		SELECT
			ROWID	,
			MAIL_BODY_DOMAIN_SETTINGS_SEQ
		FROM
			T_MAIL_BODY_DOMAIN_SETTINGS
		WHERE
			SITE_CD			= pSITE_CD	AND
			USED_NOW_FLAG	= 1;

	REC_MAIL_BODY_DOMAIN_SETTINGS	PKG_MAIL_BODY_DOMAIN_SETTINGS.CS03	%ROWTYPE;
	BUF_SEQ							NUMBER(15);
BEGIN
	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS	:= APP_COMM.STATUS_NORMAL;
	BUF_SEQ	:= 0;

	/*------------------------------*/
	/* Open Cursor					*/
	/*------------------------------*/
	OPEN PKG_MAIL_BODY_DOMAIN_SETTINGS.CS03(pROWID);

	/*------------------------------*/
	/* Update Record				*/
	/*------------------------------*/
	FETCH PKG_MAIL_BODY_DOMAIN_SETTINGS.CS03 INTO REC_MAIL_BODY_DOMAIN_SETTINGS;

	IF PKG_MAIL_BODY_DOMAIN_SETTINGS.CS03%FOUND THEN

		IF (APP_COMM.CHECK_REVISION_NO(pREVISION_NO,REC_MAIL_BODY_DOMAIN_SETTINGS.REVISION_NO) = TRUE) THEN
			IF pDEL_FLAG = 0 THEN

				UPDATE T_MAIL_BODY_DOMAIN_SETTINGS SET
						SITE_CD							= pSITE_CD							,
						MAIL_BODY_DOMAIN_SETTINGS_SEQ	= pMAIL_BODY_DOMAIN_SETTINGS_SEQ	,
						REPLACE_DOMAIN					= pREPLACE_DOMAIN					,
						USED_NOW_FLAG					= pUSED_NOW_FLAG					,
						USED_RANDOM_FLAG				= pUSED_RANDOM_FLAG					,
						CARRIER_USED_MASK				= pCARRIER_USED_MASK				,
						UPDATE_DATE						= SYSDATE							,
						REVISION_NO						= SEQ_REVISION_NO.NEXTVAL
					WHERE
						CURRENT OF PKG_MAIL_BODY_DOMAIN_SETTINGS.CS03;
			ELSE
				DELETE T_MAIL_BODY_DOMAIN_SETTINGS
					WHERE
						CURRENT OF PKG_MAIL_BODY_DOMAIN_SETTINGS.CS03;
			END IF;
		ELSE
			RAISE APP_COMM.UPDATE_BY_ANYONE;
		END IF;
	ELSE
		IF pDEL_FLAG = 0 THEN

			IF (pMAIL_BODY_DOMAIN_SETTINGS_SEQ != 0) THEN
				LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,'T_MAIL_BODY_DOMAIN_SETTINGS NOT FOUND SEQ=' || pMAIL_BODY_DOMAIN_SETTINGS_SEQ);
				RAISE PROGRAM_ERROR;
			END IF;

			SELECT SEQ_MAIL_BODY_DOMAIN_SETTINGS.NEXTVAL INTO BUF_SEQ FROM DUAL;

			INSERT INTO T_MAIL_BODY_DOMAIN_SETTINGS (
				SITE_CD							,
				MAIL_BODY_DOMAIN_SETTINGS_SEQ	,
				REPLACE_DOMAIN					,
				USED_NOW_FLAG					,
				USED_RANDOM_FLAG				,
				CARRIER_USED_MASK				,
				UPDATE_DATE						,
				REVISION_NO						
			)VALUES(
				pSITE_CD					,
				BUF_SEQ						,
				pREPLACE_DOMAIN				,
				pUSED_NOW_FLAG				,
				pUSED_RANDOM_FLAG			,
				pCARRIER_USED_MASK			,
				SYSDATE						,
				SEQ_REVISION_NO.NEXTVAL		
			);

		END IF;

	END IF;

	CLOSE PKG_MAIL_BODY_DOMAIN_SETTINGS.CS03;

	IF (pDEL_FLAG = 0 AND pUSED_NOW_FLAG = 1) THEN
		FOR REC_USED_NOW_FLAG IN CS_MAIL_BODY_DOMAIN_SETTINGS LOOP
			
			--INSERTしたﾚｺｰﾄﾞは更新しない
			IF ( (BUF_SEQ = REC_USED_NOW_FLAG.MAIL_BODY_DOMAIN_SETTINGS_SEQ) ) THEN
				NULL;
			--UPDATEしたﾚｺｰﾄﾞは更新しない
			ELSIF ( (REC_USED_NOW_FLAG.ROWID = pROWID) ) THEN
				NULL;
			--その他を更新する
			ELSE
				OPEN PKG_MAIL_BODY_DOMAIN_SETTINGS.CS03(REC_USED_NOW_FLAG.ROWID);
				FETCH PKG_MAIL_BODY_DOMAIN_SETTINGS.CS03 INTO REC_MAIL_BODY_DOMAIN_SETTINGS;
				IF PKG_MAIL_BODY_DOMAIN_SETTINGS.CS03%FOUND THEN
					UPDATE T_MAIL_BODY_DOMAIN_SETTINGS SET
							USED_NOW_FLAG	= 0,
							REVISION_NO		= SEQ_REVISION_NO.NEXTVAL
						WHERE
							CURRENT OF PKG_MAIL_BODY_DOMAIN_SETTINGS.CS03;
				END IF;
				CLOSE PKG_MAIL_BODY_DOMAIN_SETTINGS.CS03;
			END IF;
		END LOOP;
	END IF;
	COMMIT;

EXCEPTION
	WHEN OTHERS THEN
		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);
		PKG_MAIL_BODY_DOMAIN_SETTINGS.CLOSE_ALL;
		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END MAIL_BD_DOMAIN_SETTINGS_MAINTE;
/
SHOW ERROR;
