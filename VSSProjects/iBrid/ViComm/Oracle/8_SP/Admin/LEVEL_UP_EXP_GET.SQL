/*************************************************************************/
--	System			: ViComm
--	Sub System Name	: システム管理
--	Title			: 抽選獲得ｱｲﾃﾑ取得
--	Progaram ID		: LEVEL_UP_EXP_GET
--	Compile Turn	: 0
--	Creation Date	: 11.07.25
--	Update Date		:
--	Author			: iBrid
/*************************************************************************/
-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
/*-----------------------------------------------------------------------*/

CREATE OR REPLACE PROCEDURE LEVEL_UP_EXP_GET(
	pSITE_CD						IN		VARCHAR2	,
	pGAME_CHARACTER_LEVEL			IN		VARCHAR2	,
	pSEX_CD							IN		VARCHAR2	,
	pNEED_EXPERIENCE_POINT			OUT		NUMBER		,
	pCHARACTER_HP					OUT		NUMBER		,
	pFELLOW_COUNT_LIMIT				OUT		NUMBER		,
	pADD_FORCE_COUNT				OUT		NUMBER		,
	pREVISION_NO					OUT		NUMBER		,
	pSTATUS							OUT		VARCHAR2
)IS
--	CONST
	PGM_NM		CONSTANT VARCHAR(255)	:= 'LEVEL_UP_EXP_GET';

--	REC
	REC_LEVEL_UP_EXP	PKG_LEVEL_UP_EXP.CS02%ROWTYPE;
BEGIN
	pSTATUS						:= APP_COMM.STATUS_NORMAL;
	pNEED_EXPERIENCE_POINT		:= 0;
	pCHARACTER_HP				:= 0;
	pFELLOW_COUNT_LIMIT			:= 0;
	pADD_FORCE_COUNT			:= 0;
	pREVISION_NO				:= 0;

	OPEN PKG_LEVEL_UP_EXP.CS02(pSITE_CD,pGAME_CHARACTER_LEVEL,pSEX_CD);
	FETCH PKG_LEVEL_UP_EXP.CS02 INTO REC_LEVEL_UP_EXP;
	IF (PKG_LEVEL_UP_EXP.CS02%FOUND) THEN
		pNEED_EXPERIENCE_POINT		:= REC_LEVEL_UP_EXP.NEED_EXPERIENCE_POINT	;
		pCHARACTER_HP				:= REC_LEVEL_UP_EXP.CHARACTER_HP			;
		pFELLOW_COUNT_LIMIT			:= REC_LEVEL_UP_EXP.FELLOW_COUNT_LIMIT		;
		pADD_FORCE_COUNT			:= REC_LEVEL_UP_EXP.ADD_FORCE_COUNT			;
		pREVISION_NO				:= REC_LEVEL_UP_EXP.REVISION_NO				;
	END IF;
	CLOSE PKG_LEVEL_UP_EXP.CS02;
	
	COMMIT;

EXCEPTION
	WHEN OTHERS THEN
		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);
		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

		PKG_LEVEL_UP_EXP.CLOSE_ALL;

END LEVEL_UP_EXP_GET;
/
SHOW ERROR;
