/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: システム管理
--  Title			: 出演者証明書写真更新
--	Progaram ID		: CAST_ID_PIC_UPLOAD
--	Compile Turn	: 0
--  Creation Date	: 09.06.01
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE CAST_ID_PIC_UPLOAD(
	pUSER_SEQ		IN 		VARCHAR2	,
	pID_PIC_SEQ		IN		VARCHAR2	,
	pID_PIC_NO		IN		NUMBER		,
	pSTATUS			OUT		VARCHAR2
)IS
	--	CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'CAST_ID_PIC_UPLOAD';

	REC_CAST	PKG_CAST.CS02%ROWTYPE;

BEGIN
	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS	:= APP_COMM.STATUS_NORMAL;

	OPEN PKG_CAST.CS02(pUSER_SEQ);
	FETCH PKG_CAST.CS02 INTO REC_CAST;

	IF PKG_CAST.CS02%FOUND THEN
		IF (pID_PIC_NO = 1) THEN
			UPDATE T_CAST SET
					ID_PIC_SEQ1	= pID_PIC_SEQ
				WHERE
					CURRENT OF PKG_CAST.CS02;
		ELSIF (pID_PIC_NO = 2) THEN
			UPDATE T_CAST SET
					ID_PIC_SEQ2	= pID_PIC_SEQ
				WHERE
					CURRENT OF PKG_CAST.CS02;
		ELSIF (pID_PIC_NO = 3) THEN
			UPDATE T_CAST SET
					ID_PIC_SEQ3	= pID_PIC_SEQ
				WHERE
					CURRENT OF PKG_CAST.CS02;
		ELSIF (pID_PIC_NO = 4) THEN
			UPDATE T_CAST SET
					ID_PIC_SEQ4	= pID_PIC_SEQ
				WHERE
					CURRENT OF PKG_CAST.CS02;
		ELSIF (pID_PIC_NO = 5) THEN
			UPDATE T_CAST SET
					ID_PIC_SEQ5	= pID_PIC_SEQ
				WHERE
					CURRENT OF PKG_CAST.CS02;
		ELSIF (pID_PIC_NO = 6) THEN
			UPDATE T_CAST SET
					ID_PIC_SEQ6	= pID_PIC_SEQ
				WHERE
					CURRENT OF PKG_CAST.CS02;
		ELSIF (pID_PIC_NO = 7) THEN
			UPDATE T_CAST SET
					ID_PIC_SEQ7	= pID_PIC_SEQ
				WHERE
					CURRENT OF PKG_CAST.CS02;
		ELSIF (pID_PIC_NO = 8) THEN
			UPDATE T_CAST SET
					ID_PIC_SEQ8	= pID_PIC_SEQ
				WHERE
					CURRENT OF PKG_CAST.CS02;
		ELSIF (pID_PIC_NO = 9) THEN
			UPDATE T_CAST SET
					ID_PIC_SEQ9	= pID_PIC_SEQ
				WHERE
					CURRENT OF PKG_CAST.CS02;
		ELSIF (pID_PIC_NO = 10) THEN
			UPDATE T_CAST SET
					ID_PIC_SEQ10= pID_PIC_SEQ
				WHERE
					CURRENT OF PKG_CAST.CS02;
		END IF;
	END IF;

	CLOSE PKG_CAST.CS02;

	COMMIT;

EXCEPTION
	WHEN OTHERS THEN

		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);

		PKG_CAST.CLOSE_ALL;

		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END CAST_ID_PIC_UPLOAD;
/
SHOW ERROR;
