/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: 共通管理
--  Title			: セッション終了
--	Progaram ID		: END_SESSION
--	Compile Turn	: 0
--  Creation Date	: 08.10.21
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE END_SESSION(
	pWEB_SESSION_ID	IN	VARCHAR2,
	pSTATUS			OUT	VARCHAR2
)IS
	--	CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'END_SESSION';

	REC_SESSION	PKG_SESSION.CS02%ROWTYPE;
BEGIN
	pSTATUS	:= APP_COMM.STATUS_NORMAL;

	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	OPEN PKG_SESSION.CS02(pWEB_SESSION_ID);
	FETCH PKG_SESSION.CS02 INTO REC_SESSION;
	IF PKG_SESSION.CS02%FOUND THEN
		IF (REC_SESSION.VOTE_COUNT > 1) THEN
			UPDATE T_SESSION SET
				VOTE_COUNT = VOTE_COUNT - 1 
			WHERE
				CURRENT OF PKG_SESSION.CS02;
		ELSE
			DELETE T_SESSION
			WHERE
				CURRENT OF PKG_SESSION.CS02;
		END IF;
	ELSE
		NULL;
		--APP_COMM.TRACE(APP_COMM.TRACE_INFO,PGM_NM || ' NOT FOUND ' || pWEB_SESSION_ID);
	END IF;
	CLOSE PKG_SESSION.CS02;

	COMMIT;

EXCEPTION
	WHEN OTHERS THEN
		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);

		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);
		
		PKG_SESSION.CLOSE_ALL;

END END_SESSION;
/
SHOW ERROR;
