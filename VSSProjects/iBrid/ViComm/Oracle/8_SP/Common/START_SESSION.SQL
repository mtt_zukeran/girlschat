/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: 共通管理
--  Title			: システム開始
--	Progaram ID		: START_SESSION
--	Compile Turn	: 0
--  Creation Date	: 08.10.21
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE START_SESSION(
	pWEB_SESSION_ID			IN	VARCHAR2,
	pWEB_SESSION_OWNER_NM	IN	VARCHAR2,
	pSTATUS					OUT	VARCHAR2
)
IS
	--	CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'START_SESSION';

BEGIN
	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS	:= APP_COMM.STATUS_NORMAL;

	BEGIN
		INSERT INTO T_SESSION(WEB_SESSION_ID,WEB_SESSION_OWNER_NM,UPDATE_DATE)VALUES(pWEB_SESSION_ID,pWEB_SESSION_OWNER_NM,SYSDATE);
	EXCEPTION
		WHEN DUP_VAL_ON_INDEX THEN 
			APP_COMM.TRACE(APP_COMM.TRACE_INFO,PGM_NM || ' DUPLICATE SESSION ' || pWEB_SESSION_ID);
	END;

	COMMIT;

EXCEPTION
	WHEN OTHERS THEN

		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);

		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END START_SESSION;
/
SHOW ERROR;
