/*************************************************************************/
--	System			: ViComm
--	Sub System Name	: 会員
--	Title			: ｹﾞｰﾑﾚﾎﾟｰﾄﾍﾟｰｼﾞﾋﾞｭｰ更新
--	Progaram ID		: CREATE_GAME_USED_POINT
--	Compile Turn	: 0
--	Creation Date	: 11.07.25
--	Update Date		:
--	Author			: R.Suzuki
/*************************************************************************/
-- [ Update History ]
/*-----------------------------------------------------------------------*/
--	Date        Updater    Update Explain
--	yyyy/mm/dd  XXXXXXXXX
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE CREATE_GAME_USED_POINT(
	pSITE_CD				IN	VARCHAR2	,
	pUSER_SEQ				IN	VARCHAR2	,
	pUSER_CHAR_NO			IN	VARCHAR2	,
	pUSED_POINT				IN	NUMBER		,
	pCHARGE_FLAG			IN	NUMBER		,	--課金フラグ
	pSTATUS					OUT	VARCHAR2
)IS
	-- CONSTANT
	PGM_NM 		CONSTANT VARCHAR(255)	:= 'CREATE_GAME_USED_POINT';

	BUF_ACTION	CONSTANT VARCHAR2(32)	:= 'UsePoint';

	--CURSOR
	CURSOR CS_GAME_CHARACTER IS
		SELECT
			SEX_CD		,
			REGIST_DATE
		FROM
			T_GAME_CHARACTER
		WHERE
			SITE_CD			= pSITE_CD		AND
			USER_SEQ		= pUSER_SEQ		AND
			USER_CHAR_NO	= pUSER_CHAR_NO;

	--年齢(男性はｷｬﾗｸﾀの年齢 女性は実年齢)
	CURSOR CS_MAN_AGE IS
		SELECT
			AGE
		FROM
			T_USER_MAN_CHARACTER
		WHERE
			SITE_CD			= pSITE_CD		AND
			USER_SEQ		= pUSER_SEQ		AND
			USER_CHAR_NO	= pUSER_CHAR_NO;

	CURSOR CS_CAST_AGE IS
		SELECT
			NVL(TO_NUMBER(YEARS_OF_AGE(TO_DATE(BIRTHDAY, 'YYYY/MM/DD'))),0) AGE
		FROM
			T_CAST
		WHERE
			USER_SEQ		= pUSER_SEQ;

	REC_GAME_CHARACTER		CS_GAME_CHARACTER		%ROWTYPE;
	REC_GAME_REPORT_LOG		PKG_GAME_REPORT_LOG.CS02%ROWTYPE;
	REC_MAN_AGE				CS_MAN_AGE				%ROWTYPE;
	REC_CAST_AGE			CS_CAST_AGE				%ROWTYPE;

	BUF_AGE						T_DAILY_USER_AGE_REPORT.AGE	%TYPE;	-- 年齢
	BUF_REPORT_DAY				T_GAME_REPORT_LOG.REPORT_DAY%TYPE;	-- 報告日
	BUF_REPORT_MONTH			T_GAME_REPORT_LOG.REPORT_MONTH%TYPE;-- 報告月
	BUF_REPORT_HOUR				T_REAL_TIME_REPORT.REPORT_HOUR%TYPE;-- 報告時間
	BUF_INCREMENT_UNIQUE_DAY	NUMBER(1);
	BUF_INCREMENT_UNIQUE_MONTH	NUMBER(1);
BEGIN
	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS							:= APP_COMM.STATUS_NORMAL;
	
	BUF_REPORT_DAY					:= TO_CHAR(SYSDATE,'YYYY/MM/DD');
	BUF_REPORT_MONTH				:= TO_CHAR(SYSDATE,'YYYY/MM');
	BUF_REPORT_HOUR					:= TO_CHAR(SYSDATE,'HH24');
	BUF_INCREMENT_UNIQUE_DAY		:= 0;
	BUF_INCREMENT_UNIQUE_MONTH		:= 0;
	
	BUF_AGE							:= 0;

	OPEN CS_GAME_CHARACTER;
	FETCH CS_GAME_CHARACTER INTO REC_GAME_CHARACTER;
	IF CS_GAME_CHARACTER%NOTFOUND THEN
		RETURN;
	END IF;
	CLOSE CS_GAME_CHARACTER;

	/*------------------------------*/
	/* ｹﾞｰﾑﾚﾎﾟｰﾄ別集計ﾛｸﾞの更新		*/
	/*------------------------------*/
	BUF_INCREMENT_UNIQUE_DAY	:= 1;
	BUF_INCREMENT_UNIQUE_MONTH	:= 1;

	IF (pCHARGE_FLAG = 1) THEN

<< RETRY_GAME_REPORT_LOG >>

		OPEN PKG_GAME_REPORT_LOG.CS02(pSITE_CD,pUSER_SEQ,pUSER_CHAR_NO,BUF_ACTION);
		FETCH PKG_GAME_REPORT_LOG.CS02 INTO REC_GAME_REPORT_LOG;
		IF PKG_GAME_REPORT_LOG.CS02%NOTFOUND THEN		
			-- INSERT
			BEGIN
				INSERT INTO T_GAME_REPORT_LOG(
					SITE_CD			,
					USER_SEQ		,
					USER_CHAR_NO	,
					ACTION			,
					REPORT_DAY		,
					REPORT_MONTH
				)VALUES(
					pSITE_CD		,
					pUSER_SEQ		,
					pUSER_CHAR_NO	,
					BUF_ACTION		,
					BUF_REPORT_DAY	,
					BUF_REPORT_MONTH
				);
			EXCEPTION
				WHEN DUP_VAL_ON_INDEX THEN
					CLOSE PKG_GAME_REPORT_LOG.CS02;
					GOTO RETRY_GAME_REPORT_LOG;
			END;
		ELSE
			
			IF BUF_REPORT_DAY <= REC_GAME_REPORT_LOG.REPORT_DAY THEN
				BUF_INCREMENT_UNIQUE_DAY	:= 0;
			END IF;

			IF BUF_REPORT_MONTH <= REC_GAME_REPORT_LOG.REPORT_MONTH THEN
				BUF_INCREMENT_UNIQUE_MONTH	:= 0;
			END IF;

			-- UPDATE
			UPDATE T_GAME_REPORT_LOG SET 
				REPORT_DAY		= BUF_REPORT_DAY	,
				REPORT_MONTH	= BUF_REPORT_MONTH
			WHERE
				CURRENT OF PKG_GAME_REPORT_LOG.CS02;
		END IF;
		
		CLOSE PKG_GAME_REPORT_LOG.CS02;

		/*------------------------------*/
		/* 日別KPIﾚﾎﾟｰﾄの更新			*/
		/*------------------------------*/
		PKG_DAILY_KPI_REPORT.UPDATE_USED_POINT_STATUS(
			pSITE_CD					,
			BUF_REPORT_DAY				,
			REC_GAME_CHARACTER.SEX_CD	,
			pUSED_POINT					,
			1							,
			BUF_INCREMENT_UNIQUE_DAY
		);

		/*------------------------------*/
		/* 月別KPIﾚﾎﾟｰﾄの更新			*/
		/*------------------------------*/
		PKG_MONTHLY_KPI_REPORT.UPDATE_USED_POINT_STATUS(
			pSITE_CD					,
			BUF_REPORT_MONTH			,
			REC_GAME_CHARACTER.SEX_CD	,
			pUSED_POINT					,
			1							,
			BUF_INCREMENT_UNIQUE_MONTH
		);

		/*------------------------------*/
		/* 登録月別ﾎﾟｲﾝﾄﾚﾎﾟｰﾄの更新		*/
		/*------------------------------*/
		PKG_REGIST_MONTH_POINT_REPORT.UPDATE_USED_POINT_STATUS(
			pSITE_CD					,
			TO_CHAR(REC_GAME_CHARACTER.REGIST_DATE,'YYYY/MM'),
			BUF_REPORT_MONTH			,
			REC_GAME_CHARACTER.SEX_CD	,
			pUSED_POINT					,
			1							,
			BUF_INCREMENT_UNIQUE_MONTH
		);

		/*------------------------------*/
		/* 年齢別ﾚﾎﾟｰﾄの更新			*/
		/*------------------------------*/
		IF (REC_GAME_CHARACTER.SEX_CD	= VICOMM_CONST.MAN) THEN
			OPEN CS_MAN_AGE;
			FETCH CS_MAN_AGE INTO REC_MAN_AGE;
			IF CS_MAN_AGE%FOUND THEN
				BUF_AGE	:= REC_MAN_AGE.AGE;
			END IF;
			CLOSE CS_MAN_AGE;
		ELSE
			OPEN CS_CAST_AGE;
			FETCH CS_CAST_AGE INTO REC_CAST_AGE;
			IF CS_CAST_AGE%FOUND THEN
				BUF_AGE	:= REC_CAST_AGE.AGE;
			END IF;
			CLOSE CS_CAST_AGE;
		END IF;

		PKG_DAILY_USER_AGE_REPORT.UPDATE_USED_POINT_STATUS(
			pSITE_CD					,
			BUF_REPORT_DAY				,
			BUF_AGE						,
			REC_GAME_CHARACTER.SEX_CD	,
			pUSED_POINT					,
			1							,
			BUF_INCREMENT_UNIQUE_DAY
		);

		/*------------------------------*/
		/* ﾘｱﾙﾀｲﾑﾚﾎﾟｰﾄの更新			*/
		/*------------------------------*/
		PKG_REAL_TIME_REPORT.UPDATE_USED_POINT_STATUS(
			pSITE_CD								,
			BUF_REPORT_DAY							,
			BUF_REPORT_HOUR							,
			REC_GAME_CHARACTER.SEX_CD				,
			pUSED_POINT								,
			0
		);

	ELSE
		/*------------------------------*/
		/* ﾘｱﾙﾀｲﾑﾚﾎﾟｰﾄの更新			*/
		/*------------------------------*/
		PKG_REAL_TIME_REPORT.UPDATE_USED_POINT_STATUS(
			pSITE_CD								,
			BUF_REPORT_DAY							,
			BUF_REPORT_HOUR							,
			REC_GAME_CHARACTER.SEX_CD				,
			0										,
			pUSED_POINT
		);

	END IF;

	COMMIT;
	
EXCEPTION
	WHEN OTHERS THEN
		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);

		-- CLOSE CURSOR 
		PKG_GAME_REPORT_LOG.CLOSE_ALL;

		IF CS_GAME_CHARACTER%ISOPEN THEN
			CLOSE CS_GAME_CHARACTER;
		END IF;

		IF CS_MAN_AGE%ISOPEN THEN
			CLOSE CS_MAN_AGE;
		END IF;

		IF CS_CAST_AGE%ISOPEN THEN
			CLOSE CS_CAST_AGE;
		END IF;

		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END CREATE_GAME_USED_POINT;
/

SHOW ERROR;

