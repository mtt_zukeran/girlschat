/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: 共通管理
--  Title			: システム開始
--	Progaram ID		: STARTUP_SYSTEM
--	Compile Turn	: 0
--  Creation Date	: 08.10.21
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE STARTUP_SYSTEM(
	pWEB_SESSION_OWNER_NM	IN	VARCHAR2,
	pSTATUS					OUT	VARCHAR2
)IS
	--	CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'STARTUP_SYSTEM';

	BUF_RESTART_FLAG	T_RESTART.RESTART_FLAG%TYPE;
BEGIN
	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS	:= APP_COMM.STATUS_NORMAL;
	BUF_RESTART_FLAG	:= NULL;
	
	/*------------------------------*/
	/* Clear Session				*/
	/*------------------------------*/
	SELECT RESTART_FLAG INTO BUF_RESTART_FLAG FROM T_RESTART WHERE ROWNUM = 1;
	
	IF BUF_RESTART_FLAG = 1 THEN
		DELETE T_SESSION WHERE WEB_SESSION_OWNER_NM = PWEB_SESSION_OWNER_NM;
		UPDATE T_RESTART SET RESTART_FLAG = 0;
	END IF;
	
	COMMIT;

EXCEPTION
	WHEN OTHERS THEN

		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);

		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END STARTUP_SYSTEM;
/
SHOW ERROR;
