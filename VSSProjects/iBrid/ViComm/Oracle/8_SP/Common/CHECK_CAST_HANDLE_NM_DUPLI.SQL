/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: 共通
--  Title			: (女性)ハンドル名の重複ﾁｪｯｸ
--	Progaram ID		: CHECK_CAST_HANDLE_NM_DUPLI
--	Compile Turn	: 0
--  Creation Date	: 10.10.29
--	Update Date		:
--  Author			: K.Itoh
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE CHECK_CAST_HANDLE_NM_DUPLI(
	pSITE_CD		IN	VARCHAR2	,
	pUSER_SEQ		IN	VARCHAR2	,
	pUSER_CHAR_NO	IN	VARCHAR2	,
	pHANDLE_NM		IN	VARCHAR2	,	--NEW_HANDLE_NM
	pDUPLI_FLAG 	OUT	NUMBER		,
	pSTATUS			OUT	VARCHAR2
) IS
	--	CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'CHECK_CAST_HANDLE_NM_DUPLI';

	--女性は登録時にﾊﾝﾄﾞﾙﾈｰﾑの入力はしないので、T_TEMP_REGIST内の検索はしない

	--利用が可能な会員から重複ﾁｪｯｸ
	CURSOR CS_CHK_HANDLE_NM_CHAR02 IS
		SELECT
			USER_SEQ	,
			USER_CHAR_NO
		FROM
			VW_CAST_CHARACTER01
		WHERE
			SITE_CD				=	pSITE_CD						AND
			HANDLE_NM			=	pHANDLE_NM						AND
			USER_STATUS			=	VICOMM_CONST.USER_WOMAN_NORMAL	AND
			HANDLE_NM_RESETTING_FLAG= 0;
--	

	BUF_USER_SEQ		T_USER.USER_SEQ%TYPE;
	BUF_USER_CHAR_NO	T_CAST_CHARACTER.USER_CHAR_NO%TYPE;
BEGIN
	pSTATUS			:= APP_COMM.STATUS_NORMAL;
	pDUPLI_FLAG		:= 0;
	
	IF (pUSER_SEQ IS NULL) THEN
		BUF_USER_SEQ	:= -1;
		BUF_USER_CHAR_NO:= VICOMM_CONST.MAIN_CHAR;
	ELSE
		BUF_USER_SEQ	:= pUSER_SEQ;
		BUF_USER_CHAR_NO:= pUSER_CHAR_NO;
	END IF;

	IF (PKG_MANAGE_COMPANY.IS_AVAILABLE_SERVICE(PKG_MANAGE_COMPANY.RELEASE_CHECK_HANDLE_NM_DUPLI)) THEN
		-- ﾊﾝﾄﾞﾙ名の重複ﾁｪｯｸ(USER)
		-- 利用可会員のみ
		FOR REC_CHK_HANDLE_NM_CHAR02 IN CS_CHK_HANDLE_NM_CHAR02 LOOP
			IF ( (REC_CHK_HANDLE_NM_CHAR02.USER_SEQ != BUF_USER_SEQ) OR (REC_CHK_HANDLE_NM_CHAR02.USER_CHAR_NO != BUF_USER_CHAR_NO) ) THEN
				pDUPLI_FLAG := 1;
			END IF;
		END LOOP;
	END IF;	
		
EXCEPTION
	WHEN OTHERS THEN
		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);
		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);
		
		PKG_MANAGE_COMPANY.CLOSE_ALL;
	
		IF CS_CHK_HANDLE_NM_CHAR02%ISOPEN THEN
			CLOSE CS_CHK_HANDLE_NM_CHAR02;
		END IF;	

END CHECK_CAST_HANDLE_NM_DUPLI;
/
SHOW ERROR;
