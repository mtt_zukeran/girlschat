/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: バックグランド
--  Title			: バッチ制御(月次処理)
--	Progaram ID		: MONTHLY_BATCH_CONTROL
--	Compile Turn	: 0
--  Creation Date	: 10.08.23
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/

-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE MONTHLY_BATCH_CONTROL
IS
	-- CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'MONTHLY_BATCH_CONTROL';

	CURSOR CS_SITE IS SELECT SITE_CD FROM T_SITE WHERE
		SITE_CD != VICOMM_CONST.CAST_SITE_CD;
	BUF_STATUS	VARCHAR2(10);

BEGIN

	APP_COMM.TRACE(APP_COMM.TRACE_INFO,PGM_NM || ' START');

	MONTHLY_MAN_RICHINO_RANK(BUF_STATUS);

	RESET_MAN_MONTHLY_SALES;
	
	FOR REC_SITE IN CS_SITE LOOP
		PKG_QUICK_RES_REPORT.UPDATE_MAX_IN_COUNT(REC_SITE.SITE_CD);
	END LOOP;

	APP_COMM.TRACE(APP_COMM.TRACE_INFO,PGM_NM || ' END');

EXCEPTION
	WHEN OTHERS THEN

	LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);

		BUF_STATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END MONTHLY_BATCH_CONTROL;
/
SHOW ERROR;
