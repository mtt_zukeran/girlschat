/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: お宝月間ランキング作成
--  Title			: バッチ制御
--	Progaram ID		: CREATE_OBJ_REVIEW_MONTHLY
--	Compile Turn	: 0
--  Creation Date	: 12.06.02
--	Update Date		:
--  Author			: PW K.Miyazato
/*************************************************************************/
CREATE OR REPLACE PROCEDURE CREATE_OBJ_REVIEW_MONTHLY(
	pSITE_CD	IN	VARCHAR2,
	pSTATUS		OUT	VARCHAR2
)IS
	-- CONSTANT
	PGM_NM	CONSTANT VARCHAR(255)	:= 'CREATE_OBJ_REVIEW_MONTHLY';

BEGIN
	/*------------------------------*/
	/* Initilize					*/
	/*------------------------------*/
	pSTATUS := APP_COMM.STATUS_NORMAL;

	DELETE
		T_OBJ_REVIEW_MONTHLY
	WHERE
		SITE_CD = pSITE_CD
	;

	INSERT INTO
		T_OBJ_REVIEW_MONTHLY(SITE_CD,OBJ_SEQ,GOOD_POINT)
	SELECT
		SITE_CD,
		OBJ_SEQ,
		SUM(GOOD_POINT) AS GOOD_POINT
	FROM
		VW_PW_OBJ_REVIEW_HISTORY02
	WHERE
		SITE_CD = pSITE_CD AND
		CREATE_DATE >= SYSDATE-30
	GROUP BY
		SITE_CD,
		OBJ_SEQ
	;
		
	COMMIT;
		
EXCEPTION
	WHEN OTHERS THEN

		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);

		pSTATUS := APP_COMM.DISPATCH_ERROR(SQLCODE);

END CREATE_OBJ_REVIEW_MONTHLY;
/
SHOW ERROR;