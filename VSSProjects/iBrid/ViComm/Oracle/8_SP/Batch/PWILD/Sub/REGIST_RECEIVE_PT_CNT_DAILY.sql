/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: 日別出演者獲得報酬集計
--  Title			: バッチ制御
--	Progaram ID		: REGIST_RECEIVE_PT_CNT_DAILY
--	Compile Turn	: 0
--  Creation Date	: 17.03.02
--	Update Date		:
--  Author			: M&TT Zukeran
/*************************************************************************/
CREATE OR REPLACE PROCEDURE REGIST_RECEIVE_PT_CNT_DAILY(
	pSITE_CD			IN	VARCHAR2, --サイトCD
	pEXEC_DATE			IN	VARCHAR2 DEFAULT NULL --集計実行日（集計日は前日。日付形式：YYYY-MM-DD）
)IS
	-- CONSTANT
	PGM_NM 	CONSTANT VARCHAR(255)	:= 'REGIST_RECEIVE_PT_CNT_DAILY';

	--集計日前日の獲得ポイント集計用
	CURSOR CS_RECEIVE_POINT(
		prmEXEC_DATE	DATE
	) IS
		SELECT
			P.SITE_CD
			,P.MAN_USER_SEQ
			,P.CAST_USER_SEQ
			,SUM(P.PAYMENT_POINT) AS RECEIVE_POINT
			,(
				SELECT LOGIN_ID
				FROM T_USER
				WHERE USER_SEQ=P.MAN_USER_SEQ
			) AS MAN_LOGIN_ID
			,(
				SELECT LOGIN_ID
				FROM T_USER
				WHERE USER_SEQ=P.CAST_USER_SEQ
			) AS CAST_LOGIN_ID
			,(
				SELECT MIN(FIRST_RECEIVE_DATE)
				FROM T_DAILY_RECEIVE_PT_CNT
				WHERE SITE_CD=P.SITE_CD
					AND MAN_USER_SEQ=P.MAN_USER_SEQ
					AND CAST_USER_SEQ=P.CAST_USER_SEQ
			) AS FIRST_RECEIVE_DATE
		FROM
			(
				SELECT
					MAN_USER_SITE_CD AS SITE_CD
					,MAN_USER_SEQ
					,CAST_USER_SEQ
					,SUM(PAYMENT_POINT) AS PAYMENT_POINT
				FROM
					T_WEB_USED_LOG
				WHERE
					MAN_USER_SITE_CD=pSITE_CD
					AND CHARGE_START_DATE >= prmEXEC_DATE - 1
					AND CHARGE_START_DATE < prmEXEC_DATE
					AND CHARGE_TYPE IN (
						VICOMM_CONST.CHARGE_TX_MAIL
						,VICOMM_CONST.CHARGE_TX_MAIL_WITH_PIC
						,VICOMM_CONST.CHARGE_TX_MAIL_WITH_MOVIE
					)
					AND CHARGE_POINT > 0
				GROUP BY
					MAN_USER_SITE_CD
					,MAN_USER_SEQ
					,CAST_USER_SEQ
			UNION ALL
				SELECT
					IR.MAN_USER_SITE_CD AS SITE_CD
					,IR.MAN_USER_SEQ
					,IR.CAST_USER_SEQ
					,SUM(UL.PAYMENT_POINT) AS PAYMENT_POINT
				FROM
					T_USED_LOG UL
					INNER JOIN T_IVP_REQUEST IR
						ON UL.IVP_REQUEST_SEQ=IR.IVP_REQUEST_SEQ
				WHERE
					IR.MAN_USER_SITE_CD=pSITE_CD
					AND UL.CHARGE_END_DATE >= prmEXEC_DATE - 1
					AND UL.CHARGE_END_DATE < prmEXEC_DATE
					AND UL.CHARGE_TYPE IN (
						VICOMM_CONST.CHARGE_TALK_PUBLIC
						,VICOMM_CONST.CHARGE_TALK_WSHOT
						,VICOMM_CONST.CHARGE_TALK_VOICE_WSHOT
						,VICOMM_CONST.CHARGE_VIEW_LIVE
						,VICOMM_CONST.CHARGE_VIEW_TALK
						,VICOMM_CONST.CHARGE_VIEW_ONLINE
						,VICOMM_CONST.CHARGE_WIRETAPPING
						,VICOMM_CONST.CHARGE_TALK_VOICE_PUBLIC
						,VICOMM_CONST.CHARGE_CAST_TALK_PUBLIC
						,VICOMM_CONST.CHARGE_CAST_TALK_WSHOT
						,VICOMM_CONST.CHARGE_CAST_TALK_VOICE_WSHOT
						,VICOMM_CONST.CHARGE_CAST_TALK_VOICE_PUBLIC
					)
					AND UL.CHARGE_POINT > 0
				GROUP BY
					IR.MAN_USER_SITE_CD
					,IR.MAN_USER_SEQ
					,IR.CAST_USER_SEQ
			) P
		GROUP BY
			P.SITE_CD
			,P.MAN_USER_SEQ
			,P.CAST_USER_SEQ
	;

	--やりとり開始日の検索用
	CURSOR CS_FIRST_RECEIVE_DATE(
		prmMAN_USER_SEQ	T_WEB_USED_LOG.MAN_USER_SEQ%TYPE
		,prmCAST_USER_SEQ	T_WEB_USED_LOG.CAST_USER_SEQ%TYPE
	) IS
		SELECT
			MIN(P.CHARGE_DATE) AS FIRST_RECEIVE_DATE
		FROM
			(
				SELECT
					MIN(CHARGE_START_DATE) AS CHARGE_DATE
				FROM
					T_WEB_USED_LOG
				WHERE
					MAN_USER_SITE_CD=pSITE_CD
					AND MAN_USER_SEQ=prmMAN_USER_SEQ
					AND CAST_USER_SEQ=prmCAST_USER_SEQ
					AND CHARGE_TYPE IN (
						VICOMM_CONST.CHARGE_TX_MAIL
						,VICOMM_CONST.CHARGE_TX_MAIL_WITH_PIC
						,VICOMM_CONST.CHARGE_TX_MAIL_WITH_MOVIE
					)
					AND CHARGE_POINT > 0
			UNION ALL
				SELECT
					MIN(UL.CHARGE_END_DATE) AS CHARGE_DATE
				FROM
					T_USED_LOG UL
					INNER JOIN T_IVP_REQUEST IR
						ON UL.IVP_REQUEST_SEQ=IR.IVP_REQUEST_SEQ
				WHERE
					IR.MAN_USER_SITE_CD=pSITE_CD
					AND IR.MAN_USER_SEQ=prmMAN_USER_SEQ
					AND IR.CAST_USER_SEQ=prmCAST_USER_SEQ
					AND UL.CHARGE_TYPE IN (
						VICOMM_CONST.CHARGE_TALK_PUBLIC
						,VICOMM_CONST.CHARGE_TALK_WSHOT
						,VICOMM_CONST.CHARGE_TALK_VOICE_WSHOT
						,VICOMM_CONST.CHARGE_VIEW_LIVE
						,VICOMM_CONST.CHARGE_VIEW_TALK
						,VICOMM_CONST.CHARGE_VIEW_ONLINE
						,VICOMM_CONST.CHARGE_WIRETAPPING
						,VICOMM_CONST.CHARGE_TALK_VOICE_PUBLIC
						,VICOMM_CONST.CHARGE_CAST_TALK_PUBLIC
						,VICOMM_CONST.CHARGE_CAST_TALK_WSHOT
						,VICOMM_CONST.CHARGE_CAST_TALK_VOICE_WSHOT
						,VICOMM_CONST.CHARGE_CAST_TALK_VOICE_PUBLIC
					)
					AND UL.CHARGE_POINT > 0
			) P
	;

	REC_RECEIVE_POINT			CS_RECEIVE_POINT%ROWTYPE;

	BUF_DAYS				NUMBER;	--やりとり開始日からの経過日数（最小値を１とする）
	BUF_DAYS_DISP_TERM		NUMBER;	--期間（表示用）
	BUF_FIRST_RECEIVE_DATE	DATE;	--やりとり開始日
	BUF_EXEC_DATE			DATE;	--集計実行日
	BUF_REPORT_DATE			DATE;	--集計日

BEGIN
	APP_COMM.TRACE(APP_COMM.TRACE_INFO,PGM_NM || ' START');

	--集計実行日を設定
	IF (pEXEC_DATE IS NULL) THEN
		BUF_EXEC_DATE := TRUNC(SYSDATE);
	ELSE
		BUF_EXEC_DATE := TO_DATE(pEXEC_DATE,'YYYY-MM-DD');
	END IF;

	--集計日を設定
	BUF_REPORT_DATE := BUF_EXEC_DATE - 1;

	--ポイントを消費して出演者とやりとりしたデータを集計
	FOR REC_RECEIVE_POINT IN CS_RECEIVE_POINT(BUF_EXEC_DATE) LOOP
		--過去の集計データに一致する出演者-会員の組み合わせが存在しない場合
		IF (REC_RECEIVE_POINT.FIRST_RECEIVE_DATE IS NULL) THEN
			--ポイント利用履歴からやりとり開始日を検索
			OPEN CS_FIRST_RECEIVE_DATE(REC_RECEIVE_POINT.MAN_USER_SEQ,REC_RECEIVE_POINT.CAST_USER_SEQ);
			FETCH CS_FIRST_RECEIVE_DATE INTO BUF_FIRST_RECEIVE_DATE;
			CLOSE CS_FIRST_RECEIVE_DATE;
		--過去の集計データに存在する場合
		ELSE
			--集計データからやりとり開始日を取得
			BUF_FIRST_RECEIVE_DATE := REC_RECEIVE_POINT.FIRST_RECEIVE_DATE;
		END IF;

		--やりとり開始日からの経過日数を算出
		BUF_DAYS := BUF_EXEC_DATE - TRUNC(BUF_FIRST_RECEIVE_DATE);

		--経過日数の表示用期間を算出
		IF (BUF_DAYS < 8) THEN
			--1日単位
			BUF_DAYS_DISP_TERM := BUF_DAYS;
		ELSIF (BUF_DAYS < 22) THEN
			--1週間単位
			BUF_DAYS_DISP_TERM := CEIL(BUF_DAYS / 7)*7;
		ELSE
			--30日単位
			BUF_DAYS_DISP_TERM := CEIL(BUF_DAYS / 30)*30;
		END IF;

		--集計データ登録
		INSERT INTO T_DAILY_RECEIVE_PT_CNT(
			SITE_CD
			,REPORT_DATE
			,REPORT_DAY
			,CAST_USER_SEQ
			,CAST_LOGIN_ID
			,MAN_USER_SEQ
			,MAN_LOGIN_ID
			,RECEIVE_POINT
			,FIRST_RECEIVE_DATE
			,ELAPSED_DAYS
			,ELAPSED_DAYS_DISP_TERM
		) VALUES (
			pSITE_CD
			,BUF_REPORT_DATE
			,TO_CHAR(BUF_REPORT_DATE,'DD')
			,REC_RECEIVE_POINT.CAST_USER_SEQ
			,REC_RECEIVE_POINT.CAST_LOGIN_ID
			,REC_RECEIVE_POINT.MAN_USER_SEQ
			,REC_RECEIVE_POINT.MAN_LOGIN_ID
			,REC_RECEIVE_POINT.RECEIVE_POINT
			,BUF_FIRST_RECEIVE_DATE
			,BUF_DAYS
			,BUF_DAYS_DISP_TERM
		);

		COMMIT;
	END LOOP;

	IF (CS_RECEIVE_POINT%ISOPEN) THEN
		CLOSE CS_RECEIVE_POINT;
	END IF;

	IF (CS_FIRST_RECEIVE_DATE%ISOPEN) THEN
		CLOSE CS_FIRST_RECEIVE_DATE;
	END IF;

	COMMIT;

	APP_COMM.TRACE(APP_COMM.TRACE_INFO,PGM_NM || ' END');

EXCEPTION
	WHEN OTHERS THEN
		LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,SQLERRM);

		IF (CS_RECEIVE_POINT%ISOPEN) THEN
			CLOSE CS_RECEIVE_POINT;
		END IF;

		IF (CS_FIRST_RECEIVE_DATE%ISOPEN) THEN
			CLOSE CS_FIRST_RECEIVE_DATE;
		END IF;

END REGIST_RECEIVE_PT_CNT_DAILY;
/
SHOW ERROR;
