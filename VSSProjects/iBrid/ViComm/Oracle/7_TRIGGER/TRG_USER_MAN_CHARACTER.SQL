/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: システム管理
--  Title			: 男性会員ｷｬﾗｸﾀｰ トリガ
--	Progaram ID		: TRG_USER_MAN_CHARACTER Trigger
--	Compile Turn	: 0
--  Creation Date	: 11.04.14
--	Update Date		:
--  Author			: K.Itoh
/*************************************************************************/
CREATE OR REPLACE TRIGGER TRG_USER_MAN_CHARACTER AFTER INSERT ON T_USER_MAN_CHARACTER FOR EACH ROW
DECLARE
BEGIN
	INSERT INTO T_USER_MAN_CHARACTER_EX (
		SITE_CD				,
		USER_SEQ            ,
		USER_CHAR_NO        ,
		BLOG_MAIL_RX_TYPE
	) VALUES (
		:NEW.SITE_CD		,
		:NEW.USER_SEQ		,
		:NEW.USER_CHAR_NO	,
		'1'
	);

EXCEPTION
	WHEN OTHERS THEN
		APP_COMM.TRACE(APP_COMM.TRACE_INFO,'TRG_USER_MAN_CHARACTER ' || SQLERRM);

END TRG_USER_MAN_CHARACTER;
/
SHOW ERROR
