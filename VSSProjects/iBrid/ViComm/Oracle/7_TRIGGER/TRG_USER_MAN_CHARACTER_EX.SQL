/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: システム管理
--  Title			: 男性会員ｷｬﾗｸﾀｰ拡張 ﾄﾘｶﾞ
--	Progaram ID		: TRG_USER_MAN_CHARACTER_EX Trigger
--	Compile Turn	: 0
--  Creation Date	: 11.05.11
--	Update Date		:
--  Author			: K.Itoh
/*************************************************************************/
CREATE OR REPLACE TRIGGER TRG_USER_MAN_CHARACTER_EX AFTER UPDATE ON T_USER_MAN_CHARACTER_EX FOR EACH ROW
DECLARE
BEGIN
	UPDATE T_USER_MAN_CHARACTER SET
		RICHINO_RANK   			= :NEW.RICHINO_RANK
	WHERE
		T_USER_MAN_CHARACTER.SITE_CD 		= :NEW.SITE_CD 		AND
		T_USER_MAN_CHARACTER.USER_SEQ 		= :NEW.USER_SEQ 	AND
		T_USER_MAN_CHARACTER.USER_CHAR_NO 	= :NEW.USER_CHAR_NO 
	;	
EXCEPTION
	WHEN OTHERS THEN
		APP_COMM.TRACE(APP_COMM.TRACE_INFO,'TRG_USER_MAN_CHARACTER_EX ' || SQLERRM);

END TRG_USER_MAN_CHARACTER_EX;
/
SHOW ERROR
