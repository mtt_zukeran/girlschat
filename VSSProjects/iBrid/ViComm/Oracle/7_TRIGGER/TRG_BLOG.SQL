/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: システム管理
--  Title			: 男性会員ｷｬﾗｸﾀｰ拡張 ﾄﾘｶﾞ
--	Progaram ID		: TRG_BLOG Trigger
--	Compile Turn	: 0
--  Creation Date	: 11.05.11
--	Update Date		:
--  Author			: K.Itoh
/*************************************************************************/
CREATE OR REPLACE TRIGGER TRG_BLOG AFTER INSERT OR UPDATE ON T_BLOG FOR EACH ROW
DECLARE
BEGIN
		UPDATE T_CAST_CHARACTER SET
			BLOG_SEQ            = :NEW.BLOG_SEQ        ,
			BLOG_TITLE          = :NEW.BLOG_TITLE      ,
			BLOG_DOC            = :NEW.BLOG_DOC        ,
			BLOG_REGIST_DATE    = :NEW.REGIST_DATE		
		WHERE
			SITE_CD 		= :NEW.SITE_CD 				AND
			USER_SEQ 		= :NEW.USER_SEQ				AND
			USER_CHAR_NO 	= :NEW.USER_CHAR_NO
		;EXCEPTION
	WHEN OTHERS THEN
		APP_COMM.TRACE(APP_COMM.TRACE_INFO,'TRG_BLOG ' || SQLERRM);

END TRG_BLOG;
/
SHOW ERROR
