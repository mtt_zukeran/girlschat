SET DEFINE OFF;

INSERT INTO T_SITE_SETTLE(SITE_CD,SETTLE_COMPANY_CD,SETTLE_TYPE)VALUES('A001','02','6');
INSERT INTO T_SITE_SETTLE(SITE_CD,SETTLE_COMPANY_CD,SETTLE_TYPE)VALUES('A001','03','4');
INSERT INTO T_SITE_SETTLE(SITE_CD,SETTLE_COMPANY_CD,SETTLE_TYPE)VALUES('A001','04','5');
INSERT INTO T_SITE_SETTLE(SITE_CD,SETTLE_COMPANY_CD,SETTLE_TYPE)VALUES('A001','07','3');
INSERT INTO T_SITE_SETTLE(SITE_CD,SETTLE_COMPANY_CD,SETTLE_TYPE)VALUES('A001','08','D');
INSERT INTO T_SITE_SETTLE(SITE_CD,SETTLE_COMPANY_CD,SETTLE_TYPE)VALUES('A001','02','G');
INSERT INTO T_SITE_SETTLE(SITE_CD,SETTLE_COMPANY_CD,SETTLE_TYPE)VALUES('A001','09','H');
INSERT INTO T_SITE_SETTLE(SITE_CD,SETTLE_COMPANY_CD,SETTLE_TYPE)VALUES('A001','10','3');
INSERT INTO T_SITE_SETTLE(SITE_CD,SETTLE_COMPANY_CD,SETTLE_TYPE)VALUES('A001','11','3');
INSERT INTO T_SITE_SETTLE(SITE_CD,SETTLE_COMPANY_CD,SETTLE_TYPE)VALUES('A001','12','I');
INSERT INTO T_SITE_SETTLE(SITE_CD,SETTLE_COMPANY_CD,SETTLE_TYPE)VALUES('A001','12','J');
INSERT INTO T_SITE_SETTLE(SITE_CD,SETTLE_COMPANY_CD,SETTLE_TYPE)VALUES('A001','14','M');
INSERT INTO T_SITE_SETTLE(SITE_CD,SETTLE_COMPANY_CD,SETTLE_TYPE)VALUES('A001','15','O');
INSERT INTO T_SITE_SETTLE(SITE_CD,SETTLE_COMPANY_CD,SETTLE_TYPE)VALUES('A001','16','P');

INSERT INTO T_SITE_SETTLE(SITE_CD,SETTLE_COMPANY_CD,SETTLE_TYPE)VALUES('A002','02','6');
INSERT INTO T_SITE_SETTLE(SITE_CD,SETTLE_COMPANY_CD,SETTLE_TYPE)VALUES('A002','03','4');
INSERT INTO T_SITE_SETTLE(SITE_CD,SETTLE_COMPANY_CD,SETTLE_TYPE)VALUES('A002','04','5');
INSERT INTO T_SITE_SETTLE(SITE_CD,SETTLE_COMPANY_CD,SETTLE_TYPE)VALUES('A002','07','3');
INSERT INTO T_SITE_SETTLE(SITE_CD,SETTLE_COMPANY_CD,SETTLE_TYPE)VALUES('A002','08','D');
INSERT INTO T_SITE_SETTLE(SITE_CD,SETTLE_COMPANY_CD,SETTLE_TYPE)VALUES('A002','02','G');
INSERT INTO T_SITE_SETTLE(SITE_CD,SETTLE_COMPANY_CD,SETTLE_TYPE)VALUES('A002','09','H');

-- S-MONEY
UPDATE T_SITE_SETTLE SET
	CP_ID_NO			= 'CP-DUMMY'		,
	SETTLE_URL			= 'https://do.security-m.jp/sm/smoney/gmm/payment_m.php?cid={0}&cc={1}&sid={2}&ch={3}&u2={4}&c1={5}',
	CONTINUE_SETTLE_URL	= '',
	BACK_URL			= '{0}/user/start.aspx?loginid={1}&password={2}'
WHERE
	SETTLE_COMPANY_CD = '02' AND SETTLE_TYPE = '6';

-- C-CHECK
UPDATE T_SITE_SETTLE SET
	CP_ID_NO			= 'CP-DUMMY'		,
	SETTLE_URL			= 'https://www.digitalcheck.jp/settle/settle3/bp3.dll?IP={0}&SID={1}&N1={2}&K1={3}&KAKUTEI=1&STORE=11&FUKA={4}&RT={5}&OKURL={6}',
	CONTINUE_SETTLE_URL	= '',
	BACK_URL			= '{0}/user/start.aspx?loginid={1}&password={2}'
WHERE
	SETTLE_COMPANY_CD = '03';

-- BITCASH
UPDATE T_SITE_SETTLE SET
	CP_ID_NO			= 'CP-DUMMY'		,
	CP_PASSWORD			= 'CP-DUMMY'		,
	SETTLE_URL			= 'https://ssl.bitcash.co.jp/api/',
	CONTINUE_SETTLE_URL	= '',
	BACK_URL			= ''
WHERE
	SETTLE_COMPANY_CD = '04';

-- TELECOM CREDIT
UPDATE T_SITE_SETTLE SET
	CP_ID_NO			= 'CP-DUMMY'	,
	SETTLE_URL			= 'https://www.credit-cgiserver.com/inetcredit/secure/order.pl?clientip={0}&email={1}&sendid={2}&sendpoint={3}&money={4}&redirect_back_url={5}&i=on',
	CONTINUE_SETTLE_URL	= 'http://www.credit-cgiserver.com/inetcredit/secure/one-click-order.pl?clientip={0}&email={1}&sendid={2}&sendpoint={3}&money={4}&redirect_back_url={5}&i=on&usrtel={6}',
	BACK_URL			= '{0}/user/start.aspx?loginid={1}&password={2}'
WHERE
	SETTLE_COMPANY_CD = '07';

-- OFFICIAL CHECK
UPDATE T_SITE_SETTLE SET
	CP_ID_NO			= 'CP-DUMMY'		,
	CP_PASSWORD			= ''				,
	SETTLE_URL			= 'http://official-affiri.com/?s={0}&u={1}',
	CONTINUE_SETTLE_URL	= '',
	BACK_URL			= ''
WHERE
	SETTLE_COMPANY_CD = '08';

-- G-MONEY
UPDATE T_SITE_SETTLE SET
	CP_ID_NO			= 'CP-DUMMY'		,
	CP_PASSWORD			= 'CONTENTS-CD'		,
	SETTLE_URL			= 'https://user.mng.g-money.ne.jp/gmm/gmoney/charge_m.php?cid={0}&cc={1}&cd={2}&ch={3}&u2={4}&c1={5}&c2={6}&c3=NONE',
	CONTINUE_SETTLE_URL	= '',
	BACK_URL			= '{0}/user/start.aspx?loginid={1}&password={2}'
WHERE
	SETTLE_COMPANY_CD = '02' AND SETTLE_TYPE = 'G';

-- PoiCha
UPDATE T_SITE_SETTLE SET
	CP_ID_NO			= 'CP-DUMMY'		,
	CP_PASSWORD			= ''				,
	SETTLE_URL			= 'http://c00x.poicha.jp/?f={0}&u={1}&guid=ON',
	CONTINUE_SETTLE_URL	= '',
	BACK_URL			= ''
WHERE
	SETTLE_COMPANY_CD = '09';

-- ZERO CREDIT
UPDATE T_SITE_SETTLE SET
	CP_ID_NO			= 'CP-DUMMY'	,
	SETTLE_URL			= 'https://credit.zeroweb.ne.jp/cgi-bin/order.cgi?clientip={0}&usrmail={1}&sendid={2}&sendpoint={3}&money={4}&usrtel={5}&siteurl={6}&act=imode',
	CONTINUE_SETTLE_URL	= 'https://credit.zeroweb.ne.jp/cgi-bin/secure.cgi?clientip={0}&email={1}&sendid={2}&sendpoint={3}&money={4}&telno={5}&send=cardsv&cardnumber=8888888888888881&expyy=01&expmm=01&entry=non&pubsec=yes',
	BACK_URL			= '{0}/user/start.aspx?loginid={1}&password={2}'
WHERE
	SETTLE_COMPANY_CD = '10';

-- DIGICA CREDIT
UPDATE T_SITE_SETTLE SET
	CP_ID_NO			= 'CP-DUMMY'	,
	SETTLE_URL			= 'https://pay.digica-online.com/site/mobile/site_credit_mobile.php?clientip={0}&email={1}&sendid={2}&sendpoint={3}&money={4}&backurl={5}',
	CONTINUE_SETTLE_URL	= 'https://pay.digica-online.com/site/mobile/site_credit_mobile_quick.php?clientip={0}&email={1}&sendid={2}&sendpoint={3}&money={4}&telno={5}&cardnumber=8888888888888881&pubsec=yes',
	BACK_URL			= '{0}/user/start.aspx?loginid={1}&password={2}'
WHERE
	SETTLE_COMPANY_CD = '11';

-- SAKAZUKI
UPDATE T_SITE_SETTLE SET
	CP_ID_NO			= '',
	CP_PASSWORD			= '',
	SETTLE_URL			= 'http://ad.sakazuki-af.jp/cl/click.php?b_id={0}&t_id={1}&user_id={2}',
	CONTINUE_SETTLE_URL	= '',
	BACK_URL			= ''
WHERE
	SETTLE_COMPANY_CD = '12' AND SETTLE_TYPE = 'I';

-- SAKAZUKI(����ڰ�)
UPDATE T_SITE_SETTLE SET
	CP_ID_NO			= '',
	CP_PASSWORD			= '',
	SETTLE_URL			= 'http://ad.sakazuki-af.jp/tmpl/view.php?m_id={0}&tmp_id={1}&u_id={2}&t_id={3}&ct_id={4}&pt_min={5}&pt_max={6}&order={7}&limit={8}&page={9}&check={10}&cost_max={11}&cost_min={12}&guid=ON',
	CONTINUE_SETTLE_URL	= '',
	BACK_URL			= ''
WHERE
	SETTLE_COMPANY_CD = '12' AND SETTLE_TYPE = 'J';

-- PBK�E�߲�ı�ش��
UPDATE T_SITE_SETTLE SET
	CP_ID_NO			= '',
	CP_PASSWORD			= '',
	SETTLE_URL			= 'http://pbk.jp/cl/click.php?b_id={0}&t_id={1}&user_id={2}',
	CONTINUE_SETTLE_URL	= '',
	BACK_URL			= ''
WHERE
	SETTLE_COMPANY_CD = '15' AND SETTLE_TYPE = 'O';


-- GIGA-POINT
UPDATE T_SITE_SETTLE SET
	CP_ID_NO			= 'CP-DUMMY'		,
	SETTLE_URL			= 'site_code={0}&mall_order_no={1}&member_id={2}&member_pw={3}&mail_addr={4}&settle_amount={5}&point={6}&return_url={7}',
	CONTINUE_SETTLE_URL	= '',
	BACK_URL			= '{0}/user/start.aspx?loginid={1}&password={2}'
WHERE
	SETTLE_COMPANY_CD = '14' AND SETTLE_TYPE = 'M';
	
-- �y�V
UPDATE T_SITE_SETTLE SET
	CP_ID_NO			= '0123456789',
	CP_PASSWORD			= '01234567890123456',
	SETTLE_URL			= 'http://sub.maqia.jp/RakutenStub/myc_m/stepin/dl_1_0.aspx',
	CONTINUE_SETTLE_URL	= '',
	BACK_URL			= '{0}/user/start.aspx?loginid={1}&password={2}'
WHERE
	SETTLE_COMPANY_CD = '16' AND SETTLE_TYPE = 'P';
