/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: 
--  Title			: 添付ファイル削除対象の一覧をファイル出力
--	Progaram ID		: PKG_OUTPUT_DEL_FILE_LIST
--	Compile Turn	: 0
--  Creation Date	: 17.05.16
--	Update Date		:
--  Author			: Yogi
/*************************************************************************/
CREATE OR REPLACE PACKAGE PKG_OUTPUT_DEL_FILE_LIST AS

    --削除対象ファイル種類定数
    DELETE_FILE_TYPE_MAIL		VARCHAR2(1) := '0';	-- mail
    DELETE_FILE_TYPE_DIARY	    VARCHAR2(1) := '1';	-- 日記
    DELETE_FILE_TYPE_TWEET		VARCHAR2(1) := '2';	-- つぶやき
    
    --メール添付ファイル削除対象データを取得
    CURSOR CS_MAIL
    (
			prmSITE_CD		T_MAIL_LOG.TX_SITE_CD%TYPE,
			prmCREATE_DATE  T_MAIL_LOG.CREATE_DATE%TYPE
    ) 
    IS 
        SELECT
            T_REQ_TX_MAIL.PIC_SEQ
            ,T_REQ_TX_MAIL.MOVIE_SEQ
            ,T_MAIL_LOG.TX_LOGIN_ID
            ,T_REQ_TX_MAIL.ATTACHED_OBJ_TYPE
            ,T_MAIL_LOG.TX_SEX_CD
            ,T_REQ_TX_MAIL.TX_USER_IMG_PATH
            ,to_char(T_MAIL_LOG.CREATE_DATE,'YYYY-MM-DD') as MDATE
        FROM
            T_MAIL_LOG
            ,T_REQ_TX_MAIL
        WHERE
            T_MAIL_LOG.TX_SITE_CD = prmSITE_CD
            AND T_MAIL_LOG.TX_MAIL_BOX_TYPE = '1'
            AND T_MAIL_LOG.CREATE_DATE <= prmCREATE_DATE
            AND T_MAIL_LOG.DEL_PROTECT_FLAG = 0
            AND (T_REQ_TX_MAIL.ATTACHED_OBJ_TYPE = 1 OR T_REQ_TX_MAIL.ATTACHED_OBJ_TYPE = 2)
            AND (T_REQ_TX_MAIL.MOVIE_SEQ is not null OR T_REQ_TX_MAIL.PIC_SEQ is not null)
            AND T_REQ_TX_MAIL.REQUEST_TX_MAIL_SEQ = T_MAIL_LOG.REQUEST_TX_MAIL_SEQ(+)
        GROUP BY 
            T_REQ_TX_MAIL.PIC_SEQ
            ,T_REQ_TX_MAIL.MOVIE_SEQ
            ,T_MAIL_LOG.TX_LOGIN_ID
            ,T_REQ_TX_MAIL.ATTACHED_OBJ_TYPE
            ,T_MAIL_LOG.TX_SEX_CD
            ,T_REQ_TX_MAIL.TX_USER_IMG_PATH
            ,to_char(T_MAIL_LOG.CREATE_DATE,'YYYY-MM-DD')
        ORDER by T_REQ_TX_MAIL.PIC_SEQ,T_REQ_TX_MAIL.MOVIE_SEQ;
            
    --出演者日記添付ファイル削除対象データを取得
    CURSOR CS_CAST_DIARY 
    (
			prmSITE_CD		T_CAST_DIARY.SITE_CD%TYPE,
			prmREPORT_DAY	T_CAST_DIARY.REPORT_DAY%TYPE
    ) 
    IS 
        SELECT 
            CD.USER_SEQ,
            CD.PIC_SEQ ,
            U.LOGIN_ID
            ,CD.REPORT_DAY
        FROM 
            T_CAST_DIARY CD
            JOIN T_USER U ON
                CD.USER_SEQ = U.USER_SEQ
		WHERE
            CD.SITE_CD = prmSITE_CD 
            AND CD.REPORT_DAY <= prmREPORT_DAY
            AND CD.PIC_SEQ IS NOT NULL 
            AND U.LOGIN_ID IS NOT NULL;
            
    --会員つぶやきの添付ファイル削除対象データを取得処理は「PKG_MAN_TWEET.CS_DEL」を使用
    
	PROCEDURE OUTPUT_CSV
    (
        pDELETE_FILE_TYPE    IN	VARCHAR2,   --削除対象ファイル種類
        pPIC_SEQ             IN	NUMBER,
        pMOVIE_SEQ           IN	NUMBER,
        pLOGIN_ID            IN	VARCHAR2,
        pATTACHED_OBJ_TYPE   IN	VARCHAR2,
        pSEX_CD              IN	VARCHAR2,
        pCREATE_DATE         IN	DATE,
        pSTATUS		        OUT	VARCHAR2
    );

    PROCEDURE CLOSE_ALL;
    
END PKG_OUTPUT_DEL_FILE_LIST;
/
SHOW ERROR;


CREATE OR REPLACE PACKAGE BODY PKG_OUTPUT_DEL_FILE_LIST AS
    
    /*************************************************************************
    --  Title			: CSV出力
    --	Progaram ID		: OUTPUT_CSV
    --  Creation Date	: 17.05.16
    --	Update Date		:
    --  Author			: Yogi
    *************************************************************************/
	PROCEDURE OUTPUT_CSV
    (
        pDELETE_FILE_TYPE       IN	VARCHAR2,   --削除対象ファイル種類
        pPIC_SEQ                IN	NUMBER,
        pMOVIE_SEQ              IN	NUMBER,
        pLOGIN_ID               IN	VARCHAR2,
        pATTACHED_OBJ_TYPE      IN	VARCHAR2,
        pSEX_CD                 IN	VARCHAR2,
        pCREATE_DATE            IN	DATE,
        pSTATUS                 OUT	VARCHAR2
    )IS
        PGM_NM 	        CONSTANT VARCHAR(255)	:= 'OutPut_DeleteFileList';
        BUF_ERROR_MSG	VARCHAR2(512);
        ftFile          UTL_FILE.FILE_TYPE;
        BUF_FILE_NAME   VARCHAR2(30);
        OutPutData      VARCHAR2(1024);
        OutPutDir       VARCHAR2(255);
        
        file_chk  boolean;
        file_size number;
        block_size NUMBER;
        out_handle utl_file.file_type;
        
        Err_Code        NUMBER;
        Err_Msg         VARCHAR2(64);
    BEGIN
     
        APP_COMM.TRACE(APP_COMM.TRACE_DEBUG,PGM_NM || ' START');
        
        -- 初期化
        BUF_ERROR_MSG	:= NULL;
        pSTATUS			:= APP_COMM.STATUS_NORMAL;
        
        --メール
        IF pDELETE_FILE_TYPE = DELETE_FILE_TYPE_MAIL THEN
            OutPutDir := 'OUTPUT_DEL_MAIL_DIR';
        --日記
        ELSIF pDELETE_FILE_TYPE = DELETE_FILE_TYPE_DIARY THEN
            OutPutDir := 'OUTPUT_DEL_DIARY_DIR';
        --つぶやき
        ELSIF pDELETE_FILE_TYPE = DELETE_FILE_TYPE_TWEET THEN
            OutPutDir := 'OUTPUT_DEL_TWEET_DIR';
        ELSE
            RAISE_APPLICATION_ERROR(-20000, '削除対象ファイル種類に誤り');
        END IF;
        
        --ファイル名に作成日を付ける
        BUF_FILE_NAME := 'DeleteFileList' || TO_CHAR(SYSDATE,'YYYYMMDD') || '.csv';
        
        --ファイル存在チェック
        utl_file.fgetattr(OutPutDir,BUF_FILE_NAME,file_chk,file_size,block_size);       
        
        --追記モードで開く
        ftFile := UTL_FILE.FOPEN(OutPutDir,BUF_FILE_NAME,'a');
        
        --最初の1行目にヘッダーを出力
        IF file_chk = FALSE THEN
            UTL_FILE.PUT_LINE(ftFile,'PIC_SEQ,MOVIE_SEQ,LOGIN_ID,ATTACHED_OBJ_TYPE,TX_SEX_CD,MDATE');
        END IF;
        
        OutPutData := '';
        
        --「pPIC_SEQ」「pMOVIE_SEQ」は値がNULLの場合は「NULL」と出力
        IF pPIC_SEQ IS NOT NULL THEN   
            OutPutData := OutPutData || pPIC_SEQ || ',';
        ELSE
            OutPutData := OutPutData ||  'NULL,';
        END IF;
        IF pMOVIE_SEQ IS NOT NULL THEN   
            OutPutData := OutPutData || pMOVIE_SEQ || ',';
        ELSE
            OutPutData := OutPutData ||  'NULL,';
        END IF;
        IF pLOGIN_ID IS NOT NULL THEN   
            OutPutData := OutPutData || pLOGIN_ID || ',';
        ELSE
            OutPutData := OutPutData ||  'NULL,';
        END IF;
        
        OutPutData := OutPutData || pATTACHED_OBJ_TYPE || ',';
        OutPutData := OutPutData || pSEX_CD || ',';
        OutPutData := OutPutData || TO_CHAR(pCREATE_DATE,'YYYY-MM-DD');
        
        UTL_FILE.PUT_LINE(ftFile,OutPutData);     
        UTL_FILE.FFLUSH(ftFile);
        UTL_FILE.FCLOSE(ftFile);
   
        APP_COMM.TRACE(APP_COMM.TRACE_DEBUG,PGM_NM || ' END');
        
        EXCEPTION WHEN OTHERS THEN
            UTL_FILE.FCLOSE_ALL;
            
            Err_Code    := SQLCODE;
            Err_Msg     := SUBSTR(SQLERRM, 1 , 64);
            DBMS_OUTPUT.PUT_LINE('Error code ' || Err_Code || ': ' || Err_Msg);
            
            pSTATUS		:= APP_COMM.PROCEDURE_ERROR;
    END OUTPUT_CSV;
    
    PROCEDURE CLOSE_ALL
	IS
	BEGIN
        IF (CS_MAIL%ISOPEN) THEN
			CLOSE CS_MAIL;
		END IF;
        
		IF (CS_CAST_DIARY%ISOPEN) THEN
			CLOSE CS_CAST_DIARY;
		END IF;
	END CLOSE_ALL;
    
END PKG_OUTPUT_DEL_FILE_LIST;
/
SHOW ERROR;