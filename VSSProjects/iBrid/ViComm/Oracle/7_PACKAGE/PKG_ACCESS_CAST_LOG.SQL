/*************************************************************************/
--	System			: ViComm
--	Sub System Name	: ﾃｰﾌﾞﾙ管理
--	Sub System Name	: 共通
--	Title			: 出演者別アクセス集計 ACCESS_CAST_LOG Package
--	Progaram ID		: PKG_ACCESS_CAST_LOG_LOG
--	Compile Turn	: 0
--	Creation Date	: 10.08.11
--	Update Date		:
--	Author			: K.Itoh@ibrid
/*************************************************************************/
-- [ Update History ]
/*-----------------------------------------------------------------------*/
--	Date		Updater				Update Explain
--	yyyy/mm/dd	XXXXXXXXX			
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PACKAGE PKG_ACCESS_CAST_LOG AS
	TYPE_PROFILE		CONSTANT NUMBER := 1;			-- ﾌﾟﾛﾌｨｰﾙ
	TYPE_DIARY			CONSTANT NUMBER := 2;			-- 日記

	CURSOR CS01 IS SELECT ROWID,T_ACCESS_CAST_LOG.* FROM T_ACCESS_CAST_LOG
		FOR UPDATE WAIT 5;

	CURSOR CS02(
		prmSITE_CD			T_ACCESS_CAST_LOG.SITE_CD%TYPE			,
		prmUSER_SEQ			T_ACCESS_CAST_LOG.USER_SEQ%TYPE			,
		prmUSER_CHAR_NO		T_ACCESS_CAST_LOG.USER_CHAR_NO%TYPE		,
		prmACCESS_CAST_TYPE	T_ACCESS_CAST_LOG.ACCESS_CAST_TYPE%TYPE	,
		prmMAN_USER_SEQ		T_ACCESS_CAST_LOG.MAN_USER_SEQ%TYPE		
	) IS 
		SELECT 
			ROWID	,
			T_ACCESS_CAST_LOG.* 
		FROM
			T_ACCESS_CAST_LOG
		WHERE
			SITE_CD				= prmSITE_CD			AND
			USER_SEQ			= prmUSER_SEQ			AND
			USER_CHAR_NO		= prmUSER_CHAR_NO		AND
			ACCESS_CAST_TYPE	= prmACCESS_CAST_TYPE	AND
			MAN_USER_SEQ		= prmMAN_USER_SEQ	
		FOR UPDATE WAIT 5;


	PROCEDURE LOCK_EX;
	PROCEDURE CLOSE_ALL;
	
END PKG_ACCESS_CAST_LOG;
/
SHOW ERROR;


CREATE OR REPLACE PACKAGE BODY PKG_ACCESS_CAST_LOG AS

	/*------------------------------*/
	/* Lock Table					*/
	/*------------------------------*/
	PROCEDURE LOCK_EX
	IS
		--	CONSTANT
		PGM_NM 	CONSTANT VARCHAR(255)	:= 'PKG_ACCESS_CAST_LOG.LOCK_EX';

		BUF_TRY	NUMBER(1);
	BEGIN

		BUF_TRY	:= 0;

		LOOP
			BEGIN
				LOCK TABLE T_ACCESS_CAST_LOG IN EXCLUSIVE MODE NOWAIT;
				BUF_TRY := 0;
			EXCEPTION
				WHEN APP_COMM.ALREADY_LOCKED THEN
					BUF_TRY := BUF_TRY + 1;
					DBMS_LOCK.SLEEP(1);
			END;
			EXIT WHEN (BUF_TRY = 0) OR (BUF_TRY > APP_COMM.MAX_TRY);
		END LOOP;
	
		IF (BUF_TRY != 0) THEN
			LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,PGM_NM);
			RAISE APP_COMM.ALREADY_LOCKED;
		END IF;

	END LOCK_EX;


	/*------------------------------*/
	/* Close All					*/
	/*------------------------------*/
	PROCEDURE CLOSE_ALL
	IS
	BEGIN
		IF (CS01%ISOPEN) THEN
			CLOSE CS01;
		END IF;

		IF (CS02%ISOPEN) THEN
			CLOSE CS02;
		END IF;

	END CLOSE_ALL;

END PKG_ACCESS_CAST_LOG;
/
SHOW ERROR;
