/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: テーブル管理
--  Sub System Name	: 共通
--  Title			: クレジット自動決済状態 T_USER_LOGIN_COUNT_DAILY Package
--	Progaram ID		: PKG_USER_LOGIN_COUNT_DAILY
--	Compile Turn	: 0
--  Creation Date	: 16.06.27
--	Update Date		:
--  Author			: M&TT Y.Ikemiya
/*************************************************************************/
-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PACKAGE PKG_USER_LOGIN_COUNT_DAILY AS

	CURSOR CS01 (
		prmSITE_CD						T_USER_LOGIN_COUNT_DAILY.SITE_CD%TYPE				,
		prmREPORT_MONTH					T_USER_LOGIN_COUNT_DAILY.REPORT_MONTH%TYPE			,
		prmREPORT_DAY					T_USER_LOGIN_COUNT_DAILY.REPORT_DAY%TYPE			,
		prmSINCE_LAST_LOGIN_DAYS		T_USER_LOGIN_COUNT_DAILY.SINCE_LAST_LOGIN_DAYS%TYPE
	) IS
		SELECT
			ROWID,
			T_USER_LOGIN_COUNT_DAILY.*
		FROM
			T_USER_LOGIN_COUNT_DAILY
		WHERE
			T_USER_LOGIN_COUNT_DAILY.SITE_CD					= prmSITE_CD					AND
			T_USER_LOGIN_COUNT_DAILY.REPORT_MONTH				= prmREPORT_MONTH				AND
			T_USER_LOGIN_COUNT_DAILY.REPORT_DAY					= prmREPORT_DAY					AND
			T_USER_LOGIN_COUNT_DAILY.SINCE_LAST_LOGIN_DAYS		= prmSINCE_LAST_LOGIN_DAYS
		FOR UPDATE WAIT 5;

	PROCEDURE LOCK_EX;

	PROCEDURE CLOSE_ALL;

END PKG_USER_LOGIN_COUNT_DAILY;
/
SHOW ERROR;


CREATE OR REPLACE PACKAGE BODY PKG_USER_LOGIN_COUNT_DAILY AS

	/*------------------------------*/
	/* Lock Table					*/
	/*------------------------------*/
	PROCEDURE LOCK_EX
	IS
		--	CONSTANT
		PGM_NM 	CONSTANT VARCHAR(255)	:= 'PKG_USER_LOGIN_COUNT_DAILY.LOCK_EX';

		BUF_TRY	NUMBER(1);
	BEGIN

		BUF_TRY	:= 0;

		LOOP
			BEGIN
				LOCK TABLE T_USER_LOGIN_COUNT_DAILY IN EXCLUSIVE MODE NOWAIT;
				BUF_TRY := 0;
			EXCEPTION
				WHEN APP_COMM.ALREADY_LOCKED THEN
					BUF_TRY := BUF_TRY + 1;
					DBMS_LOCK.SLEEP(1);
			END;
			EXIT WHEN (BUF_TRY = 0) OR (BUF_TRY > APP_COMM.MAX_TRY);
		END LOOP;
	
		IF (BUF_TRY != 0) THEN
			LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,PGM_NM);
			RAISE APP_COMM.ALREADY_LOCKED;
		END IF;

	END LOCK_EX;

	/*------------------------------*/
	/* Close All					*/
	/*------------------------------*/
	PROCEDURE CLOSE_ALL
	IS
	BEGIN
		IF (CS01%ISOPEN) THEN
			CLOSE CS01;
		END IF;

	END CLOSE_ALL;

END PKG_USER_LOGIN_COUNT_DAILY;
/
SHOW ERROR;
