/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: Γ°ΜήΩΗ
--  Sub System Name	: €Κ
--  Title			: j«ουf¦Β T_USER_MAN_BBS Package
--	Progaram ID		: PKG_USER_MAN_BBS
--	Compile Turn	: 0
--  Creation Date	: 10.04.22
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PACKAGE PKG_USER_MAN_BBS AS

	CURSOR CS01 IS SELECT ROWID,T_USER_MAN_BBS.* FROM T_USER_MAN_BBS ORDER BY SITE_CD,USER_SEQ,USER_CHAR_NO
		FOR UPDATE WAIT 5;

	CURSOR CS02(prmSITE_CD		T_USER_MAN_BBS.SITE_CD		%TYPE,
				prmUSER_SEQ		T_USER_MAN_BBS.USER_SEQ		%TYPE,
				prmUSER_CHAR_NO	T_USER_MAN_BBS.USER_CHAR_NO	%TYPE,
				prmBBS_SEQ		T_USER_MAN_BBS.BBS_SEQ		%TYPE) IS SELECT ROWID,T_USER_MAN_BBS.* FROM T_USER_MAN_BBS
		WHERE
			SITE_CD			= prmSITE_CD		AND
			USER_SEQ		= prmUSER_SEQ		AND
			USER_CHAR_NO	= prmUSER_CHAR_NO	AND
			BBS_SEQ			= prmBBS_SEQ
		FOR UPDATE WAIT 5;

	CURSOR CS02_1(prmBBS_SEQ	T_USER_MAN_BBS.BBS_SEQ		%TYPE) IS SELECT ROWID,T_USER_MAN_BBS.* FROM T_USER_MAN_BBS
		WHERE
			BBS_SEQ			= prmBBS_SEQ
		FOR UPDATE WAIT 5;

	CURSOR CS03(prmROWID ROWID) IS SELECT T_USER_MAN_BBS.* FROM T_USER_MAN_BBS
		WHERE
			ROWID = prmROWID
		FOR UPDATE WAIT 5;

	CURSOR CS04(prmSITE_CD	T_USER_MAN_BBS.SITE_CD	%TYPE,
				prmUSER_SEQ	T_USER_MAN_BBS.USER_SEQ	%TYPE) IS SELECT ROWID FROM T_USER_MAN_BBS
		WHERE
			SITE_CD			= prmSITE_CD		AND
			USER_SEQ		= prmUSER_SEQ
		FOR UPDATE WAIT 5;

	CURSOR CS_DEL(
			prmSITE_CD 		T_USER_MAN_BBS.SITE_CD%TYPE,
			prmCREATE_DATE	T_USER_MAN_BBS.CREATE_DATE%TYPE) IS SELECT ROWID FROM T_USER_MAN_BBS
		WHERE
			SITE_CD = prmSITE_CD AND CREATE_DATE <= prmCREATE_DATE
		FOR UPDATE WAIT 10;

	PROCEDURE LOCK_EX;

	PROCEDURE CLOSE_ALL;

END PKG_USER_MAN_BBS;
/
SHOW ERROR;


CREATE OR REPLACE PACKAGE BODY PKG_USER_MAN_BBS AS

	/*------------------------------*/
	/* Lock Table					*/
	/*------------------------------*/
	PROCEDURE LOCK_EX
	IS
		--	CONSTANT
		PGM_NM 	CONSTANT VARCHAR(255)	:= 'PKG_USER_MAN_BBS.LOCK_EX';

		BUF_TRY	NUMBER(1);
	BEGIN

		BUF_TRY	:= 0;

		LOOP
			BEGIN
				LOCK TABLE T_USER_MAN_BBS IN EXCLUSIVE MODE NOWAIT;
				BUF_TRY := 0;
			EXCEPTION
				WHEN APP_COMM.ALREADY_LOCKED THEN
					BUF_TRY := BUF_TRY + 1;
					DBMS_LOCK.SLEEP(1);
			END;
			EXIT WHEN (BUF_TRY = 0) OR (BUF_TRY > APP_COMM.MAX_TRY);
		END LOOP;
	
		IF (BUF_TRY != 0) THEN
			LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,PGM_NM);
			RAISE APP_COMM.ALREADY_LOCKED;
		END IF;

	END LOCK_EX;

	/*------------------------------*/
	/* Close All					*/
	/*------------------------------*/
	PROCEDURE CLOSE_ALL
	IS
	BEGIN
		IF (CS01%ISOPEN) THEN
			CLOSE CS01;
		END IF;

		IF (CS02%ISOPEN) THEN
			CLOSE CS02;
		END IF;
		
		IF (CS02_1%ISOPEN) THEN
			CLOSE CS02_1;
		END IF;

		IF (CS03%ISOPEN) THEN
			CLOSE CS03;
		END IF;

		IF (CS04%ISOPEN) THEN
			CLOSE CS04;
		END IF;

		IF (CS_DEL%ISOPEN) THEN
			CLOSE CS_DEL;
		END IF;

	END CLOSE_ALL;

END PKG_USER_MAN_BBS;
/
SHOW ERROR;
