/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: 共通
--  Title			: 共通関数定義
--	Progaram ID		: VICOMM_FUNC
--	Compile Turn	: 0
--  Creation Date	: 17.05.10
--	Update Date		:
--  Author			: Yogi
/*************************************************************************/
CREATE OR REPLACE PACKAGE VICOMM_FUNC AS

    /*************************************************************************
    --  ファイル削除
    --※注　プロシージャ内でコミット処理を行っていますので、カーソルループ内では使用しないでください。
    --　　　ORA-01002「フェッチ順序が無効です」が出ます。
    --　　　「BULK COLLECT」で事前に一括取得して使用してください。
    *************************************************************************/
	PROCEDURE DELETE_FILE
    (
        pFILE_DIR	IN	VARCHAR2,   --削除対象ファイルの保存先 　例：\\192.168.2.100\ViComm-PWILD\Data\A001\man
        pFILE_NAME	IN	VARCHAR2,   --ファイル名               例：001.txt
        pSTATUS		OUT	VARCHAR2
    );

END VICOMM_FUNC;
/
SHOW ERROR;


CREATE OR REPLACE PACKAGE BODY VICOMM_FUNC AS

    /*************************************************************************
    --  Title			: ファイル削除
    --	Progaram ID		: DELETE_FILE
    --  Creation Date	: 17.05.10
    --	Update Date		:
    --  Author			: Yogi
    *************************************************************************/
	PROCEDURE DELETE_FILE
    (
        pFILE_DIR	IN	VARCHAR2,
        pFILE_NAME	IN	VARCHAR2,
        pSTATUS		OUT	VARCHAR2
    )IS
        PGM_NM 	        CONSTANT VARCHAR(255)	:= 'DELETE_FILE';
        BUF_ERROR_MSG	VARCHAR2(512);
      
        file_chk  boolean;
        file_size number;
        block_size number;
        out_handle utl_file.file_type;
        
        BUF_DIR_STR			VARCHAR2(256);    --作成するディレクトリ名
        BUF_CREATE_DIR_SQL  VARCHAR2(256);  --ディレクトリを作成するSQL文
    BEGIN
     
        APP_COMM.TRACE(APP_COMM.TRACE_DEBUG,PGM_NM || ' START');
      
        -- 初期化
        BUF_ERROR_MSG	:= NULL;
        BUF_DIR_STR := 'DELETE_FILE_DIR';
        BUF_CREATE_DIR_SQL := 'create or replace directory ' || BUF_DIR_STR || ' as ' || '''' || pFILE_DIR || '''';
        pSTATUS			:= APP_COMM.STATUS_NORMAL;
        
        --ディレクトリ作成
        --※「utl_file.fremove」でファイルの削除を行う場合、事前にオラクルのディレクトリにディレクトリパスを作成し、
        --作成したディレクトリ名で削除しないといけないらしい。
        EXECUTE IMMEDIATE BUF_CREATE_DIR_SQL;
        
        --削除対象のファイルが存在する場合に削除する
        utl_file.fgetattr(BUF_DIR_STR,pFILE_NAME,file_chk,file_size,block_size);
        IF file_chk THEN
            UTL_FILE.FREMOVE(BUF_DIR_STR,PFILE_NAME);
        ELSE 
            pSTATUS	:= 'NFF';  --「Not Find File」の略
        END IF;
        
        APP_COMM.TRACE(APP_COMM.TRACE_DEBUG,PGM_NM || ' END');
        
        EXCEPTION
            WHEN OTHERS THEN
                pSTATUS			:= APP_COMM.PROCEDURE_ERROR;
                LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM, SQLERRM || ' ' || BUF_ERROR_MSG);
    END DELETE_FILE;

END VICOMM_FUNC;
/
SHOW ERROR;