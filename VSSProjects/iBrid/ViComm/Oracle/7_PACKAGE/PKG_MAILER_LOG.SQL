/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: ﾃｰﾌﾞﾙ管理
--  Sub System Name	: 共通
--  Title			: メール配信ログ T_MAILER_LOG Package
--	Progaram ID		: PKG_MAILER_LOG
--	Compile Turn	: 0
--  Creation Date	: 09.06.01
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PACKAGE PKG_MAILER_LOG AS

	CURSOR CS01 IS SELECT ROWID,T_MAILER_LOG.* FROM T_MAILER_LOG ORDER BY MAIL_TX_LOG_SEQ
		FOR UPDATE WAIT 5;

	CURSOR CS02(prmMAIL_TX_LOG_SEQ	T_MAILER_LOG.MAIL_TX_LOG_SEQ%TYPE) IS SELECT ROWID,T_MAILER_LOG.* FROM T_MAILER_LOG
		WHERE
			MAIL_TX_LOG_SEQ= prmMAIL_TX_LOG_SEQ	
		FOR UPDATE WAIT 5;

	CURSOR CS03(prmROWID ROWID) IS SELECT * FROM T_MAILER_LOG
		WHERE
			ROWID = prmROWID
		FOR UPDATE WAIT 5;

	PROCEDURE LOCK_EX;
	PROCEDURE CLOSE_ALL;

END PKG_MAILER_LOG;
/
SHOW ERROR;


CREATE OR REPLACE PACKAGE BODY PKG_MAILER_LOG AS

	/*------------------------------*/
	/* Lock Table					*/
	/*------------------------------*/
	PROCEDURE LOCK_EX
	IS
		--	CONSTANT
		PGM_NM 	CONSTANT VARCHAR(255)	:= 'PKG_MAILER_LOG.LOCK_EX';

		BUF_TRY	NUMBER(1);
	BEGIN

		BUF_TRY	:= 0;

		LOOP
			BEGIN
				LOCK TABLE T_MAILER_LOG IN EXCLUSIVE MODE NOWAIT;
				BUF_TRY := 0;
			EXCEPTION
				WHEN APP_COMM.ALREADY_LOCKED THEN
					BUF_TRY := BUF_TRY + 1;
					DBMS_LOCK.SLEEP(1);
			END;
			EXIT WHEN (BUF_TRY = 0) OR (BUF_TRY > APP_COMM.MAX_TRY);
		END LOOP;
	
		IF (BUF_TRY != 0) THEN
			LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,PGM_NM);
			RAISE APP_COMM.ALREADY_LOCKED;
		END IF;

	END LOCK_EX;
	/*------------------------------*/
	/* Close All					*/
	/*------------------------------*/
	PROCEDURE CLOSE_ALL
	IS
	BEGIN
		IF (CS01%ISOPEN) THEN
			CLOSE CS01;
		END IF;

		IF (CS02%ISOPEN) THEN
			CLOSE CS02;
		END IF;

		IF (CS03%ISOPEN) THEN
			CLOSE CS03;
		END IF;

	END CLOSE_ALL;

END PKG_MAILER_LOG;
/
SHOW ERROR;
