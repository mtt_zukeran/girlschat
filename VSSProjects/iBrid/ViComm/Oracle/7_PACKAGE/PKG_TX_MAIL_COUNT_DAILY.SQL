/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: テーブル管理
--  Sub System Name	: 共通
--  Title			: 日別メール送信数 T_TX_MAIL_COUNT_DAILY Package
--	Progaram ID		: PKG_TX_MAIL_COUNT_DAILY
--	Compile Turn	: 0
--  Creation Date	: 15.06.02
--	Update Date		:
--  Author			: M&TT Y.Ikemiya
/*************************************************************************/
-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PACKAGE PKG_TX_MAIL_COUNT_DAILY AS

	CURSOR CS01 (
		prmSITE_CD						T_TX_MAIL_COUNT_DAILY.SITE_CD				%TYPE	,
		prmMAIL_TEMPLATE_NO				T_TX_MAIL_COUNT_DAILY.MAIL_TEMPLATE_NO		%TYPE	,
		prmSEX_CD						T_TX_MAIL_COUNT_DAILY.SEX_CD				%TYPE	,
		prmTX_MAIL_DAY					T_TX_MAIL_COUNT_DAILY.TX_MAIL_DAY			%TYPE
	) IS
		SELECT
			ROWID,T_TX_MAIL_COUNT_DAILY.*
		FROM
			T_TX_MAIL_COUNT_DAILY
		WHERE
			T_TX_MAIL_COUNT_DAILY.SITE_CD			= prmSITE_CD			AND
			T_TX_MAIL_COUNT_DAILY.MAIL_TEMPLATE_NO	= prmMAIL_TEMPLATE_NO	AND
			T_TX_MAIL_COUNT_DAILY.SEX_CD			= prmSEX_CD				AND
			T_TX_MAIL_COUNT_DAILY.TX_MAIL_DAY		= prmTX_MAIL_DAY
		FOR UPDATE WAIT 5;
	
	PROCEDURE LOCK_EX;

	PROCEDURE CLOSE_ALL;

END PKG_TX_MAIL_COUNT_DAILY;
/
SHOW ERROR;


CREATE OR REPLACE PACKAGE BODY PKG_TX_MAIL_COUNT_DAILY AS

	/*------------------------------*/
	/* Lock Table					*/
	/*------------------------------*/
	PROCEDURE LOCK_EX
	IS
		--	CONSTANT
		PGM_NM 	CONSTANT VARCHAR(255)	:= 'PKG_TX_MAIL_COUNT_DAILY.LOCK_EX';

		BUF_TRY	NUMBER(1);
	BEGIN

		BUF_TRY	:= 0;

		LOOP
			BEGIN
				LOCK TABLE T_TX_MAIL_COUNT_DAILY IN EXCLUSIVE MODE NOWAIT;
				BUF_TRY := 0;
			EXCEPTION
				WHEN APP_COMM.ALREADY_LOCKED THEN
					BUF_TRY := BUF_TRY + 1;
					DBMS_LOCK.SLEEP(1);
			END;
			EXIT WHEN (BUF_TRY = 0) OR (BUF_TRY > APP_COMM.MAX_TRY);
		END LOOP;
	
		IF (BUF_TRY != 0) THEN
			LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,PGM_NM);
			RAISE APP_COMM.ALREADY_LOCKED;
		END IF;

	END LOCK_EX;

	/*------------------------------*/
	/* Close All					*/
	/*------------------------------*/
	PROCEDURE CLOSE_ALL
	IS
	BEGIN
		IF (CS01%ISOPEN) THEN
			CLOSE CS01;
		END IF;

	END CLOSE_ALL;

END PKG_TX_MAIL_COUNT_DAILY;
/
SHOW ERROR;
