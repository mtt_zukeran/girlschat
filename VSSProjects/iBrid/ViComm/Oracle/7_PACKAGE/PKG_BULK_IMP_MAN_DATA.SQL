/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: Γ°ΜήΩΗ
--  Sub System Name	: €Κ
--  Title			: κουζπ T_BULK_IMP_MAN_DATA Package
--	Progaram ID		: PKG_BULK_IMP_MAN_DATA
--	Compile Turn	: 0
--  Creation Date	: 10.10.18
--	Update Date		:
--  Author			: K.Itoh
/*************************************************************************/
-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PACKAGE PKG_BULK_IMP_MAN_DATA AS

	CURSOR CS01 IS SELECT ROWID,T_BULK_IMP_MAN_DATA.* FROM T_BULK_IMP_MAN_DATA ORDER BY SITE_CD
		FOR UPDATE WAIT 5;

	CURSOR CS02(
		prmSITE_CD						T_BULK_IMP_MAN_DATA.SITE_CD						%TYPE	,
		prmBULK_IMP_MAN_HISTORY_SEQ		T_BULK_IMP_MAN_DATA.BULK_IMP_MAN_HISTORY_SEQ	%TYPE	,
		prmTEXT_LINE_NO					T_BULK_IMP_MAN_DATA.TEXT_LINE_NO				%TYPE
	) IS 
		SELECT 
			ROWID						,
			T_BULK_IMP_MAN_DATA.* 	
		FROM 
			T_BULK_IMP_MAN_DATA
		WHERE
			SITE_CD						= prmSITE_CD					AND
			BULK_IMP_MAN_HISTORY_SEQ	= prmBULK_IMP_MAN_HISTORY_SEQ	AND
			TEXT_LINE_NO				= prmTEXT_LINE_NO
		FOR UPDATE WAIT 5;
		
	CURSOR CS03(
		prmBULK_IMP_MAN_HISTORY_SEQ		T_BULK_IMP_MAN_DATA.BULK_IMP_MAN_HISTORY_SEQ	%TYPE	
	) IS 
		SELECT 
			ROWID						,
			T_BULK_IMP_MAN_DATA.* 	
		FROM 
			T_BULK_IMP_MAN_DATA
		WHERE
			BULK_IMP_MAN_HISTORY_SEQ	= prmBULK_IMP_MAN_HISTORY_SEQ
		ORDER BY
			BULK_IMP_MAN_HISTORY_SEQ,TEXT_LINE_NO
		FOR UPDATE WAIT 5;

	PROCEDURE LOCK_EX;
	PROCEDURE CLOSE_ALL;

END PKG_BULK_IMP_MAN_DATA;
/
SHOW ERROR;


CREATE OR REPLACE PACKAGE BODY PKG_BULK_IMP_MAN_DATA AS

	/*------------------------------*/
	/* Lock Table					*/
	/*------------------------------*/
	PROCEDURE LOCK_EX
	IS
		--	CONSTANT
		PGM_NM 	CONSTANT VARCHAR(255)	:= 'PKG_BULK_IMP_MAN_DATA.LOCK_EX';

		BUF_TRY	NUMBER(1);
	BEGIN

		BUF_TRY	:= 0;

		LOOP
			BEGIN
				LOCK TABLE T_BULK_IMP_MAN_DATA IN EXCLUSIVE MODE NOWAIT;
				BUF_TRY := 0;
			EXCEPTION
				WHEN APP_COMM.ALREADY_LOCKED THEN
					BUF_TRY := BUF_TRY + 1;
					DBMS_LOCK.SLEEP(1);
			END;
			EXIT WHEN (BUF_TRY = 0) OR (BUF_TRY > APP_COMM.MAX_TRY);
		END LOOP;

		IF (BUF_TRY != 0) THEN
			LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,PGM_NM);
			RAISE APP_COMM.ALREADY_LOCKED;
		END IF;

	END LOCK_EX;

	
	/*------------------------------*/
	/* Close All					*/
	/*------------------------------*/
	PROCEDURE CLOSE_ALL
	IS
	BEGIN
		IF (CS01%ISOPEN) THEN
			CLOSE CS01;
		END IF;
		IF (CS02%ISOPEN) THEN
			CLOSE CS02;
		END IF;
		IF (CS03%ISOPEN) THEN
			CLOSE CS03;
		END IF;		
	END CLOSE_ALL;
	
END PKG_BULK_IMP_MAN_DATA;
/
SHOW ERROR;
