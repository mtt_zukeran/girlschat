/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: ﾃｰﾌﾞﾙ管理
--  Sub System Name	: 共通
--  Title			: ボイス別アクセス集計 T_ACCESS_VOICE Package
--	Progaram ID		: PKG_ACCESS_VOICE
--	Compile Turn	: 0
--  Creation Date	: 10.07.30
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PACKAGE PKG_ACCESS_VOICE AS

	CURSOR CS01 IS SELECT ROWID,T_ACCESS_VOICE.* FROM T_ACCESS_VOICE ORDER BY SITE_CD,REPORT_DAY
		FOR UPDATE WAIT 5;

	CURSOR CS02(prmSITE_CD		T_ACCESS_VOICE.SITE_CD		%TYPE,
				prmUSER_SEQ		T_ACCESS_VOICE.USER_SEQ		%TYPE,
				prmUSER_CHAR_NO	T_ACCESS_VOICE.USER_CHAR_NO	%TYPE,
				prmVOICE_SEQ	T_ACCESS_VOICE.VOICE_SEQ	%TYPE,
				prmREPORT_DAY	T_ACCESS_VOICE.REPORT_DAY	%TYPE) IS SELECT ROWID,T_ACCESS_VOICE.* FROM T_ACCESS_VOICE
		WHERE
			SITE_CD			= prmSITE_CD		AND
			USER_SEQ		= prmUSER_SEQ		AND
			USER_CHAR_NO	= prmUSER_CHAR_NO	AND
			VOICE_SEQ		= prmVOICE_SEQ		AND
			REPORT_DAY		= prmREPORT_DAY
		FOR UPDATE WAIT 5;

	PROCEDURE LOCK_EX;
	PROCEDURE CLOSE_ALL;

END PKG_ACCESS_VOICE;
/
SHOW ERROR;


CREATE OR REPLACE PACKAGE BODY PKG_ACCESS_VOICE AS

	/*------------------------------*/
	/* Lock Table					*/
	/*------------------------------*/
	PROCEDURE LOCK_EX
	IS
		--	CONSTANT
		PGM_NM 	CONSTANT VARCHAR(255)	:= 'PKG_ACCESS_VOICE.LOCK_EX';

		BUF_TRY	NUMBER(1);
	BEGIN

		BUF_TRY	:= 0;

		LOOP
			BEGIN
				LOCK TABLE T_ACCESS_VOICE IN EXCLUSIVE MODE NOWAIT;
				BUF_TRY := 0;
			EXCEPTION
				WHEN APP_COMM.ALREADY_LOCKED THEN
					BUF_TRY := BUF_TRY + 1;
					DBMS_LOCK.SLEEP(1);
			END;
			EXIT WHEN (BUF_TRY = 0) OR (BUF_TRY > APP_COMM.MAX_TRY);
		END LOOP;
	
		IF (BUF_TRY != 0) THEN
			LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,PGM_NM);
			RAISE APP_COMM.ALREADY_LOCKED;
		END IF;

	END LOCK_EX;

	/*------------------------------*/
	/* Close All					*/
	/*------------------------------*/
	PROCEDURE CLOSE_ALL
	IS
	BEGIN
		IF (CS01%ISOPEN) THEN
			CLOSE CS01;
		END IF;

		IF (CS02%ISOPEN) THEN
			CLOSE CS02;
		END IF;

	END CLOSE_ALL;

END PKG_ACCESS_VOICE;
/
SHOW ERROR;
