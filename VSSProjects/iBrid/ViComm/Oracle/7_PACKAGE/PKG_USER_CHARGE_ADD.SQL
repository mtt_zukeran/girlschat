/*************************************************************************/
--	System			: ViComm 
--	Sub System Name	: ﾃｰﾌﾞﾙ管理
--	Sub System Name	: 共通
--	Title			: 会員課金追加設定 T_USER_CHARGE_ADD Package
--	Progaram ID		: PKG_USER_CHARGE_ADD
--	Compile Turn	: 0
--	Creation Date	: 12.05.28
--	Update Date		:
--	Author			: i-Brid
/*************************************************************************/
-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PACKAGE PKG_USER_CHARGE_ADD AS

	CURSOR CS01 IS SELECT ROWID,T_USER_CHARGE_ADD.* FROM T_USER_CHARGE_ADD ORDER BY SITE_CD,USER_RANK
		FOR UPDATE WAIT 5;

	CURSOR CS02(prmSITE_CD				T_USER_CHARGE_ADD.SITE_CD				%TYPE,
				prmUSER_RANK			T_USER_CHARGE_ADD.USER_RANK				%TYPE,
				prmCHARGE_TYPE			T_USER_CHARGE_ADD.CHARGE_TYPE			%TYPE,
				prmACT_CATEGORY_SEQ		T_USER_CHARGE_ADD.ACT_CATEGORY_SEQ		%TYPE,
				prmCAST_RANK			T_USER_CHARGE_ADD.CAST_RANK				%TYPE,
				prmUSER_CHARGE_ADD_CD	T_USER_CHARGE_ADD.USER_CHARGE_ADD_CD	%TYPE) IS SELECT ROWID,T_USER_CHARGE_ADD.* FROM T_USER_CHARGE_ADD
		WHERE
			SITE_CD					= prmSITE_CD				AND
			USER_RANK				= prmUSER_RANK				AND
			CHARGE_TYPE				= prmCHARGE_TYPE			AND
			ACT_CATEGORY_SEQ		= prmACT_CATEGORY_SEQ		AND
			CAST_RANK				= prmCAST_RANK				AND
			USER_CHARGE_ADD_CD		= prmUSER_CHARGE_ADD_CD
		FOR UPDATE WAIT 5;

	CURSOR CS03(prmROWID ROWID) IS SELECT REVISION_NO FROM T_USER_CHARGE_ADD
		WHERE
			ROWID = prmROWID
		FOR UPDATE WAIT 5;

	PROCEDURE LOCK_EX;

	PROCEDURE CLOSE_ALL;

END PKG_USER_CHARGE_ADD;
/
SHOW ERROR;


CREATE OR REPLACE PACKAGE BODY PKG_USER_CHARGE_ADD AS

	/*------------------------------*/
	/* Lock Table					*/
	/*------------------------------*/
	PROCEDURE LOCK_EX
	IS
		--	CONSTANT
		PGM_NM 	CONSTANT VARCHAR(255)	:= 'PKG_USER_CHARGE_ADD.LOCK_EX';

		BUF_TRY	NUMBER(1);
	BEGIN

		BUF_TRY	:= 0;

		LOOP
			BEGIN
				LOCK TABLE T_USER_CHARGE_ADD IN EXCLUSIVE MODE NOWAIT;
				BUF_TRY := 0;
			EXCEPTION
				WHEN APP_COMM.ALREADY_LOCKED THEN
					BUF_TRY := BUF_TRY + 1;
					DBMS_LOCK.SLEEP(1);
			END;
			EXIT WHEN (BUF_TRY = 0) OR (BUF_TRY > APP_COMM.MAX_TRY);
		END LOOP;
	
		IF (BUF_TRY != 0) THEN
			LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,PGM_NM);
			RAISE APP_COMM.ALREADY_LOCKED;
		END IF;

	END LOCK_EX;


	/*------------------------------*/
	/* Close All					*/
	/*------------------------------*/
	PROCEDURE CLOSE_ALL
	IS
	BEGIN
		IF (CS01%ISOPEN) THEN
			CLOSE CS01;
		END IF;

		IF (CS02%ISOPEN) THEN
			CLOSE CS02;
		END IF;

		IF (CS03%ISOPEN) THEN
			CLOSE CS03;
		END IF;

	END CLOSE_ALL;

END PKG_USER_CHARGE_ADD;
/
SHOW ERROR;
