/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: ﾃｰﾌﾞﾙ管理
--  Sub System Name	: 共通
--  Title			: 督促スケジュール T_URGE_SCHEDULE Package
--	Progaram ID		: PKG_URGE_SCHEDULE
--	Compile Turn	: 0
--  Creation Date	: 09.12.14
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PACKAGE PKG_URGE_SCHEDULE AS

	CURSOR CS01 IS SELECT ROWID,T_URGE_SCHEDULE.* FROM T_URGE_SCHEDULE ORDER BY SITE_CD,URGE_LEVEL,URGE_COUNT_PER_DAY
		FOR UPDATE WAIT 5;

	CURSOR CS02(prmSITE_CD			T_URGE_SCHEDULE.SITE_CD			%TYPE,	
				prmURGE_LEVEL		T_URGE_SCHEDULE.URGE_LEVEL		%TYPE	
	) IS SELECT ROWID,T_URGE_SCHEDULE.* FROM T_URGE_SCHEDULE
		WHERE
			SITE_CD				= prmSITE_CD			AND	
			URGE_LEVEL			= prmURGE_LEVEL			
		ORDER BY SITE_CD,URGE_LEVEL,URGE_COUNT_PER_DAY
		FOR UPDATE WAIT 5;

	CURSOR CS03(prmROWID ROWID) IS SELECT REVISION_NO FROM T_URGE_SCHEDULE
		WHERE
			ROWID = prmROWID
		FOR UPDATE WAIT 5;

	PROCEDURE LOCK_EX;

	PROCEDURE CLOSE_ALL;

END PKG_URGE_SCHEDULE;
/
SHOW ERROR;

CREATE OR REPLACE PACKAGE BODY PKG_URGE_SCHEDULE AS

	/*------------------------------*/
	/* Lock Table					*/
	/*------------------------------*/
	PROCEDURE LOCK_EX
	IS
		--	CONSTANT
		PGM_NM 	CONSTANT VARCHAR(255)	:= 'PKG_URGE_SCHEDULE.LOCK_EX';

		BUF_TRY	NUMBER(1);
	BEGIN

		BUF_TRY	:= 0;

		LOOP
			BEGIN
				LOCK TABLE T_URGE_SCHEDULE IN EXCLUSIVE MODE NOWAIT;
				BUF_TRY := 0;
			EXCEPTION
				WHEN APP_COMM.ALREADY_LOCKED THEN
					BUF_TRY := BUF_TRY + 1;
					DBMS_LOCK.SLEEP(1);
			END;
			EXIT WHEN (BUF_TRY = 0) OR (BUF_TRY > APP_COMM.MAX_TRY);
		END LOOP;

		IF (BUF_TRY != 0) THEN
			LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,PGM_NM);
			RAISE APP_COMM.ALREADY_LOCKED;
		END IF;

	END LOCK_EX;


	/*------------------------------*/
	/* Close All					*/
	/*------------------------------*/
	PROCEDURE CLOSE_ALL
	IS
	BEGIN
		IF (CS01%ISOPEN) THEN
			CLOSE CS01;
		END IF;

		IF (CS02%ISOPEN) THEN
			CLOSE CS02;
		END IF;

		IF (CS03%ISOPEN) THEN
			CLOSE CS03;
		END IF;

	END CLOSE_ALL;

END PKG_URGE_SCHEDULE;
/
SHOW ERROR;
