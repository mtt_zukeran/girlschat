/*************************************************************************/
--	System			: ViComm
--	Title			: T_BBS_BINGO_CARD Package
--	Progaram ID		: PKG_BBS_BINGO_CARD
--	Compile Turn	: 0
--	Creation Date	: 13.10.28
--	Update Date		:
--	Author			: K.Miyazato
/*************************************************************************/
CREATE OR REPLACE PACKAGE PKG_BBS_BINGO_CARD AS

	CURSOR CS01(
		prmSITE_CD	T_BBS_BINGO_CARD.SITE_CD%TYPE,
		prmTERM_SEQ	T_BBS_BINGO_CARD.TERM_SEQ%TYPE,
		prmCARD_NO	T_BBS_BINGO_CARD.CARD_NO%TYPE
	)IS
		SELECT
			ROWID,
			T_BBS_BINGO_CARD.*
		FROM
			T_BBS_BINGO_CARD
		WHERE
			SITE_CD		= prmSITE_CD	AND
			TERM_SEQ	= prmTERM_SEQ	AND
			CARD_NO		= prmCARD_NO
		FOR UPDATE WAIT 5;

	CURSOR CS02(
		prmSITE_CD	T_BBS_BINGO_CARD.SITE_CD%TYPE,
		prmTERM_SEQ	T_BBS_BINGO_CARD.TERM_SEQ%TYPE
	)IS
		SELECT
			ROWID,
			T_BBS_BINGO_CARD.*
		FROM
			T_BBS_BINGO_CARD
		WHERE
			SITE_CD		= prmSITE_CD	AND
			TERM_SEQ	= prmTERM_SEQ
		FOR UPDATE WAIT 5;

	PROCEDURE LOCK_EX;
	PROCEDURE CLOSE_ALL;

END PKG_BBS_BINGO_CARD;
/
SHOW ERROR;


CREATE OR REPLACE PACKAGE BODY PKG_BBS_BINGO_CARD AS
	/*------------------------------*/
	/* Lock Table					*/
	/*------------------------------*/
	PROCEDURE LOCK_EX
	IS
		--	CONSTANT
		PGM_NM	CONSTANT VARCHAR(255)	:= 'PKG_BBS_BINGO_CARD.LOCK_EX';
		BUF_TRY	NUMBER(1);
	BEGIN
		BUF_TRY	:= 0;

		LOOP
			BEGIN
				LOCK TABLE T_BBS_BINGO_CARD IN EXCLUSIVE MODE NOWAIT;
				BUF_TRY := 0;
			EXCEPTION
				WHEN APP_COMM.ALREADY_LOCKED THEN
					BUF_TRY := BUF_TRY + 1;
					DBMS_LOCK.SLEEP(1);
			END;
			EXIT WHEN (BUF_TRY = 0) OR (BUF_TRY > APP_COMM.MAX_TRY);
		END LOOP;

		IF (BUF_TRY != 0) THEN
			LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,PGM_NM);
			RAISE APP_COMM.ALREADY_LOCKED;
		END IF;
	END LOCK_EX;

	/*------------------------------*/
	/* Close All					*/
	/*------------------------------*/
	PROCEDURE CLOSE_ALL
	IS
	BEGIN
		IF (CS01%ISOPEN) THEN
			CLOSE CS01;
		END IF;

		IF (CS02%ISOPEN) THEN
			CLOSE CS02;
		END IF;

	END CLOSE_ALL;

END PKG_BBS_BINGO_CARD;
/
SHOW ERROR;