/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: Γ°ΜήΩΗ
--  Sub System Name	: €Κ
--  Title			: ’FΨLXg©UXPW[ T_UNAUTH_CAST_INVITE_SCHE Package
--	Progaram ID		: PKG_UNAUTH_CAST_INVITE_SCHE
--	Compile Turn	: 0
--  Creation Date	: 10.05.31
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PACKAGE PKG_UNAUTH_CAST_INVITE_SCHE AS

	CURSOR CS01 IS SELECT ROWID,T_UNAUTH_CAST_INVITE_SCHE.* FROM T_UNAUTH_CAST_INVITE_SCHE ORDER BY SITE_CD,INVITE_LEVEL,INVITE_COUNT_PER_DAY
		FOR UPDATE WAIT 5;

	CURSOR CS02(prmSITE_CD			T_UNAUTH_CAST_INVITE_SCHE.SITE_CD			%TYPE,
				prmINVITE_LEVEL		T_UNAUTH_CAST_INVITE_SCHE.INVITE_LEVEL		%TYPE
	) IS SELECT ROWID,T_UNAUTH_CAST_INVITE_SCHE.* FROM T_UNAUTH_CAST_INVITE_SCHE
		WHERE
			SITE_CD				= prmSITE_CD			AND
			INVITE_LEVEL		= prmINVITE_LEVEL
		ORDER BY SITE_CD,INVITE_LEVEL,INVITE_COUNT_PER_DAY
		FOR UPDATE WAIT 5;

	CURSOR CS03(prmROWID ROWID) IS SELECT REVISION_NO FROM T_UNAUTH_CAST_INVITE_SCHE
		WHERE
			ROWID = prmROWID
		FOR UPDATE WAIT 5;

	PROCEDURE LOCK_EX;

	PROCEDURE CLOSE_ALL;

END PKG_UNAUTH_CAST_INVITE_SCHE;
/
SHOW ERROR;

CREATE OR REPLACE PACKAGE BODY PKG_UNAUTH_CAST_INVITE_SCHE AS

	/*------------------------------*/
	/* Lock Table					*/
	/*------------------------------*/
	PROCEDURE LOCK_EX
	IS
		--	CONSTANT
		PGM_NM 	CONSTANT VARCHAR(255)	:= 'PKG_UNAUTH_CAST_INVITE_SCHE.LOCK_EX';

		BUF_TRY	NUMBER(1);
	BEGIN

		BUF_TRY	:= 0;

		LOOP
			BEGIN
				LOCK TABLE T_UNAUTH_CAST_INVITE_SCHE IN EXCLUSIVE MODE NOWAIT;
				BUF_TRY := 0;
			EXCEPTION
				WHEN APP_COMM.ALREADY_LOCKED THEN
					BUF_TRY := BUF_TRY + 1;
					DBMS_LOCK.SLEEP(1);
			END;
			EXIT WHEN (BUF_TRY = 0) OR (BUF_TRY > APP_COMM.MAX_TRY);
		END LOOP;

		IF (BUF_TRY != 0) THEN
			LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,PGM_NM);
			RAISE APP_COMM.ALREADY_LOCKED;
		END IF;

	END LOCK_EX;

	/*------------------------------*/
	/* Close All					*/
	/*------------------------------*/
	PROCEDURE CLOSE_ALL
	IS
	BEGIN
		IF (CS01%ISOPEN) THEN
			CLOSE CS01;
		END IF;

		IF (CS02%ISOPEN) THEN
			CLOSE CS02;
		END IF;

		IF (CS03%ISOPEN) THEN
			CLOSE CS03;
		END IF;

	END CLOSE_ALL;

END PKG_UNAUTH_CAST_INVITE_SCHE;
/
SHOW ERROR;
