/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: ﾃｰﾌﾞﾙ管理
--  Sub System Name	: 共通
--  Title			: 広告集計 コード別集計(月別) T_AD_DATA_USER_MONTHLY Package
--	Progaram ID		: PKG_AD_DATA_USER_MONTHLY
--	Compile Turn	: 0
--  Creation Date	: 14.06.11
--	Update Date		:
--  Author			: M&TT Y.Ikemiya
/*************************************************************************/
-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PACKAGE PKG_AD_DATA_USER_MONTHLY AS

	CURSOR CS01(prmSITE_CD		T_AD_DATA_USER_MONTHLY.SITE_CD		%TYPE,
				prmAD_CD		T_AD_DATA_USER_MONTHLY.AD_CD		%TYPE,
				prmREPORT_MONTH	T_AD_DATA_USER_MONTHLY.REPORT_MONTH	%TYPE
	) IS
		SELECT
			ROWID						,
			T_AD_DATA_USER_MONTHLY.*
		FROM
			T_AD_DATA_USER_MONTHLY
		WHERE
			SITE_CD			= prmSITE_CD		AND
			AD_CD			= prmAD_CD			AND
			REPORT_MONTH	= prmREPORT_MONTH
		FOR UPDATE WAIT 5;

	PROCEDURE LOCK_EX;

	PROCEDURE CLOSE_ALL;

END PKG_AD_DATA_USER_MONTHLY;
/
SHOW ERROR;


CREATE OR REPLACE PACKAGE BODY PKG_AD_DATA_USER_MONTHLY AS

	/*------------------------------*/
	/* Lock Table					*/
	/*------------------------------*/
	PROCEDURE LOCK_EX
	IS
		--	CONSTANT
		PGM_NM 	CONSTANT VARCHAR(255)	:= 'PKG_AD_DATA_USER_MONTHLY.LOCK_EX';

		BUF_TRY	NUMBER(1);
	BEGIN

		BUF_TRY	:= 0;

		LOOP
			BEGIN
				LOCK TABLE T_AD_DATA_USER_MONTHLY IN EXCLUSIVE MODE NOWAIT;
				BUF_TRY := 0;
			EXCEPTION
				WHEN APP_COMM.ALREADY_LOCKED THEN
					BUF_TRY := BUF_TRY + 1;
					DBMS_LOCK.SLEEP(1);
			END;
			EXIT WHEN (BUF_TRY = 0) OR (BUF_TRY > APP_COMM.MAX_TRY);
		END LOOP;

		IF (BUF_TRY != 0) THEN
			LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,PGM_NM);
			RAISE APP_COMM.ALREADY_LOCKED;
		END IF;

	END LOCK_EX;

	/*------------------------------*/
	/* Close All					*/
	/*------------------------------*/
	PROCEDURE CLOSE_ALL
	IS
	BEGIN
		IF (CS01%ISOPEN) THEN
			CLOSE CS01;
		END IF;

	END CLOSE_ALL;

END PKG_AD_DATA_USER_MONTHLY;
/
SHOW ERROR;
