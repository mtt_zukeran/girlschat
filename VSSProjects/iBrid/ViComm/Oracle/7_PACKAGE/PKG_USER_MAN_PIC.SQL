/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: Γ°ΜήΩΗ
--  Sub System Name	: €Κ
--  Title			: j«ουΚ^ T_USER_MAN_PIC Package
--	Progaram ID		: PKG_USER_MAN_PIC
--	Compile Turn	: 0
--  Creation Date	: 09.08.19
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PACKAGE PKG_USER_MAN_PIC AS

	CURSOR CS01 IS SELECT ROWID,T_USER_MAN_PIC.* FROM T_USER_MAN_PIC ORDER BY SITE_CD,USER_SEQ,USER_CHAR_NO
		FOR UPDATE WAIT 5;

	CURSOR CS02(prmSITE_CD		T_USER_MAN_PIC.SITE_CD		%TYPE,
				prmUSER_SEQ		T_USER_MAN_PIC.USER_SEQ		%TYPE,
				prmUSER_CHAR_NO	T_USER_MAN_PIC.USER_CHAR_NO	%TYPE,
				prmPIC_SEQ		T_USER_MAN_PIC.PIC_SEQ		%TYPE) IS SELECT ROWID,T_USER_MAN_PIC.* FROM T_USER_MAN_PIC
		WHERE
			SITE_CD			= prmSITE_CD		AND
			USER_SEQ		= prmUSER_SEQ		AND
			USER_CHAR_NO	= prmUSER_CHAR_NO	AND
			PIC_SEQ			= prmPIC_SEQ
		FOR UPDATE WAIT 5;

	CURSOR CS03(prmROWID ROWID) IS SELECT REVISION_NO FROM T_USER_MAN_PIC
		WHERE
			ROWID = prmROWID
		FOR UPDATE WAIT 5;

	PROCEDURE LOCK_EX;

	PROCEDURE CLOSE_ALL;

END PKG_USER_MAN_PIC;
/
SHOW ERROR;


CREATE OR REPLACE PACKAGE BODY PKG_USER_MAN_PIC AS

	/*------------------------------*/
	/* Lock Table					*/
	/*------------------------------*/
	PROCEDURE LOCK_EX
	IS
		--	CONSTANT
		PGM_NM 	CONSTANT VARCHAR(255)	:= 'PKG_USER_MAN_PIC.LOCK_EX';

		BUF_TRY	NUMBER(1);
	BEGIN

		BUF_TRY	:= 0;

		LOOP
			BEGIN
				LOCK TABLE T_USER_MAN_PIC IN EXCLUSIVE MODE NOWAIT;
				BUF_TRY := 0;
			EXCEPTION
				WHEN APP_COMM.ALREADY_LOCKED THEN
					BUF_TRY := BUF_TRY + 1;
					DBMS_LOCK.SLEEP(1);
			END;
			EXIT WHEN (BUF_TRY = 0) OR (BUF_TRY > APP_COMM.MAX_TRY);
		END LOOP;
	
		IF (BUF_TRY != 0) THEN
			LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,PGM_NM);
			RAISE APP_COMM.ALREADY_LOCKED;
		END IF;

	END LOCK_EX;

	/*------------------------------*/
	/* Close All					*/
	/*------------------------------*/
	PROCEDURE CLOSE_ALL
	IS
	BEGIN
		IF (CS01%ISOPEN) THEN
			CLOSE CS01;
		END IF;

		IF (CS02%ISOPEN) THEN
			CLOSE CS02;
		END IF;

		IF (CS03%ISOPEN) THEN
			CLOSE CS03;
		END IF;

	END CLOSE_ALL;

END PKG_USER_MAN_PIC;
/
SHOW ERROR;
