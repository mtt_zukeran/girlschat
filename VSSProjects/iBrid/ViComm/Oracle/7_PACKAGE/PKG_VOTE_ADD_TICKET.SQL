/*************************************************************************/
--	System			: ViComm
--	Title			: T_VOTE_ADD_TICKET Package
--	Progaram ID		: PKG_VOTE_ADD_TICKET
--	Compile Turn	: 0
--	Creation Date	: 13.10.18
--	Update Date		:
--	Author			: K.Miyazato
/*************************************************************************/
CREATE OR REPLACE PACKAGE PKG_VOTE_ADD_TICKET AS
	CURSOR CS01(
		prmSITE_CD			T_VOTE_ADD_TICKET.SITE_CD%TYPE,
		prmVOTE_TERM_SEQ	T_VOTE_ADD_TICKET.VOTE_TERM_SEQ%TYPE,
		prmMAN_USER_SEQ		T_VOTE_ADD_TICKET.MAN_USER_SEQ%TYPE
	)IS
		SELECT
			ROWID,
			T_VOTE_ADD_TICKET.*
		FROM
			T_VOTE_ADD_TICKET
		WHERE
			SITE_CD			= prmSITE_CD		AND
			VOTE_TERM_SEQ	= prmVOTE_TERM_SEQ	AND
			MAN_USER_SEQ	= prmMAN_USER_SEQ
		FOR UPDATE WAIT 5;

	PROCEDURE LOCK_EX;
	PROCEDURE CLOSE_ALL;

END PKG_VOTE_ADD_TICKET;
/
SHOW ERROR;


CREATE OR REPLACE PACKAGE BODY PKG_VOTE_ADD_TICKET AS
	/*------------------------------*/
	/* Lock Table					*/
	/*------------------------------*/
	PROCEDURE LOCK_EX
	IS
		--	CONSTANT
		PGM_NM	CONSTANT VARCHAR(255)	:= 'PKG_VOTE_ADD_TICKET.LOCK_EX';
		BUF_TRY	NUMBER(1);
	BEGIN
		BUF_TRY	:= 0;

		LOOP
			BEGIN
				LOCK TABLE T_VOTE_ADD_TICKET IN EXCLUSIVE MODE NOWAIT;
				BUF_TRY := 0;
			EXCEPTION
				WHEN APP_COMM.ALREADY_LOCKED THEN
					BUF_TRY := BUF_TRY + 1;
					DBMS_LOCK.SLEEP(1);
			END;
			EXIT WHEN (BUF_TRY = 0) OR (BUF_TRY > APP_COMM.MAX_TRY);
		END LOOP;

		IF (BUF_TRY != 0) THEN
			LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,PGM_NM);
			RAISE APP_COMM.ALREADY_LOCKED;
		END IF;
	END LOCK_EX;

	/*------------------------------*/
	/* Close All					*/
	/*------------------------------*/
	PROCEDURE CLOSE_ALL
	IS
	BEGIN
		IF (CS01%ISOPEN) THEN
			CLOSE CS01;
		END IF;

	END CLOSE_ALL;

END PKG_VOTE_ADD_TICKET;
/
SHOW ERROR;