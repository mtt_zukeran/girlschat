/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: ð��يǗ�
--  Sub System Name	: ����
--  Title			: �� T_URGE Package
--	Progaram ID		: PKG_URGE
--	Compile Turn	: 0
--  Creation Date	: 07.11.01
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PACKAGE PKG_URGE AS

	CURSOR CS01 IS SELECT ROWID,T_URGE.* FROM T_URGE ORDER BY USER_SEQ
		FOR UPDATE WAIT 5;

	CURSOR CS02(prmUSER_SEQ	T_URGE.USER_SEQ	%TYPE) IS SELECT ROWID,T_URGE.* FROM T_URGE
		WHERE
			USER_SEQ	= prmUSER_SEQ	
		FOR UPDATE WAIT 5;

	CURSOR CS03(prmROWID ROWID) IS SELECT REVISION_NO FROM T_URGE
		WHERE
			ROWID = prmROWID
		FOR UPDATE WAIT 5;

	CURSOR CS04(prmUSER_SEQ	T_URGE.USER_SEQ%TYPE) IS SELECT URGE_CONNECT_COUNT,URGE_EXEC_COUNT FROM T_URGE
		WHERE
			USER_SEQ	= prmUSER_SEQ	
		FOR UPDATE WAIT 5;

	PROCEDURE LOCK_EX;
	PROCEDURE CLOSE_ALL;

END PKG_URGE;
/
SHOW ERROR;


CREATE OR REPLACE PACKAGE BODY PKG_URGE AS

	/*------------------------------*/
	/* Lock Table					*/
	/*------------------------------*/
	PROCEDURE LOCK_EX
	IS
		--	CONSTANT
		PGM_NM 	CONSTANT VARCHAR(255)	:= 'PKG_URGE.LOCK_EX';

		BUF_TRY	NUMBER(1);
	BEGIN

		BUF_TRY	:= 0;

		LOOP
			BEGIN
				LOCK TABLE T_URGE IN EXCLUSIVE MODE NOWAIT;
				BUF_TRY := 0;
			EXCEPTION
				WHEN APP_COMM.ALREADY_LOCKED THEN
					BUF_TRY := BUF_TRY + 1;
					DBMS_LOCK.SLEEP(1);
			END;
			EXIT WHEN (BUF_TRY = 0) OR (BUF_TRY > APP_COMM.MAX_TRY);
		END LOOP;
	
		IF (BUF_TRY != 0) THEN
			LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,PGM_NM);
			RAISE APP_COMM.ALREADY_LOCKED;
		END IF;

	END LOCK_EX;


	/*------------------------------*/
	/* Close All					*/
	/*------------------------------*/
	PROCEDURE CLOSE_ALL
	IS
	BEGIN
		IF (CS01%ISOPEN) THEN
			CLOSE CS01;
		END IF;

		IF (CS02%ISOPEN) THEN
			CLOSE CS02;
		END IF;

		IF (CS03%ISOPEN) THEN
			CLOSE CS03;
		END IF;

		IF (CS04%ISOPEN) THEN
			CLOSE CS04;
		END IF;

	END CLOSE_ALL;

END PKG_URGE;
/
SHOW ERROR;
