/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: テーブル管理
--  Sub System Name	: 共通
--  Title			: アイドルテレビ オーディション投票設定(エントリー式） T_ELECTION_PERIOD Package
--	Progaram ID		: PKG_ELECTION_PERIOD
--	Compile Turn	: 0
--  Creation Date	: 13.11.30
--	Update Date		:
--  Author			: M&TT Y.Ikemiya
/*************************************************************************/
-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PACKAGE PKG_ELECTION_PERIOD AS

	CURSOR CS01 (
		prmSITE_CD						T_ELECTION_PERIOD.SITE_CD					%TYPE	,
		prmELECTION_PERIOD_SEQ			T_ELECTION_PERIOD.ELECTION_PERIOD_SEQ		%TYPE
	) IS
		SELECT
			ROWID,T_ELECTION_PERIOD.*
		FROM
			T_ELECTION_PERIOD
		WHERE
			T_ELECTION_PERIOD.SITE_CD					= prmSITE_CD			AND
			T_ELECTION_PERIOD.ELECTION_PERIOD_SEQ		= prmELECTION_PERIOD_SEQ
		FOR UPDATE WAIT 5;

	PROCEDURE LOCK_EX;

	PROCEDURE CLOSE_ALL;

END PKG_ELECTION_PERIOD;
/
SHOW ERROR;


CREATE OR REPLACE PACKAGE BODY PKG_ELECTION_PERIOD AS

	/*------------------------------*/
	/* Lock Table					*/
	/*------------------------------*/
	PROCEDURE LOCK_EX
	IS
		--	CONSTANT
		PGM_NM 	CONSTANT VARCHAR(255)	:= 'PKG_ELECTION_PERIOD.LOCK_EX';

		BUF_TRY	NUMBER(1);
	BEGIN

		BUF_TRY	:= 0;

		LOOP
			BEGIN
				LOCK TABLE T_ELECTION_PERIOD IN EXCLUSIVE MODE NOWAIT;
				BUF_TRY := 0;
			EXCEPTION
				WHEN APP_COMM.ALREADY_LOCKED THEN
					BUF_TRY := BUF_TRY + 1;
					DBMS_LOCK.SLEEP(1);
			END;
			EXIT WHEN (BUF_TRY = 0) OR (BUF_TRY > APP_COMM.MAX_TRY);
		END LOOP;
	
		IF (BUF_TRY != 0) THEN
			LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,PGM_NM);
			RAISE APP_COMM.ALREADY_LOCKED;
		END IF;

	END LOCK_EX;

	/*------------------------------*/
	/* Close All					*/
	/*------------------------------*/
	PROCEDURE CLOSE_ALL
	IS
	BEGIN
		IF (CS01%ISOPEN) THEN
			CLOSE CS01;
		END IF;

	END CLOSE_ALL;

END PKG_ELECTION_PERIOD;
/
SHOW ERROR;
