/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: Γ°ΜήΩΗ
--  Sub System Name	: €Κ
--  Title			: VKo^Uͺ―ΐΡ T_DISPATCH_REGIST_REPORT
--	Progaram ID		: PKG_DISPATCH_REGIST_REPORT
--	Compile Turn	: 0
--  Creation Date	: 10.03.16
--	Update Date		:
--  Author			: i-Brid(A.Koyanagi)
/*************************************************************************/
-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PACKAGE PKG_DISPATCH_REGIST_REPORT AS

	CURSOR CS01 IS SELECT ROWID,T_DISPATCH_REGIST_REPORT.* FROM T_DISPATCH_REGIST_REPORT ORDER BY PARTNER_SITE_SEQ, REPORT_DAY
		FOR UPDATE WAIT 5;

	CURSOR CS02(prmPARTNER_SITE_SEQ T_DISPATCH_REGIST_REPORT.PARTNER_SITE_SEQ%TYPE	,
				prmREPORT_DAY		T_DISPATCH_REGIST_REPORT.REPORT_DAY%TYPE		) IS SELECT ROWID,T_DISPATCH_REGIST_REPORT.* FROM T_DISPATCH_REGIST_REPORT
		WHERE
			PARTNER_SITE_SEQ	= prmPARTNER_SITE_SEQ	AND
			REPORT_DAY			= prmREPORT_DAY
		FOR UPDATE WAIT 5;

	PROCEDURE LOCK_EX;

	PROCEDURE CLOSE_ALL;

END PKG_DISPATCH_REGIST_REPORT;
/
SHOW ERROR;


CREATE OR REPLACE PACKAGE BODY PKG_DISPATCH_REGIST_REPORT AS

	/*------------------------------*/
	/* Lock Table					*/
	/*------------------------------*/
	PROCEDURE LOCK_EX
	IS
		--	CONSTANT
		PGM_NM 	CONSTANT VARCHAR(255)	:= 'PKG_DISPATCH_REGIST_REPORT.LOCK_EX';

		BUF_TRY	NUMBER(1);
	BEGIN

		BUF_TRY	:= 0;

		LOOP
			BEGIN
				LOCK TABLE T_DISPATCH_REGIST_REPORT IN EXCLUSIVE MODE NOWAIT;
				BUF_TRY := 0;
			EXCEPTION
				WHEN APP_COMM.ALREADY_LOCKED THEN
					BUF_TRY := BUF_TRY + 1;
					DBMS_LOCK.SLEEP(1);
			END;
			EXIT WHEN (BUF_TRY = 0) OR (BUF_TRY > APP_COMM.MAX_TRY);
		END LOOP;

		IF (BUF_TRY != 0) THEN
			LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,PGM_NM);
			RAISE APP_COMM.ALREADY_LOCKED;
		END IF;

	END LOCK_EX;

	/*------------------------------*/
	/* Close All					*/
	/*------------------------------*/
	PROCEDURE CLOSE_ALL
	IS
	BEGIN
		IF (CS01%ISOPEN) THEN
			CLOSE CS01;
		END IF;

		IF (CS02%ISOPEN) THEN
			CLOSE CS02;
		END IF;

	END CLOSE_ALL;

END PKG_DISPATCH_REGIST_REPORT;
/
SHOW ERROR;
