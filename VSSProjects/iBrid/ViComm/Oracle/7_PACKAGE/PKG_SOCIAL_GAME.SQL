/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: テーブル管理
--  Sub System Name	: 共通
--  Title			: ソーシャルゲーム設定 T_SOCIAL_GAME Package
--	Progaram ID		: PKG_SOCIAL_GAME
--	Compile Turn	: 0
--  Creation Date	: 11.03.28
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PACKAGE PKG_SOCIAL_GAME AS

	CURSOR CS01 IS SELECT ROWID,T_SOCIAL_GAME.* FROM T_SOCIAL_GAME ORDER BY SITE_CD
		FOR UPDATE WAIT 5;

	CURSOR CS02 (
		prmSITE_CD			T_SOCIAL_GAME.SITE_CD	%TYPE
	) IS
		SELECT
			ROWID,T_SOCIAL_GAME.*
		FROM
			T_SOCIAL_GAME
		WHERE
			T_SOCIAL_GAME.SITE_CD	= prmSITE_CD
		FOR UPDATE WAIT 5;

	CURSOR CS03(prmROWID ROWID) IS SELECT REVISION_NO FROM T_SOCIAL_GAME
		WHERE
			ROWID = prmROWID
		FOR UPDATE WAIT 5;

	PROCEDURE LOCK_EX;

	PROCEDURE CLOSE_ALL;

END PKG_SOCIAL_GAME;
/
SHOW ERROR;


CREATE OR REPLACE PACKAGE BODY PKG_SOCIAL_GAME AS

	/*------------------------------*/
	/* Lock Table					*/
	/*------------------------------*/
	PROCEDURE LOCK_EX
	IS
		--	CONSTANT
		PGM_NM 	CONSTANT VARCHAR(255)	:= 'PKG_SOCIAL_GAME.LOCK_EX';

		BUF_TRY	NUMBER(1);
	BEGIN

		BUF_TRY	:= 0;

		LOOP
			BEGIN
				LOCK TABLE T_SOCIAL_GAME IN EXCLUSIVE MODE NOWAIT;
				BUF_TRY := 0;
			EXCEPTION
				WHEN APP_COMM.ALREADY_LOCKED THEN
					BUF_TRY := BUF_TRY + 1;
					DBMS_LOCK.SLEEP(1);
			END;
			EXIT WHEN (BUF_TRY = 0) OR (BUF_TRY > APP_COMM.MAX_TRY);
		END LOOP;
	
		IF (BUF_TRY != 0) THEN
			LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,PGM_NM);
			RAISE APP_COMM.ALREADY_LOCKED;
		END IF;

	END LOCK_EX;

	/*------------------------------*/
	/* Close All					*/
	/*------------------------------*/
	PROCEDURE CLOSE_ALL
	IS
	BEGIN
		IF (CS01%ISOPEN) THEN
			CLOSE CS01;
		END IF;

		IF (CS02%ISOPEN) THEN
			CLOSE CS02;
		END IF;

		IF (CS03%ISOPEN) THEN
			CLOSE CS03;
		END IF;

	END CLOSE_ALL;

END PKG_SOCIAL_GAME;
/
SHOW ERROR;
