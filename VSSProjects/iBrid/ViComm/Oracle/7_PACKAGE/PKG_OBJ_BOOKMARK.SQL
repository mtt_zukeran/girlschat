/*************************************************************************/
--	System			: ViComm
--	Title			: お宝ブックマーク T_OBJ_BOOKMARK Package
--	Progaram ID		: PKG_OBJ_BOOKMARK
--	Compile Turn	: 0
--	Creation Date	: 12.07.31
--	Update Date		:
--	Author			: PW K.Miyazato
/*************************************************************************/
CREATE OR REPLACE PACKAGE PKG_OBJ_BOOKMARK AS

	CURSOR CS01(
		prmSITE_CD		T_OBJ_BOOKMARK.SITE_CD%TYPE,
		prmUSER_SEQ		T_OBJ_BOOKMARK.USER_SEQ%TYPE,
		prmUSER_CHAR_NO	T_OBJ_BOOKMARK.USER_CHAR_NO%TYPE,
		prmOBJ_SEQ		T_OBJ_BOOKMARK.OBJ_SEQ%TYPE
	) IS SELECT ROWID,T_OBJ_BOOKMARK.* FROM T_OBJ_BOOKMARK
		WHERE
			SITE_CD			= prmSITE_CD				AND
			USER_SEQ		= prmUSER_SEQ				AND
			USER_CHAR_NO	= prmUSER_CHAR_NO			AND
			OBJ_SEQ			= prmOBJ_SEQ
		FOR UPDATE WAIT 5;

	PROCEDURE LOCK_EX;

	PROCEDURE CLOSE_ALL;

END PKG_OBJ_BOOKMARK;
/
SHOW ERROR;


CREATE OR REPLACE PACKAGE BODY PKG_OBJ_BOOKMARK AS

	/*------------------------------*/
	/* Lock Table					*/
	/*------------------------------*/
	PROCEDURE LOCK_EX
	IS
		--	CONSTANT
		PGM_NM	CONSTANT VARCHAR(255)	:= 'PKG_OBJ_BOOKMARK.LOCK_EX';

		BUF_TRY	NUMBER(1);
	BEGIN

		BUF_TRY	:= 0;

		LOOP
			BEGIN
				LOCK TABLE T_OBJ_BOOKMARK IN EXCLUSIVE MODE NOWAIT;
				BUF_TRY := 0;
			EXCEPTION
				WHEN APP_COMM.ALREADY_LOCKED THEN
					BUF_TRY := BUF_TRY + 1;
					DBMS_LOCK.SLEEP(1);
			END;
			EXIT WHEN (BUF_TRY = 0) OR (BUF_TRY > APP_COMM.MAX_TRY);
		END LOOP;

		IF (BUF_TRY != 0) THEN
			LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,PGM_NM);
			RAISE APP_COMM.ALREADY_LOCKED;
		END IF;

	END LOCK_EX;

	/*------------------------------*/
	/* Close All					*/
	/*------------------------------*/
	PROCEDURE CLOSE_ALL
	IS
	BEGIN
		IF (CS01%ISOPEN) THEN
			CLOSE CS01;
		END IF;

	END CLOSE_ALL;

END PKG_OBJ_BOOKMARK;
/
SHOW ERROR;