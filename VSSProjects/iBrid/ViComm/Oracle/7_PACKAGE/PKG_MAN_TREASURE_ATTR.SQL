/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: ð��يǗ�
--  Sub System Name	: ����
--  Title			: �o���ґ������ T_MAN_TREASURE_ATTR Package
--	Progaram ID		: PKG_MAN_TREASURE_ATTR
--	Compile Turn	: 0
--  Creation Date	: 09.06.01
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PACKAGE PKG_MAN_TREASURE_ATTR AS

	CURSOR CS01 IS SELECT ROWID,T_MAN_TREASURE_ATTR.* FROM T_MAN_TREASURE_ATTR ORDER BY SITE_CD,CAST_GAME_PIC_ATTR_SEQ
		FOR UPDATE WAIT 5;

	CURSOR CS02(prmCAST_GAME_PIC_ATTR_SEQ	T_MAN_TREASURE_ATTR.CAST_GAME_PIC_ATTR_SEQ%TYPE) IS SELECT ROWID,T_MAN_TREASURE_ATTR.* FROM T_MAN_TREASURE_ATTR
		WHERE
			CAST_GAME_PIC_ATTR_SEQ	= prmCAST_GAME_PIC_ATTR_SEQ
		FOR UPDATE WAIT 5;

	CURSOR CS03(prmROWID ROWID) IS SELECT REVISION_NO FROM T_MAN_TREASURE_ATTR
		WHERE
			ROWID = prmROWID
		FOR UPDATE WAIT 5;

	PROCEDURE LOCK_EX;

	PROCEDURE CLOSE_ALL;

END PKG_MAN_TREASURE_ATTR;
/
SHOW ERROR;


CREATE OR REPLACE PACKAGE BODY PKG_MAN_TREASURE_ATTR AS

	/*------------------------------*/
	/* Lock Table					*/
	/*------------------------------*/
	PROCEDURE LOCK_EX
	IS
		--	CONSTANT
		PGM_NM 	CONSTANT VARCHAR(255)	:= 'PKG_MAN_TREASURE_ATTR.LOCK_EX';

		BUF_TRY	NUMBER(1);
	BEGIN

		BUF_TRY	:= 0;

		LOOP
			BEGIN
				LOCK TABLE T_MAN_TREASURE_ATTR IN EXCLUSIVE MODE NOWAIT;
				BUF_TRY := 0;
			EXCEPTION
				WHEN APP_COMM.ALREADY_LOCKED THEN
					BUF_TRY := BUF_TRY + 1;
					DBMS_LOCK.SLEEP(1);
			END;
			EXIT WHEN (BUF_TRY = 0) OR (BUF_TRY > APP_COMM.MAX_TRY);
		END LOOP;
	
		IF (BUF_TRY != 0) THEN
			LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,PGM_NM);
			RAISE APP_COMM.ALREADY_LOCKED;
		END IF;

	END LOCK_EX;

	/*------------------------------*/
	/* Close All					*/
	/*------------------------------*/
	PROCEDURE CLOSE_ALL
	IS
	BEGIN
		IF (CS01%ISOPEN) THEN
			CLOSE CS01;
		END IF;

		IF (CS02%ISOPEN) THEN
			CLOSE CS02;
		END IF;

		IF (CS03%ISOPEN) THEN
			CLOSE CS03;
		END IF;

	END CLOSE_ALL;

END PKG_MAN_TREASURE_ATTR;
/
SHOW ERROR;
