/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: ð��يǗ�
--  Sub System Name	: ����
--  Title			: �L���ʉғ��� T_DAILY_AD_SALES Package
--	Progaram ID		: PKG_DAILY_AD_SALES
--	Compile Turn	: 0
--  Creation Date	: 09.06.01
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PACKAGE PKG_DAILY_AD_SALES AS

	CURSOR CS01 IS SELECT ROWID,T_DAILY_AD_SALES.* FROM T_DAILY_AD_SALES ORDER BY SITE_CD,REPORT_DAY
		FOR UPDATE WAIT 5;

	CURSOR CS02(prmSITE_CD		T_DAILY_AD_SALES.SITE_CD	%TYPE,
				prmREPORT_DAY	T_DAILY_AD_SALES.REPORT_DAY	%TYPE,
				prmAD_CD		T_DAILY_AD_SALES.AD_CD		%TYPE) IS SELECT ROWID,T_DAILY_AD_SALES.* FROM T_DAILY_AD_SALES
		WHERE
			SITE_CD		= prmSITE_CD	AND
			REPORT_DAY	= prmREPORT_DAY	AND
			AD_CD		= prmAD_CD
		FOR UPDATE WAIT 5;

	CURSOR CS_DEL(
			prmSITE_CD		T_DAILY_AD_SALES.SITE_CD%TYPE,
			prmREPORT_DAY	T_DAILY_AD_SALES.REPORT_DAY%TYPE) IS SELECT ROWID FROM T_DAILY_AD_SALES
		WHERE
			SITE_CD = prmSITE_CD AND REPORT_DAY <= prmREPORT_DAY
		FOR UPDATE WAIT 10;


	PROCEDURE LOCK_EX;
	PROCEDURE CLOSE_ALL;

END PKG_DAILY_AD_SALES;
/
SHOW ERROR;


CREATE OR REPLACE PACKAGE BODY PKG_DAILY_AD_SALES AS

	/*------------------------------*/
	/* Lock Table					*/
	/*------------------------------*/
	PROCEDURE LOCK_EX
	IS
		--	CONSTANT
		PGM_NM 	CONSTANT VARCHAR(255)	:= 'PKG_DAILY_AD_SALES.LOCK_EX';

		BUF_TRY	NUMBER(1);
	BEGIN

		BUF_TRY	:= 0;

		LOOP
			BEGIN
				LOCK TABLE T_DAILY_AD_SALES IN EXCLUSIVE MODE NOWAIT;
				BUF_TRY := 0;
			EXCEPTION
				WHEN APP_COMM.ALREADY_LOCKED THEN
					BUF_TRY := BUF_TRY + 1;
					DBMS_LOCK.SLEEP(1);
			END;
			EXIT WHEN (BUF_TRY = 0) OR (BUF_TRY > APP_COMM.MAX_TRY);
		END LOOP;
	
		IF (BUF_TRY != 0) THEN
			LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,PGM_NM);
			RAISE APP_COMM.ALREADY_LOCKED;
		END IF;

	END LOCK_EX;

	/*------------------------------*/
	/* Close All					*/
	/*------------------------------*/
	PROCEDURE CLOSE_ALL
	IS
	BEGIN
		IF (CS01%ISOPEN) THEN
			CLOSE CS01;
		END IF;

		IF (CS02%ISOPEN) THEN
			CLOSE CS02;
		END IF;

		IF (CS_DEL%ISOPEN) THEN
			CLOSE CS_DEL;
		END IF;

	END CLOSE_ALL;

END PKG_DAILY_AD_SALES;
/
SHOW ERROR;
