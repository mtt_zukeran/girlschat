/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: ﾃｰﾌﾞﾙ管理
--  Sub System Name	: 共通
--  Title			: パック設定 T_PACK Package
--	Progaram ID		: PKG_PACK
--	Compile Turn	: 0
--  Creation Date	: 09.12.14
--	Update Date		:
--  Author			: i-Brid(T.Tokunaga)
/*************************************************************************/
-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PACKAGE PKG_PACK AS

	CURSOR CS01 IS SELECT ROWID,T_PACK.* FROM T_PACK ORDER BY SITE_CD FOR UPDATE WAIT 5;

	CURSOR CS02(prmSITE_CD				T_PACK.SITE_CD%TYPE		,
				prmSETTLE_TYPE			T_PACK.SETTLE_TYPE%TYPE	,
				prmSALES_AMT			T_PACK.SALES_AMT%TYPE	
	) IS SELECT ROWID,T_PACK.* FROM T_PACK
		WHERE(
			SITE_CD 			= prmSITE_CD	 		AND
			SETTLE_TYPE			= prmSETTLE_TYPE		AND
			SALES_AMT			= prmSALES_AMT
		)FOR UPDATE WAIT 5;

	CURSOR CS03(prmROWID ROWID) IS SELECT REVISION_NO FROM T_PACK
		WHERE
			ROWID = prmROWID
		FOR UPDATE WAIT 5;


	PROCEDURE LOCK_EX;

	PROCEDURE CLOSE_ALL;

END PKG_PACK;
/
SHOW ERROR;

CREATE OR REPLACE PACKAGE BODY PKG_PACK AS

	/*------------------------------*/
	/* Lock Table					*/
	/*------------------------------*/
	PROCEDURE LOCK_EX
	IS
		--	CONSTANT
		PGM_NM 	CONSTANT VARCHAR(255)	:= 'PKG_PACK.LOCK_EX';

		BUF_TRY	NUMBER(1);
	BEGIN

		BUF_TRY	:= 0;

		LOOP
			BEGIN
				LOCK TABLE T_PACK IN EXCLUSIVE MODE NOWAIT;
				BUF_TRY := 0;
			EXCEPTION
				WHEN APP_COMM.ALREADY_LOCKED THEN
					BUF_TRY := BUF_TRY + 1;
					DBMS_LOCK.SLEEP(1);
			END;
			EXIT WHEN (BUF_TRY = 0) OR (BUF_TRY > APP_COMM.MAX_TRY);
		END LOOP;

		IF (BUF_TRY != 0) THEN
			LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,PGM_NM);
			RAISE APP_COMM.ALREADY_LOCKED;
		END IF;

	END LOCK_EX;

	/*------------------------------*/
	/* Close All					*/
	/*------------------------------*/
	PROCEDURE CLOSE_ALL
	IS
	BEGIN
		IF (CS01%ISOPEN) THEN
			CLOSE CS01;
		END IF;

		IF (CS02%ISOPEN) THEN
			CLOSE CS02;
		END IF;

		IF (CS03%ISOPEN) THEN
			CLOSE CS03;
		END IF;

	END CLOSE_ALL;

END PKG_PACK;
/
SHOW ERROR;
