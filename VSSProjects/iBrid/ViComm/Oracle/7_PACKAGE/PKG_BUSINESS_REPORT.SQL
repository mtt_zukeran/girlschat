/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: Γ°ΜήΩΗ
--  Sub System Name	: €Κ
--  Title			: Ζ±A T_BUSINESS_REPORT Package
--	Progaram ID		: PKG_BUSINESS_REPORT
--	Compile Turn	: 0
--  Creation Date	: 09.06.01
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PACKAGE PKG_BUSINESS_REPORT AS

	CURSOR CS01 IS SELECT ROWID,T_BUSINESS_REPORT.* FROM T_BUSINESS_REPORT ORDER BY PRODUCTION_SEQ
		FOR UPDATE WAIT 5;

	CURSOR CS02(prmDOC_SEQ	T_BUSINESS_REPORT.DOC_SEQ%TYPE) IS SELECT ROWID,T_BUSINESS_REPORT.* FROM T_BUSINESS_REPORT
		WHERE
			DOC_SEQ			= prmDOC_SEQ
		FOR UPDATE WAIT 5;

	CURSOR CS03(prmROWID ROWID) IS SELECT REVISION_NO FROM T_BUSINESS_REPORT
		WHERE
			ROWID = prmROWID
		FOR UPDATE WAIT 5;

	PROCEDURE LOCK_EX;
	PROCEDURE CLOSE_ALL;

END PKG_BUSINESS_REPORT;
/
SHOW ERROR;


CREATE OR REPLACE PACKAGE BODY PKG_BUSINESS_REPORT AS

	/*------------------------------*/
	/* Lock Table					*/
	/*------------------------------*/
	PROCEDURE LOCK_EX
	IS
		--	CONSTANT
		PGM_NM 	CONSTANT VARCHAR(255)	:= 'PKG_BUSINESS_REPORT.LOCK_EX';

		BUF_TRY	NUMBER(1);
	BEGIN

		BUF_TRY	:= 0;

		LOOP
			BEGIN
				LOCK TABLE T_BUSINESS_REPORT IN EXCLUSIVE MODE NOWAIT;
				BUF_TRY := 0;
			EXCEPTION
				WHEN APP_COMM.ALREADY_LOCKED THEN
					BUF_TRY := BUF_TRY + 1;
					DBMS_LOCK.SLEEP(1);
			END;
			EXIT WHEN (BUF_TRY = 0) OR (BUF_TRY > APP_COMM.MAX_TRY);
		END LOOP;
	
		IF (BUF_TRY != 0) THEN
			LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,PGM_NM);
			RAISE APP_COMM.ALREADY_LOCKED;
		END IF;

	END LOCK_EX;


	/*------------------------------*/
	/* Close All					*/
	/*------------------------------*/
	PROCEDURE CLOSE_ALL
	IS
	BEGIN
		IF (CS01%ISOPEN) THEN
			CLOSE CS01;
		END IF;

		IF (CS02%ISOPEN) THEN
			CLOSE CS02;
		END IF;

		IF (CS03%ISOPEN) THEN
			CLOSE CS03;
		END IF;

	END CLOSE_ALL;

END PKG_BUSINESS_REPORT;
/
SHOW ERROR;
