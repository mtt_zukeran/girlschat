/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: テーブル管理
--  Sub System Name	: 共通
--  Title			: メールアタック(新人)送信履歴 T_MAIL_ATTACK_NEW_TX_LOG Package
--	Progaram ID		: PKG_MAIL_ATTACK_NEW_TX_LOG
--	Compile Turn	: 0
--  Creation Date	: 16.05.06
--	Update Date		:
--  Author			: M&TT Y.Ikemiya
/*************************************************************************/
-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PACKAGE PKG_MAIL_ATTACK_NEW_TX_LOG AS

	CURSOR CS01 (
		prmSITE_CD						T_MAIL_ATTACK_NEW_TX_LOG.SITE_CD					%TYPE	,
		prmTX_USER_SEQ					T_MAIL_ATTACK_NEW_TX_LOG.TX_USER_SEQ				%TYPE	,
		prmTX_CHAR_NO					T_MAIL_ATTACK_NEW_TX_LOG.TX_CHAR_NO					%TYPE	,
		prmRX_USER_SEQ					T_MAIL_ATTACK_NEW_TX_LOG.RX_USER_SEQ				%TYPE	,
		prmRX_CHAR_NO					T_MAIL_ATTACK_NEW_TX_LOG.RX_CHAR_NO					%TYPE	,
		prmRETURN_RX_FLAG				T_MAIL_ATTACK_NEW_TX_LOG.RETURN_RX_FLAG				%TYPE	,
		prmUNAVAILABLE_FLAG				T_MAIL_ATTACK_NEW_TX_LOG.UNAVAILABLE_FLAG			%TYPE
	) IS
		SELECT
			ROWID,T_MAIL_ATTACK_NEW_TX_LOG.*
		FROM
			T_MAIL_ATTACK_NEW_TX_LOG
		WHERE
			T_MAIL_ATTACK_NEW_TX_LOG.SITE_CD				= prmSITE_CD				AND
			T_MAIL_ATTACK_NEW_TX_LOG.TX_USER_SEQ			= prmTX_USER_SEQ			AND
			T_MAIL_ATTACK_NEW_TX_LOG.TX_CHAR_NO				= prmTX_CHAR_NO				AND
			T_MAIL_ATTACK_NEW_TX_LOG.RX_USER_SEQ			= prmRX_USER_SEQ			AND
			T_MAIL_ATTACK_NEW_TX_LOG.RX_CHAR_NO				= prmRX_CHAR_NO				AND
			T_MAIL_ATTACK_NEW_TX_LOG.RETURN_RX_FLAG			= prmRETURN_RX_FLAG			AND
			T_MAIL_ATTACK_NEW_TX_LOG.UNAVAILABLE_FLAG		= prmUNAVAILABLE_FLAG		AND
			T_MAIL_ATTACK_NEW_TX_LOG.MAN_REGIST_DATE		>= SYSDATE - T_MAIL_ATTACK_NEW_TX_LOG.AFTER_REGIST_DAYS
		FOR UPDATE WAIT 5;
	
	PROCEDURE LOCK_EX;

	PROCEDURE CLOSE_ALL;

END PKG_MAIL_ATTACK_NEW_TX_LOG;
/
SHOW ERROR;


CREATE OR REPLACE PACKAGE BODY PKG_MAIL_ATTACK_NEW_TX_LOG AS

	/*------------------------------*/
	/* Lock Table					*/
	/*------------------------------*/
	PROCEDURE LOCK_EX
	IS
		--	CONSTANT
		PGM_NM 	CONSTANT VARCHAR(255)	:= 'PKG_MAIL_ATTACK_NEW_TX_LOG.LOCK_EX';

		BUF_TRY	NUMBER(1);
	BEGIN

		BUF_TRY	:= 0;

		LOOP
			BEGIN
				LOCK TABLE T_MAIL_ATTACK_NEW_TX_LOG IN EXCLUSIVE MODE NOWAIT;
				BUF_TRY := 0;
			EXCEPTION
				WHEN APP_COMM.ALREADY_LOCKED THEN
					BUF_TRY := BUF_TRY + 1;
					DBMS_LOCK.SLEEP(1);
			END;
			EXIT WHEN (BUF_TRY = 0) OR (BUF_TRY > APP_COMM.MAX_TRY);
		END LOOP;
	
		IF (BUF_TRY != 0) THEN
			LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,PGM_NM);
			RAISE APP_COMM.ALREADY_LOCKED;
		END IF;

	END LOCK_EX;

	/*------------------------------*/
	/* Close All					*/
	/*------------------------------*/
	PROCEDURE CLOSE_ALL
	IS
	BEGIN
		IF (CS01%ISOPEN) THEN
			CLOSE CS01;
		END IF;

	END CLOSE_ALL;

END PKG_MAIL_ATTACK_NEW_TX_LOG;
/
SHOW ERROR;
