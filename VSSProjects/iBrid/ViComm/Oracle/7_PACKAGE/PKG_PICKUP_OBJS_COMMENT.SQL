/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: テーブル管理
--  Sub System Name	: 共通
--  Title			: ピックアップオブジェコメント T_PICKUP_OBJS_COMMENT Package
--	Progaram ID		: PKG_PICKUP_OBJS_COMMENT
--	Compile Turn	: 0
--  Creation Date	: 13.05.23
--	Update Date		:
--  Author			: M&TT Y.Ikemiya
/*************************************************************************/
-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PACKAGE PKG_PICKUP_OBJS_COMMENT AS

	CURSOR CS01 (
		prmSITE_CD						T_PICKUP_OBJS_COMMENT.SITE_CD						%TYPE	,
		prmPICKUP_OBJS_COMMENT_SEQ		T_PICKUP_OBJS_COMMENT.PICKUP_OBJS_COMMENT_SEQ		%TYPE
	) IS
		SELECT
			ROWID,T_PICKUP_OBJS_COMMENT.*
		FROM
			T_PICKUP_OBJS_COMMENT
		WHERE
			T_PICKUP_OBJS_COMMENT.SITE_CD						= prmSITE_CD					AND
			T_PICKUP_OBJS_COMMENT.PICKUP_OBJS_COMMENT_SEQ		= prmPICKUP_OBJS_COMMENT_SEQ
		FOR UPDATE WAIT 5;

	PROCEDURE LOCK_EX;

	PROCEDURE CLOSE_ALL;

END PKG_PICKUP_OBJS_COMMENT;
/
SHOW ERROR;


CREATE OR REPLACE PACKAGE BODY PKG_PICKUP_OBJS_COMMENT AS

	/*------------------------------*/
	/* Lock Table					*/
	/*------------------------------*/
	PROCEDURE LOCK_EX
	IS
		--	CONSTANT
		PGM_NM 	CONSTANT VARCHAR(255)	:= 'PKG_PICKUP_OBJS_COMMENT.LOCK_EX';

		BUF_TRY	NUMBER(1);
	BEGIN

		BUF_TRY	:= 0;

		LOOP
			BEGIN
				LOCK TABLE T_PICKUP_OBJS_COMMENT IN EXCLUSIVE MODE NOWAIT;
				BUF_TRY := 0;
			EXCEPTION
				WHEN APP_COMM.ALREADY_LOCKED THEN
					BUF_TRY := BUF_TRY + 1;
					DBMS_LOCK.SLEEP(1);
			END;
			EXIT WHEN (BUF_TRY = 0) OR (BUF_TRY > APP_COMM.MAX_TRY);
		END LOOP;
	
		IF (BUF_TRY != 0) THEN
			LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,PGM_NM);
			RAISE APP_COMM.ALREADY_LOCKED;
		END IF;

	END LOCK_EX;

	/*------------------------------*/
	/* Close All					*/
	/*------------------------------*/
	PROCEDURE CLOSE_ALL
	IS
	BEGIN
		IF (CS01%ISOPEN) THEN
			CLOSE CS01;
		END IF;

	END CLOSE_ALL;

END PKG_PICKUP_OBJS_COMMENT;
/
SHOW ERROR;
