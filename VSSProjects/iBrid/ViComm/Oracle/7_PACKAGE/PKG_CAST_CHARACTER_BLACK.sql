/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: ﾃｰﾌﾞﾙ管理
--  Sub System Name	: 共通
--  Title			: 出演者ブラックリスト設定 T_CAST_CHARACTER_BLACK Package
--	Progaram ID		: PKG_CAST_CHARACTER_BLACK
--	Compile Turn	: 0
--  Creation Date	: 11.04.04
--	Update Date		:
--  Author			: i-Brid(R.Suzuki)
/*************************************************************************/
-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PACKAGE PKG_CAST_CHARACTER_BLACK AS

	CURSOR CS01 IS SELECT ROWID,T_CAST_CHARACTER_BLACK.* FROM T_CAST_CHARACTER_BLACK ORDER BY SITE_CD FOR UPDATE WAIT 5;

	CURSOR CS02(prmSITE_CD				T_CAST_CHARACTER_BLACK.SITE_CD%TYPE		,
				prmUSER_SEQ				T_CAST_CHARACTER_BLACK.USER_SEQ%TYPE	,
				prmUSER_CHAR_NO			T_CAST_CHARACTER_BLACK.USER_CHAR_NO%TYPE,
				prmBLACK_TYPE			T_CAST_CHARACTER_BLACK.BLACK_TYPE%TYPE
	) IS SELECT ROWID,T_CAST_CHARACTER_BLACK.* FROM T_CAST_CHARACTER_BLACK
		WHERE
			SITE_CD 			= prmSITE_CD	 		AND
			USER_SEQ			= prmUSER_SEQ			AND
			USER_CHAR_NO		= prmUSER_CHAR_NO		AND
			BLACK_TYPE			= prmBLACK_TYPE
		FOR UPDATE WAIT 5;

	CURSOR CS03(prmROWID ROWID) IS SELECT REVISION_NO FROM T_CAST_CHARACTER_BLACK
		WHERE
			ROWID = prmROWID
		FOR UPDATE WAIT 5;


	PROCEDURE LOCK_EX;

	PROCEDURE CLOSE_ALL;

END PKG_CAST_CHARACTER_BLACK;
/
SHOW ERROR;

CREATE OR REPLACE PACKAGE BODY PKG_CAST_CHARACTER_BLACK AS

	/*------------------------------*/
	/* Lock Table					*/
	/*------------------------------*/
	PROCEDURE LOCK_EX
	IS
		--	CONSTANT
		PGM_NM 	CONSTANT VARCHAR(255)	:= 'PKG_CAST_CHARACTER_BLACK.LOCK_EX';

		BUF_TRY	NUMBER(1);
	BEGIN

		BUF_TRY	:= 0;

		LOOP
			BEGIN
				LOCK TABLE T_CAST_CHARACTER_BLACK IN EXCLUSIVE MODE NOWAIT;
				BUF_TRY := 0;
			EXCEPTION
				WHEN APP_COMM.ALREADY_LOCKED THEN
					BUF_TRY := BUF_TRY + 1;
					DBMS_LOCK.SLEEP(1);
			END;
			EXIT WHEN (BUF_TRY = 0) OR (BUF_TRY > APP_COMM.MAX_TRY);
		END LOOP;

		IF (BUF_TRY != 0) THEN
			LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,PGM_NM);
			RAISE APP_COMM.ALREADY_LOCKED;
		END IF;

	END LOCK_EX;

	/*------------------------------*/
	/* Close All					*/
	/*------------------------------*/
	PROCEDURE CLOSE_ALL
	IS
	BEGIN
		IF (CS01%ISOPEN) THEN
			CLOSE CS01;
		END IF;

		IF (CS02%ISOPEN) THEN
			CLOSE CS02;
		END IF;

		IF (CS03%ISOPEN) THEN
			CLOSE CS03;
		END IF;

	END CLOSE_ALL;

END PKG_CAST_CHARACTER_BLACK;
/
SHOW ERROR;
