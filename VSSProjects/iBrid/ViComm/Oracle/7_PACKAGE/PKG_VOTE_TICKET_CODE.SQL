/*************************************************************************/
--	System			: ViComm
--	Title			: T_VOTE_TICKET_CODE Package
--	Progaram ID		: PKG_VOTE_TICKET_CODE
--	Compile Turn	: 0
--	Creation Date	: 13.10.19
--	Update Date		:
--	Author			: K.Miyazato
/*************************************************************************/
CREATE OR REPLACE PACKAGE PKG_VOTE_TICKET_CODE AS
	CURSOR CS01(
		prmSITE_CD			T_VOTE_TICKET_CODE.SITE_CD%TYPE,
		prmVOTE_TERM_SEQ	T_VOTE_TICKET_CODE.VOTE_TERM_SEQ%TYPE,
		prmCODE_STR			T_VOTE_TICKET_CODE.CODE_STR%TYPE
	)IS
		SELECT
			ROWID,
			T_VOTE_TICKET_CODE.*
		FROM
			T_VOTE_TICKET_CODE
		WHERE
			SITE_CD			= prmSITE_CD		AND
			VOTE_TERM_SEQ	= prmVOTE_TERM_SEQ	AND
			CODE_STR		= prmCODE_STR
		FOR UPDATE WAIT 5;

	PROCEDURE LOCK_EX;
	PROCEDURE CLOSE_ALL;

END PKG_VOTE_TICKET_CODE;
/
SHOW ERROR;


CREATE OR REPLACE PACKAGE BODY PKG_VOTE_TICKET_CODE AS
	/*------------------------------*/
	/* Lock Table					*/
	/*------------------------------*/
	PROCEDURE LOCK_EX
	IS
		--	CONSTANT
		PGM_NM	CONSTANT VARCHAR(255)	:= 'PKG_VOTE_TICKET_CODE.LOCK_EX';
		BUF_TRY	NUMBER(1);
	BEGIN
		BUF_TRY	:= 0;

		LOOP
			BEGIN
				LOCK TABLE T_VOTE_TICKET_CODE IN EXCLUSIVE MODE NOWAIT;
				BUF_TRY := 0;
			EXCEPTION
				WHEN APP_COMM.ALREADY_LOCKED THEN
					BUF_TRY := BUF_TRY + 1;
					DBMS_LOCK.SLEEP(1);
			END;
			EXIT WHEN (BUF_TRY = 0) OR (BUF_TRY > APP_COMM.MAX_TRY);
		END LOOP;

		IF (BUF_TRY != 0) THEN
			LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,PGM_NM);
			RAISE APP_COMM.ALREADY_LOCKED;
		END IF;
	END LOCK_EX;

	/*------------------------------*/
	/* Close All					*/
	/*------------------------------*/
	PROCEDURE CLOSE_ALL
	IS
	BEGIN
		IF (CS01%ISOPEN) THEN
			CLOSE CS01;
		END IF;

	END CLOSE_ALL;

END PKG_VOTE_TICKET_CODE;
/
SHOW ERROR;