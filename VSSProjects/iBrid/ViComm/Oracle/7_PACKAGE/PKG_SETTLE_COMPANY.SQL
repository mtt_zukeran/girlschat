/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: ﾃｰﾌﾞﾙ管理
--  Sub System Name	: 共通
--  Title			: 決済会社 T_SETTLE_COMPANY
--	Progaram ID		: PKG_SETTLE_COMPANY
--	Compile Turn	: 0
--  Creation Date	: 09.11.05
--	Update Date		:
--  Author			: i-Brid(S.Ohtahara)
/*************************************************************************/
-- [ Update History ]
/*-----------------------------------------------------------------------*/
--
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
--
/*-----------------------------------------------------------------------*/
CREATE OR REPLACE PACKAGE PKG_SETTLE_COMPANY AS

	CURSOR CS01 IS SELECT ROWID,T_SETTLE_COMPANY.* FROM T_SETTLE_COMPANY ORDER BY SETTLE_COMPANY_CD FOR UPDATE NOWAIT;

	CURSOR CS02(prmSETTLE_COMPANY_CD T_SETTLE_COMPANY.SETTLE_COMPANY_CD%TYPE) IS SELECT ROWID,T_SETTLE_COMPANY.* FROM T_SETTLE_COMPANY
		WHERE
			SETTLE_COMPANY_CD	= prmSETTLE_COMPANY_CD
		FOR UPDATE NOWAIT;

	CURSOR CS03(prmROWID ROWID) IS SELECT REVISION_NO FROM T_SETTLE_COMPANY
		WHERE
			ROWID = prmROWID
		FOR UPDATE NOWAIT;


	PROCEDURE LOCK_EX;

	PROCEDURE CLOSE_ALL;

END PKG_SETTLE_COMPANY;
/
SHOW ERROR;


CREATE OR REPLACE PACKAGE BODY PKG_SETTLE_COMPANY AS

	/*------------------------------*/
	/* Lock Table					*/
	/*------------------------------*/
	PROCEDURE LOCK_EX
	IS
		--	CONSTANT
		PGM_NM 	CONSTANT VARCHAR(255)	:= 'PKG_SETTLE_COMPANY.LOCK_EX';

		BUF_TRY	NUMBER(1);
	BEGIN

		BUF_TRY	:= 0;

		LOOP
			BEGIN
				LOCK TABLE T_SETTLE_COMPANY IN EXCLUSIVE MODE NOWAIT;
				BUF_TRY := 0;
			EXCEPTION
				WHEN APP_COMM.ALREADY_LOCKED THEN
					BUF_TRY := BUF_TRY + 1;
					DBMS_LOCK.SLEEP(1);
			END;
			EXIT WHEN (BUF_TRY = 0) OR (BUF_TRY > APP_COMM.MAX_TRY);
		END LOOP;
	
		IF (BUF_TRY != 0) THEN
			LOGGING_PGM_TROUBLE(SYSDATE,APP_CONST.SYS_DB,PGM_NM,PGM_NM);
			RAISE APP_COMM.ALREADY_LOCKED;
		END IF;

	END LOCK_EX;

	/*------------------------------*/
	/* Close All					*/
	/*------------------------------*/
	PROCEDURE CLOSE_ALL
	IS
	BEGIN
		IF (CS01%ISOPEN) THEN
			CLOSE CS01;
		END IF;

		IF (CS02%ISOPEN) THEN
			CLOSE CS02;
		END IF;

		IF (CS03%ISOPEN) THEN
			CLOSE CS03;
		END IF;

	END CLOSE_ALL;

END PKG_SETTLE_COMPANY;
/
SHOW ERROR;
