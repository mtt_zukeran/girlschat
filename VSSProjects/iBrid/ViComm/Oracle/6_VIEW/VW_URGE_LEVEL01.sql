/*************************************************************************/
--	System			: ViComm
--  Title			: �����x�� View01
--	Progaram ID		: VW_URGE_LEVEL01
--  Creation Date	: 09.12.14
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
CREATE OR REPLACE VIEW VW_URGE_LEVEL01(
	SITE_CD		,
	URGE_LEVEL	,
	DELAY_DAYS	,
	REVISION_NO	,
	UPDATE_DATE	,
	SITE_NM		
)AS SELECT
	T_URGE_LEVEL.SITE_CD		,
	T_URGE_LEVEL.URGE_LEVEL		,
	T_URGE_LEVEL.DELAY_DAYS		,
	T_URGE_LEVEL.REVISION_NO	,
	T_URGE_LEVEL.UPDATE_DATE	,
	T_SITE.SITE_NM				
FROM
	T_URGE_LEVEL	,
	T_SITE			
WHERE
	T_URGE_LEVEL.SITE_CD	= T_SITE.SITE_CD
;
