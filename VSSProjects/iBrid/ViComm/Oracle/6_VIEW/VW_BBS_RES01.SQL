/*************************************************************************/
--	System			: ViComm
--	Title			: スレッド式掲示板レス View01
--	Progaram ID		: VW_BBS_RES01
--	Creation Date	: 11.05.30
--	Update Date		: 
--	Author			: i-Brid(R.Suzuki)
/*************************************************************************/
CREATE OR REPLACE VIEW VW_BBS_RES01(
	SITE_CD					,
	USER_SEQ				,
	USER_CHAR_NO			,
	BBS_THREAD_SEQ			,
	BBS_THREAD_HANDLE_NM	,
	BBS_THREAD_TITLE		,
	ADMIN_THREAD_FLAG		,
	LAST_RES_WRITE_DATE		,
	LAST_THREAD_SUB_SEQ		,
	THREAD_DEL_FLAG			,--THREAD
	THREAD_CREATE_DATE		,--THREAD
	THREAD_UPDATE_DATE		,--THREAD
	REVISION_NO				,
	BBS_THREAD_DOC1			,
	BBS_THREAD_DOC2			,
	BBS_THREAD_DOC3			,
	BBS_THREAD_DOC4			,
	TODAY_ACCESS_COUNT		,
	THREAD_TYPE				,
	BBS_THREAD_SUB_SEQ		,
	BBS_RES_HANDLE_NM		,
	BBS_RES_USER_SEQ		,
	BBS_RES_USER_CHAR_NO	,
	POST_BBS_THREAD_SEQ		,
	POST_THREAD_SUB_SEQ		,
	RES_DEL_FLAG			,--RES
	RES_CREATE_DATE			,--RES
	RES_UPDATE_DATE			,--RES
	BBS_RES_DOC1			,
	BBS_RES_DOC2			,
	BBS_RES_DOC3			,
	BBS_RES_DOC4			,
	ANONYMOUS_FLAG
)AS  SELECT 
	T_BBS_THREAD.SITE_CD					,
	T_BBS_THREAD.USER_SEQ					,
	T_BBS_THREAD.USER_CHAR_NO				,
	T_BBS_THREAD.BBS_THREAD_SEQ				,
	T_BBS_THREAD.BBS_THREAD_HANDLE_NM		,
	T_BBS_THREAD.BBS_THREAD_TITLE			,
	T_BBS_THREAD.ADMIN_THREAD_FLAG			,
	T_BBS_THREAD.LAST_RES_WRITE_DATE		,
	T_BBS_THREAD.LAST_THREAD_SUB_SEQ		,
	T_BBS_THREAD.DEL_FLAG					,
	T_BBS_THREAD.CREATE_DATE				,
	T_BBS_THREAD.UPDATE_DATE				,
	T_BBS_THREAD.REVISION_NO				,
	T_BBS_THREAD.BBS_THREAD_DOC1			,
	T_BBS_THREAD.BBS_THREAD_DOC2			,
	T_BBS_THREAD.BBS_THREAD_DOC3			,
	T_BBS_THREAD.BBS_THREAD_DOC4			,
	T_BBS_THREAD.TODAY_ACCESS_COUNT			,
	T_BBS_THREAD.THREAD_TYPE				,
	T_BBS_RES.BBS_THREAD_SUB_SEQ			,
	T_BBS_RES.BBS_RES_HANDLE_NM				,
	T_BBS_RES.BBS_RES_USER_SEQ				,
	T_BBS_RES.BBS_RES_USER_CHAR_NO			,
	T_BBS_RES.POST_BBS_THREAD_SEQ			,
	T_BBS_RES.POST_THREAD_SUB_SEQ			,
	T_BBS_RES.DEL_FLAG						,
	T_BBS_RES.CREATE_DATE					,
	T_BBS_RES.UPDATE_DATE					,
	T_BBS_RES.BBS_RES_DOC1					,
	T_BBS_RES.BBS_RES_DOC2					,
	T_BBS_RES.BBS_RES_DOC3					,
	T_BBS_RES.BBS_RES_DOC4					,
	T_BBS_RES.ANONYMOUS_FLAG
FROM
	T_BBS_THREAD		,
	T_BBS_RES
WHERE
	T_BBS_THREAD.SITE_CD			= T_BBS_RES.SITE_CD					AND
	T_BBS_THREAD.USER_SEQ			= T_BBS_RES.USER_SEQ				AND
	T_BBS_THREAD.USER_CHAR_NO		= T_BBS_RES.USER_CHAR_NO			AND
	T_BBS_THREAD.BBS_THREAD_SEQ		= T_BBS_RES.BBS_THREAD_SEQ
;
