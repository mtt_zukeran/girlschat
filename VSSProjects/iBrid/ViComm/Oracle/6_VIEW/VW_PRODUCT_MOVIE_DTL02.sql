/*************************************************************************/
--	System			: ViComm
--  Title			: ���i����ڍ� View2
--	Progaram ID		: VW_PRODUCT_MOVIE_DTL02
--  Creation Date	: 10.12.17
--	Update Date		:
--  Author			: K.Itoh
/*************************************************************************/
CREATE OR REPLACE VIEW VW_PRODUCT_MOVIE_DTL02(
	SITE_CD					,	
	PRODUCT_AGENT_CD    	,  
	PRODUCT_SEQ         	,  
	OBJ_SEQ             	,  
	CP_SIZE_TYPE        	,  
	CP_FILE_FORMAT      	,  
	CP_TARGET_CARRIER   	,  
	PRODUCT_MOVIE_SUBSEQ	,  
	FILE_NM             	,  
	FILE_SIZE           	,  
	DOWNLOAD_URL        	  
)AS SELECT 
	T_PRODUCT_MOVIE_DTL.SITE_CD					,
	T_PRODUCT_MOVIE_DTL.PRODUCT_AGENT_CD        ,
	T_PRODUCT_MOVIE_DTL.PRODUCT_SEQ             ,
	T_PRODUCT_MOVIE_DTL.OBJ_SEQ                 ,
	T_PRODUCT_MOVIE_DTL.CP_SIZE_TYPE            ,
	T_PRODUCT_MOVIE_DTL.CP_FILE_FORMAT          ,
	T_PRODUCT_MOVIE_DTL.CP_TARGET_CARRIER       ,
	T_PRODUCT_MOVIE_DTL.PRODUCT_MOVIE_SUBSEQ    ,
	T_PRODUCT_MOVIE_DTL.FILE_NM                 ,
	T_PRODUCT_MOVIE_DTL.FILE_SIZE               ,
	T_PRODUCT_MOVIE_DTL.DOWNLOAD_URL            
FROM 
	T_PRODUCT_MOVIE_DTL
WHERE
-- ���O����
	NOT (
		T_PRODUCT_MOVIE_DTL.PRODUCT_AGENT_CD 	= '0000'			AND
		T_PRODUCT_MOVIE_DTL.CP_TARGET_CARRIER	= 'a'			AND
		T_PRODUCT_MOVIE_DTL.CP_SIZE_TYPE		IN ('qcifd','qcifs','qvgas')
	) AND
	NOT (
		T_PRODUCT_MOVIE_DTL.PRODUCT_AGENT_CD 	= '0000'			AND
		T_PRODUCT_MOVIE_DTL.CP_TARGET_CARRIER	= 's'			AND
		T_PRODUCT_MOVIE_DTL.CP_SIZE_TYPE		IN ('qcifs','qcifs','qvgas')
	) AND
	NOT (
		T_PRODUCT_MOVIE_DTL.PRODUCT_AGENT_CD 	= '0000'		AND
		T_PRODUCT_MOVIE_DTL.CP_TARGET_CARRIER	= 'd'			AND
		T_PRODUCT_MOVIE_DTL.CP_SIZE_TYPE		IN ('qcifd','qvgad','qcifs','qvgas','qcifs2')
	)	
;

