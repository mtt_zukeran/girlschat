Oracle\E_DATABASE\REDO・UNDO設定を実行する

-- MATE VIEW権限のグラント
CONN SYS/IBDB@VICOMM_HARPYAN AS SYSDBA
GRANT CREATE MATERIALIZED VIEW TO "VICOMM_HARPYAN";


-- MateView Log作成
CONN VICOMM_HARPYAN/VICOMM_HARPYAN@VICOMM_HARPYAN
@K:\VSSProjects\iBrid\ViComm\Oracle\6_VIEW\MatView\MV_LOG

-- Refresh Group作成
@K:\VSSProjects\iBrid\ViComm\Oracle\6_VIEW\MatView\REFRESH_GROUP

-- MateView作成
@K:\VSSProjects\iBrid\ViComm\Oracle\6_VIEW\MatView\MV_CAST_BBS00
@K:\VSSProjects\iBrid\ViComm\Oracle\6_VIEW\MatView\MV_BBS_OBJS00
@K:\VSSProjects\iBrid\ViComm\Oracle\6_VIEW\MatView\MV_CAST_DIARY00
@K:\VSSProjects\iBrid\ViComm\Oracle\6_VIEW\MatView\MV_CAST_MOVIE00
@K:\VSSProjects\iBrid\ViComm\Oracle\6_VIEW\MatView\MV_CAST_PIC00
@K:\VSSProjects\iBrid\ViComm\Oracle\6_VIEW\MatView\MV_BLOG_ARTICLE00
@K:\VSSProjects\iBrid\ViComm\Oracle\6_VIEW\MatView\MV_BLOG_OBJS00

--@K:\VSSProjects\iBrid\ViComm\Oracle\6_VIEW\MatView\MV_CAST_CHARACTER00
--@K:\VSSProjects\iBrid\ViComm\Oracle\6_VIEW\MatView\MV_USER_MAN_CHARACTER00

--MateView統計情報削除及びﾛｯｸ
@K:\VSSProjects\iBrid\ViComm\Oracle\6_VIEW\MatView\DELETE_STATISTICS

-- MateView Refresh
@K:\VSSProjects\iBrid\ViComm\Oracle\6_VIEW\MatView\REFRESH

