/*************************************************************************/
--	System			: ViComm
--  Title			: 男性会員キャラクター View04
--	Progaram ID		: VW_USER_MAN_CHARACTER04
--  Creation Date	: 08.10.21
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
CREATE OR REPLACE VIEW VW_USER_MAN_CHARACTER04(
	SITE_CD			,
	USER_SEQ		,
	USER_CHAR_NO	,
	HANDLE_NM		,
	BIRTHDAY		,
	PRIORITY
)AS SELECT
	T_USER_MAN_CHARACTER.SITE_CD		,
	T_USER_MAN_CHARACTER.USER_SEQ		,
	T_USER_MAN_CHARACTER.USER_CHAR_NO	,
	T_USER_MAN_CHARACTER.HANDLE_NM		,
	T_USER_MAN_CHARACTER.BIRTHDAY		,
	T_SITE.PRIORITY
FROM
	T_USER_MAN_CHARACTER	,
	T_SITE
WHERE
	T_USER_MAN_CHARACTER.SITE_CD	= T_SITE.SITE_CD
;
