/*************************************************************************/
--	System			: ViComm
--  Title			: oÒÊ^®«íÊlView01
--	Progaram ID		: VW_CAST_PIC_ATTR_TYPE_VALUE01
--  Creation Date	: 08.10.21
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
CREATE OR REPLACE VIEW VW_CAST_PIC_ATTR_TYPE_VALUE01(
	SITE_CD						,
	CAST_PIC_ATTR_TYPE_SEQ		,
	CAST_PIC_ATTR_SEQ			,
	CAST_PIC_ATTR_NM			,
	PRIORITY					,
	POSTER_DEL_NA_FLAG			,
	USE_INDIVIDUAL_PAY_FLAG		,
	POST_PAY_AMT				,
	VIEW_PAY_AMT				,
	USE_INDIVIDUAL_CHARGE_FLAG	,
	VIEWER_CHARGE_POINT			,
	UPLOAD_LIMIT_COUNT			,
	UPDATE_DATE					,
	REVISION_NO					,
	CAST_PIC_ATTR_TYPE_PRIORITY	,
	CAST_PIC_ATTR_TYPE_NM		,
	ITEM_NO						,
	PLANNING_TYPE
)AS SELECT
	T_CAST_PIC_ATTR_TYPE_VALUE.SITE_CD						,
	T_CAST_PIC_ATTR_TYPE_VALUE.CAST_PIC_ATTR_TYPE_SEQ		,
	T_CAST_PIC_ATTR_TYPE_VALUE.CAST_PIC_ATTR_SEQ			,
	T_CAST_PIC_ATTR_TYPE_VALUE.CAST_PIC_ATTR_NM				,
	T_CAST_PIC_ATTR_TYPE_VALUE.PRIORITY						,
	T_CAST_PIC_ATTR_TYPE_VALUE.POSTER_DEL_NA_FLAG			,
	T_CAST_PIC_ATTR_TYPE_VALUE.USE_INDIVIDUAL_PAY_FLAG		,
	T_CAST_PIC_ATTR_TYPE_VALUE.POST_PAY_AMT					,
	T_CAST_PIC_ATTR_TYPE_VALUE.VIEW_PAY_AMT					,
	T_CAST_PIC_ATTR_TYPE_VALUE.USE_INDIVIDUAL_CHARGE_FLAG	,
	T_CAST_PIC_ATTR_TYPE_VALUE.VIEWER_CHARGE_POINT			,
	T_CAST_PIC_ATTR_TYPE_VALUE.UPLOAD_LIMIT_COUNT			,
	T_CAST_PIC_ATTR_TYPE_VALUE.UPDATE_DATE					,
	T_CAST_PIC_ATTR_TYPE_VALUE.REVISION_NO					,
	T_CAST_PIC_ATTR_TYPE.PRIORITY							,
	T_CAST_PIC_ATTR_TYPE.CAST_PIC_ATTR_TYPE_NM				,
	T_CAST_PIC_ATTR_TYPE.ITEM_NO							,
	T_CAST_PIC_ATTR_TYPE.PLANNING_TYPE
FROM
	T_CAST_PIC_ATTR_TYPE_VALUE	,
	T_CAST_PIC_ATTR_TYPE
WHERE
	T_CAST_PIC_ATTR_TYPE_VALUE.SITE_CD					= T_CAST_PIC_ATTR_TYPE.SITE_CD					AND
	T_CAST_PIC_ATTR_TYPE_VALUE.CAST_PIC_ATTR_TYPE_SEQ	= T_CAST_PIC_ATTR_TYPE.CAST_PIC_ATTR_TYPE_SEQ
;
