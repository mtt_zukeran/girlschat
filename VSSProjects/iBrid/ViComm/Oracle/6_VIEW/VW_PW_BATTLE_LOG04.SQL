/*************************************************************************/
--	System			: ViComm
--  Title			: ����ٹް� ���ًL�^
--	Progaram ID		: VW_PW_BATTLE_LOG04
--  Creation Date	: 11.11.10
--	Update Date		: 
--  Author			: PW Y.Ikemiya
/*************************************************************************/
CREATE OR REPLACE VIEW VW_PW_BATTLE_LOG04(
	SITE_CD							,
	USER_SEQ						,
	USER_CHAR_NO					,
	PARTNER_USER_SEQ				,
	PARTNER_USER_CHAR_NO			,
	BATTLE_LOG_SEQ					,
	BATTLE_TYPE						,
	ATTACK_WIN_FLAG					,
	DEFENCE_WIN_FLAG				,
	PARTNER_GAME_CHARACTER_TYPE
) AS SELECT
	TBL.SITE_CD						,
	TBL.USER_SEQ					,
	TBL.USER_CHAR_NO				,
	TBL.PARTNER_USER_SEQ			,
	TBL.PARTNER_USER_CHAR_NO		,
	TBL.BATTLE_LOG_SEQ				,
	TBL.BATTLE_TYPE					,
	TBL.ATTACK_WIN_FLAG				,
	TBL.DEFENCE_WIN_FLAG			,
	PARTNER.GAME_CHARACTER_TYPE		AS PARTNER_GAME_CHARACTER_TYPE	
FROM
	T_BATTLE_LOG			TBL		,
	T_GAME_CHARACTER		PARTNER		
WHERE
	TBL.SITE_CD 				= PARTNER.SITE_CD				AND
	TBL.PARTNER_USER_SEQ		= PARTNER.USER_SEQ				AND
	TBL.PARTNER_USER_CHAR_NO	= PARTNER.USER_CHAR_NO			
;