/*************************************************************************/
--	System			: ViComm
--  Title			: ソーシャルゲーム ステージ View02
--	Progaram ID		: VW_PW_GAME_STAGE02
--  Creation Date	: 11.08.03
--	Update Date		: 
--  Author			: PW K.Miyazato
/*************************************************************************/
CREATE OR REPLACE VIEW VW_PW_GAME_STAGE02(
	SITE_CD				,
	STAGE_SEQ			,
	STAGE_NM			,
	STAGE_LEVEL			,
	SEX_CD				,
	HONOR				,
	STAGE_GROUP_TYPE	,
	BOSS_NM				,
	TOWN_COUNT			,
	STAGE_GROUP_NM
)AS SELECT
	T_STAGE.SITE_CD			,
	T_STAGE.STAGE_SEQ		,
	T_STAGE.STAGE_NM		,
	T_STAGE.STAGE_LEVEL		,
	T_STAGE.SEX_CD			,
	T_STAGE.HONOR			,
	T_STAGE.STAGE_GROUP_TYPE,
	T_STAGE.BOSS_NM			,
	NVL(COUNT(T_TOWN.TOWN_SEQ),0) AS TOWN_COUNT,
	T_STAGE_GROUP.STAGE_GROUP_NM
FROM
	T_STAGE				,
	T_STAGE_GROUP		,
	T_TOWN
WHERE
	T_STAGE.SITE_CD				= T_STAGE_GROUP.SITE_CD						AND
	T_STAGE.STAGE_GROUP_TYPE	= T_STAGE_GROUP.STAGE_GROUP_TYPE			AND
	T_STAGE.SEX_CD				= T_STAGE_GROUP.SEX_CD						AND
	T_STAGE.SITE_CD				= T_TOWN.SITE_CD 						(+)	AND
	T_STAGE.STAGE_SEQ 			= T_TOWN.STAGE_SEQ						(+)	AND
	T_STAGE.SEX_CD 				= T_TOWN.SEX_CD							(+)
GROUP BY
	T_STAGE.SITE_CD					,
	T_STAGE.STAGE_SEQ				,
	T_STAGE.STAGE_NM				,
	T_STAGE.STAGE_LEVEL				,
	T_STAGE.SEX_CD					,
	T_STAGE.HONOR					,
	T_STAGE.STAGE_GROUP_TYPE		,
	T_STAGE.BOSS_NM					,
	T_STAGE_GROUP.STAGE_GROUP_NM
;
