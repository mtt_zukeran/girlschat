/*************************************************************************/
--	System			: ViComm
--  Title			: ページ一覧 View01
--	Progaram ID		: VW_PAGE01
--  Creation Date	: 14.02.07
--	Update Date		:
--  Author			: M&TT Y.Ikemiya
/*************************************************************************/
CREATE OR REPLACE VIEW VW_PAGE01(
	SITE_CD								,
	PROGRAM_ROOT						,
	PROGRAM_ID							,
	HTML_DOC_TYPE						,
	PROGRAM_NM
)AS SELECT
	MN.SITE_CD							,
	'-' AS PROGRAM_ROOT				,
	'DisplayDoc.aspx' AS PROGRAM_ID		,
	MN.HTML_DOC_TYPE					,
	DC.HTML_DOC_TITLE AS PROGRAM_NM
FROM
	T_SITE_HTML_MANAGE MN				,
	T_SITE_HTML_DOC DC
WHERE
	MN.HTML_DOC_SEQ			= DC.HTML_DOC_SEQ					AND
	MN.HTML_DOC_GROUP		= '11'								AND
	MN.ROWID IN (SELECT MIN(ROWID) FROM T_SITE_HTML_MANAGE GROUP BY SITE_CD,HTML_DOC_TYPE)
UNION
SELECT
	DISTINCT UV.SITE_CD				,
	UV.PROGRAM_ROOT						,
	UV.PROGRAM_ID						,
	'-' AS HTML_DOC_TYPE				,
	PG.PROGRAM_NM
FROM
	T_USER_VIEW UV						,
	T_PROGRAM PG
WHERE
	UV.PROGRAM_ROOT		= PG.PROGRAM_ROOT	AND
	UV.PROGRAM_ID		= PG.PROGRAM_ID
;
