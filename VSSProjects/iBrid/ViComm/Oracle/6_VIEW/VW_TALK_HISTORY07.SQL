/*************************************************************************/
--	System			: ViComm
--  Title			: 会話履歴 View（女性メイン・最終会話のみ・発着信）
--	Progaram ID		: VW_TALK_HISTORY07
--  Creation Date	: 10.08.27
--	Update Date		:
--  Author			: i-Brid(R.Suzuki)
/*************************************************************************/
CREATE OR REPLACE VIEW VW_TALK_HISTORY07(
	TALK_SEQ				,
	SITE_CD					,
	USER_SEQ				,
	USER_CHAR_NO			,
	PARTNER_USER_SEQ		,
	PARTNER_USER_CHAR_NO	,
	TALK_START_DATE			,
	TALK_COUNT				,
	UNIQUE_VALUE
)AS SELECT
	MAX(T_TALK_HISTORY.TALK_SEQ)		,
	T_TALK_HISTORY.SITE_CD				,
	T_TALK_HISTORY.PARTNER_USER_SEQ		,
	T_TALK_HISTORY.PARTNER_USER_CHAR_NO	,
	T_TALK_HISTORY.USER_SEQ				,
	T_TALK_HISTORY.USER_CHAR_NO			,
	MAX(T_TALK_HISTORY.TALK_START_DATE) ,
	COUNT(T_TALK_HISTORY.TALK_SEQ)		,
	MAX(T_TALK_HISTORY.TALK_SEQ)
FROM
	T_TALK_HISTORY
GROUP BY
	SITE_CD,USER_SEQ,USER_CHAR_NO,PARTNER_USER_SEQ,PARTNER_USER_CHAR_NO
;

