/*************************************************************************/
--	System			: ViComm
--  Title			: ����ٹްсE��V�l��
--	Progaram ID		: VW_PW_GAME_PAYMENT_LOG01
--  Creation Date	: 12.10.09
--	Update Date		: 
--  Author			: PW Y.Ikemiya
/*************************************************************************/
CREATE OR REPLACE VIEW VW_PW_GAME_PAYMENT_LOG01(
	SITE_CD							,
	USER_SEQ						,
	USER_CHAR_NO					,
	PARTNER_USER_SEQ				,
	PARTNER_USER_CHAR_NO			,
	GAME_PAYMENT_LOG_SEQ			,
	GAME_PAYMENT_TYPE				,
	MOVIE_SEQ						,
	CAST_GAME_PIC_SEQ				,
	PRESENT_HISTORY_SEQ				,
	DATE_LOG_SEQ					,
	CREATE_DATE						,
--
	MOVIE_TITLE						,
	MOVIE_DOC						,
--
	PIC_TITLE						,
	PIC_DOC
) AS SELECT
	GPL.SITE_CD						,
	GPL.USER_SEQ					,
	GPL.USER_CHAR_NO				,
	GPL.PARTNER_USER_SEQ			,
	GPL.PARTNER_USER_CHAR_NO		,
	GPL.GAME_PAYMENT_LOG_SEQ		,
	GPL.GAME_PAYMENT_TYPE			,
	GPL.MOVIE_SEQ					,
	GPL.CAST_GAME_PIC_SEQ			,
	GPL.PRESENT_HISTORY_SEQ			,
	GPL.DATE_LOG_SEQ				,
	GPL.CREATE_DATE					,
--
	CM.MOVIE_TITLE					,
	CM.MOVIE_DOC					,
--
	MT.PIC_TITLE					,
	MT.PIC_DOC
FROM
	T_GAME_PAYMENT_LOG GPL	,
	T_CAST_MOVIE CM			,
	T_MAN_TREASURE MT
WHERE
	GPL.SITE_CD				= CM.SITE_CD				(+)	AND
	GPL.MOVIE_SEQ			= CM.MOVIE_SEQ				(+)	AND
--
	GPL.SITE_CD				= MT.SITE_CD				(+)	AND
	GPL.CAST_GAME_PIC_SEQ	= MT.CAST_GAME_PIC_SEQ		(+)
;