/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: システム管理
--  Title			: 美人時計イメージパス取得
--	Progaram ID		: GET_BEAUTY_CLOCK_IMG_PATH_S
--	Compile Turn	: 0
--  Creation Date	: 11.03.29
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
CREATE OR REPLACE FUNCTION GET_BEAUTY_CLOCK_IMG_PATH_S(
	pSITE_CD			IN	VARCHAR2,
	pASSIGNED_TIME		IN	VARCHAR2,
	pPIC_SEQ			IN	VARCHAR2
)
	RETURN	VARCHAR2
IS

	BUF_PATH VARCHAR2(64);
BEGIN

	IF pSITE_CD IS NULL OR pASSIGNED_TIME IS NULL OR pPIC_SEQ IS NULL THEN
		RETURN '';
	END IF;
	
	RETURN 'extensionres/beautyclock/' || pSITE_CD || '/' || pASSIGNED_TIME || '/' || TO_CHAR(pPIC_SEQ,'FM000000000000000') || '_s.jpg';
EXCEPTION
	WHEN OTHERS THEN

		APP_COMM.TRACE(APP_COMM.TRACE_INFO,'GET_BEAUTY_CLOCK_IMG_PATH_S ' || SQLERRM);

		RETURN	NULL;

END GET_BEAUTY_CLOCK_IMG_PATH_S;
/
SHOW ERROR;
