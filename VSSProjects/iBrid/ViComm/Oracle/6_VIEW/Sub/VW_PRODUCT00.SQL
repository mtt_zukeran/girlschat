/*************************************************************************/
--	System			: ViComm
--	Title			: ���i View00
--	Progaram ID		: VW_PRODUCT00
--	Creation Date	: 10.12.17
--	Update Date		: 
--	Author			: i-Brid
/*************************************************************************/
CREATE OR REPLACE VIEW VW_PRODUCT00(
-- T_PRODUCT
	SITE_CD						,
	PRODUCT_AGENT_CD            ,
	PRODUCT_SEQ                 ,
	PRODUCT_ID                  ,
	PRODUCT_TYPE                ,
	PRODUCT_NM                  ,
	PRODUCT_DISCRIPTION         ,
	PART_NUMBER                 ,
	PUBLISH_START_DATE          ,
	PUBLISH_END_DATE            ,
	PRODUCT_SERIES_SUMMARY      ,
	CAST_SUMMARY                ,
	PRODUCT_MAKER_SUMMARY       ,
	RELEASE_DATE                ,
	GROSS_PRICE                 ,
	APPLY_CHARGE_POINT          ,
	CHARGE_POINT                ,
	SPECIAL_CHARGE_POINT        ,
	PUBLISH_FLAG                ,
	DEL_FLAG                    ,
	REMARKS                     ,
	LINK_ID                     ,
	LINK_DATE                   ,
	UNEDITABLE_FLAG             ,
	REVISION_NO                 ,
	UPDATE_DATE                 ,
	PICKUP_MASK              	,
	PUBLISH_START_DAY			,
	BUY_COUNT            		,
	FAVORIT_ME_COUNT	    	,
	NOT_NEW_FLAG				
)AS SELECT
--	T_PRODUCT
	T_PRODUCT.SITE_CD					,
	T_PRODUCT.PRODUCT_AGENT_CD          ,
	T_PRODUCT.PRODUCT_SEQ               ,
	T_PRODUCT.PRODUCT_ID                ,
	T_PRODUCT.PRODUCT_TYPE              ,
	T_PRODUCT.PRODUCT_NM                ,
	T_PRODUCT.PRODUCT_DISCRIPTION       ,
	T_PRODUCT.PART_NUMBER               ,
	T_PRODUCT.PUBLISH_START_DATE        ,
	T_PRODUCT.PUBLISH_END_DATE          ,
	T_PRODUCT.PRODUCT_SERIES_SUMMARY    ,
	T_PRODUCT.CAST_SUMMARY              ,
	T_PRODUCT.PRODUCT_MAKER_SUMMARY     ,
	T_PRODUCT.RELEASE_DATE              ,
	T_PRODUCT.GROSS_PRICE               ,
 	DECODE(                         
 		T_PRODUCT.SPECIAL_CHARGE_POINT	,   
 		0								,   
		T_PRODUCT.CHARGE_POINT			,   
		T_PRODUCT.SPECIAL_CHARGE_POINT        
 	) AS APPLY_CHARGE_POINT 			,	
	T_PRODUCT.CHARGE_POINT              ,
	T_PRODUCT.SPECIAL_CHARGE_POINT      ,
	T_PRODUCT.PUBLISH_FLAG              ,
	T_PRODUCT.DEL_FLAG                  ,
	T_PRODUCT.REMARKS                   ,
	T_PRODUCT.LINK_ID                   ,
	T_PRODUCT.LINK_DATE                 ,
	T_PRODUCT.UNEDITABLE_FLAG           ,
	T_PRODUCT.REVISION_NO               ,
	T_PRODUCT.UPDATE_DATE               ,
	T_PRODUCT.PICKUP_MASK               ,
	T_PRODUCT_SORT.PUBLISH_START_DAY	,
	T_PRODUCT_SORT.BUY_COUNT            ,
	T_PRODUCT_SORT.FAVORIT_ME_COUNT	    ,
	T_PRODUCT.NOT_NEW_FLAG				
FROM
	T_PRODUCT			,
	T_PRODUCT_SORT		
WHERE
	T_PRODUCT.SITE_CD			= T_PRODUCT_SORT.SITE_CD			AND
	T_PRODUCT.PRODUCT_AGENT_CD  = T_PRODUCT_SORT.PRODUCT_AGENT_CD   AND
	T_PRODUCT.PRODUCT_SEQ	    = T_PRODUCT_SORT.PRODUCT_SEQ	    
;
