/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: システム管理
--  Title			: 商品写真イメージパス取得
--	Progaram ID		: GET_PRODUCT_IMG_PATH_ORIGINAL
--	Compile Turn	: 0
--  Creation Date	: 10.12.10
--	Update Date		:
--  Author			: k.itoh
/*************************************************************************/
CREATE OR REPLACE FUNCTION GET_PRODUCT_IMG_PATH_ORIGINAL(
	pSITE_CD			IN	VARCHAR2,
	pPRODUCT_ID			IN	VARCHAR2,
	pOBJ_SEQ			IN	VARCHAR2
)
	RETURN	VARCHAR2
IS
BEGIN

	RETURN GET_PRODUCT_IMG_PATH_BASE(pSITE_CD,pPRODUCT_ID,pOBJ_SEQ,'_o');

EXCEPTION
	WHEN OTHERS THEN

		APP_COMM.TRACE(APP_COMM.TRACE_INFO,'GET_PRODUCT_IMG_PATH_ORIGINAL ' || SQLERRM);

		RETURN	NULL;

END GET_PRODUCT_IMG_PATH_ORIGINAL;
/
SHOW ERROR;
