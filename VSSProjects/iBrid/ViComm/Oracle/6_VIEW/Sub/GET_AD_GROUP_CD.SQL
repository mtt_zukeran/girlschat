/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: ユーザページ
--  Title			: 広告グループ取得
--	Progaram ID		: GET_AD_GROUP_CD
--	Compile Turn	: 0
--  Creation Date	: 07.11.28
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
CREATE OR REPLACE FUNCTION GET_AD_GROUP_CD(
	pSITE_CD		IN	VARCHAR2,
	pAD_CD			IN	VARCHAR2
)
	RETURN	VARCHAR2
IS
	CURSOR CS_SITE_AD_GROUP IS SELECT AD_GROUP_CD FROM T_SITE_AD_GROUP
		WHERE
			SITE_CD	= pSITE_CD		AND
			AD_CD	= pAD_CD;


	BUF_AD_GROUP_CD	T_SITE_AD_GROUP.AD_GROUP_CD%TYPE;

BEGIN
	BUF_AD_GROUP_CD := NULL;

	IF (pAD_CD IS NULL) THEN
		RETURN BUF_AD_GROUP_CD;
	END IF;

	OPEN CS_SITE_AD_GROUP;
	FETCH CS_SITE_AD_GROUP INTO BUF_AD_GROUP_CD;
	CLOSE CS_SITE_AD_GROUP;

	RETURN BUF_AD_GROUP_CD;

EXCEPTION
	WHEN OTHERS THEN

		APP_COMM.TRACE(APP_COMM.TRACE_INFO,'GET_AD_GROUP_CD ' || SQLERRM);

		IF CS_SITE_AD_GROUP%ISOPEN THEN
			CLOSE CS_SITE_AD_GROUP;
		END IF;

		RETURN  0;

END GET_AD_GROUP_CD;
/
SHOW ERROR;
