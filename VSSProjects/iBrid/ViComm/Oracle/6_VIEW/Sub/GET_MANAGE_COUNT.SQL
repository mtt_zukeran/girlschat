/*************************************************************************/
--	System			: ViComm
--  Sub System Name	: システム管理
--  Title			: マネージャー管理出演者数取得
--	Progaram ID		: GET_MANAGE_COUNT
--	Compile Turn	: 0
--  Creation Date	: 07.11.01
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
CREATE OR REPLACE FUNCTION GET_MANAGE_COUNT(
	pMANAGER_SEQ	IN	NUMBER
)
	RETURN	NUMBER
IS
	CURSOR CS_CAST IS SELECT COUNT(*) CNT FROM VW_CAST00
		WHERE
			MANAGER_SEQ = pMANAGER_SEQ	AND
			USER_STATUS	= VICOMM_CONST.USER_WOMAN_NORMAL;

	BUF_NUM NUMBER(5);
BEGIN
	BUF_NUM := 0;

	OPEN CS_CAST;
	FETCH CS_CAST INTO BUF_NUM;
	CLOSE CS_CAST;

	RETURN BUF_NUM;

EXCEPTION
	WHEN OTHERS THEN

		APP_COMM.TRACE(APP_COMM.TRACE_INFO,'GET_MANAGE_COUNT ' || SQLERRM);

		IF CS_CAST%ISOPEN THEN
			CLOSE CS_CAST;
		END IF;

		RETURN	0;

END GET_MANAGE_COUNT;
/
SHOW ERROR;
