/*************************************************************************/
--	System			: ViComm
--  Title			: パック設定 View02
--	Progaram ID		: VW_PRODUCT_MOVIE_DTL01
--  Creation Date	: 10.12.10
--	Update Date		:
--  Author			: K.Itoh
/*************************************************************************/
CREATE OR REPLACE VIEW VW_PRODUCT_MOVIE_DTL01(
--	T_PRODUCT_MOVIE_DTL
	SITE_CD					,
	PRODUCT_AGENT_CD        ,
	PRODUCT_SEQ             ,
	OBJ_SEQ                 ,
	CP_SIZE_TYPE            ,
	CP_FILE_FORMAT          ,
	CP_TARGET_CARRIER       ,
	FILE_COUNT				,
	FILE_SIZE				,
-- T_PRODUCT_MOIVE	
	PRODUCT_MOVIE_TYPE
)AS SELECT
	T_PRODUCT_MOVIE_DTL.SITE_CD							,
	T_PRODUCT_MOVIE_DTL.PRODUCT_AGENT_CD        		,
	T_PRODUCT_MOVIE_DTL.PRODUCT_SEQ             		,
	T_PRODUCT_MOVIE_DTL.OBJ_SEQ                 		,
	T_PRODUCT_MOVIE_DTL.CP_SIZE_TYPE            		,
	T_PRODUCT_MOVIE_DTL.CP_FILE_FORMAT          		,
	T_PRODUCT_MOVIE_DTL.CP_TARGET_CARRIER       		,
	COUNT(*)  AS FILE_COUNT                    		,
	MAX(T_PRODUCT_MOVIE_DTL.FILE_SIZE) AS FILE_SIZE 	,
-- T_PRODUCT_MOIVE		
	MAX(T_PRODUCT_MOVIE.PRODUCT_MOVIE_TYPE)						
FROM
	T_PRODUCT_MOVIE_DTL,
	T_PRODUCT_MOVIE
WHERE
	T_PRODUCT_MOVIE.SITE_CD 			= T_PRODUCT_MOVIE_DTL.SITE_CD 			AND
	T_PRODUCT_MOVIE.PRODUCT_AGENT_CD 	= T_PRODUCT_MOVIE_DTL.PRODUCT_AGENT_CD 	AND
	T_PRODUCT_MOVIE.PRODUCT_SEQ 		= T_PRODUCT_MOVIE_DTL.PRODUCT_SEQ 		AND
	T_PRODUCT_MOVIE.OBJ_SEQ 			= T_PRODUCT_MOVIE_DTL.OBJ_SEQ 		
GROUP BY
	T_PRODUCT_MOVIE_DTL.SITE_CD					, 
	T_PRODUCT_MOVIE_DTL.PRODUCT_AGENT_CD		, 
	T_PRODUCT_MOVIE_DTL.PRODUCT_SEQ				, 
	T_PRODUCT_MOVIE_DTL.OBJ_SEQ					, 
	T_PRODUCT_MOVIE_DTL.CP_SIZE_TYPE			, 
	T_PRODUCT_MOVIE_DTL.CP_FILE_FORMAT			, 
	T_PRODUCT_MOVIE_DTL.CP_TARGET_CARRIER       
;

