/*************************************************************************/
--	System			: ViComm
--  Title			: パック設定 View02
--	Progaram ID		: VW_PRODUCT02
--  Creation Date	: 10.12.06
--	Update Date		:
--  Author			: K.Itoh
/*************************************************************************/
CREATE OR REPLACE VIEW VW_PRODUCT02(
--	T_PRODUCT
	SITE_CD						,
	PRODUCT_AGENT_CD	        ,
	PRODUCT_SEQ	                ,
	PRODUCT_ID	                ,
	PRODUCT_TYPE	            ,
	PRODUCT_NM	                ,
	PRODUCT_DISCRIPTION	        ,
	PART_NUMBER	                ,
	PRODUCT_SERIES_SUMMARY	    ,
	CAST_SUMMARY	            ,
	PRODUCT_MAKER_SUMMARY	    ,
	PUBLISH_END_DATE	        ,
	PUBLISH_START_DATE	        ,
	RELEASE_DATE	            ,
	GROSS_PRICE	                ,
	CHARGE_POINT	            ,
	SPECIAL_CHARGE_POINT	    ,
	PUBLISH_FLAG	            ,
	DEL_FLAG	                ,
	REMARKS	                    ,
	LINK_ID	                    ,
	LINK_DATE	                ,
	UNEDITABLE_FLAG	            ,
	REVISION_NO	                ,
	UPDATE_DATE	                ,
	PICKUP_MASK					,
	NOT_NEW_FLAG				,
	ROYALTY_RATE				,
--	T_PRODUCT_AGENT
	PRODUCT_AGENT_NM
)AS SELECT	
-- T_PRODUCT
	T_PRODUCT.SITE_CD					,
	T_PRODUCT.PRODUCT_AGENT_CD         	,
	T_PRODUCT.PRODUCT_SEQ              	,
	T_PRODUCT.PRODUCT_ID               	,
	T_PRODUCT.PRODUCT_TYPE             	,
	T_PRODUCT.PRODUCT_NM               	,
	T_PRODUCT.PRODUCT_DISCRIPTION      	,
	T_PRODUCT.PART_NUMBER              	,
	T_PRODUCT.PRODUCT_SERIES_SUMMARY   	,
	T_PRODUCT.CAST_SUMMARY             	,
	T_PRODUCT.PRODUCT_MAKER_SUMMARY    	,
	T_PRODUCT.PUBLISH_END_DATE         	,
	T_PRODUCT.PUBLISH_START_DATE       	,
	T_PRODUCT.RELEASE_DATE             	,
	T_PRODUCT.GROSS_PRICE              	,
	T_PRODUCT.CHARGE_POINT             	,
	T_PRODUCT.SPECIAL_CHARGE_POINT     	,
	T_PRODUCT.PUBLISH_FLAG             	,
	T_PRODUCT.DEL_FLAG                 	,
	T_PRODUCT.REMARKS                  	,
	T_PRODUCT.LINK_ID                  	,
	T_PRODUCT.LINK_DATE                	,
	T_PRODUCT.UNEDITABLE_FLAG          	,
	T_PRODUCT.REVISION_NO              	,
	T_PRODUCT.UPDATE_DATE              	,
	T_PRODUCT.PICKUP_MASK				,
	T_PRODUCT.NOT_NEW_FLAG				,
	T_PRODUCT.ROYALTY_RATE				,
	T_PRODUCT_AGENT.PRODUCT_AGENT_NM	
FROM
	T_PRODUCT,
	T_PRODUCT_AGENT
WHERE
	T_PRODUCT_AGENT.PRODUCT_AGENT_CD 	= T_PRODUCT.PRODUCT_AGENT_CD 	
;
