/*************************************************************************/
--	System			: ViComm
--  Title			: �{�[�i�X���� View01
--	Progaram ID		: VW_BONUS_LOG01
--  Creation Date	: 09.10.14
--	Update Date		:
--  Author			: i-Brid(R.Suzuki)
/*************************************************************************/
CREATE OR REPLACE VIEW VW_BONUS_LOG01(
	USER_SEQ		,
	LOGIN_ID		,
	BONUS_SEQ		,
	BONUS_POINT		,
	REMARKS			,
	PAYMENT_FLAG	,
	PAYMENT_DATE	,
	CREATE_DATE		,
	REPORT_DAY_TIME
)AS SELECT
	T_USER.USER_SEQ			,
	T_USER.LOGIN_ID			,
	T_BONUS_LOG.BONUS_SEQ	,
	T_BONUS_LOG.BONUS_POINT	,
	T_BONUS_LOG.REMARKS		,
	T_BONUS_LOG.PAYMENT_FLAG,
	T_BONUS_LOG.PAYMENT_DATE,
	T_BONUS_LOG.CREATE_DATE	,
	T_BONUS_LOG.REPORT_DAY_TIME
FROM
	T_USER		,
	T_BONUS_LOG
WHERE
	T_USER.USER_SEQ 	= T_BONUS_LOG.USER_SEQ
;
