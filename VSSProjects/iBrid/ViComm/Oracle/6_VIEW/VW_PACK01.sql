/*************************************************************************/
--	System			: ViComm
--  Title			: パック設定 View01
--	Progaram ID		: VW_PACK01
--  Creation Date	: 09.12.14
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
CREATE OR REPLACE VIEW VW_PACK01(
	SITE_CD			,
	SETTLE_TYPE		,
	SALES_AMT		,
	COMMODITIES_CD	,
	FIRST_TIME_FLAG	,
	UPDATE_DATE		,
	SITE_NM			,
	CODE_NM
)AS SELECT
	T_PACK.SITE_CD			,
	T_PACK.SETTLE_TYPE		,
	T_PACK.SALES_AMT		,
	T_PACK.COMMODITIES_CD	,
	T_PACK.FIRST_TIME_FLAG	,
	T_PACK.UPDATE_DATE		,
	T_SITE.SITE_NM			,
	T_CODE_DTL.CODE_NM
FROM
	T_PACK		,
	T_SITE		,
	T_CODE_DTL	
WHERE
	T_PACK.SITE_CD				= T_SITE.SITE_CD 		(+)	AND	
	'91'						= T_CODE_DTL.CODE_TYPE	(+)	AND	
	T_PACK.SETTLE_TYPE			= T_CODE_DTL.CODE		(+)		
;
