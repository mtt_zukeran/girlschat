/*************************************************************************/
--	System			: ViComm
--	Title			: �o���Ҍf����OBJECT View00
--	Progaram ID		: VW_BBS_OBJS01
--	Creation Date	: 09.08.24
--	Update Date		: 10.06.29
--	Author			: i-Brid
/*************************************************************************/
CREATE OR REPLACE VIEW VW_BBS_OBJS01(
	SITE_CD			,
	USER_SEQ		,
	USER_CHAR_NO	,
	OBJ_SEQ			,
	PICKUP_ID		,
	PICKUP_FLAG
)AS SELECT
	T_CAST_BBS_OBJS.SITE_CD			,
	T_CAST_BBS_OBJS.USER_SEQ		,
	T_CAST_BBS_OBJS.USER_CHAR_NO	,
	T_CAST_BBS_OBJS.OBJ_SEQ			,
	T_PICKUP_OBJS.PICKUP_ID			,
	T_PICKUP_OBJS.PICKUP_FLAG
FROM
	T_PICKUP_OBJS		,
	T_CAST_BBS_OBJS		
WHERE
	T_PICKUP_OBJS.OBJ_SEQ = T_CAST_BBS_OBJS.OBJ_SEQ
;
