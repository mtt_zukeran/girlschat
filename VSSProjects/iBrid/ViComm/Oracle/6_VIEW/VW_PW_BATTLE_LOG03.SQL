/*************************************************************************/
--	System			: ViComm
--  Title			: ｿｰｼｬﾙｹﾞｰﾑ 女性ﾊﾞﾄﾙﾛｸﾞ関連ﾃﾞｰﾀ一覧
--	Progaram ID		: VW_PW_BATTLE_LOG03
--  Creation Date	: 11.09.23
--	Update Date		: 
--  Author			: PW Y.Ikemiya
/*************************************************************************/
CREATE OR REPLACE VIEW VW_PW_BATTLE_LOG03(
	SITE_CD								,
	USER_SEQ							,
	USER_CHAR_NO						,
	PARTNER_USER_SEQ					,
	PARTNER_USER_CHAR_NO				,
	BATTLE_LOG_SEQ						,
	BATTLE_TYPE							,
	ATTACK_WIN_FLAG						,
	DEFENCE_WIN_FLAG					,
	ATTACK_POWER						,
	USE_ATTACK_COUNT					,
	USE_DEFENCE_COUNT					,
	MOVE_GAME_POINT						,
	CREATE_DATE							,
	BONUS_GET_FLAG						,
	CAST_TREASURE_SEQ					,
	CAST_TREASURE_NM					,
	STAGE_GROUP_TYPE					,
--
	GAME_HANDLE_NM						,
	GAME_CHARACTER_TYPE					,
	TUTORIAL_MISSION_FLAG				,
	TUTORIAL_MISSION_TREASURE_FLAG		,
	TUTORIAL_BATTLE_FLAG				,
	TUTORIAL_BATTLE_TREASURE_FLAG
) AS SELECT
	TBL.SITE_CD																					,
	TBL.USER_SEQ																				,
	TBL.USER_CHAR_NO																			,
	TBL.PARTNER_USER_SEQ																		,
	TBL.PARTNER_USER_CHAR_NO																	,
	TBL.BATTLE_LOG_SEQ																			,
	TBL.BATTLE_TYPE																				,
	TBL.ATTACK_WIN_FLAG																			,
	TBL.DEFENCE_WIN_FLAG																		,
	TBL.ATTACK_POWER																			,
	TBL.USE_ATTACK_COUNT																		,
	TBL.USE_DEFENCE_COUNT																		,
	TBL.MOVE_GAME_POINT																			,
	TBL.CREATE_DATE																				,
	TBL.BONUS_GET_FLAG																			,
	TCT.CAST_TREASURE_SEQ																		,
	TCT.CAST_TREASURE_NM																		,
	TCT.STAGE_GROUP_TYPE																		,
--
	PARTNER.GAME_HANDLE_NM																		,
	PARTNER.GAME_CHARACTER_TYPE																	,
	PARTNER.TUTORIAL_MISSION_FLAG																,
	PARTNER.TUTORIAL_MISSION_TREASURE_FLAG														,
	PARTNER.TUTORIAL_BATTLE_FLAG																,
	PARTNER.TUTORIAL_BATTLE_TREASURE_FLAG
FROM
	T_BATTLE_LOG			TBL			,
	T_GAME_CHARACTER		PARTNER		,
	T_CAST_TREASURE			TCT			
WHERE
	TBL.SITE_CD 				= PARTNER.SITE_CD				AND
	TBL.PARTNER_USER_SEQ		= PARTNER.USER_SEQ				AND
	TBL.PARTNER_USER_CHAR_NO	= PARTNER.USER_CHAR_NO			AND
	TBL.SITE_CD					= TCT.SITE_CD				(+)	AND
	TBL.AIM_TREASURE_SEQ		= TCT.CAST_TREASURE_SEQ		(+)	
;