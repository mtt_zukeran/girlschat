/*************************************************************************/
--	System			: ViComm
--  Title			: �� View01
--	Progaram ID		: VW_URGE01
--  Creation Date	: 09.12.14
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
CREATE OR REPLACE VIEW VW_URGE01(
	USER_SEQ				,
	URGE_LEVEL				,
	MAX_URGE_COUNT			,
	URGE_COUNT_TODAY		,
	URGE_RESTART_DAY		,
	URGE_END_DAY			,
	URGE_EXEC_LAST_DATE		,
	URGE_EXEC_LAST_STATUS	,
	URGE_EXEC_COUNT			,
	URGE_CONNECT_COUNT		,
	REVISION_NO				,
	UPDATE_DATE				,
	URGE_EXEC_LAST_STATUS_NM
)AS SELECT
	T_URGE.USER_SEQ					,
	T_URGE.URGE_LEVEL				,
	T_URGE.MAX_URGE_COUNT			,
	T_URGE.URGE_COUNT_TODAY			,
	T_URGE.URGE_RESTART_DAY			,
	T_URGE.URGE_END_DAY				,
	T_URGE.URGE_EXEC_LAST_DATE		,
	T_URGE.URGE_EXEC_LAST_STATUS	,
	T_URGE.URGE_EXEC_COUNT			,
	T_URGE.URGE_CONNECT_COUNT		,
	T_URGE.REVISION_NO				,
	T_URGE.UPDATE_DATE				,
	T_CODE_DTL.CODE_NM	
FROM
	T_URGE		,
	T_CODE_DTL	
WHERE
	'94'						= T_CODE_DTL.CODE_TYPE	(+)	AND	
	T_URGE.URGE_EXEC_LAST_STATUS= T_CODE_DTL.CODE		(+)		
;
