/*************************************************************************/
--	System			: ViComm
--  Title			: ����ٹް� ����۸޲��ްŽ���шꗗ
--	Progaram ID		: VW_PW_GAME_ITEM03
--  Creation Date	: 11.08.08
--	Update Date		: 
--  Author			: PW Y.Ikemiya
/*************************************************************************/
CREATE OR REPLACE VIEW VW_PW_GAME_ITEM03(
	SITE_CD						,
	GAME_LOGIN_BONUS_START_DAY	,
	GAME_LOGIN_BONUS_END_DAY	,
	SEX_CD						,
	ITEM_COUNT					,
	GAME_POINT					,
--
	GAME_ITEM_SEQ				,
	GAME_ITEM_NM				,
	PUBLISH_FLAG
)AS SELECT
	BONUS.SITE_CD						,
	BONUS.GAME_LOGIN_BONUS_START_DATE	,
	BONUS.GAME_LOGIN_BONUS_END_DATE		,
	BONUS.SEX_CD						,
	BONUS.ITEM_COUNT					,
	BONUS.GAME_POINT					,
--
	ITEM.GAME_ITEM_SEQ					,
	ITEM.GAME_ITEM_NM					,
	ITEM.PUBLISH_FLAG
FROM
	T_GAME_LOGIN_BONUS_DAY	BONUS,
	T_GAME_ITEM				ITEM
WHERE
	BONUS.SITE_CD			= ITEM.SITE_CD			(+)	AND
	BONUS.GAME_ITEM_SEQ		= ITEM.GAME_ITEM_SEQ	(+)	AND
	BONUS.SEX_CD			= ITEM.SEX_CD			(+)		
;