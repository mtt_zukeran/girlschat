/*************************************************************************/
--	System			: ViComm
--  Title			: �T�C�g View01
--	Progaram ID		: VW_SITE01
--  Creation Date	: 08.10.21
--	Update Date		:
--  Author			: i-Brid(K.Tabei)
/*************************************************************************/
CREATE OR REPLACE VIEW VW_SITE01(
	SITE_CD							,
	SITE_NM							,
	SITE_MARK						,
	SITE_TYPE						,
	URL								,
	PRIORITY						,
	PC_REDIRECT_URL					,
	SUPPORT_EMAIL_ADDR				,
	INFO_EMAIL_ADDR					,
	REPORT_EMAIL_ADDR				,
	CAST_REGIST_REPORT_EMAIL_ADDR	,
	SUPPORT_TEL						,
	PAGE_TITLE             			,
	PAGE_KEYWORD           			,
	PAGE_DESCRIPTION				,
	CSS_DOCOMO						,
	CSS_SOFTBANK					,
	CSS_AU							,
	CSS_IPHONE						,
	CSS_ANDROID						,
	SUPPORT_PREMIUM_TALK_FLAG		,
	SUPPORT_KDDI_TV_TEL_FLAG		,
	SUPPORT_CHG_MONITOR_TO_TALK		,
	TRANSFER_FEE					,
	COLOR_LINE						,
	COLOR_INDEX						,
	COLOR_CHAR						,
	COLOR_BACK						,
	COLOR_LINK						,
	COLOR_ALINK						,
	COLOR_VLINK						,
	SIZE_LINE						,
	SIZE_CHAR						,
	COLOR_LINE_WOMAN				,
	COLOR_INDEX_WOMAN				,
	COLOR_CHAR_WOMAN				,
	COLOR_BACK_WOMAN				,
	COLOR_LINK_WOMAN				,
	COLOR_ALINK_WOMAN				,
	COLOR_VLINK_WOMAN				,
	SIZE_LINE_WOMAN					,
	SIZE_CHAR_WOMAN					,
	WEB_PHISICAL_DIR				,
	HOST_NM							,
	SUB_HOST_NM						,
	MAIL_HOST						,
	JOB_OFFER_SITE_HOST_NM			,
	JOB_OFFER_START_DOC				,
	JOB_OFFER_SITE_TYPE				,
	LOCAL_IP_ADDR					,
	LOCAL_PORT						,
	GLOBAL_IP_ADDR					,
	IIS_SITE_IDENTIFIER				,
	IVP_LOC_CD						,
	IVP_SITE_CD						,
	VCS_TENANT_CD					,
	VCS_WEB_LOC_CD					,
	LIGHT_CH_CONNECT_TYPE			,
	NEXT_BUSINESS_TIME				,
	ENABLE_PRIVATE_TALK_MENU_FLAG	,
	USE_OTHER_SYS_INFO_FLAG			,
	CAST_MULTI_CTL_SITE_FLAG		,
	CHAR_NO_ITEM_NM					,
	MULTI_CHAR_FLAG					,
	POINT_PRICE						,
	POINT_TAX						,
	CHARGE_START_POINT				,
	USED_CHARGE_SETTLE_FLAG			,
	URGE_DAYS						,
	RECEIPT_SIGHT					,
	USER_TOP_ID						,
	NON_USER_TOP_ID					,
	CAST_PC_MODIFY_NA_FLAG			,
	VIEW_CALL_COUNT_FLAG			,
	GOOGLE_VERIFICATION				,
	NA_FLAG							,
	MAIL_SEARCH_ENABLED				,
	MAIL_SEARCH_LIMIT				,
	TX_LO_MAILER_IP					,
	TX_HI_MAILER_IP					,
	WHITE_PLAN_NO					,
	FIRST_TIME_FREE_TALK_SEC		,
	MAN_REGIST_FUNC_LIMIT_AD_CD		,
	UPDATE_DATE						,
	REVISION_NO						,
	REGIST_SERVICE_POINT			,
	INVITE_MAIL_AVA_HOUR			,
	RE_INVITE_MAIL_NA_HOUR			,
	RE_BATCH_MAIL_NA_HOUR			,
	TX_MAIL_REF_MARKING				,
	NEW_FACE_DAYS					,
	NEW_FACE_MARK					,
	MAN_NEW_FACE_DAYS				,
	VOICE_NEW_DAYS  				,
	PROFILE_MOVIE_NEW_HOURS			,
	BBS_NEW_HOURS					,
	PIC_BBS_NEW_HOURS				,
	MOVIE_BBS_NEW_HOURS				,
	RX_MAIL_NEW_HOURS				,
	SUPPORT_CHG_OBJ_CAT_FLAG		,
	DISPLAY_MOVIE_UPLOAD_DAYS		,
	FIND_MAN_ELAPASED_DAYS			,
	CM_MAIL_RX_LIMIT				,
	MANAGEMENT_UPDATE_DATE			,
	WEB_FACE_SEQ					,
	HEADER_HTML_DOC					,
	FOOTER_HTML_DOC					,
	INQ_ADMIN_MAN_TYPE_SEQ1			,
	INQ_ADMIN_MAN_TYPE_SEQ2			,
	BBS_DUP_CHARGE_FLAG				,
	ATTACHED_MAIL_DUP_CHARGE_FLAG	,
	USE_ATTACHED_MAIL_FLAG			,
	REGIST_HANDLE_NM_INPUT_FLAG		,
	REGIST_BIRTHDAY_INPUT_FLAG		,
	CAST_REGIST_NEED_AGREE_FLAG		,
	CAST_MINIMUM_PAYMENT			,
	PROFILE_PAGEING_FLAG			,
	PROFILE_NEXT_GUIDANCE			,
	PROFILE_PREVIOUS_GUIDANCE		,
	PROFILE_PAGEING_FLAG2			,
	PROFILE_NEXT_GUIDANCE2			,
	PROFILE_PREVIOUS_GUIDANCE2		,
	NEXT_ACCESS_KEY					,
	PREVIOUS_ACCESS_KEY				,
	RANKING_CREATE_COUNT			,
	TOP_PAGE_SEEK_MODE				,
	CAST_CAN_SELECT_MONITOR_FLAG	,
	CAST_CAN_SELECT_CONNECT_TYPE	,
	MAN_ONLINE_MIN					,
	NON_AD_REGIST_REQ_AGECERT_FLAG	,
	TALK_START_NOT_CHARGE_SEC		,
	TALK2_START_NOT_CHARGE_SEC		,
	REGIST_DM_MAIL_TEMPLATE_NO		,
	REGIST_DM_MAIL_TEMPLATE_NO2		,
	MAQIA_DM_MAIL_TEMPLATE_NO		,
	REGIST_DM_MAIL_TIMING			,
	MAQIA_DM_MAIL_TIMING			,
	MOBILE_MAN_DETAIL_LIST_COUNT	,
    MOBILE_MAN_LIST_COUNT			,
    MOBILE_WOMAN_DETAIL_LIST_COUNT	,
    MOBILE_WOMAN_LIST_COUNT			,
	PICKUP_AUTO_SET_COUNT			,
	BBS_PIC_ATTR_FLAG				,
	BBS_MOVIE_ATTR_FLAG				,
	SUPPORT_VOICE_MONITOR_FLAG		,
	ZERO_SETTLE_SERVICE_POINT		,
	INTRODUCER_RATE_ON_FLAG			,
	CAST_RICHINO_SEEK_PAYMENT_CNT	,
	CAST_RICHINO_SEEK_PAYMENT_AMT	,
	CAST_WRITE_BBS_LIMIT			,
	CAST_WRITE_BBS_INTERVAL_MIN	    ,
	MAN_WRITE_BBS_LIMIT			    ,
	MAN_WRITE_BBS_INTERVAL_MIN	    ,
	PAY_MAN_WRITE_BBS_LIMIT			,
	PAY_MAN_WRITE_BBS_INTERVAL_MIN	,
	MAN_UPLOAD_EMAIL_ADDR			,
	CAST_UPLOAD_EMAIL_ADDR			,
	LOGIN_MAIL_NOT_SEND_FLAG		,
	LOGIN_MAIL2_NOT_SEND_FLAG		,
	LOGOUT_MAIL_NOT_SEND_FLAG		,
	MAN_TALK_END_SEND_MAIL_FLAG		,
	CAST_TALK_END_SEND_MAIL_FLAG	,
	MAN_DEFAULT_HANDLE_NM			,
	MAN_INTRO_POINT_GET_TIMING		,
	CAST_INTRO_POINT_GET_TIMING		,
	IMP_CAST_FIRST_LOGIN_ACT		,
	IMP_MAN_FIRST_LOGIN_ACT			,
	DUMMY_TALK_TX_LOGIN_MAIL_FLAG	,
	MAN_AUTO_LOGIN_RESIGNED_FLAG	,
	MAN_SECESSION_DEL_DATA_FLAG		,
	CAST_AUTO_LOGIN_RESIGNED_FLAG	,
	CAST_SECESSION_DEL_DATA_FLAG	,
	SITE_HTML_DOC_SEX_CHECK_FLAG	,
	HTML_FACE_NAME					,
	USER_INFO_URL					,
	PARENT_WEB_NM					,
	PARENT_WEB_TOP_PAGE_URL			,
	PARENT_WEB_LOGIN_URL			,
	PARENT_WEB_MY_PAGE_URL			,
	WANTED_START_TIME				,
	WANTED_END_TIME					,
	WANTED_ANNOUNCE_TIME			,
	WANTED_BOUNTY_POINT				,
	BUY_POINT_MAIL_COMM_DAYS		,
	LOVE_LIST_PROFILE_DAYS			,
	BBS_RES_SEARCH_LIMIT			,
	WANTED_APPLICANT_DEL_DAYS
)AS SELECT
	T_SITE.SITE_CD									,
	T_SITE.SITE_NM									,
	T_SITE.SITE_MARK								,
	T_SITE.SITE_TYPE								,
	T_SITE.URL										,
	T_SITE.PRIORITY									,
	T_SITE.PC_REDIRECT_URL							,
	T_SITE_MANAGEMENT.SUPPORT_EMAIL_ADDR			,
	T_SITE_MANAGEMENT.INFO_EMAIL_ADDR				,
	T_SITE_MANAGEMENT.REPORT_EMAIL_ADDR				,
	T_SITE_MANAGEMENT.CAST_REGIST_REPORT_EMAIL_ADDR	,
	T_SITE_MANAGEMENT.SUPPORT_TEL					,
	T_SITE.PAGE_TITLE             					,
	T_SITE.PAGE_KEYWORD           					,
	T_SITE.PAGE_DESCRIPTION							,
	T_SITE.CSS_DOCOMO								,
	T_SITE.CSS_SOFTBANK								,
	T_SITE.CSS_AU									,
	T_SITE.CSS_IPHONE								,
	T_SITE.CSS_ANDROID								,
	T_SITE.SUPPORT_PREMIUM_TALK_FLAG				,
	T_SITE.SUPPORT_KDDI_TV_TEL_FLAG					,
	T_SITE.SUPPORT_CHG_MONITOR_TO_TALK				,
	T_SITE.TRANSFER_FEE								,
	T_WEB_FACE.COLOR_LINE							,
	T_WEB_FACE.COLOR_INDEX							,
	T_WEB_FACE.COLOR_CHAR							,
	T_WEB_FACE.COLOR_BACK							,
	T_WEB_FACE.COLOR_LINK							,
	T_WEB_FACE.COLOR_ALINK							,
	T_WEB_FACE.COLOR_VLINK							,
	NVL(T_WEB_FACE.SIZE_LINE,0)						,
	NVL(T_WEB_FACE.SIZE_CHAR,0)						,
	T_WEB_FACE.COLOR_LINE_WOMAN						,
	T_WEB_FACE.COLOR_INDEX_WOMAN					,
	T_WEB_FACE.COLOR_CHAR_WOMAN						,
	T_WEB_FACE.COLOR_BACK_WOMAN						,
	T_WEB_FACE.COLOR_LINK_WOMAN						,
	T_WEB_FACE.COLOR_ALINK_WOMAN					,
	T_WEB_FACE.COLOR_VLINK_WOMAN					,
	NVL(T_WEB_FACE.SIZE_LINE_WOMAN,0)				,
	NVL(T_WEB_FACE.SIZE_CHAR_WOMAN,0)				,
	T_SITE.WEB_PHISICAL_DIR							,
	T_SITE.HOST_NM									,
	T_SITE.SUB_HOST_NM								,
	T_SITE.MAIL_HOST								,
	T_SITE.JOB_OFFER_SITE_HOST_NM					,
	T_SITE.JOB_OFFER_START_DOC						,
	T_SITE.JOB_OFFER_SITE_TYPE						,
	T_SITE.LOCAL_IP_ADDR							,
	T_SITE.LOCAL_PORT								,
	T_SITE.GLOBAL_IP_ADDR							,
	T_SITE.IIS_SITE_IDENTIFIER						,
	T_SITE.IVP_LOC_CD								,
	T_SITE.IVP_SITE_CD								,
	T_SITE.VCS_TENANT_CD							,
	T_SITE.VCS_WEB_LOC_CD							,
	T_SITE.LIGHT_CH_CONNECT_TYPE					,
	T_SITE.NEXT_BUSINESS_TIME						,
	T_SITE.ENABLE_PRIVATE_TALK_MENU_FLAG			,
	T_SITE.USE_OTHER_SYS_INFO_FLAG					,
	T_SITE.CAST_MULTI_CTL_SITE_FLAG					,
	T_SITE.CHAR_NO_ITEM_NM							,
	T_SITE.MULTI_CHAR_FLAG							,
	T_SITE.POINT_PRICE								,
	T_SITE.POINT_TAX								,
	T_SITE.CHARGE_START_POINT						,
	T_SITE.USED_CHARGE_SETTLE_FLAG					,
	T_SITE.URGE_DAYS								,
	T_SITE.RECEIPT_SIGHT							,
	T_SITE.USER_TOP_ID								,
	T_SITE.NON_USER_TOP_ID							,
	T_SITE.CAST_PC_MODIFY_NA_FLAG					,
	T_SITE.VIEW_CALL_COUNT_FLAG						,
	T_SITE.GOOGLE_VERIFICATION						,
	T_SITE.NA_FLAG									,
	T_SITE.MAIL_SEARCH_ENABLED						,
	T_SITE.MAIL_SEARCH_LIMIT						,
	T_SITE.TX_LO_MAILER_IP							,
	T_SITE.TX_HI_MAILER_IP							,
	T_SITE.WHITE_PLAN_NO							,
	T_SITE.FIRST_TIME_FREE_TALK_SEC					,
	T_SITE.MAN_REGIST_FUNC_LIMIT_AD_CD				,
	T_SITE.UPDATE_DATE								,
	T_SITE.REVISION_NO								,
	T_SITE_MANAGEMENT.REGIST_SERVICE_POINT			,
	T_SITE_MANAGEMENT.INVITE_MAIL_AVA_HOUR			,
	T_SITE_MANAGEMENT.RE_INVITE_MAIL_NA_HOUR		,
	T_SITE_MANAGEMENT.RE_BATCH_MAIL_NA_HOUR			,
	T_SITE_MANAGEMENT.TX_MAIL_REF_MARKING			,
	T_SITE_MANAGEMENT.NEW_FACE_DAYS					,
	T_SITE_MANAGEMENT.NEW_FACE_MARK					,
	T_SITE_MANAGEMENT.MAN_NEW_FACE_DAYS				,
	T_SITE_MANAGEMENT.VOICE_NEW_DAYS				,
	T_SITE_MANAGEMENT.PROFILE_MOVIE_NEW_HOURS		,
	T_SITE_MANAGEMENT.BBS_NEW_HOURS					,
	T_SITE_MANAGEMENT.PIC_BBS_NEW_HOURS				,
	T_SITE_MANAGEMENT.MOVIE_BBS_NEW_HOURS			,
	T_SITE_MANAGEMENT.RX_MAIL_NEW_HOURS				,
	T_SITE_MANAGEMENT.SUPPORT_CHG_OBJ_CAT_FLAG		,
	T_SITE_MANAGEMENT.DISPLAY_MOVIE_UPLOAD_DAYS		,
	T_SITE_MANAGEMENT.FIND_MAN_ELAPASED_DAYS		,
	T_SITE_MANAGEMENT.CM_MAIL_RX_LIMIT				,
	T_SITE_MANAGEMENT.UPDATE_DATE					,
	T_SITE_MANAGEMENT.WEB_FACE_SEQ					,
	T_SITE_MANAGEMENT.HEADER_HTML_DOC				,
	T_SITE_MANAGEMENT.FOOTER_HTML_DOC				,
	T_SITE_MANAGEMENT.INQ_ADMIN_MAN_TYPE_SEQ1		,
	T_SITE_MANAGEMENT.INQ_ADMIN_MAN_TYPE_SEQ2		,
	T_SITE_MANAGEMENT.BBS_DUP_CHARGE_FLAG			,
	T_SITE_MANAGEMENT.ATTACHED_MAIL_DUP_CHARGE_FLAG	,
	T_SITE_MANAGEMENT.USE_ATTACHED_MAIL_FLAG		,
	T_SITE_MANAGEMENT.REGIST_HANDLE_NM_INPUT_FLAG	,
	T_SITE_MANAGEMENT.REGIST_BIRTHDAY_INPUT_FLAG	,
	T_SITE_MANAGEMENT.CAST_REGIST_NEED_AGREE_FLAG	,
	T_SITE_MANAGEMENT.CAST_MINIMUM_PAYMENT			,
	T_SITE_MANAGEMENT.PROFILE_PAGEING_FLAG			,
	T_SITE_MANAGEMENT.PROFILE_NEXT_GUIDANCE			,
	T_SITE_MANAGEMENT.PROFILE_PREVIOUS_GUIDANCE		,
	T_SITE_MANAGEMENT.PROFILE_PAGEING_FLAG2			,
	T_SITE_MANAGEMENT.PROFILE_NEXT_GUIDANCE2		,
	T_SITE_MANAGEMENT.PROFILE_PREVIOUS_GUIDANCE2	,
	T_SITE_MANAGEMENT.NEXT_ACCESS_KEY				,
	T_SITE_MANAGEMENT.PREVIOUS_ACCESS_KEY			,
	T_SITE_MANAGEMENT.RANKING_CREATE_COUNT			,
	T_SITE_MANAGEMENT.TOP_PAGE_SEEK_MODE			,
	T_SITE_MANAGEMENT.CAST_CAN_SELECT_MONITOR_FLAG	,
	T_SITE_MANAGEMENT.CAST_CAN_SELECT_CONNECT_TYPE	,
	T_SITE_MANAGEMENT.MAN_ONLINE_MIN				,
	T_SITE_MANAGEMENT.NON_AD_REGIST_REQ_AGECERT_FLAG,
	T_SITE_MANAGEMENT.TALK_START_NOT_CHARGE_SEC		,
	T_SITE_MANAGEMENT.TALK2_START_NOT_CHARGE_SEC	,
	T_SITE_MANAGEMENT.REGIST_DM_MAIL_TEMPLATE_NO	,
	T_SITE_MANAGEMENT.REGIST_DM_MAIL_TEMPLATE_NO2	,
	T_SITE_MANAGEMENT.MAQIA_DM_MAIL_TEMPLATE_NO		,
	T_SITE_MANAGEMENT.REGIST_DM_MAIL_TIMING			,
	T_SITE_MANAGEMENT.MAQIA_DM_MAIL_TIMING			,
	T_SITE_MANAGEMENT.MOBILE_MAN_DETAIL_LIST_COUNT	,
	T_SITE_MANAGEMENT.MOBILE_MAN_LIST_COUNT			,
	T_SITE_MANAGEMENT.MOBILE_WOMAN_DETAIL_LIST_COUNT,
	T_SITE_MANAGEMENT.MOBILE_WOMAN_LIST_COUNT		,
	T_SITE_MANAGEMENT.PICKUP_AUTO_SET_COUNT			,
	T_SITE_MANAGEMENT.BBS_PIC_ATTR_FLAG				,
	T_SITE_MANAGEMENT.BBS_MOVIE_ATTR_FLAG			,
	T_SITE_MANAGEMENT.SUPPORT_VOICE_MONITOR_FLAG	,
	T_SITE_MANAGEMENT.ZERO_SETTLE_SERVICE_POINT		,
	T_SITE_MANAGEMENT.INTRODUCER_RATE_ON_FLAG		,
	T_SITE_MANAGEMENT.CAST_RICHINO_SEEK_PAYMENT_CNT	,
	T_SITE_MANAGEMENT.CAST_RICHINO_SEEK_PAYMENT_AMT	,
	T_SITE_MANAGEMENT.CAST_WRITE_BBS_LIMIT			,
	T_SITE_MANAGEMENT.CAST_WRITE_BBS_INTERVAL_MIN	,
	T_SITE_MANAGEMENT.MAN_WRITE_BBS_LIMIT			,
	T_SITE_MANAGEMENT.MAN_WRITE_BBS_INTERVAL_MIN	,
	T_SITE_MANAGEMENT.PAY_MAN_WRITE_BBS_LIMIT		,
	T_SITE_MANAGEMENT.PAY_MAN_WRITE_BBS_INTERVAL_MIN,
	T_SITE_MANAGEMENT.MAN_UPLOAD_EMAIL_ADDR			,
	T_SITE_MANAGEMENT.CAST_UPLOAD_EMAIL_ADDR		,
	T_SITE_MANAGEMENT.LOGIN_MAIL_NOT_SEND_FLAG		,
	T_SITE_MANAGEMENT.LOGIN_MAIL2_NOT_SEND_FLAG		,
	T_SITE_MANAGEMENT.LOGOUT_MAIL_NOT_SEND_FLAG		,
	T_SITE_MANAGEMENT.MAN_TALK_END_SEND_MAIL_FLAG	,
	T_SITE_MANAGEMENT.CAST_TALK_END_SEND_MAIL_FLAG	,
	T_SITE_MANAGEMENT.MAN_DEFAULT_HANDLE_NM			,
	T_SITE_MANAGEMENT.MAN_INTRO_POINT_GET_TIMING	,
	T_SITE_MANAGEMENT.CAST_INTRO_POINT_GET_TIMING	,
	T_SITE_MANAGEMENT.IMP_CAST_FIRST_LOGIN_ACT		,
	T_SITE_MANAGEMENT.IMP_MAN_FIRST_LOGIN_ACT		,
	T_SITE_MANAGEMENT.DUMMY_TALK_TX_LOGIN_MAIL_FLAG	,
	T_SITE_MANAGEMENT.MAN_AUTO_LOGIN_RESIGNED_FLAG	,
	T_SITE_MANAGEMENT.MAN_SECESSION_DEL_DATA_FLAG	,
	T_SITE_MANAGEMENT.CAST_AUTO_LOGIN_RESIGNED_FLAG	,
	T_SITE_MANAGEMENT.CAST_SECESSION_DEL_DATA_FLAG	,
	T_SITE_MANAGEMENT.SITE_HTML_DOC_SEX_CHECK_FLAG	,
	T_WEB_FACE.HTML_FACE_NAME						,
	T_OTHER_WEB_SITE.USER_INFO_URL					,
	T_OTHER_WEB_SITE.SITE_NM						,
	T_OTHER_WEB_SITE.TOP_PAGE_URL					,
	T_OTHER_WEB_SITE.LOGIN_URL						,
	T_OTHER_WEB_SITE.MY_PAGE_URL					,
	T_SITE_MANAGEMENT_EX.WANTED_START_TIME			,
	T_SITE_MANAGEMENT_EX.WANTED_END_TIME			,
	T_SITE_MANAGEMENT_EX.WANTED_ANNOUNCE_TIME		,
	T_SITE_MANAGEMENT_EX.WANTED_BOUNTY_POINT		,
	T_SITE_MANAGEMENT_EX.BUY_POINT_MAIL_COMM_DAYS	,
	T_SITE_MANAGEMENT_EX.LOVE_LIST_PROFILE_DAYS		,
	T_SITE_MANAGEMENT_EX.BBS_RES_SEARCH_LIMIT		,
	T_SITE_MANAGEMENT_EX.WANTED_APPLICANT_DEL_DAYS
FROM
	T_SITE				,
	T_SITE_MANAGEMENT	,
	T_SITE_MANAGEMENT_EX,
	T_OTHER_WEB_SITE	,
	T_WEB_FACE
WHERE
	T_SITE.SITE_CD			= T_SITE_MANAGEMENT.SITE_CD				(+)	AND
	T_SITE.SITE_CD			= T_SITE_MANAGEMENT_EX.SITE_CD			(+)	AND
	T_SITE.OTHER_WEB_SYS_ID	= T_OTHER_WEB_SITE.OTHER_WEB_SYS_ID		(+)	AND
	T_SITE_MANAGEMENT.WEB_FACE_SEQ = T_WEB_FACE.WEB_FACE_SEQ		(+)
;
