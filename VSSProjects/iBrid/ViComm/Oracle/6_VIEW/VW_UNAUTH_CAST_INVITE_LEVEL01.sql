/*************************************************************************/
--	System			: ViComm
--  Title			: ���F�؃L���X�g���U���x�� View01
--	Progaram ID		: VW_UNAUTH_CAST_INVITE_LEVEL01
--  Creation Date	: 10.05.28
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
CREATE OR REPLACE VIEW VW_UNAUTH_CAST_INVITE_LEVEL01(
	SITE_CD			,
	INVITE_LEVEL	,
	DELAY_DAYS		,
	REVISION_NO		,
	UPDATE_DATE		,
	SITE_NM
)AS SELECT
	T_UNAUTH_CAST_INVITE_LEVEL.SITE_CD		,
	T_UNAUTH_CAST_INVITE_LEVEL.INVITE_LEVEL	,
	T_UNAUTH_CAST_INVITE_LEVEL.DELAY_DAYS	,
	T_UNAUTH_CAST_INVITE_LEVEL.REVISION_NO	,
	T_UNAUTH_CAST_INVITE_LEVEL.UPDATE_DATE	,
	T_SITE.SITE_NM
FROM
	T_UNAUTH_CAST_INVITE_LEVEL	,
	T_SITE
WHERE
	T_UNAUTH_CAST_INVITE_LEVEL.SITE_CD	= T_SITE.SITE_CD
;
