/*************************************************************************/
--	System			: ViComm
--  Title			: �\�[�V�����Q�[�� �F�B�Љ�l���A�C�e��
--	Progaram ID		: VW_PW_GAME_INTRO_ITEM01
--  Creation Date	: 12.02.08
--	Update Date		: 
--  Author			: PW K.Miyazato
/*************************************************************************/
CREATE OR REPLACE VIEW VW_PW_GAME_INTRO_ITEM01(
	SITE_CD					,
	GAME_INTRO_SEQ			,
	ITEM_COUNT				,
	GAME_ITEM_SEQ			,
	SEX_CD					,
	GAME_ITEM_NM			,
	ATTACK_POWER			,
	DEFENCE_POWER			,
	ENDURANCE				,
	DESCRIPTION				,
	REMARKS					,
	GAME_ITEM_GET_CD		,
	PRICE					,
	PRESENT_FLAG			,
	PRESENT_GAME_ITEM_SEQ	,
	STAGE_SEQ				,
	GAME_ITEM_CATEGORY_TYPE	,
	PUBLISH_FLAG			,
	PUBLISH_DATE			,
	CREATE_DATE				
) AS SELECT
	GII.SITE_CD							,
	GII.GAME_INTRO_SEQ					,
	GII.ITEM_COUNT						,
	GI.GAME_ITEM_SEQ					,
	GI.SEX_CD							,
	GI.GAME_ITEM_NM						,
	GI.ATTACK_POWER						,
	GI.DEFENCE_POWER					,
	GI.ENDURANCE						,
	GI.DESCRIPTION						,
	GI.REMARKS							,
	GI.GAME_ITEM_GET_CD					,
	GI.PRICE							,
	GI.PRESENT_FLAG						,
	GI.PRESENT_GAME_ITEM_SEQ			,
	GI.STAGE_SEQ						,
	GI.GAME_ITEM_CATEGORY_TYPE			,
	GI.PUBLISH_FLAG						,
	GI.PUBLISH_DATE						,
	GI.CREATE_DATE						
FROM
	T_GAME_INTRO_ITEM	GII,
	T_GAME_ITEM			GI
WHERE
	GII.SITE_CD			= GI.SITE_CD		AND
	GII.GAME_ITEM_SEQ	= GI.GAME_ITEM_SEQ	
;