/*************************************************************************/
--	System			: ViCOMM
--  Sub System Name	: 番号管理
--  Title			: シーケンス（SEQ No :,ボイスSEQ）
--	Progaram ID		: SEQ_VOICE
--	Compile Turn	: 0
--  Creation Date	: 10.07.27
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
--DROP SEQUENCE SEQ_VOICE;

CREATE SEQUENCE SEQ_VOICE
       START WITH 200000 
       INCREMENT BY 1
       MINVALUE 200000
       MAXVALUE 999999999999999
       NOCACHE
       ORDER
       CYCLE
;

--SELECT SEQ_MOVIE.NEXTVAL FROM DUAL;
