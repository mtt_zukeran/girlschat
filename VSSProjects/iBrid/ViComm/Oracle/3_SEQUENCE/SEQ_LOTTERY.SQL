/*************************************************************************/
--	System			: ViCOMM
--	Sub System Name	: 番号管理
--	Title			: シーケンス（SEQ No :抽選SEQ）
--	Progaram ID		: SEQ_LOTTERY
--	Compile Turn	: 0
--	Creation Date	: 11.07.19
--	Update Date		:
--	Author			: iBrid
/*************************************************************************/
DROP SEQUENCE SEQ_LOTTERY;

CREATE SEQUENCE SEQ_LOTTERY
	START WITH 2000
	INCREMENT BY 1
	MINVALUE 2000
	MAXVALUE 999999999999999
	NOCACHE
	ORDER
	CYCLE
;

--SELECT SEQ_LOTTERY.NEXTVAL FROM DUAL;

