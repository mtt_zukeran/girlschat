/*************************************************************************/
--	System			: ViCOMM
--  Sub System Name	: 番号管理
--  Title			: シーケンス（入金ＳＥＱ）
--	Progaram ID		: 
--  Creation Date	: 09.06.01
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
DROP SEQUENCE SEQ_RECEIPT;

CREATE SEQUENCE SEQ_RECEIPT
       START WITH 1 
       INCREMENT BY 1
       MINVALUE 1
       MAXVALUE 999999999999999
       NOCACHE
       ORDER
       CYCLE
;

--SELECT SEQ_RECEIPT.NEXTVAL FROM DUAL;
