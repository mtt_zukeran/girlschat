/*************************************************************************/
--	System			: ViCOMM
--  Sub System Name	: 番号管理
--  Title			: シーケンス（SEQ No :,マネージャーSEQ）
--	Progaram ID		: SEQ_MANAGER
--	Compile Turn	: 0
--  Creation Date	: 09.06.01
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
DROP SEQUENCE SEQ_MANAGER;

CREATE SEQUENCE SEQ_MANAGER
       START WITH 200 
       INCREMENT BY 1
       MINVALUE 200
       MAXVALUE 999999999999999
       NOCACHE
       ORDER
       CYCLE
;

--SELECT SEQ_MANAGER.NEXTVAL FROM DUAL;
