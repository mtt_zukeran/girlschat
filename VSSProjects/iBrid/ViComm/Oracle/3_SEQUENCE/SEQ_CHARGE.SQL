/*************************************************************************/
--	System			: ViCOMM
--  Sub System Name	: 番号管理
--  Title			: シーケンス（SEQ No :,課金SEQ）
--	Progaram ID		: SEQ_CHARGE
--	Compile Turn	: 0
--  Creation Date	: 09.06.01
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
DROP SEQUENCE SEQ_CHARGE;

CREATE SEQUENCE SEQ_CHARGE
       START WITH 2000000 
       INCREMENT BY 1
       MINVALUE 2000000
       MAXVALUE 999999999999999
       NOCACHE
       ORDER
       CYCLE
;

--SELECT SEQ_CHARGE.NEXTVAL FROM DUAL;
