/*************************************************************************/
--	System			: ViCOMM
--  Sub System Name	: 番号管理
--  Title			: シーケンス（文章SEQ）
--	Progaram ID		: 
--  Creation Date	: 09.06.01
--	Update Date		:
--  Author			: i-Brid
/*************************************************************************/
DROP SEQUENCE SEQ_DOC;

CREATE SEQUENCE SEQ_DOC
       START WITH 1000 
       INCREMENT BY 1
       MINVALUE 1000
       MAXVALUE 999999999999999
       NOCACHE
       ORDER
       CYCLE
;

--SELECT SEQ_DOC.NEXTVAL FROM DUAL;
