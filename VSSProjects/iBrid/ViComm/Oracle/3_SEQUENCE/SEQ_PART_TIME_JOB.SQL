/*************************************************************************/
--	System			: ViCOMM
--	Sub System Name	: 番号管理
--	Title			: シーケンス（SEQ No :バイトプランSEQ）
--	Progaram ID		: SEQ_PART_TIME_JOB_PLAN
--	Compile Turn	: 0
--	Creation Date	: 11.07.19
--	Update Date		:
--	Author			: iBrid
/*************************************************************************/
DROP SEQUENCE SEQ_PART_TIME_JOB;

CREATE SEQUENCE SEQ_PART_TIME_JOB
	START WITH 1000
	INCREMENT BY 1
	MINVALUE 1000
	MAXVALUE 999999999999999
	NOCACHE
	ORDER
	CYCLE
;

--SELECT SEQ_PART_TIME_JOB_PLAN.NEXTVAL FROM DUAL;

