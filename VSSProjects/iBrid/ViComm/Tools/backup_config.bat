@ECHO OFF
REM == 引数 ==
SET site_identity=%~1

IF ""=="%site_identity%" (
	ECHO argument error.site_identity
	EXIT
)


REM == 設定 ==
SET backup_dir_base=\\10.0.1.200\ViComm-Config
SET web_dir_base=D:\i-Brid\Site


REM == 環境情報取得 ==
SET current_drive=%~d0
SET current_dir=%~dp0

REM == ログファイル名生成 ==
SET yyyymmdd=%date:/=%
SET yyyymmdd=%yyyymmdd: =0%
SET hhmmss=%time::=%
SET hhmmss=%hhmmss: =0%
SET log_fname=log_%yyyymmdd:~0,8%%hhmmss:~0,6%.log

REM ==================================================================================

SET backup_dir=%backup_dir_base%\%COMPUTERNAME%\%site_identity%\%yyyymmdd:~0,8%%hhmmss:~0,6%
SET web_dir=%web_dir_base%\%site_identity%

XCOPY /I %web_dir%\Admin\Web.config			"%backup_dir%\Admin\"
XCOPY /I %web_dir%\AutoRegist\Web.config	"%backup_dir%\AutoRegist\"
XCOPY /I %web_dir%\CastPC\Web.config		"%backup_dir%\CastPC\"
XCOPY /I %web_dir%\IVPInterface\Web.config	"%backup_dir%\IVPInterface\"
XCOPY /I %web_dir%\Mail\Web.config			"%backup_dir%\Mail\"
XCOPY /I %web_dir%\Settle\Web.config		"%backup_dir%\Settle\"
XCOPY /I %web_dir%\User\Web.config			"%backup_dir%\User\"

@ECHO ON
EXIT /B 0;
