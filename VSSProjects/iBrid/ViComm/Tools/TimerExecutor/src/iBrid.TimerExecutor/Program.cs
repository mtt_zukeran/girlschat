using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace iBrid.TimerExecutor {
	class Program {
		static void Main(string[] args) {
			if(args.Length<2){
				Console.WriteLine("argument error");
				Environment.Exit(-1);
			}				
			
			int timeout = 0;
			if(!int.TryParse(args[0],out timeout)){
				Console.WriteLine("argument[timeout] error");
				Environment.Exit(-1);
			}
			
			StringBuilder argumentsBuilder = new StringBuilder();
			for (int i = 2;i<args.Length;i++) {
				if(i>2)argumentsBuilder.Append(" ");
				argumentsBuilder.AppendFormat("\"{0}\"",args[i]);
			}
			
			ProcessStartInfo psi = new ProcessStartInfo(args[1],argumentsBuilder.ToString());
			psi.WindowStyle = ProcessWindowStyle.Hidden;

			Process p = Process.Start(psi);
			if(p.WaitForExit(timeout)){
				Environment.Exit(p.ExitCode);
				return;
			}else{
				p.Kill();
				Environment.Exit(-1);
				return;
			}
		}
		
	}
}
