using System;
using System.Collections.Generic;
using System.Text;

namespace RakutenStub {
	public interface IDispatcher {
		void Dispatch(System.Web.HttpContext ctx);
	}
}
