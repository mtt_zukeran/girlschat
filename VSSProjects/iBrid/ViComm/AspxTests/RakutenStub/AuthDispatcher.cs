using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace RakutenStub {
	public class AuthDispatcher : IDispatcher {
		public void Dispatch(HttpContext ctx){
			HttpRequest request = ctx.Request;
			string mode = request.Params["openid.mode"];
			string url = null;
			switch (mode) {
				case "checkid_setup":
					url = Utility.GenerateUrl("IdResDriver.aspx", request.QueryString);
					break;
				case "check_authentication":
					url = Utility.GenerateUrl("CheckAuthenticationStub.aspx", request.QueryString);
					break;
				default:
					string errMessage = string.Format("認識できないモード openid.mode={0}", mode);
					throw new InvalidOperationException(errMessage);
			}
			ctx.Server.Transfer(url);
		}
	}
}
