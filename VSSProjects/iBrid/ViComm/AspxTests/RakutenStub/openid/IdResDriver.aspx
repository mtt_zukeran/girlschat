﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="IdResDriver.aspx.cs" Inherits="RakutenStub.openid.IdResDriver" %>
<html>
	<body>
		<h4>OpenId引渡しドライバ</h4>    
		<form action="<%= ReturnTo %>" method="post">
			<input type="text" size="100"name="openid.ns"                 value='<%= System.Web.HttpUtility.UrlDecode("http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0") %>' /><br />
			<input type="text" size="100"name="openid.op_endpoint"        value='<%= System.Web.HttpUtility.UrlDecode("https%3A%2F%2Fapi.id.rakuten.co.jp%2Fopenid%2Fauth") %>' /><br />
			<input type="text" size="100"name="openid.claimed_id"         value='<%= OpenId  %>'/><br />
			<input type="text" size="100"name="openid.response_nonce"     value='<%= System.Web.HttpUtility.UrlDecode("2008-09-04T04%3A58%3A20Z0") %>' /><br />
			<input type="text" size="100"name="openid.mode"               value='<%= System.Web.HttpUtility.UrlDecode("id_res") %>' /><br />
			<input type="text" size="100"name="openid.identity"           value='<%= OpenId  %>'/><br />
			<input type="text" size="100"name="openid.return_to"          value='<%= ReturnTo %>' /><br />
			<input type="text" size="100"name="openid.assoc_handle"       value='<%= System.Web.HttpUtility.UrlDecode("ce1b14fb7941fcd9") %>' /><br />
			<input type="text" size="100"name="openid.signed"             value='<%= System.Web.HttpUtility.UrlDecode("op_endpoint%2Cclaimed_id%2Cidentity%2Creturn_to%2Cresponse_nonce%2Cassoc_handle") %>' /><br />
			<input type="text" size="100"name="openid.sig"                value='<%= System.Web.HttpUtility.UrlDecode("xbWVm2b4Xn4GF4O7v2opgPPrElHltmXokC1xgpjUgGw%3D") %>' /><br />
			<% if(WithNickName){ %>
			<input type="text" size="100"name="openid.ns.ax"              value='<%= System.Web.HttpUtility.UrlDecode("http%3A%2F%2Fopenid.net%2Fsrv%2Fax%2F1.0") %>' /><br />
			<input type="text" size="100"name="openid.ax.mode"            value='<%= System.Web.HttpUtility.UrlDecode("fetch_response") %>' /><br />
			<input type="text" size="100"name="openid.ax.type.nickname"   value='<%= System.Web.HttpUtility.UrlDecode("http%3A%2F%2Fschema.openid.net%2FnamePerson%2Ffriendly") %>' /><br />
			<input type="text" size="100"name="openid.ax.value.nickname"  value='<%= System.Web.HttpUtility.UrlDecode("hogehoge") %>' /><br />
			<% } %>
			<br />
			<input type="submit" value='実行' />
		</form>
	</body>
</html>