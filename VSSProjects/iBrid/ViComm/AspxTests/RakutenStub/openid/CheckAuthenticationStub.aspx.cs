using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

namespace RakutenStub.openid {
	public partial class CheckAuthenticationStub : System.Web.UI.Page {
		protected void Page_Load(object sender, EventArgs e) {
			
			// 要求のValidate(Stubなのであまり厳密にはしない)
			bool isOk = true;
			isOk = isOk && ValidateParam("openid.ns", "http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0");
			isOk = isOk && ValidateParam("openid.op_endpoint", "https%3A%2F%2Fapi.id.rakuten.co.jp%2Fopenid%2Fauth");
			isOk = isOk && ValidateParam("openid.response_nonce", "2008-09-04T04%3A58%3A20Z0");
			isOk = isOk && ValidateParam("openid.mode", "check_authentication");
			isOk = isOk && ValidateParam("openid.assoc_handle", "ce1b14fb7941fcd9");
			isOk = isOk && ValidateParam("openid.signed", "op_endpoint%2Cclaimed_id%2Cidentity%2Creturn_to%2Cresponse_nonce%2Cassoc_handle");
			isOk = isOk && ValidateParam("openid.sig", "xbWVm2b4Xn4GF4O7v2opgPPrElHltmXokC1xgpjUgGw%3D");
			
			if(!string.IsNullOrEmpty(Request.QueryString["openid.ax.mode"])){
				isOk = isOk && ValidateParam("openid.ns.ax", "http%3A%2F%2Fopenid.net%2Fsrv%2Fax%2F1.0");
				isOk = isOk && ValidateParam("openid.ax.mode", "fetch_response");
				isOk = isOk && ValidateParam("openid.ax.type.nickname", "http%3A%2F%2Fschema.openid.net%2FnamePerson%2Ffriendly");
			}
			
		
			Response.Clear();
			
			StringBuilder responseBuilder = new StringBuilder();

			responseBuilder.AppendFormat("is_valid:{0}",isOk.ToString().ToLower()).AppendLine();
			responseBuilder.Append("ns:http://specs.openid.net/auth/2.0").AppendLine();

			Response.Write(responseBuilder.ToString());
		}
		
		private bool ValidateParam(string paramName,string expectedValue){
			
			string actualValue = this.Request.QueryString[paramName];
			
			return HttpUtility.UrlDecode(expectedValue).ToLower().Equals(actualValue.ToLower());
		}
	}
}
