using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Security.Cryptography;

namespace RakutenStub.openid {
	public partial class IdResDriver : System.Web.UI.Page {
		protected string ReturnTo{
			get{return ViewState["ReturnTo"] as string;}
			set{ViewState["ReturnTo"] = value;}
		}
		protected bool WithNickName{
			get{return (ViewState["WithNickName"] as bool?)??false;}
			set{ ViewState["WithNickName"] = value;}
		}
		protected string OpenId{
			get { return (ViewState["OpenId"] as string) ?? string.Empty; }
			set { ViewState["OpenId"] = value; }
		}
		
		protected void Page_Load(object sender, EventArgs e) {
		
			if(!this.IsPostBack){
				this.ReturnTo = this.Request.QueryString["openid.return_to"];
				this.WithNickName = (!string.IsNullOrEmpty(this.Request.QueryString["openid.ax.mode"]) && this.Request.QueryString["openid.ax.mode"].Equals("fetch_response"));
				
				
				HashAlgorithm hash = new MD5CryptoServiceProvider();
				
				string openId = Convert.ToBase64String(Encoding.UTF8.GetBytes(Guid.NewGuid().ToString()));
				this.OpenId = HttpUtility.UrlEncode(string.Format("https://myid.rakuten.co.jp/openid/user/{0}", openId));
				
				
				this.DataBind();
			}
			
		}
		
		
	}
}
