using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace RakutenStub {
	public class Global : System.Web.HttpApplication {

		protected void Application_Start(object sender, EventArgs e) {

		}		

		protected void Application_End(object sender, EventArgs e) {

		}
		
		protected void Application_BeginRequest(object sender, EventArgs e) {
			string path = HttpContext.Current.Request.Path;

			IDispatcher dispatcher = null;
			if (path.ToLower().Contains("/openid/auth.aspx")) {
				dispatcher = new AuthDispatcher();
			} else if (path.ToLower().Contains("/myc_m/stepin/dl_1_0.aspx")) {
				dispatcher = new CheckoutDispatcher();
			}

			if (dispatcher != null) dispatcher.Dispatch(HttpContext.Current);
			
		}
	}
}