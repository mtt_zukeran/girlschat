﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderCompleteRequestDriver.aspx.cs" Inherits="RakutenStub.myc_m.stepin.OrderCompleteRequestDriver" ValidateRequest="false"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <form id="frmMain" runat="server">
    <div>
		<h4>■注文通知送信ドライバ■</h4>
		<br />
		<h5>商品情報</h5>		
		<br />
		<asp:TextBox ID="txtXml" runat="server" Rows="20" TextMode="MultiLine"></asp:TextBox><br /><br />
		<asp:Button ID="btnSend" runat="server" Text="注文通知送信" OnClick="btnSend_Click" />
		<h5>注文通知結果</h5>
		<asp:TextBox ID="txtResult" runat="server" Rows="20" TextMode="MultiLine"></asp:TextBox><br /><br />
		
		<h5>画面遷移</h5>
		決済終了時戻りURL：<asp:TextBox ID="txtCompletedUrl" runat="server" ></asp:TextBox><br />
		<asp:Button ID="btnComplete" runat="server" Text="成功遷移" OnClick="btnComplete_Click" /><br />
		<asp:TextBox ID="txtFailedUrl" runat="server" ></asp:TextBox><br />
		<asp:Button ID="btnFailed" runat="server" Text="失敗遷移" OnClick="btnFailed_Click" /><br />
	</div>
    </form>
    
</body>
</html>
