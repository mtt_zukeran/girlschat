using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Specialized;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Xml.XPath;
using System.Text;
using System.IO;
using System.Net;

namespace RakutenStub.myc_m.stepin {
	public partial class OrderCompleteRequestDriver : System.Web.UI.Page {
		
		protected void Page_Load(object sender, EventArgs e) {
			if(!this.IsPostBack){
				string checkout = (string)Session["checkout"];
				
				XmlDocument xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(checkout);
				string isTMode = xmlDoc.SelectNodes("//orderItemsInfo/isTMode")[0].InnerText;
				string orderTotalFee = xmlDoc.SelectNodes("//orderItemsInfo/orderTotalFee")[0].InnerText;
				string orderCartId = xmlDoc.SelectNodes("//orderItemsInfo/orderCartId")[0].InnerText;
				string itemId = xmlDoc.SelectNodes("//orderItemsInfo/itemsInfo/item[1]/itemId")[0].InnerText;
				string itemName = xmlDoc.SelectNodes("//orderItemsInfo/itemsInfo/item[1]/itemName")[0].InnerText;
				string itemNumbers = xmlDoc.SelectNodes("//orderItemsInfo/itemsInfo/item[1]/itemNumbers")[0].InnerText;
				string itemFee = xmlDoc.SelectNodes("//orderItemsInfo/itemsInfo/item[1]/itemFee")[0].InnerText;
				string completedUrl = xmlDoc.SelectNodes("//orderItemsInfo/orderCompleteUrl")[0].InnerText;
				string failedUrl = xmlDoc.SelectNodes("//orderItemsInfo/orderFailedUrl")[0].InnerText;
				
				string xml = null;
				using (StreamReader sr = new StreamReader(MapPath("OrderCompleteRequest.xml"))) {
					xml = sr.ReadToEnd();
				}
				DateTime dt = DateTime.Now;

				xml = xml.Replace(":orderId", string.Format("00{0:yyyyMMdd}-{0:HHmm}-00{0:ssffffff}", dt));
				xml = xml.Replace(":orderControlId", string.Format("dc-00{0:yyyyMMdd}-{0:HHmm}-00{0:ssffffff}", dt));
				xml = xml.Replace(":orderCartId", orderCartId);
				xml = xml.Replace(":isTMode", isTMode);
				xml = xml.Replace(":orderDate", string.Format("{0:yyyy-MM-dd HH:mm:ss}", dt));
				xml = xml.Replace(":orderTotalFee", orderTotalFee);
				xml = xml.Replace(":itemId", itemId);
				xml = xml.Replace(":itemName", itemName);
				xml = xml.Replace(":itemFee", itemFee);
				xml = xml.Replace(":itemNumbers", itemNumbers);
				this.txtXml.Text = xml;

				this.txtCompletedUrl.Text = completedUrl;
				this.txtFailedUrl.Text = failedUrl;
			}			
		}

		protected void btnSend_Click(object sender, EventArgs e) {
			if(!this.IsValid)return;
			
			string sReqXml = this.txtXml.Text;
			string sConfirmId = Convert.ToBase64String(Encoding.UTF8.GetBytes(sReqXml));

			byte[] res;
			NameValueCollection ps = new NameValueCollection();
			ps.Add("confirmId",sConfirmId);
			using(WebClient oWebClient = new WebClient()){
				res = oWebClient.UploadValues(ConfigurationManager.AppSettings["orderCompleteUrl"], "POST", ps);
			}			

			this.txtResult.Text = Encoding.UTF8.GetString(res);
		}
		protected void btnComplete_Click(object sender, EventArgs e) {
			Response.Redirect(this.txtCompletedUrl.Text);
		}
		protected void btnFailed_Click(object sender, EventArgs e) {
			Response.Redirect(this.txtFailedUrl.Text);
		}
	}
}
