﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CheckoutStub.aspx.cs" Inherits="RakutenStub.myc_m.stepin.Checkout" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <form id="frmMain" runat="server">
    <div>
		<h4>動的商品情報送信スタブ</h4>    
		<asp:TextBox ID="txtCheckout" runat="server" TextMode="MultiLine" Rows="15" ></asp:TextBox><br />
		<asp:TextBox ID="txtSig" runat="server"></asp:TextBox><br />
		
		<asp:Button ID="btnRedirectOrderCompleteRequestDriver" runat="server" Text="注文通知送信ドライバへ" OnClick="btnRedirectOrderCompleteRequestDriver_Click"/>
		
    </div>
    </form>
</body>
</html>
