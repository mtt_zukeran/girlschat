using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

namespace RakutenStub.myc_m.stepin {
	public partial class Checkout : System.Web.UI.Page {
		protected string CheckoutValue{
			get { return (string)this.ViewState["CheckoutValue"]; }
			set { this.ViewState["CheckoutValue"] = value; }
		}
	
		protected void Page_Load(object sender, EventArgs e) {
			if(!this.IsPostBack){
				string checkoutValue = this.Request.Params["checkout"];
				string sig = this.Request.Params["sig"];
				
				this.CheckoutValue =  Encoding.UTF8.GetString(Convert.FromBase64String(checkoutValue));
				this.txtCheckout.Text = this.CheckoutValue;
				this.txtSig.Text = sig;
			}
		}

		protected void btnRedirectOrderCompleteRequestDriver_Click(object sender, EventArgs e) {
			Session["checkout"] = this.CheckoutValue;
			Server.Transfer("OrderCompleteRequestDriver.aspx");
		}
	}
}
