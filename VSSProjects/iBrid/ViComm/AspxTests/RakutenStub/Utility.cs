using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Specialized;
using System.Text;

namespace RakutenStub {
	public class Utility {
		public static string GenerateUrl(string baseUrl, System.Collections.Specialized.NameValueCollection parameters) {
			StringBuilder urlBuilder = new StringBuilder();
			urlBuilder.Append(baseUrl);
			if (parameters.Count > 0) {
				urlBuilder.Append("?");
				for (int i = 0; i < parameters.Count; i++) {
					if (i != 0) {
						urlBuilder.Append("&");
					}
					string paramName = parameters.Keys[i];
					string paramValue = parameters[i];
					urlBuilder.AppendFormat("{0}={1}", paramName, HttpUtility.UrlEncode(paramValue));
				}
			}
			return urlBuilder.ToString();
		}
	}
}
