﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: キャストインポート
--	Progaram ID		: CastImport
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.IO;
using System.Text;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class System_CastImport:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
	}


	protected void btnUpload_Click(object sender,EventArgs e) {
		string sInstallDir;
		using (Sys oSys = new Sys()) {
			oSys.GetValue("INSTALL_DIR",out sInstallDir);
		}
		string sFileNm = string.Format("{0}\\Text\\{1}",sInstallDir,"cast.csv");
		string sErrorFileNm = string.Format("{0}\\Text\\CastImportError{1}.txt",sInstallDir,DateTime.Now.ToString("yyyyMMddHHmmssfff"));

		uplCsv.SaveAs(sFileNm);
		string sLine;

		using (StreamWriter writer = new StreamWriter(sErrorFileNm))
		using (StreamReader sr = new StreamReader(sFileNm,Encoding.GetEncoding("Shift_JIS"))) {
			while ((sLine = sr.ReadLine()) != null) {

				string[] sValues = sLine.Split('\t');

				switch (sValues[0]) {
					case "0":
						if (sValues.Length == 67) {
							string[] sAttrTypeSeq = new string[11];
							string[] sAttrSeq = new string[11];
							string[] sInputValue = new string[11];
							int iAttrCount = 0;
							for (int i = 36;i < sValues.Length;i++) {
								int iIdx = (i - 35) % 3;
								switch (iIdx) {
									case 0:
										sInputValue[iAttrCount] = sValues[i];
										iAttrCount++;
										sAttrSeq[iAttrCount] = "";
										sAttrTypeSeq[iAttrCount] = "";
										sInputValue[iAttrCount] = "";
										break;
									case 1:
										sAttrTypeSeq[iAttrCount] = sValues[i];
										break;
									case 2:
										sAttrSeq[iAttrCount] = sValues[i];
										break;
								}
							}
							try {
								using (DbSession db = new DbSession()) {
									db.PrepareProcedure("CAST_IMPORT");
									db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,sValues[1]);
									db.ProcedureInParm("PLOGIN_ID",DbSession.DbType.VARCHAR2,sValues[2]);
									db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,ViCommConst.MAIN_CHAR_NO);
									db.ProcedureInParm("PCAST_NM",DbSession.DbType.VARCHAR2,sValues[3]);
									db.ProcedureInParm("PCAST_KANA_NM",DbSession.DbType.VARCHAR2,sValues[4]);
									db.ProcedureInParm("PCONTACT_TEL",DbSession.DbType.VARCHAR2,sValues[5]);
									db.ProcedureInParm("PCONNECT_TYPE",DbSession.DbType.VARCHAR2,sValues[6]);
									db.ProcedureInParm("PMONITOR_ENABLE_FLAG",DbSession.DbType.NUMBER,sValues[7]);
									db.ProcedureInParm("PONLINE_LIVE_ENABLE_FLAG",DbSession.DbType.NUMBER,sValues[8]);
									db.ProcedureInParm("PPRODUCTION_SEQ",DbSession.DbType.VARCHAR2,sValues[9]);
									db.ProcedureInParm("PMANAGER_SEQ",DbSession.DbType.VARCHAR2,sValues[10]);
									db.ProcedureInParm("PUSE_TERMINAL_TYPE",DbSession.DbType.VARCHAR2,sValues[11]);
									db.ProcedureInParm("PURI",DbSession.DbType.VARCHAR2,sValues[12]);
									db.ProcedureInParm("PTERMINAL_PASSWORD",DbSession.DbType.VARCHAR2,sValues[13]);
									db.ProcedureInParm("PTEL",DbSession.DbType.VARCHAR2,sValues[14]);
									db.ProcedureInParm("PEMAIL_ADDR",DbSession.DbType.VARCHAR2,sValues[15]);
									db.ProcedureInParm("PLOGIN_PASSWORD",DbSession.DbType.VARCHAR2,sValues[16]);
									db.ProcedureInParm("PUSER_STATUS",DbSession.DbType.VARCHAR2,sValues[17]);
									db.ProcedureInParm("PREMARKS1",DbSession.DbType.VARCHAR2,sValues[18]);
									db.ProcedureInParm("PREMARKS2",DbSession.DbType.VARCHAR2,sValues[19]);
									db.ProcedureInParm("PREMARKS3",DbSession.DbType.VARCHAR2,sValues[20]);
									db.ProcedureInParm("PACT_CATEGORY_SEQ",DbSession.DbType.VARCHAR2,sValues[21]);
									db.ProcedureInParm("PHANDLE_NM",DbSession.DbType.VARCHAR2,sValues[22]);
									db.ProcedureInParm("PHANDLE_KANA_NM",DbSession.DbType.VARCHAR2,sValues[23]);
									db.ProcedureInParm("PCOMMENT_LIST",DbSession.DbType.VARCHAR2,sValues[24]);
									db.ProcedureInParm("PCOMMENT_DETAIL",DbSession.DbType.VARCHAR2,sValues[25]);
									db.ProcedureInParm("PCOMMENT_PICKUP",DbSession.DbType.VARCHAR2,sValues[26]);
									db.ProcedureInParm("PPICKUP_FLAG",DbSession.DbType.NUMBER,sValues[27]);
									db.ProcedureInParm("PPRIORITY",DbSession.DbType.NUMBER,sValues[28]);
									db.ProcedureInParm("PSTART_PERFORM_DAY",DbSession.DbType.VARCHAR2,sValues[29]);
									db.ProcedureInParm("POK_PLAY_MASK",DbSession.DbType.NUMBER,sValues[30]);
									db.ProcedureInParm("POFFLINE_DISPLAY_FLAG",DbSession.DbType.NUMBER,sValues[31]);
									db.ProcedureInParm("PLOGIN_MAIL_TEMPALTE_NO",DbSession.DbType.VARCHAR2,sValues[32]);
									db.ProcedureInParm("PLOGIN_MAIL_TEMPALTE_NO2",DbSession.DbType.VARCHAR2,sValues[33]);
									db.ProcedureInParm("PLOGOUT_MAIL_TEMPALTE_NO",DbSession.DbType.VARCHAR2,sValues[34]);
									db.ProcedureInParm("PNA_FLAG",DbSession.DbType.NUMBER,sValues[35]);
									db.ProcedureInArrayParm("PCAST_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,iAttrCount,sAttrTypeSeq);
									db.ProcedureInArrayParm("PCAST_ATTR_SEQ",DbSession.DbType.VARCHAR2,iAttrCount,sAttrSeq);
									db.ProcedureInArrayParm("PCAST_ATTR_INPUT_VALUE",DbSession.DbType.VARCHAR2,iAttrCount,sInputValue);
									db.ProcedureInParm("PCAST_ATTR_RECORD_COUNT",DbSession.DbType.NUMBER,iAttrCount);
									db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
									db.ExecuteProcedure();
								}
							} catch {
								writer.WriteLine(sLine);
							}
						} else {
							writer.WriteLine(sLine);
						}
						break;

					case "1":
						try {
							using (DbSession db = new DbSession()) {
								db.PrepareProcedure("CAST_PIC_IMPORT");
								db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,sValues[1]);
								db.ProcedureInParm("PLOGIN_ID",DbSession.DbType.VARCHAR2,sValues[2]);
								db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,ViCommConst.MAIN_CHAR_NO);
								db.ProcedureInParm("PPIC_SEQ",DbSession.DbType.VARCHAR2,sValues[3]);
								db.ProcedureInParm("PPIC_FILE_NM",DbSession.DbType.VARCHAR2,sValues[4]);
								db.ProcedureInParm("PPROFILE_PIC_FLAG",DbSession.DbType.NUMBER,sValues[5]);
								db.ProcedureInParm("PDISPLAY_POSITION",DbSession.DbType.NUMBER,sValues[6]);
								db.ProcedureInParm("PPIC_TYPE",DbSession.DbType.NUMBER,sValues[7]);
								db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
								db.ExecuteProcedure();
							}
						} catch {
							writer.WriteLine(sLine);
						}
						break;

					case "2":
						try {
							using (DbSession db = new DbSession()) {
								db.PrepareProcedure("CAST_MOVIE_IMPORT");
								db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,sValues[1]);
								db.ProcedureInParm("PLOGIN_ID",DbSession.DbType.VARCHAR2,sValues[2]);
								db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,ViCommConst.MAIN_CHAR_NO);
								db.ProcedureInParm("PMOVIE_SEQ",DbSession.DbType.VARCHAR2,sValues[3]);
								db.ProcedureInParm("PMOVIE_FILE_NM",DbSession.DbType.VARCHAR2,sValues[4]);
								db.ProcedureInParm("PMOVIE_TITLE",DbSession.DbType.VARCHAR2,sValues[5]);
								db.ProcedureInParm("PCHARGE_POINT",DbSession.DbType.NUMBER,sValues[6]);
								db.ProcedureInParm("PUNAUTH_FLAG",DbSession.DbType.NUMBER,sValues[7]);
								db.ProcedureInParm("PUPLOAD_DATE",DbSession.DbType.VARCHAR2,sValues[8]);
								db.ProcedureInParm("PMOVIE_TYPE",DbSession.DbType.NUMBER,sValues[9]);
								db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
								db.ExecuteProcedure();
							}
						} catch {
							writer.WriteLine(sLine);
						}
						break;
				}

			}
			writer.Flush();
			writer.Close();
		}
	}
}
